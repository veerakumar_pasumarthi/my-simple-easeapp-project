<?php
if(defined('STDIN') ){
  //echo("Running from CLI");
}else{
  echo("Not Running from CLI");
  exit();
}
/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 */
//if the directory is one directory above the current file's parent directory http://stackoverflow.com/a/2100763/811207
include(dirname(dirname(dirname(__FILE__))) . "/app/core/command-line-include.php");
echo "before cron job details query\n";
$cron_number = "17";
$sel_cron_number_setting = $dbcon->prepare("SELECT * FROM `cron_file_details` WHERE `sno` = :sno");
$sel_cron_number_setting->bindParam(":sno",$cron_number);
$sel_cron_number_setting->execute(); 
$res_cron_number_setting = $sel_cron_number_setting->fetchAll(PDO::FETCH_ASSOC);
print_r($res_cron_number_setting);
//exit;
foreach($res_cron_number_setting as $res_cron_number_setting_row)
{
	$res_state = $res_cron_number_setting_row["cron_file_status_setting"];
	$res_records = $res_cron_number_setting_row["cron_file_numb_record_limit"];
	$res_loop = $res_cron_number_setting_row["cron_file_numb_loop_count_limit"];
	$res_sleep_min = $res_cron_number_setting_row["cron_file_sleep_min_seconds_limit"];
	$res_sleep_max = $res_cron_number_setting_row["cron_file_sleep_max_seconds_limit"];
	$res_sleep_interval = $res_cron_number_setting_row["cron_file_sleep_interval"];
	
}
if ( $res_state == "ON" ) {

	echo "inside on condition \n";		
	for ( $i = 1; $i <=$res_loop; $i++ ) {
		echo "entered into loop\n";
		
		$elastic_search_job_rel_onboarding_data_select_sql = "SELECT * FROM `jobs` j  LEFT JOIN `job_rel_other_requirements` jror ON j.job_id = jror.job_id WHERE j.elasticsearch_content_upload_status = '0' LIMIT :res_records";
		echo "after select query\n";
		
		$elastic_search_job_rel_onboarding_data_select_query = $dbcon->prepare($elastic_search_job_rel_onboarding_data_select_sql);
		$elastic_search_job_rel_onboarding_data_select_query->bindValue(":res_records",$res_records);
		$elastic_search_job_rel_onboarding_data_select_query->execute(); 
	
		if($elastic_search_job_rel_onboarding_data_select_query->rowCount() > 0) {
			echo "after select query row count \n";
			$elastic_search_job_rel_onboarding_data_select_query_result = $elastic_search_job_rel_onboarding_data_select_query->fetchAll();
			
			foreach ($elastic_search_job_rel_onboarding_data_select_query_result as $elastic_search_job_rel_onboarding_data_select_query_result_row) {
				
				
				
				$constructed_array = array();
				$constructed_array["job_id"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["job_id"];
				$constructed_array["company_id"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["company_id"];
				$constructed_array["company_client_id"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["company_client_id"];
				/* $constructed_array["job_posted_by_member_classification_detail_id"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["job_posted_by_member_classification_detail_id"];
				$constructed_array["job_posted_by_sm_memb_id"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["job_posted_by_sm_memb_id"]; */
				$constructed_array["job_title"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["job_title"];
				$constructed_array["job_summary"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["job_summary"];
				$constructed_array["job_full_description"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["job_full_description"];
				$constructed_array["job_type_name"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["job_type_name"];
				$constructed_array["job_industry_name"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["job_industry_name"];
				$constructed_array["job_work_location_requirement_name"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["job_work_location_requirement_name"];
				$constructed_array["job_recruitment_status"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["job_recruitment_status"];
				$constructed_array["resume_submission_end_date"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["resume_submission_end_date"];
				$constructed_array["no_of_openings"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["no_of_openings"];
				$constructed_array["available_positions"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["available_positions"];
				
				$constructed_array["maximum_allowed_submittals_count"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["maximum_allowed_submittals_count"];
				$constructed_array["address_line_1"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["address_line_1"];
				$constructed_array["address_line_2"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["address_line_2"];
				$constructed_array["city"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["city"];
				$constructed_array["state"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["state"];
				$constructed_array["country"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["country"];
				/* $constructed_array["job_posted_date_time"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["job_posted_date_time"]; */
				$constructed_array["expected_job_start_date"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["expected_job_start_date"];
				$constructed_array["zipcode"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["zipcode"];
				
				
				$get_primary_recruiter_details_result = get_primary_recruiter_details($constructed_array["job_id"]);
				$constructed_array["primary_recruiter_id"] = $get_primary_recruiter_details_result["assigned_user_sm_memb_id"];
				$sm_firstname = $get_primary_recruiter_details_result["sm_firstname"]; 
				$sm_middlename= $get_primary_recruiter_details_result["sm_middlename"]; 
				$sm_lastname = $get_primary_recruiter_details_result["sm_lastname"]; 
				$constructed_array["primary_recruiter_name"] = $sm_firstname . " " .$sm_middlename. " ".$sm_lastname;
				
				$constructed_array["ot"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["ot"];
				$constructed_array["job_references"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["job_references"];
				$constructed_array["travel"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["travel"];
				$constructed_array["drug_test"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["drug_test"];
				$constructed_array["background_check"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["background_check"];
				$constructed_array["security_clearance"] = $elastic_search_job_rel_onboarding_data_select_query_result_row["security_clearance"];
				
				$job_id = $constructed_array["job_id"];
				
				//Check if index exists
				if ($elasticsearch_jobs_db_existence_status_result === true) {
					//index (as noun) Exists
					echo "index exist \n";
					//Search a Document, to find Duplicates (Exact Match)
					$document_dup_check_params = [
						'index' => $elasticsearch_jobs_index,
						//'type' => $elasticsearch_index_type,
						'body' => [
							'query' => [
								'match' => [
									'job_id' => $job_id
								]
							]
						]
					];

					$document_dup_check_response = $elasticsearch_client->search($document_dup_check_params);
					echo "<pre>";
					print_r($document_dup_check_response); 

					$duplicate_records_count = $document_dup_check_response["hits"]["total"]["value"];
					
					echo "duplicate_records_count:";
					print_r($duplicate_records_count);
					if ($duplicate_records_count == 0) {
						echo "no duplicate_records_count \n";
						//Do Index the Document (Insert)
						//print_r($constructed_array);
						//Index a Document (Insert a Document) 
						$elasticsearchDocumentIndexParams = [
							'index' => $elasticsearch_jobs_index,
							//'type' => $elasticsearch_index_type,
							//'id' => 'my_id',
							'body' => $constructed_array
						
						];	
						
						$elasticsearch_document_index_response = $elasticsearch_client->index($elasticsearchDocumentIndexParams);
						print_r($elasticsearch_document_index_response);
						echo "document created successfully";
						
						if($elasticsearch_document_index_response["_id"] != "" && $elasticsearch_document_index_response["result"] == 'created') {
						
							$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
					
							$elasticsearch_content_upload_status = "1";
							
							$elasticsearch_content_upload_status_update_sql = "UPDATE `jobs` SET `elasticsearch_content_upload_status`=:elasticsearch_content_upload_status,`elasticsearch_document_id`=:elasticsearch_document_id,`last_updated_datetime`=:last_updated_date_time,`last_updated_datetime_epoch`=:last_updated_date_time_epoch WHERE `job_id` = :job_id";
							$elasticsearch_content_upload_status_update_query = $dbcon->prepare($elasticsearch_content_upload_status_update_sql);
							$elasticsearch_content_upload_status_update_query->bindValue(":elasticsearch_content_upload_status",$elasticsearch_content_upload_status);
							$elasticsearch_content_upload_status_update_query->bindValue(":job_id",$job_id);
							$elasticsearch_content_upload_status_update_query->bindValue(":elasticsearch_document_id",$elasticsearch_document_index_response['_id']);
							$elasticsearch_content_upload_status_update_query->bindValue(":last_updated_date_time",$event_datetime);
							$elasticsearch_content_upload_status_update_query->bindValue(":last_updated_date_time_epoch",$current_epoch);	
							$elasticsearch_content_upload_status_update_query->execute();
						
						}
					}//close of if ($duplicate_records_count == 0) {
					
					
				}
			}
		}
     
	
	}//close of for ( $i = 1; $i <=$res_loop; $i++ ) {
	
}//close of if ( $res_state == "ON" ) {
function get_primary_recruiter_details($job_id){
	
	global $dbcon, $eventLog;
	$constructed_array = array();
    
	$get_primary_recruiter_details_get_sql = "SELECT jma.assigned_user_sm_memb_id,sm.sm_firstname,sm.sm_middlename,sm.sm_lastname FROM `job_management_assignees` jma JOIN `site_members` sm ON jma.assigned_user_sm_memb_id = sm.sm_memb_id WHERE jma.assigned_job_specific_role = '3' AND jma.assignment_status = '1' AND jma.job_id = :job_id";
	
	$get_primary_recruiter_details_select_query = $dbcon->prepare($get_primary_recruiter_details_get_sql);
    $get_primary_recruiter_details_select_query->bindValue(":job_id",$job_id);
    $get_primary_recruiter_details_select_query->execute();
	  
	if($get_primary_recruiter_details_select_query->rowCount() > 0) {
		$get_primary_recruiter_details_select_query_result = $get_primary_recruiter_details_select_query->fetch();
		return $get_primary_recruiter_details_select_query_result;
	
	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
	
}

?>