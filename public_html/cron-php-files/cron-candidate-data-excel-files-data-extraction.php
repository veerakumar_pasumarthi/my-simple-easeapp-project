<?php
if(defined('STDIN') ){
  //echo("Running from CLI");
}else{
  echo("Not Running from CLI");
  exit();
}

ini_set('memory_limit', '-1');
//PCRE Backtrack Limit is increased from 1000000 to 4000000, to handle supporting, bigger images in PDF Files using PDF to Text Library
ini_set('pcre.backtrack_limit', 4000000);
//if the directory is one directory above the current file's parent directory http://stackoverflow.com/a/2100763/811207
include(dirname(dirname(dirname(__FILE__))) . "/app/core/command-line-include.php");

$cron_number = "9";
$sel_filem_setting = $dbcon->prepare("SELECT * FROM `cron_file_details` WHERE `sno` = :sno");
$sel_filem_setting->bindParam(":sno",$cron_number);
$sel_filem_setting->execute(); 
$res_filem_setting = $sel_filem_setting->fetchAll(PDO::FETCH_ASSOC);
//print_r($res_filem_setting);
//exit;
foreach($res_filem_setting as $res_filem_setting_row)
{
	$res_state_filem = $res_filem_setting_row["cron_file_status_setting"];
	$res_records_filem = $res_filem_setting_row["cron_file_numb_record_limit"];
	$res_loop_filem = $res_filem_setting_row["cron_file_numb_loop_count_limit"];
	$res_sleep_min_filem = $res_filem_setting_row["cron_file_sleep_min_seconds_limit"];
	$res_sleep_max_filem = $res_filem_setting_row["cron_file_sleep_max_seconds_limit"];
	$res_sleep_interval_filem = $res_filem_setting_row["cron_file_sleep_interval"];
	
}




if ( $res_state_filem == "ON" ) {

			
	for ( $filem = 1; $filem <=$res_loop_filem; $filem++ ) {

function output_collected ($message) {
	if  (php_sapi_name( )  ==  'cli') {
		return $message;
	} else {
	    return nl2br($message);
	}	
}
/*
$inputFileName = '../excel-file-uploads/maillist.xlsx';
//echo "chosen file name: " . $inputFileName . "<br>\n";



$reader->open($inputFileName);
//print_r($reader->getSheetIterator());


foreach ($reader->getSheetIterator() as $sheet) {
	$sheetName = $sheet->getName();
	foreach ($sheet->getRowIterator() as $rowData_details) {
		// do stuff with the row
		print_r($rowData_details);
		exit;
	}
}

*/


//exit;
        //Get List of Candidate Data Excel Files, those that are with non-processed status
		$get_candidate_Data_excel_files_sql = "SELECT * FROM `candidate_data_excel_files` WHERE ((`is_active_status` =:is_active_status0) OR (`is_active_status` =:is_active_status6)) LIMIT 0,".$res_records_filem;
		//echo $get_candidate_Data_excel_files_sql . "\n";
		$get_candidate_Data_excel_files_select_query = $dbcon->prepare($get_candidate_Data_excel_files_sql);
		$get_candidate_Data_excel_files_select_query->bindValue(":is_active_status0", "0");
		$get_candidate_Data_excel_files_select_query->bindValue(":is_active_status6", "6");
		$get_candidate_Data_excel_files_select_query->execute(); 
		$get_candidate_Data_excel_files_select_query_result = $get_candidate_Data_excel_files_select_query->fetchAll(PDO::FETCH_ASSOC);
	    //print_r($get_candidate_Data_excel_files_select_query_result);
		//exit;
		if(count($get_candidate_Data_excel_files_select_query_result) > 0){
		    
			foreach ($get_candidate_Data_excel_files_select_query_result as $get_candidate_Data_excel_files_select_query_row) {	
                $candidate_data_excel_file_id = $get_candidate_Data_excel_files_select_query_row["candidate_data_excel_file_id"];
				$original_file_name = $get_candidate_Data_excel_files_select_query_row["original_file_name"];				
				$generated_file_name = $get_candidate_Data_excel_files_select_query_row["generated_file_name"];
				$document_extension = $get_candidate_Data_excel_files_select_query_row["document_extension"];
				$added_by_admin_user_id = $get_candidate_Data_excel_files_select_query_row["added_by_admin_user_id"];
				$processing_confirmation_mode = $get_candidate_Data_excel_files_select_query_row["processing_confirmation_mode"];
				$uploaded_datetime = $get_candidate_Data_excel_files_select_query_row["uploaded_datetime"];
				$uploaded_datetime_epoch = $get_candidate_Data_excel_files_select_query_row["uploaded_datetime_epoch"];
				$last_attempted_datetime = $get_candidate_Data_excel_files_select_query_row["last_attempted_datetime"];
				$last_attempted_datetime_epoch = $get_candidate_Data_excel_files_select_query_row["last_attempted_datetime_epoch"];
				$is_active_status = $get_candidate_Data_excel_files_select_query_row["is_active_status"];
				 
				 
				 
				$candidate_data_excel_file_document_path = dirname(dirname(__FILE__)) . "/excel-file-uploads/" . $generated_file_name;
				//echo "candidate_data_excel_file_document_path: " . $candidate_data_excel_file_document_path . "\n";
				//exit;
				if (file_exists($candidate_data_excel_file_document_path)) {
					//echo "The file $filename exists";
					
					if ($document_extension == "xlsx") {
						//The File Format of the being processed candidate data excel file document is XLSX
						//$inputFileName = '../excel-file-uploads/maillist.xlsx';
						//$inputFileName = dirname(dirname(__FILE__)) . "/excel-file-uploads/maillist.xlsx";
						$inputFileName = dirname(dirname(__FILE__)) . "/excel-file-uploads/" . $generated_file_name;
						echo "chosen file name: " . $inputFileName . "\n";

						$reader->open($inputFileName);
						//print_r($reader->getSheetIterator());
						
						//Last Updated Time, as per Indian Standard Time
						$last_updated_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Asia/Kolkata");
						
						//Processing Confirmation
						if (($processing_confirmation_mode == "automatic") && ($is_active_status == "6")) {
							//echo "automatic confirmation of field mapping, between Database requirements and the Headings, from the Excel File \n";
							
							//Loop through the Excel File, to Start identifying the Headings Row, while Excluding the Rest of the Content
							foreach ($reader->getSheetIterator() as $sheet) {
								$sheetName = $sheet->getName();
								
								foreach ($sheet->getRowIterator() as $rowData_details) {
									
									// do stuff with the row
									//print_r($rowData_details);
									
									
									// Do Clean the Content, in Each Cell, of the row (i.e. Remove HTML Tags etc...)
									$rowData_details = clean_excel_file_row_content($rowData_details);
									
									//Do Count the Number of Columns, in the Row
									$rowData_details_column_count = count($rowData_details);
									//echo "column count: " . $rowData_details_column_count . "\n";
									
									//Do Find the Heading Row, i.e., the Row, that does not have Empty Columns, while ensuring the input, to the function, to be an Array (Numeric Index Array that comprises of all Column values, of the particular Row)
									$array_value_empty_check_result = is_any_array_value_empty($rowData_details);
									echo "array_value_empty_check_result: " . $array_value_empty_check_result . "\n";
									
									/*
									-> $rowData_details has the row information
									-> for (loop), this, and get a column
									-> A multi-dimensional array with array definitions of different field names
									-> map the column value / key, to the required reference
									*/
									
									//Define an array, to hold Headings / Column Names, in the Headings Row
									$headings_array = array();								
									
									if (($array_value_empty_check_result != "not-array") && ($array_value_empty_check_result === false)) {
										//The Heading row, is chosen
										echo "all columns in the row are with values\n";
										
										//Create an Array, to hold the list of Column names / Headings, as part of the Headings Row
										
										for ($row = 0; $row < $rowData_details_column_count; $row++) {	
											if ($rowData_details[$row] != "") {
												
												//Create the Lowercase version of the Column Name / Field Name, w.r.t. Headings Row
												$field_name = trim(mb_strtolower($rowData_details[$row]));
												//echo "\n field_name: " . $field_name . "\n";
												
												//Check, if the particular Column Name, of the Heading Row, is matching with possible defined alternatives
												//$candidate_data_excel_file_headers_array from /app/core/main-config.php
												foreach ($candidate_data_excel_file_headers_array as $candidate_data_excel_file_headers_array_key => $candidate_data_excel_file_headers_array_value) {
													if(in_array($field_name, $candidate_data_excel_file_headers_array_value)) {
														//echo "\n value is in multidim array, key:" . $candidate_data_excel_file_headers_array_key . " \n";
														$candidate_data_excel_file_headers_array[$candidate_data_excel_file_headers_array_key]["field_value_ref"] = (string)$row;
														$candidate_data_excel_file_headers_array[$candidate_data_excel_file_headers_array_key]["field_name"] = (string)$field_name;
														$headings_array[] =  (string)$field_name;
													}
												}//close of foreach ($candidate_data_excel_file_headers_array as $candidate_data_excel_file_headers_array_key => $candidate_data_excel_file_headers_array_value) {
												
											} else {
												//The Column Name is EMPTY / NULL
											}//Close of else of if ($rowData_details[$row] != "") {
												
										}//close of for ($row = 0; $row < $rowData_details_column_count; $row++) {
											
										break;
										
									} else {
										echo "one or more columns in the row are empty\n";
										
										//break; // Note: the loop has to be allowed to happen, so, it will find the Headings Row correctly
									}
									
									
								}//close of foreach ($sheet->getRowIterator() as $rowData_details) {
								
								//Excel Files, with Multiple Sheets, only first sheet, will be processed, while the rest, will not be Processed
								break;
								
							}//close of foreach ($reader->getSheetIterator() as $sheet) {
							
							/*//Do Process the Content Rows, only if, there are no Empty Columns, in the Headings Row
							
							Note: The Assumption is, there will not be any Columns / Fields, that do not have a Heading / Column Name / Field Name
							*/
							if (($array_value_empty_check_result != "not-array") && ($array_value_empty_check_result === false)) {
								
								
								//Mark the Excel File, as In Progress
								update_processing_status_based_on_candidate_data_excel_file_id_input($candidate_data_excel_file_id, "5", $last_updated_datetime, $current_epoch);
								//exit;	
									
								//Remove Duplicate Heading Names from the Headings Array
								$headings_array_unique = array_unique($headings_array);
								
								echo "\n start processing content \n";	
								
								//Loop through the Excel File, to Start Processing Content, by Excluding the Headings Row
								foreach ($reader->getSheetIterator() as $sheet) {
									$sheetName = $sheet->getName();
									
									foreach ($sheet->getRowIterator() as $rowData_details) {
										
										// do stuff with the row
										//print_r($rowData_details);
										
										
										// Do Clean the Content, in Each Cell, of the row (i.e. Remove HTML Tags etc...)
										$rowData_details = clean_excel_file_row_content($rowData_details);
										
										//Create the Lowercase version of the Column Name / Field Name, w.r.t. Content Row
										$field_name_from_file_lowercase = trim(mb_strtolower($rowData_details[$candidate_data_excel_file_headers_array["s_no"]["field_value_ref"]]));
										
										
										
										//Do Check, if the Column Name / Field Name, w.r.t. Content Row, is Not Empty
										if($field_name_from_file_lowercase != "") {
											echo "\n field_name_from_file_lowercase: " . $field_name_from_file_lowercase . "\n";
											//print_r($headings_array_unique);
											
											//Do Check, the First Column Value, among the Column Names of the Heading Row (from the Excel File) 
											if(!in_array($field_name_from_file_lowercase, $headings_array_unique)) {
												
												echo "\n This is a Content Row, and not the Heading Row " . " \n";
												
												echo "inside row";
												//print_r($rowData_details);
												//echo "column count: " . count($rowData_details) . "\n";
												//print_r($rowData_details);
												//var_dump($rowData_details);
												//exit;
												
												
												//echo "\n sample s.no: " . $candidate_data_excel_file_headers_array["s_no"]["field_value_ref"] . "\n";
												//echo "\n rowdata_details_content: " . $rowData_details[$candidate_data_excel_file_headers_array["s_no"]["field_value_ref"]] . "\n";
												//$column_ = mb_strtolower($str);
												/*$s_no = $rowData_details[0];
												$first_name = $rowData_details[1];
												$last_name = $rowData_details[2];
												$mobile_number = $rowData_details[3];
												$email_id = $rowData_details[4];
												$location = $rowData_details[5];
												$skillsets = $rowData_details[6];
												$department = $rowData_details[7];
												$experience_level = $rowData_details[8];
												$initial_screening_comment = $rowData_details[9];
												$added_by = $rowData_details[10];*/
												$s_no = $rowData_details[$candidate_data_excel_file_headers_array["s_no"]["field_value_ref"]];
												$first_name = $rowData_details[$candidate_data_excel_file_headers_array["first_name"]["field_value_ref"]];
												
												//Do change all text, to lowercase and then make the first letter uppercase
												$first_name = ucwords(trim(mb_strtolower($first_name)));
												
												$last_name = $rowData_details[$candidate_data_excel_file_headers_array["last_name"]["field_value_ref"]];
												
												//Do change all text, to lowercase and then make the first letter uppercase
												$last_name = ucwords(trim(mb_strtolower($last_name)));
												
												$mobile_number = $rowData_details[$candidate_data_excel_file_headers_array["mobile_number"]["field_value_ref"]];
												$email_id = $rowData_details[$candidate_data_excel_file_headers_array["email_id"]["field_value_ref"]];
												
												//Do Sanitize the received Email ID and then Validate that, to the required Email Format
												$email_id = filter_var($email_id, FILTER_SANITIZE_EMAIL);
												
												if (filter_var($email_id, FILTER_VALIDATE_EMAIL)) {
												  //echo("$email_id is a valid email address");
												  
												} else {
												  //echo("$email_id is not a valid email address");
												  $email_id = "";
												}//Close of else of if (filter_var($email_id, FILTER_VALIDATE_EMAIL)) {
												
												if ($email_id != "") {
													$candidate_email_id = $email_id;
												}//close of if ($email_id != "") {
													
												
												$location = $rowData_details[$candidate_data_excel_file_headers_array["location"]["field_value_ref"]];
												
												//Do change all text, to lowercase and then make the first letter uppercase
												$location = ucwords(trim(mb_strtolower($location)));
												
												$city = $location;
												
												$country = "India";
												
												//Do change all text, to lowercase and then make the first letter uppercase
												$country = ucwords(trim(mb_strtolower($country)));
												
												$country_details = get_country_info_from_country_name_input($country);
												if ($country_details != "") {
													$country_id = $country_details["country_id"];
													$country_two_lettered_code = $country_details["two_lettered_code"];
													
													$constructed_country_details = $country_id . ":::::" . $country . ":::::" . $country_two_lettered_code;
												} else {
													$country_id = "";
													$country_two_lettered_code = "";
													
													$constructed_country_details = "";
												}//close of else of if ($country_details != "") {
												
												if ($constructed_country_details != "") {
													$country_final = $constructed_country_details;
												}//close of if ($constructed_country_details != "") {
													
												
												/*
												Country
												103:::::India:::::IN
												*/
												
												$skillsets = $rowData_details[$candidate_data_excel_file_headers_array["skillset"]["field_value_ref"]];
												//$skillsets = "Account Management, Channel Management, Business Development";
												/*
												skillsets have to be mentioned as comma separated values
												example: 
												Account Management
												or
												Account Management, Channel Management, Business Development
												
												Array
												(
													[0] => 4:::::C-Level Presentations:::::c-level-presentations
													[1] => 6:::::Consultative Selling:::::consultative-selling
													[2] => 7:::::Contract Negotiation:::::contract-negotiation
													[3] => 8:::::Cross-Selling / Up-Selling:::::cross-selling-up-selling
													[4] => 9:::::Customer Retention:::::customer-retention
												)
												
												exploded result, when skillsets in csv format, is exploded
												Array
												(
													[0] => Account Management
													[1] =>  Channel Management
													[2] =>  Business Development
												)
												
												Array
												(
													[0] => 1:::::Account Management:::::account-management
													[1] => 3:::::Channel Management:::::channel-management
													[2] => 2:::::Business Development:::::business-development
												)

												*/
												
												$skillsets_exploded = explode(",",$skillsets);
												$skillsets_exploded_count = count($skillsets_exploded);
												//echo "<pre>";
												//print_r($skillsets_exploded);
												//echo "\n skillsets_exploded_count: " . $skillsets_exploded_count . "\n";
												//exit;
												
												$skillsets_constructed_array = array();
												
												for ($skillset_row = 0; $skillset_row < $skillsets_exploded_count; $skillset_row++) {
													//Trim the Skillset Name, to ensure, no extra spaces exist, around the phrase
													$skillset_name = trim($skillsets_exploded[$skillset_row]);
													echo "\n skillset_name: " . $skillset_name . "\n";
													
													if ($skillset_name != "") {
													
														
														$skillset_details = get_skillset_details_from_skillset_name_input($skillset_name);
														
														if ($skillset_details != "") {
															//Skillset Exists, Get the Details, of the Chosen Skillset
															$skillset_id = $skillset_details["skillset_id"];
															$skillset_seo_name = $skillset_details["skillset_seo_name"];
															
															$constructed_skillset_details = $skillset_id . ":::::" . $skillset_name . ":::::" . $skillset_seo_name;
															
														} else {
															//This Skillset does not exist!!!
															
															$constructed_skillset_details = "";
															
														}//close of else of if ($skillset_details != "") {
														
														//Create a New Array, that has skillset_id, skillset_name, skillset_seo_name details constructed as a string, for all skillsets, for this candidate
														if ($constructed_skillset_details != "") {
															$skillsets_constructed_array[] = (string)$constructed_skillset_details;
														}//close of if ($constructed_skillset_details != "") {
															
														
													} else {
														//The Column Name is EMPTY / NULL
														
														echo "\n The Skillset Name is Empty / NULL \n";
														
													}//Close of else of if ($skillset_name != "") {
													
												}//close of for ($skillset_row = 0; $skillset_row < $skillsets_exploded_count; $skillset_row++) {
												//print_r($skillsets_constructed_array);
												
												//Skillsets Confirmed List Count
												$skillsets_constructed_array_count = count($skillsets_constructed_array);
												//echo "skillsets_constructed_array count: " . count($skillsets_constructed_array) . "\n";
												
												$skills_input_json = json_encode($skillsets_constructed_array);
												
												
												$department = $rowData_details[$candidate_data_excel_file_headers_array["department"]["field_value_ref"]];
												
												/*
												Department:

												example:
												Sales
												or
												Sales, Marketing
												or
												Marketing
												
												Array
												(
													[0] => 1
													[1] => 2
												)
												
												Array
												(
													[0] => 1
												)

												*/
												
												$department_exploded = explode(",",$department);
												$department_exploded_count = count($department_exploded);
												//echo "<pre>";
												//print_r($department_exploded);
												//echo "\n department_exploded_count: " . $department_exploded_count . "\n";
												//exit;
												
												$department_constructed_array = array();
												
												for ($department_row = 0; $department_row < $department_exploded_count; $department_row++) {
													//Trim the Department Name, to ensure, no extra spaces exist, around the phrase
													$department_name = trim($department_exploded[$department_row]);
													echo "\n department_name: " . $department_name . "\n";
													
													if ($department_name != "") {
													
														
														$department_details = get_department_info_from_department_name_input($department_name);
														
														if ($department_details != "") {
															//Department Exists, Get the Details, of the Chosen Department
															$department_id = $department_details["department_id"];
															$department_seo_name = $department_details["department_seo_name"];
															
															$constructed_department_details = $department_id;
															
														} else {
															//This Department does not exist!!!
															
															$constructed_department_details = "";
															
														}//close of else of if ($department_details != "") {
														
														//Create a New Array, that has department_id, for this candidate
														if ($constructed_department_details != "") {
															$department_constructed_array[] = (string)$constructed_department_details;
														}//close of if ($constructed_department_details != "") {
															
														
													} else {
														//The Column Name is EMPTY / NULL
														
														echo "\n The Department Name is Empty / NULL \n";
														
													}//Close of else of if ($department_name != "") {
													
												}//close of for ($department_row = 0; $department_row < $department_exploded_count; $department_row++) {
												//print_r($department_constructed_array);
												
												//Skillsets Confirmed List Count
												$department_constructed_array_count = count($department_constructed_array);
												//echo "department_constructed_array count: " . count($department_constructed_array) . "\n";
												
												$department_ids_input_json = json_encode($department_constructed_array);
												
												
												$experience_level = $rowData_details[$candidate_data_excel_file_headers_array["experience_level"]["field_value_ref"]];
												$experience_level = trim(mb_strtolower($experience_level));
												/*experience level,

												example: 

												entry-level
												or
												mid-level
												or
												senior-level
												or 
												none (default value, if any of the other options, are not selected)
												*/
												if ($experience_level == "") {
													$experience_level = "none";
												}
												$initial_screening_comment = $rowData_details[$candidate_data_excel_file_headers_array["initial_screening_comment"]["field_value_ref"]];
												
												if ($initial_screening_comment != "") {
													$initial_screening_comment_details_all_input = trim($initial_screening_comment);
													$initial_screening_comment_details_safe_html = get_cleaned_safe_html_content_input($initial_screening_comment_details_all_input, $page_content_file_config);
												
												} else {
													$initial_screening_comment_details_all_input = null;
													$initial_screening_comment_details_safe_html = null;
												
												}//close of else of if ($initial_screening_comment != "") {
												
												
												$added_by = $rowData_details[$candidate_data_excel_file_headers_array["added_by"]["field_value_ref"]];
												//$added_by = "Sai Sudha SVS";
												//$added_by = "Sri Veera venkata satya Narayana swamy";
												//$added_by = "Swetha arun";
												$added_by_exploded = explode(" ",$added_by);
												$added_by_exploded_count = count($added_by_exploded);
												//echo "<pre>";
												//print_r($added_by_exploded);
												//echo "\n added_by_exploded_count: " . $added_by_exploded_count . "\n";
												//exit;
												if ($added_by_exploded_count == "2") {
													$added_by_firstname = $added_by_exploded[0];
													$added_by_lastname = $added_by_exploded[1];
												} else if ($added_by_exploded_count > "2") {
													$added_by_exploded_c = $added_by_exploded;
													$lastname = array_pop($added_by_exploded_c);
													
													$count_minus_1 = $added_by_exploded_count-1;
													
													$added_by_firstname = implode(" ", $added_by_exploded_c);
													$added_by_lastname = $added_by_exploded[$count_minus_1];
													
													
												}//close of else of if ($added_by_exploded_count == "2") {
													
												$added_by_firstname = ucwords(trim(mb_strtolower($added_by_firstname)));
												$added_by_lastname = ucwords(trim(mb_strtolower($added_by_lastname)));
												
												echo "added_by_firstname: " . $added_by_firstname . "\n";
												echo "added_by_lastname: " . $added_by_lastname . "\n";
												
												$admin_user_details = get_admin_user_details_admin_user_firstname_lastname_input($added_by_firstname, $added_by_lastname);
												
												if ($admin_user_details != "") {
													$admin_user_sm_memb_id = $admin_user_details["sm_memb_id"];
													
												} else {
													$admin_user_sm_memb_id = "";
												}//close of else of if ($admin_user_details != "") {
												
												echo "\n admin_user_sm_memb_id: " . $admin_user_sm_memb_id . "\n";
												
												//Standard Inputs (based on Conventions)
												
												$profile_source_id = "1";
												$profile_source_name = "Direct";
												$member_type = "Individual";
												
												$is_ama_invitation_required_input = "0";
												
												$data_source = "excel-file";
												
												//Candidate info Added Date and Time, as per Indian Standard Time
												$added_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Asia/Kolkata");
												
												/*//commented on 06-03-2018 by raghu, when is_processed = 0, that denotes to be confirmed status
												$is_processed = "0";*/
												//is_processed = 1, that denotes, confirmed status
												$is_processed = "1";
												
												$is_active_status = "1";
												
												
												//last name is optional
												//initial screening comment is optional
												if (($first_name != "") && ($mobile_number != "") && ($email_id != "") && ($location != "") && ($skillsets_constructed_array_count >= "1") && ($department_constructed_array_count >= "1") && ($admin_user_sm_memb_id != "")) {
													echo "\n entered if condition \n";
													echo "s_no: " . $s_no . "\n";
													echo "first_name: " . $first_name . "\n";
													echo "last_name: " . $last_name . "\n";
													echo "mobile_number: " . $mobile_number . "\n";
													echo "email_id: " . $email_id . "\n";
													echo "location: " . $location . "\n";
													echo "skillsets: " . "\n";
													print_r($skillsets_constructed_array);
													echo "department: " . "\n";
													print_r($department_constructed_array);
													echo "experience_level: " . $experience_level . "\n";
													echo "initial_screening_comment: " . $initial_screening_comment . "\n";
													echo "added_by: " . $added_by . "\n";
													
													//Do Insert the details in to candidates_quick_onboarding_data db table
													//SQL Queries
													$candidate_sql = "INSERT INTO `candidates_quick_onboarding_data`(`profile_source_id`, `profile_source_name`, `email`, `member_type`, `candidate_firstname`, `candidate_lastname`, `departments`, `experience_level`, `skillsets`, `mobile_number`, `city`, `country`, `notes`, `notes_safe_html`, `is_ama_invitation_required`, `added_by_admin_user_id`, `added_datetime`, `added_datetime_epoch`, `data_source`, `candidate_data_excel_file_id`, `is_processed`, `is_active_status`) VALUES (:profile_source_id,:profile_source_name,:email,:member_type,:candidate_firstname,:candidate_lastname,:departments,:experience_level,:skillsets,:mobile_number,:city,:country,:notes,:notes_safe_html,:is_ama_invitation_required,:added_by_admin_user_id,:added_datetime,:added_datetime_epoch,:data_source,:candidate_data_excel_file_id,:is_processed,:is_active_status)";
													
													$candidate_values_array = array(":profile_source_id" => $profile_source_id, ":profile_source_name" => $profile_source_name,":email" => $candidate_email_id, ":member_type" => $member_type, ":candidate_firstname" => $first_name,":candidate_lastname" => $last_name, ":departments" => $department_ids_input_json,":experience_level" => $experience_level, ":skillsets" => $skills_input_json,":mobile_number" => $mobile_number, ":city" => $city, ":country" => $country_final,":notes" => $initial_screening_comment, ":notes_safe_html" => $initial_screening_comment_details_safe_html, ":is_ama_invitation_required" => $is_ama_invitation_required_input, ":added_by_admin_user_id" => $admin_user_sm_memb_id, ":added_datetime" => $added_datetime, ":added_datetime_epoch" => $current_epoch,  ":data_source" => $data_source, ":candidate_data_excel_file_id" => $candidate_data_excel_file_id, ":is_processed" => $is_processed, ":is_active_status" => $is_active_status);
													$last_inserted_candidate_quick_onboarding_data_id = insert_query_get_last_insert_id($candidate_sql, $candidate_values_array);
													
													if ($last_inserted_candidate_quick_onboarding_data_id != "") {
														echo "Candidate details are Added into candidates_quick_onboarding_data db table. \n";
													} else {
														echo "There is an error with saving of the Candidate Details into candidates_quick_onboarding_data db table. \n";
													}//close of else of if ($last_inserted_candidate_quick_onboarding_data_id != "") {
												
													
												} else {
													echo "\n entered else condition \n";
													echo "s_no: " . $s_no . "\n";
													echo "first_name: " . $first_name . "\n";
													echo "last_name: " . $last_name . "\n";
													echo "mobile_number: " . $mobile_number . "\n";
													echo "email_id: " . $email_id . "\n";
													echo "location: " . $location . "\n";
													echo "skillsets: " . "\n";
													print_r($skillsets_constructed_array);
													echo "department: " . "\n";
													print_r($department_constructed_array);
													echo "experience_level: " . $experience_level . "\n";
													echo "initial_screening_comment: " . $initial_screening_comment . "\n";
													echo "added_by: " . $added_by . "\n";
													
								
												}//close of else of if (($first_name != "") && ($mobile_number != "") && ($email_id != "") && ($location != "") && ($skillsets_constructed_array_count >= "1") && ($department_constructed_array_count >= "1") && ($admin_user_sm_memb_id != "")) {
													
												//echo "\n" . $s_no . " " . $first_name . " " . $last_name . " " . $mobile_number . " " . $email_id . " " . $location . " " . $skillsets . " " . $department . " " . $experience_level . " " . $initial_screening_comment . " " . $added_by . "\n";
											
											//Single Content Row Exit										
											//exit;	
											} else {
												echo "\n This is the Heading Row " . " \n";
											}//close of else of if(!in_array($field_name_from_file_lowercase, $headings_array_unique)) {
											
										} else {
											echo "\n field_name_from_file_lowercase is Empty" . "\n";
										}//close of if($field_name_from_file_lowercase != "") {	
										
										
										
									}//close of foreach ($sheet->getRowIterator() as $rowData_details) {
										
									//Excel Files, with Multiple Sheets, only first sheet, will be processed, while the rest, will not be Processed
									break;	
									
								}//close of foreach ($reader->getSheetIterator() as $sheet) {	
								
								//Mark the Excel File, as Processed
								update_processing_status_based_on_candidate_data_excel_file_id_input($candidate_data_excel_file_id, "1", $last_updated_datetime, $current_epoch);
								
							} else {
								echo "There are Empty Columns in the Excel File, this Excel File, Cannot be Processed, until the Empty Columns are Removed from the Excel File \n";
								
								//Mark the Excel File, as Rejected, due to Empty Columns
								update_processing_status_based_on_candidate_data_excel_file_id_input($candidate_data_excel_file_id, "7", $last_updated_datetime, $current_epoch);
								
							}//close of else of if (($array_value_empty_check_result != "not-array") && ($array_value_empty_check_result === false)) {
							
							
							
							
						} else if(($processing_confirmation_mode == "manual") && ($is_active_status == "6")) {
							echo "manual confirmation of field mapping, between Database requirements and the Headings, from the Excel File \n";
						}//close of if (($processing_confirmation_mode == "automatic") && ($is_active_status == "0")) {
						

						

						
					} else {
						echo "Not .xlsx file extension \n";
						
						//Mark the Excel File, as Rejected, due to Empty Columns
						update_processing_status_based_on_candidate_data_excel_file_id_input($candidate_data_excel_file_id, "8", $last_updated_datetime, $current_epoch);
								
								
					}//close of else of if ($document_extension == "xlsx") {
					
					
				} else {
					//echo "The file $filename does not exist";
					
					//Mark the Excel File, as Not Found, in the File System
					update_processing_status_based_on_candidate_data_excel_file_id_input($candidate_data_excel_file_id, "4", $last_updated_datetime, $current_epoch);
							
				}//close of else of if (file_exists($candidate_data_excel_file_document_path)) {
				
				//Clear the file_exists function result
				clearstatcache();
				
				
	
				
				
				//exit;
			}//close of foreach ($get_resumes_select_query_result as $get_resumes_select_query_row) {	
				
		}//close of if(count($get_resumes_select_query_result) > 0){					

		//code related to activity in particular cron file end
		sleep($res_sleep_filem);		
		
	}//close of for ( $filem = 1; $filem <=$res_loop_filem; $filem++ )
		
}//close of if ( $res_state_filem == "ON" ) {
?>