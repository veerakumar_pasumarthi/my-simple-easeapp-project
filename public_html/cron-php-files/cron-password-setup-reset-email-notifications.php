﻿<?php
if(defined('STDIN') ){
  //echo("Running from CLI");
}else{
  echo("Not Running from CLI");
  exit();
}
/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 */
//if the directory is one directory above the current file's parent directory http://stackoverflow.com/a/2100763/811207
include(dirname(dirname(dirname(__FILE__))) . "/app/core/command-line-include.php");

include 'cron-user-signup-email-template.php';

echo "before cron job details query\n";
$cron_number = "1";
$sel_cron_number_setting = $dbcon->prepare("SELECT * FROM `cron_file_details` WHERE `sno` = :sno");
$sel_cron_number_setting->bindParam(":sno",$cron_number);
$sel_cron_number_setting->execute(); 
$res_cron_number_setting = $sel_cron_number_setting->fetchAll(PDO::FETCH_ASSOC);
print_r($res_cron_number_setting);
//exit;
foreach($res_cron_number_setting as $res_cron_number_setting_row)
{
	$res_state = $res_cron_number_setting_row["cron_file_status_setting"];
	$res_records = $res_cron_number_setting_row["cron_file_numb_record_limit"];
	$res_loop = $res_cron_number_setting_row["cron_file_numb_loop_count_limit"];
	$res_sleep_min = $res_cron_number_setting_row["cron_file_sleep_min_seconds_limit"];
	$res_sleep_max = $res_cron_number_setting_row["cron_file_sleep_max_seconds_limit"];
	$res_sleep_interval = $res_cron_number_setting_row["cron_file_sleep_interval"];
	
}


if ( $res_state == "ON" ) {

	echo "inside on condition \n";		
	for ( $i = 1; $i <=$res_loop; $i++ ) {
		echo "entered into loop\n";
		
	$password_setup_select_sql = "SELECT * FROM `password_setup_modification_requests` WHERE `email_sending_status` = :email_sending_status AND `is_active_status` = :is_active_status LIMIT :res_records";
	echo "after select query\n";
	$password_setup_select_query = $dbcon->prepare($password_setup_select_sql);
	$password_setup_select_query->bindValue(":email_sending_status",0);
    $password_setup_select_query->bindValue(":is_active_status",1);
    $password_setup_select_query->bindValue(":res_records",$res_records);	
	$password_setup_select_query->execute(); 
	
	if($password_setup_select_query->rowCount() > 0) {
		echo "after select query row count \n";
		$password_setup_select_query_result = $password_setup_select_query->fetchAll();
		foreach ($password_setup_select_query_result as $password_setup_select_query_result_row) {
		    $sm_memb_id = $password_setup_select_query_result_row["sm_memb_id"];
			$request_purpose = $password_setup_select_query_result_row["request_purpose"];
			$recipient_email_address = $password_setup_select_query_result_row["recipient_email_address"];
			$unique_password_setup_request_ref_code = $password_setup_select_query_result_row["unique_password_setup_request_ref_code"];
			$email_sending_status = $password_setup_select_query_result_row["email_sending_status"];
			$password_setup_modification_request_id = $password_setup_select_query_result_row["password_setup_modification_request_id"];
			
			
            //$plaintext_input = "Hello! welcome to application";
			//$html_message_input = "<p>Hello!</p>";
			
			$candidate_details = user_basic_details_check_based_on_user_id($sm_memb_id);
					
			//Get Candidate's Salutation, Firstname, Middlename and Lastname
			if ($candidate_details != "") {
				$sm_salutation = $candidate_details["sm_salutation"];
				$sm_firstname = $candidate_details["sm_firstname"];
				$sm_middlename = $candidate_details["sm_middlename"];
				$sm_lastname = $candidate_details["sm_lastname"];
				$sm_user_type = $candidate_details["sm_user_type"];
				$sm_admin_level = $candidate_details["sm_admin_level"];
				$sm_user_role = $candidate_details["sm_user_role"];
				$company_id = $candidate_details["company_id"];
				
			} else {
				$sm_salutation = "";
				$sm_firstname = "";
				$sm_middlename = "";
				$sm_lastname = "";
				$sm_user_type = "";
				$sm_admin_level = "";
				$sm_user_role = "";
				$company_id = "";
				
			}//close of else of if ($candidate_details != "") {
			
			$user_classifications_details = sm_user_classification_details_get($sm_memb_id);
			
			if(count($user_classifications_details) > 0){
				
				
				//$user_classification_name = $user_classifications_details["user_privilege_summary"];
				$user_classification_id = $user_classifications_details["sm_site_member_classification_detail_id"];
				
				if ($user_classification_id == "12") {
				
					$user_account_ref = "A Site Admin Account ";
					
				} else if ($user_classification_id == "9") {
					
					$user_account_ref = "A Company Admin Account ";
					
				} else if ($user_classification_id == "7") {
					
					$user_account_ref = "A Lead Recruiter Account ";
					
				} else if ($user_classification_id == "6") {
					
					$user_account_ref = "A Recruiter Account ";
					
				} else if ($user_classification_id == "5") {
					
					$user_account_ref = "A Business Development Manager Account ";
					
				} else if ($user_classification_id == "4") {
					
					$user_account_ref = "A Account Manager Account ";
					
				} else {
					
					$user_account_ref = "A User Account ";
				}
				
				
				
			}
			
			
			if (($sm_firstname != "") && ($sm_middlename != "") && ($sm_lastname != "")) {
				$candidate_fullname = $sm_firstname . " " . $sm_middlename . " " . $sm_lastname;
				$greeting_user_summary = "Dear " . $candidate_fullname . ",";
			} else if (($sm_firstname != "") && ($sm_middlename != "") && ($sm_lastname == "")) {
				$candidate_fullname = $sm_firstname . " " . $sm_middlename;
				$greeting_user_summary = "Dear " . $candidate_fullname . ",";
			} else if (($sm_firstname != "") && ($sm_middlename == "") && ($sm_lastname == "")) {
				$candidate_fullname = $sm_firstname;
				$greeting_user_summary = "Dear " . $candidate_fullname . ",";
			} else if (($sm_firstname != "") && ($sm_middlename == "") && ($sm_lastname != "")) {
				$candidate_fullname = $sm_firstname . " " . $sm_lastname;
				$greeting_user_summary = "Dear " . $candidate_fullname . ",";
			} else if (($sm_firstname == "") && ($sm_middlename != "") && ($sm_lastname != "")) {
				$candidate_fullname = $sm_middlename . " " . $sm_lastname;
				$greeting_user_summary = "Dear " . $candidate_fullname . ",";
			} else if (($sm_firstname == "") && ($sm_middlename == "") && ($sm_lastname != "")) {
				$candidate_fullname = $sm_lastname;
				$greeting_user_summary = "Dear " . $candidate_fullname . ",";
			} else {
				$candidate_fullname = "";
				$greeting_user_summary = "Dear,";
			}//close of else of if (($candidate_firstname != "") && ($sm_middlename != "") && ($candidate_lastname != "")) {
			
			$company_name = "";
			if (!is_null($company_id)) {
					
				$candidate_company_details = get_company_details_based_on_company_id($company_id);
				if ($candidate_company_details != "") {
					
					$company_name = $candidate_company_details["company_name"];
					$company_support_email = $candidate_company_details["company_support_email"];
					
					
					
				} else {
				
					
					$company_name = "";
					$company_support_email = "";
					
				}//close of else of if ($candidate_company_details != "") {	
					
			}//close of if (!is_null($company_id)) {
				
				
			
			//Construct URL
			/*
			
			http://127.0.0.1:8080/LAYOUT-2/STANDARD/#!/account/activate/user-act-code/yd665r5er65r56edd6565te65e56e56e56ter6er5775e57


			Reset Password Link:

			http://127.0.0.1:8080/LAYOUT-2/STANDARD/#!/account/reset-password/user-pass-reset-code/yd665r5er65r56edd6565te65e56e56e56ter6er5775e57dgdfg4t45gdfgfd

			*/
			$constructed_url ="";
			
			if (($request_purpose == "activation-email") || ($request_purpose == "re-send-activation-email")) {
				
				$username = '<span style = "font-size:15px;font-style:normal;font-weight:bold;color:#333333">'.$user_account_ref.'</span> is created for you.';
				$constructed_message_plain_text = $user_account_ref . " is created for you.
			
			    " . "Please click on the following URL, to activate the account and Set your Account Password,";

				
				$constructed_message_html = "</p>
			
				<p>" . $user_account_ref . "is created for you. </p><p>Please click on the following URL, to activate the account and Set your Account Password,</p>";

				$frontend_site_url_project_main = "https://app.resumecrab.com/";
				$constructed_url = $frontend_site_url_project_main."account/activate/user-act-code/" . $unique_password_setup_request_ref_code;
				
				$elastic_email_message_html = "<span style='font-weight:bold;'>NOTE:</span> You will receive another email from “elastic email” for authorization of Mass Email. Please Activate your account";
				
			} else if ($request_purpose == "reset-password") {

				$username = "" ;
				
				$constructed_message_plain_text = "Please click on the following URL, to Re-set your Account Password,";
				$constructed_message_html = "<p>Please click on the following URL, to Re-set your Account Password,</p>";
				$frontend_site_url_project_main = "https://app.resumecrab.com/";
				$constructed_url = $frontend_site_url_project_main."account/reset-password/user-pass-reset-code/" . $unique_password_setup_request_ref_code;
				
				$elastic_email_message_html = "<span style='font-weight:bold;'>NOTE:</span> You will receive another email from “elastic email” for authorization of Mass Email. Please Activate your account";
				
			}//close of else if of if (($request_purpose == "activation-email") || ($request_purpose == "re-send-activation-email")) {
			
			echo "constructed_url: " . $constructed_url . "\n\n";
			
			$account_activation_password_setup_reset_link_without_html = $constructed_url . "\n\n";
			
			//echo "account_activation_password_setup_reset_link_without_html: " . $account_activation_password_setup_reset_link_without_html . "\n\n";
			
			$account_activation_password_setup_reset_link_with_html = "<p><a href='" . $constructed_url . "'>" . $constructed_url . "</a><br><br></p>";
			
			//echo "account_activation_password_setup_reset_link_with_html: " . $account_activation_password_setup_reset_link_with_html . "\n\n";
			
			//Send Email

			$support = "For Support, Please Contact : <span style='font-weight:bold;'>".$company_support_email."</span>" ;
			
			$plaintext_input = $greeting_user_summary . "
			
			" . $constructed_message_plain_text . "
			
			" .  
			

			"\n\n" .
			
			$account_activation_password_setup_reset_link_without_html . "\n". $elastic_email_message_html.

			"\n" .
			"Thank you \n
			Admin Team \n
			" . $company_name . "
			For Support, Please Contact: " . $company_support_email . "
			________________________________________________________________________

			";

			/*$html_message_input = "<div><p>" . $greeting_user_summary . "<br><br>

			" . $constructed_message_html

			. $account_activation_password_setup_reset_link_with_html . "\n" .$elastic_email_message_html.
			 
			"
			
			<p> </p>
			<p>Thank you<br>
			Admin Team <br>
			" . $company_name . "<br>" .
			"<u>For Support, Please Contact:</u> " . $company_support_email . "<br>" .
			"</p>
			<p>________________________________________________________________________</p>

			</div>"; */

			$html_message_input = template($greeting_user_summary ,$username, $constructed_url , $company_name , $elastic_email_message_html, $support) ;

			//echo "plaintext_input: " . $plaintext_input . "<br>";
			//echo "html_message_input: " . $html_message_input . "<br>";
			//exit;
			
			
		if (filter_var($recipient_email_address, FILTER_VALIDATE_EMAIL) == true) {
			
			$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "UTC");
		
			//send_email_using_smtp_thr_phpmailer("notifications@vdmail.securitywonks.net","Innovative Solutions", "notifications@vdmail.securitywonks.net","webmaster@securitywonks.org","Raghu sir","To check email","hello welcome","<p> hello welcome</p>");	
			//exit;
			//commented on 10-02-2019 18:10 $send_email_using_smtp_thr_phpmailer_result = send_email_using_smtp_thr_phpmailer($activation_email_sender_email, $activation_email_sender_name, $activation_email_sender_email, $recipient_email_address,null,$activation_email_subject, $plaintext_input, $html_message_input);

           
			if (($request_purpose == "activation-email") || ($request_purpose == "re-send-activation-email")) {
				
				/* $send_email_using_smtp_thr_phpmailer_result = send_email_using_smtp_thr_phpmailer($activation_email_sender_email, $activation_email_sender_name, $activation_email_sender_email, $recipient_email_address,null,$activation_email_subject, $plaintext_input, $html_message_input); */
				//$to_email[] = "nkrishnaveni@innovativesolutionsorg.com";
				//$to_email[] = $recipient_email_address;
				$to_email = array();
				$to_email[] = $recipient_email_address;

			    $to_email[] = $recipient_email_address;


				$email_subject = "Resumecrab Account Activation";
				$from_email_address = "notifications@resumecrab.com";
				$from_person_name = "Resumecrab Notifications";
				$sender_email_address = "notifications@resumecrab.com";
				$sender_person_name = "Resumecrab Notifications";
				$replyto_email_address = "notifications@resumecrab.com";
				$replyto_person_name = "Resumecrab Notifications";
				
				$send_email_using_elastic_email_result = send_email_using_elastic_email($from_email_address,$from_person_name,$sender_email_address, $sender_person_name, $replyto_person_name,$replyto_email_address, $to_email,$email_subject, $plaintext_input, $html_message_input);
				
			} else if ($request_purpose == "reset-password") {
				
				
				$to_email = array();
				$to_email[] = $recipient_email_address;

				$email_subject = "Resumecrab Account Activation";
				$from_email_address = "notifications@resumecrab.com";
				$from_person_name = "Resumecrab Notifications";
				$sender_email_address = "notifications@resumecrab.com";
				$sender_person_name = "Resumecrab Notifications";
				$replyto_email_address = "notifications@resumecrab.com";
				$replyto_person_name = "Resumecrab Notifications";
				
				/* $send_email_using_smtp_thr_phpmailer_result = send_email_using_smtp_thr_phpmailer($password_resetting_email_sender_email, $password_resetting_email_sender_name, $password_resetting_email_sender_email, $recipient_email_address,null,$password_resetting_email_subject, $plaintext_input, $html_message_input); */
				
				$send_email_using_elastic_email_result = send_email_using_elastic_email($from_email_address,$from_person_name,$sender_email_address, $sender_person_name, $replyto_person_name,$replyto_email_address, $to_email,$activation_email_subject, $plaintext_input, $html_message_input);
				
				
				
			}//close of else if of if (($request_purpose == "activation-email") || ($request_purpose == "re-send-activation-email")) {
			
			
		    echo "after mail function";
			//send_email_using_smtp_thr_phpmailer($from_email_input, $sender_name_input, $reply_to_email_input, $to_email_input, $receiver_name_input, $subject_input, $plaintext_input, $html_message_input);	
	    
		    
			if($send_email_using_elastic_email_result == true){
				
				$new_email_sending_status = "1";
				$email_service_provider_mail_ref_message = "message-sent";
				$sent_datetime = $event_datetime;
				$sent_datetime_epoch = $current_epoch;
				
				
		    } else {
				$new_email_sending_status = "4";
				$email_service_provider_mail_ref_message = null;
				$sent_datetime = null;
				$sent_datetime_epoch = null;
			}
			
			
			$password_setup_update_sql = "UPDATE `password_setup_modification_requests` SET `email_sending_status`=:email_sending_status,`sent_datetime`=:sent_datetime,`sent_datetime_epoch`=:sent_datetime_epoch,`email_service_provider`=:email_service_provider,`email_service_provider_mail_ref_id`=:email_service_provider_mail_ref_id,`email_service_provider_mail_ref_message`=:email_service_provider_mail_ref_message,`last_updated_datetime`=:last_updated_date_time,`last_updated_datetime_epoch`=:last_updated_date_time_epoch,`last_updated_by_sm_memb_id`= :last_updated_by_sm_memb_id WHERE `password_setup_modification_request_id` = :password_setup_modification_request_id";
			
			$password_setup_update_query = $dbcon->prepare($password_setup_update_sql);
			
			
			$password_setup_update_query->bindValue(":email_sending_status",$new_email_sending_status);
			$password_setup_update_query->bindValue(":sent_datetime",$sent_datetime);
			$password_setup_update_query->bindValue(":sent_datetime_epoch",$sent_datetime_epoch);
			$password_setup_update_query->bindValue(":email_service_provider",null);
			$password_setup_update_query->bindValue(":email_service_provider_mail_ref_id",null);
			$password_setup_update_query->bindValue(":email_service_provider_mail_ref_message",$email_service_provider_mail_ref_message);
			$password_setup_update_query->bindValue(":password_setup_modification_request_id",$password_setup_modification_request_id);
			$password_setup_update_query->bindValue(":last_updated_date_time",$event_datetime);
			$password_setup_update_query->bindValue(":last_updated_date_time_epoch",$current_epoch);
            // when cron job processes the record then the value of this column is null since,there is no user id can be attributed to access this record			
			$password_setup_update_query->bindValue(":last_updated_by_sm_memb_id",null);	
			if($password_setup_update_query->execute()){
				
					$user_mass_email_details_get = user_mass_email_details_get($sm_memb_id);
					
					if((isset($user_mass_email_details_get)) && (count($user_mass_email_details_get) > 0)) {
						
						$request_purpose = "resend-activation-email";
									  
				  
						$mass_auth_request_insert_result = mass_auth_request_insert($company_id,$sm_memb_id,$recipient_email_address,$request_purpose,null,$event_datetime,$current_epoch);
						
						
					} else {
						
						$request_purpose = "activation-email";
					/* 					  
					  
						$mass_auth_request_insert_result = mass_auth_request_insert($company_id,$sm_memb_id,$recipient_email_address,$request_purpose,null,$event_datetime,$current_epoch);
						 */
						
					}
					
					  //echo "done";
					  echo "Email Sent Successfully";
					  $curl_response = array();
					  
					  //$curl_url = "https://api.elasticemail.com/v2/validemail/add?apikey=8d3c61bf-91db-462f-8f3a-40f3d724a6c0&emailAddress=$to_email&returnUrl=http://www.resumecrab.com";
					  $ch = curl_init("https://api.elasticemail.com/v2/validemail/add?apikey=8d3c61bf-91db-462f-8f3a-40f3d724a6c0&emailAddress=$recipient_email_address&returnUrl=http://www.resumecrab.com");
					 // $fp = fopen("example_homepage.txt", "w");

					  //curl_setopt($ch, CURLOPT_FILE, $fp);
					  curl_setopt($ch, CURLOPT_HEADER, 0);
					  
					  $curl_response = array();
					  $curl_response = curl_exec($ch);
					  
					  
					  $curl_response_json_decoded = json_decode($curl_response);
					  
					  foreach($curl_response_json_decoded as $curl_response_row_key => $curl_response_row_value) {
						  
						  // {"success":true,"data":{"validemailid":109085,"email":"nkrishnaveni.fss@gmail.com","validated":false}}
						  if($curl_response_row_key == "data") {
							  
							  foreach($curl_response_row_value as $response_values) {
								  
									$validemailid = response_values["validemailid"];
									$email = response_values["email"];
									  
									$last_inserted_id = mass_elastic_email_reg_update($email,$validemailid);
									  
									$mass_elastic_email_reg_update_sql = "UPDATE `site_members` SET `bulk_email_reg_email_id` = :bulk_email_reg_email_id WHERE `sm_memb_id` = :sm_memb_id";
				
									$mass_elastic_email_reg_update_query = $dbcon->prepare($mass_elastic_email_reg_update_sql);
									
									
									$mass_elastic_email_reg_update_query->bindValue(":bulk_email_reg_email_id",$email);
									$mass_elastic_email_reg_update_query->bindValue(":sm_memb_id",$sm_memb_id);
									
									if($mass_elastic_email_reg_update_query->execute()){
										  echo "updated in site members";
										  
										  
										  
									}
									
								}
										 
					  
								  
							}
							  
							  
						}
						  
						  curl_close($ch);
						  
						  
				}//close of if($password_setup_update_query->execute()){
				  
		
			} else {
				echo "Recipient's Email Address is Invalid, as per Email Format Validation Checks \n\n";
			}//close of else of if (filter_var($recipient_email_address, FILTER_VALIDATE_EMAIL) == true) {
			
		} //close of foreach ($password_setup_select_query_result as $password_setup_select_query_result_row)
		
	}//if($password_setup_select_query->rowCount() > 0) {
			
    }//close of for ( $i = 1; $i <=$res_loop; $i++ ) {
	
}//close of if ( $res_state == "ON" ) {

//COPIED FROM other-functions-api.php TEMPORARILY
/*function user_basic_details_check_based_on_user_id($sm_memb_id_input) {
	global $dbcon;
	$constructed_array = array();
	$rest_user_check_sql = "SELECT * FROM `site_members` WHERE `sm_memb_id` = :sm_memb_id";
	$rest_user_check_select_query = $dbcon->prepare($rest_user_check_sql);
	$rest_user_check_select_query->bindValue(":sm_memb_id",$sm_memb_id_input);				 
	$rest_user_check_select_query->execute(); 
	
	if($rest_user_check_select_query->rowCount() > 0) {
		$rest_user_check_select_query_result = $rest_user_check_select_query->fetch();
	     return $rest_user_check_select_query_result;
	
	}//close of if($rest_user_check_select_query->rowCount() > 0) {
	return $constructed_array;
	
}*/

//COPIED FROM other-functions-api.php TEMPORARILY

/*function get_company_details_based_on_company_id($company_id_input) {

    global $dbcon;
	$constructed_array = array();
		$companies_details_get_sql = "SELECT * FROM `companies` WHERE `company_id`=:company_id";
		
		$companies_details_get_select_query = $dbcon->prepare($companies_details_get_sql);
		$companies_details_get_select_query->bindValue(":company_id",$company_id_input);
		$companies_details_get_select_query->execute();
		
	if($companies_details_get_select_query->rowCount() > 0) {
	    $companies_details_get_select_query_result = $companies_details_get_select_query->fetch();
	    //print_r($companies_details_get_select_query_result);
	    	
		return $companies_details_get_select_query_result;
	}
	return $constructed_array;
}*/	

function sm_user_classification_details_get($sm_memb_id_input){
	 global $dbcon;
	$constructed_array = array();
		$companies_details_get_sql = "SELECT smcd.sm_user_role,smcd.sm_site_member_classification_detail_id FROM `sm_site_member_classification_associations` smca JOIN `sm_site_member_classification_details` smcd ON smca.sm_site_member_classification_detail_id = smcd.sm_site_member_classification_detail_id WHERE smca.sm_memb_id=:sm_memb_id";

		$companies_details_get_select_query = $dbcon->prepare($companies_details_get_sql);
		$companies_details_get_select_query->bindValue(":sm_memb_id",$sm_memb_id_input);
		$companies_details_get_select_query->execute();

	if($companies_details_get_select_query->rowCount() > 0) {
	    $companies_details_get_select_query_result = $companies_details_get_select_query->fetch();
	    //print_r($companies_details_get_select_query_result);

		return $companies_details_get_select_query_result;
	}
	return $constructed_array;
	

}

function user_mass_email_details_get($sm_memb_id_input){
	 global $dbcon;
		$constructed_array = array();
		$request_purpose = "activation-email";
		$companies_details_get_sql = "SELECT * FROM `mass_email_sender_auth_reg_requests` WHERE sm_memb_id=:sm_memb_id AND request_purpose = :request_purpose";

		$companies_details_get_select_query = $dbcon->prepare($companies_details_get_sql);
		$companies_details_get_select_query->bindValue(":sm_memb_id",$sm_memb_id_input);
		$companies_details_get_select_query->bindValue(":request_purpose",$request_purpose);
		$companies_details_get_select_query->execute();

	if($companies_details_get_select_query->rowCount() > 0) {
	    $companies_details_get_select_query_result = $companies_details_get_select_query->fetch();
	    //print_r($companies_details_get_select_query_result);

		return $companies_details_get_select_query_result;
	}
	return $constructed_array;
	

}



function mass_auth_request_insert($company_id_input,$sm_memb_id,$email_address,$request_purpose,$user_id,$event_datetime,$current_epoch) {

    global $dbcon,$eventLog;
	if($company_id_input == "") {
		$company_id_input = null;
		
	}
    $resume_text_insert_sql ="INSERT INTO `mass_email_sender_auth_reg_requests`(`company_id`, `sm_memb_id`, `email_address`, `request_purpose`, `added_datetime`, `added_datetime_epoch`, `added_by_sm_memb_id`) VALUES (:company_id,:sm_memb_id,:email_address,:request_purpose,:added_datetime,:added_datetime_epoch,:added_by_sm_memb_id)";
	//$eventLog->log("after query");
	$resume_text_insert_query = $dbcon->prepare($resume_text_insert_sql);
	$resume_text_insert_query->bindValue(":company_id",$company_id_input);
	$resume_text_insert_query->bindValue(":sm_memb_id",$sm_memb_id);
	$resume_text_insert_query->bindValue(":email_address",$email_address);
	$resume_text_insert_query->bindValue(":request_purpose",$request_purpose);
	$resume_text_insert_query->bindValue(":added_by_sm_memb_id",$user_id);
	$resume_text_insert_query->bindValue(":added_datetime",$event_datetime);
	$resume_text_insert_query->bindValue(":added_datetime_epoch",$current_epoch);


		if ($resume_text_insert_query->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			$eventLog->log("record inserted successfully");

			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }
	
}
?>