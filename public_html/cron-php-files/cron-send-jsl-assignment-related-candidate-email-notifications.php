<?php
if(defined('STDIN') ){
  //echo("Running from CLI");
}else{
  echo("Not Running from CLI");
  exit();
}
/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 */
//if the directory is one directory above the current file's parent directory http://stackoverflow.com/a/2100763/811207
include(dirname(dirname(dirname(__FILE__))) . "/app/core/command-line-include.php");
include 'cron-job-related-email-template.php';
echo "before cron job details query\n";
$cron_number = "18";
$sel_cron_number_setting = $dbcon->prepare("SELECT * FROM `cron_file_details` WHERE `sno` = :sno");
$sel_cron_number_setting->bindParam(":sno",$cron_number);
$sel_cron_number_setting->execute(); 
$res_cron_number_setting = $sel_cron_number_setting->fetchAll(PDO::FETCH_ASSOC);
print_r($res_cron_number_setting);
//exit;
foreach($res_cron_number_setting as $res_cron_number_setting_row)
{
	$res_state = $res_cron_number_setting_row["cron_file_status_setting"];
	$res_records = $res_cron_number_setting_row["cron_file_numb_record_limit"];
	$res_loop = $res_cron_number_setting_row["cron_file_numb_loop_count_limit"];
	$res_sleep_min = $res_cron_number_setting_row["cron_file_sleep_min_seconds_limit"];
	$res_sleep_max = $res_cron_number_setting_row["cron_file_sleep_max_seconds_limit"];
	$res_sleep_interval = $res_cron_number_setting_row["cron_file_sleep_interval"];
	
}


if ( $res_state == "ON" ) {

	echo "inside on condition \n";		
	  for ( $i = 1; $i <=$res_loop; $i++ ) {
		echo "entered into loop\n";
				
		$candidate_details_get_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `event_status` = :event_status AND `candidate_notification_status` =:candidate_notification_status";
		echo "after select query\n";
		$candidate_details_select_query = $dbcon->prepare($candidate_details_get_sql);
		$candidate_details_select_query->bindValue(":event_status","1");
		$candidate_details_select_query->bindValue(":candidate_notification_status","0");		
		$candidate_details_select_query->execute(); 
		echo "before select query row count \n\n";
		
			if($candidate_details_select_query->rowCount() > 0) {
				echo "after select query row count \n";
				$candidate_details_select_query_result = $candidate_details_select_query->fetchAll();
				foreach ($candidate_details_select_query_result as $candidate_details_select_query_result_row) {
						
					$jsl_applicant_evaluation_info_id = $candidate_details_select_query_result_row["jsl_applicant_evaluation_info_id"];
					$jsl_classification_detail_id = $candidate_details_select_query_result_row["jsl_classification_detail_id"];
					$jsl_classification_detail_name = $candidate_details_select_query_result_row["jsl_classification_detail_name"];
					$jsl_sub_classification_detail_id = $candidate_details_select_query_result_row["jsl_sub_classification_detail_id"];
					$jsl_sub_classification_detail_name = $candidate_details_select_query_result_row["jsl_sub_classification_detail_name"];
					$job_id = $candidate_details_select_query_result_row["job_id"];
					$job_details_result = get_job_details($job_id);
			        $job_title = $job_details_result["job_title"];
					echo "before getting user details \n";	
				
						$candidate_details = user_details_check_based_on_user_id($jsl_applicant_evaluation_info_id);
					if ($candidate_details != "") {
					$sm_salutation = $candidate_details["sm_salutation"];
					$sm_firstname = $candidate_details["sm_firstname"];
					$sm_middlename = $candidate_details["sm_middlename"];
					$sm_lastname = $candidate_details["sm_lastname"];
					$sm_user_type = $candidate_details["sm_user_type"];
					$sm_admin_level = $candidate_details["sm_admin_level"];
					$sm_user_role = $candidate_details["sm_user_role"];
					$company_id = $candidate_details["company_id"];
					$sm_email = $candidate_details["sm_email"];
					
					
				} else {
					$sm_salutation = "";
					$sm_firstname = "";
					$sm_middlename = "";
					$sm_lastname = "";
					$sm_user_type = "";
					$sm_admin_level = "";
					$sm_user_role = "";
					$company_id = "";
					
				}//close of else of if ($candidate_details != "") {
				
             if (($sm_firstname != "") && ($sm_middlename != "") && ($sm_lastname != "")) {
					$candidate_fullname = $sm_firstname . " " . $sm_middlename . " " . $sm_lastname;
					$greeting_user_summary = "Hello " . $candidate_fullname . ",";
				} else if (($sm_firstname != "") && ($sm_middlename != "") && ($sm_lastname == "")) {
					$candidate_fullname = $sm_firstname . " " . $sm_middlename;
					$greeting_user_summary = "Hello " . $candidate_fullname . ",";
				} else if (($sm_firstname != "") && ($sm_middlename == "") && ($sm_lastname == "")) {
					$candidate_fullname = $sm_firstname;
					$greeting_user_summary = "Hello " . $candidate_fullname . ",";
				} else if (($sm_firstname != "") && ($sm_middlename == "") && ($sm_lastname != "")) {
					$candidate_fullname = $sm_firstname . " " . $sm_lastname;
					$greeting_user_summary = "Hello " . $candidate_fullname . ",";
				} else if (($sm_firstname == "") && ($sm_middlename != "") && ($sm_lastname != "")) {
					$candidate_fullname = $sm_middlename . " " . $sm_lastname;
					$greeting_user_summary = "Hello " . $candidate_fullname . ",";
				} else if (($sm_firstname == "") && ($sm_middlename == "") && ($sm_lastname != "")) {
					$candidate_fullname = $sm_lastname;
					$greeting_user_summary = "Hello " . $candidate_fullname . ",";
				} else {
					$candidate_fullname = "";
					$greeting_user_summary = "Hello,";
				}//close of else of if (($candidate_firstname != "") && ($sm_middlename != "") && ($candidate_lastname != "")) {

              $company_details_result = get_company_details($company_id);
			        $company_name = $company_details_result["company_name"];
                    $company_mobile_number =  $company_details_result["company_mobile_number"];





				
			$constructed_url ="";
							
			/*if (($request_purpose == "activation-email") || ($request_purpose == "re-send-activation-email")) {
								
			$constructed_message_plain_text = $user_account_ref . " is created for you.
							
			" . "Please click on the following URL, to activate the account and Set your Account Password,";
								
			$constructed_message_html = "</p>
							
			<p>" . $user_account_ref . "is created for you. </p><p>Please click on the following URL, to activate the account and Set your Account Password,</p>";
								
			$constructed_url = $frontend_site_url_project_main_with_path . "account/activate/user-act-code/" . $unique_password_setup_request_ref_code;
								
		    } else if ($request_purpose == "reset-password") {
								
			$constructed_message_plain_text = "Please click on the following URL, to Re-set your Account Password,";
			$constructed_message_html = "<p>Please click on the following URL, to Re-set your Account Password,</p>";
								
			$constructed_url = $frontend_site_url_project_main_with_path . "account/reset-password/user-pass-reset-code/" . $unique_password_setup_request_ref_code;
								
			}//close of else if of if (($request_purpose == "activation-email") || ($request_purpose == "re-send-activation-email")) {*/
					
		/* $constructed_message_plain_text = " As per the next steps regarding your application for $job_title  with $company_name, your interview is scheduled as below"; */
		if(($jsl_classification_detail_name == "Document Checks & Verification") &&($jsl_sub_classification_detail_name == "Verify Certificates"))	{
			$constructed_message_plain_text = "You have applied for $job_title position in $company_name\n\n\n For further procedure, the following documents/certificates are requested for verification \n\n\n Please upload these documents in the link below\n\n\n";
			$constructed_message_html = "
								
		 <p>You have applied for $job_title position in $company_name ,</p>
		 <p> For further procedure, the following documents/certificates are requested for verification</p>";
		 $link_text = "Please click Here and upload the documents" ;

		$button_content = "Upload";
          $constructed_url = $frontend_site_url_project_main . "cjic/company/".$company_id."/job/".$job_id."/invite-code/".$invite_ref_code;
		  
        } 
			if(($jsl_classification_detail_name == "Document Checks & Verification") &&($jsl_sub_classification_detail_name == "Verify Other Documents"))	{
			
            $constructed_message_plain_text = "You have applied for $job_title position in $company_name\n\n\n For further procedure, the following documents/certificates are requested for verification \n\n\n Please upload these documents in the link below";
			$constructed_message_html = "
			<p>  You have applied for $job_title position in $company_name ,</p>
			<p> For further procedure, the following other documents/certificates are requested for verification</p>";
			$link_text = "Please click Here and upload the documents" ;

		$button_content = "Upload";
			
             $constructed_url = $frontend_site_url_project_main . "cjic/company/".$company_id."/job/".$job_id."/invite-code/".$invite_ref_code;
			} 
			if(($jsl_classification_detail_name == "Interview") &&($jsl_sub_classification_detail_name == "Telephonic Interview")){
					$constructed_message_plain_text = "As per the next steps regarding your application for $job_title with $company_name, your interview is scheduled as below";
					$constructed_message_html = "
					<p>job_title: $job_title </p>
					<p>Interview Mode : $jsl_sub_classification_detail_name </p>
                    <p>You can expect a call from or on :$company_mobile_number	</p>";
                    $link_text = "" ;

		$button_content = "Interview";
			
             $constructed_url = "#";
			}						
            if(($jsl_classification_detail_name == "Interview") && ($jsl_sub_classification_detail_name == "Online Interview")){
				$constructed_message_plain_text = "As per the next steps regarding your application for $job_title with $company_name, your interview is scheduled as below";
				$constructed_message_html = "
				<p>job_title: $job_title </p>
				<p>Interview Mode : $jsl_sub_classification_detail_name </p>
				<p>You can join the meeting at mentioned time at :</p>";

				$link_text = "" ;

		$button_content = "Interview";
			
             $constructed_url = "#";
			}
			if(($jsl_classification_detail_name == "Interview") && ($jsl_sub_classification_detail_name=="In-Person Interview")){
				$constructed_message_plain_text = "As per the next steps regarding your application for $job_title with $company_name, your interview is scheduled as below";
				$constructed_message_html = "
				<p>job_title: $job_title </p>
				<p>Interview Mode : $jsl_sub_classification_detail_name </p>
				<p>You need to be present at the mentioned location at the mentioned time : </p>";

				$link_text = "" ;

		$button_content = "Interview";
			
             $constructed_url = "#";
			}
			if(($jsl_classification_detail_name == "Interview") && ($jsl_sub_classification_detail_name=="In-Person Interview")){
				$constructed_message_plain_text = "As per the next steps regarding your application for $job_title with $company_name, your interview is scheduled as below";
				$constructed_message_html = "
				<p>job_title: $job_title </p>
				<p>Interview Mode : $jsl_sub_classification_detail_name </p>
				<p>You need to be present at the mentioned location at the mentioned time : </p>";
				$link_text = "" ;

		$button_content = "Interview";
			
             $constructed_url = "#";
			}
									
	    echo "constructed_url: " . $constructed_url . "\n\n";
								
		$job_assignment_link_without_html = $constructed_url . "\n\n";
							
		//echo "job_applicant_invite_link_without_html: " . $job_applicant_invite_link_without_html . "\n\n";
							
		$job_assignment_link_with_html = "<p><a href='" . $constructed_url . "'>" . $constructed_url . "</a><br><br></p>";
							
		//echo "account_activation_password_setup_reset_link_with_html: " . $account_activation_password_setup_reset_link_with_html . "\n\n";
							
		//Send Email

		$org_name = "" ;
		$plaintext_input = $greeting_user_summary . "
							
		" . $constructed_message_plain_text . "
							
		" .  
							

		"\n\n" .
							
		$job_assignment_link_without_html .

		"\n" .
		"Thank you \n
		
		________________________________________________________________________

		";

		/*$html_message_input = "<div><p>" . $greeting_user_summary . "<br><br>

		" . $constructed_message_html

		. $job_assignment_link_with_html .
							 
				"
							
			<p> </p>
			<p>Thank you<br>
			Admin Team <br>
			</p>
		<p>________________________________________________________________________</p>

							</div>"; */

			$html_message_input = template($greeting_user_summary ,$link_text, $constructed_url , $org_name , $button_content , $constructed_message_html);


							//echo "plaintext_input: " . $plaintext_input . "<br>";
							//echo "html_message_input: " . $html_message_input . "<br>";
							//exit;
							
							
	if (filter_var($sm_email, FILTER_VALIDATE_EMAIL) == true) {
							
	$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
						
	//send_email_using_smtp_thr_phpmailer("notifications@vdmail.securitywonks.net","Innovative Solutions", "notifications@vdmail.securitywonks.net","webmaster@securitywonks.org","Raghu sir","To check email","hello welcome","<p> hello welcome</p>");	
	//exit;
	//commented on 10-02-2019 18:10 $send_email_using_smtp_thr_phpmailer_result = send_email_using_smtp_thr_phpmailer($activation_email_sender_email, $activation_email_sender_name, $activation_email_sender_email, $recipient_email_address,null,$activation_email_subject, $plaintext_input, $html_message_input);

	/*if (($request_purpose == "activation-email") || ($request_purpose == "re-send-activation-email")) {
								
	$send_email_using_smtp_thr_phpmailer_result = send_email_using_smtp_thr_phpmailer($activation_email_sender_email, $activation_email_sender_name, $activation_email_sender_email, $recipient_email_address,null,$activation_email_subject, $plaintext_input, $html_message_input);
								
	} else if ($request_purpose == "reset-password") {
								
	$send_email_using_smtp_thr_phpmailer_result = send_email_using_smtp_thr_phpmailer($password_resetting_email_sender_email, $password_resetting_email_sender_name, $password_resetting_email_sender_email, $recipient_email_address,null,$password_resetting_email_subject, $plaintext_input, $html_message_input);
								
	}//close of else if of if (($request_purpose == "activation-email") || ($request_purpose == "re-send-activation-email")) {*/
	
	                            $to_email = array();
								$to_email[] = $sm_email;
								$email_subject = "Job Screening Level Assignment notifications";
								$from_email_address = "notifications@resumecrab.com";
								$from_person_name = "Resumecrab Notifications";
								$sender_email_address = "notifications@resumecrab.com";
								$sender_person_name = "Resumecrab Notifications";
								$replyto_email_address = "notifications@resumecrab.com";
								$replyto_person_name = "Resumecrab Notifications";
								
								$send_email_using_elastic_email_result = send_email_using_elastic_email($from_email_address,$from_person_name,$sender_email_address, $sender_person_name, $replyto_person_name,$replyto_email_address, $to_email,$email_subject, $plaintext_input, $html_message_input);
	
	
	
	
							
		/* $send_email_using_smtp_thr_phpmailer_result = send_email_using_smtp_thr_phpmailer($password_resetting_email_sender_email, $password_resetting_email_sender_name, $password_resetting_email_sender_email, $sm_email,null, "job assignment notifications", $plaintext_input, $html_message_input); 
							
			echo "after mail function"; */
			//send_email_using_smtp_thr_phpmailer($from_email_input, $sender_name_input, $reply_to_email_input, $to_email_input, $receiver_name_input, $subject_input, $plaintext_input, $html_message_input);	
						
							
							if($send_email_using_elastic_email_result == true){
								
								$new_email_sending_status = "1";
								$email_service_provider_mail_ref_message = "message-sent";
								$sent_datetime = $event_datetime;
								$sent_datetime_epoch = $current_epoch;
								
								
							} else {
								$new_email_sending_status = "4";
								$email_service_provider_mail_ref_message = null;
								$sent_datetime = null;
								$sent_datetime_epoch = null;
							}
							
							
						$jsl_assignment_update_status_sql = "UPDATE `jsl_applicant_evaluation_info` SET `candidate_notification_status`=:candidate_notification_status WHERE `jsl_applicant_evaluation_info_id`=:jsl_applicant_evaluation_info_id";
							echo "after update query";
							$jsl_assignment_update_status_query = $dbcon->prepare($jsl_assignment_update_status_sql);
							$jsl_assignment_update_status_query->bindValue(":jsl_applicant_evaluation_info_id",$jsl_applicant_evaluation_info_id);
							$jsl_assignment_update_status_query->bindValue(":candidate_notification_status","1");
							
							if($jsl_assignment_update_status_query->execute()){
								
								echo "Email Sent Successfully";
								
							} else {
								
								echo "updation not done";
								
							}	
							
							
				} else {
					echo "Recipient's Email Address is Invalid, as per Email Format Validation Checks \n\n";
					}	
			}
		}
    }	
}			
function user_details_check_based_on_user_id($jsl_applicant_evaluation_info_id) {
	global $dbcon;
	$constructed_array = array();
	$rest_user_check_sql = "SELECT * FROM `site_members` WHERE `sm_memb_id` = :sm_memb_id";
	$rest_user_check_select_query = $dbcon->prepare($rest_user_check_sql);
	$rest_user_check_select_query->bindValue(":sm_memb_id",$jsl_applicant_evaluation_info_id);				 
	$rest_user_check_select_query->execute(); 
	
	if($rest_user_check_select_query->rowCount() > 0) {
		$rest_user_check_select_query_result = $rest_user_check_select_query->fetch();
	     return $rest_user_check_select_query_result;
	
	}//close of if($rest_user_check_select_query->rowCount() > 0) {
	return $constructed_array;
	
}
function get_job_details($job_id) {
	global $dbcon, $eventLog;
	$constructed_array = array();
    
	$job_details_get_sql = "SELECT * FROM `jobs` WHERE `job_id` =:job_id";
	$job_details_select_query = $dbcon->prepare($job_details_get_sql);
    $job_details_select_query->bindValue(":job_id",$job_id);
	
      $job_details_select_query->execute();
	  
	if($job_details_select_query->rowCount() > 0) {
		$job_details_select_query_result = $job_details_select_query->fetch();
		return $job_details_select_query_result;
	
	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
	
	
}
function get_company_details($company_id) {
	global $dbcon, $eventLog;
	$constructed_array = array();
    
	$company_details_get_sql = "SELECT * FROM `companies` WHERE `company_id` =:company_id";
	$company_details_select_query = $dbcon->prepare($company_details_get_sql);
    $company_details_select_query->bindValue(":company_id",$company_id);
	
      $company_details_select_query->execute();
	  
	if($company_details_select_query->rowCount() > 0) {
		$company_details_select_query_result = $company_details_select_query->fetch();
		return $company_details_select_query_result;
	
	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
	
	
}
?>			 
								
							