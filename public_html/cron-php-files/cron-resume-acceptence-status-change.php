<?php
if(defined('STDIN') ){
  //echo("Running from CLI");
}else{
  echo("Not Running from CLI");
  exit();
}

ini_set('memory_limit', '-1');
//PCRE Backtrack Limit is increased from 1000000 to 4000000, to handle supporting, bigger images in PDF Files using PDF to Text Library
ini_set('pcre.backtrack_limit', 4000000);
//if the directory is one directory above the current file's parent directory http://stackoverflow.com/a/2100763/811207
include(dirname(dirname(dirname(__FILE__))) . "/app/core/command-line-include.php");
echo "before cron job details query\n";
$cron_number = "13";
$sel_cron_number_setting = $dbcon->prepare("SELECT * FROM `cron_file_details` WHERE `sno` = :sno");
$sel_cron_number_setting->bindParam(":sno",$cron_number);
$sel_cron_number_setting->execute(); 
$res_cron_number_setting = $sel_cron_number_setting->fetchAll(PDO::FETCH_ASSOC);
print_r($res_cron_number_setting);
//exit;
foreach($res_cron_number_setting as $res_cron_number_setting_row)
{
	$res_state = $res_cron_number_setting_row["cron_file_status_setting"];
	$res_records = $res_cron_number_setting_row["cron_file_numb_record_limit"];
	$res_loop = $res_cron_number_setting_row["cron_file_numb_loop_count_limit"];
	$res_sleep_min = $res_cron_number_setting_row["cron_file_sleep_min_seconds_limit"];
	$res_sleep_max = $res_cron_number_setting_row["cron_file_sleep_max_seconds_limit"];
	$res_sleep_interval = $res_cron_number_setting_row["cron_file_sleep_interval"];
	
}


if ( $res_state == "ON" ) {

	//echo "inside on condition \n";		
	for ( $i = 1; $i <=$res_loop; $i++ ) {
		echo "entered into loop\n";

		
			$job_details_select_sql = "SELECT * FROM `jobs` WHERE `resume_acceptance_status` = :resume_acceptance_status LIMIT :res_records";
			
			$resume_acceptance_status = "1";
			$job_details_select_query = $dbcon->prepare($job_details_select_sql);
			$job_details_select_query->bindValue(":resume_acceptance_status",$resume_acceptance_status);
			$job_details_select_query->bindValue(":res_records",$res_records);
			$job_details_select_query->execute(); 
			
			if($job_details_select_query->rowCount() > 0) {
				//echo "after select query row count \n";
				$job_details_select_query_result = $job_details_select_query->fetchAll();
				//echo "after fetch";
				foreach ($job_details_select_query_result as $job_details_select_query_result_row) {
				   echo "before condition";
				   echo $current_epoch;
					$job_id_input = $job_details_select_query_result_row["job_id"];
					$resume_submission_end_date_epoch_input = $job_details_select_query_result_row["resume_submission_end_date_epoch"];
					$resume_acceptance_status_input = $job_details_select_query_result_row["resume_acceptance_status"];
					 
	                    $resume_acceptance_status_input = "0";
						if($current_epoch > $resume_submission_end_date_epoch_input){
							$resume_acceptance_status_update_sql = "UPDATE `jobs` SET `resume_acceptance_status` =:resume_acceptance_status WHERE `job_id`= :job_id";
							echo "after update query";
						    $resume_acceptance_status_update_select_query = $dbcon->prepare($resume_acceptance_status_update_sql);
						    $resume_acceptance_status_update_select_query->bindValue(":job_id",$job_id_input);
					        $resume_acceptance_status_update_select_query->bindValue(":resume_acceptance_status",0);
							
						    if($resume_acceptance_status_update_select_query->execute()){
								return true;
							} else{
								return false;
							}
								
						} 
				   
				}	
						
			} 
					
		}
	} 
		
?>