<?php
if(defined('STDIN') ){
  //echo("Running from CLI");
}else{
  echo("Not Running from CLI");
  exit();
}

ini_set('memory_limit', '-1');
//PCRE Backtrack Limit is increased from 1000000 to 4000000, to handle supporting, bigger images in PDF Files using PDF to Text Library
ini_set('pcre.backtrack_limit', 4000000);
//if the directory is one directory above the current file's parent directory http://stackoverflow.com/a/2100763/811207
include(dirname(dirname(dirname(__FILE__))) . "/app/core/command-line-include.php");

$cron_number = "10";
$sel_filem_setting = $dbcon->prepare("SELECT * FROM `cron_file_details` WHERE `sno` = :sno");
$sel_filem_setting->bindParam(":sno",$cron_number);
$sel_filem_setting->execute(); 
$res_filem_setting = $sel_filem_setting->fetchAll(PDO::FETCH_ASSOC);
//print_r($res_filem_setting);
//exit;
foreach($res_filem_setting as $res_filem_setting_row)
{
	$res_state_filem = $res_filem_setting_row["cron_file_status_setting"];
	$res_records_filem = $res_filem_setting_row["cron_file_numb_record_limit"];
	$res_loop_filem = $res_filem_setting_row["cron_file_numb_loop_count_limit"];
	$res_sleep_min_filem = $res_filem_setting_row["cron_file_sleep_min_seconds_limit"];
	$res_sleep_max_filem = $res_filem_setting_row["cron_file_sleep_max_seconds_limit"];
	$res_sleep_interval_filem = $res_filem_setting_row["cron_file_sleep_interval"];
	
}




if ( $res_state_filem == "ON" ) {

			
	for ( $filem = 1; $filem <=$res_loop_filem; $filem++ ) {

function output_collected ($message) {
	if  (php_sapi_name( )  ==  'cli') {
		return $message;
	} else {
	    return nl2br($message);
	}	
}
		//code related to activity in particular cron file start
			$candidate_quick_onboarding_data_details = get_candidate_quick_onboarding_data_based_on_is_processed_status_input("1", "1"); //is_processed value input:- 0: not-confirmed, 1: confirmed, 2: processed, 3: on hold, 4: finalized
			//echo "<pre>";
			//print_r($candidate_quick_onboarding_data_details);
			//exit;
			if ($candidate_quick_onboarding_data_details != "") {
				//When Data exists
				foreach ($candidate_quick_onboarding_data_details as $candidate_quick_onboarding_data_detail_row) {
					
				
					$confirmed_id = $candidate_quick_onboarding_data_detail_row["id"];
					$confirmed_profile_source_id = $candidate_quick_onboarding_data_detail_row["profile_source_id"];
					$confirmed_profile_source_name = $candidate_quick_onboarding_data_detail_row["profile_source_name"];
					$confirmed_email = $candidate_quick_onboarding_data_detail_row["email"];
					$confirmed_candidate_id = $candidate_quick_onboarding_data_detail_row["candidate_id"];
					$confirmed_member_type = $candidate_quick_onboarding_data_detail_row["member_type"];
					$confirmed_candidate_firstname = $candidate_quick_onboarding_data_detail_row["candidate_firstname"];
					$confirmed_candidate_lastname = $candidate_quick_onboarding_data_detail_row["candidate_lastname"];
					$confirmed_departments = $candidate_quick_onboarding_data_detail_row["departments"];
					$confirmed_experience_level = $candidate_quick_onboarding_data_detail_row["experience_level"];
					$confirmed_skillsets = $candidate_quick_onboarding_data_detail_row["skillsets"];
					$confirmed_mobile_number = $candidate_quick_onboarding_data_detail_row["mobile_number"];
					$confirmed_city = $candidate_quick_onboarding_data_detail_row["city"];
					$confirmed_country = $candidate_quick_onboarding_data_detail_row["country"];
					
					$confirmed_country_exploded = explode(":::::", $confirmed_country);
					$confirmed_country_exploded_count = count($confirmed_country_exploded);
					//echo "confirmed_country_exploded_count: " . $confirmed_country_exploded_count . "\n";
					//exit;
					if ($confirmed_country_exploded_count == "3") {
						$confirmed_country_id = $confirmed_country_exploded[0];
						$confirmed_country_name = $confirmed_country_exploded[1];
						$confirmed_country_two_lettered_code = $confirmed_country_exploded[2];
					} else {
						$confirmed_country_id = "";
						$confirmed_country_name = "";
						$confirmed_country_two_lettered_code = "";
					}//close of else of if ($confirmed_country_exploded_count == "3") {
					echo "confirmed_country_id: " . $confirmed_country_id . "\n";	
					echo "confirmed_country_name: " . $confirmed_country_name . "\n";	
					echo "confirmed_country_two_lettered_code: " . $confirmed_country_two_lettered_code . "\n";	
					//exit;	
					
					$confirmed_notes = $candidate_quick_onboarding_data_detail_row["notes"];
					$confirmed_is_ama_invitation_required = $candidate_quick_onboarding_data_detail_row["is_ama_invitation_required"];
					
					$confirmed_added_by_admin_user_id = $candidate_quick_onboarding_data_detail_row["added_by_admin_user_id"];
					$confirmed_added_by_admin_user_details = get_admin_user_details_admin_user_id_input($confirmed_added_by_admin_user_id);
					
					if ($confirmed_added_by_admin_user_details != "") {
						$confirmed_added_by_admin_user_firstname = $confirmed_added_by_admin_user_details["sm_firstname"];
						$confirmed_added_by_admin_user_lastname = $confirmed_added_by_admin_user_details["sm_lastname"];
					} else {
						$confirmed_added_by_admin_user_firstname = "";
						$confirmed_added_by_admin_user_lastname = "";
					}//close of else of if ($confirmed_added_by_admin_user_details != "") {
					
					echo "confirmed_candidate_firstname: " . $confirmed_candidate_firstname . "\n";
					echo "confirmed_candidate_lastname: " . $confirmed_candidate_lastname . "\n";
					
					echo "confirmed_added_by_admin_user_firstname: " . $confirmed_added_by_admin_user_firstname . "\n";
					echo "confirmed_added_by_admin_user_lastname: " . $confirmed_added_by_admin_user_lastname . "\n";
					//exit;
					$confirmed_data_source = $candidate_quick_onboarding_data_detail_row["data_source"];
					$confirmed_candidate_data_excel_file_id = $candidate_quick_onboarding_data_detail_row["candidate_data_excel_file_id"];
					
					$confirmed_is_processed = $candidate_quick_onboarding_data_detail_row["is_processed"];
					$confirmed_is_active_status = $candidate_quick_onboarding_data_detail_row["is_active_status"];
					
					
					if ($confirmed_departments != "") {
						$confirmed_departments_json_decoded = json_decode($confirmed_departments, true);
						$confirmed_departments_json_decoded_count = count($confirmed_departments_json_decoded);
			
						if($confirmed_departments_json_decoded_count > 0){
							//echo "<pre>";
							//print_r($confirmed_departments_json_decoded);
							//exit;
							$department_list_details = get_departments_list_from_department_status_input("1");
							$constructed_departments_array = array();
							foreach ($department_list_details as $department_list_detail_row) {
								$departments_list_department_id = $department_list_detail_row["department_id"];
								$departments_list_department_name = $department_list_detail_row["department_name"];
								$departments_temp = array();
								$departments_temp["department_id"] = $departments_list_department_id;
								$departments_temp["department_name"] = $departments_list_department_name;
								$departments_temp["checked"] = "0";
								array_push($constructed_departments_array, $departments_temp);
								
							}//close of foreach ($department_list_details as $department_list_detail_row) {
								
							foreach ($confirmed_departments_json_decoded as $department_row) {
								$department_id = $department_row;
								foreach ($constructed_departments_array as $constructed_departments_array_key => $constructed_departments_array_value) {
									$constructed_department_id = $constructed_departments_array_value["department_id"];
									if ($department_id == $constructed_department_id) {
										$constructed_departments_array[$constructed_departments_array_key]["checked"] = "1";
										
									}//close of if ($department_id == $departments_list_department_id) {
								}//close of foreach ($constructed_departments_array as $constructed_departments_array_key => $constructed_departments_array_value) {
								
							}//close of foreach ($confirmed_departments_json_decoded as $department_row) {
							//echo "<pre>";
							//print_r($constructed_departments_array);
							$constructed_departments_array_names = array();
							foreach ($constructed_departments_array as $constructed_departments_array_row) {
								$constructed_departments_department_id = $constructed_departments_array_row["department_id"];
								$constructed_departments_department_name = $constructed_departments_array_row["department_name"];
								$constructed_departments_checked = $constructed_departments_array_row["checked"];
								
								if ($constructed_departments_checked == "1") {
									$constructed_departments_array_names[] = $constructed_departments_department_name;
								}//close of if ($constructed_departments_checked == "1") {
								
							}//close of foreach ($constructed_departments_array as $constructed_departments_array_row) {
							//echo "<pre>";
							//print_r($constructed_departments_array_names);
							$department_names_comma_separated = implode(", ", $constructed_departments_array_names);
							//echo "department_names_comma_separated: " . $department_names_comma_separated . "<br>\n";
							
							
						} else {
							$department_names_comma_separated = "";
						}//Close of else of if($confirmed_departments_json_decoded_count > 0){
						
					} else {
						$department_names_comma_separated = "";
					}//close of else if ($confirmed_departments != "") {
					
					if ($confirmed_skillsets != "") {
							
						$confirmed_skillsets_json_decoded = json_decode($confirmed_skillsets, true);
						$confirmed_skillsets_json_decoded_count = count($confirmed_skillsets_json_decoded);
			
						if($confirmed_skillsets_json_decoded_count > 0){
							
							$skillset_list_details = get_skillsets_list_from_skillset_status_input("1");
							$constructed_skillsets_array = array();
							foreach ($skillset_list_details as $skillset_list_detail_row) {
								$skillsets_list_skillset_id = $skillset_list_detail_row["skillset_id"];
								$skillsets_list_skillset_name = $skillset_list_detail_row["skillset_name"];
								$skillsets_list_skillset_seo_name = $skillset_list_detail_row["skillset_seo_name"];
								$skillsets_temp = array();
								$skillsets_temp["skillset_id"] = $skillsets_list_skillset_id;
								$skillsets_temp["skillset_name"] = $skillsets_list_skillset_name;
								$skillsets_temp["skillset_seo_name"] = $skillsets_list_skillset_seo_name;
								$skillsets_temp["selected"] = "0";
								array_push($constructed_skillsets_array, $skillsets_temp);
								
							}//close of foreach ($skillset_list_details as $skillset_list_detail_row) {
							foreach ($confirmed_skillsets_json_decoded as $skillset_row) {
								$skillset_id = $skillset_row;
								foreach ($constructed_skillsets_array as $constructed_skillsets_array_key => $constructed_skillsets_array_value) {
									$constructed_skillsets_skillset_id = $constructed_skillsets_array_value["skillset_id"];
									$constructed_skillsets_skillset_name = $constructed_skillsets_array_value["skillset_name"];
									$constructed_skillsets_skillset_seo_name = $constructed_skillsets_array_value["skillset_seo_name"];
									
									$constructed_skillsets_option_value = $constructed_skillsets_skillset_id . ":::::" . $constructed_skillsets_skillset_name . ":::::" . $constructed_skillsets_skillset_seo_name;
									
									if ($skillset_id == $constructed_skillsets_option_value) {
										$constructed_skillsets_array[$constructed_skillsets_array_key]["selected"] = "1";
										
									}//close of if ($skillset_id == $constructed_skillset_id) {
								}//close of foreach ($constructed_skillsets_array as $constructed_skillsets_array_key => $constructed_skillsets_array_value) {
								
							}//close of foreach ($confirmed_skillsets_json_decoded as $skillset_row) {	
							
							//echo "<pre>";
							//print_r($constructed_skillsets_array);
							$constructed_skillsets_array_names = array();
							foreach ($constructed_skillsets_array as $constructed_skillsets_array_row) {
								$constructed_skillsets_skillset_id = $constructed_skillsets_array_row["skillset_id"];
								$constructed_skillsets_skillset_name = $constructed_skillsets_array_row["skillset_name"];
								$constructed_skillsets_skillset_seo_name = $constructed_skillsets_array_row["skillset_seo_name"];
								$constructed_skillsets_selected = $constructed_skillsets_array_row["selected"];
								
								$skillsets_option_value = $constructed_skillsets_skillset_id . ":::::" . $constructed_skillsets_skillset_name . ":::::" . $constructed_skillsets_skillset_seo_name;
								
								if ($constructed_skillsets_selected == "1") {
									$constructed_skillsets_array_names[] = $constructed_skillsets_skillset_name;
								}//close of if ($constructed_skillsets_selected == "1") {
							
							}//close of foreach ($constructed_skillsets_array as $constructed_skillsets_array_row) {
								
							//echo "<pre>";
							//print_r($constructed_skillsets_array_names);
							$skillset_names_comma_separated = implode(", ", $constructed_skillsets_array_names);
							//echo "skillset_names_comma_separated: " . $skillset_names_comma_separated . "<br>\n";
								
							
						} else {
							$skillset_names_comma_separated = "";
						}//close of else of if($confirmed_skillsets_json_decoded_count > 0){
						
						
					} else {
						$skillset_names_comma_separated = "";
					}//close of else of if ($confirmed_skillsets != "") {
					
					echo "before email empty check \n";
					//exit;
					$candidate_profile_confirmation_processing_status = "DONOT-PROCESS";
					
					/*//commented on 06-03-2018 by raghu, when is_processed is proposed to be 4
					if (($confirmed_is_active_status == "1") && ($confirmed_is_processed == "0") && ($confirmed_email != "")) {*/
					if (($confirmed_is_active_status == "1") && ($confirmed_is_processed == "1") && ($confirmed_email != "")) {
						echo "Email input is not empty, and this is not confirmed yet\n";
						
						//if (($confirmed_profile_source_id != "") && ($confirmed_profile_source_name != "") && ($confirmed_member_type != "") && ($confirmed_candidate_firstname != "") && ($confirmed_candidate_lastname != "") && ($confirmed_departments != "") && ($confirmed_experience_level != "") && ($confirmed_skillsets != "") && ($confirmed_mobile_number != "") && ($confirmed_city != "") && ($confirmed_country != "") &&  ($confirmed_is_ama_invitation_required != "")) {
						if (($confirmed_profile_source_id != "") && ($confirmed_profile_source_name != "") && ($confirmed_member_type != "") && ($confirmed_candidate_firstname != "") && ($confirmed_departments != "") && ($confirmed_experience_level != "") && ($confirmed_skillsets != "") && ($confirmed_mobile_number != "") && ($confirmed_city != "") && ($confirmed_country_name != "") &&  ($confirmed_is_ama_invitation_required != "")) {	
							echo "required inputs are available\n";
							//exit;
							//Check if this Email Address is used anywhere else
							$confirmed_email_details = check_collected_email($confirmed_email);
							if ($confirmed_email_details == "") {
								//New Email Address, as per system
								//echo "New Email Address, as per system\n<br>";
								$file_extension = "pdf";
								//Create an internal File name (for the PDF File, that will be created, to hold the generate resume of the particular candidate profile)
								$generated_file_name = hash('sha256', uniqid(rand(), TRUE));
								$generated_file_name = $generated_file_name . "." . $file_extension;
								//echo "generated_file_name: " . $generated_file_name . "\n<br>";
								
								//Resume Saving Path and Filename
								$targetFile =  $targetPath. $generated_file_name;  //from /app/core/main-config.php
								echo "targetFile: " . $targetFile . "\n";
								//exit;
								//Save the generated content of the Resume of particular candidate profile
								/*$pdf = new FPDF();
								$pdf->AddPage();
								$pdf->SetFont('Arial','B',16);
								$pdf->Cell(40,10,$pdf_content);
								$content = $pdf->Output($targetFile,'F');*/
														
								// Begin configuration
								//http://www.elated.com/articles/create-nice-looking-pdfs-php-fpdf/
								$textColour = array( 0, 0, 0 );
								$headerColour = array( 100, 100, 100 );
								$tableHeaderTopTextColour = array( 255, 255, 255 );
								$tableHeaderTopFillColour = array( 125, 152, 179 );
								$tableHeaderTopProductTextColour = array( 0, 0, 0 );
								$tableHeaderTopProductFillColour = array( 143, 173, 204 );
								$tableHeaderLeftTextColour = array( 99, 42, 57 );
								$tableHeaderLeftFillColour = array( 184, 207, 229 );
								$tableBorderColour = array( 50, 50, 50 );
								$tableRowFillColour = array( 213, 170, 170 );
								$reportName = $confirmed_candidate_firstname . " " . $confirmed_candidate_lastname . " Resume";
								$reportNameYPos = 160;
								
								$columnLabels = array( "Q1", "Q2", "Q3", "Q4" );
								$rowLabels = array( "SupaWidget", "WonderWidget", "MegaWidget", "HyperWidget" );
								
								// End configuration
			
								//Construct the Content for the PDF File
								$pdf = new FPDF();
								$pdf->AddPage();
								$pdf->SetTextColor( $headerColour[0], $headerColour[1], $headerColour[2] );
								$pdf->SetFont( 'Arial', '', 17 );
								$pdf->Cell( 0, 15, $reportName, 0, 0, 'C' );
								$pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
								//$pdf->SetFont( 'Arial', '', 20 );
								//$pdf->Write( 19, $confirmed_candidate_firstname . " " . $confirmed_candidate_lastname );
								$pdf->Ln( 20 );
								$pdf->SetFont( 'Arial', 'B', 12 );
								$pdf->Write( 6, "Profile Source Name:" );
								$pdf->Ln( 9 );
								$pdf->SetFont( 'Arial', '', 12 );
								$pdf->Write( 6, $confirmed_profile_source_name );
								$pdf->Ln( 9 );
								$pdf->SetFont( 'Arial', 'B', 12 );
								$pdf->Write( 6, "Email:" );
								$pdf->Ln( 9 );
								$pdf->SetFont( 'Arial', '', 12 );
								$pdf->Write( 6, $confirmed_email );
								$pdf->Ln( 9 );
								$pdf->SetFont( 'Arial', 'B', 12 );
								$pdf->Write( 6, "Member Type:" );
								$pdf->Ln( 9 );
								$pdf->SetFont( 'Arial', '', 12 );
								$pdf->Write( 6, $confirmed_member_type );
								$pdf->Ln( 9 );
								
								$pdf->SetFont( 'Arial', 'B', 12 );
								$pdf->Write( 6, "Departments:" );
								$pdf->Ln( 9 );
								$pdf->SetFont( 'Arial', '', 12 );
								$pdf->Write( 6, $department_names_comma_separated );
								$pdf->Ln( 9 );
								
								$pdf->SetFont( 'Arial', 'B', 12 );
								$pdf->Write( 6, "Experience Level:" );
								$pdf->Ln( 9 );
								$pdf->SetFont( 'Arial', '', 12 );
								$pdf->Write( 6, $confirmed_experience_level );
								$pdf->Ln( 9 );
								
								$pdf->SetFont( 'Arial', 'B', 12 );
								$pdf->Write( 6, "Skillsets:" );
								$pdf->Ln( 9 );
								$pdf->SetFont( 'Arial', '', 12 );
								$pdf->Write( 6, $skillset_names_comma_separated );
								$pdf->Ln( 9 );
								
								$pdf->SetFont( 'Arial', 'B', 12 );
								$pdf->Write( 6, "Mobile Number:" );
								$pdf->Ln( 9 );
								$pdf->SetFont( 'Arial', '', 12 );
								$pdf->Write( 6, $confirmed_mobile_number );
								$pdf->Ln( 9 );
								
								$pdf->SetFont( 'Arial', 'B', 12 );
								$pdf->Write( 6, "City:" );
								$pdf->Ln( 9 );
								$pdf->SetFont( 'Arial', '', 12 );
								$pdf->Write( 6, $confirmed_city );
								$pdf->Ln( 9 );
								
								$pdf->SetFont( 'Arial', 'B', 12 );
								$pdf->Write( 6, "Country:" );
								$pdf->Ln( 9 );
								$pdf->SetFont( 'Arial', '', 12 );
								$pdf->Write( 6, $confirmed_country_name );
								$pdf->Ln( 9 );
								
								$pdf->SetFont( 'Arial', 'B', 12 );
								$pdf->Write( 6, "Notes:" );
								$pdf->Ln( 9 );
								$pdf->SetFont( 'Arial', '', 12 );
								$pdf->Write( 6, $confirmed_notes );
								$pdf->Ln( 15 );
								$pdf->SetFont( 'Arial', 'I', 10 );
								$pdf->Write( 6, "This is a Dynamically created Resume, using internal Data for internal reference only @Inademy." );
								$content = $pdf->Output($targetFile,'F');


								//exit;
								//Uploaded Time, as per Indian Standard Time
								$uploaded_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Asia/Kolkata");
			
								//Do Insert Query in to Resumes DB Table
								
								//SQL Queries
								$resume_uploaded_info_sql = "INSERT INTO `resumes`(`resume_original_file_name`, `resume_generated_file_name`, `resume_document_extension`, `profile_source_id`, `profile_source_name`, `added_by_admin_user_id`, `uploaded_datetime`, `uploaded_datetime_epoch`) VALUES (:resume_original_file_name, :resume_generated_file_name, :resume_document_extension, :profile_source_id, :profile_source_name, :added_by_admin_user_id, :uploaded_datetime, :uploaded_datetime_epoch)";
								
								//array of values for inserting data in to resumes db table
								//$resume_uploaded_info_values_array = array($filename,$generated_file_name,$file_extension,$profile_source,$profile_source_name,$uploaded_datetime,$current_epoch);
								$resume_uploaded_info_values_array = array(":resume_original_file_name" => $generated_file_name, ":resume_generated_file_name" => $generated_file_name, ":resume_document_extension" => $file_extension, ":profile_source_id" => $confirmed_profile_source_id, ":profile_source_name" => $confirmed_profile_source_name, ":added_by_admin_user_id" => $confirmed_added_by_admin_user_id, ":uploaded_datetime" => $uploaded_datetime, ":uploaded_datetime_epoch" => $current_epoch);
								$last_inserted_resume_id = insert_query_get_last_insert_id($resume_uploaded_info_sql, $resume_uploaded_info_values_array);
								
								//Resume Finalized Admin User ID
								//$finalized_by_admin_user_id = $_SESSION["sm_memb_id"]; //applicable in admin panel
								$finalized_by_admin_user_id = $confirmed_added_by_admin_user_id; //applicable in cron job, wherein, the admin user id, of user, who added this will be noted for finalization as well
								
								
								//SQL Queries
								$resume_extracted_info_sql = "INSERT INTO `extracted_info`(`resume_id`, `profile_source_id`, `collected_email_address`, `firstname`, `lastname`, `finalized_by_admin_user_id`) VALUES (:resume_id,:profile_source_id,:collected_email_address,:firstname,:lastname,:finalized_by_admin_user_id)";
								
								//array of values for inserting data in to extracted_info db table
								$resume_extracted_info_values_array = array(":resume_id" => $last_inserted_resume_id, ":profile_source_id" => $confirmed_profile_source_id, ":collected_email_address" => $confirmed_email, ":firstname" => $confirmed_candidate_firstname, ":lastname" => $confirmed_candidate_lastname, ":finalized_by_admin_user_id" => $finalized_by_admin_user_id);
								$last_resume_extract_info_insert_id = insert_query_get_last_insert_id($resume_extracted_info_sql, $resume_extracted_info_values_array);
								
								//echo "last_resume_extract_info_insert_id: " . $last_resume_extract_info_insert_id . "\n<br>";
								//exit;
								if (isset($last_resume_extract_info_insert_id) && ($last_resume_extract_info_insert_id != "")) {
									
									
									//Do Update Query
								
									$update_resume_status_sql = "UPDATE `resumes` SET `last_attempted_datetime`=:last_attempted_datetime,`last_attempted_datetime_epoch`=:last_attempted_datetime_epoch,`is_active_status`=:is_active_status WHERE `resume_id`=:resume_id";
									
									//array of values for updating resumes db table
									$update_resume_status_values_array = array(":last_attempted_datetime" => $uploaded_datetime, ":last_attempted_datetime_epoch" => $current_epoch, ":is_active_status" => "1", ":resume_id" => $last_inserted_resume_id);
									
									$update_status = update_query_based_on_id($update_resume_status_sql, $update_resume_status_values_array);
								}//close of if (isset($last_resume_extract_info_insert_id) && ($last_resume_extract_info_insert_id != "")) {
									
								//Do, all inserts in different db tables, to mimic the new resume processing scenario.
								$candidate_profile_confirmation_processing_status = "PROCESS-FINALIZE";
								//exit;
							} else {
								//This Email Address is observed somewhere else in the system
								echo "This email exists in the system, but need further introspection\n";
								//Check if this email address is in extracted_info db table
								$candidate_extracted_info_details = get_extracted_info_from_extracted_info_based_on_email_address_input($confirmed_email);
								if ($candidate_extracted_info_details != "") {
									//This Email Address has a record in extracted_info db table
									//echo "This email exists in the system, and related info is extracted from the system\n<br>";
									$specific_extracted_info_id = $candidate_extracted_info_details["extracted_info_id"];
									$specific_resume_id = $candidate_extracted_info_details["resume_id"];
									$specific_profile_source_id = $candidate_extracted_info_details["profile_source_id"];
									$specific_collected_email_address = $candidate_extracted_info_details["collected_email_address"];
									$specific_firstname = $candidate_extracted_info_details["firstname"];
									$specific_lastname = $candidate_extracted_info_details["lastname"];
									$specific_is_finalized = $candidate_extracted_info_details["is_finalized"];
									$specific_finalized_datetime = $candidate_extracted_info_details["finalized_datetime"];
									$specific_finalized_datetime_epoch = $candidate_extracted_info_details["finalized_datetime_epoch"];
									//echo "specific_is_finalized: " . $specific_is_finalized . "<br>";
									//CASE 2: is_finalized = 0
									if ($specific_is_finalized == "0") {
										//Do, all inserts in different db tables, to mimic the new resume processing scenario.
										$candidate_profile_confirmation_processing_status = "PROCESS-FINALIZE";
										//echo "CASE 2: the candidate profile that is being confirmed, is not yet finalized, even though the resume is processed and email account is extracted and registered as reference.\n<br>";
									}//close of if ($specific_is_finalized == "0") {
										
									$extracted_info_candidate_details = get_candidate_details_extracted_info_id($specific_extracted_info_id);
									
									if ($extracted_info_candidate_details != "") {
										$extracted_info_candidate_id = $extracted_info_candidate_details["candidate_id"];
										
										$extracted_info_candidate_email_details = get_candidate_email_details($extracted_info_candidate_id, $confirmed_email);
										
										$extracted_info_email_id_activation_status = $extracted_info_candidate_email_details["email_id_activation_status"];
										
										//Update posting Time, as per Indian Standard Time
										$update_posting_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Asia/Kolkata");
										//CASE 3: case 3: entry exists, and is_finalized = 1, resume, email address is known and the entry is finalized, which means, required info is known.
										//If is_finalized = 1 (from extracted_info) and email_id_activation_status = 0
										if (($specific_is_finalized == "1") && ($extracted_info_email_id_activation_status == "0")) {
											//$candidate_profile_confirmation_processing_status = "COMPARE-DATA-TWO-SOURCES";
											//echo "CASE 3: The Candidate Profile that is being confirmed, is already finalized and is awaiting email activation\n<br>";
											echo "The Candidate Profile that is being confirmed, is already finalized, and is awaiting at the email activation step, of the Candidate On-boarding Process. \n";
											//DO UPDATE QUERY TO ONBOARDING DATA DB TABLE with is_processed = 3
											update_processing_status_based_on_candidate_quick_onboarding_data_id_input($confirmed_id, "3", $update_posting_datetime, $current_epoch, null);
										} else if (($specific_is_finalized == "1") && ($extracted_info_email_id_activation_status == "1")) {
											//echo "CASE 4: Candidate Profile with this Email Account is already activated\n<br>";
											//DO UPDATE QUERY TO ONBOARDING DATA DB TABLE, with is_processed = 2
											echo "The Candidate Profile with this Email Account is already activated!!! \n";
											update_processing_status_based_on_candidate_quick_onboarding_data_id_input($confirmed_id, "2", $update_posting_datetime, $current_epoch, null);
										}//close of else if of if (($specific_is_finalized == "1") && ($extracted_info_email_id_activation_status == "0")) {
										
										
										
										
										
									}/* else {
										echo "There is an error in obtaining the candidate id of matching record. \n";
									}*/
									//DO QUERY TO CANDIDATES DB TABLE based on EXTRACTED_INFO_ID, to get CANDIDATE_ID and then QUERY CANDIDATE_EMAILS DB TABLE to SEE THE EMAIL ID ACTIVATION STATUS of THE PARTICULAR EMAIL ADDRESS, and USE THE STATUS IN THE CONDITIONS
									
									//Check if the particular Candidate Profile with this specific Email Address, is Finalized?
									/*if ($specific_is_finalized == "0") {
										//A Resume exists, which has this Email Address, as linked to this candidate profile, is yet to be finalized. Do show the comparision of data between the manually entered data and the data
										
									} else if ($specific_is_finalized == "1") {
										//A Resume exists, which has this Email Address, as linked to this candidate profile, is already finalized. Do update the record with the candidate_id and then MARK this as Processed.
										
									}*/
								} else {
									echo "This Email Address is already linked to a non-candidate profile (Company) in the system.\n";
								}//close of if ($candidate_extracted_info_details != "") {
									
							}//close of else of if ($confirmed_email_details == "") {
							
						} else {
							//There are some missing fields
							echo "Please use EDIT option to complete all inputs, before Confirming the Candidate Profile.\n";
							
						}//close of else of if (($confirmed_profile_source_id != "") && ($confirmed_profile_source_name != "") && ($confirmed_member_type != "") && ($confirmed_candidate_firstname != "") && ($confirmed_departments != "") && ($confirmed_experience_level != "") && ($confirmed_skillsets != "") && ($confirmed_mobile_number != "") && ($confirmed_city != "") && ($confirmed_country_name != "") &&  ($confirmed_is_ama_invitation_required != "")) {	
						//}//close of else of if (($confirmed_profile_source_id != "") && ($confirmed_profile_source_name != "") && ($confirmed_member_type != "") && ($confirmed_candidate_firstname != "") && ($confirmed_candidate_lastname != "") && ($confirmed_departments != "") && ($confirmed_experience_level != "") && ($confirmed_skillsets != "") && ($confirmed_mobile_number != "") && ($confirmed_city != "") && ($confirmed_country != "") && ($confirmed_is_ama_invitation_required != "")) {
						
						
						
						
						
					} else {
						echo "This cannot be confirmed, as the particular Candidate Profile is in-active / confirmed / has invalid email address.\n";
					}//close of else of if (($confirmed_is_active_status == "1") && ($confirmed_is_processed == "0") && ($confirmed_email != "")) {
					

					if ($candidate_profile_confirmation_processing_status == "PROCESS-FINALIZE") {
						//echo "confirmed_candidate_email: " . $confirmed_email . "<br>";
						
						$candidate_extracted_info_details = get_extracted_info_from_extracted_info_based_on_email_address_input($confirmed_email);
						
						if ($candidate_extracted_info_details != "") {
							//This Email Address has a record in extracted_info db table
							//echo "This email exists in the system, and related info is extracted from the system\n<br>";
							$specific_extracted_info_id = $candidate_extracted_info_details["extracted_info_id"];
							$specific_resume_id = $candidate_extracted_info_details["resume_id"];
							$specific_profile_source_id = $candidate_extracted_info_details["profile_source_id"];
							$specific_collected_email_address = $candidate_extracted_info_details["collected_email_address"];
							$specific_firstname = $candidate_extracted_info_details["firstname"];
							$specific_lastname = $candidate_extracted_info_details["lastname"];
							$specific_is_finalized = $candidate_extracted_info_details["is_finalized"];
							
							$specific_finalized_by_admin_user_id = $candidate_extracted_info_details["finalized_by_admin_user_id"];
							
							$specific_finalized_by_admin_user_details = get_admin_user_details_admin_user_id_input($specific_finalized_by_admin_user_id);
							
							if ($specific_finalized_by_admin_user_details != "") {
								$specific_finalized_by_admin_user_firstname = $specific_finalized_by_admin_user_details["sm_firstname"];
								$specific_finalized_by_admin_user_lastname = $specific_finalized_by_admin_user_details["sm_lastname"];
							} else {
								$specific_finalized_by_admin_user_firstname = "";
								$specific_finalized_by_admin_user_lastname = "";
							}//close of else of if ($specific_finalized_by_admin_user_details != "") {
								
							
			
							$specific_finalized_datetime = $candidate_extracted_info_details["finalized_datetime"];
							$specific_finalized_datetime_epoch = $candidate_extracted_info_details["finalized_datetime_epoch"];
							
							//Other Required info
							$onboarded_by_user_type = "admin";
							
							if ($specific_is_finalized == "0") {
								//Do Insert Query into Candidates db table
										
								//SQL Queries
								$candidate_sql = "INSERT INTO `candidates`(`extracted_info_id`, `resume_id`, `profile_source_id`, `candidate_firstname`, `candidate_lastname`, `city`, `country`, `mobile_number`, `sub_classification`, `onboarded_by_user_type`, `added_by_admin_user_id`, `added_by_admin_user_firstname`, `added_by_admin_user_lastname`, `finalized_by_admin_user_id`, `finalized_by_admin_user_firstname`, `finalized_by_admin_user_lastname`) VALUES (:extracted_info_id,:resume_id,:profile_source_id,:candidate_firstname,:candidate_lastname,:city,:country,:mobile_number,:sub_classification,:onboarded_by_user_type,:added_by_admin_user_id,:added_by_admin_user_firstname,:added_by_admin_user_lastname,:finalized_by_admin_user_id,:finalized_by_admin_user_firstname,:finalized_by_admin_user_lastname)";
								
								$candidate_values_array = array(":extracted_info_id" => $specific_extracted_info_id, ":resume_id" => $specific_resume_id, ":profile_source_id" => $specific_profile_source_id, ":candidate_firstname" => $confirmed_candidate_firstname, ":candidate_lastname" => $confirmed_candidate_lastname, ":city" => $confirmed_city, ":country" => $confirmed_country_name, ":mobile_number" => $confirmed_mobile_number, ":sub_classification" => $confirmed_experience_level, ":onboarded_by_user_type" => $onboarded_by_user_type, ":added_by_admin_user_id" => $confirmed_added_by_admin_user_id, ":added_by_admin_user_firstname" => $confirmed_added_by_admin_user_firstname, ":added_by_admin_user_lastname" => $confirmed_added_by_admin_user_lastname, ":finalized_by_admin_user_id" => $specific_finalized_by_admin_user_id, ":finalized_by_admin_user_firstname" => $specific_finalized_by_admin_user_firstname, ":finalized_by_admin_user_lastname" => $specific_finalized_by_admin_user_lastname);
								$last_inserted_candidate_id = insert_query_get_last_insert_id($candidate_sql, $candidate_values_array);
								
								if ($last_inserted_candidate_id != "") {
									//Candidate info is inserted and now, Candidate Email linking has to be done
									$candidate_email_details = get_candidate_email_details($last_inserted_candidate_id, $confirmed_email);
									
									if ($candidate_email_details == "") {
										//There is no duplicate entry with this candidate_id and email combination
										$email_activation_code = hash('sha256', uniqid(rand(), TRUE));
										//Do Insert Query into Candidate_emails db table
												
										//SQL Queries
										$candidate_email_linking_sql = "INSERT INTO `candidate_emails`(`candidate_id`, `email`, `is_primary`, `email_activation_code`) VALUES (:candidate_id,:email,:is_primary,:email_activation_code)";
										
										$candidate_email_linking_values_array = array(":candidate_id" => $last_inserted_candidate_id, ":email" => $specific_collected_email_address, ":is_primary" => "1", ":email_activation_code" => $email_activation_code);
										$last_inserted_candidate_email_linking_id = insert_query_get_last_insert_id($candidate_email_linking_sql, $candidate_email_linking_values_array);
										
										if ($last_inserted_candidate_email_linking_id != "") {
											//Candidate related internal notes has to be inserted in to candidate_rel_internal_notes db table
											//Notes posting Time, as per Indian Standard Time
											$notes_posting_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Asia/Kolkata");
											
											if ($confirmed_notes != "") {
												//Duplicate Check, internal note on candidate_rel_internal_notes db table 
												$internal_note_details = get_internal_note_info_from_internal_note_input($specific_extracted_info_id, $confirmed_notes);
												if ($internal_note_details == "") {
													
													
													//Do Insert Query into candidate_rel_internal_notes db table
														
													//SQL Queries
													$candidate_rel_int_notes_sql = "INSERT INTO `candidate_rel_internal_notes`(`candidate_id`, `extracted_info_id`, `notes`, `sm_memb_id`, `sm_firstname`, `sm_lastname`, `event_datetime`, `event_datetime_epoch`) VALUES (:candidate_id,:extracted_info_id,:notes,:sm_memb_id,:sm_firstname,:sm_lastname,:event_datetime,:event_datetime_epoch)";
													
													//replaced SESSION Variables, with Admin User Details, from candidates_quick_onboarding_data db table
													//$candidate_rel_int_notes_values_array = array(":candidate_id" => $last_inserted_candidate_id, ":extracted_info_id" => $specific_extracted_info_id, ":notes" => $confirmed_notes, ":sm_memb_id" => $_SESSION['sm_memb_id'], ":sm_firstname" => $_SESSION['sm_firstname'], ":sm_lastname" => $_SESSION['sm_lastname'], ":event_datetime" => $notes_posting_datetime, ":event_datetime_epoch" => $current_epoch);
													$candidate_rel_int_notes_values_array = array(":candidate_id" => $last_inserted_candidate_id, ":extracted_info_id" => $specific_extracted_info_id, ":notes" => $confirmed_notes, ":sm_memb_id" => $confirmed_added_by_admin_user_id, ":sm_firstname" => $confirmed_added_by_admin_user_firstname, ":sm_lastname" => $confirmed_added_by_admin_user_lastname, ":event_datetime" => $notes_posting_datetime, ":event_datetime_epoch" => $current_epoch);
													$last_inserted_candidate_rel_internal_notes_id = insert_query_get_last_insert_id($candidate_rel_int_notes_sql, $candidate_rel_int_notes_values_array);
												} else {
													$last_inserted_candidate_rel_internal_notes_id = $internal_note_details["candidate_rel_internal_note_id"];
												}//close of else of if ($internal_note_details == "") {
												
											}//close of if ($confirmed_notes != "") {
												
											
											
											//if ($last_inserted_candidate_rel_internal_notes_id != "") {
												//Candidate info is inserted and now, Candidate & Department Mapping has to be done
												$confirmed_departments_json_decoded = json_decode($confirmed_departments, true);
												$confirmed_departments_json_decoded_count = count($confirmed_departments_json_decoded);
									
												if($confirmed_departments_json_decoded_count > 0){
													//echo "<pre>";
													//print_r($confirmed_departments_json_decoded);
													//exit;
													
													foreach ($confirmed_departments_json_decoded as $department_row) {
														$department_id = trim(filter_var($department_row, FILTER_SANITIZE_NUMBER_INT));
																			
														
														$chosen_department_details = get_details_department_id($department_id);
														if ($chosen_department_details != "") {
															//Do insert in candidate_department_mapping db table
															//Do Insert Query
																
															//SQL Queries
															$candidate_department_mapping_sql = "INSERT INTO `candidate_department_mapping`(`candidate_id`, `department_id`, `is_active_status`) VALUES (:candidate_id,:department_id,:is_active_status)";
															
															$candidate_department_mapping_values_array = array(":candidate_id" => $last_inserted_candidate_id, ":department_id" => $department_id, ":is_active_status" => "1");
															$last_insert_id = insert_query_get_last_insert_id($candidate_department_mapping_sql, $candidate_department_mapping_values_array);
															
														}//close of if ($chosen_department_details != "") {
															
														
													}//close of foreach ($confirmed_departments_json_decoded as $department_row) {
												}//close of if($confirmed_departments_json_decoded_count > 0){
												
												$confirmed_skillsets_json_decoded = json_decode($confirmed_skillsets, true);
												$confirmed_skillsets_json_decoded_count = count($confirmed_skillsets_json_decoded);
									
												if($confirmed_skillsets_json_decoded_count > 0){
													foreach ($confirmed_skillsets_json_decoded as $confirmed_skillsets_row) {
														$particular_skillset_row = $confirmed_skillsets_row;
														$particular_skillset_row_exploded = explode(":::::", $particular_skillset_row);
														$particular_skillset_row_exploded_count = count($particular_skillset_row_exploded);
														if ($particular_skillset_row_exploded_count == 3) {
															$skillset_id = $particular_skillset_row_exploded[0];
															$skillset_name = $particular_skillset_row_exploded[1];
															$skillset_seo_name = $particular_skillset_row_exploded[2];
															
															$skillset_content_details = get_skillset_content_details_input($skillset_id, $skillset_name, $skillset_seo_name);
															$skillset_candidate_id_link_count = $skillset_content_details["skillset_candidate_link_count"];
															
															if ($skillset_content_details != "") {
																//Do Duplicate Check, to see, if this particular skillset_id is linked to particular candidate_id in candidate_skillsets db table
																$skillset_candidate_id_association_details = get_skillset_candidate_id_association_details_input($skillset_id, $last_inserted_candidate_id);
																
																
																if ($skillset_candidate_id_association_details == "") {
																	//SQL Queries
																	$skillset_candidate_id_associations_sql = "INSERT INTO `candidate_skillsets`(`candidate_id`, `skillset_id`, `skillset_name`, `skillset_seo_name`, `skillset_candidate_link_date_time`, `skillset_candidate_link_date_time_epoch`, `is_active_status`) VALUES (:candidate_id,:skillset_id,:skillset_name,:skillset_seo_name,:skillset_candidate_link_date_time,:skillset_candidate_link_date_time_epoch,:is_active_status)";
																	
																	$skillset_candidate_id_associations_values_array = array(":candidate_id" => $last_inserted_candidate_id, ":skillset_id" => $skillset_id, ":skillset_name" => $skillset_name, ":skillset_seo_name" => $skillset_seo_name, ":skillset_candidate_link_date_time" => $notes_posting_datetime, ":skillset_candidate_link_date_time_epoch" => $current_epoch, ":is_active_status" => "1");
																	$last_inserted_skillset_candidate_id = insert_query_get_last_insert_id($skillset_candidate_id_associations_sql, $skillset_candidate_id_associations_values_array);
																	
																	if ($last_inserted_skillset_candidate_id != "") {
																		//Successful insertion in to candidate_skillsets db table
																		echo "Skillset inserted Successfully \n";
																		
																		$skillset_candidate_id_link_count_updated = $skillset_candidate_id_link_count+1;
																		//Do UPDATE QUERY to skillsets db table, the summary and last usage date time stat update
																		//SQL Queries
																		$skillset_candidate_id_link_status_sql = "UPDATE `skillsets` SET `skillset_candidate_link_count`=:skillset_candidate_link_count,`last_used_date_time`=:last_used_date_time,`last_used_date_time_epoch`=:last_used_date_time_epoch WHERE `skillset_seo_name` =:skillset_seo_name";
																		
																		$skillset_candidate_id_link_status_values_array = array(":skillset_candidate_link_count" => $skillset_candidate_id_link_count_updated, ":last_used_date_time" => $notes_posting_datetime, ":last_used_date_time_epoch" => $current_epoch, ":skillset_seo_name" => $skillset_seo_name);
																		$skillset_candidate_id_update_status = update_query_based_on_id($skillset_candidate_id_link_status_sql, $skillset_candidate_id_link_status_values_array);
																		
																		//echo "skillset_candidate_id_update_status: " . var_dump($skillset_candidate_id_update_status);
																		if ($skillset_candidate_id_update_status == true) {
																			echo "The Skillset Candidate ID link count is updated Successfully.\n";
																		} else {
																			echo "There is an error when updating the Skillset Candidate ID link count, Please try again. \n";
																		}//close of else of if ($skillset_candidate_id_update_status == true) {
																		
																	} else {
																		echo "There is a problem inserting the skillset, please try again. \n";
																	}//close of else of if ($last_inserted_skillset_candidate_id != "") {
																}//close of if ($skillset_candidate_id_association_details == "") {
																
																
															}//close of if ($skillset_content_details != "") {
														}//close of if ($particular_skillset_row_exploded_count == 3) {
														
														
														
													}//close of foreach ($confirmed_skillsets_json_decoded as $confirmed_skillsets_row) {
														
												}//close of if($confirmed_skillsets_json_decoded_count > 0){
													
												
											/*} else {
												echo "There is an error with saving of the notes w.r.t. this particular Candidate Profile. \n";
											}//close of if ($last_inserted_candidate_rel_internal_notes_id != "") {
											*/
											
											//Finalized Time, as per Indian Standard Time
											$finalized_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Asia/Kolkata");
										
											//Mark the Particular resume related Extracted info id record as Finalized in extracted_info db table
											//Do Update Query on extracted_info db table
											
											//SQL Queries
											$extracted_info_finalization_status_sql = "UPDATE `extracted_info` SET `firstname`=:firstname,`lastname`=:lastname,`is_finalized`=:is_finalized,`finalized_datetime`=:finalized_datetime,`finalized_datetime_epoch`=:finalized_datetime_epoch WHERE `extracted_info_id` = :extracted_info_id";
											
											$extracted_info_finalization_status_values_array = array(":firstname" => $confirmed_candidate_firstname,":lastname" => $confirmed_candidate_lastname,":is_finalized" => "1", ":finalized_datetime" => $finalized_datetime, ":finalized_datetime_epoch" => $current_epoch, ":extracted_info_id" => $specific_extracted_info_id);
											$update_status = update_query_based_on_id($extracted_info_finalization_status_sql, $extracted_info_finalization_status_values_array);
											
											//echo "update_status: " . var_dump($update_status);
											if ($update_status == true) {
												echo "The Resume that is related to the Candidate Profile, whose Email ID is (" . $confirmed_email . "), is Finalized Successfully. Activation Email will be sent to the Email ID of the Candidate soon. \n";
											} else {
												echo "There is an error when processing the Resume that is related to the Candidate Profile, whose Email ID is (" . $confirmed_email . "), Please try again.\n";
											}//close of else of if ($update_status == true) {
												
											/*commented on 06-03-2018 by raghu //DO UPDATE QUERY TO ONBOARDING DATA DB TABLE with is_processed = 1
											update_processing_status_based_on_candidate_quick_onboarding_data_id_input($confirmed_id, "1", $finalized_datetime, $current_epoch, $last_inserted_candidate_id);
											*/
											//DO UPDATE QUERY TO ONBOARDING DATA DB TABLE with is_processed = 4, that denotes Finalized candidate Profile
											update_processing_status_based_on_candidate_quick_onboarding_data_id_input($confirmed_id, "4", $finalized_datetime, $current_epoch, $last_inserted_candidate_id);
										} else {
											echo "Error with linking Candidate information and email in the database. \n";
										}//close of else of if ($last_inserted_candidate_email_linking_id != "") {
											
									} else {
										//There is an entry with this candidate id and email combination
										echo "There is an entry with this candidate id and email combination \n";
										
										//Do remove the candidate entry in this case
										//Do Delete Query on candidates db table
											
										//SQL Queries
										$candidate_entry_removal_sql = "DELETE FROM `candidates` WHERE `candidate_id` = :candidate_id";
										
										$candidate_entry_removal_values_array = array(":candidate_id" => $last_inserted_candidate_id);
										$delete_status = delete_query_based_on_id($candidate_entry_removal_sql, $candidate_entry_removal_values_array);
									}//close of else of if ($candidate_email_details == "") {
									
									
									
								} else {
									echo "Error with adding Candidate information in to the database. \n";
								}
							}//close of if ($specific_is_finalized == "0") {
								
						}//close of if ($candidate_extracted_info_details != "") {
						
					}//close of if ($candidate_profile_confirmation_processing_status == "PROCESS-FINALIZE") {
					
					//Exit, after processing, a Single Candidate Profile Data (DB Record / Row)
					//exit;
					
				}//close of foreach ($candidate_quick_onboarding_data_details as $candidate_quick_onboarding_data_detail_row) {
				
			} else {
				echo "Invalid Candidate Profile Data Reference, Please check and try again. \n";
			}//close of if ($candidate_quick_onboarding_data_details != "") {
				
			//exit;
		
        				

		//code related to activity in particular cron file end
		sleep($res_sleep_filem);		
		
	}//close of for ( $filem = 1; $filem <=$res_loop_filem; $filem++ )
		
}//close of if ( $res_state_filem == "ON" ) {
?>