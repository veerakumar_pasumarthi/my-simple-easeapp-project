<?php
if(defined('STDIN') ){
  //echo("Running from CLI");
}else{
  echo("Not Running from CLI");
  exit();
}
/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 */
//if the directory is one directory above the current file's parent directory http://stackoverflow.com/a/2100763/811207
include(dirname(dirname(dirname(__FILE__))) . "/app/core/command-line-include.php");
include 'cron-job-related-email-template.php';

echo "before cron job details query\n";
$cron_number = "7";
$sel_cron_number_setting = $dbcon->prepare("SELECT * FROM `cron_file_details` WHERE `sno` = :sno");
$sel_cron_number_setting->bindParam(":sno",$cron_number);
$sel_cron_number_setting->execute(); 
$res_cron_number_setting = $sel_cron_number_setting->fetchAll(PDO::FETCH_ASSOC);
print_r($res_cron_number_setting);
//exit;
foreach($res_cron_number_setting as $res_cron_number_setting_row)
{
	$res_state = $res_cron_number_setting_row["cron_file_status_setting"];
	$res_records = $res_cron_number_setting_row["cron_file_numb_record_limit"];
	$res_loop = $res_cron_number_setting_row["cron_file_numb_loop_count_limit"];
	$res_sleep_min = $res_cron_number_setting_row["cron_file_sleep_min_seconds_limit"];
	$res_sleep_max = $res_cron_number_setting_row["cron_file_sleep_max_seconds_limit"];
	$res_sleep_interval = $res_cron_number_setting_row["cron_file_sleep_interval"];
	
}


if ( $res_state == "ON" ) {

	echo "inside on condition \n";		
	for ( $i = 1; $i <=$res_loop; $i++ ) {
		echo "entered into loop\n";
			
		$job_invite_get_sql = "SELECT * FROM `job_applicant_invites` WHERE `email_notification_status` = :email_notification_status AND `is_active_status` = :is_active_status LIMIT :res_records";
		echo "after select query\n";
		$job_invite_select_query = $dbcon->prepare($job_invite_get_sql);
		$job_invite_select_query->bindValue(":email_notification_status","0");
		$job_invite_select_query->bindValue(":is_active_status","1");
		$job_invite_select_query->bindValue(":res_records",$res_records);	
		$job_invite_select_query->execute(); 
		echo "before select query row count \n\n";
		
		if($job_invite_select_query->rowCount() > 0) {
			echo "after select query row count \n";
			$job_invite_select_query_result = $job_invite_select_query->fetchAll();
			foreach ($job_invite_select_query_result as $job_invite_select_query_result_row) {
				$job_applicant_invite_id = $job_invite_select_query_result_row["job_applicant_invite_id"];
				$company_id = $job_invite_select_query_result_row["company_id"];
				$company_client_id = $job_invite_select_query_result_row["company_client_id"];
				$job_id = $job_invite_select_query_result_row["job_id"];
				$invitation_sent_by_user_id = $job_invite_select_query_result_row["invitation_sent_by_user_id"];
				$invitation_received_by_user_id = $job_invite_select_query_result_row["invitation_received_by_user_id"];
				$invite_ref_code = $job_invite_select_query_result_row["invite_ref_code"];
				echo "before getting user details \n";	
				
				$job_details_result = get_job_details($job_id);
				$job_title = $job_details_result["job_title"];
				$job_summary = $job_details_result["job_summary"];
				$job_type_name = $job_details_result["job_type_name"];
				$city = $job_details_result["city"];
				$job_full_description = $job_details_result["job_full_description"];
				$resume_submission_end_date = $job_details_result["resume_submission_end_date"];
				$company_id = $job_details_result["company_id"];
				$company_client_id = $job_details_result["company_client_id"];	
				$company_client_details_result = get_company_client_details($company_client_id);
				$client_company_name = $company_client_details_result["client_company_name"];
				
				//$plaintext_input = "Hello! welcome to application";
				//$html_message_input = "<p>Hello!</p>";
				$recruiter_details = user_basic_details_check_based_on_user_id($invitation_sent_by_user_id);
				$candidate_details = user_basic_details_check_based_on_user_id($invitation_received_by_user_id);
				//Get recruiter's Salutation, firstname,Middlename	and Lastname
				echo "the user details are \n";
				if($recruiter_details != ""){
					$recruiter_sm_salutation = $recruiter_details["sm_salutation"];
					$recruiter_sm_firstname = $recruiter_details["sm_firstname"];
					$recruiter_sm_middlename = $recruiter_details["sm_middlename"];
					$recruiter_sm_lastname = $recruiter_details["sm_lastname"];
				} else {
					$recruiter_sm_salutation = "";
					$recruiter_sm_firstname = "";
					$recruiter_sm_middlename = "";
					$recruiter_sm_lastname = "";
				}
				echo "the details of recruiter \n";
					
				//Get Candidate's Salutation, Firstname, Middlename and Lastname
				if ($candidate_details != "") {
					$sm_salutation = $candidate_details["sm_salutation"];
					$sm_firstname = $candidate_details["sm_firstname"];
					$sm_middlename = $candidate_details["sm_middlename"];
					$sm_lastname = $candidate_details["sm_lastname"];
					$sm_user_type = $candidate_details["sm_user_type"];
					$sm_admin_level = $candidate_details["sm_admin_level"];
					$sm_user_role = $candidate_details["sm_user_role"];
					$company_id = $candidate_details["company_id"];
					$sm_email = $candidate_details["sm_email"];
					
				} else {
					$sm_salutation = "";
					$sm_firstname = "";
					$sm_middlename = "";
					$sm_lastname = "";
					$sm_user_type = "";
					$sm_admin_level = "";
					$sm_user_role = "";
					$company_id = "";
					
				}//close of else of if ($candidate_details != "") {
				

				
				if (($sm_firstname != "") && ($sm_middlename != "") && ($sm_lastname != "")) {
					$candidate_fullname = $sm_firstname . " " . $sm_middlename . " " . $sm_lastname;
					$greeting_user_summary = "Dear " . $candidate_fullname . ",";
				} else if (($sm_firstname != "") && ($sm_middlename != "") && ($sm_lastname == "")) {
					$candidate_fullname = $sm_firstname . " " . $sm_middlename;
					$greeting_user_summary = "Dear " . $candidate_fullname . ",";
				} else if (($sm_firstname != "") && ($sm_middlename == "") && ($sm_lastname == "")) {
					$candidate_fullname = $sm_firstname;
					$greeting_user_summary = "Dear " . $candidate_fullname . ",";
				} else if (($sm_firstname != "") && ($sm_middlename == "") && ($sm_lastname != "")) {
					$candidate_fullname = $sm_firstname . " " . $sm_lastname;
					$greeting_user_summary = "Dear " . $candidate_fullname . ",";
				} else if (($sm_firstname == "") && ($sm_middlename != "") && ($sm_lastname != "")) {
					$candidate_fullname = $sm_middlename . " " . $sm_lastname;
					$greeting_user_summary = "Dear " . $candidate_fullname . ",";
				} else if (($sm_firstname == "") && ($sm_middlename == "") && ($sm_lastname != "")) {
					$candidate_fullname = $sm_lastname;
					$greeting_user_summary = "Dear " . $candidate_fullname . ",";
				} else {
					$candidate_fullname = "";
					$greeting_user_summary = "Dear,";
				}//close of else of if (($candidate_firstname != "") && ($sm_middlename != "") && ($candidate_lastname != "")) {
				
				$company_name = "";
				if (!is_null($company_id)) {
						
					$candidate_company_details = get_company_details_based_on_company_id($company_id);
					if ($candidate_company_details != "") {
						
						$company_name = $candidate_company_details["company_name"];
						$company_support_email = $candidate_company_details["company_support_email"];
						
						
						
					} else {
					
						
						$company_name = "";
						$company_support_email = "";
						
					}//close of else of if ($candidate_company_details != "") {	
						
				}//close of if (!is_null($company_id)) {
				
				//Construct URL
				/*
				
				http://127.0.0.1:8080/LAYOUT-2/STANDARD/#!/account/activate/user-act-code/yd665r5er65r56edd6565te65e56e56e56ter6er5775e57


				Reset Password Link:

				http://127.0.0.1:8080/LAYOUT-2/STANDARD/#!/account/reset-password/user-pass-reset-code/yd665r5er65r56edd6565te65e56e56e56ter6er5775e57dgdfg4t45gdfgfd

				*/
				$constructed_message_plain_text = " A new job has been posted that is suitable for your skills and preferences. The job details are mentioned below: Job Title : $job_title";
									
		$constructed_message_html = "
								
		 <p>A new job has been posted that is suitable for your skills and preferences. The job details are mentioned below:</p>
		 <p> Job Title : $job_title </p>
		 <p> Job Type  : $job_type_name  </p>
         <p> Company :$company_name </p>
		 <p> Client Company :$client_company_name </p>
		 <p> Location: $city </p>
		 <p> Job Summary : $job_summary </p> 
		 <p> Job Description : $job_full_description </p>";
		 
		 //$constructed_url = $frontend_site_url_project_main_with_path . "jsvnavigate/" . $job_id;
									
        $constructed_url = "https://app.resumecrab.com/" . "cjic/company/".$company_id."/job/".$job_id."/invite-code/".$invite_ref_code;
									
	    echo "constructed_url: " . $constructed_url . "\n\n";
								
		$job_assignment_link_without_html = $constructed_url . "\n\n";
							
		//echo "job_applicant_invite_link_without_html: " . $job_applicant_invite_link_without_html . "\n\n";
							
		$job_assignment_link_with_html = "<p><a href='" . $constructed_url . "'>" . $constructed_url . "</a><br><br></p>";
							
		//echo "account_activation_password_setup_reset_link_with_html: " . $account_activation_password_setup_reset_link_with_html . "\n\n";
							
		//Send Email

		$link_text = "If you are interested in this job, please click Here and Enter the information" ;

		$button_content = "Job Application";
							
		$plaintext_input = $greeting_user_summary . "
							
		" . $constructed_message_plain_text . "
							
		" .  
							

		"\n\n" .
							
		$job_assignment_link_without_html .

		"\n" .
		"Thank you \n
		
		________________________________________________________________________

		";

	/*	$html_message_input = "<div><p>" . $greeting_user_summary . "<br><br>

		" . $constructed_message_html

		. $job_assignment_link_with_html .
							 
				"
							
			<p> </p>
			<p>Regards,<br>
			Admin Team, <br>
			$company_name <br>
			</p>
		<p>________________________________________________________________________</p>

							</div>"; */
		$html_message_input = template($greeting_user_summary ,$link_text, $constructed_url , $company_name , $button_content , $constructed_message_html);

							//echo "plaintext_input: " . $plaintext_input . "<br>";
							//echo "html_message_input: " . $html_message_input . "<br>";
							//exit;
							
				
			if (filter_var($sm_email, FILTER_VALIDATE_EMAIL) == true) {
				
				$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
			
				//send_email_using_smtp_thr_phpmailer("notifications@vdmail.securitywonks.net","Innovative Solutions", "notifications@vdmail.securitywonks.net","webmaster@securitywonks.org","Raghu sir","To check email","hello welcome","<p> hello welcome</p>");	
				//exit;
				//commented on 10-02-2019 18:10 $send_email_using_smtp_thr_phpmailer_result = send_email_using_smtp_thr_phpmailer($activation_email_sender_email, $activation_email_sender_name, $activation_email_sender_email, $recipient_email_address,null,$activation_email_subject, $plaintext_input, $html_message_input);

				/*if (($request_purpose == "activation-email") || ($request_purpose == "re-send-activation-email")) {
					
					$send_email_using_smtp_thr_phpmailer_result = send_email_using_smtp_thr_phpmailer($activation_email_sender_email, $activation_email_sender_name, $activation_email_sender_email, $recipient_email_address,null,$activation_email_subject, $plaintext_input, $html_message_input);
					
				} else if ($request_purpose == "reset-password") {
					
					$send_email_using_smtp_thr_phpmailer_result = send_email_using_smtp_thr_phpmailer($password_resetting_email_sender_email, $password_resetting_email_sender_name, $password_resetting_email_sender_email, $recipient_email_address,null,$password_resetting_email_subject, $plaintext_input, $html_message_input);
					
				}//close of else if of if (($request_purpose == "activation-email") || ($request_purpose == "re-send-activation-email")) {*/
				
					$to_email = array();
					$to_email[] = $sm_email;
					$email_subject = "Job Invite notification";
					$from_email_address = "notifications@resumecrab.com";
					$from_person_name = "Resumecrab Notifications";
					$sender_email_address = "notifications@resumecrab.com";
					$sender_person_name = "Resumecrab Notifications";
					$replyto_email_address = "notifications@resumecrab.com";
					$replyto_person_name = "Resumecrab Notifications";
					
					$send_email_using_elastic_email_result = send_email_using_elastic_email($from_email_address,$from_person_name,$sender_email_address, $sender_person_name, $replyto_person_name,$replyto_email_address, $to_email,$email_subject, $plaintext_input, $html_message_input);
				
					/* $send_email_using_smtp_thr_phpmailer_result = send_email_using_smtp_thr_phpmailer($password_resetting_email_sender_email, $password_resetting_email_sender_name, $password_resetting_email_sender_email, $sm_email,null, "job invite notifications", $plaintext_input, $html_message_input) */; 
				
					echo "after mail function";
					//send_email_using_smtp_thr_phpmailer($from_email_input, $sender_name_input, $reply_to_email_input, $to_email_input, $receiver_name_input, $subject_input, $plaintext_input, $html_message_input);	
			
				
				if($send_email_using_elastic_email_result == true){
					
					$new_email_notification_status = "1";
					$email_service_provider_mail_ref_message = "message-sent";
					$email_sent_date_time = $event_datetime;
					$email_sent_date_time_epoch = $current_epoch;
					
					
				} else {
					$new_email_notification_status = "4";
					$email_service_provider_mail_ref_message = null;
					$email_sent_date_time = null;
					$email_sent_date_time_epoch = null;
				}
					 
					 
					
					 
					$job_applicant_invite_update_sql = "UPDATE `job_applicant_invites` SET `email_notification_status`=:email_notification_status,`email_sent_date_time`=:email_sent_date_time,`email_sent_date_time_epoch`=:email_sent_date_time_epoch,`email_service_provider`=:email_service_provider,`email_service_provider_mail_ref_id`=:email_service_provider_mail_ref_id,`email_service_provider_mail_ref_message`=:email_service_provider_mail_ref_message,`last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch WHERE `job_applicant_invite_id` = :job_applicant_invite_id";
				
				$job_applicant_invite_update_query = $dbcon->prepare($job_applicant_invite_update_sql);
				
				
				$job_applicant_invite_update_query->bindValue(":email_notification_status",$new_email_notification_status);
				$job_applicant_invite_update_query->bindValue(":email_sent_date_time",$email_sent_date_time);
				$job_applicant_invite_update_query->bindValue(":email_sent_date_time_epoch",$email_sent_date_time_epoch);
				$job_applicant_invite_update_query->bindValue(":email_service_provider",null);
				$job_applicant_invite_update_query->bindValue(":email_service_provider_mail_ref_id",null);
				$job_applicant_invite_update_query->bindValue(":email_service_provider_mail_ref_message",$email_service_provider_mail_ref_message);
				$job_applicant_invite_update_query->bindValue(":last_updated_date_time",$event_datetime);
				$job_applicant_invite_update_query->bindValue(":last_updated_date_time_epoch",$current_epoch);
				$job_applicant_invite_update_query->bindValue(":job_applicant_invite_id",$job_applicant_invite_id);
							
					
				if($job_applicant_invite_update_query->execute()){
					  //echo "done";
					  echo "Email Sent Successfully";
				}
			} else {
				echo "Recipient's Email Address is Invalid, as per Email Format Validation Checks \n\n";
			}//close of else of if (filter_var($recipient_email_address, FILTER_VALIDATE_EMAIL) == true) {
				
		}//close of for ( $i = 1; $i <=$res_loop; $i++ ) {
		}
		
	}//close of if ( $res_state == "ON" ) {
}

//COPIED FROM other-functions-api.php TEMPORARILY
/* function user_basic_details_check_based_on_user_id($sm_memb_id_input) {
	global $dbcon;
	$constructed_array = array();
	$rest_user_check_sql = "SELECT * FROM `site_members` WHERE `sm_memb_id` = :sm_memb_id";
	$rest_user_check_select_query = $dbcon->prepare($rest_user_check_sql);
	$rest_user_check_select_query->bindValue(":sm_memb_id",$sm_memb_id_input);				 
	$rest_user_check_select_query->execute(); 
	
	if($rest_user_check_select_query->rowCount() > 0) {
		$rest_user_check_select_query_result = $rest_user_check_select_query->fetch();
	     return $rest_user_check_select_query_result;
	
	}//close of if($rest_user_check_select_query->rowCount() > 0) {
	return $constructed_array;
	
} */

//COPIED FROM other-functions-api.php TEMPORARILY

/* function get_company_details_based_on_company_id($company_id_input) {

    global $dbcon;
	$constructed_array = array();
		$companies_details_get_sql = "SELECT * FROM `companies` WHERE `company_id`=:company_id";
		
		$companies_details_get_select_query = $dbcon->prepare($companies_details_get_sql);
		$companies_details_get_select_query->bindValue(":company_id",$company_id_input);
		$companies_details_get_select_query->execute();
		
	if($companies_details_get_select_query->rowCount() > 0) {
	    $companies_details_get_select_query_result = $companies_details_get_select_query->fetch();
	    //print_r($companies_details_get_select_query_result);
	    	
		return $companies_details_get_select_query_result;
	}
	return $constructed_array;
} */	

function get_company_client_details($company_client_id) {
	global $dbcon;
	$constructed_array = array();
	$company_client_details_check_sql = "SELECT * FROM `company_clients`  WHERE `company_client_id` = :company_client_id";
	$company_client_details_check_select_query = $dbcon->prepare($company_client_details_check_sql);
	$company_client_details_check_select_query->bindValue(":company_client_id",$company_client_id);
	$company_client_details_check_select_query->execute();

	if($company_client_details_check_select_query->rowCount() > 0) {
		$company_client_details_check_select_query_result = $company_client_details_check_select_query->fetch();
	     return $company_client_details_check_select_query_result;

	}//close of if($company_details_check_select_query->rowCount() > 0) {
	return $constructed_array;
}
function get_job_details($job_id) {
	global $dbcon;
	$constructed_array = array();
	$job_details_check_sql = "SELECT * FROM `jobs` WHERE `job_id` = :job_id";
	$job_details_check_select_query = $dbcon->prepare($job_details_check_sql);
	$job_details_check_select_query->bindValue(":job_id",$job_id);
	$job_details_check_select_query->execute();

	if($job_details_check_select_query->rowCount() > 0) {
		$job_details_check_select_query_result = $job_details_check_select_query->fetch();
	     return $job_details_check_select_query_result;

	}//close of if($company_details_check_select_query->rowCount() > 0) {
	return $constructed_array;
} 
?>