<?php
if(defined('STDIN') ){
  //echo("Running from CLI");
}else{
  echo("Not Running from CLI");
  exit();
}

ini_set('memory_limit', '-1');
//PCRE Backtrack Limit is increased from 1000000 to 4000000, to handle supporting, bigger images in PDF Files using PDF to Text Library
ini_set('pcre.backtrack_limit', 4000000);
//if the directory is one directory above the current file's parent directory http://stackoverflow.com/a/2100763/811207
include(dirname(dirname(dirname(__FILE__))) . "/app/core/command-line-include.php");

$cron_number = "1";
$sel_filem_setting = $dbcon->prepare("SELECT * FROM `cron_file_details` WHERE `sno` = :sno");
$sel_filem_setting->bindParam(":sno",$cron_number);
$sel_filem_setting->execute(); 
$res_filem_setting = $sel_filem_setting->fetchAll(PDO::FETCH_ASSOC);
//print_r($res_filem_setting);
//exit;
foreach($res_filem_setting as $res_filem_setting_row)
{
	$res_state_filem = $res_filem_setting_row["cron_file_status_setting"];
	$res_records_filem = $res_filem_setting_row["cron_file_numb_record_limit"];
	$res_loop_filem = $res_filem_setting_row["cron_file_numb_loop_count_limit"];
	$res_sleep_min_filem = $res_filem_setting_row["cron_file_sleep_min_seconds_limit"];
	$res_sleep_max_filem = $res_filem_setting_row["cron_file_sleep_max_seconds_limit"];
	$res_sleep_interval_filem = $res_filem_setting_row["cron_file_sleep_interval"];
	
}




if ( $res_state_filem == "ON" ) {

			
	for ( $filem = 1; $filem <=$res_loop_filem; $filem++ ) {

function output_collected ($message) {
	if  (php_sapi_name( )  ==  'cli') {
		return $message;
	} else {
	    return nl2br($message);
	}	
}
		/*
		Source Folder: /home/inademy/public_html/pdf-resumes
		Destination Folder: /home/inademy/public_html/pdf-destination-resumes
		
		
		*/
		
		
		
		echo "File Copy w.r.t. Email Address, that is received from parsed pd files \n";
		
		$pdf_file_directory = $siteroot_basedir_command_line . "pdf-resumes" . "/";
		echo "pdf_file_directory: " . $pdf_file_directory . "\n";
		
		$pdf_file_destination_directory = $siteroot_basedir_command_line . "pdf-destination-resumes" . "/";
		echo "pdf_file_destination_directory: " . $pdf_file_destination_directory . "\n";
		
		/*$pdf_files_list = list_files($pdf_file_directory);
		
		echo "<pre>";
		print_r($pdf_files_list);
		
		
		//find list of PDF files in the directory and return them as an array
		function list_files($filedirectory) {        
			 $list_files = glob($filedirectory . "/" . "*" . "pdf");
			return $list_files;
		}*/
		
		function find_file_extension($path) {
			 $file_type = pathinfo($path);
			//Determine the extension/type and process file accordingly
			$file_extension = $file_type['extension'];
			return $file_extension;
		}
		
		
		//get Resume_id from extracted_info db table using collected_email_address column
		function get_resume_id_based_on_collected_email_address_input($collected_email_address_input){
		global $dbcon;
			
			$search_input = '%'.$collected_email_address_input.'%';
			$resume_id_get_sql = "SELECT * FROM `extracted_info` WHERE `collected_email_address` LIKE :collected_email_address";
		
			$resume_id_get_select_query = $dbcon->prepare($resume_id_get_sql);
			$resume_id_get_select_query->bindValue(":collected_email_address", $search_input, PDO::PARAM_STR);
			$resume_id_get_select_query->execute();
			
			if($resume_id_get_select_query->rowCount() > 0) {
			   $resume_id_get_select_query_result = $resume_id_get_select_query->fetch();
			   //print_r($resume_id_get_select_query_result);
			   //exit;
			   $resume_id = $resume_id_get_select_query_result["resume_id"];
			   return $resume_id;
			}
			
				
		return "";
		}
		
		
		//get resume_generated_file_name from resumes db table using resume_id column
		function get_resume_generated_file_name_based_on_resume_id_input($resume_id_input){
		global $dbcon;
			
			
			$resume_id_get_sql = "SELECT * FROM `resumes` WHERE `resume_id` =:resume_id";
		
			$resume_id_get_select_query = $dbcon->prepare($resume_id_get_sql);
			$resume_id_get_select_query->bindValue(":resume_id",$resume_id_input);
			$resume_id_get_select_query->execute();
			
			if($resume_id_get_select_query->rowCount() > 0) {
			   $resume_id_get_select_query_result = $resume_id_get_select_query->fetch();
			   $resume_generated_file_name = $resume_id_get_select_query_result["resume_generated_file_name"];
			   return $resume_generated_file_name;
			}
			
				
		return "";
		}

		$directory = opendir($pdf_file_directory);
		  while ($file = readdir($directory)) {
			if($file!="." && $file!=".."){
				
				echo $file."\n";
				$file_extension = find_file_extension($file);
				echo $file_extension . "\n";
				if ($file_extension == "pdf") {
					
					$resume_document_path = $pdf_file_directory . "/" . $file;
					
					
					//The File Format of the being processed resume document is PDF
					//$txt = new PdfToText ($resume_document_path);
					$pdf = new PdfToText ( $resume_document_path, PdfToText::PDFOPT_DECODE_IMAGE_DATA ) ;
					$image_count_in_pdf_file = count ($pdf->Images) ;
					
					if  ($image_count_in_pdf_file) {
						//echo "One or more images are found in the sample file " . $resume_document_path;
						$txt = $pdf->Text;
					} else {
						//echo "No image was found in sample file " . $resume_document_path;
						$txt = $pdf->Text;
					}//close of else of if  ($image_count_in_pdf_file) {
				}//close of if ($file_extension == "pdf") {
				//echo "txt: " . $txt . "\n";
				$txt_strlen = mb_strlen($txt);
				//echo "txt_strlen: " . $txt_strlen . "\n";
				if ($txt_strlen > 0) {
					//echo "The string length is greater than zero.\n";
					//Convert NL2Br				
					$output_collected = output_collected($txt);
					//echo $output_collected . "\n";
					//exit;
					//Process Text and Collect Email IDs
					$collected_email_ids = get_email_ids_from_html($output_collected);
					//echo "<pre>";
					//print_r($collected_email_ids);
					//echo "</pre>";
					//exit;
					//Find Count of obtained unique email ids
					$collected_email_ids_count = count($collected_email_ids);
					//echo "<pre>";
					//print_r($collected_email_ids);
					//exit;
					//Check if more than one unique email id is obtained
					if ($collected_email_ids_count == 1) {
						//Only one Email ID is obtained
						$final_email_id = $collected_email_ids[0];
					} else if ($collected_email_ids_count > 1) {
						//More than One Email ID is obtained then, sort them based on length of value of the array
						function sort_based_on_length($a,$b){
							return strlen($b)-strlen($a);
						}

						usort($collected_email_ids,'sort_based_on_length');
						
						//Choosing the Email ID with Higher Length as final EMail ID
						$final_email_id = $collected_email_ids[0];
					}
					//print_r($final_email_id);
					//var_dump($final_email_id);
					//exit;
					//Extract Local Part of Email ID
					//$final_email_id_exploded = explode("@", $final_email_id);
					//$email_id_localpart = $final_email_id_exploded[0];
					
					
					//Enumerate First Name and Last Name from the Email ID Username
					
					//Remove numbers from Email
					//echo $email_id_localpart;
					
					$firstname = "";
					$lastname = "";
					
					//Last attempted Time, in terms of data extraction of the resume, as per Indian Standard Time
					$last_attempted_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Asia/Kolkata");
					
					if ($final_email_id != "") {
						echo "final_email_id (in if condition): " . $final_email_id . "\n";
						$resume_id = get_resume_id_based_on_collected_email_address_input($final_email_id);
						echo "resume_id: " . $resume_id . "\n";
						if ($resume_id != "") {
							$resume_generated_file_name = get_resume_generated_file_name_based_on_resume_id_input($resume_id);
							
							echo "resume_generated_file_name: " . $resume_generated_file_name . "\n";
							
							$resume_document_destination_path = $pdf_file_destination_directory . $resume_generated_file_name;
							
							echo "resume_document_destination_path: " . $resume_document_destination_path . "\n";
							//exit;
							if ($resume_generated_file_name != "") {
								//rename($resume_document_path, $resume_document_destination_path);
								if (!rename($resume_document_path, $resume_document_destination_path)) {
									echo "failed to copy " . $resume_document_path . "... to new folder, " . $resume_document_destination_path . " \n";
								}//if (!rename($resume_document_path, $resume_document_destination_path)) {
								//exit;
							}//close of if ($resume_generated_file_name != "") {
						} else {
							echo "There is no linked resume, for email id: " . $final_email_id . "\n";
						}//close of else of if ($resume_id != "") {
						
							
					}//close of if ($final_email_id != "") {
						
				} else {
					echo "The string length is equal to zero. This means, Email ID is not found.\n";
					
				}//close of else of if ($txt_strlen > 0) {
					
			  
			}
		  }
		closedir($directory);
		
		

				
				
								

		//code related to activity in particular cron file end
		sleep($res_sleep_filem);		
		
	}//close of for ( $filem = 1; $filem <=$res_loop_filem; $filem++ )
		
}//close of if ( $res_state_filem == "ON" ) {
?>