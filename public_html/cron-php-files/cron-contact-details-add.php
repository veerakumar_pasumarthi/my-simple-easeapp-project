<?php
if(defined('STDIN') ){
  //echo("Running from CLI");
}else{
  echo("Not Running from CLI");
  exit();
}
/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 */
//if the directory is one directory above the current file's parent directory http://stackoverflow.com/a/2100763/811207
include(dirname(dirname(dirname(__FILE__))) . "/app/core/command-line-include.php");
echo "before cron job details query\n";
$cron_number = "23";
$sel_cron_number_setting = $dbcon->prepare("SELECT * FROM `cron_file_details` WHERE `sno` = :sno");
$sel_cron_number_setting->bindParam(":sno",$cron_number);
$sel_cron_number_setting->execute(); 
$res_cron_number_setting = $sel_cron_number_setting->fetchAll(PDO::FETCH_ASSOC);
print_r($res_cron_number_setting);
//exit;
foreach($res_cron_number_setting as $res_cron_number_setting_row)
{
	$res_state = $res_cron_number_setting_row["cron_file_status_setting"];
	$res_records = $res_cron_number_setting_row["cron_file_numb_record_limit"];
	$res_loop = $res_cron_number_setting_row["cron_file_numb_loop_count_limit"];
	$res_sleep_min = $res_cron_number_setting_row["cron_file_sleep_min_seconds_limit"];
	$res_sleep_max = $res_cron_number_setting_row["cron_file_sleep_max_seconds_limit"];
	$res_sleep_interval = $res_cron_number_setting_row["cron_file_sleep_interval"];
	
}


if ( $res_state == "ON" ) {

	echo "inside on condition \n";		
	  for ( $i = 1; $i <=$res_loop; $i++ ) {
		echo "entered into loop\n";
				
		$job_details_get_sql = "SELECT * FROM `contact_page_queries` WHERE `cust_rel_email_sending_status` = :cust_rel_email_sending_status AND `is_active_status`=:is_active_status";
		echo "after select query\n";
		$job_details_select_query = $dbcon->prepare($job_details_get_sql);
		$job_details_select_query->bindValue(":cust_rel_email_sending_status","0");
		$job_details_select_query->bindValue(":is_active_status","1");
				
		$job_details_select_query->execute(); 
		echo "before select query row count \n\n";
		
			if($job_details_select_query->rowCount() > 0) {
				echo "after select query row count \n";
				$job_details_select_query_result = $job_details_select_query->fetchAll();
				foreach ($job_details_select_query_result as $job_details_select_query_result_row) {
					
					$country_name = $job_details_select_query_result_row["country_name"];
					$purpose_id = $job_details_select_query_result_row["purpose_id"];
					$to_email_input = $job_details_select_query_result_row["email"];
					$candidate_fullname = $job_details_select_query_result_row["full_name"];
					$cp_query_id = $job_details_select_query_result_row["cp_query_id"];
					
					echo "before getting user details \n";	
					$candidate_details = get_contact_mail_user_details($purpose_id,$country_name);
					
			//Get Candidate's Salutation, Firstname, Middlename and Lastname
				if (count($candidate_details)>0) {
					$reply_to_email_input = $candidate_details["email"];
					
				
				} else {
					$reply_to_email_input = "";
					
				
				}

				if (($candidate_fullname != ""))  {
					$greeting_user_summary = "Hi " . $candidate_fullname . "," ;
				} else {
					$candidate_fullname = "";
					$greeting_user_summary = "Hi,";
				}
				
						
					
			$plaintext_input = $greeting_user_summary . "
			
			Thanks for using

			
           
			\n" .
			"Regards \n
			
		     Federal Soft Systems
			________________________________________________________________________

			";

			$html_message_input = "<div><p>" . $greeting_user_summary . "<br><br></p>
			
				Thanks for using
			   
			
			<p> </p>
			<p>Regards<br>
			Federal Soft Systems
		    </p>
			<p>________________________________________________________________________</p>

			</div>";

			echo "plaintext_input: " . $plaintext_input . "<br>";
			//echo "html_message_input: " . $html_message_input . "<br>";
			$recipient_email_address= $to_email_input;
			 echo "Email \n";
			 echo $recipient_email_address;
			 $request_subject = "contact details" ;			
							
	if (filter_var($recipient_email_address, FILTER_VALIDATE_EMAIL) == true) {
							
	$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
						
$to_email = array();
					$sm_email = $recipient_email_address;
					$to_email[] = $sm_email;
					$email_subject = $request_subject;
					$from_email_address = "noreply@federalsoftsystems.com";
					$from_person_name = "Federal Soft System Notifications";
					$sender_email_address = "noreply@federalsoftsystems.com";
					$sender_person_name = "Federal Soft System Notifications";
					$replyto_email_address = $reply_to_email_input;
					$replyto_person_name = "Federal Soft System Notifications";
					
					$send_email_using_elastic_email_result = send_email_using_elastic_email($from_email_address,$from_person_name,$sender_email_address, $sender_person_name, $replyto_person_name,$replyto_email_address, $to_email,$email_subject, $plaintext_input,$html_message_input);
					echo "Email Sending Status".$send_email_using_elastic_email_result;
					
					$email_service_provider =  "Elastic Email";
		    
				if($send_email_using_elastic_email_result == '1'){
					
					$new_email_notification_status = "1";
					$email_service_provider_mail_ref_message = "message-sent";
					$email_sent_date_time = $event_datetime;
					$email_sent_date_time_epoch = $current_epoch;
					
					
				} else {
					$new_email_notification_status = "4";
					$email_service_provider_mail_ref_message = null;
					$email_sent_date_time = null;
					$email_sent_date_time_epoch = null;
				
				}
					echo "yooooooooov in 229\n";
					
				$contact_details_setup_update_sql = "UPDATE `contact_page_queries` SET `cust_rel_email_sending_status`=:cust_rel_email_sending_status,`cust_rel_sent_datetime`=:cust_rel_sent_datetime,`cust_rel_sent_datetime_epoch`=:cust_rel_sent_datetime_epoch,`cust_rel_email_service_provider`=:cust_rel_email_service_provider,`cust_rel_email_service_provider_mail_ref_id`=:cust_rel_email_service_provider_mail_ref_id, `cust_rel_email_service_provider_mail_ref_message`=:cust_rel_email_service_provider_mail_ref_message,`last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch WHERE `cp_query_id` = :cp_query_id";
			
					$user_profile_source_update_q = $dbcon->prepare($contact_details_setup_update_sql);
	$user_profile_source_update_q->bindValue(":cust_rel_email_sending_status",$new_email_notification_status);
	$user_profile_source_update_q->bindValue(":cust_rel_sent_datetime",$email_sent_date_time);
	$user_profile_source_update_q->bindValue(":cust_rel_sent_datetime_epoch",$email_sent_date_time_epoch);
	$user_profile_source_update_q->bindValue(":cust_rel_email_service_provider",$email_service_provider);
	$user_profile_source_update_q->bindValue(":cust_rel_email_service_provider_mail_ref_id",null);
	$user_profile_source_update_q->bindValue(":cust_rel_email_service_provider_mail_ref_message",null);
	$user_profile_source_update_q->bindValue(":cp_query_id",$cp_query_id);

	$user_profile_source_update_q->bindValue(":last_updated_date_time",$event_datetime);
	$user_profile_source_update_q->bindValue(":last_updated_date_time_epoch",$current_epoch);
    
		if ($user_profile_source_update_q->execute()) {
						//echo "done";
						echo "Email Sent Successfully";
					} else {
						echo "Recipient's Email Address is Invalid, as per Email Format Validation Checks \n\n";
					}
				} //close of else of if (filter_var($recipient_email_address, FILTER_VALIDATE_EMAIL) == true) { else {
					echo "Recipient's Email Address is Invalid, as per Email Format Validation Checks \n\n";
					}	
			}
		}
    }	
		
								 
function get_contact_mail_user_details($purpose_id,$country_name) {

	global $dbcon;
	$constructed_array = array();
	$user_details_get_sql = "SELECT * FROM `purpose_mapped_company_emails` WHERE `purpose_id`=:purpose_id AND `country_name`=:country_name";
					
	$user_details_get_select_query = $dbcon->prepare($user_details_get_sql);
	$user_details_get_select_query->bindValue(":purpose_id",$purpose_id);
	$user_details_get_select_query->bindValue(":country_name",$country_name);
	$user_details_get_select_query->execute();
					
	if($user_details_get_select_query->rowCount() > 0) {
	$user_details_get_select_query_result = $user_details_get_select_query->fetch();
		//print_r($companies_details_get_select_query_result);
						
		return $user_details_get_select_query_result;
		}
		return $constructed_array;
	}
?>			 
								
							