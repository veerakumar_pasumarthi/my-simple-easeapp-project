<?php
if(defined('STDIN') ){
  //echo("Running from CLI");
}else{
  echo("Not Running from CLI");
  exit();
}
/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 */
//if the directory is one directory above the current file's parent directory http://stackoverflow.com/a/2100763/811207
include(dirname(dirname(dirname(__FILE__))) . "/app/core/command-line-include.php");
echo "before cron job details query\n";
$cron_number = "2";
$sel_cron_number_setting = $dbcon->prepare("SELECT * FROM `cron_file_details` WHERE `sno` = :sno");
$sel_cron_number_setting->bindParam(":sno",$cron_number);
$sel_cron_number_setting->execute(); 
$res_cron_number_setting = $sel_cron_number_setting->fetchAll(PDO::FETCH_ASSOC);
print_r($res_cron_number_setting);
//exit;
foreach($res_cron_number_setting as $res_cron_number_setting_row)
{
	$res_state = $res_cron_number_setting_row["cron_file_status_setting"];
	$res_records = $res_cron_number_setting_row["cron_file_numb_record_limit"];
	$res_loop = $res_cron_number_setting_row["cron_file_numb_loop_count_limit"];
	$res_sleep_min = $res_cron_number_setting_row["cron_file_sleep_min_seconds_limit"];
	$res_sleep_max = $res_cron_number_setting_row["cron_file_sleep_max_seconds_limit"];
	$res_sleep_interval = $res_cron_number_setting_row["cron_file_sleep_interval"];
	
}
if ( $res_state == "ON" ) {

	echo "inside on condition \n";		
	for ( $i = 1; $i <=$res_loop; $i++ ) {
		echo "entered into loop\n";
		
	$expire_unused_password_setup_select_sql = "SELECT * FROM `password_setup_modification_requests` WHERE `is_active_status` = :is_active_status LIMIT :res_records";
	echo "after select query\n";
	$expire_unused_password_setup_select_query = $dbcon->prepare($expire_unused_password_setup_select_sql);
	$expire_unused_password_setup_select_query->bindValue(":is_active_status",1);
    $expire_unused_password_setup_select_query->bindValue(":res_records",$res_records);	
	$expire_unused_password_setup_select_query->execute(); 
	
	if($expire_unused_password_setup_select_query->rowCount() > 0) {
		echo "after select query row count \n";
		$expire_unused_password_setup_select_query_result = $expire_unused_password_setup_select_query->fetchAll();
		foreach ($expire_unused_password_setup_select_query_result as $expire_unused_password_setup_select_query_result_row) {

			$request_expiry_datetime_epoch = $expire_unused_password_setup_select_query_result_row["request_expiry_datetime_epoch"];
			$password_setup_modification_request_id = $expire_unused_password_setup_select_query_result_row["password_setup_modification_request_id"];
   
			$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
		    /*echo $current_epoch;
			echo "\n";
			echo $request_expiry_datetime_epoch;
            var_dump($request_expiry_datetime_epoch);*/
			if($current_epoch > $request_expiry_datetime_epoch){
				echo "enter into if condition";
				$is_active_status = "3";
				$email_service_provider_mail_ref_message = "request-expired";
				
				$expire_unused_password_setup_update_sql = "UPDATE `password_setup_modification_requests` SET `is_active_status`=:is_active_status,`email_service_provider_mail_ref_message`=:email_service_provider_mail_ref_message,`last_updated_datetime`=:last_updated_date_time,`last_updated_datetime_epoch`=:last_updated_date_time_epoch,`last_updated_by_sm_memb_id`= :last_updated_by_sm_memb_id WHERE `password_setup_modification_request_id` = :password_setup_modification_request_id";
				$expire_unused_password_setup_update_query = $dbcon->prepare($expire_unused_password_setup_update_sql);
				$expire_unused_password_setup_update_query->bindValue(":is_active_status",$is_active_status);
				$expire_unused_password_setup_update_query->bindValue(":email_service_provider_mail_ref_message",$email_service_provider_mail_ref_message);
				$expire_unused_password_setup_update_query->bindValue(":password_setup_modification_request_id",$password_setup_modification_request_id);
				$expire_unused_password_setup_update_query->bindValue(":last_updated_date_time",$event_datetime);
				$expire_unused_password_setup_update_query->bindValue(":last_updated_date_time_epoch",$current_epoch);
				// when cron job processes the record then the value of this column is null since,there is no user id can be attributed to access this record			
				$expire_unused_password_setup_update_query->bindValue(":last_updated_by_sm_memb_id",null);	
				$expire_unused_password_setup_update_query->execute();
			}
		}//close of for ( $i = 1; $i <=$res_loop; $i++ ) {
	}
	
}//close of if ( $res_state == "ON" ) {
}

?>