<?php
if(defined('STDIN') ){
  //echo("Running from CLI");
}else{
  echo("Not Running from CLI");
  exit();
}

ini_set('memory_limit', '-1');
//PCRE Backtrack Limit is increased from 1000000 to 4000000, to handle supporting, bigger images in PDF Files using PDF to Text Library
ini_set('pcre.backtrack_limit', 4000000);
//if the directory is one directory above the current file's parent directory http://stackoverflow.com/a/2100763/811207
include(dirname(dirname(dirname(__FILE__))) . "/app/core/command-line-include.php");

$cron_number = "15";
$sel_filem_setting = $dbcon->prepare("SELECT * FROM `cron_file_details` WHERE `sno` = :sno");
$sel_filem_setting->bindParam(":sno",$cron_number);
$sel_filem_setting->execute(); 
$res_filem_setting = $sel_filem_setting->fetchAll(PDO::FETCH_ASSOC);
//print_r($res_filem_setting);
//exit;
foreach($res_filem_setting as $res_filem_setting_row)
{
	$res_state_filem = $res_filem_setting_row["cron_file_status_setting"];
	$res_records_filem = $res_filem_setting_row["cron_file_numb_record_limit"];
	$res_loop_filem = $res_filem_setting_row["cron_file_numb_loop_count_limit"];
	$res_sleep_min_filem = $res_filem_setting_row["cron_file_sleep_min_seconds_limit"];
	$res_sleep_max_filem = $res_filem_setting_row["cron_file_sleep_max_seconds_limit"];
	$res_sleep_interval_filem = $res_filem_setting_row["cron_file_sleep_interval"];
	
}




if ( $res_state_filem == "ON" ) {

			
	for ( $filem = 1; $filem <=$res_loop_filem; $filem++ ) {

function output_collected ($message) {
	if  (php_sapi_name( )  ==  'cli') {
		return $message;
	} else {
	    return nl2br($message);
	}	
}
		//code related to activity in particular cron file start
			//$candidate_quick_onboarding_data_details = get_candidate_quick_onboarding_data_based_on_is_processed_status_input("1", "1"); //is_processed value input:- 0: not-confirmed, 1: confirmed, 2: processed, 3: on hold, 4: finalized
			//echo "<pre>";
			//print_r($candidate_quick_onboarding_data_details);
			//exit;
			echo "enter to loop";
			$candidate_paste_resume_data_select_sql = "SELECT * FROM `candidate_rel_submitted_resume_texts` ";
			$candidate_paste_resume_data_select_sql_query = $dbcon->prepare($candidate_paste_resume_data_select_sql);
			//$candidate_paste_resume_data_select_sql_query->bindValue(":sm_user_status",0);	
			$candidate_paste_resume_data_select_sql_query->execute(); 
			
			if($candidate_paste_resume_data_select_sql_query->rowCount() > 0) {
				echo "after select query row count \n";
				$candidate_paste_resume_data_select_sql_query_result = $candidate_paste_resume_data_select_sql_query->fetchAll();
				foreach ($candidate_paste_resume_data_select_sql_query_result as $candidate_paste_resume_data_select_sql_query_result_row) {
			
					$company_id = $candidate_paste_resume_data_select_sql_query_result_row["company_id"];
					$profile_source_name = $candidate_paste_resume_data_select_sql_query_result_row["profile_source_name"];
					$resume_text = $candidate_paste_resume_data_select_sql_query_result_row["resume_text"];
				
						//if (($confirmed_profile_source_id != "") && ($confirmed_profile_source_name != "") && ($confirmed_member_type != "") && ($confirmed_candidate_firstname != "") && ($confirmed_candidate_lastname != "") && ($confirmed_departments != "") && ($confirmed_experience_level != "") && ($confirmed_skillsets != "") && ($confirmed_mobile_number != "") && ($confirmed_city != "") && ($confirmed_country != "") &&  ($confirmed_is_ama_invitation_required != "")) {
						if (($company_id != "") && ($profile_source_name != "") && ($resume_text != "") ) {	
							echo "required inputs are available\n";
							
								$file_extension = "pdf";
								//Create an internal File name (for the PDF File, that will be created, to hold the generate resume of the particular candidate profile)
								$generated_file_name = hash('sha256', uniqid(rand(), TRUE));
								$generated_file_name = $generated_file_name . "." . $file_extension;
								//echo "generated_file_name: " . $generated_file_name . "\n<br>";
								
								//Resume Saving Path and Filename
								$targetPath = $site_home_path . "uploaded-documents/";
								$targetFile =  $targetPath. $generated_file_name;  //from /app/core/main-config.php
								echo "targetFile: " . $targetFile . "\n";
								
								$mpdf = new \Mpdf\Mpdf([
								'margin_left' => 0,
								'margin_right' => 0,
								'margin_top' => 0,
								'margin_bottom' => 0,
								'margin_header' => 0,
								'margin_footer' => 0
								]);
								$mpdf->SetProtection(array('print'));
								$mpdf->SetTitle("ResumeCrab. - Resume");
								$mpdf->SetAuthor("ResumeCrab.");
								$mpdf->SetWatermarkText("ResumeCrab");
								$mpdf->showWatermarkText = true;
								$mpdf->watermark_font = 'DejaVuSansCondensed';
								$mpdf->watermarkTextAlpha = 0.1;
								$mpdf->SetDisplayMode('fullpage');
								$mpdf->WriteHTML('Resume');
								$mpdf->Write( 10, "Profile Source Name: $profile_source_name");
								$mpdf->Write( 6, "Resume: $resume_text");
								$mpdf->Ln( 20 );
							/* 	$mpdf->SetFont( 'Arial', 'B', 12 );
								$mpdf->Write( 6, "Profile Source Name:");
								$mpdf->Write( 6, "Profile Source Name:"); */
								//$pdf->Ln( 9 );
								/* $pdf->SetFont( 'Arial', '', 12 );
								$pdf->Write( 6, "generated by system" );
								$pdf->Ln( 9 ); */
						
								$mpdf->Output($targetFile,'F');
								
								
								
								/* $pdf->SetFont( 'Arial', 'I', 10 );
								$pdf->Write( 6, "This is a Dynamically created Resume, using internal Data for internal reference only @ResumeCrab." );
								$content = $pdf->Output($targetFile,'F'); */

						}
							
								
				}
						
			} else {
				echo "no records available";
			}
					
		}
	} 
		
?>