<?php
if(defined('STDIN') ){
  //echo("Running from CLI");
}else{
  echo("Not Running from CLI");
  exit();
}

ini_set('memory_limit', '-1');
//PCRE Backtrack Limit is increased from 1000000 to 4000000, to handle supporting, bigger images in PDF Files using PDF to Text Library
ini_set('pcre.backtrack_limit', 4000000);
//if the directory is one directory above the current file's parent directory http://stackoverflow.com/a/2100763/811207
include(dirname(dirname(dirname(__FILE__))) . "/app/core/command-line-include.php");

$cron_number = "1";
$sel_filem_setting = $dbcon->prepare("SELECT * FROM `cron_file_details` WHERE `sno` = :sno");
$sel_filem_setting->bindParam(":sno",$cron_number);
$sel_filem_setting->execute(); 
$res_filem_setting = $sel_filem_setting->fetchAll(PDO::FETCH_ASSOC);
//print_r($res_filem_setting);
//exit;
foreach($res_filem_setting as $res_filem_setting_row)
{
	$res_state_filem = $res_filem_setting_row["cron_file_status_setting"];
	$res_records_filem = $res_filem_setting_row["cron_file_numb_record_limit"];
	$res_loop_filem = $res_filem_setting_row["cron_file_numb_loop_count_limit"];
	$res_sleep_min_filem = $res_filem_setting_row["cron_file_sleep_min_seconds_limit"];
	$res_sleep_max_filem = $res_filem_setting_row["cron_file_sleep_max_seconds_limit"];
	$res_sleep_interval_filem = $res_filem_setting_row["cron_file_sleep_interval"];
	
}




if ( $res_state_filem == "ON" ) {

			
	for ( $filem = 1; $filem <=$res_loop_filem; $filem++ ) {

function output_collected ($message) {
	if  (php_sapi_name( )  ==  'cli') {
		return $message;
	} else {
	    return nl2br($message);
	}	
}
		
        //Get List of Resumes, those that are with non-processed status
		$get_resumes_sql = "SELECT `resume_id`, `resume_original_file_name`, `resume_generated_file_name`, `resume_document_extension`, `profile_source_id` FROM `resumes` WHERE `is_active_status` = :is_active_status LIMIT 0,".$res_records_filem;
		//echo $get_resumes_sql . "\n";
		$get_resumes_select_query = $dbcon->prepare($get_resumes_sql);
		$get_resumes_select_query->bindValue(":is_active_status", "0");
		$get_resumes_select_query->execute(); 
		$get_resumes_select_query_result = $get_resumes_select_query->fetchAll(PDO::FETCH_ASSOC);
	    //print_r($get_resumes_select_query_result);
		//exit;
		if(count($get_resumes_select_query_result) > 0){
		    
			foreach ($get_resumes_select_query_result as $get_resumes_select_query_row) {	
                $resume_id = $get_resumes_select_query_row["resume_id"];
				$resume_original_file_name = $get_resumes_select_query_row["resume_original_file_name"];				
				$resume_generated_file_name = $get_resumes_select_query_row["resume_generated_file_name"];
				$resume_document_extension = $get_resumes_select_query_row["resume_document_extension"];
				$profile_source_id = $get_resumes_select_query_row["profile_source_id"];
				 
				$resume_document_path = dirname(dirname(__FILE__)) . "/uploads/" . $resume_generated_file_name;
				if ($resume_document_extension == "pdf") {
					//The File Format of the being processed resume document is PDF
					//$txt = new PdfToText ($resume_document_path);
					$pdf = new PdfToText ( $resume_document_path, PdfToText::PDFOPT_DECODE_IMAGE_DATA ) ;
					$image_count_in_pdf_file = count ($pdf->Images) ;
					
					if  ($image_count_in_pdf_file) {
						//echo "One or more images are found in the sample file " . $resume_document_path;
						$txt = $pdf->Text;
					} else {
						//echo "No image was found in sample file " . $resume_document_path;
						$txt = $pdf->Text;
					}//close of else of if  ($image_count_in_pdf_file) {
                } else if ($resume_document_extension == "docx") {
					//The File Format of the being processed resume document is DOCX
					$docObj = new Doc2Txt($resume_document_path);
					$txt = $docObj->convertToText();
				}//close of elseif of if ($resume_document_extension == "pdf") {
				echo "txt: " . $txt . "\n";
				$txt_strlen = mb_strlen($txt);
				echo "txt_strlen: " . $txt_strlen . "\n";
				if ($txt_strlen > 0) {
					//echo "The string length is greater than zero.\n";
					//Convert NL2Br				
					$output_collected = output_collected($txt);
					//echo $output_collected . "\n";
					//exit;
					//Process Text and Collect Email IDs
					$collected_email_ids = get_email_ids_from_html($output_collected);
					//echo "<pre>";
					//print_r($collected_email_ids);
					//echo "</pre>";
					//exit;
					//Find Count of obtained unique email ids
					$collected_email_ids_count = count($collected_email_ids);
					echo "<pre>";
					print_r($collected_email_ids);
					//exit;
					//Check if more than one unique email id is obtained
					if ($collected_email_ids_count == 1) {
						//Only one Email ID is obtained
						$final_email_id = $collected_email_ids[0];
					} else if ($collected_email_ids_count > 1) {
						//More than One Email ID is obtained then, sort them based on length of value of the array
						function sort_based_on_length($a,$b){
							return strlen($b)-strlen($a);
						}

						usort($collected_email_ids,'sort_based_on_length');
						
						//Choosing the Email ID with Higher Length as final EMail ID
						$final_email_id = $collected_email_ids[0];
					}
					//print_r($final_email_id);
					//var_dump($final_email_id);
					//exit;
					//Extract Local Part of Email ID
					//$final_email_id_exploded = explode("@", $final_email_id);
					//$email_id_localpart = $final_email_id_exploded[0];
					
					
					//Enumerate First Name and Last Name from the Email ID Username
					
					//Remove numbers from Email
					//echo $email_id_localpart;
					
					$firstname = "";
					$lastname = "";
					
					//Last attempted Time, in terms of data extraction of the resume, as per Indian Standard Time
					$last_attempted_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Asia/Kolkata");
					
					if ($final_email_id != "") {
						//Dup check, if this collected email id is already mapped to this internal url id
						$collected_email_id_int_url_map_result = check_collected_email($final_email_id);
						
						//print_r($collected_email_id_int_url_map_result);
						//exit;
						
						if ($collected_email_id_int_url_map_result == "") {
							//Do Insert Query
								
							//SQL Queries
							$resume_extracted_info_sql = "INSERT INTO `extracted_info`(`resume_id`, `profile_source_id`, `collected_email_address`, `firstname`, `lastname`) VALUES (:resume_id,:profile_source_id,:collected_email_address,:firstname,:lastname)";
							
							//array of values for inserting data in to extracted_info db table
							$resume_extracted_info_values_array = array(":resume_id" => $resume_id, ":profile_source_id" => $profile_source_id, ":collected_email_address" => $final_email_id, ":firstname" => $firstname, ":lastname" => $lastname);
							$last_resume_extract_info_insert_id = insert_query_get_last_insert_id($resume_extracted_info_sql, $resume_extracted_info_values_array);
							
							echo "last_resume_extract_info_insert_id: " . $last_resume_extract_info_insert_id . "\n";
							if (isset($last_resume_extract_info_insert_id) && ($last_resume_extract_info_insert_id != "")) {
								
								
								//Do Update Query
							
								$update_resume_status_sql = "UPDATE `resumes` SET `last_attempted_datetime`=:last_attempted_datetime,`last_attempted_datetime_epoch`=:last_attempted_datetime_epoch,`is_active_status`=:is_active_status WHERE `resume_id`=:resume_id";
								
								//array of values for updating resumes db table
								$update_resume_status_values_array = array(":last_attempted_datetime" => $last_attempted_datetime, ":last_attempted_datetime_epoch" => $current_epoch, ":is_active_status" => "1", ":resume_id" => $resume_id);
								
								$update_status = update_query_based_on_id($update_resume_status_sql, $update_resume_status_values_array);
							}//close of if (isset($last_resume_extract_info_insert_id) && ($last_resume_extract_info_insert_id != "")) {
							
						} else {
							echo "This collected email id already exists in the database. \n";
							//Do Update Query
							
							$update_resume_status_sql = "UPDATE `resumes` SET `last_attempted_datetime`=:last_attempted_datetime,`last_attempted_datetime_epoch`=:last_attempted_datetime_epoch,`is_active_status`=:is_active_status WHERE `resume_id`=:resume_id";
							
							//array of values for updating resumes db table
							$update_resume_status_values_array = array(":last_attempted_datetime" => $last_attempted_datetime, ":last_attempted_datetime_epoch" => $current_epoch, ":is_active_status" => "3", ":resume_id" => $resume_id);
							
							$update_status = update_query_based_on_id($update_resume_status_sql, $update_resume_status_values_array);
						}//close of else of if ($collected_email_id_int_url_map_result == "") {
						
					} else {
						echo "Email ID cannot be empty\n";
						//Do Update Query
							
						$update_resume_status_sql = "UPDATE `resumes` SET `last_attempted_datetime`=:last_attempted_datetime,`last_attempted_datetime_epoch`=:last_attempted_datetime_epoch,`is_active_status`=:is_active_status WHERE `resume_id`=:resume_id";
						
						//array of values for updating resumes db table
						$update_resume_status_values_array = array(":last_attempted_datetime" => $last_attempted_datetime, ":last_attempted_datetime_epoch" => $current_epoch, ":is_active_status" => "4", ":resume_id" => $resume_id);
						
						$update_status = update_query_based_on_id($update_resume_status_sql, $update_resume_status_values_array);
					}//close of else of if ($final_email_id != "") {
					
				} else {
					echo "The string length is equal to zero. This means, Email ID is not found.\n";
					//Do Update Query
							
					$update_resume_status_sql = "UPDATE `resumes` SET `last_attempted_datetime`=:last_attempted_datetime,`last_attempted_datetime_epoch`=:last_attempted_datetime_epoch,`is_active_status`=:is_active_status WHERE `resume_id`=:resume_id";
					
					//array of values for updating resumes db table
					$update_resume_status_values_array = array(":last_attempted_datetime" => $last_attempted_datetime, ":last_attempted_datetime_epoch" => $current_epoch, ":is_active_status" => "4", ":resume_id" => $resume_id);
					
					$update_status = update_query_based_on_id($update_resume_status_sql, $update_resume_status_values_array);
				}//close of else of if ($txt_strlen > 0) {
				
	
				
				
				//exit;
			}//close of foreach ($get_resumes_select_query_result as $get_resumes_select_query_row) {	
				
		}//close of if(count($get_resumes_select_query_result) > 0){					

		//code related to activity in particular cron file end
		sleep($res_sleep_filem);		
		
	}//close of for ( $filem = 1; $filem <=$res_loop_filem; $filem++ )
		
}//close of if ( $res_state_filem == "ON" ) {
?>