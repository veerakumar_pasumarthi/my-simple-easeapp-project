<?php
if(defined('STDIN') ){
  //echo("Running from CLI");
}else{
  echo("Not Running from CLI");
  exit();
}
/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 */
//if the directory is one directory above the current file's parent directory http://stackoverflow.com/a/2100763/811207
include(dirname(dirname(dirname(__FILE__))) . "/app/core/command-line-include.php");

include 'cron-job-related-email-template.php';

echo "before cron job details query\n";
$cron_number = "7";
$sel_cron_number_setting = $dbcon->prepare("SELECT * FROM `cron_file_details` WHERE `sno` = :sno");
$sel_cron_number_setting->bindParam(":sno",$cron_number);
$sel_cron_number_setting->execute(); 
$res_cron_number_setting = $sel_cron_number_setting->fetchAll(PDO::FETCH_ASSOC);
print_r($res_cron_number_setting);
//exit;
foreach($res_cron_number_setting as $res_cron_number_setting_row)
{
	$res_state = $res_cron_number_setting_row["cron_file_status_setting"];
	$res_records = $res_cron_number_setting_row["cron_file_numb_record_limit"];
	$res_loop = $res_cron_number_setting_row["cron_file_numb_loop_count_limit"];
	$res_sleep_min = $res_cron_number_setting_row["cron_file_sleep_min_seconds_limit"];
	$res_sleep_max = $res_cron_number_setting_row["cron_file_sleep_max_seconds_limit"];
	$res_sleep_interval = $res_cron_number_setting_row["cron_file_sleep_interval"];
	
}


if ( $res_state == "ON" ) {

	echo "inside on condition \n";		
	  for ( $i = 1; $i <=$res_loop; $i++ ) {
		echo "entered into loop\n";
				
		$jsl_recruiter_mapping_email_notifications_details_get_sql = "SELECT * FROM `jsl_recruiter_mapping_email_notifications` WHERE `event_status` = :event_status";
		echo "after select query\n";
		$jsl_recruiter_mapping_email_notifications_details_select_query = $dbcon->prepare($jsl_recruiter_mapping_email_notifications_details_get_sql);
		$jsl_recruiter_mapping_email_notifications_details_select_query->bindValue(":event_status","0");
				
		$jsl_recruiter_mapping_email_notifications_details_select_query->execute(); 
		echo "before select query row count \n\n";
		
			if($jsl_recruiter_mapping_email_notifications_details_select_query->rowCount() > 0) {
				echo "after select query row count \n";
				$jsl_recruiter_mapping_email_notifications_details_select_query_result = $jsl_recruiter_mapping_email_notifications_details_select_query->fetchAll();
				foreach ($jsl_recruiter_mapping_email_notifications_details_select_query_result as $jsl_recruiter_mapping_email_notifications_details_select_query_result_row) {
					
					$jsl_recruiter_mapping_email_notification_id = $jsl_recruiter_mapping_email_notifications_details_select_query_result_row["jsl_recruiter_mapping_email_notification_id"];
					$jsl_info_id = $jsl_recruiter_mapping_email_notifications_details_select_query_result_row["jsl_info_id"];
					$recruiter_sm_memb_id = $jsl_recruiter_mapping_email_notifications_details_select_query_result_row["recruiter_sm_memb_id"];
					$job_id = $jsl_recruiter_mapping_email_notifications_details_select_query_result_row["job_id"];
					
					echo "before getting user details \n";	
				
						
						$jsl_details_result = jsl_details($jsl_info_id); 
						$jsl_title = $jsl_details_result["jsl_title"];
						$job_details_result = job_details($job_id);
						$job_title = $job_details_result["job_title"];
							
						$user_details = user_basic_details_check_based_on_user_id($recruiter_sm_memb_id);
									
							//Get Candidate's Salutation, Firstname, Middlename and Lastname
							if ($user_details != "") {
								$sm_salutation = $user_details["sm_salutation"];
								$sm_firstname = $user_details["sm_firstname"];
								$sm_middlename = $user_details["sm_middlename"];
								$sm_lastname = $user_details["sm_lastname"];
								$sm_user_type = $user_details["sm_user_type"];
								$sm_admin_level = $user_details["sm_admin_level"];
								$sm_user_role = $user_details["sm_user_role"];
								$company_id = $user_details["company_id"];
								$sm_email = $user_details["sm_email"];
								
							} else {
								$sm_salutation = "";
								$sm_firstname = "";
								$sm_middlename = "";
								$sm_lastname = "";
								$sm_user_type = "";
								$sm_admin_level = "";
								$sm_user_role = "";
								$company_id = "";
								$sm_email = "";
							}//close of else of if ($candidate_details != "") {
							
							if (($sm_firstname != "") && ($sm_middlename != "") && ($sm_lastname != "")) {
								$user_fullname = $sm_firstname . " " . $sm_middlename . " " . $sm_lastname;
								$greeting_user_summary = "Dear " . $user_fullname . ",";
							} else if (($sm_firstname != "") && ($sm_middlename != "") && ($sm_lastname == "")) {
								$user_fullname = $sm_firstname . " " . $sm_middlename;
								$greeting_user_summary = "Dear " . $user_fullname . ",";
							} else if (($sm_firstname != "") && ($sm_middlename == "") && ($sm_lastname == "")) {
								$user_fullname = $sm_firstname;
								$greeting_user_summary = "Dear " . $user_fullname . ",";
							} else if (($sm_firstname != "") && ($sm_middlename == "") && ($sm_lastname != "")) {
								$user_fullname = $sm_firstname . " " . $sm_lastname;
								$greeting_user_summary = "Dear " . $user_fullname . ",";
							} else if (($sm_firstname == "") && ($sm_middlename != "") && ($sm_lastname != "")) {
								$user_fullname = $sm_middlename . " " . $sm_lastname;
								$greeting_user_summary = "Dear " . $user_fullname . ",";
							} else if (($sm_firstname == "") && ($sm_middlename == "") && ($sm_lastname != "")) {
								$user_fullname = $sm_lastname;
								$greeting_user_summary = "Dear " . $user_fullname . ",";
							} else {
								$user_fullname = "";
								$greeting_user_summary = "Dear,";
							}//close of else of if (($candidate_firstname != "") && ($sm_middlename != "") && ($candidate_lastname != "")) {
							
							$company_name = "";
							if (!is_null($company_id)) {
									
								$candidate_company_details = get_company_details_based_on_company_id($company_id);
								if ($candidate_company_details != "") {
									
									$company_name = $candidate_company_details["company_name"];
									$company_support_email = $candidate_company_details["company_support_email"];
									
									
									
								} else {
								
									
									$company_name = "";
									$company_support_email = "";
									
								}//close of else of if ($candidate_company_details != "") {	
									
							}//close of if (!is_null($company_id)) {
							
							//Construct URL
							/*
							
							http://127.0.0.1:8080/LAYOUT-2/STANDARD/#!/account/activate/user-act-code/yd665r5er65r56edd6565te65e56e56e56ter6er5775e57


							Reset Password Link:

							http://127.0.0.1:8080/LAYOUT-2/STANDARD/#!/account/reset-password/user-pass-reset-code/yd665r5er65r56edd6565te65e56e56e56ter6er5775e57dgdfg4t45gdfgfd

							*/
							$constructed_url ="";
							

							$constructed_message_plain_text = "You have been assigned as an evaluator  for $jsl_title with respect to $job_title. 
						
							" . "Please click the link below for further actions.";
							
							$constructed_message_html = "<p> You have been assigned as an evaluator  for $jsl_title with respect to $job_title. </p>";
							
							//$constructed_url = $frontend_site_url_project_main_with_path . "jsvnavigate/" . $job_id;

							$constructed_url = $frontend_site_url_project_main."jsv/company/".$company_id."/job/". $job_id."/single-view";
							echo "constructed_url: " . $constructed_url . "\n\n";
							
							$account_activation_password_setup_reset_link_without_html = $constructed_url . "\n\n";
							
							//echo "account_activation_password_setup_reset_link_without_html: " . $account_activation_password_setup_reset_link_without_html . "\n\n";
							
							$account_activation_password_setup_reset_link_with_html = "<p><a href='" . $constructed_url . "'>" . $constructed_url . "</a><br><br></p>";
							
							//echo "account_activation_password_setup_reset_link_with_html: " . $account_activation_password_setup_reset_link_with_html . "\n\n";
							
							//Send Email

							$link_text = "Please click Here below for further actions" ;

		$button_content = "Job Application";
							
							$plaintext_input = $greeting_user_summary . "
							
							" . $constructed_message_plain_text . "
							
							" .  
							

							"\n\n" .
							
							$account_activation_password_setup_reset_link_without_html .

							"\n" .
							"Thank you \n
							Admin Team \n
							" . $company_name . "
							For Support, Please Contact: " . $company_support_email . "
							________________________________________________________________________

							";

						/*	$html_message_input = "<div><p>" . $greeting_user_summary . "<br><br>

							" . $constructed_message_html

							. $account_activation_password_setup_reset_link_with_html .
							 
							"
							
							<p> </p>
							<p>Thank you<br>
							Admin Team <br>
							" . $company_name . "<br>" .
							"<u>For Support, Please Contact:</u> " . $company_support_email . "<br>" .
							"</p>
							<p>________________________________________________________________________</p>

							</div>"; */

							$html_message_input = template($greeting_user_summary ,$link_text, $constructed_url , $company_name , $button_content , $constructed_message_html);

							//echo "plaintext_input: " . $plaintext_input . "<br>";
							//echo "html_message_input: " . $html_message_input . "<br>";
							//exit;
							
							
						if (filter_var($sm_email, FILTER_VALIDATE_EMAIL) == true) {
							
							$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
						
							//send_email_using_smtp_thr_phpmailer("notifications@vdmail.securitywonks.net","Innovative Solutions", "notifications@vdmail.securitywonks.net","webmaster@securitywonks.org","Raghu sir","To check email","hello welcome","<p> hello welcome</p>");	
							//exit;
							//commented on 10-02-2019 18:10 $send_email_using_smtp_thr_phpmailer_result = send_email_using_smtp_thr_phpmailer($activation_email_sender_email, $activation_email_sender_name, $activation_email_sender_email, $recipient_email_address,null,$activation_email_subject, $plaintext_input, $html_message_input);
								
								/* $send_email_using_smtp_thr_phpmailer_result = send_email_using_smtp_thr_phpmailer($activation_email_sender_email, $activation_email_sender_name, $activation_email_sender_email, $sm_email,null,$activation_email_subject, $plaintext_input, $html_message_input); */
								$to_email = array();
								$to_email[] = $sm_email;
								$email_subject = "Job Screening Level Assignment notifications";
								$from_email_address = "notifications@resumecrab.com";
								$from_person_name = "Resumecrab Notifications";
								$sender_email_address = "notifications@resumecrab.com";
								$sender_person_name = "Resumecrab Notifications";
								$replyto_email_address = "notifications@resumecrab.com";
								$replyto_person_name = "Resumecrab Notifications";
								
								$send_email_using_elastic_email_result = send_email_using_elastic_email($from_email_address,$from_person_name,$sender_email_address, $sender_person_name, $replyto_person_name,$replyto_email_address, $to_email,$email_subject, $plaintext_input, $html_message_input);
							
							
							echo "after mail function";
							//send_email_using_smtp_thr_phpmailer($from_email_input, $sender_name_input, $reply_to_email_input, $to_email_input, $receiver_name_input, $subject_input, $plaintext_input, $html_message_input);	
						
							
							if($send_email_using_elastic_email_result == true){
								
								$new_email_sending_status = "1";
								$email_service_provider_mail_ref_message = "message-sent";
								$sent_datetime = $event_datetime;
								$sent_datetime_epoch = $current_epoch;
								
								
							} else {
								$new_email_sending_status = "4";
								$email_service_provider_mail_ref_message = null;
								$sent_datetime = null;
								$sent_datetime_epoch = null;
							}
							
							//echo $new_email_sending_status;
								
							$jsl_recruiter_mapping_email_notification_update_status_sql = "UPDATE `jsl_recruiter_mapping_email_notifications` SET `event_status`=:event_status,`service_provider_email_ref_message` = :service_provider_email_ref_message WHERE `jsl_recruiter_mapping_email_notification_id`=:jsl_recruiter_mapping_email_notification_id";
							//echo "after update query";
							$jsl_recruiter_mapping_email_notification_update_status_query = $dbcon->prepare($jsl_recruiter_mapping_email_notification_update_status_sql);
							$jsl_recruiter_mapping_email_notification_update_status_query->bindValue(":event_status",$new_email_sending_status);
							$jsl_recruiter_mapping_email_notification_update_status_query->bindValue(":jsl_recruiter_mapping_email_notification_id",$jsl_recruiter_mapping_email_notification_id);
							$jsl_recruiter_mapping_email_notification_update_status_query->
							bindValue(":service_provider_email_ref_message",$email_service_provider_mail_ref_message);
							
							if($jsl_recruiter_mapping_email_notification_update_status_query->execute()){
								
								echo "Email Sent Successfully";
								
							} else {
								
								echo "updation not done";
								
							}
						} else {
							echo "Recipient's Email Address is Invalid, as per Email Format Validation Checks \n\n";
						}//close of else of if (filter_var($recipient_email_address, FILTER_VALIDATE_EMAIL) == true) {
				}		
			} else {
				echo "no records found";
			}//close of for ( $i = 1; $i <=$res_loop; $i++ ) {
	}
	
}//close of if ( $res_state == "ON" ) {
		
								 
function get_assigned_user_details_based_on_assigned_user_sm_memb_id($assigned_user_sm_memb_id) {

	global $dbcon;
	$constructed_array = array();
	$user_details_get_sql = "SELECT * FROM `site_members` WHERE `sm_memb_id`=:sm_memb_id";
					
	$user_details_get_select_query = $dbcon->prepare($user_details_get_sql);
	$user_details_get_select_query->bindValue(":sm_memb_id",$assigned_user_sm_memb_id);
	$user_details_get_select_query->execute();
					
	if($user_details_get_select_query->rowCount() > 0) {
	
		$user_details_get_select_query_result = $user_details_get_select_query->fetch();
	
		return $user_details_get_select_query_result;
	}
	return $constructed_array;
}

						
function jsl_details($jsl_info_id) {

	global $dbcon;
	$constructed_array = array();
	$user_details_get_sql = "SELECT * FROM `jsl_info` WHERE `jsl_info_id`=:jsl_info_id";
					
	$user_details_get_select_query = $dbcon->prepare($user_details_get_sql);
	$user_details_get_select_query->bindValue(":jsl_info_id",$jsl_info_id);
	$user_details_get_select_query->execute();
					
	if($user_details_get_select_query->rowCount() > 0) {
	
		$user_details_get_select_query_result = $user_details_get_select_query->fetch();
	
		return $user_details_get_select_query_result;
	}
	return $constructed_array;
}

function job_details($job_id) {

	global $dbcon;
	$constructed_array = array();
	$user_details_get_sql = "SELECT * FROM `jobs` WHERE `job_id`=:job_id";
					
	$user_details_get_select_query = $dbcon->prepare($user_details_get_sql);
	$user_details_get_select_query->bindValue(":job_id",$job_id);
	$user_details_get_select_query->execute();
					
	if($user_details_get_select_query->rowCount() > 0) {
	
		$user_details_get_select_query_result = $user_details_get_select_query->fetch();
	
		return $user_details_get_select_query_result;
	}
	return $constructed_array;
}
?>			 
								
							