<?php
if(defined('STDIN') ){
  //echo("Running from CLI");
}else{
  echo("Not Running from CLI");
  exit();
}
/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 */
//if the directory is one directory above the current file's parent directory http://stackoverflow.com/a/2100763/811207
include(dirname(dirname(dirname(__FILE__))) . "/app/core/command-line-include.php");
echo "before cron job details query\n";
$cron_number = "12";
$sel_cron_number_setting = $dbcon->prepare("SELECT * FROM `cron_file_details` WHERE `sno` = :sno");
$sel_cron_number_setting->bindParam(":sno",$cron_number);
$sel_cron_number_setting->execute(); 
$res_cron_number_setting = $sel_cron_number_setting->fetchAll(PDO::FETCH_ASSOC);
print_r($res_cron_number_setting);
//exit;
foreach($res_cron_number_setting as $res_cron_number_setting_row)
{
	$res_state = $res_cron_number_setting_row["cron_file_status_setting"];
	$res_records = $res_cron_number_setting_row["cron_file_numb_record_limit"];
	$res_loop = $res_cron_number_setting_row["cron_file_numb_loop_count_limit"];
	$res_sleep_min = $res_cron_number_setting_row["cron_file_sleep_min_seconds_limit"];
	$res_sleep_max = $res_cron_number_setting_row["cron_file_sleep_max_seconds_limit"];
	$res_sleep_interval = $res_cron_number_setting_row["cron_file_sleep_interval"];
	
}
if ( $res_state == "ON" ) {

	echo "inside on condition \n";		
	for ( $i = 1; $i <=$res_loop; $i++ ) {
		echo "entered into loop\n";
		
		$elastic_search_onboarding_data_select_sql = "SELECT * FROM `site_members` sm JOIN `candidate_rel_uploaded_resumes` crur ON sm.sm_memb_id = crur.sm_memb_id JOIN `resume_extracted_info` rei ON sm.sm_memb_id = rei.sm_memb_id WHERE crur.resume_processing_status = '1' AND crur.elasticsearch_content_upload_status = '0' LIMIT :res_records";
		echo "after select query\n";
		
		$elastic_search_onboarding_data_select_query = $dbcon->prepare($elastic_search_onboarding_data_select_sql);
		$elastic_search_onboarding_data_select_query->bindValue(":res_records",$res_records);
		$elastic_search_onboarding_data_select_query->execute(); 
	
		if($elastic_search_onboarding_data_select_query->rowCount() > 0) {
			echo "after select query row count \n";
			$elastic_search_onboarding_data_select_query_result = $elastic_search_onboarding_data_select_query->fetchAll();
			
			foreach ($elastic_search_onboarding_data_select_query_result as $elastic_search_onboarding_data_select_query_result_row) {
				
				$constructed_array = array();
				$constructed_array["sm_memb_id"] = $elastic_search_onboarding_data_select_query_result_row["sm_memb_id"];
				$constructed_array["sm_email"] = $elastic_search_onboarding_data_select_query_result_row["sm_email"];
				$constructed_array["sm_mobile"] = $elastic_search_onboarding_data_select_query_result_row["sm_mobile"];
				$constructed_array["sm_alternate_email"] = $elastic_search_onboarding_data_select_query_result_row["sm_alternate_email"];
				$constructed_array["sm_salutation"] = $elastic_search_onboarding_data_select_query_result_row["sm_salutation"];
				$constructed_array["sm_firstname"] = $elastic_search_onboarding_data_select_query_result_row["sm_firstname"];
				$constructed_array["sm_middlename"] = $elastic_search_onboarding_data_select_query_result_row["sm_middlename"];
				$constructed_array["sm_lastname"] = $elastic_search_onboarding_data_select_query_result_row["sm_lastname"];
				$constructed_array["sm_city"] = $elastic_search_onboarding_data_select_query_result_row["sm_city"];
				$constructed_array["sm_state"] = $elastic_search_onboarding_data_select_query_result_row["sm_state"];
				$constructed_array["sm_country"] = $elastic_search_onboarding_data_select_query_result_row["sm_country"];
				$constructed_array["company_id"] = $elastic_search_onboarding_data_select_query_result_row["company_id"];
				$constructed_array["sm_job_experience_level"] = $elastic_search_onboarding_data_select_query_result_row["sm_job_experience_level"];
				$constructed_array["crur_id"] = $elastic_search_onboarding_data_select_query_result_row["crur_id"];
				$constructed_array["original_filename"] = $elastic_search_onboarding_data_select_query_result_row["original_filename"];
				$constructed_array["resume_extracted_info_id"] = $elastic_search_onboarding_data_select_query_result_row["resume_extracted_info_id"];
				$constructed_array["resume_text"] = $elastic_search_onboarding_data_select_query_result_row["resume_text"];
                 
				$crur_id = $constructed_array["crur_id"];
				 
				
				//Check if index exists
				if ($elasticsearch_db_existence_status_result === true) {
					//index (as noun) Exists
					
					//Search a Document, to find Duplicates (Exact Match)
					$document_dup_check_params = [
						'index' => $elasticsearch_index_name,
						'type' => $elasticsearch_index_type,
						'body' => [
							'query' => [
								'match' => [
									'crur_id' => $crur_id
								]
							]
						]
					];

					$document_dup_check_response = $elasticsearch_client->search($document_dup_check_params);
					echo "<pre>";
					print_r($document_dup_check_response); 

					$duplicate_records_count = $document_dup_check_response["hits"]["total"];

					//echo "duplicate_records_count: " . $duplicate_records_count;
					if ($duplicate_records_count == 0) {
						//Do Index the Document (Insert)
						//print_r($constructed_array);
						//Index a Document (Insert a Document) 
						$elasticsearchDocumentIndexParams = [
							'index' => $elasticsearch_index_name,
							'type' => $elasticsearch_index_type,
							//'id' => 'my_id',
							'body' => $constructed_array
						
						];	
						
						$elasticsearch_document_index_response = $elasticsearch_client->index($elasticsearchDocumentIndexParams);
						print_r($elasticsearch_document_index_response);
						echo "document created successfully";
						
						if($elasticsearch_document_index_response["_id"] != "" && $elasticsearch_document_index_response["result"] == 'created') {
						
							$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
					
							$elasticsearch_content_upload_status = "1";
							
							$elasticsearch_content_upload_status_update_sql = "UPDATE `candidate_rel_uploaded_resumes` SET `elasticsearch_content_upload_status`=:elasticsearch_content_upload_status,`elasticsearch_document_id`=:elasticsearch_document_id,`last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch WHERE `crur_id` = :crur_id";
							$elasticsearch_content_upload_status_update_query = $dbcon->prepare($elasticsearch_content_upload_status_update_sql);
							$elasticsearch_content_upload_status_update_query->bindValue(":elasticsearch_content_upload_status",$elasticsearch_content_upload_status);
							$elasticsearch_content_upload_status_update_query->bindValue(":crur_id",$crur_id);
							$elasticsearch_content_upload_status_update_query->bindValue(":elasticsearch_document_id",$elasticsearch_document_index_response['_id']);
							$elasticsearch_content_upload_status_update_query->bindValue(":last_updated_date_time",$event_datetime);
							$elasticsearch_content_upload_status_update_query->bindValue(":last_updated_date_time_epoch",$current_epoch);	
							$elasticsearch_content_upload_status_update_query->execute();
						
						}
					}//close of if ($duplicate_records_count == 0) {
					
					
				}
			}
		}
     
	
	}//close of for ( $i = 1; $i <=$res_loop; $i++ ) {
	
}//close of if ( $res_state == "ON" ) {


?>