<?php
if(defined('STDIN') ){
  //echo("Running from CLI");
}else{
  echo("Not Running from CLI");
  exit();
}
/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 */
//if the directory is one directory above the current file's parent directory http://stackoverflow.com/a/2100763/811207
include(dirname(dirname(dirname(__FILE__))) . "/app/core/command-line-include.php");

include 'cron-job-related-email-template.php';

echo "before cron job details query\n";
$cron_number = "19";
$sel_cron_number_setting = $dbcon->prepare("SELECT * FROM `cron_file_details` WHERE `sno` = :sno");
$sel_cron_number_setting->bindParam(":sno",$cron_number);
$sel_cron_number_setting->execute(); 
$res_cron_number_setting = $sel_cron_number_setting->fetchAll(PDO::FETCH_ASSOC);
print_r($res_cron_number_setting);
//exit;
foreach($res_cron_number_setting as $res_cron_number_setting_row)
{
	$res_state = $res_cron_number_setting_row["cron_file_status_setting"];
	$res_records = $res_cron_number_setting_row["cron_file_numb_record_limit"];
	$res_loop = $res_cron_number_setting_row["cron_file_numb_loop_count_limit"];
	$res_sleep_min = $res_cron_number_setting_row["cron_file_sleep_min_seconds_limit"];
	$res_sleep_max = $res_cron_number_setting_row["cron_file_sleep_max_seconds_limit"];
	$res_sleep_interval = $res_cron_number_setting_row["cron_file_sleep_interval"];
	
}


if ( $res_state == "ON" ) {

	echo "inside on condition \n";		
	  for ( $i = 1; $i <=$res_loop; $i++ ) {
		echo "entered into loop\n";
		$job_management_mail_case_update_sql = "UPDATE `job_management_assignees` jma JOIN jobs j ON j.job_id = jma.job_id SET jma.email_notification_status = '5' WHERE j.job_rel_email_notification_status = '0'";
		$job_management_mail_case_update_query = $dbcon->prepare($job_management_mail_case_update_sql);
	    if($job_management_mail_case_update_query->execute()) {
			echo "override case updated\n";
        }		
		
				
		/* $job_details_get_sql = "SELECT * FROM `jobs` WHERE `job_rel_email_notification_status` != :job_rel_email_notification_status";
		echo "after select query\n";
		$job_details_select_query = $dbcon->prepare($job_details_get_sql);
		$job_details_select_query->bindValue(":job_rel_email_notification_status","0");
				
		$job_details_select_query->execute(); 
		echo "before select query row count \n\n"; */
		
			/* if($job_details_select_query->rowCount() > 0) {
				echo "after select query row count \n";
				$job_details_select_query_result = $job_details_select_query->fetchAll();
				foreach ($job_details_select_query_result as $job_details_select_query_result_row) { */
					//echo "before job id \n\n";
					
					
					$job_aasignment_email_details_result = job_assignment_email_details();								
					if(count($job_aasignment_email_details_result)>0){
						
						echo "enter into count check\n";
			 
			 
						foreach($job_aasignment_email_details_result as $job_aasignment_email_details_result_row) {
						 
							/* $assigned_user_sm_memb_id = "";
							$assigned_job_specific_role = ""; */
							 $assigned_user_sm_memb_id = $job_aasignment_email_details_result_row["assigned_user_sm_memb_id"];
							$assigned_job_specific_role = $job_aasignment_email_details_result_row["assigned_job_specific_role"];
							
							$job_id = $job_details_select_query_result_row["job_id"];
					
					
							echo "before getting user details .$job_id.\n";	
							$job_details_result = get_job_details($job_id);
							$job_title = $job_details_result["job_title"];
							$job_summary = $job_details_result["job_summary"];
							$resume_submission_end_date = $job_details_result["resume_submission_end_date"];
							$company_id = $job_details_result["company_id"];
							echo "company_id_from_jobs . $company_id\n";
							$company_client_id = $job_details_result["company_client_id"];	
							$company_details_result = get_company_details($company_id);
							$company_name = $company_details_result ["company_name"];
							echo "company_id_from_jobs . $company_name\n";
							$company_client_details_result = get_company_client_details($company_client_id);
							$client_company_name = $company_client_details_result["client_company_name"];
							
							echo "assigned_user_sm_memb_id.$assigned_user_sm_memb_id\n";
							echo "assigned_job_specific_role.$assigned_job_specific_role\n";
							
							/* 	
							
							} else {
								$assigned_user_sm_memb_id = "";
								$assigned_job_specific_role = "";
							}  */
						//echo "before getting user details .$assigned_user_sm_memb_id.\n";
						//echo "before getting user details .$assigned_job_specific_role.\n";	 
						if($assigned_job_specific_role == "1")
						{
							$assigned_job_specific_role ="Primary Sales";
						}
						if($assigned_job_specific_role == "2")
						{
							$assigned_job_specific_role ="Sales";
						}
						if($assigned_job_specific_role == "3")
						{
							$assigned_job_specific_role ="Primary Recruiter";
						}
						if($assigned_job_specific_role == "4")
						{
							$assigned_job_specific_role ="Recruiter";
						} 
						
						
						
						$assigned_user_details = get_assigned_user_details_based_on_assigned_user_sm_memb_id($assigned_user_sm_memb_id);
						if (count($assigned_user_details) > 0){
									$sm_email =	$assigned_user_details["sm_email"];
									$sm_firstname = $assigned_user_details["sm_firstname"];
									$sm_middlename = $assigned_user_details["sm_middlename"];
									$sm_lastname = $assigned_user_details["sm_lastname"];
						} else {
							$sm_email =	"";
							$sm_firstname = "";
							$sm_middlename = "";
							$sm_lastname = "";
						}
						
							
							
							if (($sm_firstname != "") && ($sm_middlename != "") && ($sm_lastname != "")) {
							$candidate_fullname = $sm_firstname . " " . $sm_middlename . " " . $sm_lastname;
							$greeting_user_summary = "Dear " . $candidate_fullname . ",";
						} else if (($sm_firstname != "") && ($sm_middlename != "") && ($sm_lastname == "")) {
							$candidate_fullname = $sm_firstname . " " . $sm_middlename;
							$greeting_user_summary = "Dear " . $candidate_fullname . ",";
						} else if (($sm_firstname != "") && ($sm_middlename == "") && ($sm_lastname == "")) {
							$candidate_fullname = $sm_firstname;
							$greeting_user_summary = "Dear " . $candidate_fullname . ",";
						} else if (($sm_firstname != "") && ($sm_middlename == "") && ($sm_lastname != "")) {
							$candidate_fullname = $sm_firstname . " " . $sm_lastname;
							$greeting_user_summary = "Dear " . $candidate_fullname . ",";
						} else if (($sm_firstname == "") && ($sm_middlename != "") && ($sm_lastname != "")) {
							$candidate_fullname = $sm_middlename . " " . $sm_lastname;
							$greeting_user_summary = "Dear " . $candidate_fullname . ",";
						} else if (($sm_firstname == "") && ($sm_middlename == "") && ($sm_lastname != "")) {
							$candidate_fullname = $sm_lastname;
							$greeting_user_summary = "Dear " . $candidate_fullname . ",";
						} else {
							$candidate_fullname = "";
							$greeting_user_summary = "Dear,";
						}//close of else of if (($candidate_firstname != "") && ($sm_middlename != "") && ($candidate_lastname != "")) {
							
							
							
							
							
					$constructed_url ="";
									
					
								
				$constructed_message_plain_text = " You have been assigned as $assigned_job_specific_role for  $job_title,";
											
				$constructed_message_html = "
										
				 <p>You have been assigned as $assigned_job_specific_role for $job_title ,</p>
				 <p> Below are the details for the Job,</p>
				 <p> Job Title : $job_title </p>
				 <p> Company  : $company_name  </p>
				 <p> Client Company :$client_company_name </p>
				 <p> Resume Submission End Date : $resume_submission_end_date </p>
				 <p> Job Summary : $job_summary </p>";
				 
				 //$constructed_url = $frontend_site_url_project_main_with_path . "jsvnavigate/" . $job_id;
											
				$constructed_url = "https://www.resumecrab.com/jsv/company/".$company_id."/job/". $job_id."/single-view";
											
				echo "constructed_url: " . $constructed_url . "\n\n";
										
				$job_assignment_link_without_html = $constructed_url . "\n\n";
									
				//echo "job_applicant_invite_link_without_html: " . $job_applicant_invite_link_without_html . "\n\n";
									
				$job_assignment_link_with_html = "<p><a href='" . $constructed_url . "'>" . $constructed_url . "</a><br><br></p>";
									
				//echo "account_activation_password_setup_reset_link_with_html: " . $account_activation_password_setup_reset_link_with_html . "\n\n";
									
				//Send Email

				$link_text = "To view further details or to work on the job, please click Here" ;

		$button_content = "Job Application";
									
				$plaintext_input = $greeting_user_summary . "
									
				" . $constructed_message_plain_text . "
									
				" .  
									

				"\n\n" .
									
				$job_assignment_link_without_html .

				"\n" .
				"Thank you \n
				
				________________________________________________________________________

				";

			/*	$html_message_input = "<div><p>" . $greeting_user_summary . "<br><br>

				" . $constructed_message_html

				. $job_assignment_link_with_html .
									 
						"
									
					<p> </p>
					<p>Regards,<br>
					Admin Team, <br>
					$company_name <br>
					</p>
				<p>________________________________________________________________________</p>

									</div>"; */

				$html_message_input = template($greeting_user_summary ,$link_text, $constructed_url , $company_name , $button_content , $constructed_message_html);

									//echo "plaintext_input: " . $plaintext_input . "<br>";
									//echo "html_message_input: " . $html_message_input . "<br>";
									//exit;
									
									
			if (filter_var($sm_email, FILTER_VALIDATE_EMAIL) == true) {
									
			$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
			
				/* $send_email_using_smtp_thr_phpmailer_result = send_email_using_smtp_thr_phpmailer($password_resetting_email_sender_email, $password_resetting_email_sender_name, $password_resetting_email_sender_email, $sm_email,null, "job assignment notifications", $plaintext_input, $html_message_input); */ 
				$to_email = array();
				$to_email[] = $sm_email;
				$email_subject = "Job Assignment notifications";
				$from_email_address = "notifications@resumecrab.com";
				$from_person_name = "Resumecrab Notifications";
				$sender_email_address = "notifications@resumecrab.com";
				$sender_person_name = "Resumecrab Notifications";
				$replyto_email_address = "notifications@resumecrab.com";
				$replyto_person_name = "Resumecrab Notifications";
				/* 
				$send_email_using_elastic_email_result = send_email_using_elastic_email($from_email_address,$from_person_name,$sender_email_address, $sender_person_name, $replyto_person_name,$replyto_email_address, $to_email,$email_subject, $plaintext_input, $html_message_input); */
									
					echo "after mail function";
					//send_email_using_smtp_thr_phpmailer($from_email_input, $sender_name_input, $reply_to_email_input, $to_email_input, $receiver_name_input, $subject_input, $plaintext_input, $html_message_input);	
								$send_email_using_elastic_email_result = true;
									
									if($send_email_using_elastic_email_result == true){
										
										$new_email_notification_status = "1";
										$email_service_provider_mail_ref_message = "message-sent";
										$email_sent_date_time = $event_datetime;
										$email_sent_date_time_epoch = $current_epoch;
										
										
									} else {
										$new_email_notification_status = "4";
										$email_service_provider_mail_ref_message = null;
										$email_sent_date_time = null;
										$email_sent_date_time_epoch = null;
									}
									
									$job_email_notification_update_status_sql = "UPDATE `job_management_assignees` SET `email_notification_status`=:email_notification_status, `service_provider_email_ref_message` = :service_provider_email_ref_message WHERE `job_id`=:job_id AND `assigned_user_sm_memb_id` = :assigned_user_sm_memb_id";
								
								
								echo "before prepare statement";	
								$job_email_notification_update_status_query = $dbcon->prepare($job_email_notification_update_status_sql);
								$job_email_notification_update_status_query->bindValue(":job_id",$job_id);
								$job_email_notification_update_status_query->bindValue(":assigned_user_sm_memb_id",$assigned_user_sm_memb_id);
								
								$job_email_notification_update_status_query->bindValue(":email_notification_status",$new_email_notification_status);
								$job_email_notification_update_status_query->bindValue(":service_provider_email_ref_message",$email_service_provider_mail_ref_message);
							   
						
								  
								  if($job_email_notification_update_status_query->execute()){
							  //echo "done";
							  echo "Email Sent Successfully";
								}
									
						} else {
							echo "Recipient's Email Address is Invalid, as per Email Format Validation Checks \n\n";
							}	
					}
				}
			//}	
		//}
	}
}		
								 
function get_assigned_user_details_based_on_assigned_user_sm_memb_id($assigned_user_sm_memb_id) {

	global $dbcon;
	$constructed_array = array();
	$user_details_get_sql = "SELECT * FROM `site_members` WHERE `sm_memb_id`=:sm_memb_id";
					
	$user_details_get_select_query = $dbcon->prepare($user_details_get_sql);
	$user_details_get_select_query->bindValue(":sm_memb_id",$assigned_user_sm_memb_id);
	$user_details_get_select_query->execute();
					
	if($user_details_get_select_query->rowCount() > 0) {
	$user_details_get_select_query_result = $user_details_get_select_query->fetch();
		//print_r($companies_details_get_select_query_result);
						
		return $user_details_get_select_query_result;
		}
		return $constructed_array;
	}
function job_assignment_email_details() {

	global $dbcon;
	/* $jma_email_notification_status_1 = "1";
	$jma_email_notification_status_2 = "2";
	$job_rel_email_notification_status = "0";
	 */
	// echo "before job assignment email details \n\n\n";
	$constructed_array = array();
	/* $job_assignment_email_get_sql = " SELECT * FROM `job_management_assignees` jma JOIN `jobs` j  ON jma.job_id = j.job_id WHERE (jma.email_notification_status = :jma_email_notification_status) AND (j.job_rel_email_notification_status = :job_rel_email_notification_status_1 OR  j.job_rel_email_notification_status = :job_rel_email_notification_status_2) AND jma.assignment_status = '1' AND jma.company_id = :company_id"; */
	
	$job_assignment_email_get_sql = " SELECT * FROM `job_management_assignees` jma JOIN `jobs` j  ON jma.job_id = j.job_id WHERE (jma.email_notification_status = :jma_email_notification_status) AND j.job_rel_email_notification_status IN (1,2) AND jma.assignment_status = '1'";
	
					 echo "before job assignment query details \n";
	$job_aasignment_email_get_select_query = $dbcon->prepare($job_assignment_email_get_sql);
		                /* $job_aasignment_email_details_select_query->bindValue(":job_id",$job_id); */  
					   $job_aasignment_email_get_select_query->bindValue(":jma_email_notification_status","0");
					   //$job_aasignment_email_get_select_query->bindValue(":job_rel_email_notification_status_1","1");
					   //$job_aasignment_email_get_select_query->bindValue(":job_rel_email_notification_status_2","2");
					   //$job_aasignment_email_get_select_query->bindValue(":company_id",$company_id);
		$job_aasignment_email_get_select_query->execute(); 				
					 echo "after excute \n";
	if($job_aasignment_email_get_select_query->rowCount() > 0) {
	       echo "after row count \n";
		$job_aasignment_email_get_select_query_result = $job_aasignment_email_get_select_query->fetchAll();
	 echo "after result";
		return $job_aasignment_email_get_select_query_result;
	}
	return $constructed_array;
}
function get_job_details($job_id) {
	global $dbcon;
	$constructed_array = array();
	$job_details_check_sql = "SELECT * FROM `jobs` WHERE `job_id` = :job_id";
	$job_details_check_select_query = $dbcon->prepare($job_details_check_sql);
	$job_details_check_select_query->bindValue(":job_id",$job_id);
	$job_details_check_select_query->execute();

	if($job_details_check_select_query->rowCount() > 0) {
		$job_details_check_select_query_result = $job_details_check_select_query->fetch();
	     return $job_details_check_select_query_result;

	}//close of if($company_details_check_select_query->rowCount() > 0) {
	return $constructed_array;
	} 
function get_company_details($company_id) {
	global $dbcon;
	$constructed_array = array();
	$company_details_check_sql = "SELECT * FROM `companies` WHERE `company_id` = :company_id";
	$company_details_check_select_query = $dbcon->prepare($company_details_check_sql);
	$company_details_check_select_query->bindValue(":company_id",$company_id);
	$company_details_check_select_query->execute();

	if($company_details_check_select_query->rowCount() > 0) {
		$company_details_check_select_query_result = $company_details_check_select_query->fetch();
	     return $company_details_check_select_query_result;

	}//close of if($company_details_check_select_query->rowCount() > 0) {
	return $constructed_array;
	} 	
function get_company_client_details($company_client_id) {
	global $dbcon;
	$constructed_array = array();
	$company_client_details_check_sql = "SELECT * FROM `company_clients`  WHERE `company_client_id` = :company_client_id";
	$company_client_details_check_select_query = $dbcon->prepare($company_client_details_check_sql);
	$company_client_details_check_select_query->bindValue(":company_client_id",$company_client_id);
	$company_client_details_check_select_query->execute();

	if($company_client_details_check_select_query->rowCount() > 0) {
		$company_client_details_check_select_query_result = $company_client_details_check_select_query->fetch();
	     return $company_client_details_check_select_query_result;

	}//close of if($company_details_check_select_query->rowCount() > 0) {
	return $constructed_array;
	} 	
?>			 
								
							