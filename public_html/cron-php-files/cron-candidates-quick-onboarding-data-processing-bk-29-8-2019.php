<?php
if(defined('STDIN') ){
  //echo("Running from CLI");
}else{
  echo("Not Running from CLI");
  exit();
}

ini_set('memory_limit', '-1');
//PCRE Backtrack Limit is increased from 1000000 to 4000000, to handle supporting, bigger images in PDF Files using PDF to Text Library
ini_set('pcre.backtrack_limit', 4000000);
//if the directory is one directory above the current file's parent directory http://stackoverflow.com/a/2100763/811207
include(dirname(dirname(dirname(__FILE__))) . "/app/core/command-line-include.php");

$cron_number = "10";
$sel_filem_setting = $dbcon->prepare("SELECT * FROM `cron_file_details` WHERE `sno` = :sno");
$sel_filem_setting->bindParam(":sno",$cron_number);
$sel_filem_setting->execute(); 
$res_filem_setting = $sel_filem_setting->fetchAll(PDO::FETCH_ASSOC);
//print_r($res_filem_setting);
//exit;
foreach($res_filem_setting as $res_filem_setting_row)
{
	$res_state_filem = $res_filem_setting_row["cron_file_status_setting"];
	$res_records_filem = $res_filem_setting_row["cron_file_numb_record_limit"];
	$res_loop_filem = $res_filem_setting_row["cron_file_numb_loop_count_limit"];
	$res_sleep_min_filem = $res_filem_setting_row["cron_file_sleep_min_seconds_limit"];
	$res_sleep_max_filem = $res_filem_setting_row["cron_file_sleep_max_seconds_limit"];
	$res_sleep_interval_filem = $res_filem_setting_row["cron_file_sleep_interval"];
	
}




if ( $res_state_filem == "ON" ) {

			
	for ( $filem = 1; $filem <=$res_loop_filem; $filem++ ) {

function output_collected ($message) {
	if  (php_sapi_name( )  ==  'cli') {
		return $message;
	} else {
	    return nl2br($message);
	}	
}
		//code related to activity in particular cron file start
			//$candidate_quick_onboarding_data_details = get_candidate_quick_onboarding_data_based_on_is_processed_status_input("1", "1"); //is_processed value input:- 0: not-confirmed, 1: confirmed, 2: processed, 3: on hold, 4: finalized
			//echo "<pre>";
			//print_r($candidate_quick_onboarding_data_details);
			//exit;
			echo "enter to loop";
			$candidate_quick_onboarding_data_select_sql = "SELECT * FROM `site_members` WHERE `sm_user_type` LIKE :sm_user_type AND `sm_user_status` = :sm_user_status";
			echo "after select query\n";
			$sm_user_type = "%"."member"."%";
			$candidate_quick_onboarding_data_select_query = $dbcon->prepare($candidate_quick_onboarding_data_select_sql);
			$candidate_quick_onboarding_data_select_query->bindValue(":sm_user_type",$sm_user_type);
			$candidate_quick_onboarding_data_select_query->bindValue(":sm_user_status",0);
			//$candidate_quick_onboarding_data_select_query->bindValue(":res_records",$res_records);	
			$candidate_quick_onboarding_data_select_query->execute(); 
			
			if($candidate_quick_onboarding_data_select_query->rowCount() > 0) {
				echo "after select query row count \n";
				$candidate_quick_onboarding_data_select_query_result = $candidate_quick_onboarding_data_select_query->fetchAll();
				foreach ($candidate_quick_onboarding_data_select_query_result as $candidate_quick_onboarding_data_select_query_result_row) {
				
					$sm_memb_id = $candidate_quick_onboarding_data_select_query_result_row["sm_memb_id"];
					 
					$salutation = $candidate_quick_onboarding_data_select_query_result_row["sm_salutation"];
					$firstname = $candidate_quick_onboarding_data_select_query_result_row["sm_firstname"];
					$middlename = $candidate_quick_onboarding_data_select_query_result_row["sm_middlename"];
					$lastname = $candidate_quick_onboarding_data_select_query_result_row["sm_lastname"];
					$mobile_number = $candidate_quick_onboarding_data_select_query_result_row["sm_mobile"];
					$email_id = $candidate_quick_onboarding_data_select_query_result_row["sm_email"];
					$alternate_email = $candidate_quick_onboarding_data_select_query_result_row["sm_alternate_email"];
					$city = $candidate_quick_onboarding_data_select_query_result_row["sm_city"];
					$state = $candidate_quick_onboarding_data_select_query_result_row["sm_state"];
					$country = $candidate_quick_onboarding_data_select_query_result_row["sm_country"];
					$experience_years = $candidate_quick_onboarding_data_select_query_result_row["experience_years"];
					$experience_months = $candidate_quick_onboarding_data_select_query_result_row["experience_months"];
					$company_id = $candidate_quick_onboarding_data_select_query_result_row["company_id"];
			
					
					$notes_get_sql = "SELECT notes FROM `candidate_rel_internal_notes` WHERE `company_id` = :company_id AND `candidate_id` = :candidate_id";
					$notes_get_q = $dbcon->prepare($notes_get_sql);
					$notes_get_q->bindValue(":company_id",$company_id);
					$notes_get_q->bindValue(":candidate_id",$sm_memb_id);
					$notes_get_q->execute();

					if($notes_get_q->rowCount() > 0) {
						$notes_get_q_result = $notes_get_q->fetch();
						
					} else {
						$notes_get_q_result = "";
					}
					$notes = $notes_get_q_result;
							
						
						//if (($confirmed_profile_source_id != "") && ($confirmed_profile_source_name != "") && ($confirmed_member_type != "") && ($confirmed_candidate_firstname != "") && ($confirmed_candidate_lastname != "") && ($confirmed_departments != "") && ($confirmed_experience_level != "") && ($confirmed_skillsets != "") && ($confirmed_mobile_number != "") && ($confirmed_city != "") && ($confirmed_country != "") &&  ($confirmed_is_ama_invitation_required != "")) {
						if (($sm_memb_id != "") && ($salutation != "") && ($firstname != "") && ($mobile_number != "") && ($email_id != "") && ($city != "") && ($country != "")) {	
							echo "required inputs are available\n";
							
								$file_extension = "pdf";
								//Create an internal File name (for the PDF File, that will be created, to hold the generate resume of the particular candidate profile)
								$generated_file_name = hash('sha256', uniqid(rand(), TRUE));
								$generated_file_name = $generated_file_name . "." . $file_extension;
								//echo "generated_file_name: " . $generated_file_name . "\n<br>";
								
								//Resume Saving Path and Filename
								$targetPath = $site_home_path . "uploaded-documents/";
								$targetFile =  $targetPath. $generated_file_name;  //from /app/core/main-config.php
								echo "targetFile: " . $targetFile . "\n";
								//exit;
								//Save the generated content of the Resume of particular candidate profile
								/*$pdf = new FPDF();
								$pdf->AddPage();
								$pdf->SetFont('Arial','B',16);
								$pdf->Cell(40,10,$pdf_content);
								$content = $pdf->Output($targetFile,'F');*/
														
								$mpdf = new \Mpdf\Mpdf([
								'margin_left' => 0,
								'margin_right' => 0,
								'margin_top' => 0,
								'margin_bottom' => 0,
								'margin_header' => 0,
								'margin_footer' => 0
								]);
								$mpdf->SetProtection(array('print'));
								$mpdf->SetTitle("Acme Trading Co. - Invoice");
								$mpdf->SetAuthor("Acme Trading Co.");
								/* $mpdf->SetWatermarkText("Paid");
								$mpdf->showWatermarkText = true;
								$mpdf->watermark_font = 'DejaVuSansCondensed';
								$mpdf->watermarkTextAlpha = 0.1; */
								$mpdf->SetDisplayMode('fullpage');
								$mpdf->Write("Profile Source Name:";
								$mpdf->Ln( 20 );
								$mpdf->SetFont( 'Arial', 'B', 12 );
								$mpdf->Write( 6, "Profile Source Name:" );
								//$pdf->Ln( 9 );
								/* $pdf->SetFont( 'Arial', '', 12 );
								$pdf->Write( 6, "generated by system" );
								$pdf->Ln( 9 ); */
								$mpdf->Output($targetFile,'F');
								
								
			
								/* //Construct the Content for the PDF File
								$pdf = new FPDF();
								$pdf->AddPage();
								//$pdf->SetTextColor( $headerColour[0], $headerColour[1], $headerColour[2] );
								$pdf->SetFont( 'Arial', '', 17 );
								//$pdf->Cell( 0, 15, $reportName, 0, 0, 'C' );
								//$pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
								//$pdf->SetFont( 'Arial', '', 20 );
								//$pdf->Write( 19, $confirmed_candidate_firstname . " " . $confirmed_candidate_lastname );
								$pdf->Ln( 20 );
								$pdf->SetFont( 'Arial', 'B', 12 );
								$pdf->Write( 6, "Profile Source Name:" );
								$pdf->Ln( 9 );
								$pdf->SetFont( 'Arial', '', 12 );
								$pdf->Write( 6, "generated by system" );
								$pdf->Ln( 9 );
								
						
								
								$pdf->SetFont( 'Arial', 'I', 10 );
								$pdf->Write( 6, "This is a Dynamically created Resume, using internal Data for internal reference only @Inademy." );
								$content = $pdf->Output($targetFile,'F'); */

						}
							
								
				}
						
			} else {
				echo "no records available";
			}
					
		}
	} 
		
?>