<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * Candidate related File Document View
 *
 */
 

$ea_maintenance_mode = false; 


//Get Maintenance State Details of REST Services
$ea_maintanance_mode_time = getMaintananceInfo();

//Check maintenance mode
if ($ea_maintanance_mode_time != "") {
	$ea_maintenance_mode = true;	
	//echo "Maintenance mode (true): " . $ea_maintenance_mode . "\n";
}//close of if ($ea_maintanance_mode_time != "") {

//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	echo "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
}//close of if ($ea_maintenance_mode) {


$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();


//https://dev-visadoc.securitywonks.net/viewer/11/candidate/11/uploaded-file-ref-id/46/display/expires/1111111/link-signature/bd27845343fdfer3redsfr3regfh6t3rfdggj78453f

//Include maintenance Check
//include expires check
//$user_defined_routes["candidate-rel-pdf-document-display"] = array("route_value" => "/viewer/" . $routing_eng_var_2 . "/candidate/" . $routing_eng_var_4 . "/uploaded-file-ref-id/" . $routing_eng_var_6 . "/display/expires/" . $routing_eng_var_9 . "/link-signature/" . $routing_eng_var_11,

$viewer_id_input = $routing_eng_var_2;
//echo "viewer_id_input: " . $viewer_id_input . "<br>";
$candidate_id_input = $routing_eng_var_4;
//echo "candidate_id_input: " . $candidate_id_input . "<br>";
$uploaded_file_ref_id_input = $routing_eng_var_6;
//echo "uploaded_file_ref_id_input: " . $uploaded_file_ref_id_input . "<br>";
$expires_at_input = $routing_eng_var_9;
//echo "expires_at_input: " . $expires_at_input . "<br>";
$link_signature_input = $routing_eng_var_11;
//echo "link_signature_input: " . $link_signature_input . "<br>";


								$candidate_rel_uploaded_visa_document_details_result = candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id($candidate_id_input, $uploaded_file_ref_id_input);
								
								if (count($candidate_rel_uploaded_visa_document_details_result) > 0) {
									
									$crur_id = $candidate_rel_uploaded_visa_document_details_result["crur_id"];
									$company_id = $candidate_rel_uploaded_visa_document_details_result["company_id"];
									$sm_memb_id = $candidate_rel_uploaded_visa_document_details_result["sm_memb_id"];
									$resume_upload_mode = $candidate_rel_uploaded_visa_document_details_result["resume_upload_mode"];
									$resume_source = $candidate_rel_uploaded_visa_document_details_result["resume_source"];
									$additional_document_ref = $candidate_rel_uploaded_visa_document_details_result["additional_document_ref"];
									$original_filename = $candidate_rel_uploaded_visa_document_details_result["original_filename"];
									$generated_filename = $candidate_rel_uploaded_visa_document_details_result["generated_filename"];
									$file_size_bytes = $candidate_rel_uploaded_visa_document_details_result["file_size_bytes"];
									$file_size_kilo_bytes = $candidate_rel_uploaded_visa_document_details_result["file_size_kilo_bytes"];
									$file_size_mega_bytes = $candidate_rel_uploaded_visa_document_details_result["file_size_mega_bytes"];
									$is_active_status = $candidate_rel_uploaded_visa_document_details_result["is_active_status"];
									
									if (($is_active_status == "1") || ($is_active_status == "2")) {
										//Valid Status of the PDF Document
										
										$user_rel_active_jwt_token_details_result = ea_get_user_rel_active_jwt_token_details($viewer_id_input);
										//echo "<pre>";
										//print_r($user_rel_active_jwt_token_details_result);
										//exit;
										
										foreach ($user_rel_active_jwt_token_details_result as $user_rel_active_jwt_token_details_result_row) {
											
											$user_auth_token_id = $user_rel_active_jwt_token_details_result_row["user_auth_token_id"];
											
											$uploaded_file_url_without_signature = $site_url_project_main . "viewer/" . $viewer_id_input . "/candidate/" . $candidate_id_input . "/uploaded-file-ref-id/" . $uploaded_file_ref_id_input . "/display/expires/" . $expires_at_input;
				
											//echo "uploaded_file_url (without signature): " . $uploaded_file_url_without_signature . "<br>";
											
											/*//Creating Link Signature
											$created_link_signature = hash('sha256', $user_auth_token_id . $uploaded_file_url_without_signature);
											
											if (hash_equals($link_signature_input, $created_link_signature)) {
												
											}
											*/
											
											//Collect the Base64 Decoded version of the Expiring Link Secret Key
											$expiring_link_secret_base64_decoded = base64_decode($expiring_link_secret_key);
											
											
											//Re-Create Signature, using the extracted Header and Payload of the received JWT Auth Token
											$created_hash_for_verification = hash_hmac($expiring_link_hash_algorithm, $user_auth_token_id . $uploaded_file_url_without_signature, $expiring_link_secret_base64_decoded, true);
											
											//Base64 Encode the Created Hash
											$created_hash_for_verification_base64_encoded = base64_encode($created_hash_for_verification);


											//As per Base64 URL Encoding Concept that is described in https://tools.ietf.org/html/rfc4648#page-7
											//http://stackoverflow.com/a/11449627
											
											//Base64 URL Encode the Created Hash
											$created_hash_for_verification_base64_urlencoded = base64url_encode($created_hash_for_verification);

											//Do Remove padding (=), from the Base64 URL Encoded Hash
											$created_hash_for_verification_base64_urlencoded_after_removing_padding = str_replace("=", "", $created_hash_for_verification_base64_urlencoded);
											
											//echo "created_hash_for_verification_base64_urlencoded_after_removing_padding: " . $created_hash_for_verification_base64_urlencoded_after_removing_padding . "<br>";
											
											//Do Compare if the Created Hash equals received Link's Signature, in a timing safe comparison approach
											if ((function_exists('hash_equals')) && (hash_equals($link_signature_input, $created_hash_for_verification_base64_urlencoded_after_removing_padding))) {
												//Valid Signature Scenario
												
												//$specific_candidate_resume_original_file_name = $resume_details["resume_original_file_name"];
												//$specific_candidate_resume_generated_file_name = $resume_details["resume_generated_file_name"];
												
												//$crauvd_id_folder_name = create_seo_name(strtolower_utf8_extended($activity_ref_id)) . "_" . create_seo_name(strtolower_utf8_extended($additional_document_ref));
												
												$crauvd_id_folder_name = create_seo_name(strtolower_utf8_extended($crauvd_id)) . "_" . create_seo_name(strtolower_utf8_extended($activity_ref_id)) . "_" . create_seo_name(strtolower_utf8_extended($additional_document_ref));
												
												//echo "valid signature<br>";
												
												if ((isset($generated_filename)) && ($generated_filename != "")) {
													
													/*if ($current_epoch > $expires_at_input) {
														
														echo "The Link got Expired, please refresh the page, to get the updated link.";
														
													} else {*/
														//$resume_pdf_file_path = 'files/123-yah-com-resume.pdf';
														//$resume_pdf_file_path = $relative_resume_folder_path_document_root . $generated_filename;
														$candidate_uploaded_file_rel_relative_path = $candidate_document_upload_main_folder_rel_download_path . $candidate_id_input . "/" . $crauvd_id_folder_name . "/" . $generated_filename;
														
														
														//echo "candidate_uploaded_file_rel_relative_path: " . $candidate_uploaded_file_rel_relative_path . "<br>";
														
														if(file_exists($candidate_uploaded_file_rel_relative_path)) {
															//echo "file exists";
															$fileSize = filesize($candidate_uploaded_file_rel_relative_path);   
															//$fileName = "123-yah-com-resume.pdf";     
															//$fileName = $original_filename; 
															header("Cache-Control: private");
															//header("Content-Type: application/stream");
															header('Content-type:application/pdf');
															header('content-Transfer-Encoding:binary');
															header("Content-Length: ".$fileSize);
															header("Content-Disposition: inline; filename=".$original_filename);
															// Output file.
															readfile ($candidate_uploaded_file_rel_relative_path);                   
															exit();
														} else {
															echo "file does not exist";
														}//close of if(file_exists($candidate_uploaded_file_rel_relative_path)) {
															
													//}//close of else of if ($current_epoch > $expires_at_input) {
														
													
												} else {
													echo "generated file name is empty";
												}//close of if ((isset($generated_filename)) && ($generated_filename != "")) {
												
												
											} else {
												//Invalid Expiring Link Signature
												echo "Invalid Signature";
											}//close of else of if ((function_exists('hash_equals')) && (hash_equals($link_signature_input, $created_hash_for_verification_base64_urlencoded_after_removing_padding))) {
											
											
										}//close of foreach ($user_rel_active_jwt_token_details_result as $user_rel_active_jwt_token_details_result_row) {
											
											
									} else {
										//This PDF Document is explicitly made in-active
										//Construct Content, that will be sent in Response body, of the REST Web Service
										$response['data'] = array();
										$response['status'] = "invalidated-user-pdf-document-references";
										$response['status_description'] = "These PDF Document References are already Invalidated by Candidate thyself / Admin User w.r.t. respective Company / Platform scope, Please check and try again.";
										
										$eventLog->log("These PDF Document References are already Invalidated by Candidate thyself / Admin User w.r.t. respective Company / Platform scope, Please check and try again.");
										
									}//close of else of if (($is_active_status == "1") || ($is_active_status == "2")) {
										
								} else {
									//Invalid user_id and pdf document id inputs
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "invalid-user-pdf-document-references";
									$response['status_description'] = "Invalid PDF Document References, Please check and try again.";
									
									$eventLog->log("Invalid PDF Document References, Please check and try again.");
									
								}//close of else of if (count($candidate_rel_uploaded_visa_document_details_result) > 0) {
									
								
								
								

function candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crur_id($user_id_input, $crauvd_id_input) {
    global $dbcon;
	$constructed_array = array();
	
	$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crur_id_sql = "SELECT * FROM `candidate_rel_all_uploaded_visa_documents` WHERE `crauvd_id`=:crauvd_id AND `sm_memb_id`=:sm_memb_id";
	$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crur_id_query = $dbcon->prepare($candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crur_id_sql);
	$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crur_id_query->bindValue(":crauvd_id",$crauvd_id_input);	
	$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crur_id_query->bindValue(":sm_memb_id",$user_id_input);	
	$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crur_id_query->execute(); 
	
	if($candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crur_id_query->rowCount() > 0) {
		$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crur_id_query_result = $candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crur_id_query->fetch();
	     return $candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crur_id_query_result;
	
	}//close of if($candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query->rowCount() > 0) {
	return $constructed_array;
}


?>