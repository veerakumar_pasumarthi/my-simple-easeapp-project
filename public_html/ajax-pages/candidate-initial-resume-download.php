<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * Candidate related File Document View
 *
 */
 
echo "hello";
$ea_maintenance_mode = false; 

//include 'ajax-practice.php';
include 'ajax-petition-practice.php';

//Get Maintenance State Details of REST Services
$ea_maintanance_mode_time = getMaintananceInfo();

//Check maintenance mode
if ($ea_maintanance_mode_time != "") {
	$ea_maintenance_mode = true;	
	//echo "Maintenance mode (true): " . $ea_maintenance_mode . "\n";
}//close of if ($ea_maintanance_mode_time != "") {

//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	echo "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
}//close of if ($ea_maintenance_mode) {


$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();


//https://dev-visadoc.securitywonks.net/viewer/11/candidate/11/uploaded-file-ref-id/46/download/expires/1111111/link-signature/bd27845343fdfer3redsfr3regfh6t3rfdggj78453f

//Include maintenance Check
//include expires check
//$user_defined_routes["candidate-rel-pdf-document-display"] = array("route_value" => "/viewer/" . $routing_eng_var_2 . "/candidate/" . $routing_eng_var_4 . "/uploaded-file-ref-id/" . $routing_eng_var_6 . "/display/expires/" . $routing_eng_var_9 . "/link-signature/" . $routing_eng_var_11,

//https://uploaded-files-dev.resumecrab.com/candidate-initial-resume/resume-id/9

$resume_name_input = $routing_eng_var_2;
//echo "resume_id_input: " . $resume_id_input . "<br>";

echo "hello... just for checking";
//$resume_document_path = dirname(dirname(dirname(__FILE__))) . "/uploaded-documents/clean-documents/";

//echo $resume_document_path;


$candidate_quick_onboarding_data_select_sql = "SELECT * FROM `site_members` WHERE `sm_user_type` LIKE :sm_user_type AND `sm_user_status` = :sm_user_status AND `sm_memb_id` = :sm_memb_id";
      echo "after select query\n";
      $sm_user_type = "%"."member"."%";
      $candidate_quick_onboarding_data_select_query = $dbcon->prepare($candidate_quick_onboarding_data_select_sql);
      $candidate_quick_onboarding_data_select_query->bindValue(":sm_user_type",$sm_user_type);
      $candidate_quick_onboarding_data_select_query->bindValue(":sm_user_status",1);
      $candidate_quick_onboarding_data_select_query->bindValue(":sm_memb_id",14);
      //$candidate_quick_onboarding_data_select_query->bindValue(":res_records",$res_records);  
      $candidate_quick_onboarding_data_select_query->execute(); 
      
      if($candidate_quick_onboarding_data_select_query->rowCount() > 0) {
        echo "after select query row count \n";
        $candidate_quick_onboarding_data_select_query_result = $candidate_quick_onboarding_data_select_query->fetchAll();
        foreach ($candidate_quick_onboarding_data_select_query_result as $candidate_quick_onboarding_data_select_query_result_row) {
          
          $sm_memb_id = $candidate_quick_onboarding_data_select_query_result_row["sm_memb_id"];
          
          $salutation = $candidate_quick_onboarding_data_select_query_result_row["sm_salutation"];
          $firstname = $candidate_quick_onboarding_data_select_query_result_row["sm_firstname"];
          $middlename = $candidate_quick_onboarding_data_select_query_result_row["sm_middlename"];
          $lastname = $candidate_quick_onboarding_data_select_query_result_row["sm_lastname"];
          $fullname = $firstname." ".$middlename." ".$lastname ;
          $mobile_number = $candidate_quick_onboarding_data_select_query_result_row["sm_mobile"];
          $email_id = $candidate_quick_onboarding_data_select_query_result_row["sm_email"];
          $alternate_email = $candidate_quick_onboarding_data_select_query_result_row["sm_alternate_email"];
          $address = $candidate_quick_onboarding_data_select_query_result_row["sm_address"];
          $city = $candidate_quick_onboarding_data_select_query_result_row["sm_city"];
          $state = $candidate_quick_onboarding_data_select_query_result_row["sm_state"];
          $country = $candidate_quick_onboarding_data_select_query_result_row["sm_country"];
          //$experience_years = $candidate_quick_onboarding_data_select_query_result_row["experience_years"];
          //$experience_months = $candidate_quick_onboarding_data_select_query_result_row["experience_months"];
          $company_id = $candidate_quick_onboarding_data_select_query_result_row["company_id"];
      }
    }


    $candidate_quick_onboarding_data_select_sql = "SELECT * FROM `candidate_rel_education_qualifications` WHERE `sm_memb_id` LIKE :sm_memb_id";
      echo "after select query\n";
      $candidate_quick_onboarding_data_select_query = $dbcon->prepare($candidate_quick_onboarding_data_select_sql);
     
      $candidate_quick_onboarding_data_select_query->bindValue(":sm_memb_id",$sm_memb_id);
      //$candidate_quick_onboarding_data_select_query->bindValue(":res_records",$res_records);  
      $candidate_quick_onboarding_data_select_query->execute(); 
      
      if($candidate_quick_onboarding_data_select_query->rowCount() > 0) {
        echo "after select query row count \n";
        $candidate_quick_onboarding_data_select_query_result = $candidate_quick_onboarding_data_select_query->fetchAll();
        foreach ($candidate_quick_onboarding_data_select_query_result as $candidate_quick_onboarding_data_select_query_result_row) {
          
          $school_name = $candidate_quick_onboarding_data_select_query_result_row["school_name"];
          
          $educational_program = $candidate_quick_onboarding_data_select_query_result_row["educational_program"];
          $year_of_passout = $candidate_quick_onboarding_data_select_query_result_row["year_of_passout"];
          $major = $candidate_quick_onboarding_data_select_query_result_row["major"];
          $earned_percentage = $candidate_quick_onboarding_data_select_query_result_row["earned_percentage"];
          $earned_points = $candidate_quick_onboarding_data_select_query_result_row["earned_points"];
          $sequence_order = $candidate_quick_onboarding_data_select_query_result_row["sequence_order"];

          if($earned_percentage != "" && $earned_points == ""){

            $Aggregate = $earned_percentage ;

          } else if($earned_percentage == "" && $earned_points != ""){

            $Aggregate = $earned_points ;

          } else {
            $Aggregate = $earned_percentage ;
          }

          $edu_temp = '<tr style="border-collapse:collapse"> 
                          <td style="padding:8px;Margin:0;border:1px solid #DDDDDD;color:#333333">'.$sequence_order.'</td> 
                          <td style="padding:8px;Margin:0;border:1px solid #DDDDDD;color:#333333">'.$educational_program.'</td> 
                          <td style="padding:8px;Margin:0;border:1px solid #DDDDDD;color:#333333">'.$major.'</td> 
                          <td style="padding:8px;Margin:0;border:1px solid #DDDDDD;color:#333333">'.$school_name.'</td> 
                          <td style="padding:8px;Margin:0;border:1px solid #DDDDDD;color:#333333">'.$year_of_passout.'</td> 
                          <td style="padding:8px;Margin:0;border:1px solid #DDDDDD;color:#333333">'.$Aggregate.'</td> 
                         </tr>' ;
          $edu_res .= $edu_temp ;
          
      }
    }


 $candidate_quick_onboarding_data_select_sql = "SELECT * FROM `candidate_rel_preferred_job_locations` WHERE `sm_memb_id` LIKE :sm_memb_id";
      echo "after select query\n";
      $candidate_quick_onboarding_data_select_query = $dbcon->prepare($candidate_quick_onboarding_data_select_sql);
     
      $candidate_quick_onboarding_data_select_query->bindValue(":sm_memb_id",$sm_memb_id);
      //$candidate_quick_onboarding_data_select_query->bindValue(":res_records",$res_records);  
      $candidate_quick_onboarding_data_select_query->execute(); 
      
      if($candidate_quick_onboarding_data_select_query->rowCount() > 0) {
        echo "after select query row count \n";
        $candidate_quick_onboarding_data_select_query_result = $candidate_quick_onboarding_data_select_query->fetchAll();
        foreach ($candidate_quick_onboarding_data_select_query_result as $candidate_quick_onboarding_data_select_query_result_row) {
          
          $preferred_country_name = $candidate_quick_onboarding_data_select_query_result_row["country_name"];
          
         
          
      }
    }


 $candidate_quick_onboarding_data_select_sql = "SELECT * FROM `candidate_rel_additional_info` WHERE `sm_memb_id` LIKE :sm_memb_id";
      echo "after select query\n";
      $candidate_quick_onboarding_data_select_query = $dbcon->prepare($candidate_quick_onboarding_data_select_sql);
     
      $candidate_quick_onboarding_data_select_query->bindValue(":sm_memb_id",$sm_memb_id);
      //$candidate_quick_onboarding_data_select_query->bindValue(":res_records",$res_records);  
      $candidate_quick_onboarding_data_select_query->execute(); 
      
      if($candidate_quick_onboarding_data_select_query->rowCount() > 0) {
        echo "after select query row count \n";
        $candidate_quick_onboarding_data_select_query_result = $candidate_quick_onboarding_data_select_query->fetchAll();
        foreach ($candidate_quick_onboarding_data_select_query_result as $candidate_quick_onboarding_data_select_query_result_row) {
          
          $experience_years = $candidate_quick_onboarding_data_select_query_result_row["experience_years"];
          $experience_months = $candidate_quick_onboarding_data_select_query_result_row["experience_months"];

          $experiance = $experience_years.' years '.$experience_months.' months' ;
          
         
          
      }
    }

    $candidate_quick_onboarding_data_select_sql = "SELECT * FROM `candidate_rel_skillsets` WHERE `sm_memb_id` LIKE :sm_memb_id";
      echo "after select query\n";
      $candidate_quick_onboarding_data_select_query = $dbcon->prepare($candidate_quick_onboarding_data_select_sql);
     
      $candidate_quick_onboarding_data_select_query->bindValue(":sm_memb_id",$sm_memb_id);
      //$candidate_quick_onboarding_data_select_query->bindValue(":res_records",$res_records);  
      $candidate_quick_onboarding_data_select_query->execute(); 
      
      if($candidate_quick_onboarding_data_select_query->rowCount() > 0) {
        echo "after select query row count \n";
        $candidate_quick_onboarding_data_select_query_result = $candidate_quick_onboarding_data_select_query->fetchAll();
        foreach ($candidate_quick_onboarding_data_select_query_result as $candidate_quick_onboarding_data_select_query_result_row) {
          
          $skill_name = $candidate_quick_onboarding_data_select_query_result_row["skill_name"];
          
          $version = $candidate_quick_onboarding_data_select_query_result_row["version"];
          $last_used_year = $candidate_quick_onboarding_data_select_query_result_row["last_used_year"];
          $experience_months = $candidate_quick_onboarding_data_select_query_result_row["experience_months"];
           $experience_years = $candidate_quick_onboarding_data_select_query_result_row["experience_years"];
          
          $sequence_order = $candidate_quick_onboarding_data_select_query_result_row["sequence_order"];

          

          $skill_temp = '<tr style="border-collapse:collapse"> 
                          <td style="padding:8px;Margin:0;border:1px solid #DDDDDD;color:#333333">'.$sequence_order.'</td> 
                          <td style="padding:8px;Margin:0;border:1px solid #DDDDDD;color:#333333">'.$skill_name.'</td> 
                          <td style="padding:8px;Margin:0;border:1px solid #DDDDDD;color:#333333">'.$version.'</td> 
                          <td style="padding:8px;Margin:0;border:1px solid #DDDDDD;color:#333333">'.$experience_years.'</td> 
                          <td style="padding:8px;Margin:0;border:1px solid #DDDDDD;color:#333333">'.$experience_months.'</td> 
                          <td style="padding:8px;Margin:0;border:1px solid #DDDDDD;color:#333333">'.$last_used_year.'</td> 
                         </tr>' ;
          $skill_res .= $skill_temp ;
          
      }
    }


$candidate_quick_onboarding_data_select_sql = "SELECT * FROM `candidate_rel_professional_experiences` WHERE `sm_memb_id` LIKE :sm_memb_id";
      echo "after select query\n";
      $candidate_quick_onboarding_data_select_query = $dbcon->prepare($candidate_quick_onboarding_data_select_sql);
     
      $candidate_quick_onboarding_data_select_query->bindValue(":sm_memb_id",$sm_memb_id);
      //$candidate_quick_onboarding_data_select_query->bindValue(":res_records",$res_records);  
      $candidate_quick_onboarding_data_select_query->execute(); 
      
      if($candidate_quick_onboarding_data_select_query->rowCount() > 0) {
        echo "after select query row count \n";
        $candidate_quick_onboarding_data_select_query_result = $candidate_quick_onboarding_data_select_query->fetchAll();
        foreach ($candidate_quick_onboarding_data_select_query_result as $candidate_quick_onboarding_data_select_query_result_row) {
          
          $company_name = $candidate_quick_onboarding_data_select_query_result_row["company_name"];
          
          $designation = $candidate_quick_onboarding_data_select_query_result_row["designation"];
          $location_city = $candidate_quick_onboarding_data_select_query_result_row["location_city"];
          $location_state = $candidate_quick_onboarding_data_select_query_result_row["location_state"];
          $location_country = $candidate_quick_onboarding_data_select_query_result_row["location_country"];
          $from_month = $candidate_quick_onboarding_data_select_query_result_row["from_month"];
          $from_year = $candidate_quick_onboarding_data_select_query_result_row["from_year"];
          $to_month = $candidate_quick_onboarding_data_select_query_result_row["to_month"];
          $to_year = $candidate_quick_onboarding_data_select_query_result_row["to_year"];
          $is_current_company = $candidate_quick_onboarding_data_select_query_result_row["is_current_company"];

          if($is_current_company == 0){

            $till = $to_month.'-'.$to_year.' (month-year)' ;

          }  else {
            $till = 'now ( still working )' ;
          }

          $pro_temp = ' <tr style="border-collapse:collapse"> <td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-top:15px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:21px;color:#333333">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; I have worked in '.$company_name.' which is located in '.$location_city.' city of '.$location_state.' in '.$location_country.'. I have worked as '.$designation.' from '.$from_month.'-'.$from_year.' (month-year) to '.$till.'.</p></td> </tr>' ;
          $pro_res .= $pro_temp ;
          
      }
    }





$mpdf = new \Mpdf\Mpdf();

// Write some HTML code:
//$mpdf->WriteHTML('Hello World');

$message = '';
				$message .= 
'<html>
<body style="width:100%;font-family:arial,helvetica neue, helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0"> 
  <div class="es-wrapper-color" style="background-color:#F6F6F6"> 
   <!--[if gte mso 9]>
      <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
        <v:fill type="tile" color="#f6f6f6"></v:fill>
      </v:background>
    <![endif]--> 
   <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top"> 
    <tbody>
     <tr style="border-collapse:collapse"> 
      <td valign="top" style="padding:0;Margin:0"> 
       <table class="es-content" cellspacing="0" cellpadding="0" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
        <tbody>
         <tr style="border-collapse:collapse"> 
          <td align="center" style="padding:0;Margin:0"> 
           <table class="es-content-body" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center"> 
            <tbody>
             <tr style="border-collapse:collapse"> 
              <td align="left" style="padding:0;Margin:0;padding-left:30px;padding-right:30px"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                <tbody>
                 <tr style="border-collapse:collapse"> 
                  <td valign="top" align="center" style="padding:0;Margin:0;width:540px"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                    <tbody>
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;padding-top:15px;font-size:0"> 
                       <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                        <tbody>
                         <tr style="border-collapse:collapse"> 
                          <td style="padding:0;Margin:0;border-bottom:1px solid #DADADA;background:#FFFFFFnone repeat scroll 0% 0%;height:1px;width:100%;margin:0px"></td> 
                         </tr> 
                        </tbody>
                       </table></td> 
                     </tr> 
                    </tbody>
                   </table></td> 
                 </tr> 
                </tbody>
               </table></td> 
             </tr> 
            </tbody>
           </table></td> 
         </tr> 
        </tbody>
       </table> 
       <table class="es-content" cellspacing="0" cellpadding="0" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
        <tbody>
         <tr style="border-collapse:collapse"> 
          <td align="center" style="padding:0;Margin:0"> 
           <table class="es-content-body" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center"> 
            <tbody>
             <tr style="border-collapse:collapse"> 
              <td style="padding:0;Margin:0;padding-top:25px;background-color:#B5BCC4" bgcolor="#b5bcc4" align="left"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                <tbody>
                 <tr style="border-collapse:collapse"> 
                  <td valign="top" align="center" style="padding:0;Margin:0;width:600px"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                    <tbody>
                     <tr style="border-collapse:collapse"> 
                      <td class="es-m-txt-c" align="center" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;padding-bottom:25px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:arial, helvetica neue, helvetica, sans-serif;font-size:30px;font-style:normal;font-weight:normal;color:#FFFFFF"><b>'.$fullname.'</b></h1></td> 
                     </tr> 
                    </tbody>
                   </table></td> 
                 </tr> 
                </tbody>
               </table></td> 
             </tr> 
             <tr style="border-collapse:collapse"> 
              <td align="left" style="padding:0;Margin:0;padding-top:30px;padding-left:30px;padding-right:30px"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                <tbody>
                 <tr style="border-collapse:collapse"> 
                  <td valign="top" align="center" style="padding:0;Margin:0;width:540px"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                    <tbody>
                     <tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:5px;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:32px;color:#333333">Email Id : '.$email_id.'</p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:40px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:21px;color:#333333">&nbsp;Mobile Number&nbsp;: '.$mobile_number.'&nbsp;</p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse"> 
                      <td class="es-m-txt-l" align="center" bgcolor="#cccccc" style="padding:0;Margin:0"><h3 style="Margin:0;line-height:30px;mso-line-height-rule:exactly;font-family:arial, helvetica neue, helvetica, sans-serif;font-size:20px;font-style:normal;font-weight:normal;color:#666666">Career Objective</h3></td> 
                     </tr> 
                     <tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;padding-top:20px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:21px;color:#333333">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; To be associated with a progressive organization where I can use my logical and analytical skills for consistently delivering effective solution with high level of energy, integrity and ethics with the exceptional ability to deliver outstanding bottom line results in shorter deadlines.</p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:40px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:21px;color:#333333">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;To secure a challenging position in a reputable organization to expand my learnings, knowledge, and skills.</p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse"> 
                      <td class="es-m-txt-l" align="center" bgcolor="#cccccc" style="padding:0;Margin:0"><h3 style="Margin:0;line-height:30px;mso-line-height-rule:exactly;font-family:arial, helvetica neue, helvetica, sans-serif;font-size:20px;font-style:normal;font-weight:normal;color:#666666">Educational Qualifications</h3></td> 
                     </tr> 
                     <tr style="border-collapse:collapse"> 
                      <td style="padding:0;Margin:0;padding-top:25px;padding-bottom:40px"> 
                       <table role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"></table> 
                       <table id="customers" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:100%"> 
                        <tbody>
                         <tr style="border-collapse:collapse"> 
                          <th style="padding-top:12px;padding-bottom:12px;text-align:left;background-color:#B5BCC4;color:#333333;border:1px solid #DDDDDD;padding:8px">S.No</th> 
                          <th style="padding-top:12px;padding-bottom:12px;text-align:left;background-color:#B5BCC4;color:#333333;border:1px solid #DDDDDD;padding:8px">Educational Program</th> 
                          <th style="padding-top:12px;padding-bottom:12px;text-align:left;background-color:#B5BCC4;color:#333333;border:1px solid #DDDDDD;padding:8px">Major</th> 
                          <th style="padding-top:12px;padding-bottom:12px;text-align:left;background-color:#B5BCC4;color:#333333;border:1px solid #DDDDDD;padding:8px">School Name</th> 
                          <th style="padding-top:12px;padding-bottom:12px;text-align:left;background-color:#B5BCC4;color:#333333;border:1px solid #DDDDDD;padding:8px">Year Of Graduation</th> 
                          <th style="padding-top:12px;padding-bottom:12px;text-align:left;background-color:#B5BCC4;color:#333333;border:1px solid #DDDDDD;padding:8px">Aggregate</th> 
                         </tr>'.$edu_res.' 
                        </tbody>
                       </table></td> 
                     </tr> 
                    </tbody>
                   </table></td> 
                 </tr> 
                </tbody>
               </table></td> 
             </tr> 
             <tr style="border-collapse:collapse"> 
              <td align="left" style="padding:0;Margin:0;padding-left:30px;padding-right:30px"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                <tbody>
                 <tr style="border-collapse:collapse"> 
                  <td valign="top" align="center" style="padding:0;Margin:0;width:540px"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                    <tbody>
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;padding-top:5px;font-size:0"> 
                       <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                        <tbody>
                         <tr style="border-collapse:collapse"> 
                          <td style="padding:0;Margin:0;border-bottom:1px solid #DADADA;background:#FFFFFFnone repeat scroll 0% 0%;height:1px;width:100%;margin:0px"></td> 
                         </tr> 
                        </tbody>
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse"> 
                      <td class="es-m-txt-l" align="center" bgcolor="#cccccc" style="padding:0;Margin:0"><h3 style="Margin:0;line-height:30px;mso-line-height-rule:exactly;font-family:arial, helvetica neue, helvetica, sans-serif;font-size:20px;font-style:normal;font-weight:normal;color:#666666">Personal Information</h3></td> 
                     </tr>
                     <tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:5px;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:32px;color:#333333">Address : '.$address.'</p></td> 
                     </tr>
                    <tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:5px;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:32px;color:#333333">City : '.$city.'</p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:5px;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:32px;color:#333333">State (country) : '.$state.' ('.$country.')</p></td> 
                     </tr> 

                      <tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:5px;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:32px;color:#333333">Experiance : '.$Experiance.'</p></td> 
                     </tr> 

                     <tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:5px;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:32px;color:#333333">Preferred Job Country : '.$preferred_country_name.'</p></td> 
                     </tr> 
                    
                    </tbody>
                   </table></td> 
                 </tr> 
                </tbody>
               </table></td> 
             </tr> 
             <tr style="border-collapse:collapse"> 
              <td align="left" style="padding:0;Margin:0;padding-left:30px;padding-right:30px"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                <tbody>
                 <tr style="border-collapse:collapse"> 
                  <td valign="top" align="center" style="padding:0;Margin:0;width:540px"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                    <tbody>
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;padding-top:5px;font-size:0"> 
                       <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                        <tbody>
                         <tr style="border-collapse:collapse"> 
                          <td style="padding:0;Margin:0;border-bottom:1px solid #DADADA;background:#FFFFFFnone repeat scroll 0% 0%;height:1px;width:100%;margin:0px"></td> 
                         </tr> 
                        </tbody>
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse"> 
                      <td class="es-m-txt-l" align="center" bgcolor="#cccccc" style="padding:0;Margin:0"><h3 style="Margin:0;line-height:30px;mso-line-height-rule:exactly;font-family:arial, helvetica neue, helvetica, sans-serif;font-size:20px;font-style:normal;font-weight:normal;color:#666666">Proffesional Summary</h3></td> 
                     </tr> 
                     '.$pro_res.'
                    </tbody>
                   </table></td> 
                 </tr> 
                </tbody>
               </table></td> 
             </tr> 
             <tr style="border-collapse:collapse"> 
              <td align="left" style="padding:0;Margin:0;padding-left:30px;padding-right:30px"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                <tbody>
                 <tr style="border-collapse:collapse"> 
                  <td valign="top" align="center" style="padding:0;Margin:0;width:540px"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                    <tbody>
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;padding-top:5px;font-size:0"> 
                       <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                        <tbody>
                         <tr style="border-collapse:collapse"> 
                          <td style="padding:0;Margin:0;border-bottom:1px solid #DADADA;background:#FFFFFFnone repeat scroll 0% 0%;height:1px;width:100%;margin:0px"></td> 
                         </tr> 
                        </tbody>
                       </table></td> 
                     </tr> 
                       <tr> 
                      <td style="padding-top:30px"></td> 
                     </tr>
                     <tr style="border-collapse:collapse"> 
                      <td class="es-m-txt-l" align="center" bgcolor="#cccccc" style="padding:0;Margin:0"><h3 style="Margin:0;line-height:30px;mso-line-height-rule:exactly;font-family:arial, helvetica neue, helvetica, sans-serif;font-size:20px;font-style:normal;font-weight:normal;color:#666666">Job Skills</h3></td> 
                     </tr> 
                     <tr style="border-collapse:collapse"> 
                      <td style="padding:0;Margin:0;padding-top:25px;padding-bottom:40px"> 
                       <table role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"></table> 
                       <table id="customers" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:100%"> 
                        <tbody>
                         <tr style="border-collapse:collapse"> 
                          <th style="padding-top:12px;padding-bottom:12px;text-align:left;background-color:#B5BCC4;color:#333333;border:1px solid #DDDDDD;padding:8px">S.No</th> 
                          <th style="padding-top:12px;padding-bottom:12px;text-align:left;background-color:#B5BCC4;color:#333333;border:1px solid #DDDDDD;padding:8px">Skill Name</th> 
                          <th style="padding-top:12px;padding-bottom:12px;text-align:left;background-color:#B5BCC4;color:#333333;border:1px solid #DDDDDD;padding:8px">Version</th> 
                          <th style="padding-top:12px;padding-bottom:12px;text-align:left;background-color:#B5BCC4;color:#333333;border:1px solid #DDDDDD;padding:8px">Experiance Years</th> 
                          <th style="padding-top:12px;padding-bottom:12px;text-align:left;background-color:#B5BCC4;color:#333333;border:1px solid #DDDDDD;padding:8px">Experiance Months</th> 
                          <th style="padding-top:12px;padding-bottom:12px;text-align:left;background-color:#B5BCC4;color:#333333;border:1px solid #DDDDDD;padding:8px">Last Used Year</th> 
                         </tr>'.$skill_res.' 
                        </tbody>
                       </table></td> 
                     </tr> 
                    </tbody>
                   </table></td> 
                 </tr> 
                </tbody>
               </table></td> 
             </tr> 
             <tr style="border-collapse:collapse"> 
              <td align="left" style="padding:0;Margin:0;padding-left:30px;padding-right:30px"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                <tbody>
                 <tr style="border-collapse:collapse"> 
                  <td valign="top" align="center" style="padding:0;Margin:0;width:540px"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                    <tbody>
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;padding-top:5px;font-size:0"> 
                       <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                        <tbody>
                         <tr style="border-collapse:collapse"> 
                          <td style="padding:0;Margin:0;border-bottom:1px solid #DADADA;background:#FFFFFFnone repeat scroll 0% 0%;height:1px;width:100%;margin:0px"></td> 
                         </tr> 
                        </tbody>
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse"> 
                      <td class="es-m-txt-l" align="center" bgcolor="#cccccc" style="padding:0;Margin:0"><h3 style="Margin:0;line-height:30px;mso-line-height-rule:exactly;font-family:arial, helvetica neue, helvetica, sans-serif;font-size:20px;font-style:normal;font-weight:normal;color:#666666">Declaration</h3></td> 
                     </tr> 
                     <tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-top:15px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:21px;color:#333333">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; hereby declare that all the above said information is correct as per my knowledge and I take the responsibility of correctness of the Information.</p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse"> 
                      <td align="right" style="padding:0;Margin:0;padding-top:15px;padding-bottom:40px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:21px;color:#333333">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Thanks You<br>'.$fullname.'</p></td> 
                     </tr> 
                    </tbody>
                   </table></td> 
                 </tr> 
                </tbody>
               </table></td> 
             </tr> 
            </tbody>
           </table></td> 
         </tr> 
        </tbody>
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-footer" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"> 
        <tbody>
         <tr style="border-collapse:collapse"> 
          <td align="center" style="padding:0;Margin:0"> 
           <table class="es-footer-body" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#9AAEA6;width:600px" cellspacing="0" cellpadding="0" bgcolor="#9aaea6" align="center"> 
            <tbody>
             <tr style="border-collapse:collapse"> 
              <td align="left" style="padding:20px;Margin:0"> 
               <table cellspacing="0" cellpadding="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                <tbody>
                 <tr style="border-collapse:collapse"> 
                  <td align="left" style="padding:0;Margin:0;width:560px"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                    <tbody>
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;font-size:0px"><img class="adapt-img" src="https://iscfig.stripocdn.email/content/guids/fe897b45-9ab4-4368-a713-8065abd12556/images/76381602072106365.png" alt="" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></td> 
                     </tr> 
                    </tbody>
                   </table></td> 
                 </tr> 
                </tbody>
               </table></td> 
             </tr> 
            </tbody>
           </table></td> 
         </tr> 
        </tbody>
       </table></td> 
     </tr> 
    </tbody>
   </table> 
  </div>  
 
</body>
</html>
';
$text = '<div>Section 1 text</div>';
$mpdf->WriteHTML($message);
ob_clean();flush();

// Output a PDF file directly to the browser
$mpdf->Output();


?>