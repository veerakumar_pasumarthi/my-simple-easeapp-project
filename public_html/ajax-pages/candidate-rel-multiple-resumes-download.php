<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * Candidate related All Documents Download
 *
 */
 


$ea_maintenance_mode = false; 


//Get Maintenance State Details of REST Services
$ea_maintanance_mode_time = getMaintananceInfo();

//Check maintenance mode
if ($ea_maintanance_mode_time != "") {
	$ea_maintenance_mode = true;	
	//echo "Maintenance mode (true): " . $ea_maintenance_mode . "\n";
}//close of if ($ea_maintanance_mode_time != "") {

//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	echo "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	exit;
}//close of if ($ea_maintenance_mode) {


$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();



//SAMPLE URL, WITH INVALID SIGNATURE
//https://api-devjobs.securitywonks.net/viewer/1/candidate-resumes/50/download/expires/1548658056/link-signature/UWN8hiLjt9CzoEkwKNV21mVS4xLOxkuez6m1s33Dfk=


//Include maintenance Check
//include expires check


$viewer_id_input = $routing_eng_var_2;
//echo "viewer_id_input: " . $viewer_id_input . "<br>";
$candidate_resumes_zip_file_id_input = $routing_eng_var_4;
//echo "candidate_resumes_zip_file_id_input: " . $candidate_resumes_zip_file_id_input . "<br>";
$expires_at_input = $routing_eng_var_7;
//echo "expires_at_input: " . $expires_at_input . "<br>";
$link_signature_input = $routing_eng_var_9;
//echo "link_signature_input: " . $link_signature_input . "<br>";

$candidate_rel_uploaded_resumes_details_result = candidate_rel_uploaded_resume_details_based_on_zip_file_id($candidate_resumes_zip_file_id_input);


	if (count($candidate_rel_uploaded_resumes_details_result) > 0) {

	$crzfr_id = $candidate_rel_uploaded_resumes_details_result["crzfr_id"];
	$zip_file_name = $candidate_rel_uploaded_resumes_details_result["zip_file_name"];
	//$sm_memb_id = $candidate_rel_uploaded_resumes_details_result["sm_memb_id"];
	//Get Active Token Details of the Admin User / Viewer
	$user_rel_active_jwt_token_details_result = ea_get_user_rel_active_jwt_token_details($viewer_id_input);
	

		foreach ($user_rel_active_jwt_token_details_result as $user_rel_active_jwt_token_details_result_row) {
			
			$user_auth_token_id = $user_rel_active_jwt_token_details_result_row["user_auth_token_id"];
			
			//https://api-devjobs.securitywonks.net/viewer/1/candidate-resumes/50/download/expires/1548658056/link-signature/UWN8hiLjt9CzoEkwKNV21mVS4xLOxkuez6m1s33Dfk=
			
			$expiring_file_url_without_signature = $site_url_project_main . "viewer/" . $viewer_id_input . "/candidate-resumes/" . $candidate_resumes_zip_file_id_input . "/download/expires/" . $expires_at_input;

			//echo "expiring_file_url_without_signature: " . $expiring_file_url_without_signature . "<br>";
			
			//Collect the Base64 Decoded version of the Expiring Link Secret Key
			$expiring_link_secret_base64_decoded = base64_decode($expiring_link_secret_key);
			
			
			//Re-Create Signature, using the extracted Header and Payload of the received JWT Auth Token
			$created_hash_for_verification = hash_hmac($expiring_link_hash_algorithm, $user_auth_token_id . $expiring_file_url_without_signature, $expiring_link_secret_base64_decoded, true);
			
			//Base64 Encode the Created Hash
			//$created_hash_for_verification_base64_encoded = base64_encode($created_hash_for_verification);


			//As per Base64 URL Encoding Concept that is described in https://tools.ietf.org/html/rfc4648#page-7
			//http://stackoverflow.com/a/11449627
			
			//Base64 URL Encode the Created Hash
			$created_hash_for_verification_base64_urlencoded = base64url_encode($created_hash_for_verification);

			//Do Remove padding (=), from the Base64 URL Encoded Hash
			$created_hash_for_verification_base64_urlencoded_after_removing_padding = str_replace("=", "", $created_hash_for_verification_base64_urlencoded);
			
			//echo "created_hash_for_verification_base64_urlencoded_after_removing_padding: " . $created_hash_for_verification_base64_urlencoded_after_removing_padding . "<br>";
			
			//Do Compare if the Created Hash equals received Link's Signature, in a timing safe comparison approach
			if ((function_exists('hash_equals')) && (hash_equals($link_signature_input, $created_hash_for_verification_base64_urlencoded_after_removing_padding))) {
			
				//Valid Signature Scenario
				
				//echo "Valid Signature<br>";
				
						
				/* //Resume Files Archive Folder and corresponding File Type Details
				$resume_folder_absolute_path = dirname(dirname(__FILE__)) . "/uploaded-documents-archive/";
				$resume_folder_relative_path = "uploaded-documents-archive/";
				
				$archive_relative_path = $resume_folder_relative_path . $created_referencing_filename;
				$archive_absolute_path = $site_home_path . $resume_folder_absolute_path . $created_referencing_filename;
				 */
				$zip_file_name = $zip_file_name;
				$candidate_uploaded_file_rel_relative_path = $candidate_document_archive_main_folder_rel_download_path . $zip_file_name;
				
				$archive_file_absolute_path = $site_home_path . "uploaded-documents-archive/" . $zip_file_name;
				
				
				//Do enumerate the File Size in bytes, kilo bytes, mega bytes
				$file_size_bytes = filesize($archive_file_absolute_path);
				$file_size_kilo_bytes = $file_size_bytes*1000;
				$file_size_mega_bytes = $file_size_kilo_bytes*1000;
				
				//echo "candidate_uploaded_file_rel_relative_path: " . $candidate_uploaded_file_rel_relative_path . "<br>";
				//echo "archive_file_absolute_path: " . $archive_file_absolute_path . "<br>";
				
				
				
				
				if(file_exists($candidate_uploaded_file_rel_relative_path)) {
					//echo "file exists";
					$fileSize = $file_size_bytes;   
					header("Cache-Control: private");
					//header("Content-Type: application/stream");
					header('Content-type:application/octet-stream');
					//header('content-Transfer-Encoding:binary');
					header("Content-Length: ".$fileSize);
					header("Content-Disposition: attachment; filename=".$zip_file_name);
					// Output file.
					readfile ($candidate_uploaded_file_rel_relative_path);                   
					exit();
				} else {
					echo "file does not exist";
				}//close of if(file_exists($candidate_uploaded_file_rel_relative_path)) {
				
					
				
				
				
				
				
				
			} else {
				//Invalid Expiring Link Signature
				echo "Invalid Signature";
			}//close of else of if ((function_exists('hash_equals')) && (hash_equals($link_signature_input, $created_hash_for_verification_base64_urlencoded_after_removing_padding))) {
			
			
		}//close of foreach ($user_rel_active_jwt_token_details_result as $user_rel_active_jwt_token_details_result_row) {

	}
function candidate_rel_uploaded_resume_details_based_on_zip_file_id($zip_file_id_input) {
    global $dbcon;
	$constructed_array = array();
	
	$candidate_rel_uploaded_resume_details_based_on_zip_file_id_sql = "SELECT * FROM `created_resume_zip_file_refs` WHERE `crzfr_id` = :crzfr_id";
	$candidate_rel_uploaded_resume_details_based_on_zip_file_id_query = $dbcon->prepare($candidate_rel_uploaded_resume_details_based_on_zip_file_id_sql);
	$candidate_rel_uploaded_resume_details_based_on_zip_file_id_query->bindValue(":crzfr_id",$zip_file_id_input);	
	$candidate_rel_uploaded_resume_details_based_on_zip_file_id_query->execute(); 
	
	if($candidate_rel_uploaded_resume_details_based_on_zip_file_id_query->rowCount() > 0) {
		$candidate_rel_uploaded_resume_details_based_on_zip_file_id_query_result = $candidate_rel_uploaded_resume_details_based_on_zip_file_id_query->fetch();
	     return $candidate_rel_uploaded_resume_details_based_on_zip_file_id_query_result;
	
	}//close of if($candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query->rowCount() > 0) {
	return $constructed_array;
}



?>