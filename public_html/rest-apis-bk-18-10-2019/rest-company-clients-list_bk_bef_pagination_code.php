<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Companies, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs
			
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$client_company_status_input = trim(isset($ea_received_rest_ws_raw_array_input['client_company_status']) ? filter_var($ea_received_rest_ws_raw_array_input['client_company_status'], FILTER_SANITIZE_NUMBER_INT) : '');
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
			
				$eventLog->log("company_id_input -> " . $company_id_input);
				$eventLog->log("client_company_status_input -> " . $client_company_status_input);
				$eventLog->log("ip_address_input -> " . $ip_address_input);
				
				//Check if all inputs are received correctly from the REST Web Service
				
				if (($client_company_status_input != "0") && ($client_company_status_input != "1") && ($client_company_status_input != "")) {
					//Invalid company Status scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-company-status";
					$response['status_description'] = "Invalid Company Status info submitted, please check and try again.";
					
					$eventLog->log("Please provide a valid Company Status info.");
					
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-additional-information";
					$response['status_description'] = "Some Additional Information like IP Address (IPv4) is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");
							  
				} else {
					//All inputs are Valid
				
					$eventLog->log("All inputs are valid.");
				
					try { 
					    /*
						//displaying all companies irrespctive of  company status
						$company_details_result = get_company_details_based_on_company_id($company_id_input);
						$received_company_status = $company_details_result["is_active_status"];
						
						if ($received_company_status == "0") {
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "inactive-company-id-submitted";
							$response['status_description'] = "In-active Company ID Submitted, please check and try again.";
							
							$eventLog->log("inactive-company-id-submitted: In-active Company ID Submitted, please check and try again.");
							
						}//close of if ($received_company_status == "0") {
						*/
						
						if ($ea_extracted_jwt_token_user_company_id != "") {
							//Do Get User Groups List, w.r.t. specific Company ID from sm_site_member_classification_details db table
							
							if ($client_company_status_input == "") {
								$client_companies_list_result = client_companies_list_specific_company_user_based_on_company_id_input($ea_extracted_jwt_token_user_company_id,null);
							} else {
								$client_companies_list_result = client_companies_list_specific_company_user_based_on_company_id_input($ea_extracted_jwt_token_user_company_id,$client_company_status_input);
							}
							/*$client_companies_list_result = client_companies_list_specific_company_user_based_on_company_id_input($ea_extracted_jwt_token_user_company_id,$client_company_status_input);*/
							
						} else {
							//Do Get User Groups List, from sm_site_member_classification_details db table
							
							if ($client_company_status_input == "") {
								$client_companies_list_result = client_companies_list_specific_company_user_based_on_company_id_input($company_id_input,null);
							} else {
								$client_companies_list_result = client_companies_list_specific_company_user_based_on_company_id_input($company_id_input,$client_company_status_input);
							}
							/*$client_companies_list_result = client_companies_list_specific_company_user_based_on_company_id_input($company_id_input,$client_company_status_input);*/
							
						}//close of else of if ($ea_extracted_jwt_token_user_company_id != "") {
						
						if (count($client_companies_list_result) > 0) {
							//One or More User Groups Exist and Active
							
							$response['data'] = $client_companies_list_result;
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['status'] = "client-companies-list-received";
							$response['status_description'] = "Client Companies List Successfully Received";
							
							$client_companies_list_result_json_encoded = json_encode($client_companies_list_result);
						
							$eventLog->log("companies List -> " . $client_companies_list_result_json_encoded); 
							
						} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "active-client-companies-doesnot-exist";
							$response['status_description'] = "No Active Client companies list Exist for your company, please check and try again.";
							
							//No Active company list Exist
							$eventLog->log("Companies List -> No Active Client companies Exist for your company, please check and try again."); 
						}//close of else of if ($company_list_result_count > "0") {
						
					} catch (Exception $e){
						$eventLog->log("Exception -> " . html_escaped_output($e->getMessage())); 
						//addLog($logFile, "Exception -> ".$e->getMessage());	
					}//close of  catch (Exception $e){
					
				
				}//close of if (($company_status_input != "0") && ($company_status_input != "1") && ($company_status_input != "")) {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){

exit;
?>