<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {

	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
			    $job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
			    $classified_user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['classified_user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['classified_user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
			    $page_number_input = trim(isset($ea_received_rest_ws_raw_array_input['page_number']) ? filter_var($ea_received_rest_ws_raw_array_input['page_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				$number_of_records_input = trim(isset($ea_received_rest_ws_raw_array_input['number_of_records']) ? filter_var($ea_received_rest_ws_raw_array_input['number_of_records'], FILTER_SANITIZE_NUMBER_INT) : '');
				$sort_field_input = trim(isset($ea_received_rest_ws_raw_array_input['sort_field']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_field'], FILTER_SANITIZE_STRING) : '');
				$sort_order_input = trim(isset($ea_received_rest_ws_raw_array_input['sort_order']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_order'], FILTER_SANITIZE_STRING) : '');
				$search_criteria_input = trim(isset($ea_received_rest_ws_raw_array_input['search_criteria']) ? filter_var($ea_received_rest_ws_raw_array_input['search_criteria'], FILTER_SANITIZE_STRING) : '');
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {

                if ($job_id_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-id";
					$response['status_description'] = "Missing Job Id";
					
					$eventLog->log("missing-user-id: Missing Job Id.");
					
		/* 		} else if ($company_id_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company Id";
					
					$eventLog->log("missing-company-id: Missing Company Id."); */
					
				} else if ($page_number_input == "0") {
					//Invalid Page Number scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-page-number";
					$response['status_description'] = "Invalid Page Number info submitted, please check and try again.";
					
					$eventLog->log("invalid-page-number: Invalid Page Number info submitted, please check and try again.");
					
				} else if (($sort_order_input != "ASC") && ($sort_order_input != "DESC") && ($sort_order_input != "")) {
					//Invalid Sorting Order scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-Sorting-order";
					$response['status_description'] = "Invalid Sorting Order info submitted, please check and try again.";
					
					$eventLog->log("invalid-Sorting-order: Invalid Sorting Order info submitted, please check and try again.");				
				
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else {
					//all inputs are valid
					try {
						
						if ($ea_extracted_jwt_token_user_company_id == "") {
							//action taker is of platform scope - super admin / site admin scenario
							$job_sent_invitations_list_info_result = job_sent_invitations_list_info_with_pagination_inputs($company_id_input,$job_id_input,$classified_user_id_input,$page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input);
							
						
						} else {
						   //action taker is of company scope - company admin, legal / immigration, internal admin etc scenario
							$job_sent_invitations_list_info_result = job_sent_invitations_list_info_with_pagination_inputs($ea_extracted_jwt_token_user_company_id,$job_id_input,$classified_user_id_input,$page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input);
							//get_candidates_list_with_pagination_inputs($ea_extracted_jwt_token_user_company_id,$sm_user_status_input, $sm_user_type_input, $page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input);
						   
						}
						
						$job_sent_invitations_list_info_result_count = count($job_sent_invitations_list_info_result);
						
						$eventLog->log("Count -> " . $job_sent_invitations_list_info_result_count);
						
						if (count($job_sent_invitations_list_info_result) > 0) {
						
						$response['data'] = $job_sent_invitations_list_info_result;
						$response['status'] = "job-sent-invitations-list-successfully-fetched";
						$response['status_description'] = "Job sent invitations List Successfully Received";
					
						} else {
							
							$response['data'] = array();
							$response['status'] = "no-sent-invitations-found";
							$response['status_description'] = "No sent invitations found for this job";
							
						}
							
							
					} catch (Exception $e){
						
						$response['data'] = array();
						$response['status'] = "job-sent-invitations-list-fetching-error";
						$response['status_description'] = "Job sent invitations List Fetching Error";
					}
					
					
				}
					
			}
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
	


function  job_sent_invitations_list_info_with_pagination_inputs($company_id_input,$job_id_input,$classified_user_id_input,$page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input) {

    global $dbcon, $default_number_of_records_pagination; //app/core/main-config.php
	global $eventLog;
	$constructed_array = array();
	$eventLog->log("before is_null concept");

	if ($search_criteria_input == "") {
		$search_criteria_input = null;
	}//close of if ($search_criteria_input == "") {
	if ($sort_field_input == "") {
		$sort_field_input = null;
	}	
	
	if ($classified_user_id_input == "") {
		
		$classified_user_add_sub_query = "";
		
	} else {
		
		$classified_user_add_sub_query = " AND jai.invitation_sent_by_user_id = :classified_user_id ";
	}
	

	if (!is_null($search_criteria_input)) {
		//When Search Criteria input is not empty
		$search_criteria_variable = "%" . $search_criteria_input . "%";
		

	    //AND ((jai.job_id LIKE :job_id_search_keyword) OR (jai.company_client_id LIKE :company_client_id_search_keyword) OR (j.job_title LIKE :job_title_search_keyword) OR (jai.invitation_sent_by_user_id LIKE :invitation_sent_by_user_id) OR (jai.event_date_time LIKE :invitation_initiate_at_search_keyword) OR (jai.email_sent_date_time LIKE :invite_email_sent_at_search_keyword) OR (jai.sms_sent_date_time LIKE :invite_sms_sent_at_search_keyword) OR (jai.is_active_status LIKE :status_search_keyword) OR (jai.invite_ref_code LIKE :invite_ref_code_search_keyword))  ORDER BY jai.job_applicant_invite_id ASC
		$search_criteria_in_query_with_where_keyword = " WHERE ((jai.invitation_received_by_user_id LIKE :invitation_received_by_user_id_search_keyword) OR (jai.company_client_id LIKE :company_client_id_search_keyword) OR (jai.invitation_sent_by_user_id LIKE :invitation_sent_by_search_keyword) OR (sm.sm_firstname LIKE :candidate_first_name_search_keyword) OR (sm.sm_middlename LIKE :candidate_middle_name_search_keyword) OR (sm.sm_lastname LIKE :candidate_last_name_search_keyword) OR  (jai.event_date_time LIKE :invitation_initiate_at_search_keyword) OR  (jai.invite_fulfillment_status LIKE :invite_fulfillment_status_search_keyword) OR (jai.is_active_status LIKE :status_search_keyword))";
		
		$eventLog->log("after search criteria concept");
		$search_criteria_in_query_without_where_keyword = " AND ((jai.invitation_received_by_user_id LIKE :invitation_received_by_user_id_search_keyword) OR (jai.company_client_id LIKE :company_client_id_search_keyword) OR (jai.invitation_sent_by_user_id LIKE :invitation_sent_by_search_keyword) OR (sm.sm_firstname LIKE :candidate_first_name_search_keyword) OR (sm.sm_middlename LIKE :candidate_middle_name_search_keyword) OR (sm.sm_lastname LIKE :candidate_last_name_search_keyword) OR  (jai.event_date_time LIKE :invitation_initiate_at_search_keyword) OR  (jai.invite_fulfillment_status LIKE :invite_fulfillment_status_search_keyword) OR (jai.is_active_status LIKE :status_search_keyword) ";
	    $eventLog->log("after search criteria without where concept");
	} else {
		//When Search Criteria input is empty
		$search_criteria_in_query_with_where_keyword = " WHERE ";

		$search_criteria_in_query_without_where_keyword = "";

	}//close of else of if (!is_null($search_criteria_input)) {
	$eventLog->log("before sort order concept");
	
	if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {

		if (!is_null($sort_field_input)) {
			//When the Sort Field input is not empty

			if ($sort_field_input == "invitation_received_by_user_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY jai.invitation_received_by_user_id " . $sort_order_input;

			} else if ($sort_field_input == "company_client_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY company_client_id " . $sort_order_input;

			
            } else if ($sort_field_input == "invitation_sent_by_user_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY jai.invitation_sent_by_user_id " . $sort_order_input;

			} else if ($sort_field_input == "candidate_first_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_firstname " . $sort_order_input;
				
			} else if ($sort_field_input == "invitation_initiate_at") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY jai.event_date_time " . $sort_order_input;

			} else if ($sort_field_input == "invitation_fulfillment_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY jai.invite_fulfillment_status " . $sort_order_input;

			} else if ($sort_field_input == "job_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `is_active_status` " . $sort_order_input;

			} else {
				//When Sort Field input, is not among the expected values, use primary key, with provided Sorting Order input
				$sort_details_in_query = " ORDER BY jai.job_applicant_invite_id " . $sort_order_input;
			}

		} else {
			//When the Sort Field input is empty, primary key has to be taken, with provided Sorting Order input
			$sort_details_in_query = " ORDER BY jai.job_applicant_invite_id " . $sort_order_input;

		}//close of if (!is_null($sort_field_input)) {


	} else {
		//When the Sort Field input is empty, primary key has to be taken, with ASC Order, by default
		$sort_details_in_query = " ORDER BY `job_applicant_invite_id` ASC ";

	}//close of else of if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {


	$limit_offset_value = 0;

	if (($page_number_input == "") && ($number_of_records_input == "")) {
		//Give All Data
		//No LIMIT Clause in the Query
		$limit_offset_in_query = "";

	} else if (($page_number_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than page_number_input, till the number of records, as defined in the $default_number_of_records_pagination

		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $default_number_of_records_pagination;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$default_number_of_records_pagination;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $default_number_of_records_pagination;
		}//close of else if of if ($page_number_input == "1") {



	} else if (($page_number_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records
		$limit_offset_in_query = " LIMIT " . $number_of_records_input;

	} else {
		//Give Data, from given Record, till the requested number of records
		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $number_of_records_input;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$number_of_records_input;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $number_of_records_input;
		}//close of else if of if ($page_number_input == "1") {

	}//close of else of if (($page_number_input == "") && ($number_of_records_input == "")) {

    if (!is_null($search_criteria_input)) {
		
		
			
		    $job_sent_invitations_list_named_parameters_values_array_input = array(":company_id" => $company_id_input,":job_id_input" => $job_id_input,":company_client_id_search_keyword" => $search_criteria_variable,":invitation_received_by_user_id" => $search_criteria_variable, ":invitation_sent_by_search_keyword" => $search_criteria_variable,":candidate_first_name_search_keyword" => $search_criteria_variable, ":candidate_middle_name_search_keyword"=>$search_criteria_variable, ":candidate_last_name_search_keyword" => $search_criteria_variable, ":invitation_initiate_at_search_keyword" => $search_criteria_variable, ":invite_fulfillment_status_search_keyword"=> $search_criteria_variable, ":status_search_keyword" => $search_criteria_variable);    

			if($classified_user_id_input != ""){
				$job_sent_invitations_list_named_parameters_values_array_input[":classified_user_id"] = $classified_user_id_input;
			}
			
			$job_sent_invitations_list_count_get_sql = "SELECT COUNT(*) FROM `job_applicant_invites` jai JOIN `company_clients` cc ON  jai.company_client_id = cc.company_client_id JOIN `site_members` sm  ON  jai.invitation_received_by_user_id=sm.sm_memb_id WHERE jai.company_id=:company_id AND jai.job_id=:job_id_input" . $classified_user_add_sub_query . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;
			$eventLog->log($job_sent_invitations_list_count_get_sql);
			$job_sent_invitations_list_count_get_select_query = $dbcon->prepare($job_sent_invitations_list_count_get_sql);
			$job_sent_invitations_list_count_get_select_query->execute($job_sent_invitations_list_named_parameters_values_array_input);

			//Get Jobs List
			
			
			$job_sent_invitations_list_info_sql = "SELECT * FROM `job_applicant_invites` jai JOIN `company_clients` cc ON  jai.company_client_id = cc.company_client_id JOIN `site_members` sm  ON  jai.invitation_received_by_user_id=sm.sm_memb_id WHERE jai.company_id=:company_id AND jai.job_id=:job_id_input" . $classified_user_add_sub_query . $search_criteria_in_query_without_where_keyword . $sort_details_in_query . $limit_offset_in_query;
            $eventLog->log($job_sent_invitations_list_info_sql);
			$job_sent_invitations_list_get_select_query = $dbcon->prepare($job_sent_invitations_list_info_sql);
			$job_sent_invitations_list_get_select_query->execute($job_sent_invitations_list_named_parameters_values_array_input);

	} else {
			
			$job_sent_invitations_list_named_parameters_values_array_input = array(":company_id" => $company_id_input,":job_id_input" => $job_id_input);
			
			if($classified_user_id_input != ""){
				$job_sent_invitations_list_named_parameters_values_array_input[":classified_user_id"] = $classified_user_id_input;
			}
			//$candiate_invited_jobs_list_count_get_sql = "SELECT COUNT(*) FROM `job_applicant_invites` jai JOIN `jobs` j  ON jai.job_id=j.job_id WHERE j.company_id=:company_id AND jai.invitation_received_by_user_id=:invitation_received_by_user_id " . $sort_details_in_query;
			$job_sent_invitations_list_count_get_sql = "SELECT COUNT(*) AS count FROM `job_applicant_invites` jai JOIN `company_clients` cc ON  jai.company_client_id = cc.company_client_id JOIN `site_members` sm  ON  jai.invitation_received_by_user_id=sm.sm_memb_id WHERE jai.company_id=:company_id AND jai.job_id=:job_id_input" . $classified_user_add_sub_query . $sort_details_in_query;
			$eventLog->log($job_sent_invitations_list_count_get_sql);
			$job_sent_invitations_list_count_get_select_query = $dbcon->prepare($job_sent_invitations_list_count_get_sql);
			$job_sent_invitations_list_count_get_select_query->execute($job_sent_invitations_list_named_parameters_values_array_input);

			//Give List of Jobs, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			//$candiate_invited_jobs_list_info_sql = "SELECT * FROM `job_applicant_invites` jai JOIN `jobs` j  ON jai.job_id=j.job_id WHERE j.company_id=:company_id AND jai.invitation_received_by_user_id=:invitation_received_by_user_id " . $sort_details_in_query . $limit_offset_in_query;
			$job_sent_invitations_list_info_sql = "SELECT * FROM `job_applicant_invites` jai JOIN `company_clients` cc ON  jai.company_client_id = cc.company_client_id JOIN `site_members` sm  ON  jai.invitation_received_by_user_id=sm.sm_memb_id WHERE jai.company_id=:company_id AND jai.job_id=:job_id_input" . $classified_user_add_sub_query . $sort_details_in_query . $limit_offset_in_query;
			$job_sent_invitations_list_get_select_query = $dbcon->prepare($job_sent_invitations_list_info_sql);
			$job_sent_invitations_list_get_select_query->execute($job_sent_invitations_list_named_parameters_values_array_input);

	}//close of else of if (!is_null($search_criteria_input)) {

	

	//Process / Get Companies Count
	if($job_sent_invitations_list_count_get_select_query->rowCount() > 0) {
	    $job_sent_invitations_list_count_get_select_query_result = $job_sent_invitations_list_count_get_select_query->fetch();
	    //print_r($jobs_list_count_get_select_query_result);

		//$total_candiate_invited_jobs_count = $candiate_invited_jobs_list_count_get_select_query_result["count"];
		$total_job_sent_invitations_list_count = $job_sent_invitations_list_count_get_select_query_result["count"];
		$constructed_array["total_records_count"] = $total_job_sent_invitations_list_count;

	}//close of if($jobs_list_count_get_select_query->rowCount() > 0) {

	//Process / Fetch Companies List
	if($job_sent_invitations_list_get_select_query->rowCount() > 0) {
	    $job_sent_invitations_list_get_select_query_result = $job_sent_invitations_list_get_select_query->fetchAll();
	    //print_r($companies_list_get_select_query_result);
		

		foreach ($job_sent_invitations_list_get_select_query_result as $job_sent_invitations_list_get_select_query_result_row ) {
			
			$temp_row_array = array();
			$temp_row_array["job_applicant_invite_id"] = $job_sent_invitations_list_get_select_query_result_row["job_applicant_invite_id"];
		    $temp_row_array["company_id"] = $job_sent_invitations_list_get_select_query_result_row["company_id"];
		    $temp_row_array["company_client_id"] = $job_sent_invitations_list_get_select_query_result_row["company_client_id"];
			$temp_row_array["company_client_name"] = $job_sent_invitations_list_get_select_query_result_row["client_company_name"];
			$temp_row_array["job_id"] = $job_sent_invitations_list_get_select_query_result_row["job_id"];
			
			$temp_row_array["invitation_received_by_user_id"] = $job_sent_invitations_list_get_select_query_result_row["invitation_received_by_user_id"];
			$temp_row_array["candidate_first_name"] = $job_sent_invitations_list_get_select_query_result_row["sm_firstname"];
			$temp_row_array["candidate_middle_name"] = $job_sent_invitations_list_get_select_query_result_row["sm_middlename"];
			$temp_row_array["candidate_last_name"] = $job_sent_invitations_list_get_select_query_result_row["sm_lastname"];
			
			$temp_row_array["candidate_full_name"] = $temp_row_array["candidate_first_name"] . " " . $temp_row_array["candidate_middle_name"] . " " .$temp_row_array["candidate_last_name"];
			
			$temp_row_array["invitation_sent_by"] = $job_sent_invitations_list_get_select_query_result_row["invitation_sent_by_user_id"];
			$user_basic_details_result = user_basic_details_check_based_on_user_id($temp_row_array["invitation_sent_by"]);
				$temp_row_array["sm_firstname"] = $user_basic_details_result["sm_firstname"];
				$temp_row_array["sm_middlename"] = $user_basic_details_result["sm_middlename"];
				$temp_row_array["sm_lastname"] = $user_basic_details_result["sm_lastname"];
				$temp_row_array["invitation_sent_by_name"] = $temp_row_array["sm_firstname"] . " " . $temp_row_array["sm_middlename"] . " " .$temp_row_array["sm_lastname"];
			
			$temp_row_array["invitation_initiate_at"] = $job_sent_invitations_list_get_select_query_result_row["event_date_time"];
			
			$temp_row_array["invitation_fulfillment_status"] = $job_sent_invitations_list_get_select_query_result_row["invite_fulfillment_status"];
			$temp_row_array["status"] = $job_sent_invitations_list_get_select_query_result_row["is_active_status"];
			
	        $constructed_array["list"][] = $temp_row_array;
	    }
			
	}//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {
	return $constructed_array;
}


exit;
?>