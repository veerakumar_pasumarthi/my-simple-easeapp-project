
<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "6")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
						
					if (isset($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'])) 
			
					if (isset($ea_received_rest_ws_raw_array_input['event_status'])) {
						$content .= $ea_received_rest_ws_raw_array_input['event_status'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['event_status'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['notes'])) {
						$content .= $ea_received_rest_ws_raw_array_input['notes'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['notes']))
					
					if (isset($ea_received_rest_ws_raw_array_input['file_rel_original_filename'])) {
						$content .= $ea_received_rest_ws_raw_array_input['file_rel_original_filename'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['file_rel_original_filename'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['file_content'])) {
						//$content .= $ea_received_rest_ws_raw_array_input['file_content'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['file_content']))
					
					
					
					
				}	

					
					$jsl_applicant_evaluation_info_id_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					$event_status_id_input = trim(isset($ea_received_rest_ws_raw_array_input['event_status']) ? filter_var($ea_received_rest_ws_raw_array_input['event_status'], FILTER_SANITIZE_NUMBER_INT) : '');
					$notes_input = trim(isset($ea_received_rest_ws_raw_array_input['notes']) ? filter_var($ea_received_rest_ws_raw_array_input['notes'], FILTER_SANITIZE_STRING) : '');
					
					$file_rel_original_filename_input = trim(isset($ea_received_rest_ws_raw_array_input['file_rel_original_filename']) ? filter_var($ea_received_rest_ws_raw_array_input['file_rel_original_filename'], FILTER_SANITIZE_STRING) : '');
					
					$file_content_input = trim(isset($ea_received_rest_ws_raw_array_input['file_content']) ? filter_var($ea_received_rest_ws_raw_array_input['file_content'], FILTER_SANITIZE_STRING) : '');
					
					
					$jsl_evaluation_info_result = jsl_evaluation_info_get($jsl_applicant_evaluation_info_id_input);
				
					$company_id_input = $jsl_evaluation_info_result["company_id"];
					$company_client_id_input = $jsl_evaluation_info_result["company_client_id"];
					$job_id_input = $jsl_evaluation_info_result["job_id"];
					$job_applicant_sm_memb_id_input = $jsl_evaluation_info_result["job_applicant_sm_memb_id"];
					
				
					if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
					} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
					}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					
					
					  //invalid company_id scenario
					
					 if ($jsl_applicant_evaluation_info_id_input == "") {
						
						$response['data'] = array();
						$response['status'] = "missing-jsl-applicant-evaluation-info-id";
						$response['status_description'] = "Missing JSL applicant evaluation info Id";
						
						$eventLog->log("Missing JSL applicant evaluation info Id.");
						
					
					} else if ($event_status_id_input == "") {
						
						$response['data'] = array();
						$response['status'] = "missing-event-status-input";
						$response['status_description'] = "Missing event status input";
						
						$eventLog->log("Missing event status input.");
					
					} else {	
						
							$job_rel_screening_level_add_next_step = "";
							if ($ea_extracted_jwt_token_user_type == "admin") {
								
								if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
									$job_rel_screening_level_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("before company id condition");
								} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
									
									$job_rel_screening_level_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("after add condition");
								} else {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "insufficient-permissions-different-company-user-id";
									$response['status_description'] = "Insufficient Set of Permissions";
									
									//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
									header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
									$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
									
								}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								
								
								
							} else {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
								$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
							
							}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
								
							$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
							$notes_safe_html_input = get_cleaned_safe_html_content_input($notes_input, $page_content_file_config);
							$eventLog->log($notes_safe_html_input);
							
							
							if ($job_rel_screening_level_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
								try {
									
									
									$jsl_applicant_evaluation_status_update_reuslt =
									jsl_applicant_evaluation_status_update($jsl_applicant_evaluation_info_id_input,$event_status_id_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
									
									if ($jsl_applicant_evaluation_status_update_reuslt == true) {
										
										
										if($notes_input != "") {
											$jsl_applicant_evaluation_info_result_notes_add_result =
											jsl_applicant_evaluation_info_result_notes_add($company_id_input,$company_client_id_input,$job_id_input,$job_applicant_sm_memb_id_input,$jsl_applicant_evaluation_info_id_input,$notes_input,$notes_safe_html_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
										}
										if($file_rel_original_filename_input != "") {
											
											//data:image/png;base64,iVBORw0KGgoAAAANSUhEUgA

											//Extract the received file content, and mime type parts
											$file_content_input_semi_colon_base64_comma_exploded = explode(";base64,",$file_content_input);
											
											if (count($file_content_input_semi_colon_base64_comma_exploded) == "2") {
												
												$uploaded_file_extension_rel_mime_type_and_data = $file_content_input_semi_colon_base64_comma_exploded[0];
												$uploaded_file_base64_encoded_data = $file_content_input_semi_colon_base64_comma_exploded[1];
												
												//Extract mime type from the mime type parts
												$uploaded_file_extension_rel_mime_type_and_data_exploded = explode(":",$uploaded_file_extension_rel_mime_type_and_data);
												
												if (count($uploaded_file_extension_rel_mime_type_and_data_exploded) == "2") {
													$uploaded_file_extension_rel_mime_type_and_data_first_part = $uploaded_file_extension_rel_mime_type_and_data_exploded[0];
													$uploaded_file_mime_type = $uploaded_file_extension_rel_mime_type_and_data_exploded[1];
													
													$eventLog->log("uploaded_file_mime_type: " . $uploaded_file_mime_type);	
													
													$random_text_generation_inputs = uniqid(rand(), TRUE);
													$generated_file_name_without_file_extension = hash('sha256', $random_text_generation_inputs);
													$candidate_document_upload_folder = "./uploaded-notes/";
													
													//https://gist.github.com/raphael-riel/1253986
													$mime_types = array(
																'pdf'     => 'application/pdf',
																'doc'     => 'application/msword'
															);
															
													
														 //function for compare the mimetypes of what we allow the mime_types and upload from the user mime_type
													$extracted_file_extension_result = extract_file_extension_based_on_uploaded_file_mime_type_input($mime_types, $uploaded_file_mime_type);
													
													$eventLog->log("extracted_file_extension_result: " . $extracted_file_extension_result);
													
													if ($extracted_file_extension_result != "") {
														
														//Generated File Name
														$generated_file_name = $generated_file_name_without_file_extension . "." . $extracted_file_extension_result; 
														
														
														
														
														$crauvd_id_full_folder_absolute_path = $site_home_path . "/home/uplo-files-dev-rc/webapps/app-uplo-files/uploaded-notes";
														$eventLog->log("Full Upload Folder (with absolute path): " . $crauvd_id_full_folder_absolute_path);
														
														if(!is_dir($crauvd_id_full_folder_absolute_path)) {
														
															mkdir($crauvd_id_full_folder_absolute_path, 0755, true);
															chmod($crauvd_id_full_folder_absolute_path, 0755);
															clearstatcache();	
															
															$eventLog->log("(Absolute Path - Not a Directory Structure) Condition");
															
														} else if(!is_writable($crauvd_id_full_folder_absolute_path)) {
														
															chmod($crauvd_id_full_folder_absolute_path, 0755);
															clearstatcache();
															
															$eventLog->log("(Absolute Path - Directory Structure Not Writable) Condition");
															
														}//close of else if of if(!is_dir($site_home_path . "uploaded-documents/" . $user_id_input . "/" . $crauvd_id_folder_name)) {
														
														
														//Create Full Upload Folder related Relative Path
														$crauvd_id_full_folder_relative_path = $candidate_document_upload_main_folder . "/"; // $candidate_document_upload_main_folder from /app/core/main-config.php
														
														$eventLog->log("Full Upload Folder (with relative path): " . $crauvd_id_full_folder_relative_path);
														
														//Stored File Location
														$targetFile =  $crauvd_id_full_folder_relative_path. $generated_file_name;
														
														
														/*
														$targetFile =  $candidate_document_upload_main_folder. $generated_file_name;
														*/
														$eventLog->log("targetFile (with path): " . $targetFile);
														
														
														//Base64 Decode the received file input
														$uploaded_file_base64_decoded_data = base64_decode($uploaded_file_base64_encoded_data);
														
														//Store the Content in the FILEINFO_MIME
														file_put_contents($targetFile, $uploaded_file_base64_decoded_data);
														
														$eventLog->log("The Uploaded file is saved now");
														
														//Create Full Upload Folder related Absolute Path
														$targetFile_absolute_path = $candidate_document_upload_main_folder_rel_absolute_path . "/" . $generated_file_name; // $candidate_document_upload_main_folder from /app/core/hostname-check.php
														
														
														$eventLog->log("Full Upload Folder (with relative path): " . $crauvd_id_full_folder_relative_path);
														
														
														//Check Received File Mimetype
														$finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
														//$file_mime_type = finfo_file($finfo, $targetFile) . "\n";
														$file_mime_type = finfo_file($finfo, $targetFile);
														finfo_close($finfo);
														
														$eventLog->log("enumerated file_mime_type: " . $file_mime_type);
														
														//Check, if user submitted file mime type = enumerated file mime type
														if ($uploaded_file_mime_type == $file_mime_type) {
															
															$file_size_bytes_input = filesize($targetFile);
															$file_size_kilo_bytes_input = $file_size_bytes_input/1024;
															$file_size_mega_bytes_input = $file_size_bytes_input/(1024*1024);
															
															$eventLog->log("bytes: " . $file_size_bytes_input . " kilo bytes: " . $file_size_kilo_bytes_input . " mega bytes: " . $file_size_mega_bytes_input);
															
																//Check the File Size of the Uploaded File
																if ($file_size_mega_bytes_input > 25) {
																	
																	//Construct Content, that will be sent in Response body, of the REST Web Service
																	$response['data'] = array();
																	$response['status'] = "larger-file-uploaded";
																	$response['status_description'] = "The File Size of the Uploaded File is > 25MB. Please uploaded the File, that is Smaller in Size.";
																	
																	$eventLog->log("The File Size of the Uploaded File is > 25MB");
																	
																} else {
																	$eventLog->log("Done");
																	$last_inserted_id = jsl_applicant_evaluation_info_result_notes_rel_fileupload_add($company_id_input,$company_client_id_input,$job_id_input,$job_applicant_sm_memb_id_input,$jsl_applicant_evaluation_info_id_input,$file_rel_original_filename_input,$generated_file_name,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
																
																}//close of else of if ($file_size_mega_bytes_input > 25) {
																		
																		
														} else {
															//File Mime Type is mis-matching
															
															//Construct Content, that will be sent in Response body, of the REST Web Service
															$response['data'] = array();
															$response['status'] = "file-upload-mime-type-mismatching";
															$response['status_description'] = "File Mime Type is mis-matching, w.r.t. particular User's document reference";
															
															$eventLog->log("file-upload-mime-type-mismatching: File Mime Type is mis-matching, w.r.t. particular User's document reference, Please check and try again.");
															
														}//close of if ($uploaded_file_mime_type == $file_mime_type) {

																	
													} else {
														//Construct Content, that will be sent in Response body, of the REST Web Service
														$response['data'] = array();
														$response['status'] = "file-upload-mime-type-not-accepted";
														$response['status_description'] = "The Uploaded File's Mime Type is not in the accepted list of file types";
														
														$eventLog->log("file-upload-mime-type-not-accepted: The Uploaded File's Mime Type is not in the accepted list of file types, please check and try again.");	
													}//close of else of if ($extracted_file_extension_result != "") {
														
																
																
												} else {
													//Construct Content, that will be sent in Response body, of the REST Web Service
													$response['data'] = array();
													$response['status'] = "invalid-file-extension-input";
													$response['status_description'] = "Invalid File Extension Input";
													
													$eventLog->log("invalid-file-extension-input: Invalid info w.r.t. file extension part. Invalid File Extension Input, Please Check and try again.");	
												}//close of else of if (count($uploaded_file_extension_rel_mime_type_and_data_exploded) == "2") {
												
															
															
											} else {
												//Construct Content, that will be sent in Response body, of the REST Web Service
												$response['data'] = array();
												$response['status'] = "invalid-base64-encoded-file-input";
												$response['status_description'] = "Invalid Base64 Encoded File Input";
												
												$eventLog->log("invalid-base64-encoded-file-input: Invalid base64 encoded file content");	
											}//close of else of if (count($file_content_input_semi_colon_base64_comma_exploded) == "2") {	
											
										}
										
										
										if($jsl_applicant_evaluation_info_result_notes_add_result == "" || $last_inserted_id == ""){
											
											$response['data'] = array();
											$response['status'] = "jsl-applicant-evaluation-status-changed-successfully";
											$response['status_description'] = "JSL applicant evaluation status changed successfully";
										
										} else {
											
											$response['data'] = array();
											$response['status'] = "jsl-applicant-evaluation-status-adding-error";
											$response['status_description'] = "JSL applicant evaluation status adding error";
										
											
										}
										
									}	
									
								} catch (Exception $e) {
									
									$response['data'] = array();
									$response['status'] = "job-related-screening-level-insertion-error";
									$response['status_description'] = "Error occurred when adding the job related screening level.";
										
									$eventLog->log("job-related-screening-level-insertion-error: Error occurred when adding the job related screening level.");
										
								}
							}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
							
						}
			//}//close of else of if ($email_id_input == "") {
					
		}//close of //close of if ($ea_is_user_page_access_authorized) {
					
				
	} else {
				
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "invalid-auth-token-submitted";
				$response['status_description'] = "Invalid Authentication";
				
				//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
				header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
				
				$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
				
			}//close of else of if ($ea_auth_token_validation_status) {
		
		}//close of if ($ea_maintenance_mode == false) {
		
} else {

		//Construct Content, that will be sent in Response body, of the REST Web Service
		$response['data'] = array();
		$response['status'] = "invalid-input";
		$response['status_description'] = "Invalid Input, Please check and provide all information.";
		
		//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
		
		$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
				
		
		}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




	//Check if Maintenance Mode is Turned On
	if ($ea_maintenance_mode) {	
		
		//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
		//header('Maintenance-Progress: true', false);
		//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
		$response['data'] = array();
		$response['status'] = "application-maintenance-in-progress";
		$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
		
		$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
		
	} else {	

		//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
		header('Content-Type: application/json');
		echo json_encode($response,JSON_PRETTY_PRINT);
		
	}//close of else of if ($ea_maintenance_mode){
		
function jsl_applicant_evaluation_status_update($jsl_applicant_evaluation_info_id_input,$event_status_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch) {
	
	
	global $dbcon,$eventLog;
	
	$jsl_applicant_evaluation_status_update_sql = "UPDATE `jsl_applicant_evaluation_info` SET `event_status`= :event_status,`last_updated_datetime`=:last_updated_datetime,`last_updated_datetime_epoch`=:last_updated_datetime_epoch,`last_updated_by_sm_memb_id`= :last_updated_by_sm_memb_id WHERE `jsl_applicant_evaluation_info_id`=:jsl_applicant_evaluation_info_id";

	$jsl_applicant_evaluation_status_update_query = $dbcon->prepare($jsl_applicant_evaluation_status_update_sql);
	
	$jsl_applicant_evaluation_status_update_query->bindValue(":last_updated_datetime",$event_datetime);
	$jsl_applicant_evaluation_status_update_query->bindValue(":last_updated_datetime_epoch",$current_epoch);
	$jsl_applicant_evaluation_status_update_query->bindValue(":last_updated_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$jsl_applicant_evaluation_status_update_query->bindValue(":event_status",$event_status_input);
	
	$jsl_applicant_evaluation_status_update_query->bindValue(":jsl_applicant_evaluation_info_id",$jsl_applicant_evaluation_info_id_input);
	
	if ($jsl_applicant_evaluation_status_update_query->execute()) {
          
			return true;

	} else {
		  
			return false;
    }

}

function jsl_applicant_evaluation_info_result_notes_rel_fileupload_add($company_id_input,$company_client_id_input,$job_id_input,$job_applicant_sm_memb_id_input,$jsl_applicant_evaluation_info_id_input,$file_rel_original_filename_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch) {
	

	
}

exit;
?>