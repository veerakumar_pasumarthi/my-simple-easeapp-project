<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Edit Quick Admin Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "2")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['job_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['user_id'])) 
					/* if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) */
				
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				/* $company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : ''); */
				
				
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				
				if ($job_id_input == "") {
					//Invalid Mobile Number scenario
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-id";
					$response['status_description'] = "invalid job id, please check and try again.";
					
					$eventLog->log("Please provide a valid job id.");
					
				
				
				} /* else if ($company_id_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "missing company id, please check and try again.";
					
					$eventLog->log("Please provide all information.");
				} */else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Some Additional Information is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//Event Time, as per Indian Standard Time
					$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
		
					$company_proceed_next_step = "";
					
					
					if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$company_proceed_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$company_proceed_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
					} else {
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
					}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
							
					if ($company_proceed_next_step == "PROCEED-TO-NEXT-STEP") {
						
							try {
								    $job_recruitment_status_details_result = job_recruitment_status_details_check_based_on_job_id($job_id_input);
								    if(count($job_recruitment_status_details_result) > 0){
										  
										$response['data'] = $job_recruitment_status_details_result;
										$response['status'] = "job-recruitment-status-details-successfully-fetched";
										$response['status_description'] = "job recruitment status details fetched Successfully.";

										$eventLog->log("job recruitment status details fetched Successfully.");
													     
									} else {
												
										
												  $response['data'] = array();
												  $response['status'] = "no-records-found-with-this-id";
												  $response['status_description'] = "No records found with this id";
													
												  $eventLog->log("No records found with this id");
												  
												}		
								} catch (Exception $e) {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "job-recruitment-status-details-fetching-error";
									$response['status_description'] = "There is an error, when fetching job recruitment status details.";
									
									$eventLog->log("There is an error, when fetching job recruitment status details.");	
									
								}
					   
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "The User Account, that is attempted to be edited, is invalid, please check and try again.";
						
						$eventLog->log("Please provide a Valid user ID.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
					
						
				}//close of else of if ($email_id_input == "") {
				
			}
			
		}	
			
	} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	

	
function job_recruitment_status_details_check_based_on_job_id($job_id_input) {

	global $dbcon, $eventLog;
	$constructed_array = array();
    
	$job_recruitment_status_details_get_sql = "SELECT * FROM `job_recruitment_status_change_event_notes` WHERE `job_id`=:job_id";
	$job_recruitment_status_details_select_query = $dbcon->prepare($job_recruitment_status_details_get_sql);
    $job_recruitment_status_details_select_query->bindValue(":job_id",$job_id_input);
	


	$job_recruitment_status_details_select_query->execute();

	if($job_recruitment_status_details_select_query->rowCount() > 0) {
		$eventLog->log("in if condition");
	    $job_recruitment_status_details_select_query_result = $job_recruitment_status_details_select_query->fetchAll();
		
		foreach ($job_recruitment_status_details_select_query_result as $job_recruitment_status_details_select_query_result_row) {
			
		    $constructed_array["old_recruitment_status"] = $job_recruitment_status_details_select_query_result_row["old_recruitment_status"];
			if($constructed_array["old_recruitment_status"] == 1)
			{
				$constructed_array["old_recruitment_status"]="Job is Open";
			}
			if($constructed_array["old_recruitment_status"] == 2)
			{
				$constructed_array["old_recruitment_status"]="Job is On-hold";
			}
			if($constructed_array["old_recruitment_status"] == 3)
			{
				$constructed_array["old_recruitment_status"]="Job is Filled";
			}
			
			if($constructed_array["old_recruitment_status"] == 4)
			{
				$constructed_array["old_recruitment_status"]="job is cancelled";
			}
			if($constructed_array["old_recruitment_status"] == 5)
			{
				$constructed_array["old_recruitment_status"]="Job is Expired";
			}
			if($constructed_array["old_recruitment_status"] == 6)
			{
				$constructed_array["old_recruitment_status"]="Job is Ignored";
			}
		    $constructed_array["new_recruitment_status"] = $job_recruitment_status_details_select_query_result_row["new_recruitment_status"];
			if($constructed_array["new_recruitment_status"] == 1)
			{
				$constructed_array["new_recruitment_status"]="Job is Open";
			}
			if($constructed_array["new_recruitment_status"] == 2)
			{
				$constructed_array["new_recruitment_status"]="Job is On-hold";
			}
			if($constructed_array["new_recruitment_status"] == 3)
			{
				$constructed_array["new_recruitment_status"]="Job is Filled";
			}
			
			if($constructed_array["new_recruitment_status"] == 4)
			{
				$constructed_array["new_recruitment_status"]="job is cancelled";
			}
			if($constructed_array["new_recruitment_status"] == 5)
			{
				$constructed_array["new_recruitment_status"]="Job is Expired";
			}
			if($constructed_array["new_recruitment_status"] == 6)
			{
				$constructed_array["new_recruitment_status"]="Job is Ignored";
			}
			
			$constructed_array["status"] = "Recruitment status of this job is changed from"  ." ". $constructed_array["old_recruitment_status"] . " ". "to" . " " . $constructed_array["new_recruitment_status"];
			$constructed_array["notes"] = $job_recruitment_status_details_select_query_result_row["notes"];
			$constructed_array["recruitment_status_change_time"] = $job_recruitment_status_details_select_query_result_row["added_datetime"];
			
			
			$constructed_array["status_change_person_id"] = $job_recruitment_status_details_select_query_result_row["added_by_sm_memb_id"];
			$user_basic_details_result = user_basic_details_check_based_on_user_id($constructed_array["status_change_person_id"]);
			$constructed_array["sm_firstname"] = $user_basic_details_result["sm_firstname"];
				$constructed_array["sm_middlename"] = $user_basic_details_result["sm_middlename"];
				$constructed_array["sm_lastname"] = $user_basic_details_result["sm_lastname"];
				$constructed_array["status_change_person_name"] = $constructed_array["sm_firstname"] . " " . $constructed_array["sm_middlename"] . " " .$constructed_array["sm_lastname"];
			
			
	    }

		return $constructed_array;
	}
	return $constructed_array;

}
exit;
?>


