<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		/* if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User"); */
			
			//If User is Authorized, to access this Page / REST API Service
			/* if ($ea_is_user_page_access_authorized) { */
				//Filter Inputs	
				
				$jsl_info_id_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_info_id']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_info_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$jsl_applicant_evaluation_info_id_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                $eventLog->log("jsl-info-id-input =>".$jsl_info_id_input);
				
                 if ($jsl_info_id_input == "") {
					//Invalid Sorting Order scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-jsl-info-id";
					$response['status_description'] = "Missing jsl info id.";
					
					$eventLog->log("Missing jsl info id.");
					
					
				
                
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else {
					//all inputs are valid
					
					try{
						
					    $eventLog->log("before list function.");
					    $candidate_list_info = get_candidate_list($jsl_info_id_input,$jsl_applicant_evaluation_info_id_input);
						/* $candidate_list_info_result_count = count($candidate_list_info_result);
						
						$eventLog->log("Count -> " . $candidate_list_info_result_count); */
						 
					     if(count($candidate_list_info) > 0) {
					       $response['data'] = $candidate_list_info;
					       $response['status'] = "uploaded-related-candidate-document-list-info-result-received";
					       $response['status_description'] = "Uploaded related candidate document list info result received";
							
					     $candidate_list_info_json_encoded = json_encode($candidate_list_info);
						
					       //eventLog->log("Quick User Info -> " . $candidate_list_info_json_encoded); 
					    }else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "active-candidates-doesnot-exist";
							$response['status_description'] = "No Active Candidates list Exist for your company, please check and try again.";
							
					    }		//No Active company list Exist
							
					    $eventLog->log("Companies List -> No Active Candidates Exist for your company, please check and try again."); 	
				    
					}catch (Exception $e){
						$eventLog->log("Exception -> " . html_escaped_output($e->getMessage())); 
						//addLog($logFile, "Exception -> ".$e->getMessage());	
					}//close of  catch (Exception $e){	}//close of else of if ($company_list_result_count > "0") {					
                  
				}//close of else of if ($user_type_input != 'admin') {
					
			//}close of //close of if ($ea_is_user_page_access_authorized) {
			
			
		/* } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		} *///close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
function get_candidate_list($jsl_info_id_input,$jsl_applicant_evaluation_info_id_input) {
    global $dbcon,$eventLog;
	$temp_array = array();
	$is_active_status = "1";
	
	$candidate_list_info_sql = "SELECT * FROM `jsl_rel_document_check_initial_criteria` WHERE `jsl_info_id`=:jsl_info_id AND `is_active_status` =:is_active_status";
	$candidate_list_info_query = $dbcon->prepare($candidate_list_info_sql);	
	$candidate_list_info_query->bindValue(":jsl_info_id",$jsl_info_id_input);
	$candidate_list_info_query->bindValue(":is_active_status",$is_active_status);
	$candidate_list_info_query->execute(); 
	$eventLog->log("before count value");
	if($candidate_list_info_query->rowCount() > 0) {
		
		$candidate_list_info_query_result = $candidate_list_info_query->fetchAll();
		foreach($candidate_list_info_query_result as $candidate_list_info_query_result_row){
			$constructed_array = array();
			
			$constructed_array["jrdcic_id"] = $candidate_list_info_query_result_row["jrdcic_id"];
			$jrdcic_id = $constructed_array["jrdcic_id"];
			$constructed_array["jsl_rel_docu_check_initial_criteria_name"] = $candidate_list_info_query_result_row["jsl_rel_docu_check_initial_criteria_name"];
			$constructed_array["job_id"] = $candidate_list_info_query_result_row["job_id"];
			$job_id = $constructed_array["job_id"];
			$constructed_array["jsl_classification_detail_id"] = $candidate_list_info_query_result_row["jsl_classification_detail_id"];
			$constructed_array["jsl_sub_classification_detail_id"] = $candidate_list_info_query_result_row["jsl_sub_classification_detail_id"];
			$constructed_array["jsl_info_id"] = $candidate_list_info_query_result_row["jsl_info_id"];
			$constructed_array["company_id"] = $candidate_list_info_query_result_row["company_id"];
			
			$candidate_document_check_criteria_value_result = candidate_document_check_criteria_value($jrdcic_id,$job_id,$jsl_applicant_evaluation_info_id_input);
			
			if(count($candidate_document_check_criteria_value_result)> 0){
				$constructed_array["status"] = "Document already uploaded";
			} else{
				$constructed_array["status"] = null;
			}
			
			$temp_array[] = $constructed_array;
			
		}
			
			
		return $temp_array;
	}
	return $temp_array;

}
function candidate_document_check_criteria_value($jrdcic_id,$job_id,$jsl_applicant_evaluation_info_id_input) {
	global $dbcon;
	$constructed_array = array();

	$candidate_document_check_criteria_check_sql = "SELECT * FROM `jsl_rel_criteria_applicant_evaluation_values` WHERE  `jrdcic_id` = :jrdcic_id AND `job_id` = :job_id AND `jsl_applicant_evaluation_info_id`=:jsl_applicant_evaluation_info_id";
	$candidate_document_check_criteria_check_q = $dbcon->prepare($candidate_document_check_criteria_check_sql);
	$candidate_document_check_criteria_check_q->bindValue(":jrdcic_id",$jrdcic_id);
    $candidate_document_check_criteria_check_q->bindValue(":job_id",$job_id);
	 $candidate_document_check_criteria_check_q->bindValue(":jsl_applicant_evaluation_info_id",$jsl_applicant_evaluation_info_id_input);
	$candidate_document_check_criteria_check_q->execute();

	if($candidate_document_check_criteria_check_q->rowCount() > 0) {
		$candidate_document_check_criteria_check_q_result = $candidate_document_check_criteria_check_q->fetch();
	     return $candidate_document_check_criteria_check_q_result;

	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;

}
exit;
?>