<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "13")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['job_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_id'] . "\r\n";
					}//close of if(isset($ea_received_rest_ws_raw_array_input['job_id']))

                    if (isset($ea_received_rest_ws_raw_array_input['job_applied_by_user_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_applied_by_user_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_applied_by_user_id']))
					
                    if (isset($ea_received_rest_ws_raw_array_input['current_ctc_confidentiality_status'])) {
						$content .= $ea_received_rest_ws_raw_array_input['current_ctc_confidentiality_status'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['current_ctc_confidentiality_status']))	

					if (isset($ea_received_rest_ws_raw_array_input['current_ctc_currency_three_lettered_code'])) {
						$content .= $ea_received_rest_ws_raw_array_input['current_ctc_currency_three_lettered_code'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['current_ctc_currency_three_lettered_code']))		
 					
                    if (isset($ea_received_rest_ws_raw_array_input['current_ctc_amount'])) {
						$content .= $ea_received_rest_ws_raw_array_input['current_ctc_amount'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['current_ctc_amount']))
						
					if (isset($ea_received_rest_ws_raw_array_input['current_ctc_compensation_period_name'])) {
						$content .= $ea_received_rest_ws_raw_array_input['current_ctc_compensation_period_name'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['current_ctc_compensation_period_name']))
                    
				    if (isset($ea_received_rest_ws_raw_array_input['expected_ctc_currency_three_lettered_code'])) {
						$content .= $ea_received_rest_ws_raw_array_input['expected_ctc_currency_three_lettered_code'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['expected_ctc_currency_three_lettered_code']))
                    
				    if (isset($ea_received_rest_ws_raw_array_input['expected_ctc_amount'])) {
						$content .= $ea_received_rest_ws_raw_array_input['expected_ctc_amount'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['expected_ctc_amount']))
						
					if (isset($ea_received_rest_ws_raw_array_input['expected_ctc_compensation_period_name'])) {
						$content .= $ea_received_rest_ws_raw_array_input['expected_ctc_compensation_period_name'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['expected_ctc_compensation_period_name']))
                    
                    if (isset($ea_received_rest_ws_raw_array_input['is_salary_negotiable'])) {
						$content .= $ea_received_rest_ws_raw_array_input['is_salary_negotiable'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['is_salary_negotiable']))	
					
                    if (isset($ea_received_rest_ws_raw_array_input['reason_for_applying'])) {
						$content .= $ea_received_rest_ws_raw_array_input['reason_for_applying'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['reason_for_applying']))	
                    
                     if (isset($ea_received_rest_ws_raw_array_input['job_specific_resume_status'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_specific_resume_status'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_specific_resume_status']))					
				
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
			
				$job_applied_by_user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_applied_by_user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_applied_by_user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$current_ctc_confidentiality_status_input = trim(isset($ea_received_rest_ws_raw_array_input['current_ctc_confidentiality_status']) ? filter_var($ea_received_rest_ws_raw_array_input['current_ctc_confidentiality_status'], FILTER_SANITIZE_STRING) : '');
				
				$current_ctc_currency_three_lettered_code_input = trim(isset($ea_received_rest_ws_raw_array_input['current_ctc_currency_three_lettered_code']) ? filter_var($ea_received_rest_ws_raw_array_input['current_ctc_currency_three_lettered_code'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$current_ctc_amount_input = trim(isset($ea_received_rest_ws_raw_array_input['current_ctc_amount']) ? filter_var($ea_received_rest_ws_raw_array_input['current_ctc_amount'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$current_ctc_compensation_period_name_input = trim(isset($ea_received_rest_ws_raw_array_input['current_ctc_compensation_period_name']) ? filter_var($ea_received_rest_ws_raw_array_input['current_ctc_compensation_period_name'], FILTER_SANITIZE_STRING) : '');
				
                /*$current_ctc_amount_input = trim(isset($ea_received_rest_ws_raw_array_input['current_ctc_amount']) ? filter_var($ea_received_rest_ws_raw_array_input['current_ctc_amount'], FILTER_SANITIZE_NUMBER_INT) : '');*/
                
                $expected_ctc_currency_three_lettered_code_input = 	trim(isset($ea_received_rest_ws_raw_array_input['expected_ctc_currency_three_lettered_code']) ? filter_var($ea_received_rest_ws_raw_array_input['expected_ctc_currency_three_lettered_code'], FILTER_SANITIZE_STRING) : '');	
                
                $expected_ctc_amount_input = trim(isset($ea_received_rest_ws_raw_array_input['expected_ctc_amount']) ? filter_var($ea_received_rest_ws_raw_array_input['expected_ctc_amount'], FILTER_SANITIZE_NUMBER_INT) : '');	
                
                $expected_ctc_compensation_period_name_input = 	trim(isset($ea_received_rest_ws_raw_array_input['expected_ctc_compensation_period_name']) ? filter_var($ea_received_rest_ws_raw_array_input['expected_ctc_compensation_period_name'], FILTER_SANITIZE_STRING) : '');	
				
				$is_salary_negotiable_input = 	trim(isset($ea_received_rest_ws_raw_array_input['is_salary_negotiable']) ? filter_var($ea_received_rest_ws_raw_array_input['is_salary_negotiable'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$reason_for_applying_input =  trim(isset($ea_received_rest_ws_raw_array_input['reason_for_applying']) ? filter_var($ea_received_rest_ws_raw_array_input['reason_for_applying'], FILTER_SANITIZE_STRING) : '');
				$reason_for_applying_all_input = trim($ea_received_rest_ws_raw_array_input["reason_for_applying"]);
					$reason_for_applying_safe_html_input = get_cleaned_safe_html_content_input($reason_for_applying_all_input, $page_content_file_config);
				
				
				
				$job_specific_resume_status_input = trim(isset($ea_received_rest_ws_raw_array_input['job_specific_resume_status']) ? filter_var($ea_received_rest_ws_raw_array_input['job_specific_resume_status'], FILTER_SANITIZE_STRING) : '');
				
				
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				if ($job_id_input == "") {
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-id";
					$response['status_description'] = "Missing Job ID";
					
					$eventLog->log("missing-job-id: Please provide a valid Job Id.");
					
				} else if ($job_applied_by_user_id_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-applied-by-user-id";
					$response['status_description'] = "Missing job applied by user id";
					
					$eventLog->log("missing-job-applied-by-user-id: Please provide a valid job applicant user id.");
						
				} else if ($current_ctc_confidentiality_status_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-current-ctc-confidentiality-status";
					$response['status_description'] = "Missing current ctc confidentiality status";
					
					$eventLog->log("missing-current-ctc-confidentiality-status: Please provide a valid current ctc confidentiality status.");
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					
						//Event Time, as per Indian Standard Time
						$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
						
						$candidate_add_next_step = "";
						
						if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$candidate_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else if ((isset($ea_extracted_jwt_token_user_company_id)) /*&& ($company_id_input == $ea_extracted_jwt_token_user_company_id)*/) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$candidate_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
						} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
						}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
						$job_details_result = job_details_based_on_job_id($job_id_input);
						
						
						$company_id_input = $job_details_result["company_id"];
						
						$company_client_id_input = $job_details_result["company_client_id"];
						
						$job_posted_by_user_id_input = $job_details_result["job_posted_by_sm_memb_id"];
						
						
						
						if($current_ctc_confidentiality_status_input == 0)
						{
							$current_ctc_currency_three_lettered_code_input = null;
							$current_ctc_amount_input = null;
							$current_ctc_compensation_period_name_input = null;
						}	
							
						if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							try {
								
								$last_inserted_id = job_applicant_request_information_insert($company_id_input,$company_client_id_input,$job_id_input,$job_posted_by_user_id_input,$job_applied_by_user_id_input,$current_ctc_confidentiality_status_input,$current_ctc_currency_three_lettered_code_input,$current_ctc_amount_input,$current_ctc_compensation_period_name_input,$expected_ctc_currency_three_lettered_code_input,$expected_ctc_amount_input,$expected_ctc_compensation_period_name_input,$is_salary_negotiable_input,$reason_for_applying_input,$reason_for_applying_safe_html_input,$job_specific_resume_status_input,$event_datetime,$current_epoch,$ea_extracted_jwt_token_sub);
								
								$eventLog->log($last_inserted_id);
								
									
									if ($last_inserted_id != "") {
										
										    $response['data'] = array();
											$response['status'] = "job-applicant-request-information-insertion-successful";
											$response['status_description'] = "job applicant request information is added Successfully.";
										
											$eventLog->log("job-applicant-request-information-insertion-successful:job applicant request information is added Successfully.");   
										
									} else {
										//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
										$response['data'] = array();
										$response['status'] = "job-applicant-request-information-insertion-error";
										$response['status_description'] = "Error occurred when adding the job applicant request information into the Database.";
										
										$eventLog->log("job-applicant-request-information-insert-error: There is an error, when adding the job applicant request information into the Database.");
										
									}
									
								
									
							         
									
								
							} catch (Exception $e) {
								
								//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
								$response['data'] = array();
								$response['status'] = "job-applicant-request-information-insertion-error";
								$response['status_description'] = "Error occurred when adding the job applicant request information in the Database.";
								
								$eventLog->log("job-applicant-request-insertion-error: There is an error, when adding the job applicant request information in the Database.");
							}
						}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							
						
					
				}//close of else of if ($email_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
    
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
			
	
	}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function job_applicant_request_information_insert($company_id_input,$company_client_id_input,$job_id_input,$job_posted_by_user_id_input,$job_applied_by_user_id_input,$current_ctc_confidentiality_status_input,$current_ctc_currency_three_lettered_code_input,$current_ctc_amount_input,$current_ctc_compensation_period_name_input,$expected_ctc_currency_three_lettered_code_input,$expected_ctc_amount_input,$expected_ctc_compensation_period_name_input,$is_salary_negotiable_input,$reason_for_applying_input,$reason_for_applying_safe_html_input,$job_specific_resume_status_input,$event_datetime,$current_epoch,$ea_extracted_jwt_token_sub) {
    global $dbcon,$eventLog;
	$is_active_status = '1';
    $job_applicantrequest_information_insert_sql ="INSERT INTO `job_applicant_requests_info`(`company_id`, `company_client_id`, `job_id`, `job_posted_by_user_id`, `job_applied_by_user_id`, `current_ctc_confidentiality_status`, `current_ctc_currency_three_lettered_code`, `current_ctc_amount`, `current_ctc_compensation_period_name`, `expected_ctc_currency_three_lettered_code`, `expected_ctc_amount`, `expected_ctc_compensation_period_name`, `is_salary_negotiable`, `reason_for_applying`, `reason_for_applying_safe_html`, `job_specific_resume_status`, `event_date_time`, `event_date_time_epoch`, `added_by`,`is_active_status`) VALUES(:company_id,:company_client_id,:job_id,:job_posted_by_user_id,:job_applied_by_user_id,:current_ctc_confidentiality_status,:current_ctc_currency_three_lettered_code,:current_ctc_amount,:current_ctc_compensation_period_name,:expected_ctc_currency_three_lettered_code,:expected_ctc_amount,:expected_ctc_compensation_period_name,:is_salary_negotiable,:reason_for_applying,:reason_for_applying_safe_html,:job_specific_resume_status,:event_date_time,:event_date_time_epoch,:added_by,:is_active_status)";
	
	$eventLog->log("after query");
	
	$job_applicantrequest_information_insert_query = $dbcon->prepare($job_applicantrequest_information_insert_sql);
	$job_applicantrequest_information_insert_query->bindValue(":company_id",$company_id_input);
	$job_applicantrequest_information_insert_query->bindValue(":company_client_id",$company_client_id_input);
	$job_applicantrequest_information_insert_query->bindValue(":job_id",$job_id_input);
	$job_applicantrequest_information_insert_query->bindValue(":job_posted_by_user_id",$job_posted_by_user_id_input);
	$job_applicantrequest_information_insert_query->bindValue(":job_applied_by_user_id",$job_applied_by_user_id_input);
	$job_applicantrequest_information_insert_query->bindValue(":current_ctc_confidentiality_status",$current_ctc_confidentiality_status_input);
	$job_applicantrequest_information_insert_query->bindValue(":current_ctc_currency_three_lettered_code",$current_ctc_currency_three_lettered_code_input);
	$job_applicantrequest_information_insert_query->bindValue(":current_ctc_amount",$current_ctc_amount_input);
	$job_applicantrequest_information_insert_query->bindValue(":current_ctc_compensation_period_name",$current_ctc_compensation_period_name_input);
	$job_applicantrequest_information_insert_query->bindValue(":expected_ctc_currency_three_lettered_code",$expected_ctc_currency_three_lettered_code_input);
	$job_applicantrequest_information_insert_query->bindValue(":expected_ctc_amount",$expected_ctc_amount_input);
	$job_applicantrequest_information_insert_query->bindValue(":expected_ctc_compensation_period_name",$expected_ctc_compensation_period_name_input);
	$job_applicantrequest_information_insert_query->bindValue(":is_salary_negotiable",$is_salary_negotiable_input);$job_applicantrequest_information_insert_query->bindValue(":reason_for_applying",$reason_for_applying_input);
	$job_applicantrequest_information_insert_query->bindValue(":reason_for_applying_safe_html",$reason_for_applying_safe_html_input);
	$job_applicantrequest_information_insert_query->bindValue(":job_specific_resume_status",$job_specific_resume_status_input);
	$job_applicantrequest_information_insert_query->bindValue(":event_date_time",$event_datetime);
	$job_applicantrequest_information_insert_query->bindValue(":event_date_time_epoch",$current_epoch);
	$job_applicantrequest_information_insert_query->bindValue(":added_by",$ea_extracted_jwt_token_sub);
	$job_applicantrequest_information_insert_query->bindValue(":is_active_status",$is_active_status);
	

		if ($job_applicantrequest_information_insert_query->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			$eventLog->log("record inserted successfully");

			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}	
function job_details_based_on_job_id($job_id_input) {
	global $dbcon;
	$constructed_array = array();
	$job_details_sql = "SELECT * FROM `jobs` WHERE `job_id` = :job_id";
	$job_details_select_query = $dbcon->prepare($job_details_sql);
	$job_details_select_query->bindValue(":job_id",$job_id_input);
	$job_details_select_query->execute();

	if($job_details_select_query->rowCount() > 0) {
		$job_details_select_query_result = $job_details_select_query->fetch();
	     return $job_details_select_query_result;

	}//close of if($rest_company_check_select_query->rowCount() > 0) {
	return $constructed_array;

}

exit;
?>