
<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "17")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_info_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_info_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_info_id']))
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_client_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['job_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_id'])) 
						
	                if (isset($ea_received_rest_ws_raw_array_input['jsl_title'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_title'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_title'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['jsl_classification_detail_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_classification_detail_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_classification_detail_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_owned_by'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_owned_by'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_owned_by'])) 	
							
					if (isset($ea_received_rest_ws_raw_array_input['jsl_j_s_candidate_email_notification_setting'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_j_s_candidate_email_notification_setting'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_j_s_candidate_email_notification_setting'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_j_s_candidate_sms_notification_setting'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_j_s_candidate_sms_notification_setting'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_j_s_candidate_sms_notification_setting'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_j_s_recruiter_email_notification_setting'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_j_s_recruiter_email_notification_setting'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_j_s_recruiter_email_notification_setting'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_j_s_recruiter_sms_notification_setting'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_j_s_recruiter_sms_notification_setting'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_j_s_recruiter_sms_notification_setting'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_event_deadline_requirement_status'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_event_deadline_requirement_status'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_event_deadline_requirement_status'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_event_deadline_datetime'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_event_deadline_datetime'] . "\r\n";
					}//close of if(isset($ea_received_rest_ws_raw_array_input['jsl_event_deadline_datetime'])) 
					
					
					
				}	
					$job_rel_assignees_list_input = $ea_received_rest_ws_raw_array_input['job_rel_assignees_list'];
					$jsl_rel_criteria_list_input = $ea_received_rest_ws_raw_array_input['jsl_rel_criteria_list'];
					$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					$company_client_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_client_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_client_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					$job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					$jsl_title_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_title']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_title'], FILTER_SANITIZE_STRING) : '');
					$jsl_classification_detail_id_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_classification_detail_id']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_classification_detail_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					$jsl_sub_classification_detail_id_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_id']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					//$jsl_rel_criteria_name_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_rel_criteria_name']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_rel_criteria_name'], FILTER_SANITIZE_STRING) : '');
					//$in_person_interview_location_address_type_input = trim(isset($ea_received_rest_ws_raw_array_input['in_person_interview_location_address_type']) ? filter_var($ea_received_rest_ws_raw_array_input['in_person_interview_location_address_type'], FILTER_SANITIZE_NUMBER_INT) : '');
					//$in_person_interview_location_address_id_input = trim(isset($ea_received_rest_ws_raw_array_input['in_person_interview_location_address_id']) ? filter_var($ea_received_rest_ws_raw_array_input['in_person_interview_location_address_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					$jsl_owned_by_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_owned_by']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_owned_by'], FILTER_SANITIZE_NUMBER_INT) : '');
					$jsl_info_id_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_info_id']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_info_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					$jsl_j_s_candidate_email_notification_setting_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_j_s_candidate_email_notification_setting']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_j_s_candidate_email_notification_setting'], FILTER_SANITIZE_NUMBER_INT) : '');
					$jsl_j_s_candidate_sms_notification_setting_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_j_s_candidate_sms_notification_setting']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_j_s_candidate_sms_notification_setting'], FILTER_SANITIZE_NUMBER_INT) : '');
					$jsl_j_s_recruiter_email_notification_setting_by_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_j_s_recruiter_email_notification_setting']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_j_s_recruiter_email_notification_setting'], FILTER_SANITIZE_NUMBER_INT) : '');
					$jsl_j_s_recruiter_sms_notification_setting_by_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_j_s_recruiter_sms_notification_setting']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_j_s_recruiter_sms_notification_setting'], FILTER_SANITIZE_NUMBER_INT) : '');
					$jsl_event_deadline_requirement_status_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_event_deadline_requirement_status']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_event_deadline_requirement_status'], FILTER_SANITIZE_NUMBER_INT) : '');
					$jsl_event_deadline_datetime_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_event_deadline_datetime']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_event_deadline_datetime'], FILTER_SANITIZE_STRING) : '');
					
					$jsl_seo_title_input = strtolower(create_seo_name($jsl_title_input));
					
					$jsl_classifications_details_result = jsl_classification_details_based_on_jsl_classification_detail_id($jsl_classification_detail_id_input);
					
					$jsl_classification_detail_name_input = $jsl_classifications_details_result["jsl_classification_detail_name"];
					$jsl_classification_detail_seo_name_input = $jsl_classifications_details_result["jsl_classification_detail_seo_name"];
					
					$jsl_sub_classifications_details_result = jsl_sub_classification_details_based_on_jsl_sub_classification_detail_id($jsl_sub_classification_detail_id_input);
					
					$jsl_sub_classification_detail_name_input = $jsl_sub_classifications_details_result["jsl_sub_classification_detail_name"];
					$jsl_sub_classification_detail_seo_name_input = $jsl_sub_classifications_details_result["jsl_sub_classification_detail_seo_name"];
					
					
					
					if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
					} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
					}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						

					
					  //invalid company_id scenario
					
					if ($company_id_input == "") {
				
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "missing-company-id";
						$response['status_description'] = "missing company id";
						
						$eventLog->log("missing-company-id: Please provide a valid Company id.");
						
					} else if ($company_client_id_input == "") {
						//Invalid Mobile Number scenario
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "missing-company-client-id";
						$response['status_description'] = "Missing Company client id";
						
						$eventLog->log("missing-company-client-id: Please provide a valid Company client id.");
						
					} else if ($job_id_input == "") {
						//Invalid Mobile Number scenario
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "missing-job-id";
						$response['status_description'] = "Missing Job id";
						
						$eventLog->log("missing-job-id: Please provide a valid Job id.");
						
					} else if ($company_client_id_input == "") {
						//Invalid Mobile Number scenario
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						//$response['status'] = "missing-company_client_id";
						$response['status'] = "missing-company-client-id";
						$response['status_description'] = "Missing Company client id";
						
						$eventLog->log("missing-company-client-id: Please provide a valid Company client id.");
						
					
					} else if ($jsl_title_input == "") {
						//Invalid Mobile Number scenario
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "missing-job-screening-level-title";
						$response['status_description'] = "Missing job screening level title";
						
						$eventLog->log("missing-job-screening-level-title: Please provide a valid job screening level title.");
						
					} else if ($jsl_classification_detail_id_input == "") {
						
						$response['data'] = array();
						$response['status'] = "missing-job-classification-detail-id";
						$response['status_description'] = "Missing job classification detail id";
						
						$eventLog->log("missing-job-classification-detail-id: Please provide a valid job classification detail id.");
					
					} else if ($jsl_sub_classification_detail_id_input == "") {
						
						$response['data'] = array();
						$response['status'] = "missing-job-sub-classification-detail-id";
						$response['status_description'] = "Missing job sub classification detail id";
						
						$eventLog->log("missing-job-sub-classification-detail-id: Please provide a valid job sub classification detail id.");
					
					/* } else if ( (($jsl_sub_classification_detail_id_input != "5") && ($jsl_sub_classification_detail_id_input != "6")) && (count($jsl_rel_criteria_list_input) > 1)) {
						
						$response['data'] = array();
						$response['status'] = "invalid-jsl-rel-criteria-list-input";
						$response['status_description'] = "Multiple JSL related criteria Not allowed.";
						
						$eventLog->log("invalid-jsl-rel-criteria-list-input: Multiple JSL related criteria Not allowed.");
				 */
				
					} else if ($jsl_owned_by_input == "") {
						
						$response['data'] = array();
						$response['status'] = "missing-job-screening-level-owned-by-input";
						$response['status_description'] = "Missing job screening level owned by input";
						
						$eventLog->log("missing-job-screening-level-owned-by-input: Please provide a valid job screening level owned by input.");
				
					} else if ($jsl_owned_by_input == "1" && $job_rel_assignees_list_input == "") {
						
						$response['data'] = array();
						$response['status'] = "missing-job-related-assignees-list-input";
						$response['status_description'] = "Missing job related assignees list input";
						
						$eventLog->log("missing-job-related-assignees-list-input: Please provide a valid job related assignees list.");
				    
					
				
				
					} else {	
						
							$job_rel_screening_level_add_next_step = "";
							if ($ea_extracted_jwt_token_user_type == "admin") {
								
								if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
									$job_rel_screening_level_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("before company id condition");
								} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
									
									$job_rel_screening_level_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("after add condition");
								} else {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "insufficient-permissions-different-company-user-id";
									$response['status_description'] = "Insufficient Set of Permissions";
									
									//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
									header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
									$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
									
								}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								
								
								
							} else {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
								$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
							
							}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
								
							$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
							
							$jsl_event_deadline_datetime_epoch_input = df_convert_date_to_unix_timestamp($jsl_event_deadline_datetime_input);
							
							if ($job_rel_screening_level_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
								try {
									
									$jsl_update_result =
									job_rel_screening_level_details_update($jsl_info_id_input,$company_id_input,$company_client_id_input,$job_id_input,$jsl_title_input,$jsl_seo_title_input,$jsl_classification_detail_id_input,$jsl_classification_detail_name_input,$jsl_classification_detail_seo_name_input,$jsl_sub_classification_detail_id_input,$jsl_sub_classification_detail_name_input,$jsl_sub_classification_detail_seo_name_input,$jsl_owned_by_input,$jsl_j_s_candidate_email_notification_setting_input,$jsl_j_s_candidate_sms_notification_setting_input,$jsl_j_s_recruiter_email_notification_setting_by_input,$jsl_j_s_recruiter_sms_notification_setting_by_input,$jsl_event_deadline_requirement_status_input,$jsl_event_deadline_datetime_input,$jsl_event_deadline_datetime_epoch_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
									
									if ($jsl_update_result == true) {
										
										if($jsl_classification_detail_id_input == 1) {
											if ((is_array($jsl_rel_criteria_list_input)) && (count($jsl_rel_criteria_list_input) > 0)) {
										
											
											
												$jsl_rel_criteria_name = array();
												$jsl_rel_criteria_list_input_count = count($jsl_rel_criteria_list_input);
												$jsl_rel_criteria_list_input_row_count = 0;
												
												
												$jsl_criteria_array = array();
												
												foreach ($jsl_rel_criteria_list_input as $jsl_rel_criteria_list_input_row) {
													$jsl_rel_criteria_name_input = $jsl_rel_criteria_list_input_row["jsl_rel_criteria_name"];
													
													$jsl_criteria_array[] = $jsl_rel_criteria_list_input_row["jsl_rel_criteria_name"];
													
													$jsl_criteria_duplicate_check_result = jsl_rel_critaria_duplicate_check($job_id_input,$jsl_info_id_input,$jsl_rel_criteria_name_input);
													
													
													if(count($jsl_criteria_duplicate_check_result) > 0){
														$eventLog->log("no need to insert");	
														$jsl_rel_criteria_list_input_row_count++;
														
														$jrdcic_id = $jsl_criteria_duplicate_check_result["jrdcic_id"];
													
													} else {
														
														$jsl_criteria_insert_result  = jsl_rel_criteria_add($company_id_input,$company_client_id_input,$job_id_input,$jsl_classification_detail_id_input,$jsl_sub_classification_detail_id_input,$jsl_info_id_input,$jsl_rel_criteria_name_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
														$eventLog->log($jsl_criteria_insert_result);
														$jsl_rel_criteria_list_input_row_count++;
														$eventLog->log($jsl_rel_criteria_list_input_row_count);
														$jrdcic_id = $jsl_criteria_insert_result ;
														
													}		
														
												}
												
												if ($jsl_rel_criteria_list_input_count == $jsl_rel_criteria_list_input_row_count) {
													 
													 $jsl_rel_criteria_result = true;
													 $eventLog->log("true");
													
												} else {
													
													$jsl_rel_criteria_result = false;
													$eventLog->log("true");
													
												}
												
													
											}
											
											
											
											$db_collected_jsl_criteria_list = jsl_rel_critaria_check($job_id_input,$jsl_info_id_input);
										
										
										
										
											foreach($db_collected_jsl_criteria_list as $db_collected_jsl_criteria_list_row) {
												
												$jsl_rel_criteria_name_input = $db_collected_jsl_criteria_list_row["jsl_rel_docu_check_initial_criteria_name"];
												$jrdcic_id = $db_collected_jsl_criteria_list_row["jrdcic_id"];
												
												
												if(in_array($jsl_rel_criteria_name_input,$jsl_criteria_array)){
				
															$eventLog->log("NO need to delete");
															
												} else {
													
													
													$jsl_criteria_change_status_result = jsl_rel_criteria_change_status($jrdcic_id,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
													
													if($jsl_criteria_change_status_result == true) {
														$eventLog->log($jrdcic_id);
														$eventLog->log("delete case dealed successfully");
														
													} else {
														
														$eventLog->log("delete case dealing error");
													}
													
													
												}
												
												
												
												
												
											}
										} else {
											$eventLog->log("not document classification case");
										
										}
										
										if ((is_array($job_rel_assignees_list_input)) && (count($job_rel_assignees_list_input) > 0)) {
									
											$jsl_assignees_array = array();
										
											foreach ($job_rel_assignees_list_input as $job_rel_assignees_list_input_row) {
													$recruiter_sm_memb_id = $job_rel_assignees_list_input_row["sm_memb_id"];
													//$sm_email_id = $multiple_email_requests_input_row["sm_email"];
													
													$eventLog->log($recruiter_sm_memb_id);
													$jsl_assignees_array[] = $recruiter_sm_memb_id;
													$job_screening_level_assignees_add_duplicate_check_result = job_screening_level_assignees_add_duplicate_check($jsl_info_id_input,$recruiter_sm_memb_id);
									
													$eventLog->log("job_screening_level_assignees_add_duplicate_check_result: job_screening_level_assignees_add_duplicate_check_result");
													
													if (count($job_screening_level_assignees_add_duplicate_check_result) > 0) {
											
														$eventLog->log("job-screening-level-already-assigned for this id".$recruiter_sm_memb_id);	
														//$count--;
														continue;
													
													} else {
														
														$last_inserted_id = job_screening_level_assignees_add($company_id_input,$company_client_id_input,$job_id_input,$jsl_classification_detail_id_input,$jsl_classification_detail_name_input,$jsl_classification_detail_seo_name_input,$jsl_sub_classification_detail_id_input,$jsl_sub_classification_detail_name_input,$jsl_sub_classification_detail_seo_name_input,$jsl_info_id_input,$recruiter_sm_memb_id,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
														$eventLog->log($last_inserted_id);
														//$row_count++;
														//$eventLog->log($row_count);
														
													}
													
											}
											
											
											$db_collected_jsl_assignees_list = job_screening_level_assignees_check($job_id_input,$jsl_info_id_input);
										
										
										
										
											foreach($db_collected_jsl_assignees_list as $db_collected_jsl_assignees_list_row) {
												
												$recruiter_sm_memb_id = $db_collected_jsl_assignees_list_row["recruiter_sm_memb_id"];
												$jsl_recruiter_mapping_id = $db_collected_jsl_assignees_list_row["jsl_recruiter_mapping_id"];
												
												
												if(in_array($recruiter_sm_memb_id,$jsl_assignees_array)){
				
															$eventLog->log("NO need to delete");
															
												} else {
													
													
													$jsl_assignees_change_status_result = jsl_assignees_change_status($jsl_recruiter_mapping_id,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
													
													if($jsl_assignees_change_status_result == true) {
														$eventLog->log($jsl_recruiter_mapping_id);
														$eventLog->log("delete case dealed successfully");
														
													} else {
														
														$eventLog->log("delete case dealing error");
													}
													
													
												}
												
												
												
												
												
											}
											
											
											
											
											
										} else {
											
											$eventLog->log("not assigness case");
										}
										
										
										
										
										
										
										$response['data'] = array();
										$response['status'] = "job-related-screening-level-updation-successful";
										$response['status_description'] = "Job related screening level updated Successfully.";
								
										$eventLog->log("Job related screening level updated Successfully.");
								
									} else {
										
										$response['data'] = array();
										$response['status'] = "job-related-screening-level-updation-successful";
										$response['status_description'] = "Job related screening level updated Successfully.";
								
										$eventLog->log("Job related screening level updated Successfully.");
										
										
									}
										
										
									
								} catch (Exception $e) {
									
									$response['data'] = array();
									$response['status'] = "job-related-screening-level-insertion-error";
									$response['status_description'] = "Error occurred when adding the job related screening level.";
										
									$eventLog->log("job-related-screening-level-insertion-error: Error occurred when adding the job related screening level.");
										
								}
							}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
							
						}
			//}//close of else of if ($email_id_input == "") {
					
		}//close of //close of if ($ea_is_user_page_access_authorized) {
					
				
	} else {
				
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "invalid-auth-token-submitted";
				$response['status_description'] = "Invalid Authentication";
				
				//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
				header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
				
				$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
				
		}//close of else of if ($ea_auth_token_validation_status) {
		
    }//close of if ($ea_maintenance_mode == false) {
		
} else {

		//Construct Content, that will be sent in Response body, of the REST Web Service
		$response['data'] = array();
		$response['status'] = "invalid-input";
		$response['status_description'] = "Invalid Input, Please check and provide all information.";
		
		//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
		
		$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
				
		
		}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




	//Check if Maintenance Mode is Turned On
	if ($ea_maintenance_mode) {	
		
		//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
		//header('Maintenance-Progress: true', false);
		//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
		$response['data'] = array();
		$response['status'] = "application-maintenance-in-progress";
		$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
		
		$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
		
	} else {	

		//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
		header('Content-Type: application/json');
		echo json_encode($response,JSON_PRETTY_PRINT);
		
	}//close of else of if ($ea_maintenance_mode){
		
function job_rel_screening_level_details_update($jsl_info_id_input,$company_id_input,$company_client_id_input,$job_id_input,$jsl_title_input,$jsl_seo_title_input,$jsl_classification_detail_id_input,$jsl_classification_detail_name_input,$jsl_classification_detail_seo_name_input,$jsl_sub_classification_detail_id_input,$jsl_sub_classification_detail_name_input,$jsl_sub_classification_detail_seo_name_input,$jsl_owned_by_input,$jsl_j_s_candidate_email_notification_setting_input,$jsl_j_s_candidate_sms_notification_setting_input,$jsl_j_s_recruiter_email_notification_setting_by_input,$jsl_j_s_recruiter_sms_notification_setting_by_input,$jsl_event_deadline_requirement_status_input,$jsl_event_deadline_datetime_input,$jsl_event_deadline_datetime_epoch_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch) {
	
	global $dbcon,$eventLog;
	$is_active_status = '1';
	
    
	$job_rel_screening_level_details_update_sql = "UPDATE `jsl_info` SET `company_id`=:company_id,`company_client_id`=:company_client_id,`job_id`=:job_id,`jsl_title`=:jsl_title,`jsl_seo_title`=:jsl_seo_title,`jsl_classification_detail_id`=:jsl_classification_detail_id,`jsl_classification_detail_name`=:jsl_classification_detail_name,`jsl_classification_detail_seo_name`=:jsl_classification_detail_seo_name,`jsl_sub_classification_detail_id`=:jsl_sub_classification_detail_id,`jsl_sub_classification_detail_name`=:jsl_sub_classification_detail_name,`jsl_sub_classification_detail_seo_name`=:jsl_sub_classification_detail_seo_name,`jsl_owned_by`=:jsl_owned_by,`jsl_j_s_candidate_email_notification_setting`=:jsl_j_s_candidate_email_notification_setting,`jsl_j_s_candidate_sms_notification_setting`=:jsl_j_s_candidate_sms_notification_setting,`jsl_j_s_recruiter_email_notification_setting`=:jsl_j_s_recruiter_email_notification_setting,`jsl_j_s_recruiter_sms_notification_setting`=:jsl_j_s_recruiter_sms_notification_setting,`jsl_event_deadline_requirement_status`=:jsl_event_deadline_requirement_status,`jsl_event_deadline_datetime`=:jsl_event_deadline_datetime,`jsl_event_deadline_datetime_epoch`=:jsl_event_deadline_datetime_epoch,`last_updated_datetime`=:last_updated_datetime,`last_updated_datetime_epoch`=:last_updated_datetime_epoch,`last_updated_by_sm_memb_id`=:last_updated_by_sm_memb_id,`is_active_status`=:is_active_status WHERE `jsl_info_id` =:jsl_info_id ";

	$job_rel_screening_level_details_update_query = $dbcon->prepare($job_rel_screening_level_details_update_sql);
	$job_rel_screening_level_details_update_query->bindValue(":jsl_info_id",$jsl_info_id_input);
	$job_rel_screening_level_details_update_query->bindValue(":company_id",$company_id_input);
	$job_rel_screening_level_details_update_query->bindValue(":company_client_id",$company_client_id_input);
	$job_rel_screening_level_details_update_query->bindValue(":job_id",$job_id_input);	
	$job_rel_screening_level_details_update_query->bindValue(":jsl_title",$jsl_title_input);
	$job_rel_screening_level_details_update_query->bindValue(":jsl_seo_title",$jsl_seo_title_input);
	$job_rel_screening_level_details_update_query->bindValue(":jsl_classification_detail_id",$jsl_classification_detail_id_input);
	$job_rel_screening_level_details_update_query->bindValue(":jsl_classification_detail_name",$jsl_classification_detail_name_input);
	$job_rel_screening_level_details_update_query->bindValue(":jsl_classification_detail_seo_name",$jsl_classification_detail_seo_name_input);
	$job_rel_screening_level_details_update_query->bindValue(":jsl_sub_classification_detail_id",$jsl_sub_classification_detail_id_input);
	$job_rel_screening_level_details_update_query->bindValue(":jsl_sub_classification_detail_name",$jsl_sub_classification_detail_name_input);
	$job_rel_screening_level_details_update_query->bindValue(":jsl_sub_classification_detail_seo_name",$jsl_sub_classification_detail_seo_name_input);
	$job_rel_screening_level_details_update_query->bindValue(":jsl_owned_by",$jsl_owned_by_input);
    $job_rel_screening_level_details_update_query->bindValue(":jsl_j_s_candidate_email_notification_setting",$jsl_j_s_candidate_email_notification_setting_input);
	$job_rel_screening_level_details_update_query->bindValue(":jsl_j_s_candidate_sms_notification_setting",$jsl_j_s_candidate_sms_notification_setting_input);
	$job_rel_screening_level_details_update_query->bindValue(":jsl_j_s_recruiter_email_notification_setting",$jsl_j_s_recruiter_email_notification_setting_by_input);
	$job_rel_screening_level_details_update_query->bindValue(":jsl_j_s_recruiter_sms_notification_setting",$jsl_j_s_recruiter_sms_notification_setting_by_input);
	$job_rel_screening_level_details_update_query->bindValue(":jsl_event_deadline_requirement_status",$jsl_event_deadline_requirement_status_input);
	$job_rel_screening_level_details_update_query->bindValue(":jsl_event_deadline_datetime",$jsl_event_deadline_datetime_input);
	$job_rel_screening_level_details_update_query->bindValue(":jsl_event_deadline_datetime_epoch",$jsl_event_deadline_datetime_epoch_input);
	//$job_rel_screening_level_details_update_query->bindValue(":jsl_sequence_order",$jsl_sequence_order);
	$job_rel_screening_level_details_update_query->bindValue(":last_updated_datetime",$event_datetime);
	$job_rel_screening_level_details_update_query->bindValue(":last_updated_datetime_epoch",$current_epoch);
	$job_rel_screening_level_details_update_query->bindValue(":last_updated_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$job_rel_screening_level_details_update_query->bindValue(":is_active_status",$is_active_status);
	
	if ($job_rel_screening_level_details_update_query->execute()) {
       
           
			return true;

		} else {
		   
			return false;
	    }

}

function job_screening_level_assignees_add($company_id_input,$company_client_id_input,$job_id_input,$jsl_classification_detail_id_input,$jsl_classification_detail_name_input,$jsl_classification_detail_seo_name_input,$jsl_sub_classification_detail_id_input,$jsl_sub_classification_detail_name_input,$jsl_sub_classification_detail_seo_name_input,$last_inserted_id,$recruiter_sm_memb_id,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch) {
	
	
	global $dbcon,$eventLog;
	$is_active_status = '1';
	
    
	$job_screening_level_assignees_add_sql = "INSERT INTO `jsl_recruiter_mapping`(`company_id`, `company_client_id`, `job_id`, `jsl_classification_detail_id`, `jsl_classification_detail_name`, `jsl_classification_detail_seo_name`, `jsl_sub_classification_detail_id`, `jsl_sub_classification_detail_name`, `jsl_sub_classification_detail_seo_name`,`jsl_info_id`, `recruiter_sm_memb_id`, `added_datetime`, `added_datetime_epoch`, `added_by_sm_memb_id`,`is_active_status`) VALUES (:company_id,:company_client_id,:job_id,:jsl_classification_detail_id,:jsl_classification_detail_name,:jsl_classification_detail_seo_name,:jsl_sub_classification_detail_id,:jsl_sub_classification_detail_name,:jsl_sub_classification_detail_seo_name,:jsl_info_id,:recruiter_sm_memb_id,:added_datetime,:added_datetime_epoch,:added_by_sm_memb_id,:is_active_status)";

	$job_screening_level_assignees_add_query = $dbcon->prepare($job_screening_level_assignees_add_sql);
	$job_screening_level_assignees_add_query->bindValue(":company_id",$company_id_input);
	$job_screening_level_assignees_add_query->bindValue(":company_client_id",$company_client_id_input);
	$job_screening_level_assignees_add_query->bindValue(":job_id",$job_id_input);	
	$job_screening_level_assignees_add_query->bindValue(":jsl_classification_detail_id",$jsl_classification_detail_id_input);
	$job_screening_level_assignees_add_query->bindValue(":jsl_classification_detail_name",$jsl_classification_detail_name_input);
	$job_screening_level_assignees_add_query->bindValue(":jsl_classification_detail_seo_name",$jsl_classification_detail_seo_name_input);
	$job_screening_level_assignees_add_query->bindValue(":jsl_sub_classification_detail_id",$jsl_sub_classification_detail_id_input);
	$job_screening_level_assignees_add_query->bindValue(":jsl_sub_classification_detail_name",$jsl_sub_classification_detail_name_input);
	$job_screening_level_assignees_add_query->bindValue(":jsl_sub_classification_detail_seo_name",$jsl_sub_classification_detail_seo_name_input);
	$job_screening_level_assignees_add_query->bindValue(":jsl_info_id",$last_inserted_id);
    $job_screening_level_assignees_add_query->bindValue(":recruiter_sm_memb_id",$recruiter_sm_memb_id);
	$job_screening_level_assignees_add_query->bindValue(":added_datetime",$event_datetime);
	$job_screening_level_assignees_add_query->bindValue(":added_datetime_epoch",$current_epoch);
	$job_screening_level_assignees_add_query->bindValue(":added_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$job_screening_level_assignees_add_query->bindValue(":is_active_status",$is_active_status);
	
	if ($job_screening_level_assignees_add_query->execute()) {
          $eventLog->log("after execute.");
            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");
           
			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}

function jsl_rel_criteria_add($company_id_input,$company_client_id_input,$job_id_input,$jsl_classification_detail_id_input,$jsl_sub_classification_detail_id_input,$last_inserted_id,$jsl_rel_criteria_name_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch) {
	
	
	global $dbcon,$eventLog;
	$is_active_status = '1';
	
    
	$jsl_rel_criteria_add_sql = "INSERT INTO `jsl_rel_document_check_initial_criteria`(`company_id`, `company_client_id`, `job_id`, `jsl_classification_detail_id`,`jsl_sub_classification_detail_id`, `jsl_info_id`,`jsl_rel_docu_check_initial_criteria_name`, `added_datetime`, `added_datetime_epoch`, `added_by_sm_memb_id`,`is_active_status`) VALUES (:company_id,:company_client_id,:job_id,:jsl_classification_detail_id,:jsl_sub_classification_detail_id,:jsl_info_id,:jsl_rel_docu_check_initial_criteria_name,:added_datetime,:added_datetime_epoch,:added_by_sm_memb_id,:is_active_status)";

	$jsl_rel_criteria_add_query = $dbcon->prepare($jsl_rel_criteria_add_sql);
	$jsl_rel_criteria_add_query->bindValue(":company_id",$company_id_input);
	$jsl_rel_criteria_add_query->bindValue(":company_client_id",$company_client_id_input);
	$jsl_rel_criteria_add_query->bindValue(":job_id",$job_id_input);	
	$jsl_rel_criteria_add_query->bindValue(":jsl_classification_detail_id",$jsl_classification_detail_id_input);
	$jsl_rel_criteria_add_query->bindValue(":jsl_sub_classification_detail_id",$jsl_sub_classification_detail_id_input);
	$jsl_rel_criteria_add_query->bindValue(":jsl_info_id",$last_inserted_id);
	$jsl_rel_criteria_add_query->bindValue(":jsl_rel_docu_check_initial_criteria_name",$jsl_rel_criteria_name_input);
	$jsl_rel_criteria_add_query->bindValue(":added_datetime",$event_datetime);
	$jsl_rel_criteria_add_query->bindValue(":added_datetime_epoch",$current_epoch);
	$jsl_rel_criteria_add_query->bindValue(":added_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$jsl_rel_criteria_add_query->bindValue(":is_active_status",$is_active_status);
	
	if ($jsl_rel_criteria_add_query->execute()) {
          $eventLog->log("after execute.");
            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");
           
			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}

function job_screening_level_assignees_add_duplicate_check($last_inserted_id,$recruiter_sm_memb_id) {
	global $dbcon;
	$constructed_array = array();
	
	$job_screening_level_assignees_add_duplicate_check_sql = "SELECT * FROM `jsl_recruiter_mapping` WHERE `jsl_info_id` = :jsl_info_id AND `recruiter_sm_memb_id` = :recruiter_sm_memb_id";
	$job_screening_level_assignees_add_duplicate_check_query = $dbcon->prepare($job_screening_level_assignees_add_duplicate_check_sql);	
	$job_screening_level_assignees_add_duplicate_check_query->bindValue(":jsl_info_id",$last_inserted_id);	
	$job_screening_level_assignees_add_duplicate_check_query->bindValue(":recruiter_sm_memb_id",$recruiter_sm_memb_id);	
	$job_screening_level_assignees_add_duplicate_check_query->execute(); 
	
	if($job_screening_level_assignees_add_duplicate_check_query->rowCount() > 0) {
		$job_screening_level_assignees_add_duplicate_check_query_result = $job_screening_level_assignees_add_duplicate_check_query->fetchAll();
	     return $job_screening_level_assignees_add_duplicate_check_query_result;
	
	}
	return $constructed_array;
}

function job_screening_level_assignees_check($job_id_input,$jsl_info_id_input){
	
	global $dbcon;
	$constructed_array = array();
	
	$job_screening_level_assignees_add_duplicate_check_sql = "SELECT * FROM `jsl_recruiter_mapping` WHERE `jsl_info_id` = :jsl_info_id AND `job_id` = :job_id";
	$job_screening_level_assignees_add_duplicate_check_query = $dbcon->prepare($job_screening_level_assignees_add_duplicate_check_sql);	
	$job_screening_level_assignees_add_duplicate_check_query->bindValue(":jsl_info_id",$jsl_info_id_input);	
	$job_screening_level_assignees_add_duplicate_check_query->bindValue(":job_id",$job_id_input);	
	$job_screening_level_assignees_add_duplicate_check_query->execute(); 
	
	if($job_screening_level_assignees_add_duplicate_check_query->rowCount() > 0) {
		$job_screening_level_assignees_add_duplicate_check_query_result = $job_screening_level_assignees_add_duplicate_check_query->fetchAll();
	     return $job_screening_level_assignees_add_duplicate_check_query_result;
	
	}
	return $constructed_array;
	
	
}

function job_screening_levels_record_count($job_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	
	$job_screening_levels_record_count_sql = "SELECT COUNT(*) AS count FROM `jsl_info` WHERE `job_id`=:job_id";
	$job_screening_levels_record_count_query = $dbcon->prepare($job_screening_levels_record_count_sql);
	$job_screening_levels_record_count_query->bindValue(":job_id",$job_id_input);			
	$job_screening_levels_record_count_query->execute(); 
	
	if($job_screening_levels_record_count_query->rowCount() > 0) {
		$job_screening_levels_record_count_query_result = $job_screening_levels_record_count_query->fetch();
		$current_count = $job_screening_levels_record_count_query_result["count"];
		
		$constructed_array["current_count"] = $current_count;
		
	    return $constructed_array;
	
	} else {
		$constructed_array["current_count"] = 0;
		
		return $constructed_array;
		
	}//close of if($candidate_education_qualification_record_count_select_query->rowCount() > 0) {
	
	
}

function jsl_rel_critaria_duplicate_check($job_id_input,$jsl_info_id_input,$jsl_rel_criteria_name_input) {
	global $dbcon;
	$constructed_array = array();
	$is_active_status = "1";
	
	$jsl_rel_critaria_duplicate_check_sql = "SELECT * FROM `jsl_rel_document_check_initial_criteria` WHERE `jsl_info_id` = :jsl_info_id AND `jsl_rel_docu_check_initial_criteria_name` = :jsl_rel_docu_check_initial_criteria_name AND `job_id` = :job_id AND `is_active_status` = :is_active_status";
	/* $jsl_rel_critaria_duplicate_check_sql = "SELECT * FROM `jsl_rel_document_check_initial_criteria` WHERE `jsl_info_id` = :jsl_info_id AND `jsl_rel_docu_check_initial_criteria_name` = :jsl_rel_docu_check_initial_criteria_name AND `jsl_sub_classification_detail_id` =:jsl_sub_classification_detail_id AND `company_id` = :company_id AND `job_id` = :job_id AND `is_active_status` = 1"; */
	$jsl_rel_critaria_duplicate_check_query = $dbcon->prepare($jsl_rel_critaria_duplicate_check_sql);	
	//$jsl_rel_critaria_duplicate_check_query->bindValue(":company_id",$company_id_input);
	$jsl_rel_critaria_duplicate_check_query->bindValue(":job_id",$job_id_input);	
	//$jsl_rel_critaria_duplicate_check_query->bindValue(":jsl_sub_classification_detail_id",$jsl_sub_classification_detail_id_input);
	$jsl_rel_critaria_duplicate_check_query->bindValue(":jsl_info_id",$jsl_info_id_input);
	$jsl_rel_critaria_duplicate_check_query->bindValue(":jsl_rel_docu_check_initial_criteria_name",$jsl_rel_criteria_name_input);	
	$jsl_rel_critaria_duplicate_check_query->bindValue(":is_active_status",$is_active_status);	
	$jsl_rel_critaria_duplicate_check_query->execute(); 
	
	if($jsl_rel_critaria_duplicate_check_query->rowCount() > 0) {
		$jsl_rel_critaria_duplicate_check_query_result = $jsl_rel_critaria_duplicate_check_query->fetch();
	     return $jsl_rel_critaria_duplicate_check_query_result;
	
	}
	return $constructed_array;
	
}

function jsl_rel_critaria_check($job_id_input,$jsl_info_id_input) {
	global $dbcon;
	$constructed_array = array();
	$is_active_status = "1";
	
	$jsl_rel_critaria_duplicate_check_sql = "SELECT * FROM `jsl_rel_document_check_initial_criteria` WHERE `jsl_info_id` = :jsl_info_id AND `job_id` = :job_id AND `is_active_status` = :is_active_status";
	/* $jsl_rel_critaria_duplicate_check_sql = "SELECT * FROM `jsl_rel_document_check_initial_criteria` WHERE `jsl_info_id` = :jsl_info_id AND `jsl_rel_docu_check_initial_criteria_name` = :jsl_rel_docu_check_initial_criteria_name AND `jsl_sub_classification_detail_id` =:jsl_sub_classification_detail_id AND `company_id` = :company_id AND `job_id` = :job_id AND `is_active_status` = 1"; */
	$jsl_rel_critaria_duplicate_check_query = $dbcon->prepare($jsl_rel_critaria_duplicate_check_sql);	
	//$jsl_rel_critaria_duplicate_check_query->bindValue(":company_id",$company_id_input);
	$jsl_rel_critaria_duplicate_check_query->bindValue(":job_id",$job_id_input);	
	//$jsl_rel_critaria_duplicate_check_query->bindValue(":jsl_sub_classification_detail_id",$jsl_sub_classification_detail_id_input);
	$jsl_rel_critaria_duplicate_check_query->bindValue(":jsl_info_id",$jsl_info_id_input);
	
	$jsl_rel_critaria_duplicate_check_query->bindValue(":is_active_status",$is_active_status);	
	$jsl_rel_critaria_duplicate_check_query->execute(); 
	
	if($jsl_rel_critaria_duplicate_check_query->rowCount() > 0) {
		$jsl_rel_critaria_duplicate_check_query_result = $jsl_rel_critaria_duplicate_check_query->fetchAll();
	     return $jsl_rel_critaria_duplicate_check_query_result;
	
	}
	return $constructed_array;
	
}


function jsl_rel_criteria_change_status($jrdcic_id,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch){
	
	
	
	global $dbcon,$eventLog;
	$is_active_status = '0';
	
    
	$jsl_rel_criteria_update_sql = "UPDATE `jsl_rel_document_check_initial_criteria` SET `last_updated_datetime`=:last_updated_datetime,`last_updated_datetime_epoch`=:last_updated_datetime_epoch,`last_updated_by_sm_memb_id`=:last_updated_by_sm_memb_id,`is_active_status`= :is_active_status WHERE `jrdcic_id` = :jrdcic_id ";

	$jsl_rel_criteria_update_query = $dbcon->prepare($jsl_rel_criteria_update_sql);
	$jsl_rel_criteria_update_query->bindValue(":jrdcic_id",$jrdcic_id);
	
	$jsl_rel_criteria_update_query->bindValue(":last_updated_datetime",$event_datetime);
	$jsl_rel_criteria_update_query->bindValue(":last_updated_datetime_epoch",$current_epoch);
	$jsl_rel_criteria_update_query->bindValue(":last_updated_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$jsl_rel_criteria_update_query->bindValue(":is_active_status",$is_active_status);
	
	if ($jsl_rel_criteria_update_query->execute()) {
          $eventLog->log("after execute.");
          return true;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

	
}

function jsl_assignees_change_status($jsl_recruiter_mapping_id,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch){
	
	
	
	global $dbcon,$eventLog;
	$is_active_status = '0';
	
    
	$jsl_rel_criteria_update_sql = "UPDATE `jsl_recruiter_mapping` SET `last_updated_datetime`=:last_updated_datetime,`last_updated_datetime_epoch`=:last_updated_datetime_epoch,`last_updated_by_sm_memb_id`=:last_updated_by_sm_memb_id,`is_active_status`= :is_active_status WHERE `jsl_recruiter_mapping_id` = :jsl_recruiter_mapping_id ";

	$jsl_rel_criteria_update_query = $dbcon->prepare($jsl_rel_criteria_update_sql);
	$jsl_rel_criteria_update_query->bindValue(":jsl_recruiter_mapping_id",$jsl_recruiter_mapping_id);
	
	$jsl_rel_criteria_update_query->bindValue(":last_updated_datetime",$event_datetime);
	$jsl_rel_criteria_update_query->bindValue(":last_updated_datetime_epoch",$current_epoch);
	$jsl_rel_criteria_update_query->bindValue(":last_updated_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$jsl_rel_criteria_update_query->bindValue(":is_active_status",$is_active_status);
	
	if ($jsl_rel_criteria_update_query->execute()) {
          $eventLog->log("after execute.");
          return true;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

	
}



exit;
?>