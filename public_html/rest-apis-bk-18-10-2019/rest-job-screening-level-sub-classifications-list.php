<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "2")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
				$jsl_classification_id_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_classification_id']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_classification_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {


				if ($ip_address_input == "") {
					
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else if ($jsl_classification_id_input == "") {
					
					$response['data'] = array();
					$response['status'] = "missing-jsl-classification-id";
					$response['status_description'] = "Missing Job screening level related classification id";
					
					$eventLog->log("Missing Job screening level related classification id.");
					
				} else {
					
					try {
						
						$job_rel_screening_levels_sub_classifications_list_info_result = job_screening_level_rel_sub_classifications_list_info($jsl_classification_id_input);
						
						if (count($job_rel_screening_levels_sub_classifications_list_info_result) > 0) {
						
							$response['data'] = $job_rel_screening_levels_sub_classifications_list_info_result;
							$response['status'] = "job-screening-level-related-sub-classifications-list-successfully-fetched";
							$response['status_description'] = "Job Screening Level related Sub Classifications list Successfully Received";
						
						} else {
						
							$response['data'] = array();
							$response['status'] = "no-sub-classifications-found";
							$response['status_description'] = "No sub classifications found for this classification";	
						}
						
					} catch (Exception $e) {
									
						$response['data'] = array();
						$response['status'] = "job-screening-level-related-sub-classifications-list-fetching-error";
						$response['status_description'] = "Error occurred while fetching Job Screening Level related Sub Classifications list.";
					
									
					}
				
					
				}
					
			}
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function job_screening_level_rel_sub_classifications_list_info($jsl_classification_id_input) {
    global $dbcon;
	$constructed_array = array();
	
	$job_screening_level_rel_sub_classifications_list_info_sql = "SELECT * FROM `jsl_sub_classification_details` WHERE `jsl_classification_detail_id` =:jsl_classification_detail_id ";
	$job_rel_screening_levels_classifications_list_info_query = $dbcon->prepare($job_screening_level_rel_sub_classifications_list_info_sql);
	$job_rel_screening_levels_classifications_list_info_query->bindValue(":jsl_classification_detail_id",$jsl_classification_id_input);		
	$job_rel_screening_levels_classifications_list_info_query->execute(); 
	
	if($job_rel_screening_levels_classifications_list_info_query->rowCount() > 0) {
		$job_rel_screening_levels_classifications_list_info_query_result = $job_rel_screening_levels_classifications_list_info_query->fetchAll();
	     return $job_rel_screening_levels_classifications_list_info_query_result;
	
	}//close of if($candidate_rel_uploaded_resumes_based_on_sm_memb_id_query->rowCount() > 0) {
	return $constructed_array;
}


exit;
?>