<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of jobs, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "6")) { 

	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$company_client_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_client_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_client_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$job_status_input = trim(isset($ea_received_rest_ws_raw_array_input['job_status']) ? filter_var($ea_received_rest_ws_raw_array_input['job_status'], FILTER_SANITIZE_NUMBER_INT) : '');
				$records_after_input = trim(isset($ea_received_rest_ws_raw_array_input['records_after']) ? filter_var($ea_received_rest_ws_raw_array_input['records_after'], FILTER_SANITIZE_NUMBER_INT) : '');
				$number_of_records_input = trim(isset($ea_received_rest_ws_raw_array_input['number_of_records']) ? filter_var($ea_received_rest_ws_raw_array_input['number_of_records'], FILTER_SANITIZE_NUMBER_INT) : '');
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
			
				
				$eventLog->log("ip_address_input -> " . $ip_address_input);
				
				//Check if all inputs are received correctly from the REST Web Service
				if ($company_id_input == "") {
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company_id";
					$response['status_description'] = "missing company id .";
					
					$eventLog->log("Please provide all information.");
					
					
				} else  if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-additional-information";
					$response['status_description'] = "Some Additional Information like IP Address (IPv4) is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");
							  
				} else {
					//All inputs are Valid
				
					$eventLog->log("All inputs are valid.");
				
                    //$jobs_list_info_result = jobs_list_info($company_id_input);
					
					if ($company_client_id_input == "") {
						$company_client_id_input = null;
					}//close of if ($company_client_id_input == "") {
						
					
					if ($ea_extracted_jwt_token_user_company_id != "") {
						
							$jobs_list_info_result = get_jobs_list_with_pagination_inputs($ea_extracted_jwt_token_user_company_id,$job_status_input,$company_client_id_input,$records_after_input,$number_of_records_input);
						
					} else {
						//Platform Scenario: The logged in User is either a Site Admin or a Super Admin
						
							$jobs_list_info_result = get_jobs_list_with_pagination_inputs($company_id_input,$job_status_input, $company_client_id_input,$records_after_input, $number_of_records_input);
					}//close of else of if ($company_client_id_input == "") {
						
					
					
					if (count($jobs_list_info_result) > 0) {
			
						//One or More User Groups Exist and Active
							
						$response['data'] = $jobs_list_info_result;
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['status'] = "jobs-list-received";
						$response['status_description'] = "jobs List Successfully Received";
						
						$jobs_list_info_result_json_encoded = json_encode($jobs_list_info_result);
					
						$eventLog->log("jobs List -> " . $jobs_list_info_result_json_encoded); 
							
				    } else {
							
					    //Construct Content, that will be sent in Response body, of the REST Web Service
					    $response['data'] = array();
					    $response['status'] = "active-jobs-doesnot-exist";
					    $response['status_description'] = "No Active jobs list Exist, please check and try again.";
				
						//No Active jobs list Exist
					    $eventLog->log("jobs List -> No Active jobs Exist, please check and try again."); 
					}//close of else of if ($jobs_list_result_count > "0") {
				
			    }
													
			}//close of if ($ea_is_user_page_access_authorized)	
				
			   
			
		} else {

			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
		 
		    //Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
		 
		    header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
	   
	    }		
    }//close of if ($ea_maintenance_mode == false) {
			
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}



//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){

exit;
?>