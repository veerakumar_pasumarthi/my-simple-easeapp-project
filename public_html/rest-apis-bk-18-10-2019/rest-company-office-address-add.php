<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
				
					if (isset($ea_received_rest_ws_raw_array_input['premise_name'])) {
						$content .= $ea_received_rest_ws_raw_array_input['premise_name'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['premise_name'])) 
 
                    if (isset($ea_received_rest_ws_raw_array_input['address_line_1'])) {
						$content .= $ea_received_rest_ws_raw_array_input['address_line_1'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['address_line_1'])) 

					if (isset($ea_received_rest_ws_raw_array_input['address_line_2'])) {
						$content .= $ea_received_rest_ws_raw_array_input['address_line_2'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['address_line_2'])) 

					if (isset($ea_received_rest_ws_raw_array_input['city'])) {
						$content .= $ea_received_rest_ws_raw_array_input['city'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['city'])) 

					if (isset($ea_received_rest_ws_raw_array_input['state'])) {
						$content .= $ea_received_rest_ws_raw_array_input['state'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['state'])) 
 
                    if (isset($ea_received_rest_ws_raw_array_input['country'])) {
						$content .= $ea_received_rest_ws_raw_array_input['country'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['country'])) 

					if (isset($ea_received_rest_ws_raw_array_input['pincode'])) {
						$content .= $ea_received_rest_ws_raw_array_input['pincode'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['pincode'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$premise_name_input = trim(isset($ea_received_rest_ws_raw_array_input['premise_name']) ? filter_var($ea_received_rest_ws_raw_array_input['premise_name'], FILTER_SANITIZE_STRING) : '');
				$address_line_1_input = trim(isset($ea_received_rest_ws_raw_array_input['address_line_1']) ? filter_var($ea_received_rest_ws_raw_array_input['address_line_1'], FILTER_SANITIZE_STRING) : '');
				$address_line_2_input = trim(isset($ea_received_rest_ws_raw_array_input['address_line_2']) ? filter_var($ea_received_rest_ws_raw_array_input['address_line_2'], FILTER_SANITIZE_STRING) : '');
				$city_input = trim(isset($ea_received_rest_ws_raw_array_input['city']) ? filter_var($ea_received_rest_ws_raw_array_input['city'], FILTER_SANITIZE_STRING) : '');
				$state_input = trim(isset($ea_received_rest_ws_raw_array_input['state']) ? filter_var($ea_received_rest_ws_raw_array_input['state'], FILTER_SANITIZE_STRING) : '');
				$country_input = trim(isset($ea_received_rest_ws_raw_array_input['country']) ? filter_var($ea_received_rest_ws_raw_array_input['country'], FILTER_SANITIZE_STRING) : '');
				$pincode_input = trim(isset($ea_received_rest_ws_raw_array_input['pincode']) ? filter_var($ea_received_rest_ws_raw_array_input['pincode'], FILTER_SANITIZE_NUMBER_INT) : '');
						
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				if ($company_id_input == "") {
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "missing company id";
					
					$eventLog->log("missing-company-name: Please provide a valid Company Name.");
					
				} else if ($address_line_1_input == "") {
					//Invalid Mobile Number scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-address-line-1";
					$response['status_description'] = "Missing  Address Line 1";
					
					$eventLog->log("missing-address-line-1: Please provide a valid Company Address.");
					
				} else if ($pincode_input == "") {
					//Invalid Salutation scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-address-pincode";
					$response['status_description'] = "Missing Company Address Pincode";
					
					$eventLog->log("missing-company-address-pincode: Missing Company Address Pincode.");			
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
						
						//Event Time, as per Indian Standard Time
						$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
						$company_address_add_next_step = "";
						
						if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$company_address_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$company_address_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
						} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
						}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
							
						if ($company_address_add_next_step == "PROCEED-TO-NEXT-STEP") {
							try {
								
								$company_office_address_duplicate_check_result = company_office_address_duplicate_check_based_on_company_id($company_id_input,$address_line_1_input,$pincode_input);
								
								if (count($company_office_address_duplicate_check_result) > 0) {
									
										    $response['data'] = array();
											$response['status'] = "company-office-address-already-exists";
											$response['status_description'] = "Company office Address already exist";
										
											$eventLog->log("company-office-address-already-exists: Company office Address already exist.");
									
								} else {
									$company_office_address_inserted_id = company_office_address_record_insert($company_id_input, $premise_name_input, $address_line_1_input, $address_line_2_input, $city_input, $state_input, $country_input, $pincode_input, $event_datetime, $current_epoch);
									
									if ($company_office_address_inserted_id != "") {
										
										    $response['data'] = array();
											$response['status'] = "company-office-address-insertion-successful";
											$response['status_description'] = " Company  office address is added Successfully.";
										
											$eventLog->log("company-office-address-insert-successful: Company offfice address is added Successfully.");   
										
									} else {
										//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
										$response['data'] = array();
										$response['status'] = "company-office-address-insertion-error";
										$response['status_description'] = "Error occurred when adding the Company office address in the Database.";
										
										$eventLog->log("company-office-address-insert-error: There is an error, when adding the Company office address in the Database.");
										
									}
									
								}//close of else of if (count($user_ass_oc_validation_result) > 0) {		
								
							} catch (Exception $e) {
								
								//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
								$response['data'] = array();
								$response['status'] = "company-office-address-insertion-error";
								$response['status_description'] = "Error occurred when adding the Company office address in the Database.";
								
								$eventLog->log("company-office-address-insertion-error: There is an error, when adding the Company office address in the Database.");
							}
						}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
			
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
		}
		
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
    
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
			
	
	}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {

	//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){

exit;
?>