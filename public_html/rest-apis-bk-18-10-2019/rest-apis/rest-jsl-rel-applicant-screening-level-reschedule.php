
<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "8")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'])) 	
							
					if (isset($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['rescheduled_event_datetime'])) {
						$content .= $ea_received_rest_ws_raw_array_input['rescheduled_event_datetime'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['rescheduled_event_datetime']))

					if (isset($ea_received_rest_ws_raw_array_input['rescheduled_jsl_criteria'])) {
						$content .= $ea_received_rest_ws_raw_array_input['rescheduled_jsl_criteria'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['rescheduled_jsl_criteria'])) 					
					if (isset($ea_received_rest_ws_raw_array_input['notes'])) {
						$content .= $ea_received_rest_ws_raw_array_input['notes'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['notes'])) 
					
					
				}	
	
					$jsl_applicant_evaluation_info_id_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					$job_applicant_request_info_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					
		
					$rescheduled_jsl_criteria_input = trim(isset($ea_received_rest_ws_raw_array_input['rescheduled_jsl_criteria']) ? filter_var($ea_received_rest_ws_raw_array_input['rescheduled_jsl_criteria'], FILTER_SANITIZE_STRING) : '');
					
					$notes_input = trim(isset($ea_received_rest_ws_raw_array_input['notes']) ? filter_var($ea_received_rest_ws_raw_array_input['notes'], FILTER_SANITIZE_STRING) : '');
					
					$scheduled_event_country_two_lett_code_input = trim(isset($ea_received_rest_ws_raw_array_input['scheduled_event_country_two_lett_code']) ? filter_var($ea_received_rest_ws_raw_array_input['scheduled_event_country_two_lett_code'], FILTER_SANITIZE_STRING) : '');
					
					$scheduled_event_iana_timezone_input = trim(isset($ea_received_rest_ws_raw_array_input['scheduled_event_iana_timezone']) ? filter_var($ea_received_rest_ws_raw_array_input['scheduled_event_iana_timezone'], FILTER_SANITIZE_STRING) : '');
					
					$rescheduled_event_datetime_input = trim(isset($ea_received_rest_ws_raw_array_input['rescheduled_event_datetime']) ? filter_var($ea_received_rest_ws_raw_array_input['rescheduled_event_datetime'], FILTER_SANITIZE_STRING) : '');
					
					
					if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
					} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
					}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						

					if ($jsl_applicant_evaluation_info_id_input == "") {
				
						
						$response['data'] = array();
						$response['status'] = "missing-jsl-applicant-evaluation-info-id";
						$response['status_description'] = "missing JSL applicant evaluation info id";
						
						$eventLog->log("missing JSL applicant evaluation info id.");
						
					} else if ($job_applicant_request_info_id_input == "") {
						
						$response['data'] = array();
						$response['status'] = "missing-job-applicant-request-info-id";
						$response['status_description'] = "Missing Job applicant request info Id";
						
						$eventLog->log("Missing Job applicant request info Id");
						
					} else if ($rescheduled_event_datetime_input == "") {
					
						$response['data'] = array();
						$response['status'] = "missing-rescheduled-event-date-time";
						$response['status_description'] = "Missing rescheduled event date time";
						
						$eventLog->log("Missing rescheduled event date time.");
						
					} else if ($rescheduled_jsl_criteria_input == "") {
						
						$response['data'] = array();
						$response['status'] = "missing-rescheduled-jsl-criteria";
						$response['status_description'] = "Missing Rescheduled JSL criteria";
						
						$eventLog->log("Missing Rescheduled JSL criteria");
						
					
					} else if ($notes_input == "") {
						
						$response['data'] = array();
						$response['status'] = "missing-notes";
						$response['status_description'] = "Missing notes for this reschedule event";
						
						$eventLog->log("Missing notes for this reschedule event.");
						
					
					} else {	
						
							$job_rel_screening_level_add_next_step = "";
							if ($ea_extracted_jwt_token_user_type == "admin") {
								
								if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
									$job_rel_screening_level_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("before company id condition");
								} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
									
									$job_rel_screening_level_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("after add condition");
								} else {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "insufficient-permissions-different-company-user-id";
									$response['status_description'] = "Insufficient Set of Permissions";
									
									//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
									header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
									$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
									
								}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								
								
								
							} else {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
								$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
							
							}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
								
								
							
								
							$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
							$notes_safe_html_input = get_cleaned_safe_html_content_input($notes_input, $page_content_file_config);
					
							$jsl_evaluation_info_result = jsl_evaluation_info_get($jsl_applicant_evaluation_info_id_input);
						
							$company_id_input = $jsl_evaluation_info_result["company_id"];
							$company_client_id_input = $jsl_evaluation_info_result["company_client_id"];
							$job_id_input = $jsl_evaluation_info_result["job_id"];
							$jsl_classification_detail_id_input = $jsl_evaluation_info_result["jsl_classification_detail_id"];
							$jsl_classification_detail_name_input = $jsl_evaluation_info_result["jsl_classification_detail_name"];
							$jsl_classification_detail_seo_name_input = $jsl_evaluation_info_result["jsl_classification_detail_seo_name"];
							$jsl_sub_classification_detail_id_input = $jsl_evaluation_info_result["jsl_sub_classification_detail_id"];
							$jsl_sub_classification_detail_name_input = $jsl_evaluation_info_result["jsl_sub_classification_detail_name"];
							$jsl_sub_classification_detail_seo_name_input = $jsl_evaluation_info_result["jsl_sub_classification_detail_seo_name"];
							$jsl_info_id_input = $jsl_evaluation_info_result["jsl_info_id"];
							$job_applicant_sm_memb_id_input = $jsl_evaluation_info_result["job_applicant_sm_memb_id"];
							$event_status_input = $jsl_evaluation_info_result["event_status"];
							
							if($jsl_classification_detail_name_input == "Interview") {
								
								if($jsl_sub_classification_detail_name_input == "Telephonic Interview") {
									
									$telephonic_interview_number_input = $rescheduled_jsl_criteria_input;
									
								} else if ($jsl_sub_classification_detail_name_input == "Online Interview"){
									$online_interview_link_input = $rescheduled_jsl_criteria_input;
									
								} else if($jsl_sub_classification_detail_name_input == "In-Person Interview") {
									
									$in_person_interview_location_address_input = $rescheduled_jsl_criteria_input;
								}
								
								
							} else {
								$online_interview_link_input = null;
								$telephonic_interview_number_input = null;
								$in_person_interview_location_address_input = null;
							}
							
							
							
							$rescheduled_event_datetime_epoch_input = df_convert_date_to_unix_timestamp($rescheduled_event_datetime_input);
							
							if ($job_rel_screening_level_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
								$job_pool_specific_existence_check_result = job_pool_specific_existence_check($ea_extracted_jwt_token_sub,$job_id_input);
								
								if(count($job_pool_specific_existence_check_result) > 0) {
								
									try {
										
										if($event_status_input == '1') {
											
											$applicant_screening_level_reschedule_update_result =applicant_screening_level_reschedule_update($jsl_applicant_evaluation_info_id_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
											
											if($applicant_screening_level_reschedule_update_result == true) {
												$last_inserted_id =
												jsl_applicant_evaluation_info_add($company_id_input,$company_client_id_input,$job_id_input,$jsl_classification_detail_id_input,$jsl_classification_detail_name_input,$jsl_classification_detail_seo_name_input,$jsl_sub_classification_detail_id_input,$jsl_sub_classification_detail_name_input,$jsl_sub_classification_detail_seo_name_input,$jsl_info_id_input,$job_applicant_request_info_id_input,$job_applicant_sm_memb_id_input,$rescheduled_event_datetime_input,$rescheduled_event_datetime_epoch_input,$online_interview_link_input,$telephonic_interview_number_input,$in_person_interview_location_address_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
												
												if ($last_inserted_id != "") {
													
													$jsl_applicant_evaluation_info_result_notes_add_result =
													jsl_applicant_evaluation_info_result_notes_add($company_id_input,$company_client_id_input,$job_id_input,$job_applicant_sm_memb_id_input,$jsl_applicant_evaluation_info_id_input,$notes_input,$notes_safe_html_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
													
													if($jsl_applicant_evaluation_info_result_notes_add_result != "") {
													
														$response['data'] = array();
														$response['status'] = "applicant-specific-job-screening-level-rescheduled-successfully";
														$response['status_description'] = "Applicant specific Job screening level rescheduled Successfully.";
												
														$eventLog->log("Applicant specific Job screening level rescheduled Successfully.");   
													
													
													} else {
																
															$response['data'] = array();
															$response['status'] = "applicant-specific-job-screening-level-reschedule-error";
															$response['status_description'] = "Error occurred when rescheduling job screening level.";
															
															$eventLog->log("Error occurred when rescheduling job screening level.");
															
													}
													
												} else {
													
													$eventLog->log("Applicant screening level reschedule insertion error.");
													
												}
												
											} else {
												
												$eventLog->log("Applicant screening level reschedule updation error.");  
												
											}
											
										} else {
											
											$response['data'] = array();
											$response['status'] = "screening-level-was-not-currently-scheduled";
											$response['status_description'] = "screening level is not current schedule screening level for this applicant.";
												
											$eventLog->log("screening level is not current schedule screening level for this applicant.");
											
										}
										
									} catch (Exception $e) {
										
										$response['data'] = array();
										$response['status'] = "job-related-screening-level-insertion-error";
										$response['status_description'] = "Error occurred when adding the job related screening level.";
											
										$eventLog->log("job-related-screening-level-insertion-error: Error occurred when adding the job related screening level.");
											
									}
									
								} else {
									
									$response['data'] = array();
									$response['status'] = "insufficient-permissions";
									$response['status_description'] = "Insufficient Set of Permissions";
									
									//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
									header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
					
									$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
									
								}
							}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
							
						}
			//}//close of else of if ($email_id_input == "") {
					
		}//close of //close of if ($ea_is_user_page_access_authorized) {
					
				
	} else {
				
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "invalid-auth-token-submitted";
				$response['status_description'] = "Invalid Authentication";
				
				//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
				header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
				
				$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
				
			}//close of else of if ($ea_auth_token_validation_status) {
		
		}//close of if ($ea_maintenance_mode == false) {
		
} else {

		//Construct Content, that will be sent in Response body, of the REST Web Service
		$response['data'] = array();
		$response['status'] = "invalid-input";
		$response['status_description'] = "Invalid Input, Please check and provide all information.";
		
		//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
		
		$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
				
		
		}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




	//Check if Maintenance Mode is Turned On
	if ($ea_maintenance_mode) {	
		
		//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
		//header('Maintenance-Progress: true', false);
		//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
		$response['data'] = array();
		$response['status'] = "application-maintenance-in-progress";
		$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
		
		$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
		
	} else {	

		//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
		header('Content-Type: application/json');
		echo json_encode($response,JSON_PRETTY_PRINT);
		
	}//close of else of if ($ea_maintenance_mode){
		

function jsl_applicant_evaluation_info_add($company_id_input,$company_client_id_input,$job_id_input,$jsl_classification_detail_id_input,$jsl_classification_detail_name_input,$jsl_classification_detail_seo_name_input,$jsl_sub_classification_detail_id_input,$jsl_sub_classification_detail_name_input,$jsl_sub_classification_detail_seo_name_input,$jsl_info_id_input,$job_applicant_request_info_id_input,$job_applicant_sm_memb_id_input,$rescheduled_event_datetime_input,$rescheduled_event_datetime_epoch_input,$online_interview_link_input,$telephonic_interview_number_input,$in_person_interview_location_address_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch) {
	
	
	global $dbcon,$eventLog;
	$is_active_status = '1';
	$event_status_input = '1';
 
	$jsl_applicant_evaluation_info_add_sql = "INSERT INTO `jsl_applicant_evaluation_info`(`company_id`, `company_client_id`, `job_id`, `jsl_classification_detail_id`, `jsl_classification_detail_name`, `jsl_classification_detail_seo_name`, `jsl_sub_classification_detail_id`, `jsl_sub_classification_detail_name`, `jsl_sub_classification_detail_seo_name`, `jsl_info_id`, `job_applicant_request_info_id`, `job_applicant_sm_memb_id`, `scheduled_event_datetime`, `scheduled_event_datetime_epoch`, `online_interview_link`, `telephonic_interview_number`, `in_person_interview_location_address`, `added_datetime`, `added_datetime_epoch`, `added_by_sm_memb_id`, `event_status`, `is_active_status`) VALUES(:company_id,:company_client_id,:job_id,:jsl_classification_detail_id,:jsl_classification_detail_name,:jsl_classification_detail_seo_name,:jsl_sub_classification_detail_id,:jsl_sub_classification_detail_name,:jsl_sub_classification_detail_seo_name,:jsl_info_id,:job_applicant_request_info_id,:job_applicant_sm_memb_id,:scheduled_event_datetime,:scheduled_event_datetime_epoch,:online_interview_link,:telephonic_interview_number,:in_person_interview_location_address,:added_datetime,:added_datetime_epoch,:added_by_sm_memb_id,:event_status,:is_active_status)";

	$jsl_applicant_evaluation_info_add_query = $dbcon->prepare($jsl_applicant_evaluation_info_add_sql);
	$jsl_applicant_evaluation_info_add_query->bindValue(":company_id",$company_id_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":company_client_id",$company_client_id_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":job_id",$job_id_input);	
	$jsl_applicant_evaluation_info_add_query->bindValue(":jsl_classification_detail_id",$jsl_classification_detail_id_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":jsl_classification_detail_name",$jsl_classification_detail_name_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":jsl_classification_detail_seo_name",$jsl_classification_detail_seo_name_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":jsl_sub_classification_detail_id",$jsl_sub_classification_detail_id_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":jsl_sub_classification_detail_name",$jsl_sub_classification_detail_name_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":jsl_sub_classification_detail_seo_name",$jsl_sub_classification_detail_seo_name_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":jsl_info_id",$jsl_info_id_input);
    $jsl_applicant_evaluation_info_add_query->bindValue(":job_applicant_request_info_id",$job_applicant_request_info_id_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":job_applicant_sm_memb_id",$job_applicant_sm_memb_id_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":scheduled_event_datetime",$rescheduled_event_datetime_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":scheduled_event_datetime_epoch",$rescheduled_event_datetime_epoch_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":online_interview_link",$online_interview_link_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":telephonic_interview_number",$telephonic_interview_number_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":in_person_interview_location_address",$in_person_interview_location_address_input);

	$jsl_applicant_evaluation_info_add_query->bindValue(":added_datetime",$event_datetime);
	$jsl_applicant_evaluation_info_add_query->bindValue(":added_datetime_epoch",$current_epoch);
	$jsl_applicant_evaluation_info_add_query->bindValue(":added_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$jsl_applicant_evaluation_info_add_query->bindValue(":event_status",$event_status_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":is_active_status",$is_active_status);
	
	if ($jsl_applicant_evaluation_info_add_query->execute()) {
          $eventLog->log("after execute.");
            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");
           
			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}

function applicant_screening_level_reschedule_update($jsl_applicant_evaluation_info_id_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch) {
	
	global $dbcon,$eventLog;
	$is_active_status = '1';
	$event_status_input = '2';
	
	$applicant_screening_level_reschedule_update_sql = "UPDATE `jsl_applicant_evaluation_info` SET `event_status`= :event_status,`last_updated_datetime`=:last_updated_datetime,`last_updated_datetime_epoch`=:last_updated_datetime_epoch,`last_updated_by_sm_memb_id`=:last_updated_by_sm_memb_id WHERE `jsl_applicant_evaluation_info_id` = :jsl_applicant_evaluation_info_id ";
	
	$applicant_screening_level_reschedule_update_query = $dbcon->prepare($applicant_screening_level_reschedule_update_sql);
	$applicant_screening_level_reschedule_update_query->bindValue(":last_updated_datetime",$event_datetime);
	$applicant_screening_level_reschedule_update_query->bindValue(":last_updated_datetime_epoch",$current_epoch);
	$applicant_screening_level_reschedule_update_query->bindValue(":last_updated_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$applicant_screening_level_reschedule_update_query->bindValue(":event_status",$event_status_input);
	$applicant_screening_level_reschedule_update_query->bindValue(":jsl_applicant_evaluation_info_id",$jsl_applicant_evaluation_info_id_input);
	
	if ($applicant_screening_level_reschedule_update_query->execute()) {
        
			return true;

	} else {
	
		return false;
	}
	
	
}

function applicant_screening_level_duplicate_check($job_applicant_request_info_id_input,$jsl_info_id_inpu){

	global $dbcon,$eventLog;
	$event_status_input = '1';
	$constructed_array = array();
	
	$applicant_screening_level_duplicate_check_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `jsl_info_id` = :jsl_info_id AND `job_applicant_request_info_id` =:job_applicant_request_info_id AND `event_status` =:event_status";
	
	$applicant_screening_level_duplicate_check_query = $dbcon->prepare($applicant_screening_level_duplicate_check_sql);
	$applicant_screening_level_duplicate_check_query->bindValue(":jsl_info_id",$jsl_info_id_inpu);
	$applicant_screening_level_duplicate_check_query->bindValue(":event_status",$event_status_input);
	$applicant_screening_level_duplicate_check_query->bindValue(":job_applicant_request_info_id",$job_applicant_request_info_id_input);
	
	$applicant_screening_level_duplicate_check_query->execute(); 
	
	if($applicant_screening_level_duplicate_check_query->rowCount() > 0) {
		$applicant_screening_level_duplicate_check_query_result = $applicant_screening_level_duplicate_check_query->fetchAll();
	     return $applicant_screening_level_duplicate_check_query_result;
	
	}
	return $constructed_array;


}

function job_pool_specific_existence_check($ea_extracted_jwt_token_sub,$job_id_input) {
	
	global $dbcon,$eventLog;
	$assignment_status_input = '1';
	$constructed_array = array();
	
	$job_pool_specific_existence_check_sql = "SELECT * FROM `job_management_assignees` WHERE `job_id` = :job_id AND `assigned_user_sm_memb_id` = :assigned_user_sm_memb_id AND `assignment_status` = :assignment_status";
	
	$job_pool_specific_existence_check_query = $dbcon->prepare($job_pool_specific_existence_check_sql);
	$job_pool_specific_existence_check_query->bindValue(":job_id",$job_id_input);
	$job_pool_specific_existence_check_query->bindValue(":assignment_status",$assignment_status_input);
	$job_pool_specific_existence_check_query->bindValue(":assigned_user_sm_memb_id",$ea_extracted_jwt_token_sub);
	
	$job_pool_specific_existence_check_query->execute(); 
	
	if($job_pool_specific_existence_check_query->rowCount() > 0) {
		$job_pool_specific_existence_check_query_result = $job_pool_specific_existence_check_query->fetchAll();
	     return $job_pool_specific_existence_check_query_result;
	
	}
	return $constructed_array;
}

exit;
?>