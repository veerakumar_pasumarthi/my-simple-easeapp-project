<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * REST API for candidate all education qualifications collect
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "4")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['user_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['user_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['user_id']))
                    
                    if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
					$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id']))					
					
					if (isset($ea_received_rest_ws_raw_array_input['all_skillsets'])) {
						$content .= json_encode($ea_received_rest_ws_raw_array_input['all_skillsets']) . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['all_skillsets'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					//$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$all_skillsets_input = $ea_received_rest_ws_raw_array_input['all_skillsets'];
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//Check if all inputs are received correctly from the REST Web Service
				if ($user_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-user-id";
					$response['status_description'] = "Missing User ID";
					
					$eventLog->log("missing-user-id: Please provide a valid User ID.");
					
				} else if ($company_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company ID";
					
					$eventLog->log("missing-company-id: Please provide a valid Company ID.");
					
					
				} else if ($all_skillsets_input == "") {	
					//Invalid Education Qualifications Received scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "empty-skillset-details-input";
					$response['status_description'] = "Empty Skillset details";
					
					$eventLog->log("empty-skillset-details-input: Skillsets are not received for this Candidate, Please check and try again.");
					
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Missing Some Additional Information";
					
					$eventLog->log("missing-some-additional-information: Some Additional Information is missing, please check and try again.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					$ea_action_specific_auth_check_result = "";
					
					//User Details, based on User ID input, through api request
					$user_basic_details_result = user_basic_details_check_based_on_user_id($user_id_input);
					
					if (count($user_basic_details_result) > 0) {
						
						$user_rel_company_id = $user_basic_details_result["company_id"];
						$user_rel_sm_user_status = $user_basic_details_result["sm_user_status"];
						
						$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
						//$ea_action_specific_auth_check_result = ea_check_action_specific_user_privileges_different_user_levels_reset_password_feature($ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_user_company_id, $ea_extracted_jwt_token_sub, $user_id_input, $ea_extracted_jwt_token_user_privileges_list, $user_rel_company_id, $page_filename);					
						
						//echo "ea_action_specific_auth_check_result: " . $ea_action_specific_auth_check_result;					
							
							
						
					
						
						if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
								$eventLog->log("inter into try");
								//Event Time, as per Indian Standard Time
								$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
								
									
								//Define an array, to Collect "degree" and "field of study" field values of all received Education Qualification info.
								$all_received_skill_name_fields_array = array();
								
								//Define an array, to Collect "degree" and "field of study" field values of all Education Qualification info, from the Database.
								$all_db_based_skill_name_fields_array = array();
								
								
								//Define an array, to Collect all received Education Qualification info, with corresponding record processing status and info
								$all_collected_content_with_result_array = array();
								$eventLog->log("before count");
								//Collect All Education Qualifications List, of particular candidate, to extract degree and field of study field content
								$candidate_skillset_records_list_result = candidate_skillset_records_list($user_id_input);
								$eventLog->log("after count");
								$candidate_skillset_records_list_result_count = count($candidate_skillset_records_list_result);
								
								$eventLog->log("candidate_skillset_records_list_result_count: " . $candidate_skillset_records_list_result_count);
								
								
								if ((is_array($all_skillsets_input)) && ((count($all_skillsets_input) > 0) || ($candidate_skillset_records_list_result_count > 0))) {
										
									//Do process each record (Education Qualification)
									foreach ($all_skillsets_input as $all_skillsets_input_row) {
										
										$received_skill_name_fields_array = array();
										$collected_content_with_result_array = array();
										
										$collected_content_with_result_array = $all_skillsets_input_row;
										
										
										$collected_content_with_result_array_json_encoded = json_encode($collected_content_with_result_array);
										$eventLog->log("collected_array row initial stage: " . $collected_content_with_result_array_json_encoded);	
										
										$all_skillsets_input_row_count = count($all_skillsets_input_row);
										//$eventLog->log("collected_array row initial stage count: " . $all_education_qualifications_input_row_count);
										
										if (count($all_skillsets_input_row) == 6) {
											
											$eventLog->log("inside count(all_skillsets_input_row) == 6 if condition: ");
											
											
											//[{"ceq_id":"","degree":"dgfgf","field_of_study":"ghghgh","institution_name":"fhghghg","university_name":"","board_name":"","year_of_passout":"2012","earned_percentage":"65","earned_grade":""},{"ceq_id":"","degree":"gfgfg","field_of_study":"fgdfgf","institution_name":"fgdfg","university_name":"","board_name":"","year_of_passout":"2011","earned_percentage":"75","earned_grade":""}]
										
											$crs_id = trim(isset($all_skillsets_input_row["crs_id"]) ? filter_var($all_skillsets_input_row["crs_id"], FILTER_SANITIZE_NUMBER_INT) : '');
											$skill_name = trim(isset($all_skillsets_input_row["skill_name"]) ? filter_var($all_skillsets_input_row["skill_name"], FILTER_SANITIZE_STRING) : '');
											$version = trim(isset($all_skillsets_input_row["version"]) ? filter_var($all_skillsets_input_row["version"], FILTER_SANITIZE_STRING) : '');
											$experience_years = trim(isset($all_skillsets_input_row["experience_years"]) ? filter_var($all_skillsets_input_row["experience_years"], FILTER_SANITIZE_STRING) : '');
											$experience_months = trim(isset($all_skillsets_input_row["experience_months"]) ? filter_var($all_skillsets_input_row["experience_months"], FILTER_SANITIZE_STRING) : '');
											$last_used_year = trim(isset($all_skillsets_input_row["last_used_year"]) ? filter_var($all_skillsets_input_row["last_used_year"], FILTER_SANITIZE_NUMBER_INT) : '');
											
											//Do process the record (Education Qualification), and Collect "degree" and "field of study" field values of received Education Qualification
											$received_skill_name_fields_array["skill_name"] = $all_skillsets_input_row["skill_name"];
											//$received_degree_fieldofstudy_fields_array["educational_program"] = $all_skillsets_input_row["educational_program"];
											
											//Check if all record level inputs are received, as part of "all_education_qualifications" field, and correctly from the REST Web Service
											if ($skill_name == "") {
												//Empty Degree Field
												
												$collected_content_with_result_array["skill_name"] = $skill_name;
												
												$eventLog->log("Empty Skill Name input");
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
											} else if ($version == "") {
												//Empty Field of Study Field
												
												$collected_content_with_result_array["version"] = $version;
												
												$eventLog->log("Empty Version input");
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
											} else if ($experience_years == "") {
												//three fields are Empty
												
												$collected_content_with_result_array["experience_years"] = $experience_years;
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";

											} else if ($experience_months == "") {
												//three fields are Empty
												
												$collected_content_with_result_array["experience_months"] = $experience_months;
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
												
											} else if (strlen($last_used_year) != "4") {
												//Invalid Year of Passout Input
												
												$collected_content_with_result_array["last_used_year"] = $last_used_year;
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
												$eventLog->log("Invalid Year of Passout input, as it is not of 4 characters length");
												
										
							
											} else {
												//All Education Qualification Details are valid, for this record
												
												$eventLog->log("All Education Qualification Details are valid, for this record");
												
												/* COMMENTED TO PLACE THIS OUTSIDE CONDITION //Do process the record (Education Qualification), and Collect "degree" and "field of study" field values of received Education Qualification
												$received_degree_fieldofstudy_fields_array["degree"] = $all_education_qualifications_input_row["degree"];
												$received_degree_fieldofstudy_fields_array["field_of_study"] = $all_education_qualifications_input_row["field_of_study"];
												*/
									
												//Do Duplicate Check, for each Education Qualification, in the application, w.r.t. the particular candidate's sm_memb_id
												$candidate_skill_set_check_result = candidate_skillset_details_duplicate_check($user_id_input, $skill_name, $company_id_input);
												
												if (count($candidate_skill_set_check_result) > 0) {
													//Do get the reference id
													$crs_id_from_db = $candidate_skill_set_check_result["crs_id"];
													$eventLog->log("crs_id_from_db :");
													$eventLog->log($crs_id_from_db);
													$eventLog->log("crs_id:");
													$eventLog->log($crs_id);
													
													//Check, if ceq_id, is same, from both sources (received from api & from db source, after duplicate check)
													if ($crs_id == $crs_id_from_db) {
														//ceq_id from the api request and from db source are matching, for particular education qualification record.
														$eventLog->log("ceq_id from the api request and from db source are matching, for particular education qualification record.");
														
														
														$collected_content_with_result_array["crs_id"] = $crs_id;
													
														$eventLog->log("Update Query Condition");
														
														//Do Update Query to existing record
														$candidate_skill_set_update_result = update_candidate_skillset_details_based_on_crs_id($version, $experience_years, $experience_months, $last_used_year , $ea_extracted_jwt_token_sub, $event_datetime, $current_epoch, $crs_id);
														
														if ($candidate_skill_set_update_result) {
															//Update Success
															$collected_content_with_result_array["query_type"] = "UPDATE-QUERY";
															$collected_content_with_result_array["query_status"] = "SUCCESS";
															$collected_content_with_result_array["input_record_validity_status"] = "VALID";
															
															$eventLog->log("Update Query Condition - Success");
															
														} else {
															$collected_content_with_result_array["query_type"] = "UPDATE-QUERY";
															$collected_content_with_result_array["query_status"] = "FAILURE";
															$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
											
															$eventLog->log("Update Query Condition - Failure");
															
														}//close of else of if ($candidate_education_qualification_update_result) {
														
														
													} else {
														//ceq_id from the api request and from db source are different, for particular education qualification record.
														$eventLog->log("ceq_id from the api request and from db source are different, for particular education qualification record.");
														
														$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
														
													}//close of else of if ($ceq_id == $ceq_id_from_db) {
														
													
												} else {
													
													$eventLog->log("Insert Query Condition");
													
													$eventLog->log("user_id_input: " . $user_id_input);
													$eventLog->log("company_id_input: " . $company_id_input);
													$eventLog->log("crs_id: " . $crs_id);
													$eventLog->log("skill_name: " . $skill_name);
													$eventLog->log("version: " . $version);
													$eventLog->log("experience_years: " . $experience_years);
													$eventLog->log("experience_months: " . $experience_months);
													$eventLog->log("last_used_year: " . $last_used_year);
											        $eventLog->log("sequence order: " . "1111");
													$eventLog->log("ea_extracted_jwt_token_sub: " . $ea_extracted_jwt_token_sub);
													$eventLog->log("event_datetime: " . $event_datetime);
													$eventLog->log("current_epoch: " . $current_epoch);
													
													//Find the Sequence Number, for the New Education Qualification
													$candidate_skillset_count_result = candidate_skillset_record_count($user_id_input);
													$eventLog->log("candidate_skillset_count_result");
													if (isset($candidate_skillset_count_result["current_count"])) {
														
														$candidate_current_skill_set_count = $candidate_skillset_count_result["current_count"];
														
													} else {
														$candidate_skillset_count_result = 0;
														
													}//close of else of if (isset($candidate_current_education_qualification_count_result["current_count"])) {
														
													$candidate_skill_sequence_order = $candidate_current_skill_set_count+1;
													
													$eventLog->log("before insert");
													//Do Insert Query to add a new record
													$candidate_skillset_insert_result = insert_candidate_skillset_details($user_id_input, $skill_name, $version, $experience_years, $experience_months, $last_used_year, $candidate_skill_sequence_order, $ea_extracted_jwt_token_sub, $event_datetime, $current_epoch, $company_id_input);
													
													if (isset($candidate_skillset_insert_result["last_inserted_id"])) {
														//Insert Success
														$collected_content_with_result_array["crs_id"] = $candidate_skillset_insert_result["last_inserted_id"];
														$collected_content_with_result_array["query_type"] = "INSERT-QUERY";
														$collected_content_with_result_array["query_status"] = "SUCCESS";
														$collected_content_with_result_array["input_record_validity_status"] = "VALID";
														
														$eventLog->log("Insert Query Condition - Success");
														//$additional_document_ref_input = $degree . " (" . $field_of_study . ")";
														$last_inserted_candidate_skillset_id = $candidate_skillset_insert_result["last_inserted_id"];
														//candidate_education_qualification_rel_file_upload_references_insert($user_id_input,"education-qualifications",$last_inserted_candidate_education_qualification_id,$additional_document_ref_input,null,null,$ea_extracted_jwt_token_user_type,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
														
														//COMMENTED< PLEASE CHANGE THIS FUNCTION candidate_vrpi_rel_file_upload_references_insert($user_id_input,"visa-rel-personal-info",$last_inserted_candidate_visa_rel_personal_info_id,$additional_document_ref,null,null,$ea_extracted_jwt_token_user_type,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
														
													} else {
														//Insert Failure
														$collected_content_with_result_array["crs_id"] = null;
														$collected_content_with_result_array["query_type"] = "INSERT-QUERY";
														$collected_content_with_result_array["query_status"] = "FAILURE";
														$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
										
														$eventLog->log("Insert Query Condition - Failure");
														
													}//close of else of if (isset($candidate_education_qualification_insert_result["last_inserted_id"])) {
														
													
												}//close of else of if (count($candidate_education_qualification_check_result) > 0) {
												
												$collected_content_with_result_array_json_encoded = json_encode($collected_content_with_result_array);
												//$eventLog->log("collected_array row next stage: " . $collected_content_with_result_array_json_encoded);	
												
												
											}//close of else of if ($degree == "") {
												
											
										} else {
											
											$eventLog->log("inside count(all_education_qualifications_input_row) == 10 else condition: ");
											
											//Expected number of fields are not sent, per education qualification record
											$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
										
										}//close of else of if (count($all_education_qualifications_input_row)) {
										
										$all_received_skill_name_fields_array[] = $received_skill_name_fields_array;
										$all_collected_content_with_result_array[] = $collected_content_with_result_array;
																								
										
									}//close of foreach ($all_education_qualifications_input as $all_education_qualifications_input_row) {
									
									$all_collected_content_with_result_array_json_encoded = json_encode($all_collected_content_with_result_array);
									$eventLog->log("collected_array final (didnot enter foreach loop): " . $all_collected_content_with_result_array_json_encoded);	
									
									
									/*//Collect All Education Qualifications List, of particular candidate, to extract degree and field of study field content
									$candidate_education_qualification_records_list_result = candidate_education_qualification_records_list($user_id_input);
									$candidate_education_qualification_records_list_result_count = count($candidate_education_qualification_records_list_result);
									
									$eventLog->log("candidate_education_qualification_records_list_result_count: " . $candidate_education_qualification_records_list_result_count);
									*/
									
									if (count($candidate_skillset_records_list_result) > 0) {
										//One or More than one Education Qualification is received from the Database.
										
										$all_received_skill_name_fields_array_json_encoded = json_encode($all_received_skill_name_fields_array);
										$eventLog->log("all_received_skill_name_fields_array_json_encoded : " . $all_received_skill_name_fields_array_json_encoded);	
										
										//Do process the record (Education Qualification), and Collect "degree" and "field of study" field values of received Education Qualification
										foreach($candidate_skillset_records_list_result as $candidate_skillset_records_list_result_row) {
											
											$db_based_crs_id_field_value = $candidate_skillset_records_list_result_row["crs_id"];
											$db_based_skill_name_field_value = $candidate_skillset_records_list_result_row["skill_name"];
											// $db_based_fieldofstudy_field_value = $candidate_education_qualification_records_list_result_row["educational_program"];
											// $db_based_additional_document_ref = $db_based_degree_field_value . " (" . $db_based_fieldofstudy_field_value . ")";
											
											// $eventLog->log("db_based_degree_field_value: ". $db_based_degree_field_value);
											// $eventLog->log("db_based_fieldofstudy_field_value: ". $db_based_fieldofstudy_field_value);
											
											$record_deletion_status = "not-matched";
											
											foreach ($all_received_skill_name_fields_array as $all_received_skill_name_fields_array_row) {
												if(count($all_received_skill_name_fields_array_row)>0){
												$all_received_row_based_skill_name_field_value = $all_received_skill_name_fields_array_row["skill_name"];
											} else {
												$all_received_row_based_skill_name_field_value = null ;
											}
												//$all_received_row_based_fieldofstudy_field_value = $all_received_degree_fieldofstudy_fields_array_row["educational_program"];
												
												$eventLog->log("all_received_row_based_skill_name_field_value: ". $all_received_row_based_skill_name_field_value);
												//$eventLog->log("all_received_row_based_fieldofstudy_field_value: ". $all_received_row_based_fieldofstudy_field_value);
											
												if (($all_received_row_based_skill_name_field_value == $db_based_skill_name_field_value)) {
													//Delete this matching Record, from the Form
													
													//$eventLog->log("Match Found with: Degree: " . $all_received_row_based_degree_field_value . " Field of Study: " . $all_received_row_based_fieldofstudy_field_value);
													
													$record_deletion_status = "matched";
													
													break;
													
												}//close of if (($all_received_row_based_degree_field_value != $db_based_degree_field_value) && ($all_received_row_based_fieldofstudy_field_value != $db_based_fieldofstudy_field_value)) {
													
											}//close of foreach ($all_received_degree_fieldofstudy_fields_array as $all_received_degree_fieldofstudy_fields_array_row) {
											
											/* -----------------if ($record_deletion_status == "not-matched") {
												
												$candidate_education_qualification_unused_file_record_delete_status_result = delete_candidate_unused_file_record_based_on_activity_ref_id_input($db_based_ceq_id_field_value,$db_based_additional_document_ref);
												
												$eventLog->log("After delete query");
												
												$values = json_encode($candidate_education_qualification_unused_file_record_delete_status_result);
												
												$eventLog->log($values);
											
												if (count($candidate_education_qualification_unused_file_record_delete_status_result)>0) {
													//Candidate specific Professional Summary record is Deleted Successfully.
													$eventLog->log("Candidate specific education qualification unused file record is Deleted Successfully.");
											
													$candidate_education_qualification_record_delete_status_result = delete_candidate_education_qualification_record_based_on_ceq_id_input($db_based_ceq_id_field_value);
													
													if($candidate_education_qualification_record_delete_status_result) {
																$eventLog->log("Candidate specific Educational Qualification record is Deleted Successfully.");
													}	
												
												} else {
													//Error Deleting the Candidate specific Professional Summary record, that is not received in the API Request.
													$eventLog->log("Error Deleting the Candidate specific Educational Qualification record, that is not received in the API Request.");
													
												}//close of else of if ($candidate_professional_summary_record_delete_status_result) {
												
												
											}//close of if ($record_deletion_status == "not-matched") { ---------------*/
												
											
										}//close of foreach($candidate_education_qualification_records_list_result as $candidate_education_qualification_records_list_result_row) {
										
										
									} else {
										//Education Qualifications of the Candidate are not available in the Database, this means, Nothing to Remove.
										$eventLog->log("Education Qualifications of the Candidate are not available in the Database, this means, Nothing to Remove.");
										
									}//close of else of if (count($candidate_education_qualification_records_list_result) > 0) {
										
									
									$all_collected_content_with_result_array_json_encoded = json_encode($all_collected_content_with_result_array);
									$eventLog->log("all_collected_content_with_result_array_json_encoded: " . $all_collected_content_with_result_array_json_encoded);
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = $all_collected_content_with_result_array;
									$response['status'] = "candidate-skill-set-data-ingestion-result";
									$response['status_description'] = "All Candidate submitted Skill Set Data is processed.";
									
									$eventLog->log("candidate-skill-set-data-ingestion-result: All Candidate submitted Skill Set Data is processed.");	
									
								} else {
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "no-records-received";
									$response['status_description'] = "Please Submit at least One Skill.";
									
									$eventLog->log("no-records-received: Please Submit at least One Skill.");	
									
								}//close of else of if ((is_array($all_education_qualifications_input)) && ((count($all_education_qualifications_input) > 0) || ($candidate_education_qualification_records_list_result_count > 0))) {
								
								
							} catch (Exception $e) {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "candidate-skill-set-processing-error";
								$response['status_description'] = "Error occurred when processing candidate's Skill Set.";
								
								$eventLog->log("candidate-skill-set-processing-error: Error occurred when processing candidate's Skill Set.");	
								
							}
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-subject-platform-scope-user";
							$response['status_description'] = "Insufficient Set of Permissions, to complete this action";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-subject-platform-scope-user: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-different-company-user-id";
							$response['status_description'] = "Insufficient Set of Permissions, as the User belongs to a different company";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-INSUFFICIENT-PERMISSIONS") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions, as this process will not be allowed any further";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-self-action";
							$response['status_description'] = "Insufficient Set of Permissions, as self action is Dis-allowed";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-disallowed-self-action: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-ACTION-SUBJECT-IS-A-DIFFERENT-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-action-on-different-user";
							$response['status_description'] = "Insufficient Set of Permissions, as action subject is a different user, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-disallowed-action-on-different-user: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						}//close of if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
						
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "Invalid User Account that is attempted to be edited";
						
						$eventLog->log("invalid-user-account: The User Account, that is attempted to be edited, is invalid, please check and try again.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
						
					
				}//close of else of if ($user_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token";
			
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");	
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input";
	
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));	
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){


function candidate_skillset_details_duplicate_check ($user_id_input, $skill_name, $company_id_input) {
	
	global $dbcon;
	
	$constructed_array = array();
	
	$candidate_skill_name_input = '%'.$skill_name.'%';
	
	$candidate_skill_set_check_sql = "SELECT * FROM `candidate_rel_skillsets` WHERE `sm_memb_id`=:sm_memb_id AND `skill_name` LIKE :skill_name AND `company_id`=:company_id";
	$candidate_skill_set_check_select_query = $dbcon->prepare($candidate_skill_set_check_sql);
	$candidate_skill_set_check_select_query->bindValue(":sm_memb_id",$user_id_input);
    $candidate_skill_set_check_select_query->bindValue(":company_id",$company_id_input);		
	$candidate_skill_set_check_select_query->bindValue(":skill_name",$candidate_skill_name_input);	
	$candidate_skill_set_check_select_query->execute(); 
	
	if($candidate_skill_set_check_select_query->rowCount() > 0) {
		$candidate_skill_set_check_select_query_result = $candidate_skill_set_check_select_query->fetch();
	    return $candidate_skill_set_check_select_query_result;
	
	}//close of if($candidate_education_qualification_check_select_query->rowCount() > 0) {
	return $constructed_array;
	
}


function update_candidate_skillset_details_based_on_crs_id($version, $experience_years, $experience_months, $last_used_year , $ea_extracted_jwt_token_sub, $event_datetime, $current_epoch, $crs_id) {
	global $dbcon;
	
	$candidate_skillset_details_update_sql = "UPDATE `candidate_rel_skillsets` SET `version`=:version,`experience_years`=:experience_years,`experience_months`=:experience_months,`last_used_year`=:last_used_year ,`last_updated_by_user_id`=:last_updated_by_user_id,`last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch WHERE `crs_id`=:crs_id";	
	$candidate_skillset_details_update_query = $dbcon->prepare($candidate_skillset_details_update_sql);		
	$candidate_skillset_details_update_query->bindValue(":version",$version);
	$candidate_skillset_details_update_query->bindValue(":experience_years",$experience_years);
	$candidate_skillset_details_update_query->bindValue(":experience_months",$experience_months);
	$candidate_skillset_details_update_query->bindValue(":last_used_year",$last_used_year);
	$candidate_skillset_details_update_query->bindValue(":last_updated_by_user_id",$ea_extracted_jwt_token_sub);
	$candidate_skillset_details_update_query->bindValue(":last_updated_date_time",$event_datetime);
	$candidate_skillset_details_update_query->bindValue(":last_updated_date_time_epoch",$current_epoch);
	$candidate_skillset_details_update_query->bindValue(":crs_id",$crs_id);
	
	if ($candidate_skillset_details_update_query->execute()) {
		return true;
		
	}//close of if ($candidate_education_qualification_details_update_query->execute()) {
	
	return false;
}



function insert_candidate_skillset_details($user_id_input, $skill_name, $version, $experience_years, $experience_months, $last_used_year, $candidate_skill_sequence_order, $ea_extracted_jwt_token_sub, $event_datetime, $current_epoch, $company_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	
	//Do Insert Query

	$candidate_skillset_insert_sql = "INSERT INTO `candidate_rel_skillsets`(`sm_memb_id`, `company_id`, `skill_name`, `version`, `last_used_year`, `experience_years`, `experience_months`,`sequence_order`, `added_by_user_id`, `added_date_time`, `added_date_time_epoch`) VALUES (:sm_memb_id,:company_id,:skill_name,:version,:last_used_year,:experience_years,:experience_months,:sequence_order,:added_by_user_id,:added_date_time,:added_date_time_epoch)";
	
	
	$candidate_skillset_insert_query = $dbcon->prepare($candidate_skillset_insert_sql);
	$candidate_skillset_insert_query->bindValue(":sm_memb_id",$user_id_input);
	$candidate_skillset_insert_query->bindValue(":company_id",$company_id_input);
	$candidate_skillset_insert_query->bindValue(":skill_name",$skill_name);
	$candidate_skillset_insert_query->bindValue(":version",$version);
	$candidate_skillset_insert_query->bindValue(":last_used_year",$last_used_year);
	$candidate_skillset_insert_query->bindValue(":experience_years",$experience_years);
	$candidate_skillset_insert_query->bindValue(":experience_months",$experience_months);
	
	$candidate_skillset_insert_query->bindValue(":sequence_order",$candidate_skill_sequence_order);
	/* $candidate_skillset_insert_query->bindValue(":added_by_user_type",$ea_extracted_jwt_token_user_type) */
	$candidate_skillset_insert_query->bindValue(":added_by_user_id",$ea_extracted_jwt_token_sub);
	$candidate_skillset_insert_query->bindValue(":added_date_time",$event_datetime);
	$candidate_skillset_insert_query->bindValue(":added_date_time_epoch",$current_epoch);
	
	if ($candidate_skillset_insert_query->execute()) {
		
		$last_inserted_id = $dbcon->lastInsertId();			
		//$eventLog->log("record inserted successfully");
		
		$constructed_array["last_inserted_id"] = $last_inserted_id;
		
		return $constructed_array;
					
	} else {
		//$eventLog->log("Error occurred during process. Please try again");						
			return $constructed_array;						
	}

}

function candidate_skillset_record_count($user_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	
	$candidate_skillset_record_count_sql = "SELECT COUNT(*) AS count FROM `candidate_rel_skillsets` WHERE `sm_memb_id`=:sm_memb_id";
	$candidate_skillset_record_count_select_query = $dbcon->prepare($candidate_skillset_record_count_sql);
	$candidate_skillset_record_count_select_query->bindValue(":sm_memb_id",$user_id_input);			
	$candidate_skillset_record_count_select_query->execute(); 
	
	if($candidate_skillset_record_count_select_query->rowCount() > 0) {
		$candidate_skillset_record_count_select_query_result = $candidate_skillset_record_count_select_query->fetch();
		$current_count = $candidate_skillset_record_count_select_query_result["count"];
		
		$constructed_array["current_count"] = $current_count;
		
	    return $constructed_array;
	
	} else {
		$constructed_array["current_count"] = 0;
		
		return $constructed_array;
		
	}//close of if($candidate_education_qualification_record_count_select_query->rowCount() > 0) {
	
	
}


function candidate_skillset_records_list($user_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	
	$candidate_skillset_records_list_sql = "SELECT * FROM `candidate_rel_skillsets` WHERE `sm_memb_id`=:sm_memb_id";
	$candidate_skillset_records_list_select_query = $dbcon->prepare($candidate_skillset_records_list_sql);
	$candidate_skillset_records_list_select_query->bindValue(":sm_memb_id",$user_id_input);			
	$candidate_skillset_records_list_select_query->execute(); 
	
	if($candidate_skillset_records_list_select_query->rowCount() > 0) {
		$candidate_skillset_records_list_select_query_result = $candidate_skillset_records_list_select_query->fetchAll();
		return $candidate_skillset_records_list_select_query_result;
	
	}//close of if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
	return $constructed_array;
}

function delete_candidate_education_qualification_record($user_id_input, $candidate_degree_input, $candidate_field_of_study_input) {
	global $dbcon;
	
	$candidate_degree_input = '%'.$candidate_degree_input.'%';
	$candidate_field_of_study_input = '%'.$candidate_field_of_study_input.'%';
	
	$candidate_education_qualification_delete_sql = "DELETE FROM `candidate_education_qualifications` WHERE `sm_memb_id`=:sm_memb_id AND `degree` LIKE :degree AND `field_of_study` LIKE :field_of_study";
	$candidate_education_qualification_delete_query = $dbcon->prepare($candidate_education_qualification_delete_sql);
	$candidate_education_qualification_delete_query->bindValue(":sm_memb_id",$user_id_input);	
	$candidate_education_qualification_delete_query->bindValue(":degree",$candidate_degree_input);	
	$candidate_education_qualification_delete_query->bindValue(":field_of_study",$candidate_field_of_study_input);		
	$candidate_education_qualification_delete_query->execute(); 
	
	if ($candidate_education_qualification_delete_query->execute()) {
		return true;
		
	}//close of if ($candidate_education_qualification_delete_query->execute()) {
	
	return false;
	
}

function delete_candidate_education_qualification_record_based_on_ceq_id_input($ceq_id_input) {
	global $dbcon;
	
	$candidate_educational_qualification_record_delete_sql = "DELETE FROM `candidate_education_qualifications` WHERE `ceq_id`=:ceq_id";
	$candidate_educational_qualification_record_delete_query = $dbcon->prepare($candidate_educational_qualification_record_delete_sql);
	$candidate_educational_qualification_record_delete_query->bindValue(":ceq_id",$ceq_id_input);		
	if ($candidate_educational_qualification_record_delete_query->execute()) {
		return true;
		
	}//close of if ($candidate_professional_summary_record_delete_query->execute()) {
	
	return false;
	
}

exit;
?>