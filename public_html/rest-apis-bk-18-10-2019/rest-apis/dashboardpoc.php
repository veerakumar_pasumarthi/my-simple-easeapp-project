<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

//if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "0")) {

	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				$dummy_data = array();
				$temp = array();
				$sample = array();
				$count = 0;
				
										
				/* for ($x = 1; $x <= 500; $x++) {
					
						$dummy_data[] = $x;
		
				}
					 foreach($dummy_data as $value){ 
							  $temp = array();
							  $temp["person number is"] = $value;
							  $temp["person name is "] = "Person". $value;
							  $temp["person age is "] = 2 * $value;  
								
							  $sample["list"][] = $temp;
							  $count++;
						} 
				
				$sample["count"] = $count; */
				
				
					 //foreach($dummy_data as $value){ 
							  $temp = array();
							  $temp["Name"] = "Vinay";
							  $temp["Rate"] = "205";
							  $sample["list"][] = $temp;
							  $count++;
							  $temp = array();
							  $temp["Name"] = "Leela";
							  $temp["Rate"] = "417";
							  $sample["list"][] = $temp;
							  $count++;
							  $temp = array();
							  $temp["Name"] = "Sudheer";
							  $temp["Rate"] = "101";
							  $sample["list"][] = $temp;
							  $count++;
							  $temp = array();
							  $temp["Name"] = "Krishna";
							  $temp["Rate"] = "612";
							  $sample["list"][] = $temp;
							  $count++;
							  $temp = array();
							  $temp["Name"] = "Sravani";
							  $temp["Rate"] = "935";
							  $sample["list"][] = $temp;
							  $count++;
							  $temp = array();
							  $temp["Name"] = "Prasanth";
							  $temp["Rate"] = "345";
							  $sample["list"][] = $temp;
							  $count++;
						//} 
				
				$sample["count"] = $count;
		
	

					
					if (count($sample) > 0) {
						
						$response['data'] = $sample;
						$response['status'] = "dashboard-dummy-data-successfully-fetched";
						$response['status_description'] = "Dashboard Dummy data Successfully Received";
					} 			
			    
			    
				
                
					
			}
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
/* } else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {

 */


//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
	


function  job_rel_screening_levels_list_info($job_id_input) {
global $dbcon;
	$constructed_array = array();
	
	$job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_info` WHERE `job_id` =:job_id ";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":job_id",$job_id_input);		
	$job_rel_screening_levels_list_info_query->execute(); 
	
	if($job_rel_screening_levels_list_info_query->rowCount() > 0) {
		$job_rel_screening_levels_list_info_query_result = $job_rel_screening_levels_list_info_query->fetchAll();
	     return $job_rel_screening_levels_list_info_query_result;
	
	}
	return $constructed_array;
}
exit;
?>