<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Edit Quick Admin Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "11")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
				 	if (isset($ea_received_rest_ws_raw_array_input['sm_firstname'])) {
						$content .= $ea_received_rest_ws_raw_array_input['sm_firstname'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) 

					if (isset($ea_received_rest_ws_raw_array_input['sm_lastname'])) {
						$content .= $ea_received_rest_ws_raw_array_input['sm_lastname'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) 
						if (isset($ea_received_rest_ws_raw_array_input['sm_email'])) {
						$content .= $ea_received_rest_ws_raw_array_input['sm_email'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) 
						if (isset($ea_received_rest_ws_raw_array_input['sm_mobile'])) {
						$content .= $ea_received_rest_ws_raw_array_input['sm_mobile'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) 
						 
						if (isset($ea_received_rest_ws_raw_array_input['visa_status'])) {
						$content .= $ea_received_rest_ws_raw_array_input['visa_status'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) 
						if (isset($ea_received_rest_ws_raw_array_input['visa_sponsorship_requirement_status'])) {
						$content .= $ea_received_rest_ws_raw_array_input['visa_sponsorship_requirement_status'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) 
					if (isset($ea_received_rest_ws_raw_array_input['original_filename'])) {
						$content .= $ea_received_rest_ws_raw_array_input['original_filename'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) 

					if (isset($ea_received_rest_ws_raw_array_input['file_content'])) {
						//$content .= $ea_received_rest_ws_raw_array_input['file_content'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['file_content']))

						if (isset($ea_received_rest_ws_raw_array_input['api_key'])) {
						$content .= $ea_received_rest_ws_raw_array_input['api_key'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) 
						if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) 
						
						
						
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$sm_firstname_input = trim(isset($ea_received_rest_ws_raw_array_input['sm_firstname']) ? filter_var($ea_received_rest_ws_raw_array_input['sm_firstname'], FILTER_SANITIZE_STRING) : '');
				$sm_lastname_input = trim(isset($ea_received_rest_ws_raw_array_input['sm_lastname']) ? filter_var($ea_received_rest_ws_raw_array_input['sm_lastname'], FILTER_SANITIZE_STRING) : '');
				$sm_email_input = trim(isset($ea_received_rest_ws_raw_array_input['sm_email']) ? filter_var($ea_received_rest_ws_raw_array_input['sm_email'], FILTER_SANITIZE_EMAIL) : '');
				$sm_mobile_input = trim(isset($ea_received_rest_ws_raw_array_input['sm_mobile']) ? filter_var($ea_received_rest_ws_raw_array_input['sm_mobile'], FILTER_SANITIZE_STRING) : '');
				
				$visa_status_input = trim(isset($ea_received_rest_ws_raw_array_input['visa_status']) ? filter_var($ea_received_rest_ws_raw_array_input['visa_status'], FILTER_SANITIZE_STRING) : '');
				$visa_sponsorship_requirement_status_input = trim(isset($ea_received_rest_ws_raw_array_input['visa_sponsorship_requirement_status']) ? filter_var($ea_received_rest_ws_raw_array_input['visa_sponsorship_requirement_status'], FILTER_SANITIZE_STRING) : '');
				
				$original_filename_input = trim(isset($ea_received_rest_ws_raw_array_input['original_filename']) ? filter_var($ea_received_rest_ws_raw_array_input['original_filename'], FILTER_SANITIZE_STRING) : '');
				$file_content_input = trim(isset($ea_received_rest_ws_raw_array_input['file_content']) ? filter_var($ea_received_rest_ws_raw_array_input['file_content'], FILTER_SANITIZE_STRING) : '');
				

				
				
				
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				
				if ($company_id_input == "") {
					//Invalid Mobile Number scenario
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "invalid company  id, please check and try again.";
					
					$eventLog->log("Please provide a valid company id.");
					
				
				
				} else if ($file_content_input == "") {
					//Empty File Content scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "empty-file-content-received";
					$response['status_description'] = "Empty File Content Received";
					
					$eventLog->log("empty-file-content-received: Empty File Content Received, Please check and try again.");
					
				} else if ($sm_lastname_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-lastname";
					$response['status_description'] = "invalid lastname, please check and try again.";
					
					
					$eventLog->log("Please provide all information.");	
				
				} else if ($sm_firstname_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-firstname";
					$response['status_description'] = "invalid firstname, please check and try again.";
					
					
					$eventLog->log("Please provide all information.");	
				
				}else if ($sm_email_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-email";
					$response['status_description'] = "invalid email, please check and try again.";
					
					
					$eventLog->log("Please provide all information.");	
				
				}else if ($sm_mobile_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-mobile";
					$response['status_description'] = "invalid mobile, please check and try again.";
					
					
					$eventLog->log("Please provide all information.");	
				
				} else if ($visa_status_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-visa-status";
					$response['status_description'] = "invalid visa status, please check and try again.";
					
					
					$eventLog->log("Please provide all information.");	
				
				} else if ($visa_sponsorship_requirement_status_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-visa-sponsorship";
					$response['status_description'] = "invalid visa sponsorship, please check and try again.";
					
					
					$eventLog->log("Please provide all information.");	
				
				} else if ($original_filename_input == "") {
					//Invalid File Upload Reference ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-file-ref-original-filename";
					$response['status_description'] = "Missing File related Original File Name";
					
					$eventLog->log("missing-file-ref-original-filename: Missing File related Original File Name, please check and try again.");
					
				}  else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "invalid company  id, please check and try again.";
					
					
					$eventLog->log("Please provide all information.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//Event Time, as per Indian Standard Time
					$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
		
					
					try {
						 $company_client_basic_details_result = company_basic_details_check_based_on_company_client_id($company_id_input,$api_key_input);
								   
					   if(count($company_client_basic_details_result) > 0){
									   
					$company_client_basic_details_get = company_basic_details_check_based_on_sm_memb_id($sm_email_input);
					  if(count($company_client_basic_details_get)>0) {		   
                        if($sm_user_type_input == "admin") {
                        	$response['data'] = array();
							$response['status'] = "useris-rejected";
							$response['status_description'] = "user is rejected.";
										
							$eventLog->log("useer is rejected.");
                        } else  { 
                               $company_client_basic_details_get = company_basic_details_check_based_on_sm_memb_id($sm_email_input);
                            $response['data'] = $company_client_basic_details_get;
							$response['status'] = "email-is-already-there";
							$response['status_description'] = "email is already there.";

                        } else {
                             
                             $job_details_get=job_details_sm_memb_id($company_id_input);

                        	$company_client_id_get=company_client_details_sm_memb_id($company_id_input);

                        	$insert_into_sm_memb_table= insert_into_sm_table($company_id_input,$sm_firstname_input,$sm_lastname_input,$sm_email_input,$sm_mobile_input);

                        	$insert_into_assosciate_table=insert_into_assosciate_table($sm_memb_id_input);

                             $event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");

			//data:image/png;base64,iVBORw0KGgoAAAANSUhEUgA
		
			//Extract the received file content, and mime type parts
            $event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
			$file_content_input_semi_colon_base64_comma_exploded = explode(";base64,",$file_content_input);
									
			if (count($file_content_input_semi_colon_base64_comma_exploded) == "2") {
				
				$uploaded_file_extension_rel_mime_type_and_data = $file_content_input_semi_colon_base64_comma_exploded[0];
				$uploaded_file_base64_encoded_data = $file_content_input_semi_colon_base64_comma_exploded[1];
				
				//Extract mime type from the mime type parts
				$uploaded_file_extension_rel_mime_type_and_data_exploded = explode(":",$uploaded_file_extension_rel_mime_type_and_data);
				
				if (count($uploaded_file_extension_rel_mime_type_and_data_exploded) == "2") {
					$uploaded_file_extension_rel_mime_type_and_data_first_part = $uploaded_file_extension_rel_mime_type_and_data_exploded[0];
					$uploaded_file_mime_type = $uploaded_file_extension_rel_mime_type_and_data_exploded[1];
					
					$eventLog->log("uploaded_file_mime_type: " . $uploaded_file_mime_type);	
					
					$random_text_generation_inputs = uniqid(rand(), TRUE);
					$generated_file_name_without_file_extension = hash('sha256', $random_text_generation_inputs);
					$candidate_document_upload_folder = "./uploaded-documents/";
					
					//https://gist.github.com/raphael-riel/1253986
					$mime_types = array(
								'pdf'     => 'application/pdf',
								'doc'     => 'application/msword'
							);
							
					
						 //function for compare the mimetypes of what we allow the mime_types and upload from the user mime_type
					$extracted_file_extension_result = extract_file_extension_based_on_uploaded_file_mime_type_input($mime_types, $uploaded_file_mime_type);
					
					$eventLog->log("extracted_file_extension_result: " . $extracted_file_extension_result);
					
					if ($extracted_file_extension_result != "") {
						
						//Generated File Name
						$generated_file_name = $generated_file_name_without_file_extension . "." . $extracted_file_extension_result; 
						
						
						
						$crauvd_id_full_folder_absolute_path = $site_home_path . "uploaded-documents/";
						$eventLog->log("Full Upload Folder (with absolute path): " . $crauvd_id_full_folder_absolute_path);
						
						if(!is_dir($crauvd_id_full_folder_absolute_path)) {
						
							mkdir($crauvd_id_full_folder_absolute_path, 0755, true);
							chmod($crauvd_id_full_folder_absolute_path, 0755);
							clearstatcache();	
							
							$eventLog->log("(Absolute Path - Not a Directory Structure) Condition");
							
						} else if(!is_writable($crauvd_id_full_folder_absolute_path)) {
						
							chmod($crauvd_id_full_folder_absolute_path, 0755);
							clearstatcache();
							
							$eventLog->log("(Absolute Path - Directory Structure Not Writable) Condition");
							
						}//close of else if of if(!is_dir($site_home_path . "uploaded-documents/" . $user_id_input . "/" . $crauvd_id_folder_name)) {
						
						
						//Create Full Upload Folder related Relative Path
						$crauvd_id_full_folder_relative_path = $candidate_document_upload_main_folder . "/"; // $candidate_document_upload_main_folder from /app/core/main-config.php
						
						$eventLog->log("Full Upload Folder (with relative path): " . $crauvd_id_full_folder_relative_path);
						
						//Stored File Location
						$targetFile =  $crauvd_id_full_folder_relative_path. $generated_file_name;
						
						
						/*
						$targetFile =  $candidate_document_upload_main_folder. $generated_file_name;
						*/
						$eventLog->log("targetFile (with path): " . $targetFile);
						
						
						//Base64 Decode the received file input
						$uploaded_file_base64_decoded_data = base64_decode($uploaded_file_base64_encoded_data);
						
						//Store the Content in the FILEINFO_MIME
						file_put_contents($targetFile, $uploaded_file_base64_decoded_data);
						
						$eventLog->log("The Uploaded file is saved now");
						
						//Create Full Upload Folder related Absolute Path
						$targetFile_absolute_path = $candidate_document_upload_main_folder_rel_absolute_path . "/" . $generated_file_name; // $candidate_document_upload_main_folder from /app/core/hostname-check.php
						
						
						$eventLog->log("Full Upload Folder (with relative path): " . $crauvd_id_full_folder_relative_path);
						
						
						//Check Received File Mimetype
						$finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
						//$file_mime_type = finfo_file($finfo, $targetFile) . "\n";
						$file_mime_type = finfo_file($finfo, $targetFile);
						finfo_close($finfo);
						
						$eventLog->log("enumerated file_mime_type: " . $file_mime_type);
						
						//Check, if user submitted file mime type = enumerated file mime type
						if ($uploaded_file_mime_type == $file_mime_type) {
							//Do Update Query
							//$user_file_upload_ref_id_input
							//$file_rel_original_filename_input
							
							$file_size_bytes_input = filesize($targetFile);
							$file_size_kilo_bytes_input = $file_size_bytes_input/1024;
							$file_size_mega_bytes_input = $file_size_bytes_input/(1024*1024);
							
							$eventLog->log("bytes: " . $file_size_bytes_input . " kilo bytes: " . $file_size_kilo_bytes_input . " mega bytes: " . $file_size_mega_bytes_input);
							
							//Check the File Size of the Uploaded File
							if ($file_size_mega_bytes_input > 25) {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "larger-file-uploaded";
								$response['status_description'] = "The File Size of the Uploaded File is > 25MB. Please uploaded the File, that is Smaller in Size.";
								
								$eventLog->log("The File Size of the Uploaded File is > 25MB");
								
							} else {
								$eventLog->log("Done");
								
								//File Size is <= 25MB
								
								if ($job_id_input == "") {
									$job_id_input = null;
								}
							$candidate_rel_document_file_info_insert_result = candidate_rel_additional_documents_insert($crur_id_input,$company_id_input,$sm_memb_id_input,$job_id_input, $document_extension_input,$extracted_file_extension_result, $file_size_bytes_input, $file_size_kilo_bytes_input, $file_size_mega_bytes_input, $original_filename_input, $generated_file_name, $sm_memb_id_input, $event_datetime, $current_epoch);

								$eventLog->log("file_info_update var_dump: " . $candidate_rel_document_file_info_insert_result);
								
								if ($candidate_rel_document_file_info_insert_result) {
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "file-upload-request-successful";
									$response['status_description'] = "The File is Uploaded Successfully, w.r.t. particular User's document reference.";
									
									$eventLog->log("The File is Uploaded Successfully, w.r.t. particular User's document reference.");	
									
								} else {
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "file-upload-request-error";
									$response['status_description'] = "There occurred an error when uploading the file, w.r.t. particular User's document reference, please check and try again.";
									
									$eventLog->log("There occurred an error when uploading the file, w.r.t. particular User's document reference, please check and try again.");	
									
								}//close of else of if ($candidate_rel_document_file_info_update_result) {
								
							}//close of else of if ($file_size_mega_bytes_input > 25) {
							
							
						} else {
							//File Mime Type is mis-matching
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "file-upload-mime-type-mismatching";
							$response['status_description'] = "File Mime Type is mis-matching, w.r.t. particular User's document reference";
							
							$eventLog->log("file-upload-mime-type-mismatching: File Mime Type is mis-matching, w.r.t. particular User's document reference, Please check and try again.");
							
						}//close of if ($uploaded_file_mime_type == $file_mime_type) {

						
						
						
					} else {
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "file-upload-mime-type-not-accepted";
						$response['status_description'] = "The Uploaded File's Mime Type is not in the accepted list of file types";
						
						$eventLog->log("file-upload-mime-type-not-accepted: The Uploaded File's Mime Type is not in the accepted list of file types, please check and try again.");	
					}//close of else of if ($extracted_file_extension_result != "") {
						
					
					
				} else {
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-file-extension-input";
					$response['status_description'] = "Invalid File Extension Input";
					
					$eventLog->log("invalid-file-extension-input: Invalid info w.r.t. file extension part. Invalid File Extension Input, Please Check and try again.");	
				}//close of else of if (count($uploaded_file_extension_rel_mime_type_and_data_exploded) == "2") {
				
				
				
			} else {
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "invalid-base64-encoded-file-input";
				$response['status_description'] = "Invalid Base64 Encoded File Input";
				
				$eventLog->log("invalid-base64-encoded-file-input: Invalid base64 encoded file content");	
			}

                        	


                        }
                        $get_crur_id = get_crur_id_from_upload($sm_memb_id_input);
                        $insert_job_details_table = job_applicant_request_information_insert($crur_id_input,$company_id_input,$company_client_id_input,$job_id_input,$job_posted_by_user_id_input,$job_applied_by_user_id_input,$job_specific_resume_status_input,$crur_id_input,$visa_status_input,$visa_sponsorship_requirement_status_input,$event_datetime,$current_epoch,$ea_extracted_jwt_token_sub,$is_active_status);

                        $response['data'] = array();
						$response['status'] = "candidate-info-inserted-successfully";
						$response['status_description'] = "candidate info inserted successfully.";
										
						$eventLog->log("candidate info inserted successfully.");											     
							} else {
									
							$response['data'] = array();
							$response['status'] = "candidate-info-insertion-failure";
							$response['status_description'] = "candidate info insertion failure.";
										
						$eventLog->log("candidate info insertion failure.");
													
									}			
						} catch (Exception $e) {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "candidate-info-insertion-error";
						$response['status_description'] = "There is an error, when inserting candidate info.";
						
						$eventLog->log("There is an error, when inserting candidate info.");	
									
								}
					   
						
					
					
						
				}//close of else of if ($email_id_input == "") {
				
			}
			
		//}	
			
	} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	}
	
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	

	
function company_basic_details_check_based_on_company_client_id($company_id_input,$api_key_input) {

	global $dbcon;
	$constructed_array = array();
	$is_active_status = "1";
	
	$candidate_preferred_job_location_record_duplicate_check_sql = "SELECT * FROM `company_rel_api_keys` WHERE `company_id` = :company_id AND `api_key` = :api_key AND `is_active_status` = :is_active_status";

	$candidate_preferred_job_location_record_duplicate_check_q = $dbcon->prepare($candidate_preferred_job_location_record_duplicate_check_sql);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":company_id",$company_id_input);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":api_key",$api_key_input);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":is_active_status",$is_active_status);

    
	$candidate_preferred_job_location_record_duplicate_check_q->execute();
  
	if($candidate_preferred_job_location_record_duplicate_check_q->rowCount() > 0) {
		$candidate_preferred_job_location_record_duplicate_check_q_result = $candidate_preferred_job_location_record_duplicate_check_q->fetch();
		
		/* foreach ($candidate_preferred_job_location_record_duplicate_check_q_result as $candidate_preferred_job_location_record_duplicate_check_q_result_row) {

			$temp_array=array();
			$temp_array["is_shown_in_panel"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["is_shown_in_panel"];
			$is_shown_in_panel_input=$temp_array["is_shown_in_panel"];
			
			$constructed_array=$temp_array;
		} */

	     return $candidate_preferred_job_location_record_duplicate_check_q_result;
	    

	} //close of if($candidate_preferred_location_record_duplicate_check_q_result->rowCount() > 0) {
	return $constructed_array;


}
function  company_basic_details_check_based_on_sm_memb_id($sm_email_input) {

	global $dbcon;
	$constructed_array = array();
	//$is_active_status = "1";
	
	$candidate_preferred_job_location_record_get_sql = "SELECT * FROM `site_members` WHERE `sm_email` = :sm_email";

	$candidate_preferred_job_location_record_duplicate_check_q = $dbcon->prepare($candidate_preferred_job_location_record_get_sql);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":sm_email",$sm_email_input);

	$candidate_preferred_job_location_record_duplicate_check_q->execute();
  
	if($candidate_preferred_job_location_record_duplicate_check_q->rowCount() > 0) {
		$candidate_preferred_job_location_record_duplicate_check_q_result = $candidate_preferred_job_location_record_duplicate_check_q->fetch();
		
		 foreach ($candidate_preferred_job_location_record_duplicate_check_q_result as $candidate_preferred_job_location_record_duplicate_check_q_result_row) {

			$temp_array=array();
			$temp_array["sm_memb_id"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["sm_memb_id"];
			$sm_memb_id_input=$temp_array["sm_memb_id"];
			$temp_array["sm_email"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["sm_email"];
			$temp_array["sm_user_type"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["sm_user_type"];
			
			$sm_user_type_input=$temp_array["sm_user_type"];


			
			$constructed_array=$temp_array;
		} 

	     return $constructed_array;
	    

	} //close of if($candidate_preferred_location_record_duplicate_check_q_result->rowCount() > 0) {
	return $constructed_array;


}

function  job_details_sm_memb_id($company_id_input) {

	global $dbcon;
	$constructed_array = array();
	$is_active_status = "1";
	
	$candidate_preferred_job_location_record_get_sql = "SELECT * FROM `jobs` WHERE `company_id` = :company_id AND `is_active_status` = :is_active_status";

	$candidate_preferred_job_location_record_duplicate_check_q = $dbcon->prepare($candidate_preferred_job_location_record_get_sql);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":company_id",$company_id_input);
    
    $candidate_preferred_job_location_record_duplicate_check_q->bindValue(":is_active_status",$is_active_status);

	$candidate_preferred_job_location_record_duplicate_check_q->execute();
  
	if($candidate_preferred_job_location_record_duplicate_check_q->rowCount() > 0) {
		$candidate_preferred_job_location_record_duplicate_check_q_result = $candidate_preferred_job_location_record_duplicate_check_q->fetch();
		
		 foreach ($candidate_preferred_job_location_record_duplicate_check_q_result as $candidate_preferred_job_location_record_duplicate_check_q_result_row) {

			$temp_array=array();
			$temp_array["job_id"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["job_id"];
			$job_id_input=$temp_array["job_id"];
			$temp_array["job_posted_by_sm_memb_id"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["job_posted_by_sm_memb_id"];
			
			
			$job_posted_by_sm_memb_id_input=$temp_array["job_posted_by_sm_memb_id"];
			
			$constructed_array=$temp_array;
		} 

	     return $constructed_array;
	    

	} //close of if($candidate_preferred_location_record_duplicate_check_q_result->rowCount() > 0) {
	return $constructed_array;


}


function  company_client_details_sm_memb_id($company_id_input) {

	global $dbcon;
	$constructed_array = array();
	$is_active_status = "1";
	
	$candidate_preferred_job_location_record_get_sql = "SELECT * FROM `company_clients` WHERE `company_id` = :company_id AND `is_active_status` = :is_active_status";

	$candidate_preferred_job_location_record_duplicate_check_q = $dbcon->prepare($candidate_preferred_job_location_record_get_sql);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":company_id",$company_id_input);
    
    $candidate_preferred_job_location_record_duplicate_check_q->bindValue(":is_active_status",$is_active_status);

	$candidate_preferred_job_location_record_duplicate_check_q->execute();
  
	if($candidate_preferred_job_location_record_duplicate_check_q->rowCount() > 0) {
		$candidate_preferred_job_location_record_duplicate_check_q_result = $candidate_preferred_job_location_record_duplicate_check_q->fetch();
		
		 foreach ($candidate_preferred_job_location_record_duplicate_check_q_result as $candidate_preferred_job_location_record_duplicate_check_q_result_row) {

			$temp_array=array();
			$temp_array["company_client_id"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["company_client_id"];
			$company_client_id_input=$temp_array["company_client_id"];
			
			$constructed_array=$temp_array;
		} 

	     return $constructed_array;
	    

	} //close of if($candidate_preferred_location_record_duplicate_check_q_result->rowCount() > 0) {
	return $constructed_array;


}

function insert_into_sm_table($company_id_input,$sm_firstname_input,$sm_lastname_input,$sm_email_input,$sm_mobile_input,,$event_datetime,$current_epoch)

global $dbcon;
	$sm_user_status = '1';
     $sm_user_type="member";
	 $sm_admin_level="0";
	 $sm_user_role="member";
	 $added_by_admin_user_id="null";
	$candidate_preferred_location_details_sql = "INSERT INTO `site_members`( `sm_email`,`sm_mobile`, `sm_firstname`, `sm_lastname`,`sm_user_type`,`sm_admin_level`,`sm_user_role.`added_by_admin_user_id`,`company_id`,`added_date_time`, `added_date_time_epoch`, `sm_user_status`) VALUES (:sm_email,:sm_mobile,:sm_firstname,:sm_lastname,:sm_user_type,:sm_admin_level,:sm_user_role,:added_by_admin_user_id,:company_id,:added_date_time,:added_date_time_epoch,:sm_user_status)";

	
	$candidate_preferred_location_details_q = $dbcon->prepare($candidate_preferred_location_details_sql);
	
	$candidate_preferred_location_details_q->bindValue(":sm_email",$sm_email_input);
	$candidate_preferred_location_details_q->bindValue(":sm_mobile",$sm_mobile_input);
	$candidate_preferred_location_details_q->bindValue(":sm_firstname",$sm_firstname_input);
	$candidate_preferred_location_details_q->bindValue(":sm_lastname",$sm_lastname_input);
	$candidate_preferred_location_details_q->bindValue(":sm_user_type",$sm_user_type);
	$candidate_preferred_location_details_q->bindValue(":sm_admin_level",$sm_admin_level);
	$candidate_preferred_location_details_q->bindValue(":sm_user_role",$sm_user_role);
	$candidate_preferred_location_details_q->bindValue(":added_by_admin_user_id",$added_by_admin_user_id);
	$candidate_preferred_location_details_q->bindValue(":company_id",$company_id_input);
	
    $candidate_preferred_location_details_q->bindValue(":added_date_time",$event_datetime);

	$candidate_preferred_location_details_q->bindValue(":added_date_time_epoch",$current_epoch);

	$candidate_preferred_location_details_q->bindValue(":sm_user_status",$sm_user_status);

       // $candidate_preferred_location_details_q->execute();
		if ($candidate_preferred_location_details_q->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");
			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }
} 

function insert_into_assosciate_table($sm_memb_id_input,,$event_datetime,$current_epoch) {

global $dbcon;
	
     $sm_site_member_classification_detail_id="1";
	 $valid_from_date="event_datetime";
	 $valid_to_date="present";
	 $is_active_status="1";
	$candidate_preferred_location_details_sql = "INSERT INTO `sm_site_member_classification_associations`( `sm_memb_id`,`sm_site_member_classification_detail_id`, `valid_from_date`, `valid_to_date`,`is_active_status`) VALUES (:sm_memb_id,:sm_site_member_classification_detail_id,:valid_from_date,:valid_to_date,:is_active_status)";

	
	$candidate_preferred_location_details_q = $dbcon->prepare($candidate_preferred_location_details_sql);
	
	$candidate_preferred_location_details_q->bindValue(":sm_memb_id",$sm_memb_id_input);
	$candidate_preferred_location_details_q->bindValue(":sm_site_member_classification_detail_id",$sm_site_member_classification_detail_id_input);
	$candidate_preferred_location_details_q->bindValue(":valid_from_date",$valid_from_date_input);
	$candidate_preferred_location_details_q->bindValue(":valid_to_date",$valid_to_date_input);
	$candidate_preferred_location_details_q->bindValue(":is_active_status",$is_active_status);
	

	
       // $candidate_preferred_location_details_q->execute();
		if ($candidate_preferred_location_details_q->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");
			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }
} 



function candidate_rel_additional_documents_insert($company_id_input,$sm_memb_id_input,$job_id_input, $document_extension_input,$extracted_file_extension_result, $file_size_bytes_input, $file_size_kilo_bytes_input, $file_size_mega_bytes_input, $original_filename_input, $generated_file_name,$event_datetime, $current_epoch) {	
    global $dbcon;
	$resume_upload_mode="3";
	$resume_source="3";
	$profile_source_id="11";
	$profile_source_name="through_company_website";
	$profile_source_seo_name="through_company_website";
	$$document_extension_input="pdf";
	$av_scan_status="0";
	$resume_processing_status ="null";
	$elasticsearch_content_upload_status ="0";
	$added_by_user_type="null";
	$added_by_user_id="null";
	$is_active_status="1";

	$candidate_rel_document_file_info_insert_sql = "INSERT INTO `candidate_rel_uploaded_resumes`(`company_id`,`sm_memb_id`,`resume_upload_mode`, `resume_source`,`profile_source_id`,`profile_source_name`,`profile_source_seo_name`,`job_id`,`original_filename`, `generated_filename`, `document_extension`, `file_size_bytes`, `file_size_kilo_bytes`, `file_size_mega_bytes`,`av_scan_status`,`resume_processing_status`,`elasticsearch_content_upload_status`,`added_date_time`, `added_date_time_epoch`, `is_active_status`) VALUES (:company_id,:sm_memb_id,:resume_upload_mode,:resume_source,:profile_source_id,:profile_source_name,:profile_source_seo_name,:job_id,:original_filename,:generated_filename,:document_extension,:file_size_bytes,:file_size_kilo_bytes,:file_size_mega_bytes,:av_scan_status,:resume_processing_status,:elasticsearch_content_upload_status,:added_date_time,:added_date_time_epoch,:is_active_status)";
	
	$candidate_rel_document_file_info_insert_query = $dbcon->prepare($candidate_rel_document_file_info_insert_sql);
	$candidate_rel_document_file_info_insert_query->bindValue(":company_id",$company_id_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":sm_memb_id",$sm_memb_id_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":resume_upload_mode",$resume_upload_mode);
	$candidate_rel_document_file_info_insert_query->bindValue(":resume_source",$resume_source);
	$candidate_rel_document_file_info_insert_query->bindValue(":profile_source_id",$profile_source_id);
	$candidate_rel_document_file_info_insert_query->bindValue(":profile_source_name",$profile_source_name);
	$candidate_rel_document_file_info_insert_query->bindValue(":profile_source_seo_name",$profile_source_seo_name);
	$candidate_rel_document_file_info_insert_query->bindValue(":job_id",$job_id_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":original_filename",$original_filename_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":generated_filename",$generated_filename_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":document_extension",$document_extension_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":file_size_bytes",$file_size_bytes_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":file_size_kilo_bytes",$file_size_kilo_bytes_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":file_size_mega_bytes",$file_size_mega_bytes_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":av_scan_status",$av_scan_status);
	$candidate_rel_document_file_info_insert_query->bindValue(":resume_processing_status",$resume_processing_status);
	$candidate_rel_document_file_info_insert_query->bindValue(":elasticsearch_content_upload_status",$elasticsearch_content_upload_status);
	$candidate_rel_document_file_info_insert_query->bindValue(":added_date_time",$event_datetime);
	$candidate_rel_document_file_info_insert_query->bindValue(":added_date_time_epoch",$current_epoch);
	$candidate_rel_document_file_info_insert_query->bindValue(":is_active_status",$is_active_status);
	if ($candidate_rel_document_file_info_insert_query->execute()) {
			
        $last_inserted_id = $dbcon->lastInsertId();			
			return $last_inserted_id;
						
	} else {						
			return "";						
	}
	
}

function get_crur_id_from_upload($sm_memb_id_input) {

	global $dbcon;
	$constructed_array = array();
	$is_active_status = "1";
	
	$candidate_preferred_job_location_record_get_sql = "SELECT * FROM `candidate_rel_uploaded_resumes` WHERE `sm_memb_id` = :sm_memb_id AND `is_active_status` = :is_active_status";

	$candidate_preferred_job_location_record_duplicate_check_q = $dbcon->prepare($candidate_preferred_job_location_record_get_sql);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":sm_memb_id",$sm_memb_id_input);
    
    $candidate_preferred_job_location_record_duplicate_check_q->bindValue(":is_active_status",$is_active_status);

	$candidate_preferred_job_location_record_duplicate_check_q->execute();
  
	if($candidate_preferred_job_location_record_duplicate_check_q->rowCount() > 0) {
		$candidate_preferred_job_location_record_duplicate_check_q_result = $candidate_preferred_job_location_record_duplicate_check_q->fetch();
		
		 foreach ($candidate_preferred_job_location_record_duplicate_check_q_result as $candidate_preferred_job_location_record_duplicate_check_q_result_row) {

			$temp_array=array();
			$temp_array["crur_id"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["crur_id"];
			$crur_id_input=$temp_array["crur_id"];
			
			$constructed_array=$temp_array;
		} 

	     return $constructed_array;
	    

	} //close of if($candidate_preferred_location_record_duplicate_check_q_result->rowCount() > 0) {
	return $constructed_array;


}

function job_applicant_request_information_insert($crur_id_input,$company_id_input,$company_client_id_input,$job_id_input,$job_posted_by_user_id_input,$job_applied_by_user_id_input,$job_specific_resume_status_input,$crur_id_input,$visa_status_input,$visa_sponsorship_requirement_status_input,$event_datetime,$current_epoch,$ea_extracted_jwt_token_sub,$is_active_status) {
    global $dbcon,$eventLog;
	$is_active_status = '1';
	$job_specific_resume_status_input="1";
    $job_applicantrequest_information_insert_sql ="INSERT INTO `job_applicant_requests_info`(`company_id`, `company_client_id`, `job_id`, `job_posted_by_user_id`, `job_applied_by_user_id`, `job_specific_resume_status`,`crur_id`,`visa_status`,`visa_sponsorship_requirement_status`, `event_date_time`, `event_date_time_epoch`,`is_active_status`) VALUES(:company_id,:company_client_id,:job_id,:job_posted_by_user_id,:job_applied_by_user_id,:job_specific_resume_status,:crur_id,:visa_status,:visa_sponsorship_requirement_status,:event_date_time,:event_date_time_epoch,:is_active_status)";
	
	$eventLog->log("after query");
	
	$job_applicantrequest_information_insert_query = $dbcon->prepare($job_applicantrequest_information_insert_sql);
	$job_applicantrequest_information_insert_query->bindValue(":company_id",$company_id_input);
	$job_applicantrequest_information_insert_query->bindValue(":company_client_id",$company_client_id_input);
	$job_applicantrequest_information_insert_query->bindValue(":job_id",$job_id_input);
	$job_applicantrequest_information_insert_query->bindValue(":job_posted_by_user_id",$job_posted_by_user_id_input);
	$job_applicantrequest_information_insert_query->bindValue(":job_applied_by_user_id",$job_applied_by_user_id_input);
	
	$job_applicantrequest_information_insert_query->bindValue(":job_specific_resume_status",$job_specific_resume_status_input);
	$job_applicantrequest_information_insert_query->bindValue(":crur_id",$crur_id_input);
	$job_applicantrequest_information_insert_query->bindValue(":visa_status",$visa_status_input);
	$job_applicantrequest_information_insert_query->bindValue(":visa_sponsorship_requirement_status",$visa_sponsorship_requirement_status_input);
	$job_applicantrequest_information_insert_query->bindValue(":event_date_time",$event_datetime);
	$job_applicantrequest_information_insert_query->bindValue(":event_date_time_epoch",$current_epoch);
	
	$job_applicantrequest_information_insert_query->bindValue(":is_active_status",$is_active_status);
	

		if ($job_applicantrequest_information_insert_query->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			$eventLog->log("record inserted successfully");

			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}

exit;
?>


