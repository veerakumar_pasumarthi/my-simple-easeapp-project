<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "8")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$page_number_input= trim(isset($ea_received_rest_ws_raw_array_input['page_number']) ? filter_var($ea_received_rest_ws_raw_array_input['page_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				$sm_user_status_input = trim(isset($ea_received_rest_ws_raw_array_input['sm_user_status']) ? filter_var($ea_received_rest_ws_raw_array_input['sm_user_status'], FILTER_SANITIZE_NUMBER_INT) : '');
				$number_of_records_input = trim(isset($ea_received_rest_ws_raw_array_input['number_of_records']) ? filter_var($ea_received_rest_ws_raw_array_input['number_of_records'], FILTER_SANITIZE_NUMBER_INT) : '');
				$sort_field_input= trim(isset($ea_received_rest_ws_raw_array_input['sort_field']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_field'], FILTER_SANITIZE_STRING) : '');
				$sort_order_input = trim(isset($ea_received_rest_ws_raw_array_input['sort_order']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_order'], FILTER_SANITIZE_STRING) : '');
				$search_criteria_input = trim(isset($ea_received_rest_ws_raw_array_input['search_criteria']) ? filter_var($ea_received_rest_ws_raw_array_input['search_criteria'], FILTER_SANITIZE_STRING) : '');
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {

                if (($sm_user_status_input != "0") && ($sm_user_status_input != "1") && ($sm_user_status_input != "") && ($sm_user_status_input != "2") && ($sm_user_status_input != "3")  ) {
					//Invalid company Status scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-user-status";
					$response['status_description'] = "Invalid user status.";
					
					$eventLog->log("Please provide a valid user Status info.");
				
				
				}else if (($sort_order_input != "ASC") && ($sort_order_input != "DESC") && ($sort_order_input != "")) {
					//Invalid Sorting Order scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-Sorting-order";
					$response['status_description'] = "Invalid sorting order.";
					
					$eventLog->log("invalid-Sorting-order: Invalid Sorting Order info submitted, please check and try again.");
					$eventLog->log("Please provide all information.");
				
                
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else {
					//all inputs are valid
					
					try{
						
					    if ($sm_user_status_input == "") {
							$sm_user_status_input = null;
							
						}//close of if ($sm_user_status_input == "") {
						
					       $candidate_list_info_result = array();
						   $sm_user_type_input = 'admin';
						   
					    if ($ea_extracted_jwt_token_user_company_id == "") {
						    //action taker is of platform scope - super admin / site admin scenario
						    $admin_users_list_info_result = get_admin_users_list_with_pagination_inputs($company_id_input,$sm_user_status_input, $page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input);
					
					    } else {
						   //action taker is of company scope - company admin, legal / immigration, internal admin etc scenario
						    $admin_users_list_info_result = get_admin_users_list_with_pagination_inputs($ea_extracted_jwt_token_user_company_id,$sm_user_status_input, $page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input);
					       
					    }//close of else of if ($ea_extracted_jwt_token_user_company_id == "") {
					    //$candidate_list_info = get_candidate_list($company_id_input, "member");
						$admin_users_list_info_result_count = count($admin_users_list_info_result);
						
						$eventLog->log("Count -> " . $admin_users_list_info_result_count["total_records_count"]);
						
					    if ($admin_users_list_info_result_count > "0") {
					       $response['data'] = $admin_users_list_info_result;
					       $response['status'] = "admin-users-list-received";
					       $response['status_description'] = "List successfully received";
							
					     $admin_users_list_info_json_encoded = json_encode($admin_users_list_info_result);
						 $eventLog->log("Companies List ->.Admin Users List is Successfully Received"); 	
						
					       //eventLog->log("Quick User Info -> " . $candidate_list_info_json_encoded); 
					    }else {
							
							$eventLog->log("Companies List -> No Active Admin Users Exist for your company, please check and try again."); 	
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "active-admin-users-doesnot-exist";
							$response['status_description'] = "No records found.";
							
					    }		//No Active company list Exist
							
				    }catch (Exception $e){
						$eventLog->log("Exception -> " . html_escaped_output($e->getMessage())); 
						//addLog($logFile, "Exception -> ".$e->getMessage());	
					}//close of  catch (Exception $e){	}//close of else of if ($company_list_result_count > "0") {					
                  
				}//close of else of if ($user_type_input != 'admin') {
					
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function sm_user_classification_details_get($sm_memb_id_input){
	 global $dbcon;
	$constructed_array = array();
		$companies_details_get_sql = "SELECT smcd.sm_user_role,smcd.sm_site_member_classification_detail_id FROM `sm_site_member_classification_associations` smca JOIN `sm_site_member_classification_details` smcd ON smca.sm_site_member_classification_detail_id = smcd.sm_site_member_classification_detail_id WHERE smca.sm_memb_id=:sm_memb_id AND smca.is_active_status = '1'";

		$companies_details_get_select_query = $dbcon->prepare($companies_details_get_sql);
		$companies_details_get_select_query->bindValue(":sm_memb_id",$sm_memb_id_input);
		$companies_details_get_select_query->execute();

	if($companies_details_get_select_query->rowCount() > 0) {
	    $companies_details_get_select_query_result = $companies_details_get_select_query->fetch();
	    //print_r($companies_details_get_select_query_result);

		return $companies_details_get_select_query_result;
	}
	return $constructed_array;
	

}
	
	

exit;
?>