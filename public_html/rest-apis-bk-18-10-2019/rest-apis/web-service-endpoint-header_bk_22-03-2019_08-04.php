<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * REST API Service, and Common header for both pre-login and post-login scenarios.
 *
 */
 
$ea_auth_token_validation_status = false;
$ea_auth_token = "";
$ea_maintenance_mode = false; 
$ea_is_user_page_access_authorized = false;
$response = array();

//Get Maintenance State Details of REST Services
$ea_maintanance_mode_time = getMaintananceInfo();

//Check maintenance mode
if ($ea_maintanance_mode_time != "") {
	$ea_maintenance_mode = true;	
	//echo "Maintenance mode (true): " . $ea_maintenance_mode . "\n";
}//close of if ($ea_maintanance_mode_time != "") {

//Process received Raw PHP Input, at the REST Web Service Endpoint. $_GET / $_POST Superglobals do not work, when Data is received with Content-type: application/json.
$ea_received_rest_web_service_data = ea_create_array_from_http_raw_json_data();

//Collect JSON Decoded Result of the received Raw PHP Input.
$ea_received_rest_ws_raw_array_input = $ea_received_rest_web_service_data["received_data_array"];

//Collect received Content Type		
$ea_received_rest_ws_content_type = $ea_received_rest_web_service_data["received_content_type"];

//Check if received content type is not application/json and send appropriate response headers.
if ($ea_received_rest_ws_content_type != "application/json") {
	
	//Define Response Header, with 406 Unacceptable HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 406 Unacceptable');
	
}//close of if ($ea_received_rest_ws_content_type != "application/json") {

// Auth token validation 
//apache_request_headers() is not available in FastCGI with PHP-FPM Setups. A workaround is to use alternative functions or in conjunction with .htaccess based directives.
//$ea_received_request_headers = apache_request_headers();

//getRequestHeaders() is used to collect Request Headers w.r.t. REST API, when apache_request_headers() function is unavailable.
$ea_received_request_headers = getRequestHeaders();

//JSON Encode the Request Headers, to show them in the Log.
$ea_received_request_headers_json_encoded = json_encode($ea_received_request_headers);

foreach ($ea_received_request_headers as $ea_received_request_header => $ea_received_request_header_value) {
	if (strtolower($ea_received_request_header) == "authorization") {
		list($ea_auth_token) = sscanf(trim($ea_received_request_header_value), 'Bearer %s');
	}//close of if (strtolower($ea_received_request_header) == "authorization") {
		
}//close of foreach ($ea_received_request_headers as $ea_received_request_header => $ea_received_request_header_value) {

if ($ea_auth_token != "") {		
	
	//$ea_auth_token_validation_status = validateAuthToken($ea_auth_token);
	$ea_auth_token_validation_status = ea_validate_hs256_alg_jwt_token($ea_auth_token);
	
	//Do Verify, if the JWT Auth Token Verification Status is Valid
	if ($ea_auth_token_validation_status) {
		
		//Extract JSON Web Token Details
		$ea_extracted_jwt_token_details = ea_extract_hs256_alg_jwt_token_details($ea_auth_token);
		//print_r($ea_extracted_jwt_token_details);
		
		//Get User ID & Other Info from the extracted JWT Token Details
		$ea_extracted_jwt_token_sub = $ea_extracted_jwt_token_details["sub"];
		$ea_extracted_jwt_token_user_company_id = $ea_extracted_jwt_token_details["user_company_id"];
		$ea_extracted_jwt_token_user_type = $ea_extracted_jwt_token_details["user_type"];
		$ea_extracted_jwt_token_user_privileges_list = $ea_extracted_jwt_token_details["user_privileges_list"];
		
		$ea_extracted_jwt_token_user_privileges_list_exploded = explode(",", $ea_extracted_jwt_token_user_privileges_list);
		
		//Check if User Privileges List is Same between data that is received from JWT Token and Database Sources
		$ea_user_privileges_list_check_result = ea_check_user_privileges_list_jwt_token_db_source($ea_extracted_jwt_token_user_privileges_list_exploded, $ea_extracted_jwt_token_sub);
		
		//Do act, based on User Privileges List Match Result
		if ($ea_user_privileges_list_check_result === true) {
			//User Privileges List that is received from the JWT Token is matching with the data, that is received from the Database Source. Do Authorization Checks (Check to see if at least One of the User Privileges List, that is received from JWT Auth Token matches with Allowed User Privileges, those that are accepted on specific Web Page / REST API Endpoint).
			
			//Do Check if at least one User Privilege matches the List of User Classifications, that are authorized, for this particular web page / rest api.
			$user_classification_page_api_check_result = ea_check_user_privileges_for_specific_page_api_db_source($ea_extracted_jwt_token_user_privileges_list_exploded, $page_filename);
			
			//https://stackoverflow.com/a/10691946
			if ($user_classification_page_api_check_result) {
				
				$ea_is_user_page_access_authorized = true;
			
			} else {
				
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "access-forbidden";
				$response['status_description'] = "Resources that require a different set of access permissions are requested, please contact Company Administrator / Site Administrator, if access to these resources are required.";
				
				//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
				header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
			}//close of else of if ($user_classification_page_api_check_result) {
				
						
		} else {
			//User privileges List that is received from the JWT Token does not match with the data, that is received from the Database Source. Do Reject the Token Now.
			
			//Do De-activate the User's Active Auth Token
			ea_update_user_rel_active_jwt_token_status_based_on_user_id($ea_extracted_jwt_token_sub);
						
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "valid-auth-token-submitted-different-user-privileges-list";
			$response['status_description'] = "The Privileges of the User Account in the application are different, when compared to the data that is received from  particular JWT Auth Token, Please do Re-Login once again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission / Valid JWT Token, but with different User Privileges List, by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
		
		}//close of else of if ($ea_user_privileges_list_check_result === true) {
		
		
		//Do Collect Quick User Details, for value of "sub / user_id", from the JWT Auth Token
		$ea_token_based_sub_user_id_details = user_basic_details_check_based_on_user_id($ea_extracted_jwt_token_sub);
		
		
		
		
	}//close of if ($ea_auth_token_validation_status) {
	
			
}//close of if ($ea_auth_token != "") {

?>