<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
				$job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {


				if ($job_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-id";
					$response['status_description'] = "Missing Job ID";
					
					$eventLog->log("missing-user-id: Please provide a valid Job ID.");
					
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Missing IP-Address";
					
					$eventLog->log("missing-ip-address: Please provide valid ip_address.");
					
				} else {
					//all inputs are valid
					$eventLog->log("All inputs are valid.");
					
					$ea_action_specific_auth_check_result = "";
					
					//User Details, based on User ID input, through api request
					$user_basic_details_result = user_basic_details_check_based_on_user_id($ea_extracted_jwt_token_sub);
					
					if (count($user_basic_details_result) > 0) {
						
						$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
						//$ea_action_specific_auth_check_result = ea_check_action_specific_user_privileges_different_user_levels_reset_password_feature($ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_user_company_id,  $ea_extracted_jwt_token_sub, $user_id_input, $ea_extracted_jwt_token_user_privileges_list, $user_basic_details_result["company_id"], $page_filename);					
						
						//echo "ea_action_specific_auth_check_result: " . $ea_action_specific_auth_check_result;					
						
						if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
									
									$job_rel_assigned_users_roles_list_result = job_rel_assigned_users_roles_list($job_id_input,$company_id_input);
									
									//$all_collected_candidate_visa_documentation_info_array["education_qualifications"] = candidate_education_qualification_records_list_with_uploaded_file_info($user_id_input);
									
								if(count($job_rel_assigned_users_roles_list_result) > 0) {	
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = $job_rel_assigned_users_roles_list_result;
									$response['status'] = "job-related-assigned-users-roles-list-fetched-successfully";
									$response['status_description'] = "Job related assigned users roles list fetched successfully";
									
									$eventLog->log("Job related assigned users roles list fetched successfully.");	
									
								
								} else {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "no-assignees-found-for-this-job";
									$response['status_description'] = "No assignees found for this jobs";
									
									$eventLog->log("No assignees found for this jobs.");	
									
									
								}//close of else of if ($data_scope_input == "all") {
									
								
									
							} catch (Exception $e) {
								
								$response['data'] = array();
								$response['status'] = "job-related-assigned-users-roles-list-fetching-error";
								$response['status_description'] = "Error occurred when fetching Job related assigned users roles list";
									
								$eventLog->log("Error occurred when fetching Job related assigned users roles list");	
									
							}
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-subject-platform-scope-user";
							$response['status_description'] = "Insufficient Set of Permissions, to complete this action";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-subject-platform-scope-user: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-different-company-user-id";
							$response['status_description'] = "Insufficient Set of Permissions, as the User belongs to a different company";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-INSUFFICIENT-PERMISSIONS") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions, as this process will not be allowed any further";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-self-action";
							$response['status_description'] = "Insufficient Set of Permissions, as self action is Dis-allowed";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-disallowed-self-action: Insufficient Set of Permissions, as self action is Dis-allowed, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-ACTION-SUBJECT-IS-A-DIFFERENT-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-action-on-different-user";
							$response['status_description'] = "Insufficient Set of Permissions, as action subject is a different user";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-disallowed-action-on-different-user: Insufficient Set of Permissions, as action subject is a different user, please check and try again.");	
							
						}//close of if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
						
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "Invalid User Account, that is attempted to be edited";
						
						$eventLog->log("invalid-user-account:Invalid User Account, that is attempted to be edited.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
					

				}//close of else of if ($user_type_input != 'admin') {
					
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted";
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input";
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){

//THIS FUNCTION ALREADY EXISTS IN EDUCATION QUALIFICATIONS PAGE. ONE COPY HAS TO BE DELETED, WHEN MOVING THEM IN TO COMMON FILES.
function job_rel_assigned_users_roles_list($job_id_input,$company_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	$assignment_status = "1";
	
	$job_rel_assigned_users_roles_list_sql = "SELECT * FROM `job_management_assignees` WHERE `job_id` = :job_id AND `company_id` = :company_id AND `assignment_status` = :assignment_status GROUP BY assigned_user_sm_memb_id ";
	$job_rel_assigned_users_roles_list_select_query = $dbcon->prepare($job_rel_assigned_users_roles_list_sql);
	$job_rel_assigned_users_roles_list_select_query->bindValue(":job_id",$job_id_input);			
	$job_rel_assigned_users_roles_list_select_query->bindValue(":company_id",$company_id_input);
	$job_rel_assigned_users_roles_list_select_query->bindValue(":assignment_status",$assignment_status);
	$job_rel_assigned_users_roles_list_select_query->execute(); 
	
	if($job_rel_assigned_users_roles_list_select_query->rowCount() > 0) {
		$job_rel_assigned_users_roles_list_select_query_query_result = $job_rel_assigned_users_roles_list_select_query->fetchAll();
		
		foreach($job_rel_assigned_users_roles_list_select_query_query_result as $job_rel_assigned_users_roles_list_select_query_query_result_row) {
			
			//$temp_array = array();		
			
			$assigned_user_sm_memb_id = $job_rel_assigned_users_roles_list_select_query_query_result_row["assigned_user_sm_memb_id"];
			
			//$temp_array = ;
			
			
			
			
			$constructed_array[] = job_rel_management_assignees_with_assigned_roles_info_based_on_job_id_recruiter_id_inputs($job_id_input, $assigned_user_sm_memb_id);
		}
		
		return $constructed_array;
	
	}//close of if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
	return $constructed_array;
}

function get_assignee_roles($assignie_id,$job_id,$company_id) {
	global $dbcon;
	
	$constructed_array = array();
	$assignment_status = "1";
	
	$get_assignee_roles_list_sql = "SELECT * FROM `job_management_assignees` WHERE `job_id` = :job_id AND `company_id` = :company_id AND `assignment_status` = :assignment_status AND `assignie_id` =:assignie_id";
	$get_assignee_roles_list_query = $dbcon->prepare($get_assignee_roles_list_sql);
	$get_assignee_roles_list_query->bindValue(":job_id",$job_id_input);			
	$get_assignee_roles_list_query->bindValue(":company_id",$company_id);
	$get_assignee_roles_list_query->bindValue(":assignie_id",$assignie_id);
	$get_assignee_roles_list_query->bindValue(":assignment_status",$assignment_status);
	$get_assignee_roles_list_query->execute(); 
	
	if($get_assignee_roles_list_query->rowCount() > 0) {
		$get_assignee_roles_list_query_result = $get_assignee_roles_list_query->fetchAll();
		
		/* foreach($get_assignee_roles_list_query_result as $get_assignee_roles_list_query_result_row) {
			
			$temp_array = array();		
			/* $temp_array["jma_id"] = $get_assignee_roles_list_query_result_row["jma_id"];
			$temp_array["company_id"] = $get_assignee_roles_list_query_result_row["company_id"];
			$temp_array["company_client_id"] = $get_assignee_roles_list_query_result_row["company_client_id"];
			$temp_array["job_id"] = $get_assignee_roles_list_query_result_row["job_id"];
			$temp_array["assigned_user_sm_memb_id"] = $get_assignee_roles_list_query_result_row["assigned_user_sm_memb_id"];
			 
			$temp_array["assigned_job_specific_role"] = $get_assignee_roles_list_query_result_row["assigned_job_specific_role"];
			//$temp_array["assignment_status"] = $get_assignee_roles_list_query_result_row["assignment_status"];
			
			$constructed_array[] = $temp_array;
		}
		 */
		return $get_assignee_roles_list_query_result;
	
	}//close of if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
	return $constructed_array;
}

function job_rel_management_assignees_with_assigned_roles_info_based_on_job_id_recruiter_id_inputs($job_id_input, $assigned_user_sm_memb_id_input) {
	global $dbcon;
	$constructed_array = array();
	
	//$constructed_array["assignee_id"] = $assigned_user_sm_memb_id_input;
	
	$jma_roles_array = array();
	$jma_roles_array["sm_memb_id"] = $assigned_user_sm_memb_id_input;
	$user_basic_details_result = user_basic_details_check_based_on_user_id($assigned_user_sm_memb_id_input);
	$sm_firstname= $user_basic_details_result["sm_firstname"];
	$sm_middlename = $user_basic_details_result["sm_middlename"];
	$sm_lastname = $user_basic_details_result["sm_lastname"];
	$jma_roles_array["sm_fullname"] = $sm_firstname . " " . $sm_middlename. " " .$sm_lastname;
	
	$jma_roles_array["is_primary_sales"] = "0";
	$jma_roles_array["is_sales"] = "0";
	$jma_roles_array["is_primary_recruiter"] = "0";
	$jma_roles_array["is_recruiter"] = "0";
	
	$job_management_assignees_get_sql = "SELECT * FROM `job_management_assignees` WHERE `job_id`=:job_id AND `assigned_user_sm_memb_id`=:assigned_user_sm_memb_id";
	$job_management_assignees_get_select_query = $dbcon->prepare($job_management_assignees_get_sql);
	$job_management_assignees_get_select_query->bindValue(":job_id",$job_id_input);
    $job_management_assignees_get_select_query->bindValue(":assigned_user_sm_memb_id",$assigned_user_sm_memb_id_input);
	$job_management_assignees_get_select_query->execute();
	
	if($job_management_assignees_get_select_query->rowCount() > 0) {
		$job_management_assignees_get_select_query_result = $job_management_assignees_get_select_query->fetchAll();
		
		foreach ($job_management_assignees_get_select_query_result as $job_management_assignees_get_select_query_result_row) {
			
			$assigned_job_specific_role = $job_management_assignees_get_select_query_result_row["assigned_job_specific_role"];
			$assignment_status = $job_management_assignees_get_select_query_result_row["assignment_status"];
			
			if ($assigned_job_specific_role == "1") {
				//As Primary Sales
				
				if ($assignment_status == "1") {
					$jma_roles_array["is_primary_sales"] = "1";
				}//close of if ($assignment_status == "1") {
					
			} else if ($assigned_job_specific_role == "2") {
				//As Sales
				
				if ($assignment_status == "1") {
					$jma_roles_array["is_sales"] = "1";
				}//close of if ($assignment_status == "1") {
					
			} else if ($assigned_job_specific_role == "3") {
				//As Primary Recruiter
				
				if ($assignment_status == "1") {
					$jma_roles_array["is_primary_recruiter"] = "1";
				}//close of if ($assignment_status == "1") {
					
			} else if ($assigned_job_specific_role == "4") {
				//As Recruiter
				
				if ($assignment_status == "1") {
					$jma_roles_array["is_recruiter"] = "1";
				}//close of if ($assignment_status == "1") {
					
			}//close of else if of if ($assigned_job_specific_role == "1") {
			
		}//close of foreach ($job_management_assignees_get_select_query_result as $job_management_assignees_get_select_query_result_row) {
		
		//$constructed_array[] = $jma_roles_array;
		return $jma_roles_array;
		
	}//close of else of if($job_management_assignees_get_select_query->rowCount() > 0) {
	return $constructed_array;

}

exit;
?>