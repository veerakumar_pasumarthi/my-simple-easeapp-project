<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "4")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					/* if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id']))  */
					
					if (isset($ea_received_rest_ws_raw_array_input['profile_source_name'])) {
						$content .= $ea_received_rest_ws_raw_array_input['profile_source_name'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['profile_source_name']))
                    
                    if (isset($ea_received_rest_ws_raw_array_input['profile_source_type'])) {
						$content .= $ea_received_rest_ws_raw_array_input['profile_source_type'] . "\r\n";
					}//close of if(isset($ea_received_rest_ws_raw_array_input['profile_source_type']))

                    if (isset($ea_received_rest_ws_raw_array_input['profile_source_url'])) {
						$content .= $ea_received_rest_ws_raw_array_input['profile_source_url'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['profile_source_url']))
 								 
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$profile_source_name_input = trim(isset($ea_received_rest_ws_raw_array_input['profile_source_name']) ? filter_var($ea_received_rest_ws_raw_array_input['profile_source_name'], FILTER_SANITIZE_STRING) : '');
			
				$profile_source_type_input = trim(isset($ea_received_rest_ws_raw_array_input['profile_source_type']) ? filter_var($ea_received_rest_ws_raw_array_input['profile_source_type'], FILTER_SANITIZE_STRING) : '');
				
				$profile_source_url_input = trim(isset($ea_received_rest_ws_raw_array_input['profile_source_url']) ? filter_var($ea_received_rest_ws_raw_array_input['profile_source_url'], FILTER_SANITIZE_URL) : '');
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				if ($profile_source_name_input == "") {
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-profile-source-name";
					$response['status_description'] = "Missing profile source name.";
					
					$eventLog->log("missing-profile-source-name: Please provide a valid Profile source name.");
					
				} else if ($profile_source_type_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-profile-source-type";
					$response['status_description'] = "Missing profile source type.";
					
					$eventLog->log("missing-profile-source-type: Please provide a valid Profile Source Type.");
						
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					
						//Event Time, as per Indian Standard Time
						$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
						
						$candidate_add_next_step = "";
						
						if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$candidate_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else if ((isset($ea_extracted_jwt_token_user_company_id)) /*&& ($company_id_input == $ea_extracted_jwt_token_user_company_id)*/) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$candidate_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
						} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
						}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
						$profile_source_seo_name_input=strtolower_utf8_extended(create_seo_name($profile_source_name_input));
							
						if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							try {
								$profile_source_details_duplicate_check_result = profile_source_details_duplicate_check($profile_source_name_input);
								if (count($profile_source_details_duplicate_check_result) > 0) {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "profile-source-already-exists";
							$response['status_description'] = "Profile source name already exists.";
							
							$eventLog->log("profile-source-already-exists: A profile source already exists with this Name");						
						
						} else {
								$last_inserted_id = profile_source_insert($profile_source_name_input,$profile_source_seo_name_input,$profile_source_type_input,$profile_source_url_input);
								
								$eventLog->log($last_inserted_id);
								
									
									if ($last_inserted_id != "") {
										
										    $response['data'] = array();
											$response['status'] = "profile-source-insertion-successful";
											$response['status_description'] = "Submitted successfully.";
										
											$eventLog->log("profile-source-insertion-successful:Profile Source is added Successfully.");   
										
									} else {
										//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
										$response['data'] = array();
										$response['status'] = "profile-source-insertion-error";
										$response['status_description'] = "No records found.";
										
										$eventLog->log("profile-source-insertion-error: There is an error, when adding the Profile Source into the Database.");
										
									}
						}	
								
									
							         
									
								
							} catch (Exception $e) {
								
								//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
								$response['data'] = array();
								$response['status'] = "resume-text-insertion-error";
								$response['status_description'] = "No records found.";
								
								$eventLog->log("resume-text-insertion-error: There is an error, when adding the Resume Text in the Database.");
							}
						}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							
						
					
				}//close of else of if ($email_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
    
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
			
	
	}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function profile_source_insert($profile_source_name_input,$profile_source_seo_name_input,$profile_source_type_input,$profile_source_url_input) {
    global $dbcon,$eventLog;
    $profile_source_insert_sql ="INSERT INTO `profile_sources`(`profile_source_name`, `profile_source_seo_name`, `profile_source_type`, `profile_source_url`) VALUES (:profile_source_name,:profile_source_seo_name,:profile_source_type,:profile_source_url)";
	
	$eventLog->log("after query");
	if($profile_source_url_input != "") {
	   $profile_source_url_input == $profile_source_url_input;	
	} else {
	   $profile_source_url_input == "";
	}
	$profile_source_insert_query = $dbcon->prepare($profile_source_insert_sql);
	$profile_source_insert_query->bindValue(":profile_source_name",$profile_source_name_input);
	$profile_source_insert_query->bindValue(":profile_source_seo_name",$profile_source_seo_name_input);
	$profile_source_insert_query->bindValue(":profile_source_type",$profile_source_type_input);
	$profile_source_insert_query->bindValue(":profile_source_url",$profile_source_url_input);

		if ($profile_source_insert_query->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			$eventLog->log("record inserted successfully");

			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}	

function profile_source_details_duplicate_check($profile_source_name_input) {
	global $dbcon;
	$constructed_array = array();

	$profile_source_duplicate_check_sql = "SELECT * FROM `profile_sources` WHERE `profile_source_name` = :profile_source_name";
	$profile_source_duplicate_check_q = $dbcon->prepare($profile_source_duplicate_check_sql);
	$profile_source_duplicate_check_q->bindValue(":profile_source_name",$profile_source_name_input);
    
	$profile_source_duplicate_check_q->execute();

	if($profile_source_duplicate_check_q->rowCount() > 0) {
		$profile_source_duplicate_check_q_result = $profile_source_duplicate_check_q->fetch();
	     return $profile_source_duplicate_check_q_result;

	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;

}
exit;
?>