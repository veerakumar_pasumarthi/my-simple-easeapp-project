<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of jobs, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "13")) { 

	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		/*if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs*/
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$country_input = trim(isset($ea_received_rest_ws_raw_array_input['country']) ? filter_var($ea_received_rest_ws_raw_array_input['country'], FILTER_SANITIZE_STRING) : '');
				$api_key_input = trim(isset($ea_received_rest_ws_raw_array_input['api_key']) ? filter_var($ea_received_rest_ws_raw_array_input['api_key'], FILTER_SANITIZE_STRING) : '');
                $time_period_input = trim(isset($ea_received_rest_ws_raw_array_input['time_period']) ? filter_var($ea_received_rest_ws_raw_array_input['time_period'], FILTER_SANITIZE_STRING) : '');

				$page_number_input = trim(isset($ea_received_rest_ws_raw_array_input['page_number']) ? filter_var($ea_received_rest_ws_raw_array_input['page_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				$number_of_records_input = trim(isset($ea_received_rest_ws_raw_array_input['number_of_records']) ? filter_var($ea_received_rest_ws_raw_array_input['number_of_records'], FILTER_SANITIZE_NUMBER_INT) : '');
				$sort_field_input = trim(isset($ea_received_rest_ws_raw_array_input['sort_field']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_field'], FILTER_SANITIZE_STRING) : '');
				$sort_order_input = trim(isset($ea_received_rest_ws_raw_array_input['sort_order']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_order'], FILTER_SANITIZE_STRING) : '');
				$search_criteria_input = trim(isset($ea_received_rest_ws_raw_array_input['search_criteria']) ? filter_var($ea_received_rest_ws_raw_array_input['search_criteria'], FILTER_SANITIZE_STRING) : '');
				$company_client_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_client_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_client_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				//$job_status_input = trim(isset($ea_received_rest_ws_raw_array_input['job_status']) ? filter_var($ea_received_rest_ws_raw_array_input['job_status'], FILTER_SANITIZE_NUMBER_INT) : '');
				$job_recruitment_status_input = trim(isset($ea_received_rest_ws_raw_array_input['job_recruitment_status']) ? filter_var($ea_received_rest_ws_raw_array_input['job_recruitment_status'], FILTER_SANITIZE_NUMBER_INT) : '');
				$resume_acceptance_status_input = trim(isset($ea_received_rest_ws_raw_array_input['resume_acceptance_status']) ? filter_var($ea_received_rest_ws_raw_array_input['resume_acceptance_status'], FILTER_SANITIZE_NUMBER_INT) : '');
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
			
				
				$eventLog->log("ip_address_input -> " . $ip_address_input);
				
				//Check if all inputs are received correctly from the REST Web Service
				if ($company_id_input == "") {
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "missing company id .";
					
					$eventLog->log("Please provide all information.");
					
                } else if ($page_number_input == "0") {
					//Invalid Page Number scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-page-number";
					$response['status_description'] = "Invalid Page Number info submitted, please check and try again.";
					
					$eventLog->log("invalid-page-number: Invalid Page Number info submitted, please check and try again.");
					
				} else if (($sort_order_input != "ASC") && ($sort_order_input != "DESC") && ($sort_order_input != "")) {
					//Invalid Sorting Order scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-Sorting-order";
					$response['status_description'] = "Invalid Sorting Order info submitted, please check and try again.";
					
					$eventLog->log("invalid-Sorting-order: Invalid Sorting Order info submitted, please check and try again.");				
				
					
				} else  if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-additional-information";
					$response['status_description'] = "Some Additional Information like IP Address (IPv4) is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");
							  
				} else {
					//All inputs are Valid
				
					$eventLog->log("All inputs are valid.");
				
                    //$jobs_list_info_result = jobs_list_info($company_id_input);
					
					if ($company_client_id_input == "") {
						$company_client_id_input = null;
					}//close of if ($company_client_id_input == "") {
						
					
					/*if ($ea_extracted_jwt_token_user_company_id != "") {
						                            
							$jobs_list_info_result = get_jobs_list_with_pagination_inputs($ea_extracted_jwt_token_user_company_id,$company_client_id_input,$page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input,$job_recruitment_status_input,$resume_acceptance_status_input);
						
					} else {
						//Platform Scenario: The logged in User is either a Site Admin or a Super Admin
						
							$jobs_list_info_result = get_jobs_list_with_pagination_inputs($company_id_input,$company_client_id_input,$page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input,$job_recruitment_status_input,$resume_acceptance_status_input);
					}//close of else of if ($company_client_id_input == "") {*/
				    $api_key_with_company_id_get = get_api_key_with_company_id($company_id_input,$api_key_input);
				    if(count($api_key_with_company_id_get) > 0) {

					$jobs_list_info_result = get_jobs_list_with_pagination_inputs($company_id_input,$company_client_id_input,$page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input,$job_recruitment_status_input,$resume_acceptance_status_input,$country_input);	
					
					
					 if (count($jobs_list_info_result) > 0) {
			
						//One or More User Groups Exist and Active
							
						$response['data'] = $jobs_list_info_result;
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['status'] = "prelogin-jobs-list-received";
						$response['status_description'] = "prelogin jobs List Successfully Received";
						
						$jobs_list_info_result_json_encoded = json_encode($jobs_list_info_result);
					
						$eventLog->log("jobs List -> " . $jobs_list_info_result_json_encoded); 
							
				    } else {
							
					    //Construct Content, that will be sent in Response body, of the REST Web Service
					    $response['data'] = array();
					    $response['status'] = "active-jobs-doesnot-exist";
					    $response['status_description'] = "No Active jobs list Exist, please check and try again.";
				
						//No Active jobs list Exist
					    $eventLog->log("jobs List -> No Active jobs Exist, please check and try again."); 
					}//close of else of if ($jobs_list_result_count > "0") {
				} else {
					$response['data'] = array();
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['status'] = "api-key-doesnt-match";
					$response['status_description'] = "api key doesnt match";
				}
				
			    }
													
			//}//close of if ($ea_is_user_page_access_authorized)	
				
			   
			
		/*} else {

			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
		 
		    //Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
		 
		    header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
	   
	    }	*/	
    }//close of if ($ea_maintenance_mode == false) {
			
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}



//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
function get_jobs_list_with_pagination_inputs($company_id_input,$company_client_id_input,$page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input,$job_recruitment_status_input,$resume_acceptance_status_input,$country_input) {

    global $dbcon, $default_number_of_records_pagination; //app/core/main-config.php
	global $eventLog;
	$constructed_array = array();
	$eventLog->log("before is_null concept");

	if ($search_criteria_input == "") {
		$search_criteria_input = null;
	}//close of if ($search_criteria_input == "") {

	if (!is_null($search_criteria_input)) {
		//When Search Criteria input is not empty
		$search_criteria_variable = "%" . $search_criteria_input . "%";
		//$search_criteria_in_query = "(`company_name` LIKE '%info%') OR (`company_seo_name` LIKE '%info%') OR (`company_brand_name` LIKE '%info%') OR (`is_active_status` LIKE '%info%')";
		$search_criteria_in_query_with_where_keyword = " WHERE ((`job_id` LIKE :job_id_search_keyword) OR (`company_id` LIKE :company_id_search_keyword) OR  (`company_client_id` LIKE :company_client_id_search_keyword) OR (`job_title` LIKE :job_title_search_keyword) OR (`available_positions` LIKE :available_positions_search_keyword) OR (`city` LIKE :city_search_keyword) OR (`state` LIKE :state_search_keyword) OR (`country` LIKE :country_search_keyword) OR (`job_posted_date_time` LIKE :job_posted_date_time_search_keyword) OR (`job_industry_name` LIKE :job_industry_name_search_keyword) OR (`resume_submission_end_date` LIKE :resume_submission_end_date_search_keyword) OR (`job_recruitment_status` LIKE :job_recruitment_status_search_keyword) OR (`is_active_status` LIKE :is_active_status_search_keyword)) ";
		$eventLog->log("after search criteria concept");
		$search_criteria_in_query_without_where_keyword = " AND ((`job_id` LIKE :job_id_search_keyword) OR (`company_id` LIKE :company_id_search_keyword) OR  (`company_client_id` LIKE :company_client_id_search_keyword) OR (`job_title` LIKE :job_title_search_keyword) OR (`available_positions` LIKE :available_positions_search_keyword) OR (`city` LIKE :city_search_keyword) OR (`state` LIKE :state_search_keyword) OR (`country` LIKE :country_search_keyword) OR (`job_posted_date_time` LIKE :job_posted_date_time_search_keyword) OR (`job_industry_name` LIKE :job_industry_name_search_keyword) OR (`resume_submission_end_date` LIKE :resume_submission_end_date_search_keyword) OR (`job_recruitment_status` LIKE :job_recruitment_status_search_keyword) OR (`is_active_status` LIKE :is_active_status_search_keyword)) ";
	    $eventLog->log("after search criteria without where concept");
	} else {
		//When Search Criteria input is empty
		$search_criteria_in_query_with_where_keyword = " WHERE ";

		$search_criteria_in_query_without_where_keyword = "";

	}//close of else of if (!is_null($search_criteria_input)) {
	$eventLog->log("before sort order concept");
	
	if ($company_client_id_input == "") {
		
		$company_client_id_in_query = "";
	
	} else {
		//When Search Criteria input is empty
		$company_client_id_in_query = "AND `company_client_id`=:company_client_id";
	}
	
	if ($job_recruitment_status_input != "" && $resume_acceptance_status_input != "" ) {
		$recruitment_status_add_query = "AND `job_recruitment_status`=:job_recruitment_status AND `resume_acceptance_status`=:resume_acceptance_status ";
	
	} else if ($job_recruitment_status_input == "" && $resume_acceptance_status_input != "") {
		$recruitment_status_add_query = "AND `resume_acceptance_status`=:resume_acceptance_status ";
		
	} else if ($job_recruitment_status_input != "" && $resume_acceptance_status_input == "") {
		$recruitment_status_add_query = "AND `job_recruitment_status`=:job_recruitment_status ";
		
	} else {
		$recruitment_status_add_query = "";
	}

	if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {

		if (!is_null($sort_field_input)) {
			//When the Sort Field input is not empty

			if ($sort_field_input == "job_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `job_id` " . $sort_order_input;

			} else if ($sort_field_input == "company_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_id` " . $sort_order_input;

			} else if ($sort_field_input == "company_client_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_client_id` " . $sort_order_input;

			} else if ($sort_field_input == "job_title") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `job_title` " . $sort_order_input;

			} else if ($sort_field_input == "job_industry_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `job_industry_name` " . $sort_order_input;

			} else if ($sort_field_input == "city") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `city` " . $sort_order_input;

			} else if ($sort_field_input == "state") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `state` " . $sort_order_input;

			} else if ($sort_field_input == "country") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `country` " . $sort_order_input;

			} else if ($sort_field_input == "available_positions") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `available_positions` " . $sort_order_input;

			} else if ($sort_field_input == "resume_submission_end_date") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `resume_submission_end_date` " . $sort_order_input;

			} else if ($sort_field_input == "job_recruitment_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `job_recruitment_status` " . $sort_order_input;
				
			} else if ($sort_field_input == "resume_acceptance_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `resume_acceptance_status` " . $sort_order_input;

			} else if ($sort_field_input == "job_posted_date_time") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `job_posted_date_time` " . $sort_order_input;

			} else if ($sort_field_input == "job_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `is_active_status` " . $sort_order_input;

			} else {
				//When Sort Field input, is not among the expected values, use primary key, with provided Sorting Order input
				$sort_details_in_query = " ORDER BY `job_id` " . $sort_order_input;
			}

		} else {
			//When the Sort Field input is empty, primary key has to be taken, with provided Sorting Order input
			$sort_details_in_query = " ORDER BY `job_id` " . $sort_order_input;

		}//close of if (!is_null($sort_field_input)) {


	} else {
		//When the Sort Field input is empty, primary key has to be taken, with ASC Order, by default
		$sort_details_in_query = " ORDER BY `job_id` ASC ";

	}//close of else of if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {


	$limit_offset_value = 0;

	if (($page_number_input == "") && ($number_of_records_input == "")) {
		//Give All Data
		//No LIMIT Clause in the Query
		$limit_offset_in_query = "";

	} else if (($page_number_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than page_number_input, till the number of records, as defined in the $default_number_of_records_pagination

		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $default_number_of_records_pagination;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$default_number_of_records_pagination;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $default_number_of_records_pagination;
		}//close of else if of if ($page_number_input == "1") {



	} else if (($page_number_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records
		$limit_offset_in_query = " LIMIT " . $number_of_records_input;

	} else {
		//Give Data, from given Record, till the requested number of records
		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $number_of_records_input;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$number_of_records_input;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $number_of_records_input;
		}//close of else if of if ($page_number_input == "1") {

	}//close of else of if (($page_number_input == "") && ($number_of_records_input == "")) {
	
	if (!is_null($search_criteria_input)) {
		
		//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)

		//Define Named parameters and corresponding Values
		
		if($company_client_id_input=="") {
			$job_named_parameters_values_array_input = array(":company_id" => $company_id_input,":job_id_search_keyword" => $search_criteria_variable,":company_id_search_keyword" => $search_criteria_variable,":company_client_id_search_keyword" => $search_criteria_variable, ":job_title_search_keyword" => $search_criteria_variable, ":job_industry_name_search_keyword" => $search_criteria_variable, ":city_search_keyword" => $search_criteria_variable, ":state_search_keyword" => $search_criteria_variable, ":country_search_keyword" => $search_criteria_variable, ":available_positions_search_keyword" => $search_criteria_variable,":resume_submission_end_date_search_keyword" => $search_criteria_variable,":job_id_search_keyword" => $search_criteria_variable,":job_recruitment_status_search_keyword" => $search_criteria_variable,":job_posted_date_time_search_keyword" => $search_criteria_variable, ":is_active_status_search_keyword" => $search_criteria_variable);    
		} else {
			$job_named_parameters_values_array_input = array(":company_id" => $company_id_input,":company_client_id" => $company_client_id_input,":job_id_search_keyword" => $search_criteria_variable,":company_id_search_keyword" => $search_criteria_variable, ":company_client_id_search_keyword" => $search_criteria_variable, ":job_title_search_keyword" => $search_criteria_variable, ":job_industry_name_search_keyword" => $search_criteria_variable, ":city_search_keyword" => $search_criteria_variable, ":state_search_keyword" => $search_criteria_variable, ":country_search_keyword" => $search_criteria_variable, ":available_positions_search_keyword" => $search_criteria_variable,":resume_submission_end_date_search_keyword" => $search_criteria_variable,":job_id_search_keyword" => $search_criteria_variable,":job_recruitment_status_search_keyword" => $search_criteria_variable,":job_posted_date_time_search_keyword" => $search_criteria_variable, ":is_active_status_search_keyword" => $search_criteria_variable);
		}
		
		if ($job_recruitment_status_input != "" && $resume_acceptance_status_input != "" ) {
		
			
			$job_named_parameters_values_array_input[":job_recruitment_status"] = $job_recruitment_status_input;
			$job_named_parameters_values_array_input[":resume_acceptance_status"] = $resume_acceptance_status_input;
			
			
		} else if ($job_recruitment_status_input == "" && $resume_acceptance_status_input != "") {
		    
			$job_named_parameters_values_array_input[":resume_acceptance_status"] = $resume_acceptance_status_input;
			
			
		} else if ($job_recruitment_status_input != "" && $resume_acceptance_status_input == "") {
			
			$job_named_parameters_values_array_input[":job_recruitment_status"] = $job_recruitment_status_input;
		}//Get Jobs List Count
		
		$jobs_list_count_get_sql = "SELECT COUNT(*) AS count FROM `jobs` WHERE `company_id`=:company_id " . $recruitment_status_add_query . $company_client_id_in_query . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;
		$eventLog->log($jobs_list_count_get_sql);
		$jobs_list_count_get_select_query = $dbcon->prepare($jobs_list_count_get_sql);
		$jobs_list_count_get_select_query->execute($job_named_parameters_values_array_input);

		//Get Jobs List
		$jobs_list_get_sql = "SELECT * FROM `jobs` WHERE `company_id`=:company_id " . $recruitment_status_add_query . $company_client_id_in_query . $search_criteria_in_query_without_where_keyword . $sort_details_in_query . $limit_offset_in_query;

		$jobs_list_get_select_query = $dbcon->prepare($jobs_list_get_sql);
		/*$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);
		$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
		$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
		$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
		$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
		$companies_list_get_select_query->execute();*/
		$jobs_list_get_select_query->execute($job_named_parameters_values_array_input);

	} else {
		
		
		//Give List of Jobs Count, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
		if ($company_client_id_input == "") {
		  $job_named_parameters_values_array_input = array(":company_id" => $company_id_input);
		  
		} else {
			$job_named_parameters_values_array_input = array(":company_id" => $company_id_input,":company_client_id" => $company_client_id_input);
		}
		if ($job_recruitment_status_input != "" && $resume_acceptance_status_input != "" ) {
		
			
			$job_named_parameters_values_array_input[":job_recruitment_status"] = $job_recruitment_status_input;
			$job_named_parameters_values_array_input[":resume_acceptance_status"] = $resume_acceptance_status_input;
			
			
		} else if ($job_recruitment_status_input == "" && $resume_acceptance_status_input != "") {
		    
			$job_named_parameters_values_array_input[":resume_acceptance_status"] = $resume_acceptance_status_input;
			
			
		} else if ($job_recruitment_status_input != "" && $resume_acceptance_status_input == "") {
			
			$job_named_parameters_values_array_input[":job_recruitment_status"] = $job_recruitment_status_input;
		}//Get Jobs List Count
		$jobs_list_count_get_sql = "SELECT COUNT(*) AS count FROM `jobs` WHERE `company_id`=:company_id ". $recruitment_status_add_query . $company_client_id_in_query . $sort_details_in_query;
		$eventLog->log($jobs_list_count_get_sql);
		$jobs_list_count_get_select_query = $dbcon->prepare($jobs_list_count_get_sql);
		$jobs_list_count_get_select_query->execute($job_named_parameters_values_array_input);


		//Give List of Jobs, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
		$jobs_list_get_sql = "SELECT * FROM `jobs` WHERE `company_id`=:company_id " . $recruitment_status_add_query . $company_client_id_in_query . $sort_details_in_query . $limit_offset_in_query;
		$jobs_list_get_select_query = $dbcon->prepare($jobs_list_get_sql);
		$jobs_list_get_select_query->execute($job_named_parameters_values_array_input);

	}//close of else of if (!is_null($search_criteria_input)) {

	

	//Process / Get Companies Count
	if($jobs_list_count_get_select_query->rowCount() > 0) {
	    $jobs_list_count_get_select_query_result = $jobs_list_count_get_select_query->fetch();
	    //print_r($jobs_list_count_get_select_query_result);

		$total_jobs_count = $jobs_list_count_get_select_query_result["count"];
		$constructed_array["total_records_count"] = $total_jobs_count;

	}//close of if($jobs_list_count_get_select_query->rowCount() > 0) {

	//Process / Fetch Companies List
	if($jobs_list_get_select_query->rowCount() > 0) {
	    $jobs_list_get_select_query_result = $jobs_list_get_select_query->fetchAll();
	    //print_r($companies_list_get_select_query_result);

		foreach ($jobs_list_get_select_query_result as $jobs_list_get_select_query_result_row) {

			$temp_row_array = array();
		    $temp_row_array["job_id"] = $jobs_list_get_select_query_result_row["job_id"];
		    $temp_row_array["company_id"] = $jobs_list_get_select_query_result_row["company_id"];
			$temp_row_array["company_client_id"] = $jobs_list_get_select_query_result_row["company_client_id"];
		    $temp_row_array["job_title"] = $jobs_list_get_select_query_result_row["job_title"];
		    $temp_row_array["job_industry_name"] = $jobs_list_get_select_query_result_row["job_industry_name"];
			$temp_row_array["city"] = $jobs_list_get_select_query_result_row["city"];
			$temp_row_array["state"] = $jobs_list_get_select_query_result_row["state"];
			$temp_row_array["country"] = $jobs_list_get_select_query_result_row["country"];
			$temp_row_array["no_of_openings"] = $jobs_list_get_select_query_result_row["no_of_openings"];
			$temp_row_array["available_positions"] = $jobs_list_get_select_query_result_row["available_positions"];
			$temp_row_array["resume_submission_end_date"] = $jobs_list_get_select_query_result_row["resume_submission_end_date"];
			$temp_row_array["job_posted_date_time"] = $jobs_list_get_select_query_result_row["job_posted_date_time"];
			$temp_row_array["job_recruitment_status"] = $jobs_list_get_select_query_result_row["job_recruitment_status"];
			$temp_row_array["resume_acceptance_status"] = $jobs_list_get_select_query_result_row["resume_acceptance_status"];
			$temp_row_array["job_status"] = $jobs_list_get_select_query_result_row["is_active_status"];

		    $constructed_array["list"][] = $temp_row_array;
	    }//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

	}//close of if($companies_list_get_select_query->rowCount() > 0) {

	return $constructed_array;
}
function get_api_key_with_company_id($company_id_input,$api_key_input) {
	global $dbcon;
	$constructed_array = array();
	$is_active_status = "1";
	
	$candidate_preferred_job_location_record_duplicate_check_sql = "SELECT * FROM `company_rel_api_keys` WHERE `company_id` = :company_id AND `api_key` = :api_key AND `is_active_status` = :is_active_status";

	$candidate_preferred_job_location_record_duplicate_check_q = $dbcon->prepare($candidate_preferred_job_location_record_duplicate_check_sql);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":company_id",$company_id_input);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":is_active_status",$is_active_status);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":api_key",$api_key_input);

    
	$candidate_preferred_job_location_record_duplicate_check_q->execute();
  
	if($candidate_preferred_job_location_record_duplicate_check_q->rowCount() > 0) {
		$candidate_preferred_job_location_record_duplicate_check_q_result = $candidate_preferred_job_location_record_duplicate_check_q->fetchAll();
		
			
			return $candidate_preferred_job_location_record_duplicate_check_q_result;
		}

	     return $constructed_array;
	    

	 //close of if($candidate_preferred_location_record_duplicate_check_q_result->rowCount() > 0) {
	}
exit;
?>