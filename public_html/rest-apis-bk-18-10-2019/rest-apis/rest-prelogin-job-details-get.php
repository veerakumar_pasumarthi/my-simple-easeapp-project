<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Edit Quick Admin Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "4")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		/*if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				*/
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['job_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_id'])) 
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id']))
					if (isset($ea_received_rest_ws_raw_array_input['api_key'])) {
						$content .= $ea_received_rest_ws_raw_array_input['api_key'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');

				$api_key_input = trim(isset($ea_received_rest_ws_raw_array_input['api_key']) ? filter_var($ea_received_rest_ws_raw_array_input['api_key'], FILTER_SANITIZE_STRING) : '');

				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				
				if ($job_id_input == "") {
					//Invalid Mobile Number scenario
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-id";
					$response['status_description'] = "invalid Job Id, please check and try again.";
					
					$eventLog->log("Please provide a valid company Job id.");
					
				
				
				} else if ($company_id_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "missing company id.";
					
					$eventLog->log("Please provide all information.");	
				
				}else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Some Additional Information is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//Event Time, as per Indian Standard Time
					$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
		
					/*$company_proceed_next_step = "";
					
					
					if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$company_proceed_next_step = "PROCEED-TO-NEXT-STEP";
								
							//} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {	
							} else if (isset($ea_extracted_jwt_token_user_company_id)) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$company_proceed_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
					} else {
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
					}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
							
					if ($company_proceed_next_step == "PROCEED-TO-NEXT-STEP") {*/
						
							try {
							$api_key_with_company_id_get = get_api_key_with_company_id($company_id_input,$api_key_input);
				           if(count($api_key_with_company_id_get) > 0) {
								    $job_details_result = job_details_get_based_on_job_id($job_id_input,$company_id_input);
								    if(count($job_details_result) > 0){
										  
										$response['data'] = $job_details_result;
										$response['status'] = "prelogin-job-details-successfully-fetched";
										$response['status_description'] = "pre login job details fetched Successfully.";

										$eventLog->log("The job details fetched Successfully.");
													     
									} else {
									
										$response['data'] = array();
										$response['status'] = "no-prelogin-job-exist";
										$response['status_description'] = "No prelogin job details found for this Job.";
										
										$eventLog->log("No job details found for this Job.");
													
									}	

									} else {
										$response['data'] = array();
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
					                    $response['status'] = "api-key-doesnt-match";
					                    $response['status_description'] = "api key doesnt match";
									}		
								} catch (Exception $e) {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "prelogin-job-details-fetching-error";
									$response['status_description'] = "There is an error, when fetching prelogin  job details.";
									
									$eventLog->log("There is an error, when fetching job details.");	
									
								}
					   
						
					/*} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "The User Account, that is attempted to be edited, is invalid, please check and try again.";
						
						$eventLog->log("Please provide a Valid user ID.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
					
						
				}*///close of else of if ($email_id_input == "") {
				
			}
			
		/*}	
			
	} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {*/
	
	}
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function job_details_get_based_on_job_id($job_id_input,$company_id_input) {
	global $dbcon;
	$constructed_array = array();
	$job_details_get_sql = "SELECT * FROM `jobs` j JOIN `company_clients` cc ON j.company_client_id = cc.company_client_id WHERE j.job_id =:job_id AND j.company_id =:company_id";
	$job_details_ge_select_query = $dbcon->prepare($job_details_get_sql);
	$job_details_ge_select_query->bindValue(":job_id",$job_id_input);
	$job_details_ge_select_query->bindValue(":company_id",$company_id_input);
	$job_details_ge_select_query->execute(); 
	
	if($job_details_ge_select_query->rowCount() > 0) {
		$job_details_ge_select_query_result = $job_details_ge_select_query->fetchAll();
		foreach ($job_details_ge_select_query_result as $job_details_ge_select_query_result_row) {
			
			
			$constructed_array["job_id"] = $job_details_ge_select_query_result_row["job_id"];
			$constructed_array["company_id"] = $job_details_ge_select_query_result_row["company_id"];
			$constructed_array["company_client_id"] = $job_details_ge_select_query_result_row["company_client_id"];
		    $constructed_array["client_company_name"] = $job_details_ge_select_query_result_row["client_company_name"];
			$constructed_array["job_title"] = $job_details_ge_select_query_result_row["job_title"];
			$constructed_array["job_type_name"] = $job_details_ge_select_query_result_row["job_type_name"];
			$constructed_array["job_summary"] = $job_details_ge_select_query_result_row["job_summary"];
			$constructed_array["job_full_description"] = $job_details_ge_select_query_result_row["job_full_description"];
			$constructed_array["min_experience_period"] = $job_details_ge_select_query_result_row["min_experience_period"];
			$constructed_array["max_experience_period"] = $job_details_ge_select_query_result_row["max_experience_period"];
			$constructed_array["job_work_location_requirement_name"] = $job_details_ge_select_query_result_row["job_work_location_requirement_name"];
		    $constructed_array["job_industry_name"] = $job_details_ge_select_query_result_row["job_industry_name"];
			
			$constructed_array["pay_rate_min_value"] = $job_details_ge_select_query_result_row["pay_rate_min_value"];
			$constructed_array["pay_rate_max_value"] = $job_details_ge_select_query_result_row["pay_rate_max_value"];
			$constructed_array["salary_display_options"] = $job_details_ge_select_query_result_row["salary_display_options"];
			$constructed_array["maximum_allowed_submittals_count"] = $job_details_ge_select_query_result_row["maximum_allowed_submittals_count"];
			
			$constructed_array["bill_rate_currency_three_lettered_code"] = $job_details_ge_select_query_result_row["bill_rate_currency_three_lettered_code"];
			$constructed_array["bill_rate_value"] = $job_details_ge_select_query_result_row["bill_rate_value"];
			$constructed_array["bill_rate_job_compensation_period_id"] = $job_details_ge_select_query_result_row["bill_rate_job_compensation_period_id"];
			$constructed_array["bill_rate_job_compensation_period_name"] = $job_details_ge_select_query_result_row["bill_rate_job_compensation_period_name"];
			$constructed_array["bill_rate_job_compensation_period_seo_name"] = $job_details_ge_select_query_result_row["bill_rate_job_compensation_period_seo_name"];
			
			$constructed_array["bill_rate"] = $constructed_array["bill_rate_value"] . " ". $constructed_array["bill_rate_currency_three_lettered_code"]. "/" . $constructed_array["bill_rate_job_compensation_period_name"] ;
			
			$constructed_array["pay_rate_currency_three_lettered_code"] = $job_details_ge_select_query_result_row["pay_rate_currency_three_lettered_code"];
			$constructed_array["pay_rate_job_compensation_period_id"] = $job_details_ge_select_query_result_row["pay_rate_job_compensation_period_id"];
			$constructed_array["pay_rate_job_compensation_period_name"] = $job_details_ge_select_query_result_row["pay_rate_job_compensation_period_name"];
			$constructed_array["pay_rate_job_compensation_period_seo_name"] = $job_details_ge_select_query_result_row["pay_rate_job_compensation_period_seo_name"];
			
			$constructed_array["min_pay_rate"] = $constructed_array["pay_rate_min_value"] . " ". $constructed_array["pay_rate_currency_three_lettered_code"] . "/" .$constructed_array["pay_rate_job_compensation_period_name"];
			$constructed_array["max_pay_rate"] = $constructed_array["pay_rate_max_value"] . " ". $constructed_array["pay_rate_currency_three_lettered_code"] . "/" .$constructed_array["pay_rate_job_compensation_period_name"];
			
			
			$constructed_array["finalized_pay_rate_currency_three_lettered_code"] = $job_details_ge_select_query_result_row["finalized_pay_rate_currency_three_lettered_code"];
			$constructed_array["finalized_pay_rate_value"] = $job_details_ge_select_query_result_row["finalized_pay_rate"];
			$constructed_array["finalized_pay_rate_job_compensation_period_id"] = $job_details_ge_select_query_result_row["finalized_pay_rate_job_compensation_period_id"];
			$constructed_array["finalized_pay_rate_job_compensation_period_name"] = $job_details_ge_select_query_result_row["finalized_pay_rate_job_compensation_period_name"];
			$constructed_array["finalized_pay_rate_job_compensation_period_seo_name"] = $job_details_ge_select_query_result_row["finalized_pay_rate_job_compensation_period_seo_name"];
			$constructed_array["finalized_pay_rate"] = $constructed_array["finalized_pay_rate_value"] . " ". $constructed_array["finalized_pay_rate_currency_three_lettered_code"] . "/" .$constructed_array["finalized_pay_rate_job_compensation_period_name"];
			
			$constructed_array["address_line_1"] = $job_details_ge_select_query_result_row["address_line_1"];
			$constructed_array["address_line_2"] = $job_details_ge_select_query_result_row["address_line_2"];
			$constructed_array["city"] = $job_details_ge_select_query_result_row["city"];
			$constructed_array["state"] = $job_details_ge_select_query_result_row["state"];
			$constructed_array["country"] = $job_details_ge_select_query_result_row["country"];
			$constructed_array["zipcode"] = $job_details_ge_select_query_result_row["zipcode"];
			
			$constructed_array["no_of_openings"] = $job_details_ge_select_query_result_row["no_of_openings"];
			$constructed_array["available_positions"] = $job_details_ge_select_query_result_row["available_positions"];
			$constructed_array["resume_submission_end_date"] = $job_details_ge_select_query_result_row["resume_submission_end_date"];
			$constructed_array["job_posted_date_time"] = $job_details_ge_select_query_result_row["job_posted_date_time"];
			$constructed_array["job_posted_by_sm_memb_id"] = $job_details_ge_select_query_result_row["job_posted_by_sm_memb_id"];
			$sm_memb_id_input=$constructed_array["job_posted_by_sm_memb_id"];
			$user_basic_details_result = user_basic_details_check_based_on_user_id($sm_memb_id_input);
			if(count($user_basic_details_result)>0) {
				$sm_firstname = $user_basic_details_result["sm_firstname"];
				$sm_middlename = $user_basic_details_result["sm_middlename"];
				$sm_lastname = $user_basic_details_result["sm_lastname"];
			} else {
				$sm_firstname ="null";
				$sm_middlename = "null";
				$sm_lastname = "null";
				$constructed_array["job_posted_by"] = "null";
			}
				
			$constructed_array["job_posted_by"] = $sm_firstname . " " . $sm_middlename . " " .$sm_lastname;
			$constructed_array["expected_job_start_date"] = $job_details_ge_select_query_result_row["expected_job_start_date"];
			
			$constructed_array["expected_job_end_date"] = $job_details_ge_select_query_result_row["expected_job_end_date"];
			$constructed_array["resume_acceptance_status"] = $job_details_ge_select_query_result_row["resume_acceptance_status"];
			$constructed_array["job_recruitment_status"] = $job_details_ge_select_query_result_row["job_recruitment_status"];
			$constructed_array["is_active_status"] = $job_details_ge_select_query_result_row["is_active_status"];
			$constructed_array["jsl_definition_ownership_status"] = $job_details_ge_select_query_result_row["jsl_definition_ownership_status"];
			$constructed_array["jsl_internal_team_manual_assignment_req_status"] = $job_details_ge_select_query_result_row["jsl_internal_team_manual_assignment_req_status"];
			$constructed_array["job_rel_email_notification_status"] = $job_details_ge_select_query_result_row["job_rel_email_notification_status"];
			$constructed_array["job_rel_sms_notification_status"] = $job_details_ge_select_query_result_row["job_rel_sms_notification_status"];
			$constructed_array["job_rel_sms_notification_status"] = $job_details_ge_select_query_result_row["job_rel_sms_notification_status"];
			
			
			
			
	    }

		return $constructed_array;
	}
	return $constructed_array;
}

function get_api_key_with_company_id($company_id_input,$api_key_input) {
	global $dbcon;
	$constructed_array = array();
	$is_active_status = "1";
	
	$candidate_preferred_job_location_record_duplicate_check_sql = "SELECT * FROM `company_rel_api_keys` WHERE `company_id` = :company_id AND `api_key` = :api_key AND `is_active_status` = :is_active_status";

	$candidate_preferred_job_location_record_duplicate_check_q = $dbcon->prepare($candidate_preferred_job_location_record_duplicate_check_sql);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":company_id",$company_id_input);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":is_active_status",$is_active_status);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":api_key",$api_key_input);

    
	$candidate_preferred_job_location_record_duplicate_check_q->execute();
  
	if($candidate_preferred_job_location_record_duplicate_check_q->rowCount() > 0) {
		$candidate_preferred_job_location_record_duplicate_check_q_result = $candidate_preferred_job_location_record_duplicate_check_q->fetchAll();
		
			
			return $candidate_preferred_job_location_record_duplicate_check_q_result;
		}

	     return $constructed_array;
	    

	 //close of if($candidate_preferred_location_record_duplicate_check_q_result->rowCount() > 0) {
	}
	

exit;
?>


