<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Edit Quick Admin Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "2")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				
				
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				
				if ($company_id_input == "") {
					//Invalid Mobile Number scenario
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "invalid company  id, please check and try again.";
					
					$eventLog->log("Please provide a valid company id.");
					
				
				
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Some Additional Information is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//Event Time, as per Indian Standard Time
					$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
		
					
							try {
							       $company_client_basic_details_result = company_basic_details_check_based_on_company_client_id($company_id_input);
								   
								   if(count($company_client_basic_details_result) > 0){
									   
									   
									   if($company_client_basic_details_result["is_shown_in_panel"] == "0") {
										
											$company_id_update=C_update_based_on_company_id_and_is_active_status($company_id_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
											
											$company_basic_details_result=company_details_check_based_on_company_client_id($company_id_input);	  
											
											$response['data'] = $company_basic_details_result;
											$response['status'] = "company-api-key-details-successfully-fetched";
											$response['status_description'] = "company api key details fetched Successfully.";

											$eventLog->log("The api key details fetched Successfully.");
											
										
									    } else {
										   
											$response['data'] = array();
											$response['status'] = "api-key-already-shown";
											$response['status_description'] = "API Key Shown already";
										   
									    }
													     
									} else {
									
										$response['data'] = array();
										$response['status'] = "company-api-key-details-fetching-error";
										$response['status_description'] = "There is an error, when fetching company api key details.";
										
										$eventLog->log("There is an error, when fetching api key details.");
													
									}			
								} catch (Exception $e) {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "company-api-key-fetching-error";
									$response['status_description'] = "There is an error, when fetching company api key details.";
									
									$eventLog->log("There is an error, when fetching company api key details.");	
									
								}
					   
						
					
					
						
				}//close of else of if ($email_id_input == "") {
				
			}
			
		//}	
			
	} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	}
	
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	

	
function company_basic_details_check_based_on_company_client_id($company_id_input) {

	global $dbcon;
	$constructed_array = array();
	$is_active_status = "1";
	
	$candidate_preferred_job_location_record_duplicate_check_sql = "SELECT * FROM `company_rel_api_keys` WHERE `company_id` = :company_id AND `is_active_status` = :is_active_status";

	$candidate_preferred_job_location_record_duplicate_check_q = $dbcon->prepare($candidate_preferred_job_location_record_duplicate_check_sql);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":company_id",$company_id_input);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":is_active_status",$is_active_status);

    
	$candidate_preferred_job_location_record_duplicate_check_q->execute();
  
	if($candidate_preferred_job_location_record_duplicate_check_q->rowCount() > 0) {
		$candidate_preferred_job_location_record_duplicate_check_q_result = $candidate_preferred_job_location_record_duplicate_check_q->fetch();
		
		/* foreach ($candidate_preferred_job_location_record_duplicate_check_q_result as $candidate_preferred_job_location_record_duplicate_check_q_result_row) {

			$temp_array=array();
			$temp_array["is_shown_in_panel"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["is_shown_in_panel"];
			$is_shown_in_panel_input=$temp_array["is_shown_in_panel"];
			
			$constructed_array=$temp_array;
		} */

	     return $candidate_preferred_job_location_record_duplicate_check_q_result;
	    

	} //close of if($candidate_preferred_location_record_duplicate_check_q_result->rowCount() > 0) {
	return $constructed_array;


}
function C_update_based_on_company_id_and_is_active_status($company_id_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch) {

 global $dbcon,$eventLog;
       $is_shown_in_panel_input="1";

    $candidate_additional_information_update_sql = "UPDATE `company_rel_api_keys` SET `is_shown_in_panel`=:is_shown_in_panel,`last_updated_datetime`=:last_updated_datetime,`last_updated_datetime_epoch`=:last_updated_datetime_epoch,`last_updated_by_sm_memb_id`=:last_updated_by_sm_memb_id WHERE `company_id` =:company_id";

	
	$candidate_additional_information_update_q = $dbcon->prepare($candidate_additional_information_update_sql);
	
	$candidate_additional_information_update_q->bindValue(":company_id",$company_id_input);

	$candidate_additional_information_update_q->bindValue(":is_shown_in_panel",$is_shown_in_panel_input);

	
	$candidate_additional_information_update_q->bindValue(":last_updated_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$candidate_additional_information_update_q->bindValue(":last_updated_datetime",$event_datetime);
	$candidate_additional_information_update_q->bindValue(":last_updated_datetime_epoch",$current_epoch);

		
	if ($candidate_additional_information_update_q->execute()) {

			return true;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return false;
	    }

}
function company_details_check_based_on_company_client_id($company_id_input) {

	global $dbcon;
	$constructed_array = array();
	$is_active_status = "1";
	$is_shown_in_panel_input="1";
	
	$candidate_preferred_job_location_record_duplicate_check_sql = "SELECT * FROM `company_rel_api_keys` WHERE `company_id` = :company_id AND `is_active_status` = :is_active_status AND `is_shown_in_panel` = :is_shown_in_panel";

	$candidate_preferred_job_location_record_duplicate_check_q = $dbcon->prepare($candidate_preferred_job_location_record_duplicate_check_sql);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":company_id",$company_id_input);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":is_active_status",$is_active_status);

    $candidate_preferred_job_location_record_duplicate_check_q->bindValue(":is_shown_in_panel",$is_shown_in_panel_input);

    
    
	$candidate_preferred_job_location_record_duplicate_check_q->execute();
  
	if($candidate_preferred_job_location_record_duplicate_check_q->rowCount() > 0) {
		$candidate_preferred_job_location_record_duplicate_check_q_result = $candidate_preferred_job_location_record_duplicate_check_q->fetchAll();
		foreach ($candidate_preferred_job_location_record_duplicate_check_q_result as $candidate_preferred_job_location_record_duplicate_check_q_result_row) {

			$temp_array=array();
			
			$temp_array["company_rel_api_id"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["company_rel_api_id"];
			$temp_array["company_id"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["company_id"];
			$temp_array["api_key"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["api_key"];
			$temp_array["api_secret"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["api_secret"];
			$temp_array["is_shown_in_panel"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["is_shown_in_panel"];
			$temp_array["added_datetime"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["added_datetime"];
			$temp_array["added_datetime_epoch"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["added_datetime_epoch"];
			$temp_array["added_by_sm_memb_id"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["added_by_sm_memb_id"];
			$temp_array["last_updated_datetime"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["last_updated_datetime"];
			$temp_array["last_updated_datetime_epoch"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["last_updated_datetime_epoch"];
			$temp_array["last_updated_by_sm_memb_id"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["last_updated_by_sm_memb_id"];
			$temp_array["is_active_status"]=$candidate_preferred_job_location_record_duplicate_check_q_result_row["is_active_status"];
			$constructed_array=$temp_array;
		}

	     return $constructed_array;
	    

	} else {

           return "no one is shown in panel";
	}//close of if($candidate_preferred_location_record_duplicate_check_q_result->rowCount() > 0) {
	return $constructed_array;


}


exit;
?>


