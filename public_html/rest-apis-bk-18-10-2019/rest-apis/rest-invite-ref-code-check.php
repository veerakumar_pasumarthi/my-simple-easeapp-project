<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * REST API for Checking the Status of Password Setup Request Ref Code
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
 
if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "4")) {
				
		//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		if (is_array($ea_received_rest_ws_raw_array_input)) {
				$content = "";
				
			if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
					$content .= $ea_received_rest_ws_raw_array_input['company_id'];
			}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
			if (isset($ea_received_rest_ws_raw_array_input['job_id'])) {
					$content .= $ea_received_rest_ws_raw_array_input['job_id'];
			}//close of if (isset($ea_received_rest_ws_raw_array_input['job_id'])) {
			if (isset($ea_received_rest_ws_raw_array_input['invite_ref_code'])) {
					$content .= $ea_received_rest_ws_raw_array_input['invite_ref_code'];
			}//close of if (isset($ea_received_rest_ws_raw_array_input['invite_ref_code'])) {	
				
			if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
					$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
			}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
				
				$eventLog->log("Received Inputs => ".$content);
				
				//DO WRITE REST WEB SERVICE AUTHORIZATION CHECK, for ALL REST WEB SERVICES, IN HERE.
				
			//close of if ($ea_received_rest_ws_raw_array_input != "") {
		}	
		//Filter Inputs	
			$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_STRING) : '');
			$job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_STRING) : '');
			$invite_ref_code_input = trim(isset($ea_received_rest_ws_raw_array_input['invite_ref_code']) ? filter_var($ea_received_rest_ws_raw_array_input['invite_ref_code'], FILTER_SANITIZE_STRING) : '');
			
			//Check if the IP Address Input is a Valid IPv4 Address
			if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
				//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
				$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
			} else {
				$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
				$ip_address_input = '';
			}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
		
		
		//Check if all inputs are received correctly from the REST Web Service
			if ($company_id_input == "") {
				//Invalid Password Request Code scenario
				
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "missing-company-id";
				$response['status_description'] = "Missing company id.";
				
				$eventLog->log("Missing company id.");
				
			}  else if ($job_id_input == "") {
				//Invalid Password Request Code scenario
				
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "missing-job-id";
				$response['status_description'] = "Missing job id.";
				
				$eventLog->log("Missing job id.");
				
			} else if ($invite_ref_code_input == "") {
				//Invalid Password Request Code scenario
				
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "missing-invite-ref-code";
				$response['status_description'] = "Missing invite ref code.";
				
				$eventLog->log("Missing invite ref code.");
				
			} else if ($ip_address_input == "") {
				//One or More Inputs are Missing!!!
				
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "missing-additional-information";
				$response['status_description'] = "Some Additional Information like Password and / or IP Address (IPv4) is missing, please check and try again.";
				
				//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application
				header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
				
				$eventLog->log("Please provide all information.");
						  
			} else {	
				//All inputs are Valid
				
				//Event Time, as per Indian Standard Time
				$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
										
										
				$eventLog->log("All inputs are valid.");
				//$eventLog->log("Received Inputs - " . $content);
				
					
					//Get Details of Password Request Code
					$invite_ref_code_details_result = get_invite_ref_code_details($company_id_input,$job_id_input,$invite_ref_code_input);
					if (count($invite_ref_code_details_result) > 0) {
						$job_applied_by_user_id_input = $invite_ref_code_details_result["invitation_received_by_user_id"];
					} else {
						$job_applied_by_user_id_input = "";
					}
					
					if($job_applied_by_user_id_input != "") {
						$job_applicant_request_info_duplicate_check_result = job_applicant_request_info_duplicate_check_based_on_job_id($job_id_input,$job_applied_by_user_id_input);
					}
					
					
					if (count($invite_ref_code_details_result) > 0) {
						
						if (count($job_applicant_request_info_duplicate_check_result) == 0) {
								
								
								$is_active_status = $invite_ref_code_details_result["is_active_status"];
								
								//Check the request status
									if ($is_active_status == "1") {
							
										  
											 $invitation_received_by_user_id = $invite_ref_code_details_result["invitation_received_by_user_id"];
											 
											 
										
											$response['data'] = $invitation_received_by_user_id;
											$response['status'] = "invite-ref-code-is-valid";
											$response['status_description'] = "This invite ref code is valid.";
											
											$eventLog->log("This invite ref code is valid.");
											
										
										
									
									} else if ($is_active_status == "2") {
										//Password Request is Fulfilled
										
										//Construct Content, that will be sent in Response body, of the REST Web Service
										$response['data'] = array();
										$response['status'] = "invite-has-expired-for-this-refcode";
										$response['status_description'] = "this invite has expired for this refcode.";
										
										$eventLog->log("this invite has expired.");
									
									} else if ($is_active_status == "0") {
										//Password Request got Expired
										
										//Construct Content, that will be sent in Response body, of the REST Web Service
										$response['data'] = array();
										$response['status'] = "invite-has-disabled-for-this-refcode";
										$response['status_description'] = "this invite has disabled for this refcode.";
										$eventLog->log("this invite has disabled for this refcode.");
										
									} else {
										//Invalid Password Request Status received
										
										//Construct Content, that will be sent in Response body, of the REST Web Service
										$response['data'] = array();
										$response['status'] = "invalid-invite-ref-code";
										$response['status_description'] = "Invalid invite ref code.";
										
										$eventLog->log("Invalid invite ref code.");
									
									}//close of else of if ($is_active_status == "1") {
										
										
							} else {
								
								$response['data'] = array();
								$response['status'] = "candidate-already-filled-requested-data";
								$response['status_description'] = "Candidate already filled requested Data.";
								
								$eventLog->log("Invalid invite ref code.");
						
								
							}
									
					} else {
						$response['data'] = array();
						$response['status'] = "no-invite-ref-code-found";
						$response['status_description'] = "No invite ref code found in database.";
						
						$eventLog->log("No invite ref code found in database.");
						
					}
			}	
					
			//close of else of if ($user_unique_identifier_string_setting == "") {
		}//close of if ($ea_maintenance_mode == false) {
		
	} else {
		
		//Construct Content, that will be sent in Response body, of the REST Web Service
		$response['data'] = array();
		$response['status'] = "invalid-input";
		$response['status_description'] = "Invalid Input, Please check and provide all information.";
		//$response['jwt-audience'] = array();
		
		//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
	}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "2")) {

	//Check if Maintenance Mode is Turned On
	if ($ea_maintenance_mode) {	
		
		//Define Response Header, with Maintenance Status and corresponding Wait time information, back to the Client Application
		//header('Maintenance-Progress: true', false);
		//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
		$response['data'] = array();
		$response['status'] = "application-maintenance-in-progress";
		$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
		
	} else {	

		//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
		header('Content-Type: application/json');
		echo json_encode($response,JSON_PRETTY_PRINT);
		
	}//close of else of if ($ea_maintenance_mode){
function get_invite_ref_code_details($company_id_input,$job_id_input,$invite_ref_code_input) {
		
		global $dbcon;
		
		$constructed_array = array();
		
		$invite_request_details_get_sql = "SELECT * FROM  `job_applicant_invites` WHERE `invite_ref_code` =:invite_ref_code AND `company_id`=:company_id AND `job_id`=:job_id";	
		$invite_request_details_get_select_query = $dbcon->prepare($invite_request_details_get_sql);		
		$invite_request_details_get_select_query->bindValue("company_id",$company_id_input);
		$invite_request_details_get_select_query->bindValue("job_id",$job_id_input);
		$invite_request_details_get_select_query->bindValue("invite_ref_code",$invite_ref_code_input);
		$invite_request_details_get_select_query->execute();
		if($invite_request_details_get_select_query->rowCount() > 0) {
			$invite_request_details_get_select_query_result = $invite_request_details_get_select_query->fetch();
			
			
			return $invite_request_details_get_select_query_result;
			
		}//close of if($user_account_password_request_details_get_select_query->rowCount() > 0) {
		return $constructed_array;
}
function job_applicant_request_info_duplicate_check_based_on_job_id($job_id_input,$job_applied_by_user_id_input){

	global $dbcon;
	$constructed_array = array();
	$is_active_status = '1';
	
	$job_applicant_request_info_duplicate_check_sql = "SELECT * FROM `job_applicant_requests_info` WHERE `job_id`=:job_id AND `job_applied_by_user_id`=:job_applied_by_user_id";

	$job_applicant_request_info_select_query = $dbcon->prepare($job_applicant_request_info_duplicate_check_sql);

	$job_applicant_request_info_select_query->bindValue(":job_id",$job_id_input);
	$job_applicant_request_info_select_query->bindValue(":job_applied_by_user_id",$job_applied_by_user_id_input);
	
	$job_applicant_request_info_select_query->execute();
	if($job_applicant_request_info_select_query->rowCount() > 0) {
		$job_applicant_request_info_select_query_result = $job_applicant_request_info_select_query->fetch();
	     return $job_applicant_request_info_select_query_result;

	}//close of if($company_details_select_query_result->rowCount() > 0) {
	return $constructed_array;

}
exit;
?>