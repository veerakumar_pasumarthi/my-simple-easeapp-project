<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Edit Quick Admin Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['user_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['user_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['user_id'])) 
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id']))
				
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$eventLog->log("company_id=>".$company_id_input);
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				
				if ($user_id_input == "") {
					//Invalid Mobile Number scenario
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-user-id";
					$response['status_description'] = "invalid user id, please check and try again.";
					
					$eventLog->log("Please provide a valid user id.");
					
				
				
				} else if ($company_id_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "missing company id, please check and try again.";
					
					$eventLog->log("Please provide all information.");
				}else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Some Additional Information is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//Event Time, as per Indian Standard Time
					$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
		
					$company_proceed_next_step = "";
					
					
					if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$company_proceed_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$company_proceed_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
					} else {
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
					}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
							
					if ($company_proceed_next_step == "PROCEED-TO-NEXT-STEP") {
						
							try {
								    $candidate_basic_details_result = candidate_basic_details_check_based_on_user_id($user_id_input,$company_id_input);
								    if(count($candidate_basic_details_result) > 0){
										  
										$response['data'] = $candidate_basic_details_result;
										$response['status'] = "candidate-details-successfully-fetched";
										$response['status_description'] = "candidate details fetched Successfully.";

										$eventLog->log("The candidate details fetched Successfully.");
													     
									} else {
									
										$response['data'] = array();
										$response['status'] = "no-records-found-with-this-id";
										$response['status_description'] = "no records found with user id.";
										
										$eventLog->log("no records found with user id.");
													
									}			
								} catch (Exception $e) {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "candidate-details-fetching-error";
									$response['status_description'] = "There is an error, when fetching candidate details.";
									
									$eventLog->log("There is an error, when fetching candidate details.");	
									
								}
					   
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "The User Account, that is attempted to be edited, is invalid, please check and try again.";
						
						$eventLog->log("Please provide a Valid user ID.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
					
						
				}//close of else of if ($email_id_input == "") {
				
			}
			
		}	
			
	} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	

	
function candidate_basic_details_check_based_on_user_id($user_id_input,$company_id_input) {

	global $dbcon, $eventLog;
	$constructed_array = array();
    $eventLog->log("before enter into function");
	$candidate_basic_details_get_sql = "SELECT * FROM site_members s LEFT JOIN candidate_rel_additional_info crai ON crai.sm_memb_id = s.sm_memb_id WHERE s.company_id =:company_id AND s.sm_memb_id =:sm_memb_id";
	  $eventLog->log("after enter into query");
	$candidate_basic_details_select_query = $dbcon->prepare($candidate_basic_details_get_sql);
    $candidate_basic_details_select_query->bindValue(":sm_memb_id",$user_id_input);
	 $eventLog->log("user_id_input=>".$user_id_input);
	$candidate_basic_details_select_query->bindValue(":company_id",$company_id_input);
	
 $eventLog->log("company_id_input=>".$company_id_input);
  
	$candidate_basic_details_select_query->execute();
  $eventLog->log("after excute");
	if($candidate_basic_details_select_query->rowCount() > 0) {
		$eventLog->log("in if condition");
	    $candidate_basic_details_select_query_result = $candidate_basic_details_select_query->fetchAll();
		
		foreach ($candidate_basic_details_select_query_result as $candidate_basic_details_select_query_result_row) {
			
		    $constructed_array["sm_memb_id"] = $candidate_basic_details_select_query_result_row["sm_memb_id"];
		    $constructed_array["sm_mobile"] = $candidate_basic_details_select_query_result_row["sm_mobile"];
			$constructed_array["sm_mobile_phone_country_code"] = $candidate_basic_details_select_query_result_row["sm_mobile_phone_country_code"];
			$constructed_array["sm_work_phone_country_code"] = $candidate_basic_details_select_query_result_row["sm_work_phone_country_code"];
			$constructed_array["sm_work_phone"] = $candidate_basic_details_select_query_result_row["sm_work_phone"];
			
			$constructed_array["sm_home_phone"] = $candidate_basic_details_select_query_result_row["sm_home_phone"];
			$constructed_array["sm_home_phone_country_code"] = $candidate_basic_details_select_query_result_row["sm_home_phone_country_code"];
			$constructed_array["sm_fax_number_country_code"] = $candidate_basic_details_select_query_result_row["sm_fax_number_country_code"];
			$constructed_array["sm_fax_number"] = $candidate_basic_details_select_query_result_row["sm_fax_number"];
			$constructed_array["sm_alternate_email"] = $candidate_basic_details_select_query_result_row["sm_alternate_email"];
		    $constructed_array["sm_salutation"] = $candidate_basic_details_select_query_result_row["sm_salutation"];
			$constructed_array["sm_firstname"] = $candidate_basic_details_select_query_result_row["sm_firstname"];
			$constructed_array["sm_middlename"] = $candidate_basic_details_select_query_result_row["sm_middlename"];
			$constructed_array["sm_lastname"] = $candidate_basic_details_select_query_result_row["sm_lastname"];
			$constructed_array["sm_address"] = $candidate_basic_details_select_query_result_row["sm_address"];
			$constructed_array["sm_city"] = $candidate_basic_details_select_query_result_row["sm_city"];
			$constructed_array["sm_state"] = $candidate_basic_details_select_query_result_row["sm_state"];
			$constructed_array["sm_country"] = $candidate_basic_details_select_query_result_row["sm_country"];
			$constructed_array["sm_zipcode"] = $candidate_basic_details_select_query_result_row["sm_zipcode"];
		    $constructed_array["company_id"] = $candidate_basic_details_select_query_result_row["company_id"];
			$constructed_array["sm_email"] = $candidate_basic_details_select_query_result_row["sm_email"];
			
			$candidate_internal_notes_details = candidate_internal_notes($user_id_input,$company_id_input);
			if(count($candidate_internal_notes_details)>0)
			{
				$constructed_array["notes"]= $candidate_internal_notes_details["notes"];
			} else {
				$constructed_array["notes"]= "";
				
			}
			
			/* $constructed_array["notes"]= $candidate_internal_notes_details["notes"];
			$notes = $constructed_array["notes"];
			
			if($notes!= ""){
				$constructed_array["notes"] = $notes;
			}
			else
			{
				$constructed_array["notes"] = "";
			}  */
			$constructed_array["profile_source_id"] = $candidate_basic_details_select_query_result_row["profile_source_id"];
			$constructed_array["profile_source_name"] = $candidate_basic_details_select_query_result_row["profile_source_name"];
			$constructed_array["profile_source_seo_name"] = $candidate_basic_details_select_query_result_row["profile_source_seo_name"];
			$constructed_array["sm_user_status"] = $candidate_basic_details_select_query_result_row["sm_user_status"];
	    }

		return $constructed_array;
	}
	return $constructed_array;

}

function candidate_internal_notes($user_id_input,$company_id_input) {
	global $dbcon;
	$constructed_array = array();
	$candidate_details_get_sql = "SELECT * FROM `candidate_rel_internal_notes` WHERE  `candidate_id` =:candidate_id AND `company_id`=:company_id";
	$candidate_details_select_query = $dbcon->prepare($candidate_details_get_sql);
	$candidate_details_select_query->bindValue(":candidate_id",$user_id_input);
	$candidate_details_select_query->bindValue(":company_id",$company_id_input);
	$candidate_details_select_query->execute();

	if($candidate_details_select_query->rowCount() > 0) {
		$candidate_details_select_query_result = $candidate_details_select_query->fetch();
	     return $candidate_details_select_query_result;

	}//close of if($company_details_check_select_query->rowCount() > 0) {
	return $constructed_array;
	}

exit;
?>


