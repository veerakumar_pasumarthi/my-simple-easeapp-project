<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_name'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_name'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_name'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['company_brand_name'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_brand_name'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_brand_name'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['company_registration_type'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_registration_type'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_registration_type'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['company_mobile_number'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_mobile_number'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_mobile_number'])) 
					if (isset($ea_received_rest_ws_raw_array_input['company_mobile_number_country_code'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_mobile_number_country_code'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_mobile_number_country_code'])) 		
					
					if (isset($ea_received_rest_ws_raw_array_input['company_website_url'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_website_url'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_website_url'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['company_email'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_email'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_email'])) 	
						
					if (isset($ea_received_rest_ws_raw_array_input['company_support_email'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_support_email'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_support_email'])) 
                    
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$company_name_input = trim(isset($ea_received_rest_ws_raw_array_input['company_name']) ? filter_var($ea_received_rest_ws_raw_array_input['company_name'], FILTER_SANITIZE_STRING) : '');
				
				$company_brand_name_input = trim(isset($ea_received_rest_ws_raw_array_input['company_brand_name']) ? filter_var($ea_received_rest_ws_raw_array_input['company_brand_name'], FILTER_SANITIZE_STRING) : '');
				
				$company_registration_type_input = trim(isset($ea_received_rest_ws_raw_array_input['company_registration_type']) ? filter_var($ea_received_rest_ws_raw_array_input['company_registration_type'], FILTER_SANITIZE_STRING) : '');
				if($company_registration_type_input ==""){
					$company_registration_type_input =null;
				}
				
				$eventLog->log("company_registration_type_input => ".$company_registration_type_input);
				
				$company_registration_type_input_exploded = explode(":::::",$company_registration_type_input);
				
				$company_registration_type_input_exploded_count = count($company_registration_type_input_exploded);
				
				$company_registration_type_id_input = "";
					
				$company_registration_type_name_input = "";
				
				$company_registration_type_seo_name_input = "";
				
				if ($company_registration_type_input_exploded_count == "3") {
					
					$company_registration_type_id_input = trim(isset($company_registration_type_input_exploded[0]) ? filter_var($company_registration_type_input_exploded[0], FILTER_SANITIZE_NUMBER_INT) : '');
					
					$company_registration_type_name_input = trim(isset($company_registration_type_input_exploded[1]) ? filter_var($company_registration_type_input_exploded[1], FILTER_SANITIZE_STRING) : '');
					
					$company_registration_type_seo_name_input = trim(isset($company_registration_type_input_exploded[2]) ? filter_var($company_registration_type_input_exploded[2], FILTER_SANITIZE_STRING) : '');
					
				}//close of if ($company_registration_type_input_exploded_count != "3") {
				
				
				/*$company_registration_type_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_registration_type_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_registration_type_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$company_registration_type_name_input = trim(isset($ea_received_rest_ws_raw_array_input['company_registration_type_name']) ? filter_var($ea_received_rest_ws_raw_array_input['company_registration_type_name'], FILTER_SANITIZE_STRING) : '');
				$company_registration_type_seo_name_input = trim(isset($ea_received_rest_ws_raw_array_input['company_registration_type_seo_name']) ? filter_var($ea_received_rest_ws_raw_array_input['company_registration_type_seo_name'], FILTER_SANITIZE_STRING) : '');*/
				
				
				$company_mobile_number_input = trim(isset($ea_received_rest_ws_raw_array_input['company_mobile_number']) ? filter_var($ea_received_rest_ws_raw_array_input['company_mobile_number'], FILTER_SANITIZE_STRING) : '');
				$company_mobile_number_country_code_input = trim(isset($ea_received_rest_ws_raw_array_input['company_mobile_number_country_code']) ? filter_var($ea_received_rest_ws_raw_array_input['company_mobile_number_country_code'], FILTER_SANITIZE_STRING) : '');
				
				$company_email_input = trim(isset($ea_received_rest_ws_raw_array_input['company_email']) ? filter_var($ea_received_rest_ws_raw_array_input['company_email'], FILTER_SANITIZE_EMAIL) : '');
				$company_support_email_input = trim(isset($ea_received_rest_ws_raw_array_input['company_support_email']) ? filter_var($ea_received_rest_ws_raw_array_input['company_support_email'], FILTER_SANITIZE_EMAIL) : '');
				$company_premise_name_input = trim(isset($ea_received_rest_ws_raw_array_input['company_premise_name']) ? filter_var($ea_received_rest_ws_raw_array_input['company_premise_name'], FILTER_SANITIZE_STRING) : '');
				
				$company_website_url_input = trim(isset($ea_received_rest_ws_raw_array_input['company_website_url']) ? filter_var($ea_received_rest_ws_raw_array_input['company_website_url'], FILTER_SANITIZE_URL) : '');
				/* $company_recruiter_purposed_email_input = trim(isset($ea_received_rest_ws_raw_array_input['company_recruiter_purposed_email']) ? filter_var($ea_received_rest_ws_raw_array_input['company_recruiter_purposed_email'], FILTER_SANITIZE_EMAIL) : ''); */
				
				if (!filter_var($company_website_url_input, FILTER_VALIDATE_URL) == true) {
					
					$eventLog->log($company_website_url_input . " - Not a Valid url ");
					$company_website_url_input = null;
					
				} 
				
			    if ($company_mobile_number_input == "") {
					$company_mobile_number_input = null;
				}
				
				
			    if ($company_registration_type_id_input== "") {
					$company_registration_type_id_input = null;
				}
				
				if ($company_registration_type_name_input == "") {
					$company_registration_type_name_input = null;
				}
				
				if ($company_registration_type_seo_name_input == "") {
					$company_registration_type_seo_name_input = null;
				}
				
				
				if (!filter_var($company_email_input, FILTER_VALIDATE_EMAIL) == true) {
					
					$eventLog->log($company_email_input . " - Not a Valid Email Address");
					$company_email_input = "";
					
				} else if (!filter_var($company_support_email_input, FILTER_VALIDATE_EMAIL) == true) {
					
					$eventLog->log($company_support_email_input . " - Not a Valid Email Address");
					$company_support_email_input = "";
				}/*  else if (!filter_var($company_recruiter_purposed_email_input, FILTER_VALIDATE_EMAIL) == true) {
					
					$eventLog->log($company_recruiter_purposed_email_input . " - Not a Valid Email Address");
					$company_recruiter_purposed_email_input = "";
					
				}  */
				
				 		
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				if ($company_name_input == "") {
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-name";
					$response['status_description'] = "missing company name";
					
					$eventLog->log("missing-company-name: Please provide a valid Company Name.");
					
				} else if ($company_email_input == "") {
					//Invalid Mobile Number scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-email";
					$response['status_description'] = "Missing company email.";
					
					$eventLog->log("missing-company-email: Please provide a valid Company Email.");
					
				} else if ($company_brand_name_input == "") {
					//Invalid Salutation scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-brand-name";
					$response['status_description'] = "Missing company brand name.";
					
					$eventLog->log("missing-company-brand-name: Please provide a valid Company Brand Name.");
				/*} else if ($company_recruiter_purposed_email_input == "") {
					//Invalid Salutation scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-recruiter-purposed-email";
					$response['status_description'] = "Missing Company recruiter purposed email ";
					
					$eventLog->log("missing-company-recruiter-purposed-email: Please provide a valid company recruiter purposed email.");	
				
				}  else if ($company_registration_type_input_exploded_count != "3") {
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-company-registration-type-info";
					$response['status_description'] = "Invalid Company Registration Type Info provided, Please check and try again.";
					
					$eventLog->log("invalid-company-registration-type-info: Invalid Company Registration Type Info provided, Please check and try again.");
					*/
					
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					$company_duplicate_check_result = company_details_duplicate_check_based_on_company_name($company_name_input);
					if (count($company_duplicate_check_result) > 0) {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "company-already-exists";
						$response['status_description'] = "Company name already exists.";
						
                        $eventLog->log("company-already-exists: company name already exists");						
					
					} else {
						
						//Event Time, as per Indian Standard Time
						$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
						$event_expiry_datetime_epoch = $current_epoch+$password_resetting_link_expiry_period;	
						//$valid_from_date_input = '2018-11-10 16:18:20';
						
						
						$candidate_add_next_step = "";
						
						if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$candidate_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$candidate_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
						} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient set of permissions.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
						}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
							
						if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							try {
								
								/*COMMENTED on 05-04-2019 20:08
								//Check if the User, whose sm_memb_id is received from "sub" of JWT Auth Token exists
								if (count($ea_token_based_sub_user_id_details) > 0) {
									
									//$eventLog->log($ea_token_based_sub_user_id_details["sm_lastname"]);
														
									$last_inserted_id = company_basic_details_insert($company_name_input,$company_seo_name_input,$company_brand_name_input,$company_registration_type_id_input,$company_registration_type_name_input,$company_registration_type_seo_name_input,$company_mobile_number_input,$company_website_url_input, $company_email_input, $company_support_email_input); 
									
								} else {
									$last_inserted_id = company_basic_details_insert($company_name_input,$company_seo_name_input,$company_brand_name_input,$company_registration_type_id_input,$company_registration_type_name_input,$company_registration_type_seo_name_input,$company_mobile_number_input,$company_website_url_input,$company_email_input,$company_support_email_input);
								    
								}//close of else of if (count($ea_token_based_sub_user_id_details) > 0) {
								*/
								$eventLog->log("before insert query");
								$company_seo_name_input=strtolower_utf8_extended(create_seo_name($company_name_input));
								$last_inserted_id = company_basic_details_insert($company_name_input,$company_seo_name_input,$company_brand_name_input,$company_registration_type_id_input,$company_registration_type_name_input,$company_registration_type_seo_name_input,$company_mobile_number_input,$company_website_url_input,$company_email_input,$company_support_email_input,$company_mobile_number_country_code_input,$event_datetime,$current_epoch);
								   $eventLog->log("after insert query"); 
								
								$eventLog->log($last_inserted_id);
								
									
									if ($last_inserted_id != "") {
										
										
										$client_last_inserted_id = client_company_basic_details_insert($last_inserted_id,$company_name_input, $company_seo_name_input, $company_brand_name_input, $company_registration_type_id_input,$company_registration_type_name_input,$company_registration_type_seo_name_input,$company_mobile_number_input,$company_mobile_number_country_code_input,$company_website_url_input, $company_email_input, $company_support_email_input,$event_datetime,$current_epoch);
										
										
										if($client_last_inserted_id != "") {
										    $response['data'] = array();
											$response['status'] = "company-insertion-successful";
											$response['status_description'] = "Submitted successfully.";
										
											$eventLog->log("company-insert-successful: A Company is added Successfully.");   
											
										} else {
											
											$response['data'] = array();
											$response['status'] = "company-insertion-error";
											$response['status_description'] = "No records found.";
											
											$eventLog->log("company-insert-error: There is an error, when adding the Company in the Database.");
												
											
										}
										
									} else {
										//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
										$response['data'] = array();
										$response['status'] = "company-insertion-error";
										$response['status_description'] = "No records found.";
										
										$eventLog->log("company-insert-error: There is an error, when adding the Company in the Database.");
										
									}
									
								
									
							         
									
								
							} catch (Exception $e) {
								
								//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
								$response['data'] = array();
								$response['status'] = "company-insertion-error";
								$response['status_description'] = "No records found.";
								
								$eventLog->log("company-insertion-error: There is an error, when adding the Company in the Database.");
							}
						}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							
						
					}
				}//close of else of if ($email_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
    
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
			
	
	}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){

exit;
?>