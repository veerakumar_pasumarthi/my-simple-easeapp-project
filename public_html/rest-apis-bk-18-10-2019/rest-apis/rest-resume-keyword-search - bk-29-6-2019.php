<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$search_criteria_input = trim(isset($ea_received_rest_ws_raw_array_input['search_criteria']) ? filter_var($ea_received_rest_ws_raw_array_input['search_criteria'], FILTER_SANITIZE_STRING) : '');
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {

                if ($search_criteria_input == "") {
					//Invalid company Status scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-search-criteria-input";
					$response['status_description'] = "Missing Search Criteria Input";
					
					$eventLog->log("Please provide a valid search criteria info.");
				
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else {
					//all inputs are valid
					
					try{
						
					   
						
					       $candidate_list_info_result = array();
						   
					    if ($ea_extracted_jwt_token_user_company_id == "") {
						    //action taker is of platform scope - super admin / site admin scenario
						    $candidates_resumes_keyword_search_info_result = candidates_resumes_keyword_search_info($company_id_input,$search_criteria_input);
					
					    } else {
						   //action taker is of company scope - company admin, legal / immigration, internal admin etc scenario
						    $candidates_resumes_keyword_search_info_result = candidates_resumes_keyword_search_info($ea_extracted_jwt_token_user_company_id,$search_criteria_input);
					       
					    }//close of else of if ($ea_extracted_jwt_token_user_company_id == "") {
					    //$candidate_list_info = get_candidate_list($company_id_input, "member");
						$candidates_resumes_keyword_search_info_result_count = count($candidates_resumes_keyword_search_info_result);
						
						$eventLog->log("Count -> " . $candidates_resumes_keyword_search_info_result_count);
						
					    if ($candidates_resumes_keyword_search_info_result_count > "0") {
					       $response['data'] = $candidates_resumes_keyword_search_info_result;
					       $response['status'] = "candidates-resumes-received";
					       $response['status_description'] = "Candidates resumes is Successfully Received";
							
					     $candidate_list_info_json_encoded = json_encode($candidates_resumes_keyword_search_info_result);
						
					       //eventLog->log("Quick User Info -> " . $candidate_list_info_json_encoded); 
					    }else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "no-resumes-found-for-this-search";
							$response['status_description'] = "No Resumes found for this Search criteria, please check and try again.";
							
					    }		
							
				    }catch (Exception $e){
						$eventLog->log("Exception -> " . html_escaped_output($e->getMessage())); 
						//addLog($logFile, "Exception -> ".$e->getMessage());	
					}//close of  catch (Exception $e){	}//close of else of if ($company_list_result_count > "0") {					
                  
				}//close of else of if ($user_type_input != 'admin') {
					
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
	
/*function candidates_resumes_keyword_search_info($company_id_input,$search_criteria_input){
	global $dbcon;
	$constructed_array = array();

	$candidates_resumes_keyword_search_info_sql = "SELECT * FROM `resume_extracted_info` WHERE MATCH(`resume_text`) AGAINST('".$search_criteria_input."' IN NATURAL LANGUAGE MODE)";
	$candidates_resumes_keyword_search_info_query = $dbcon->prepare($candidates_resumes_keyword_search_info_sql);
	//$candidates_resumes_keyword_search_info_query->bindValue(":search_criteria_input",$search_criteria_input);
    //$candidates_resumes_keyword_search_info_query->bindValue(":sm_user_status","4");
	$candidates_resumes_keyword_search_info_query->execute();

	if($candidates_resumes_keyword_search_info_query->rowCount() > 0) {
		$candidates_resumes_keyword_search_info_query_result = $candidates_resumes_keyword_search_info_query->fetchAll();
	     return $candidates_resumes_keyword_search_info_query_result;

	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;

}*/

function candidates_resumes_keyword_search_info($company_id_input,$search_criteria_input){
	global $dbcon, $ea_extracted_jwt_token_sub, $current_epoch, $expiring_link_lifetime, $expiring_link_secret_key, $expiring_link_hash_algorithm, $site_hostname_value;
	$constructed_array = array();

	$candidates_resumes_keyword_search_info_sql = "SELECT * FROM `resume_extracted_info` WHERE MATCH(`resume_text`) AGAINST('".$search_criteria_input."' IN NATURAL LANGUAGE MODE)";
	$candidates_resumes_keyword_search_info_query = $dbcon->prepare($candidates_resumes_keyword_search_info_sql);
	//$candidates_resumes_keyword_search_info_query->bindValue(":search_criteria_input",$search_criteria_input);
    //$candidates_resumes_keyword_search_info_query->bindValue(":sm_user_status","4");
	$candidates_resumes_keyword_search_info_query->execute();

	if($candidates_resumes_keyword_search_info_query->rowCount() > 0) {
		$candidates_resumes_keyword_search_info_query_result = $candidates_resumes_keyword_search_info_query->fetchAll();

		foreach ($candidates_resumes_keyword_search_info_query_result as $candidates_resumes_keyword_search_info_query_result_row) {

			$temp_row_array = array();
		    $temp_row_array["resume_extracted_info_id"] = $candidates_resumes_keyword_search_info_query_result_row["resume_extracted_info_id"];
		    $temp_row_array["company_id"] = $candidates_resumes_keyword_search_info_query_result_row["company_id"];
			$temp_row_array["sm_memb_id"] = $candidates_resumes_keyword_search_info_query_result_row["sm_memb_id"];
		    $temp_row_array["crur_id"] = $candidates_resumes_keyword_search_info_query_result_row["crur_id"];
		    $temp_row_array["sm_salutation"] = $candidates_resumes_keyword_search_info_query_result_row["sm_salutation"];
            $temp_row_array["sm_firstname"] = $candidates_resumes_keyword_search_info_query_result_row["sm_firstname"];
		    $temp_row_array["sm_middlename"] = $candidates_resumes_keyword_search_info_query_result_row["sm_middlename"];
			$temp_row_array["sm_lastname"] = $candidates_resumes_keyword_search_info_query_result_row["sm_lastname"];
			$temp_row_array["sm_email"] = $candidates_resumes_keyword_search_info_query_result_row["sm_email"];
			$temp_row_array["sm_alternate_email"] = $candidates_resumes_keyword_search_info_query_result_row["sm_alternate_email"];
			$temp_row_array["sm_mobile"] = $candidates_resumes_keyword_search_info_query_result_row["sm_mobile"];
			$temp_row_array["sm_city"] = $candidates_resumes_keyword_search_info_query_result_row["sm_city"];
			$temp_row_array["sm_state"] = $candidates_resumes_keyword_search_info_query_result_row["sm_state"];
			$temp_row_array["sm_country"] = $candidates_resumes_keyword_search_info_query_result_row["sm_country"];
			$temp_row_array["sm_job_experience_level"] = $candidates_resumes_keyword_search_info_query_result_row["sm_job_experience_level"];
			$temp_row_array["resume_text"] = $candidates_resumes_keyword_search_info_query_result_row["resume_text"];
			//$replace = '<span style="background-color: #FF0;">' . $search_criteria_input . '</span>'; // create replacement
			//$temp_row_array["content"] = str_replace( $search_criteria_input, $replace, $temp_row_array["resume_text"] );
			//$temp_row_array["content"] = highlightTerms($temp_row_array["resume_text"], $search_criteria_input);
			//Create Uploaded File Link with Signature
			$link_expiry_time = $current_epoch+$expiring_link_lifetime;
			$uploaded_file_link_with_signature_result = create_uploaded_file_link_with_signature($ea_extracted_jwt_token_sub, $temp_row_array["sm_memb_id"], $temp_row_array["crur_id"], "https", $site_hostname_value, $link_expiry_time, $expiring_link_hash_algorithm, $expiring_link_secret_key);
			if (isset($uploaded_file_link_with_signature_result["uploaded_file_url"])) {
			$temp_row_array["uploaded_file_url"] = $uploaded_file_link_with_signature_result["uploaded_file_url"];
				
			} else {
				$temp_row_array["uploaded_file_url"] = null;
			}//close of if (isset($uploaded_file_link_with_signature_result["uploaded_file_url"])) {
				
			$download_file_link_with_signature_result = create_download_file_link_with_signature($ea_extracted_jwt_token_sub, $temp_row_array["sm_memb_id"], $temp_row_array["crur_id"], "https", $site_hostname_value, $link_expiry_time, $expiring_link_hash_algorithm, $expiring_link_secret_key);
			if (isset($download_file_link_with_signature_result["download_file_url"])) {
			$temp_row_array["download_file_url"] = $download_file_link_with_signature_result["download_file_url"];
				
			} else {
				$temp_row_array["download_file_url"] = null;
			}//close of if (isset($download_file_link_with_signature_result["uploaded_file_url"])) {

			
			$constructed_array[] = $temp_row_array;
	    }//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

		return $constructed_array;
	}
	return $constructed_array;
}

function highlightTerms($text_string, $terms){
    //We can loop through the array of terms from string 
    //foreach ($terms as $term)
    //{
         //use preg_quote 
            $term = preg_quote($terms);
           //Now we can  highlight the terms 
            $text_string = preg_replace("/\b($term)\b/i", '<span class="hilitestyle">\1</span>', $text_string);
    //}
          //lastly, return text string with highlighted term in it
          return  $text_string;
}


/*function candidates_resumes_keyword_search_info($company_id_input,$search_criteria_input){
	global $dbcon;
	$constructed_array = array();

	$candidates_resumes_keyword_search_info_sql = "SELECT * FROM `resume_extracted_info` WHERE MATCH(`resume_text`) AGAINST('".$search_criteria_input."' IN NATURAL LANGUAGE MODE)";
	$candidates_resumes_keyword_search_info_query = $dbcon->prepare($candidates_resumes_keyword_search_info_sql);
	//$candidates_resumes_keyword_search_info_query->bindValue(":search_criteria_input",$search_criteria_input);
    //$candidates_resumes_keyword_search_info_query->bindValue(":sm_user_status","4");
	$candidates_resumes_keyword_search_info_query->execute();

	if($candidates_resumes_keyword_search_info_query->rowCount() > 0) {
		$candidates_resumes_keyword_search_info_query_result = $candidates_resumes_keyword_search_info_query->fetchAll();
	     return $candidates_resumes_keyword_search_info_query_result;

	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;

}*/

function getSearchTermToBold($text, $words)
{
    preg_match_all('~[A-Za-z0-9_äöüÄÖÜ]+~', $words, $m);
    if (!$m)
        return $text;
    $re = '~(' . implode('|', $m[0]) . ')~i';
    return preg_replace($re, '<b>$0</b>', $text);
}

function create_uploaded_file_link_with_signature($viewer_user_id_input, $candidate_user_id_input, $uploaded_file_ref_id_input, $uploaded_file_url_protocol_input, $uploaded_file_url_hostname_input, $expiring_link_lifetime_input, $expiring_link_hash_algorithm_input, $expiring_link_secret_key_input) {
	global $dbcon, $eventLog;
	
	$constructed_array = array();
	
	//Use Active JWT Token Details of the Document Viewing User (Admin user / Company User / Candidate thyself)
	$viewing_user_rel_active_jwt_token_details_result = ea_get_user_rel_active_jwt_token_details($viewer_user_id_input);
	
	if (count($viewing_user_rel_active_jwt_token_details_result) > 0) {
		
		foreach($viewing_user_rel_active_jwt_token_details_result as $viewing_user_rel_active_jwt_token_details_result_key => $viewing_user_rel_active_jwt_token_details_result_value) {
				
			//$user_auth_token_id = $viewing_user_rel_active_jwt_token_details_result_row["user_auth_token_id"];
			$user_auth_token_id = $viewing_user_rel_active_jwt_token_details_result_key;
			
			
			$candidate_rel_uploaded_visa_document_details_result = candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id($candidate_user_id_input, $uploaded_file_ref_id_input);
			
			if (count($candidate_rel_uploaded_visa_document_details_result) > 0) {
				
				$generated_filename = $candidate_rel_uploaded_visa_document_details_result["generated_filename"];
				
				
				
				/*//Gathering inputs for Link Signature
				$link_signature_inputs = $viewer_user_id_input . ":::::" . $candidate_user_id_input . ":::::" . $uploaded_file_ref_id_input . ":::::" . $generated_filename . ":::::" . $user_auth_token_id;
				//expiring_links_lifetime
				$link_signature = hash('sha256', $link_signature_inputs);
				*/
				//return $constructed_array["link_signature"] = $link_signature;
				
				//Create Uploaded File URL
				//https://dev-visadoc.securitywonks.net/viewer/1/candidate/11/uploaded-file-ref-id/151/display/link-signature/tff8t76rft8tyfy88y8f8y
				////https://dev-visadoc.securitywonks.net/viewer/11/candidate/11/uploaded-file-ref-id/46/display/expires/1111111/link-signature/bd27845343fdfer3redsfr3regfh6t3rfdggj78453f
				
				//$uploaded_file_url = $uploaded_file_url_protocol_input . "://" . $uploaded_file_url_hostname_input . "/viewer/" . $viewer_user_id_input . "/candidate/" . $candidate_user_id_input . "/uploaded-file-ref-id/" . $uploaded_file_ref_id_input . "/display/expires/" . $expiring_link_lifetime_input . "/link-signature/" . $link_signature;
				
				$uploaded_file_url_without_signature = $uploaded_file_url_protocol_input . "://" . $uploaded_file_url_hostname_input . "/viewer/" . $viewer_user_id_input . "/candidate/" . $candidate_user_id_input . "/uploaded-file-ref-id/" . $uploaded_file_ref_id_input . "/display/expires/" . $expiring_link_lifetime_input;
				
				$eventLog->log("uploaded_file_url (without signature): " . $uploaded_file_url_without_signature);
				
				/*//Creating Link Signature
				$link_signature = hash('sha256', $user_auth_token_id . $uploaded_file_url_without_signature);
				*/
				
				//Base64 Decode the Base64 Encoded Expiring Link related Secret Key
				$expiring_link_rel_secret_base64_encoded = $expiring_link_secret_key_input;
				$expiring_link_rel_secret_base64_decoded = base64_decode($expiring_link_rel_secret_base64_encoded);
				
				//Create Link Signature using sha256
				$created_link_signature = hash_hmac($expiring_link_hash_algorithm_input, $user_auth_token_id . $uploaded_file_url_without_signature, $expiring_link_rel_secret_base64_decoded, true);
				
				//Base64 URL Encode the Created Token Signature
				$created_link_signature_base64_urlencoded = base64url_encode($created_link_signature); //from /app/includes/other-functions-api.php
				
				//remove padding (=), from Base64 URL Encoded Token Signature
				$created_link_signature_base64_urlencoded_after_removing_padding = str_replace("=", "", $created_link_signature_base64_urlencoded);
				
				$uploaded_file_url_with_signature = $uploaded_file_url_without_signature . "/link-signature/" . $created_link_signature_base64_urlencoded_after_removing_padding;
				
				$constructed_array["uploaded_file_url"] = $uploaded_file_url_with_signature;
				
				$eventLog->log("uploaded_file_url (with signature): " . $uploaded_file_url_with_signature);
				
				return $constructed_array;
				
			} else {
				$eventLog->log("Uploaded file ref id is not Valid!!!!");	
			}//close of else of if (count($candidate_rel_uploaded_visa_document_details_result) > 0) {
				
			
			
		}//close of foreach($viewing_user_rel_active_jwt_token_details_result as $viewing_user_rel_active_jwt_token_details_result_key => $viewing_user_rel_active_jwt_token_details_result_value) {
		
	} else {
		$eventLog->log("Active Token Details of Viewing User is not received");
	}//close of else of if (count($viewing_user_rel_active_jwt_token_details_result) > 0) {
	
	return $constructed_array;
}

function create_download_file_link_with_signature($viewer_user_id_input, $candidate_user_id_input, $uploaded_file_ref_id_input, $uploaded_file_url_protocol_input, $uploaded_file_url_hostname_input, $expiring_link_lifetime_input, $expiring_link_hash_algorithm_input, $expiring_link_secret_key_input) {
	global $dbcon, $eventLog;
	
	$constructed_array = array();
	
	//Use Active JWT Token Details of the Document Viewing User (Admin user / Company User / Candidate thyself)
	$viewing_user_rel_active_jwt_token_details_result = ea_get_user_rel_active_jwt_token_details($viewer_user_id_input);
	
	if (count($viewing_user_rel_active_jwt_token_details_result) > 0) {
		
		foreach($viewing_user_rel_active_jwt_token_details_result as $viewing_user_rel_active_jwt_token_details_result_key => $viewing_user_rel_active_jwt_token_details_result_value) {
				
			//$user_auth_token_id = $viewing_user_rel_active_jwt_token_details_result_row["user_auth_token_id"];
			$user_auth_token_id = $viewing_user_rel_active_jwt_token_details_result_key;
			
			
			$candidate_rel_uploaded_visa_document_details_result = candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id($candidate_user_id_input, $uploaded_file_ref_id_input);
			
			if (count($candidate_rel_uploaded_visa_document_details_result) > 0) {
				
				$generated_filename = $candidate_rel_uploaded_visa_document_details_result["generated_filename"];
				
				
				
				/*//Gathering inputs for Link Signature
				$link_signature_inputs = $viewer_user_id_input . ":::::" . $candidate_user_id_input . ":::::" . $uploaded_file_ref_id_input . ":::::" . $generated_filename . ":::::" . $user_auth_token_id;
				//expiring_links_lifetime
				$link_signature = hash('sha256', $link_signature_inputs);
				*/
				//return $constructed_array["link_signature"] = $link_signature;
				
				//Create Uploaded File URL
				//https://dev-visadoc.securitywonks.net/viewer/1/candidate/11/uploaded-file-ref-id/151/display/link-signature/tff8t76rft8tyfy88y8f8y
				////https://dev-visadoc.securitywonks.net/viewer/11/candidate/11/uploaded-file-ref-id/46/display/expires/1111111/link-signature/bd27845343fdfer3redsfr3regfh6t3rfdggj78453f
				
				//$uploaded_file_url = $uploaded_file_url_protocol_input . "://" . $uploaded_file_url_hostname_input . "/viewer/" . $viewer_user_id_input . "/candidate/" . $candidate_user_id_input . "/uploaded-file-ref-id/" . $uploaded_file_ref_id_input . "/display/expires/" . $expiring_link_lifetime_input . "/link-signature/" . $link_signature;
				
				$download_file_url_without_signature = $uploaded_file_url_protocol_input . "://" . $uploaded_file_url_hostname_input . "/viewer/" . $viewer_user_id_input . "/candidate/" . $candidate_user_id_input . "/uploaded-file-ref-id/" . $uploaded_file_ref_id_input . "/download/expires/" . $expiring_link_lifetime_input;
				
				$eventLog->log("uploaded_file_url (without signature): " . $download_file_url_without_signature);
				
				/*//Creating Link Signature
				$link_signature = hash('sha256', $user_auth_token_id . $download_file_url_without_signature);
				*/
				
				//Base64 Decode the Base64 Encoded Expiring Link related Secret Key
				$expiring_link_rel_secret_base64_encoded = $expiring_link_secret_key_input;
				$expiring_link_rel_secret_base64_decoded = base64_decode($expiring_link_rel_secret_base64_encoded);
				
				//Create Link Signature using sha256
				$created_link_signature = hash_hmac($expiring_link_hash_algorithm_input, $user_auth_token_id . $download_file_url_without_signature, $expiring_link_rel_secret_base64_decoded, true);
				
				//Base64 URL Encode the Created Token Signature
				$created_link_signature_base64_urlencoded = base64url_encode($created_link_signature); //from /app/includes/other-functions-api.php
				
				//remove padding (=), from Base64 URL Encoded Token Signature
				$created_link_signature_base64_urlencoded_after_removing_padding = str_replace("=", "", $created_link_signature_base64_urlencoded);
				
				$download_file_url_with_signature = $download_file_url_without_signature . "/link-signature/" . $created_link_signature_base64_urlencoded_after_removing_padding;
				
				$constructed_array["download_file_url"] = $download_file_url_with_signature;
				
				$eventLog->log("download_file_url (with signature): " . $download_file_url_with_signature);
				
				return $constructed_array;
				
			} else {
				$eventLog->log("Uploaded file ref id is not Valid!!!!");	
			}//close of else of if (count($candidate_rel_uploaded_visa_document_details_result) > 0) {
				
			
			
		}//close of foreach($viewing_user_rel_active_jwt_token_details_result as $viewing_user_rel_active_jwt_token_details_result_key => $viewing_user_rel_active_jwt_token_details_result_value) {
		
	} else {
		$eventLog->log("Active Token Details of Viewing User is not received");
	}//close of else of if (count($viewing_user_rel_active_jwt_token_details_result) > 0) {
	
	return $constructed_array;
}


function candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id($user_id_input, $crauvd_id_input) {
    global $dbcon;
	$constructed_array = array();
	
	$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_sql = "SELECT * FROM `candidate_rel_uploaded_resumes` WHERE `crur_id`=:crur_id AND `sm_memb_id`=:sm_memb_id";
	$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query = $dbcon->prepare($candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_sql);
	$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query->bindValue(":crur_id",$crauvd_id_input);	
	$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query->bindValue(":sm_memb_id",$user_id_input);	
	$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query->execute(); 
	
	if($candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query->rowCount() > 0) {
		$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query_result = $candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query->fetch();
	     return $candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query_result;
	
	}//close of if($candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query->rowCount() > 0) {
	return $constructed_array;
}



	

exit;
?>