<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "5")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			//if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					/*if (isset($ea_received_rest_ws_raw_array_input['user_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['user_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['user_id'])) */
					
					if (isset($ea_received_rest_ws_raw_array_input['resume_upload_mode'])) {
						$content .= $ea_received_rest_ws_raw_array_input['resume_upload_mode'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['resume_upload_mode'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['resume_source'])) {
						$content .= $ea_received_rest_ws_raw_array_input['resume_source'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['resume_source'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['file_rel_original_filename'])) {
						$content .= $ea_received_rest_ws_raw_array_input['file_rel_original_filename'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['file_rel_original_filename'])) 
					
					/*if (isset($ea_received_rest_ws_raw_array_input['job_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_id']))*/ 
					
					if (isset($ea_received_rest_ws_raw_array_input['file_content'])) {
						//$content .= $ea_received_rest_ws_raw_array_input['file_content'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['file_content'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				//$user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$resume_upload_mode_input = trim(isset($ea_received_rest_ws_raw_array_input['resume_upload_mode']) ? filter_var($ea_received_rest_ws_raw_array_input['resume_upload_mode'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$resume_source_input = trim(isset($ea_received_rest_ws_raw_array_input['resume_source']) ? filter_var($ea_received_rest_ws_raw_array_input['resume_source'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				//$job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$file_rel_original_filename_input = trim(isset($ea_received_rest_ws_raw_array_input['file_rel_original_filename']) ? filter_var($ea_received_rest_ws_raw_array_input['file_rel_original_filename'], FILTER_SANITIZE_STRING) : '');
				
				//$user_file_upload_ref_id_input = trim(isset($ea_received_rest_ws_raw_array_input['user_file_upload_ref_id']) ? filter_var($ea_received_rest_ws_raw_array_input['user_file_upload_ref_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$file_content_input = trim(isset($ea_received_rest_ws_raw_array_input['file_content']) ? filter_var($ea_received_rest_ws_raw_array_input['file_content'], FILTER_SANITIZE_STRING) : '');
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//Check if all inputs are received correctly from the REST Web Service
				/*if ($user_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-user-id";
					$response['status_description'] = "Missing User ID, please check and try again.";
					
					$eventLog->log("Please provide a valid User ID.");*/
					
				 if ($resume_upload_mode_input == "") {
					//Invalid File Upload Reference ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-file-upload-ref-id";
					$response['status_description'] = "Missing File Upload Reference ID";
					
					$eventLog->log("missing-file-upload-ref-id: Missing File Upload Reference ID, please check and try again.");
				
				} else if ($resume_source_input == "") {
					//Invalid File Upload Reference ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-file-upload-ref-id";
					$response['status_description'] = "Missing File Upload Reference ID";
					
					$eventLog->log("missing-file-upload-ref-id: Missing File Upload Reference ID, please check and try again.");
				
				/*} else if ($resume_upload_mode_input != "" && $job_id_input == "") {
					//Invalid File Upload Reference ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-file-upload-ref-id";
					$response['status_description'] = "Missing File Upload Reference ID";
					
					$eventLog->log("missing-file-upload-ref-id: Missing File Upload Reference ID, please check and try again.");
				*/	
				} else if ($file_rel_original_filename_input == "") {
					//Invalid File Upload Reference ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-file-ref-original-filename";
					$response['status_description'] = "Missing File related Original File Name";
					
					$eventLog->log("missing-file-ref-original-filename: Missing File related Original File Name, please check and try again.");
					
				} else if ($file_content_input == "") {
					//Empty File Content scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "empty-file-content-received";
					$response['status_description'] = "Empty File Content Received";
					
					$eventLog->log("empty-file-content-received: Empty File Content Received, Please check and try again.");
					
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Some Additional Information is missing";
					
					$eventLog->log("missing-some-additional-information: Please provide all information.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					$ea_action_specific_auth_check_result = "";
					
					//User Details, based on User ID input, through api request
					//$user_basic_details_result = user_basic_details_check_based_on_user_id($user_id_input);
					
					//if (count($user_basic_details_result) > 0) {
						
						//$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
						//$ea_action_specific_auth_check_result = ea_check_action_specific_user_privileges_on_different_user_levels($ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_user_company_id,  $ea_extracted_jwt_token_sub, $user_id_input, $ea_extracted_jwt_token_user_privileges_list, $user_basic_details_result["company_id"], $page_filename);					
						
						//echo "ea_action_specific_auth_check_result: " . $ea_action_specific_auth_check_result;					
							
							
						

						
						//if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
								
								$eventLog->log("file_content_input: " . $file_content_input);	
								$eventLog->log("All inputs are Valid");	
								
								//Event Time, as per Indian Standard Time
								$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
					
								
								/*$candidate_rel_document_file_info_result = candidate_rel_document_file_info_get($user_file_upload_ref_id_input);
											
								if (count($candidate_rel_document_file_info_result) > 0) {
									
									$crauvd_id = $candidate_rel_document_file_info_result["crauvd_id"];
									$crauvd_id_rel_activity_ref = $candidate_rel_document_file_info_result["activity_ref"];
									$crauvd_id_rel_activity_ref_id = $candidate_rel_document_file_info_result["activity_ref_id"];
									$crauvd_id_rel_additional_document_ref = $candidate_rel_document_file_info_result["additional_document_ref"];
									$crauvd_id_rel_original_filename = $candidate_rel_document_file_info_result["original_filename"];
									$crauvd_id_rel_generated_filename = $candidate_rel_document_file_info_result["generated_filename"];
									
									
									if ($crauvd_id_rel_generated_filename == "") {*/
										
										//data:image/png;base64,iVBORw0KGgoAAAANSUhEUgA
									
										//Extract the received file content, and mime type parts
										$file_content_input_semi_colon_base64_comma_exploded = explode(";base64,",$file_content_input);
										
										if (count($file_content_input_semi_colon_base64_comma_exploded) == "2") {
											
											$uploaded_file_extension_rel_mime_type_and_data = $file_content_input_semi_colon_base64_comma_exploded[0];
											$uploaded_file_base64_encoded_data = $file_content_input_semi_colon_base64_comma_exploded[1];
											
											//Extract mime type from the mime type parts
											$uploaded_file_extension_rel_mime_type_and_data_exploded = explode(":",$uploaded_file_extension_rel_mime_type_and_data);
											
											if (count($uploaded_file_extension_rel_mime_type_and_data_exploded) == "2") {
												$uploaded_file_extension_rel_mime_type_and_data_first_part = $uploaded_file_extension_rel_mime_type_and_data_exploded[0];
												$uploaded_file_mime_type = $uploaded_file_extension_rel_mime_type_and_data_exploded[1];
												
												$eventLog->log("uploaded_file_mime_type: " . $uploaded_file_mime_type);	
												
												$random_text_generation_inputs = uniqid(rand(), TRUE);
												$generated_file_name_without_file_extension = hash('sha256', $random_text_generation_inputs);
												//$candidate_document_upload_folder = "./uploaded-documents/";
												
												
												
												
												//https://gist.github.com/raphael-riel/1253986
												$mime_types = array(
															'pdf'     => 'application/pdf'
														);
														
												
												
												$extracted_file_extension_result = extract_file_extension_based_on_uploaded_file_mime_type_input($mime_types, $uploaded_file_mime_type);
												
												$eventLog->log("extracted_file_extension_result: " . $extracted_file_extension_result);
												
												if ($extracted_file_extension_result != "") {
													
													//Generated File Name
													$generated_file_name = $generated_file_name_without_file_extension . "." . $extracted_file_extension_result; 
													
													
													/*$crauvd_id_rel_activity_ref_id = $candidate_rel_document_file_info_result["activity_ref_id"];
													$crauvd_id_rel_additional_document_ref = $candidate_rel_document_file_info_result["additional_document_ref"];
													
													$crauvd_id_folder_name = create_seo_name(strtolower_utf8_extended($crauvd_id)) . "_" . create_seo_name(strtolower_utf8_extended($crauvd_id_rel_activity_ref_id)) . "_" . create_seo_name(strtolower_utf8_extended($crauvd_id_rel_additional_document_ref));
													*/
													//Create Full Upload Folder related Absolute Path
													//$crauvd_id_full_folder_absolute_path = $site_home_path . "uploaded-documents/" . $user_id_input . "/" . $crauvd_id_folder_name;
													$crauvd_id_full_folder_absolute_path = $site_home_path . "uploaded-documents/";
													$eventLog->log("Full Upload Folder (with absolute path): " . $crauvd_id_full_folder_absolute_path);
													
													if(!is_dir($crauvd_id_full_folder_absolute_path)) {
													
														mkdir($crauvd_id_full_folder_absolute_path, 0755, true);
														chmod($crauvd_id_full_folder_absolute_path, 0755);
														clearstatcache();	
														
														$eventLog->log("(Absolute Path - Not a Directory Structure) Condition");
														
												    } else if(!is_writable($crauvd_id_full_folder_absolute_path)) {
													
														chmod($crauvd_id_full_folder_absolute_path, 0755);
														clearstatcache();
														
														$eventLog->log("(Absolute Path - Directory Structure Not Writable) Condition");
														
												    }//close of else if of if(!is_dir($site_home_path . "uploaded-documents/" . $user_id_input . "/" . $crauvd_id_folder_name)) {
													
													
													//Create Full Upload Folder related Relative Path
													$crauvd_id_full_folder_relative_path = $candidate_document_upload_main_folder . "/"; // $candidate_document_upload_main_folder from /app/core/main-config.php
													
													$eventLog->log("Full Upload Folder (with relative path): " . $crauvd_id_full_folder_relative_path);
													
													//Stored File Location
													$targetFile =  $crauvd_id_full_folder_relative_path. $generated_file_name;
													
													
													/*
													$targetFile =  $candidate_document_upload_main_folder. $generated_file_name;
													*/
													$eventLog->log("targetFile (with path): " . $targetFile);
													
													
													//Base64 Decode the received file input
													$uploaded_file_base64_decoded_data = base64_decode($uploaded_file_base64_encoded_data);
													
													//Store the Content in the FILEINFO_MIME
													file_put_contents($targetFile, $uploaded_file_base64_decoded_data);
													
													$eventLog->log("The Uploaded file is saved now");
													
													//Create Full Upload Folder related Absolute Path
													$targetFile_absolute_path = $candidate_document_upload_main_folder_rel_absolute_path . "/" . $generated_file_name; // $candidate_document_upload_main_folder from /app/core/hostname-check.php
													
													
													$eventLog->log("Full Upload Folder (with relative path): " . $crauvd_id_full_folder_relative_path);
													
													
													//Check Received File Mimetype
													$finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
													//$file_mime_type = finfo_file($finfo, $targetFile) . "\n";
													$file_mime_type = finfo_file($finfo, $targetFile);
													finfo_close($finfo);
													
													$eventLog->log("enumerated file_mime_type: " . $file_mime_type);
													
													//Check, if user submitted file mime type = enumerated file mime type
													if ($uploaded_file_mime_type == $file_mime_type) {
														//Do Update Query
														//$user_file_upload_ref_id_input
														//$file_rel_original_filename_input
														
														$file_size_bytes_input = filesize($targetFile);
														$file_size_kilo_bytes_input = $file_size_bytes_input/1024;
														$file_size_mega_bytes_input = $file_size_bytes_input/(1024*1024);
														
														$eventLog->log("bytes: " . $file_size_bytes_input . " kilo bytes: " . $file_size_kilo_bytes_input . " mega bytes: " . $file_size_mega_bytes_input);
														
														//Check the File Size of the Uploaded File
														if ($file_size_mega_bytes_input > 25) {
															
															//Construct Content, that will be sent in Response body, of the REST Web Service
															$response['data'] = array();
															$response['status'] = "larger-file-uploaded";
															$response['status_description'] = "The File Size of the Uploaded File is > 25MB. Please uploaded the File, that is Smaller in Size.";
															
															$eventLog->log("The File Size of the Uploaded File is > 25MB");
															
														} else {
															$eventLog->log("Done");
															
															//File Size is <= 25MB
															
															//Update Query
															/*$candidate_rel_document_file_info_update_result = candidate_rel_document_file_info_update($user_id_input, $file_size_bytes_input, $file_size_kilo_bytes_input, $file_size_mega_bytes_input, $file_rel_original_filename_input, $generated_file_name, "member", $user_id_input, $event_datetime, $current_epoch, $user_file_upload_ref_id_input);	
															
															//$candidate_rel_document_file_info_update_result_var_dump = var_dump($candidate_rel_document_file_info_update_result);
															
															//$eventLog->log("file_info_update var_dump: " . $candidate_rel_document_file_info_update_result_var_dump);
															
															if ($candidate_rel_document_file_info_update_result) {
																//Construct Content, that will be sent in Response body, of the REST Web Service
																$response['data'] = array();
																$response['status'] = "file-upload-request-successful";
																$response['status_description'] = "The File is Uploaded Successfully, w.r.t. particular User's document reference.";
																
																$eventLog->log("The File is Uploaded Successfully, w.r.t. particular User's document reference.");	
																
															} else {
																//Construct Content, that will be sent in Response body, of the REST Web Service
																$response['data'] = array();
																$response['status'] = "file-upload-request-error";
																$response['status_description'] = "There occurred an error when uploading the file, w.r.t. particular User's document reference, please check and try again.";
																
																$eventLog->log("There occurred an error when uploading the file, w.r.t. particular User's document reference, please check and try again.");	
																
															}//close of else of if ($candidate_rel_document_file_info_update_result) {
														*/	
														}//close of else of if ($file_size_mega_bytes_input > 25) {
														
														
													} else {
														//File Mime Type is mis-matching
														
														//Construct Content, that will be sent in Response body, of the REST Web Service
														$response['data'] = array();
														$response['status'] = "file-upload-mime-type-mismatching";
														$response['status_description'] = "File Mime Type is mis-matching, w.r.t. particular User's document reference";
														
														$eventLog->log("file-upload-mime-type-mismatching: File Mime Type is mis-matching, w.r.t. particular User's document reference, Please check and try again.");
														
													}//close of if ($uploaded_file_mime_type == $file_mime_type) {

													
													
													
												} else {
													//Construct Content, that will be sent in Response body, of the REST Web Service
													$response['data'] = array();
													$response['status'] = "file-upload-mime-type-not-accepted";
													$response['status_description'] = "The Uploaded File's Mime Type is not in the accepted list of file types";
													
													$eventLog->log("file-upload-mime-type-not-accepted: The Uploaded File's Mime Type is not in the accepted list of file types, please check and try again.");	
												}//close of else of if ($extracted_file_extension_result != "") {
													
												
												
											} else {
												//Construct Content, that will be sent in Response body, of the REST Web Service
												$response['data'] = array();
												$response['status'] = "invalid-file-extension-input";
												$response['status_description'] = "Invalid File Extension Input";
												
												$eventLog->log("invalid-file-extension-input: Invalid info w.r.t. file extension part. Invalid File Extension Input, Please Check and try again.");	
											}//close of else of if (count($uploaded_file_extension_rel_mime_type_and_data_exploded) == "2") {
											
											
											
										} else {
											//Construct Content, that will be sent in Response body, of the REST Web Service
											$response['data'] = array();
											$response['status'] = "invalid-base64-encoded-file-input";
											$response['status_description'] = "Invalid Base64 Encoded File Input";
											
											$eventLog->log("invalid-base64-encoded-file-input: Invalid base64 encoded file content");	
										}//close of else of if (count($file_content_input_semi_colon_base64_comma_exploded) == "2") {
										
										
									/*} else {
										//Construct Content, that will be sent in Response body, of the REST Web Service
										$response['data'] = array();
										$response['status'] = "file-already-uploaded";
										$response['status_description'] = "The File is already Uploaded";
										
										$eventLog->log("file-already-uploaded: The File is already Uploaded, w.r.t. particular User's document reference.");	
										
									}//close of else of if ($crauvd_id_rel_generated_filename == "") {
									
										
									
								} else {
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "invalid-file-upload-ref-id";
									$response['status_description'] = "Invalid File Upload Ref ID Submitted";
									
									$eventLog->log("invalid-file-upload-ref-id: Invalid File Upload Ref ID Submitted, please check and try again.");	
									
								}//close of else of if (count($candidate_rel_document_file_info_result) > 0) {
								
								*/
								
							} catch (Exception $e) {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "file-upload-request-initiating-error";
								$response['status_description'] = "Error occurred when initiating the File Upload Request";
								
								$eventLog->log("file-upload-request-initiating-error: Error occurred when initiating the File Upload Request, w.r.t. particular User's document reference.");	
								
							}
					}
						/*} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-subject-platform-scope-user";
							$response['status_description'] = "Insufficient Set of Permissions, to complete this action, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-different-company-user-id";
							$response['status_description'] = "Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-INSUFFICIENT-PERMISSIONS") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-self-action";
							$response['status_description'] = "Insufficient Set of Permissions, as self action is Dis-allowed, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						}//close of if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
						
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "The User Account, that is attempted to be edited, is invalid, please check and try again.";
						
						$eventLog->log("Please provide a Valid User ID.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
						
					
				}//close of else of if ($user_id_input == "") {*/
				
				
			//}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	//}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "5")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){

function extract_file_extension_based_on_uploaded_file_mime_type_input($mime_types_input, $uploaded_file_mime_type_input) {
											
	$uploaded_file_rel_extension = "";
	
	foreach ($mime_types_input as $mime_types_input_key => $mime_types_input_value) {
	
		if (trim($uploaded_file_mime_type_input) == $mime_types_input_value) {
			
			$uploaded_file_rel_extension = $mime_types_input_key;
			
			return $uploaded_file_rel_extension;
			
		}//close of if (in_array(trim($uploaded_file_mime_type_input), $mime_types_input_value, true)) {
			
	}//close of foreach ($mime_types_input as $mime_types_input_key => $mime_types_input_value) {
	return $uploaded_file_rel_extension;
	
}


function candidate_rel_document_file_info_get($crauvd_id_input) {
    global $dbcon;
	$constructed_array = array();
	$candidate_rel_document_file_info_get_sql = "SELECT * FROM `candidate_rel_all_uploaded_visa_documents` WHERE `crauvd_id`=:crauvd_id";
	$candidate_rel_document_file_info_get_select_query = $dbcon->prepare($candidate_rel_document_file_info_get_sql);
	$candidate_rel_document_file_info_get_select_query->bindValue(":crauvd_id",$crauvd_id_input);	
	$candidate_rel_document_file_info_get_select_query->execute(); 
	
	if($candidate_rel_document_file_info_get_select_query->rowCount() > 0) {
		$candidate_rel_document_file_info_get_select_query_result = $candidate_rel_document_file_info_get_select_query->fetch();
	     return $candidate_rel_document_file_info_get_select_query_result;
	
	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
}

function candidate_rel_document_file_info_update($user_id_input, $file_size_bytes_input, $file_size_kilo_bytes_input, $file_size_mega_bytes_input, $original_filename_input, $generated_filename_input, $last_updated_by_user_type_input, $last_updated_by_user_id_input, $last_updated_date_time_input, $last_updated_date_time_epoch_input, $crauvd_id_input) {	
    global $dbcon;
	
	
	
	$candidate_rel_document_file_info_update_sql = "UPDATE `candidate_rel_all_uploaded_visa_documents` SET `original_filename`=:original_filename,`generated_filename`=:generated_filename,`file_size_bytes`=:file_size_bytes,`file_size_kilo_bytes`=:file_size_kilo_bytes,`file_size_mega_bytes`=:file_size_mega_bytes,`last_updated_by_user_type`=:last_updated_by_user_type,`last_updated_by_user_id`=:last_updated_by_user_id,`last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch WHERE `crauvd_id`=:crauvd_id";
	$candidate_rel_document_file_info_update_update_query = $dbcon->prepare($candidate_rel_document_file_info_update_sql);
	$candidate_rel_document_file_info_update_update_query->bindValue(":original_filename",$original_filename_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":generated_filename",$generated_filename_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":file_size_bytes",$file_size_bytes_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":file_size_kilo_bytes",$file_size_kilo_bytes_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":file_size_mega_bytes",$file_size_mega_bytes_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":last_updated_by_user_type",$last_updated_by_user_type_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":last_updated_by_user_id",$last_updated_by_user_id_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":last_updated_date_time",$last_updated_date_time_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":last_updated_date_time_epoch",$last_updated_date_time_epoch_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":crauvd_id",$crauvd_id_input);
	
	if ($candidate_rel_document_file_info_update_update_query->execute()) {
		return true;
	}
	return false;
}

exit;
?>