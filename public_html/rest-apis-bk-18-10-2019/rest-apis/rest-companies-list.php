<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Companies, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "7")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs
			
				$company_status_input = trim(isset($ea_received_rest_ws_raw_array_input['company_status']) ? filter_var($ea_received_rest_ws_raw_array_input['company_status'], FILTER_SANITIZE_NUMBER_INT) : '');
				$page_number_input = trim(isset($ea_received_rest_ws_raw_array_input['page_number']) ? filter_var($ea_received_rest_ws_raw_array_input['page_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				$number_of_records_input = trim(isset($ea_received_rest_ws_raw_array_input['number_of_records']) ? filter_var($ea_received_rest_ws_raw_array_input['number_of_records'], FILTER_SANITIZE_NUMBER_INT) : '');
				$sort_field_input = trim(isset($ea_received_rest_ws_raw_array_input['sort_field']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_field'], FILTER_SANITIZE_STRING) : '');
				$sort_order_input = trim(isset($ea_received_rest_ws_raw_array_input['sort_order']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_order'], FILTER_SANITIZE_STRING) : '');
				$search_criteria_input = trim(isset($ea_received_rest_ws_raw_array_input['search_criteria']) ? filter_var($ea_received_rest_ws_raw_array_input['search_criteria'], FILTER_SANITIZE_STRING) : '');
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
				
				$eventLog->log("company_status_input -> " . $company_status_input);
				$eventLog->log("page_number_input -> " . $page_number_input);
				$eventLog->log("number_of_records_input -> " . $number_of_records_input);
				$eventLog->log("sort_field_input -> " . $sort_field_input);
				$eventLog->log("sort_order_input -> " . $sort_order_input);
				$eventLog->log("search_criteria_input -> " . $search_criteria_input);
				$eventLog->log("ip_address_input -> " . $ip_address_input);
				
				//Check if all inputs are received correctly from the REST Web Service
				
				if (($company_status_input != "0") && ($company_status_input != "1") && ($company_status_input != "")) {
					//Invalid company Status scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-company-status";
					$response['status_description'] = "Invalid Company Status info submitted, please check and try again.";
					
					$eventLog->log("invalid-company-status: Please provide a valid Company Status info.");
					
				} else if ($page_number_input == "0") {
					//Invalid Page Number scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-page-number";
					$response['status_description'] = "Invalid Page Number info submitted, please check and try again.";
					
					$eventLog->log("invalid-page-number: Invalid Page Number info submitted, please check and try again.");
					
				} else if (($sort_order_input != "ASC") && ($sort_order_input != "DESC") && ($sort_order_input != "")) {
					//Invalid Sorting Order scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-Sorting-order";
					$response['status_description'] = "Invalid Sorting Order info submitted, please check and try again.";
					
					$eventLog->log("invalid-Sorting-order: Invalid Sorting Order info submitted, please check and try again.");
					
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-additional-information";
					$response['status_description'] = "Some Additional Information like IP Address (IPv4) is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");
							  
				} else {
					//All inputs are Valid
				
					$eventLog->log("All inputs are valid.");
				
					try { 
						/*if (($page_number_input == "") && ($number_of_records_input == "")) {
							//Give All Data
							$page_number_input = "0";
							$number_of_records_input = "all";
							
						} else if (($page_number_input != "") && ($number_of_records_input == "")) {
							//Give Data, whose primary key id value is greater than page_number_input, till the number of records, as defined in the $default_number_of_records_pagination
							$number_of_records_input = $default_number_of_records_pagination; //app/core/main-config.php
							
						} else if (($page_number_input == "") && ($number_of_records_input != "")) {
							//Give Data, from first record, till the requested number of records
							$page_number_input = "0";
							
						}//close of else if of if (($page_number_input == "") && ($number_of_records_input == "")) {
						*/
						$companies_list_result = array();
						
						if ($ea_extracted_jwt_token_user_company_id != "") {
							$eventLog->log("company-id=>".$ea_extracted_jwt_token_user_company_id);
							$eventLog->log("user-id=>".$ea_extracted_jwt_token_sub);
							//when the logged in user, is belongs to a Company
							$response['data'] = array();
							$response['status'] = "not-a-platform-scope-user";
							$response['status_description'] = "Not a Platform Scope User, please check and try again.";
							
							$eventLog->log("not-a-platform-scope-user -> Not a Platform Scope User, please check and try again."); 
							
						} else {
							//Do Get User Groups List, from sm_site_member_classification_details db table
							//$companies_list_result = get_companies_list($company_status_input);
							
							if ($company_status_input == "") {
								$company_status_input = null;
							}//close of if ($company_status_input == "") {
								
							/*$companies_list_result = get_companies_list_with_pagination_inputs($company_status_input, $page_number_input, $number_of_records_input);*/
							
							$companies_list_result = get_companies_list_with_pagination_inputs($company_status_input, $page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input);
							
						}//close of else of if ($ea_extracted_jwt_token_user_company_id != "") {
							
						
						$companies_list_result_count = count($companies_list_result);
						
						$eventLog->log("Count -> " . $companies_list_result_count); 
						
						if ($companies_list_result_count > "0") {
							//One or More User Groups Exist and Active
							
							$response['data'] = $companies_list_result;
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['status'] = "companies-list-received";
							$response['status_description'] = "List successfully received";
							
							$companies_list_result_count_json_encoded = json_encode($companies_list_result_count);
						
							$eventLog->log("companies List -> " . $companies_list_result_count_json_encoded); 
							
						} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "active-companies-doesnot-exist";
							$response['status_description'] = "No records found.";
							
							//No Active company list Exist
							$eventLog->log("Companies List -> No Active companies Exist, please check and try again."); 
						}//close of else of if ($company_list_result_count > "0") {
						
					} catch (Exception $e){
						$eventLog->log("Exception -> " . html_escaped_output($e->getMessage())); 
						//addLog($logFile, "Exception -> ".$e->getMessage());	
					}//close of  catch (Exception $e){
					
				
				}//close of if (($company_status_input != "0") && ($company_status_input != "1") && ($company_status_input != "")) {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "7")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
/* function get_companies_list_with_pagination_inputs($company_status_input, $page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input) {

    global $dbcon, $default_number_of_records_pagination; //app/core/main-config.php

	$constructed_array = array();


	if ($search_criteria_input == "") {
		$search_criteria_input = null;
	}//close of if ($search_criteria_input == "") {


	if (!is_null($search_criteria_input)) {
		//When Search Criteria input is not empty
		$search_criteria_variable = "%" . $search_criteria_input . "%";
		//$search_criteria_in_query = "(`company_name` LIKE '%info%') OR (`company_seo_name` LIKE '%info%') OR (`company_brand_name` LIKE '%info%') OR (`is_active_status` LIKE '%info%')";

		$search_criteria_in_query_with_where_keyword = " WHERE (`company_id` LIKE :company_id_search_keyword) OR (`company_name` LIKE :company_name_search_keyword) OR (`company_seo_name` LIKE :company_seo_name_search_keyword) OR (`company_brand_name` LIKE :company_brand_name_search_keyword) OR (`is_active_status` LIKE :is_active_status_search_keyword) ";

		$search_criteria_in_query_without_where_keyword = " AND (`company_id` LIKE :company_id_search_keyword) OR (`company_name` LIKE :company_name_search_keyword) OR (`company_seo_name` LIKE :company_seo_name_search_keyword) OR (`company_brand_name` LIKE :company_brand_name_search_keyword) OR (`is_active_status` LIKE :is_active_status_search_keyword) ";

	} else {
		//When Search Criteria input is empty
		$search_criteria_in_query_with_where_keyword = " WHERE ";

		$search_criteria_in_query_without_where_keyword = "";

	}//close of else of if (!is_null($search_criteria_input)) {


	if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {

		if (!is_null($sort_field_input)) {
			//When the Sort Field input is not empty


			if ($sort_field_input == "company_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_id` " . $sort_order_input;

			} else if ($sort_field_input == "company_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_name` " . $sort_order_input;

			} else if ($sort_field_input == "company_seo_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_seo_name` " . $sort_order_input;

			} else if ($sort_field_input == "company_brand_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_brand_name` " . $sort_order_input;

			} else if ($sort_field_input == "company_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `is_active_status` " . $sort_order_input;

			} else {
				//When Sort Field input, is not among the expected values, use primary key, with provided Sorting Order input
				$sort_details_in_query = " ORDER BY `company_id` " . $sort_order_input;



			}


		} else {
			//When the Sort Field input is empty, primary key has to be taken, with provided Sorting Order input
			$sort_details_in_query = " ORDER BY `company_id` " . $sort_order_input;

		}//close of if (!is_null($sort_field_input)) {


	} else {
		//When the Sort Field input is empty, primary key has to be taken, with ASC Order, by default
		$sort_details_in_query = " ORDER BY `company_id` ASC ";

	}//close of else of if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {


	$limit_offset_value = 0;

	if (($page_number_input == "") && ($number_of_records_input == "")) {
		//Give All Data
		//No LIMIT Clause in the Query
		$limit_offset_in_query = "";

	} else if (($page_number_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than page_number_input, till the number of records, as defined in the $default_number_of_records_pagination

		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $default_number_of_records_pagination;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$default_number_of_records_pagination;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $default_number_of_records_pagination;
		}//close of else if of if ($page_number_input == "1") {



	} else if (($page_number_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records
		$limit_offset_in_query = " LIMIT " . $number_of_records_input;

	} else {
		//Give Data, from given Record, till the requested number of records
		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $number_of_records_input;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$number_of_records_input;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $number_of_records_input;
		}//close of else if of if ($page_number_input == "1") {

	}//close of else of if (($page_number_input == "") && ($number_of_records_input == "")) {

	if (is_null($company_status_input)) {

		if (!is_null($search_criteria_input)) {
			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)

			//Define Named parameters and corresponding Values
			$company_named_parameters_values_array_input = array(":company_id_search_keyword" => $search_criteria_variable, ":company_name_search_keyword" => $search_criteria_variable, ":company_seo_name_search_keyword" => $search_criteria_variable, ":company_brand_name_search_keyword" => $search_criteria_variable, ":is_active_status_search_keyword" => $search_criteria_variable);

			//Get Companies List Count
			$companies_list_count_get_sql = "SELECT COUNT(*) AS count FROM `companies`" . $search_criteria_in_query_with_where_keyword . $sort_details_in_query;
			$companies_list_count_get_select_query = $dbcon->prepare($companies_list_count_get_sql);
			$companies_list_count_get_select_query->execute($company_named_parameters_values_array_input);

			//Get Companies List
			$companies_list_get_sql = "SELECT * FROM `companies`" . $search_criteria_in_query_with_where_keyword . $sort_details_in_query . $limit_offset_in_query;

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->execute();
			$companies_list_get_select_query->execute($company_named_parameters_values_array_input);

		} else {
			//Give List of Companies Count, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$companies_list_count_get_sql = "SELECT COUNT(*) AS count FROM `companies`" . $sort_details_in_query;
			$companies_list_count_get_select_query = $dbcon->prepare($companies_list_count_get_sql);
			$companies_list_count_get_select_query->execute();


			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$companies_list_get_sql = "SELECT * FROM `companies`" . $sort_details_in_query . $limit_offset_in_query;
			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			$companies_list_get_select_query->execute();

		}//close of else of if (!is_null($search_criteria_input)) {


	} else {
		if (!is_null($search_criteria_input)) {

			//Define Named parameters and corresponding Values
			$company_named_parameters_values_array_input = array(":company_id_search_keyword" => $search_criteria_variable, ":company_name_search_keyword" => $search_criteria_variable, ":company_seo_name_search_keyword" => $search_criteria_variable, ":company_brand_name_search_keyword" => $search_criteria_variable, ":is_active_status_search_keyword" => $search_criteria_variable, ":is_active_status" => $company_status_input);

			//Get Companies List Count
			$companies_list_count_get_sql = "SELECT COUNT(*) AS count FROM `companies` WHERE `is_active_status`=:is_active_status" . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;
			$companies_list_count_get_select_query = $dbcon->prepare($companies_list_count_get_sql);
			$companies_list_count_get_select_query->execute($company_named_parameters_values_array_input);


			//Give List of Companies, based on value of is_active_status
			$companies_list_get_sql = "SELECT * FROM `companies` WHERE `is_active_status`=:is_active_status" . $search_criteria_in_query_without_where_keyword . $sort_details_in_query . $limit_offset_in_query;

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->execute();
			$companies_list_get_select_query->execute($company_named_parameters_values_array_input);

		} else {

			//Define Named parameters and corresponding Values
			$company_named_parameters_values_array_input = array(":is_active_status" => $company_status_input);

			//Give List of Companies Count, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$companies_list_count_get_sql = "SELECT COUNT(*) AS count FROM `companies` WHERE `is_active_status`=:is_active_status" . $sort_details_in_query;
			$companies_list_count_get_select_query = $dbcon->prepare($companies_list_count_get_sql);
			$companies_list_count_get_select_query->execute($company_named_parameters_values_array_input);


			//Give List of Companies, based on value of is_active_status
			$companies_list_get_sql = "SELECT * FROM `companies` WHERE `is_active_status`=:is_active_status" . $sort_details_in_query . $limit_offset_in_query;

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->execute();
			$companies_list_get_select_query->execute($company_named_parameters_values_array_input);

		}//close of else of if (!is_null($search_criteria_input)) {



	}//close of else of if (is_null($company_status_input)) {

	//Process / Get Companies Count
	if($companies_list_count_get_select_query->rowCount() > 0) {
	    $companies_list_count_get_select_query_result = $companies_list_count_get_select_query->fetch();
	    //print_r($companies_list_count_get_select_query_result);

		$total_companies_count = $companies_list_count_get_select_query_result["count"];
		$constructed_array["total_records_count"] = $total_companies_count;

	}//close of if($companies_list_count_get_select_query->rowCount() > 0) {


	//Process / Fetch Companies List
	if($companies_list_get_select_query->rowCount() > 0) {
	    $companies_list_get_select_query_result = $companies_list_get_select_query->fetchAll();
	    //print_r($companies_list_get_select_query_result);

		foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

			$temp_row_array = array();
		    $temp_row_array["company_id"] = $companies_list_get_select_query_result_row["company_id"];
		    $temp_row_array["company_name"] = $companies_list_get_select_query_result_row["company_name"];
			$temp_row_array["company_seo_name"] = $companies_list_get_select_query_result_row["company_seo_name"];
		    $temp_row_array["company_brand_name"] = $companies_list_get_select_query_result_row["company_brand_name"];
		    $temp_row_array["company_status"] = $companies_list_get_select_query_result_row["is_active_status"];

		    $constructed_array["list"][] = $temp_row_array;
	    }//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

	}//close of if($companies_list_get_select_query->rowCount() > 0) {

	return $constructed_array;
}
 */
 function get_companies_list_with_pagination_inputs($company_status_input, $page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input) {

    global $dbcon, $default_number_of_records_pagination; //app/core/main-config.php

	$constructed_array = array();


	if ($search_criteria_input == "") {
		$search_criteria_input = null;
	}//close of if ($search_criteria_input == "") {


	if (!is_null($search_criteria_input)) {
		//When Search Criteria input is not empty
		$search_criteria_variable = "%" . $search_criteria_input . "%";
		//$search_criteria_in_query = "(`company_name` LIKE '%info%') OR (`company_seo_name` LIKE '%info%') OR (`company_brand_name` LIKE '%info%') OR (`is_active_status` LIKE '%info%')";

		$search_criteria_in_query_with_where_keyword = " WHERE (`company_id` LIKE :company_id_search_keyword) OR (`company_name` LIKE :company_name_search_keyword) OR (`company_seo_name` LIKE :company_seo_name_search_keyword) OR (`company_brand_name` LIKE :company_brand_name_search_keyword) OR (`is_active_status` LIKE :is_active_status_search_keyword) ";

		$search_criteria_in_query_without_where_keyword = " AND (`company_id` LIKE :company_id_search_keyword) OR (`company_name` LIKE :company_name_search_keyword) OR (`company_seo_name` LIKE :company_seo_name_search_keyword) OR (`company_brand_name` LIKE :company_brand_name_search_keyword) OR (`is_active_status` LIKE :is_active_status_search_keyword) ";

	} else {
		//When Search Criteria input is empty
		$search_criteria_in_query_with_where_keyword = " WHERE ";

		$search_criteria_in_query_without_where_keyword = "";

	}//close of else of if (!is_null($search_criteria_input)) {


	if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {

		if (!is_null($sort_field_input)) {
			//When the Sort Field input is not empty


			if ($sort_field_input == "company_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_id` " . $sort_order_input;

			} else if ($sort_field_input == "company_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_name` " . $sort_order_input;

			} else if ($sort_field_input == "company_seo_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_seo_name` " . $sort_order_input;

			} else if ($sort_field_input == "company_brand_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_brand_name` " . $sort_order_input;

			} else if ($sort_field_input == "company_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `is_active_status` " . $sort_order_input;

			} else {
				//When Sort Field input, is not among the expected values, use primary key, with provided Sorting Order input
				$sort_details_in_query = " ORDER BY `company_id` " . $sort_order_input;



			}


		} else {
			//When the Sort Field input is empty, primary key has to be taken, with provided Sorting Order input
			$sort_details_in_query = " ORDER BY `company_id` " . $sort_order_input;

		}//close of if (!is_null($sort_field_input)) {


	} else {
		//When the Sort Field input is empty, primary key has to be taken, with ASC Order, by default
		$sort_details_in_query = " ORDER BY `company_id` ASC ";

	}//close of else of if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {


	$limit_offset_value = 0;

	if (($page_number_input == "") && ($number_of_records_input == "")) {
		//Give All Data
		//No LIMIT Clause in the Query
		$limit_offset_in_query = "";

	} else if (($page_number_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than page_number_input, till the number of records, as defined in the $default_number_of_records_pagination

		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $default_number_of_records_pagination;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$default_number_of_records_pagination;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $default_number_of_records_pagination;
		}//close of else if of if ($page_number_input == "1") {



	} else if (($page_number_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records
		$limit_offset_in_query = " LIMIT " . $number_of_records_input;

	} else {
		//Give Data, from given Record, till the requested number of records
		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $number_of_records_input;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$number_of_records_input;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $number_of_records_input;
		}//close of else if of if ($page_number_input == "1") {

	}//close of else of if (($page_number_input == "") && ($number_of_records_input == "")) {

	if (is_null($company_status_input)) {

		if (!is_null($search_criteria_input)) {
			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)

			//Define Named parameters and corresponding Values
			$company_named_parameters_values_array_input = array(":company_id_search_keyword" => $search_criteria_variable, ":company_name_search_keyword" => $search_criteria_variable, ":company_seo_name_search_keyword" => $search_criteria_variable, ":company_brand_name_search_keyword" => $search_criteria_variable, ":is_active_status_search_keyword" => $search_criteria_variable);

			//Get Companies List Count
			$companies_list_count_get_sql = "SELECT COUNT(*) AS count FROM `companies`" . $search_criteria_in_query_with_where_keyword . $sort_details_in_query;
			$companies_list_count_get_select_query = $dbcon->prepare($companies_list_count_get_sql);
			$companies_list_count_get_select_query->execute($company_named_parameters_values_array_input);

			//Get Companies List
			$companies_list_get_sql = "SELECT * FROM `companies`" . $search_criteria_in_query_with_where_keyword . $sort_details_in_query . $limit_offset_in_query;

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->execute();*/
			$companies_list_get_select_query->execute($company_named_parameters_values_array_input);

		} else {
			//Give List of Companies Count, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$companies_list_count_get_sql = "SELECT COUNT(*) AS count FROM `companies`" . $sort_details_in_query;
			$companies_list_count_get_select_query = $dbcon->prepare($companies_list_count_get_sql);
			$companies_list_count_get_select_query->execute();


			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$companies_list_get_sql = "SELECT * FROM `companies`" . $sort_details_in_query . $limit_offset_in_query;
			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			$companies_list_get_select_query->execute();

		}//close of else of if (!is_null($search_criteria_input)) {


	} else {
		if (!is_null($search_criteria_input)) {

			//Define Named parameters and corresponding Values
			$company_named_parameters_values_array_input = array(":company_id_search_keyword" => $search_criteria_variable, ":company_name_search_keyword" => $search_criteria_variable, ":company_seo_name_search_keyword" => $search_criteria_variable, ":company_brand_name_search_keyword" => $search_criteria_variable, ":is_active_status_search_keyword" => $search_criteria_variable, ":is_active_status" => $company_status_input);

			//Get Companies List Count
			$companies_list_count_get_sql = "SELECT COUNT(*) AS count FROM `companies` WHERE `is_active_status`=:is_active_status" . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;
			$companies_list_count_get_select_query = $dbcon->prepare($companies_list_count_get_sql);
			$companies_list_count_get_select_query->execute($company_named_parameters_values_array_input);


			//Give List of Companies, based on value of is_active_status
			$companies_list_get_sql = "SELECT * FROM `companies` WHERE `is_active_status`=:is_active_status" . $search_criteria_in_query_without_where_keyword . $sort_details_in_query . $limit_offset_in_query;

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->execute();*/
			$companies_list_get_select_query->execute($company_named_parameters_values_array_input);

		} else {

			//Define Named parameters and corresponding Values
			$company_named_parameters_values_array_input = array(":is_active_status" => $company_status_input);

			//Give List of Companies Count, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$companies_list_count_get_sql = "SELECT COUNT(*) AS count FROM `companies` WHERE `is_active_status`=:is_active_status" . $sort_details_in_query;
			$companies_list_count_get_select_query = $dbcon->prepare($companies_list_count_get_sql);
			$companies_list_count_get_select_query->execute($company_named_parameters_values_array_input);


			//Give List of Companies, based on value of is_active_status
			$companies_list_get_sql = "SELECT * FROM `companies` WHERE `is_active_status`=:is_active_status" . $sort_details_in_query . $limit_offset_in_query;

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->execute();*/
			$companies_list_get_select_query->execute($company_named_parameters_values_array_input);

		}//close of else of if (!is_null($search_criteria_input)) {



	}//close of else of if (is_null($company_status_input)) {

	//Process / Get Companies Count
	if($companies_list_count_get_select_query->rowCount() > 0) {
	    $companies_list_count_get_select_query_result = $companies_list_count_get_select_query->fetch();
	    //print_r($companies_list_count_get_select_query_result);

		$total_companies_count = $companies_list_count_get_select_query_result["count"];
		$constructed_array["total_records_count"] = $total_companies_count;

	}//close of if($companies_list_count_get_select_query->rowCount() > 0) {


	//Process / Fetch Companies List
	if($companies_list_get_select_query->rowCount() > 0) {
	    $companies_list_get_select_query_result = $companies_list_get_select_query->fetchAll();
	    //print_r($companies_list_get_select_query_result);

		foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

			$temp_row_array = array();
		    $temp_row_array["company_id"] = $companies_list_get_select_query_result_row["company_id"];
		    $temp_row_array["company_name"] = $companies_list_get_select_query_result_row["company_name"];
			$temp_row_array["company_seo_name"] = $companies_list_get_select_query_result_row["company_seo_name"];
		    $temp_row_array["company_brand_name"] = $companies_list_get_select_query_result_row["company_brand_name"];
		    $temp_row_array["company_status"] = $companies_list_get_select_query_result_row["is_active_status"];

		    $constructed_array["list"][] = $temp_row_array;
	    }//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

	}//close of if($companies_list_get_select_query->rowCount() > 0) {

	return $constructed_array;
}

exit;
?>