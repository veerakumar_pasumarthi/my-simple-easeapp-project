<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "13")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_client_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id']))
					
				    if (isset($ea_received_rest_ws_raw_array_input['job_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_id']))
						
					if (isset($ea_received_rest_ws_raw_array_input['page_number'])) {
						$content .= $ea_received_rest_ws_raw_array_input['page_number'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['page_number']))
					
					if (isset($ea_received_rest_ws_raw_array_input['number_of_records'])) {
						$content .= $ea_received_rest_ws_raw_array_input['number_of_records'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['number_of_records']))
					
				    if (isset($ea_received_rest_ws_raw_array_input['sort_field'])) {
						$content .= $ea_received_rest_ws_raw_array_input['sort_field'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['sort_field']))
					
					if (isset($ea_received_rest_ws_raw_array_input['sort_order'])) {
						$content .= $ea_received_rest_ws_raw_array_input['sort_order'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['sort_order']))
					
					if (isset($ea_received_rest_ws_raw_array_input['job_start_from_date'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_start_from_date'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_start_from_date']))
					
					if (isset($ea_received_rest_ws_raw_array_input['job_start_to_date'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_start_to_date'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_start_to_date']))
					
				    if (isset($ea_received_rest_ws_raw_array_input['resume_submission_end_from_date'])) {
						$content .= $ea_received_rest_ws_raw_array_input['resume_submission_end_from_date'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['resume_submission_end_from_date']))
					
					if (isset($ea_received_rest_ws_raw_array_input['resume_submission_end_to_date'])) {
						$content .= $ea_received_rest_ws_raw_array_input['resume_submission_end_to_date'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['resume_submission_end_to_date']))
					
					if (isset($ea_received_rest_ws_raw_array_input['search_criteria'])) {
						$content .= $ea_received_rest_ws_raw_array_input['search_criteria'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['search_criteria']))
						
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
				
				//Filter Inputs	
				
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$company_client_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_client_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_client_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$page_number_input= trim(isset($ea_received_rest_ws_raw_array_input['page_number']) ? filter_var($ea_received_rest_ws_raw_array_input['page_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				$number_of_records_input = trim(isset($ea_received_rest_ws_raw_array_input['number_of_records']) ? filter_var($ea_received_rest_ws_raw_array_input['number_of_records'], FILTER_SANITIZE_NUMBER_INT) : '');
				$sort_field_input= trim(isset($ea_received_rest_ws_raw_array_input['sort_field']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_field'], FILTER_SANITIZE_STRING) : '');
				$sort_order_input = trim(isset($ea_received_rest_ws_raw_array_input['sort_order']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_order'], FILTER_SANITIZE_STRING) : '');
				$search_criteria_input = trim(isset($ea_received_rest_ws_raw_array_input['search_criteria']) ? filter_var($ea_received_rest_ws_raw_array_input['search_criteria'], FILTER_SANITIZE_STRING) : '');
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {

                if ($search_criteria_input == "") {
					//Invalid company Status scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-search-criteria-input";
					$response['status_description'] = "Missing Search Criteria Input";
					
					$eventLog->log("Please provide a valid search criteria info.");
					
				} else if ($company_id_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company Id Input, Please check and try again.";	
					
				}else if (($sort_order_input != "asc") && ($sort_order_input != "desc") && ($sort_order_input != "")) {
					//Invalid Sorting Order scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-Sorting-order";
					$response['status_description'] = "Invalid Sorting Order info submitted, please check and try again.";
					
					$eventLog->log("invalid-Sorting-order: Invalid Sorting Order info submitted, please check and try again.");
					
				
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else {
					//all inputs are valid
					
					try{
						
					   
						
					       $candidate_list_info_result = array();
						   
					    if ($ea_extracted_jwt_token_user_company_id == "") {
						    //action taker is of platform scope - super admin / site admin scenario
						    $jobs_keyword_search_info_result = jobs_keyword_search_info($company_id_input,$company_client_id_input,$job_id_input,$search_criteria_input,$page_number_input,$number_of_records_input,$sort_field_input,$sort_order_input);
					
					    } else {
						   //action taker is of company scope - company admin, legal / immigration, internal admin etc scenario
						    $jobs_keyword_search_info_result = jobs_keyword_search_info($ea_extracted_jwt_token_user_company_id,$company_client_id_input,$job_id_input,$search_criteria_input,$page_number_input,$number_of_records_input,$sort_field_input,$sort_order_input);
					       
					    }//close of else of if ($ea_extracted_jwt_token_user_company_id == "") {
					    //$candidate_list_info = get_candidate_list($company_id_input, "member");
						$jobs_keyword_search_info_result_count = count($jobs_keyword_search_info_result);
						
						$eventLog->log("Count -> " . $jobs_keyword_search_info_result_count);
						
						
						
					    if ($jobs_keyword_search_info_result_count > "0") {
					       $response['data'] = $jobs_keyword_search_info_result;
					       $response['status'] = "resulted-jobs-received";
					       $response['status_description'] = "Resulted Jobs is Successfully Received";
							
					     $jobs_list_info_json_encoded = json_encode($jobs_keyword_search_info_result);
						
					    
					    } else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "no-jobs-found-for-this-search";
							$response['status_description'] = "No Jobs found for this Search criteria, please check and try again.";
							
					    }		
							
				    }catch (Exception $e){
						$eventLog->log("Exception -> " . html_escaped_output($e->getMessage())); 
						//addLog($logFile, "Exception -> ".$e->getMessage());	
					}//close of  catch (Exception $e){	}//close of else of if ($company_list_result_count > "0") {					
                  
				}//close of else of if ($user_type_input != 'admin') {
					
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
	
/*function candidates_resumes_keyword_search_info($company_id_input,$search_criteria_input){
	global $dbcon;
	$constructed_array = array();

	$candidates_resumes_keyword_search_info_sql = "SELECT * FROM `resume_extracted_info` WHERE MATCH(`resume_text`) AGAINST('".$search_criteria_input."' IN NATURAL LANGUAGE MODE)";
	$candidates_resumes_keyword_search_info_query = $dbcon->prepare($candidates_resumes_keyword_search_info_sql);
	//$candidates_resumes_keyword_search_info_query->bindValue(":search_criteria_input",$search_criteria_input);
    //$candidates_resumes_keyword_search_info_query->bindValue(":sm_user_status","4");
	$candidates_resumes_keyword_search_info_query->execute();

	if($candidates_resumes_keyword_search_info_query->rowCount() > 0) {
		$candidates_resumes_keyword_search_info_query_result = $candidates_resumes_keyword_search_info_query->fetchAll();
	     return $candidates_resumes_keyword_search_info_query_result;

	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;

}*/

function jobs_keyword_search_info($company_id_input,$company_client_id_input,$job_id_input,$search_criteria_input,$page_number_input,$number_of_records_input,$sort_field_input,$sort_order_input){
	
	global $dbcon, $elasticsearch_jobs_index,$elasticsearch_client , $ea_extracted_jwt_token_sub, $current_epoch, $default_number_of_records_pagination,$eventLog;
	$constructed_array = array();
	if($sort_field_input == "") {
		$sort_field_input = "job_id";
		
	} else {
		$sort_field_input = $sort_field_input;
	}
	if($sort_order_input == "") {
		$sort_order_input = "asc";
		
	} else {
		$sort_order_input = $sort_order_input;
	}
	
	/*$searchparams = [
						'index' => $elasticsearch_index_name,
						'type' => $elasticsearch_index_type,
						'body' => [
							'query' => [
								'match' => [
									'resume_text' => $search_criteria_input
								]
							]
						]
					];
	*/
	//$limit_offset_value = 0;

	if (($page_number_input == "") && ($number_of_records_input == "")) {
		//Give All Data
		//No LIMIT Clause in the Query
		//$limit_offset_in_query = "";
		$size_value = $default_number_of_records_pagination;
	    $from_value = 0;

	} else if (($page_number_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than page_number_input, till the number of records, as defined in the $default_number_of_records_pagination

		if ($page_number_input == "1") {
			$size_value = $default_number_of_records_pagination;
			$from_value = 0;
			//$limit_offset_in_query = " LIMIT 0, " . $default_number_of_records_pagination;
		} else if ($page_number_input >= "2") {
			$size_value = $default_number_of_records_pagination;
			$from_value = ($page_number_input-1)*$default_number_of_records_pagination;
			
			/* $limit_offset_value = ($page_number_input-1)*$default_number_of_records_pagination;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $default_number_of_records_pagination; */
		}//close of else if of if ($page_number_input == "1") {



	} else if (($page_number_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records
		//$limit_offset_in_query = " LIMIT " . $number_of_records_input;
		$size_value = $number_of_records_input;
		$from_value = 0;

	} else {
		//Give Data, from given Record, till the requested number of records
		if ($page_number_input == "1") {
			
			//$limit_offset_in_query = " LIMIT 0, " . $number_of_records_input;
			$size_value = $number_of_records_input;
			$from_value = 0;
			$eventLog->log("size:".$size_value);
		} else if ($page_number_input >= "2") {
			$size_value = $number_of_records_input;
			$from_value = ($page_number_input-1)*$number_of_records_input;
			//$limit_offset_value = ($page_number_input-1)*$number_of_records_input;
			//$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $number_of_records_input;
		}//close of else if of if ($page_number_input == "1") {

	}//close of else of if (($page_number_input == "") && ($number_of_records_input == "")) {
		/* $sort_field = $sort_field_input.".keyword";
		$eventLog->log("sort_field:".$sort_field);
		 */
		 
	
	 	
	/* $filter_terms = array();
	$filter_terms["company_id"] = array($company_id_input);
	$filter_terms["company_client_id"] = array($company_client_id_input); */
	
	 
	
	$jobMapping = array(
		'properties' => array(
			'job_id' => array(
				'type' => 'keyword'
			),
			'badges' => array(
				'type' => 'integer'
			),
			'company_id' => array(
				'type' => 'keyword'
			),
			'company_client_id' => array(
				'type' => 'keyword'
			),
			'client_company_name' => array(
				'type' => 'keyword'
			),
			
			/* 'job_posted_by_member_classification_detail_id' => array(
				'type' => 'keyword'
			),
			'job_posted_by_sm_memb_id' => array(
				'type' => 'keyword'
			), */
			'job_title' => array(
				'type' => 'keyword'
			),
			'job_summary' => array(
				'type' => 'text'
			),
			'job_full_description' => array(
				'type' => 'text'
			),
			'job_type_name' => array(
				'type' => 'keyword'
			),
			'job_industry_name' => array(
				'type' => 'keyword'
			),
			'job_work_location_requirement_name' => array(
				'type' => 'keyword'
			),
			'job_recruitment_status' => array(
				'type' => 'keyword'
			),
			'resume_submission_end_date' => array(
				'type' => 'date'
			),
			'no_of_openings' => array(
				'type' => 'keyword'
			),
			'available_positions' => array(
				'type' => 'keyword'
			),
			'maximum_allowed_submittals_count' => array(
				'type' => 'keyword'
			),
			'address_line_1' => array(
				'type' => 'text'
			),
			'address_line_2' => array(
				'type' => 'text'
			),
			'city' => array(
				'type' => 'text'
			),
			'state' => array(
				'type' => 'text'
			),
			'country' => array(
				'type' => 'text'
			),
			/* 'job_posted_date_time' => array(
				'type' => 'date'
			), */
			/* 'expected_job_start_date' => array(
				'type' => 'date'
			), */
			
			'zipcode' => array(
				'type' => 'keyword'
			),
			'primary_recruiter_id' => array(
				'type' => 'keyword'
			),
			'primary_recruiter_name' => array(
				'type' => 'text'
			),
			'ot' => array(
				'type' => 'keyword'
			),
			'job_references' => array(
				'type' => 'keyword'
			),
			'travel' => array(
				'type' => 'keyword'
			),
			'drug_test' => array(
				'type' => 'keyword'
			),
			'background_check' => array(
				'type' => 'keyword'
			),
			'security_clearance' => array(
				'type' => 'keyword'
			)
		)
	);
	
	$search_response['body']['mappings'] = $jobMapping;
	 
		
	
	$querystring_search_params = [
									'index' => $elasticsearch_jobs_index,
									//'type' => $elasticsearch_index_type,
									'from' => $from_value,
									'size' => $size_value,
									/* 'sort' => [
												 "sm_mobile" => "desc"
											  ], */
								/* 	"sort" => [
												"sm_mobile" => [
												 "order" => "desc" 
												 ]
											], */
																					
									'body' => [
									
									   'query' => [
	
											"bool" => [
												'filter' => [
													'term' => [ 
														'company_id' => $company_id_input
													], 
													'term' => [ 	
													    'company_client_id' => $company_client_id_input
													],
													'term' => [
													    'job_id' => $job_id_input
												    ]
											    ],
											  "should" => [
												'query_string' => [
													//"default_operator" => "AND",
													
													
										
													"fields" => ["job_title","city","state","country","zipcode","job_id","job_summary","job_full_description","job_type_name","company_client_id","client_company_name","job_recruitment_status","security_clearance","background_check","drug_test","travel","job_references","ot","primary_recruiter_name"],
													
													/* "job_id","job_title","job_summary","job_full_description","job_type_name","job_industry_name","job_work_location_requirement_name","job_recruitment_status","resume_submission_end_date","city","state","country","job_posted_date_time","zipcode","primary_recruiter_name","ot","job_references","travel","drug_test","background_check","security_clearance" */
													"query" => $search_criteria_input
													 ]
												]
										   
											]
										],
										"sort" => [
										$sort_field_input => ["order" => $sort_order_input]
										]
									]
										   
								];
											 

					$search_response = $elasticsearch_client->search($querystring_search_params);
					//echo "<pre>";
					//print_r($search_response); 

					//$duplicate_records_count = $search_response["hits"]["total"];
					
				     $result_rows_array = array();
					 $count = 0;
        
        
					foreach ($search_response['hits']['hits'] as $response_data_row) {
						/* print_r($response_data_row);
						echo "<br>";
						echo "_index: " . $response_data_row["_index"] . "<br>";
						echo "name field value, from _source: " . $response_data_row["_source"]["name"] . "<br>";
						 */
							
						foreach ($response_data_row as $response_data_row_key => $response_data_row_value) {
							
							
							if ($response_data_row_key == "_source") {
								$temp = array();
								
								foreach ($response_data_row_value as $response_data_row_value_key => $response_data_row_value_details) {
									$temp[$response_data_row_value_key] = $response_data_row_value_details;
									
								}//close of foreach ($response_data_row_value as $response_data_row_value_details) {
								$result_rows_array[] = $temp;
								$temp_row_array = array();
								
								foreach ($result_rows_array as $response_data_row_array_value) {
									//print_r($response_data_row_array_value);

									$response_data_row_array_value_default = array();
									$response_data_row_array_value_default["job_id"] = null;
									$response_data_row_array_value_default["company_id"] = null;
									$response_data_row_array_value_default["company_client_id"] = null;
									$response_data_row_array_value_default["client_company_name"] = null;
									$response_data_row_array_value_default["job_title"] = null;
									$response_data_row_array_value_default["job_summary"] = null;
									$response_data_row_array_value_default["job_full_description"] = null;
									$response_data_row_array_value_default["job_type_name"] = null;
									$response_data_row_array_value_default["job_industry_name"] = null;
									$response_data_row_array_value_default["job_work_location_requirement_name"] = null;
									$response_data_row_array_value_default["job_recruitment_status"] = null;
									$response_data_row_array_value_default["resume_submission_end_date"] = null;
									$response_data_row_array_value_default["no_of_openings"] = null;
									$response_data_row_array_value_default["available_positions"] = null;
									$response_data_row_array_value_default["maximum_allowed_submittals_count"] = null;
									$response_data_row_array_value_default["address_line_1"] = null;
									$response_data_row_array_value_default["address_line_2"] = null;
									$response_data_row_array_value_default["city"] = null;
									$response_data_row_array_value_default["state"] = null;
									$response_data_row_array_value_default["country"] = null;
									$response_data_row_array_value_default["expected_job_start_date"] = null;
									$response_data_row_array_value_default["zipcode"] = null;
									$response_data_row_array_value_default["primary_recruiter_id"] = null;
									$response_data_row_array_value_default["primary_recruiter_name"] = null;
									$response_data_row_array_value_default["ot"] = null;
									$response_data_row_array_value_default["job_references"] = null;
									$response_data_row_array_value_default["drug_test"] = null;
									$response_data_row_array_value_default["background_check"] = null;
									$response_data_row_array_value_default["travel"] = null;
									$response_data_row_array_value_default["security_clearance"] = null;
									
									$result = array_merge($response_data_row_array_value_default, $response_data_row_array_value);
									$count++;
									
									/* $temp_row_array["job_id"] = $response_data_row_value["job_id"];
									$temp_row_array["company_id"] = $response_data_row_value["company_id"];
									$temp_row_array["company_client_id"] = $response_data_row_value["company_client_id"]; */
									/* $temp_row_array["job_posted_by_member_classification_detail_id"] = $response_data_row_value["job_posted_by_member_classification_detail_id"];
									$temp_row_array["job_posted_by_sm_memb_id"] = $response_data_row_value["job_posted_by_sm_memb_id"]; */
									/* $temp_row_array["job_title"] = $response_data_row_value["job_title"];
									$temp_row_array["job_summary"] = $response_data_row_value["job_summary"];
									$temp_row_array["job_full_description"] = $response_data_row_value["job_full_description"];
									$temp_row_array["job_type_name"] = $response_data_row_value["job_type_name"];
									$temp_row_array["job_industry_name"] = $response_data_row_value["job_industry_name"];
									$temp_row_array["job_work_location_requirement_name"] = $response_data_row_value["job_work_location_requirement_name"];
									$temp_row_array["job_recruitment_status"] = $response_data_row_value["job_recruitment_status"];
									$temp_row_array["resume_submission_end_date"] = $response_data_row_value["resume_submission_end_date"];
									$temp_row_array["no_of_openings"] = $response_data_row_value["no_of_openings"];
									$temp_row_array["available_positions"] = $response_data_row_value["available_positions"];
									
									$temp_row_array["maximum_allowed_submittals_count"] = $response_data_row_value["maximum_allowed_submittals_count"];
									$temp_row_array["address_line_1"] = $response_data_row_value["address_line_1"];
									$temp_row_array["address_line_2"] = $response_data_row_value["address_line_2"];
									$temp_row_array["city"] = $response_data_row_value["city"];
									$temp_row_array["state"] = $response_data_row_value["state"];
									$temp_row_array["country"] = $response_data_row_value["country"];
									/* $temp_row_array["job_posted_date_time"] = $response_data_row_value["job_posted_date_time"]; */
									/* $temp_row_array["expected_job_start_date"] = $response_data_row_value["expected_job_start_date"];
									$temp_row_array["zipcode"] = $response_data_row_value["zipcode"];
									$temp_row_array["primary_recruiter_id"] = $response_data_row_value["primary_recruiter_id"];
									$temp_row_array["primary_recruiter_name"] = $response_data_row_value["primary_recruiter_name"];
							
									
									$temp_row_array["ot"] = $response_data_row_value["ot"];
									$temp_row_array["job_references"] = $response_data_row_value["job_references"];
									$temp_row_array["travel"] = $response_data_row_value["travel"];
									$temp_row_array["drug_test"] = $response_data_row_value["drug_test"];
									$temp_row_array["background_check"] = $response_data_row_value["background_check"];
									$temp_row_array["security_clearance"] = $response_data_row_value["security_clearance"];  */
				
							
									$temp_row_array["list"][] = $result;
									
									
									
								}//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {
									$constructed_array["list"] = array_unique($temp_row_array["list"], SORT_REGULAR);
									$constructed_array["count"] = $count;
								
							} else {
								
								
							}//close of else of if ($response_data_row_key == "_source") {
							
											
							/* echo "<br>";
							echo "response_data_row_key: " . $response_data_row_key . "<br>";
							echo "response_data_row_value: <br>";
							print_r($response_data_row_value); */
							
						}//close of foreach ($response_data_row as $response_data_row_key => $response_data_row_value) {
							
						//exit;    
					}//close of foreach ($response['hits']['hits'] as $response_data_row) {

	
			return $constructed_array;
	//}
	//return $constructed_array;
}


	

exit;
?>