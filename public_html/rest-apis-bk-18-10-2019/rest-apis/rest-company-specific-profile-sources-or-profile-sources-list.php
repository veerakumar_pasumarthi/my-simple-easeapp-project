<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "2")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {


				if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else if ($company_id_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Invalid company id, Please check and try again.";
					
					$eventLog->log("Please provide valid company id.");
					
				} else {
					
					
					try { 
							//all inputs are valid
							$profile_sources = array();
							$company_specific_profile_sources_list_info_result = company_specific_profile_sources_list_info($company_id_input);
							$profile_sources_list_info_result = profile_sources_list_info();
							
							$profile_sources["company_specific_profile_sources"] = $company_specific_profile_sources_list_info_result;
							$profile_sources["application_specific_profile_sources"] = $profile_sources_list_info_result;
							
							if (count($profile_sources) > 0) {
								
								$response['data'] = $profile_sources;
								$response['status'] = "profile-sources-list-fetched";
								$response['status_description'] = "Profile Sources List fetched Successfully";
								
								$eventLog->log("Profile Sources List fetched Successfully");
								
								
							} else {
				
								$response['data'] = array();
								$response['status'] = "no-profile-sources-found";
								$response['status_description'] = "No Profile Sources found";
								
								
							}//close of else of if ($user_group_list_result_count > "0") {
									
					} catch (Exception $e){
						
						$response['data'] = array();
						$response['status'] = "profile-sources-fetching-error";
						$response['status_description'] = "Error occurred while fetching profile sources";
						
					}//close of  catch (Exception $e){
					
					
				
					
					
			}
					
		}
			
			
	} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){

function company_specific_profile_sources_list_info($company_id_input) {
	global $dbcon;
	$is_active_status = "1";
	$constructed_array = array();
	$company_specific_profile_sources_check_sql = "SELECT * FROM `company_specific_profile_sources` WHERE `company_id`=:company_id AND `is_active_status` =:is_active_status";
	$company_specific_profile_sources_check_select_query = $dbcon->prepare($company_specific_profile_sources_check_sql);
	$company_specific_profile_sources_check_select_query->bindValue(":company_id",$company_id_input);
	$company_specific_profile_sources_check_select_query->bindValue(":is_active_status",$is_active_status);
	$company_specific_profile_sources_check_select_query->execute();

	if($company_specific_profile_sources_check_select_query->rowCount() > 0) {
		$company_specific_profile_sources_check_select_query_result = $company_specific_profile_sources_check_select_query->fetchAll();
	     return $company_specific_profile_sources_check_select_query_result;

	}//close of if($company_details_check_select_query->rowCount() > 0) {
	return $constructed_array;
}
function profile_sources_list_info() {
	global $dbcon;
	$is_active_status = '1';
	$constructed_array = array();
	$profile_sources_check_sql = "SELECT * FROM `profile_sources` WHERE `is_active_status`= :is_active_status"; 
	$profile_sources_check_select_query = $dbcon->prepare($profile_sources_check_sql);
	$profile_sources_check_select_query->bindValue(":is_active_status",$is_active_status);
	$profile_sources_check_select_query->execute();

	if($profile_sources_check_select_query->rowCount() > 0) {
		$profile_sources_check_select_query_result = $profile_sources_check_select_query->fetchAll();
	     return $profile_sources_check_select_query_result;

	}//close of if($company_details_check_select_query->rowCount() > 0) {
	return $constructed_array;
}
	
exit;
?>