<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * REST API for candidate all education qualifications collect
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "4")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_client_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['user_id']))
                    
                    if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
					$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id']))					
					
					
						
					if (isset($ea_received_rest_ws_raw_array_input['client_spoc_details'])) {
						$content .= json_encode($ea_received_rest_ws_raw_array_input['client_spoc_details']) . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['skill_related_all_certifications'])) 	
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					//$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$company_client_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_client_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_client_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$client_spoc_details_input = $ea_received_rest_ws_raw_array_input['client_spoc_details'];
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//Check if all inputs are received correctly from the REST Web Service
				if ($company_client_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-client-id";
					$response['status_description'] = "Missing Company Client Id";
					
					$eventLog->log("missing-company-client-id: Please provide a valid Company Client Id.");
					
				} else if ($company_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company ID";
					
					$eventLog->log("missing-company-id: Please provide a valid Company ID.");
					
					
				} else if ($client_spoc_details_input == "") {	
					//Invalid Education Qualifications Received scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "empty-client-spoc-details-input";
					$response['status_description'] = "Empty Client Spoc Details Input details";
					
					$eventLog->log("empty-client-spoc-details-input: Client Spoc Details Input details are not received for this Candidate, Please check and try again.");
				
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Missing Some Additional Information";
					
					$eventLog->log("missing-some-additional-information: Some Additional Information is missing, please check and try again.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					$ea_action_specific_auth_check_result = "";
					
					//User Details, based on User ID input, through api request
				//	$user_basic_details_result = user_basic_details_check_based_on_user_id($user_id_input);
					
					//if (count($user_basic_details_result) > 0) {
						
					//	$user_rel_company_id = $user_basic_details_result["company_id"];
					//	$user_rel_sm_user_status = $user_basic_details_result["sm_user_status"];
						
					//	$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
						//$ea_action_specific_auth_check_result = ea_check_action_specific_user_privileges_different_user_levels_reset_password_feature($ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_user_company_id, $ea_extracted_jwt_token_sub, $user_id_input, $ea_extracted_jwt_token_user_privileges_list, $user_rel_company_id, $page_filename);					
						
						//echo "ea_action_specific_auth_check_result: " . $ea_action_specific_auth_check_result;					
							
							
						
					
						
					//	if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
								$eventLog->log("inter into try");
								//Event Time, as per Indian Standard Time
								$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
								
									
								//Define an array, to Collect "degree" and "field of study" field values of all received Education Qualification info.
								$all_received_skill_related_certification_fields_array = array();
								
								//Define an array, to Collect "degree" and "field of study" field values of all Education Qualification info, from the Database.
								$all_db_based_skill_related_certification_fields_array = array();
								
								
								//Define an array, to Collect all received Education Qualification info, with corresponding record processing status and info
								$all_collected_content_with_result_array = array();
								$eventLog->log("before count");
								//Collect All Education Qualifications List, of particular candidate, to extract degree and field of study field content
								$candidate_skill_certification_records_list_result = company_client_spocs_list($company_id_input);
								$eventLog->log("after count");
								$candidate_skill_certification_records_list_result_count = count($candidate_skill_certification_records_list_result);
								
								$eventLog->log("candidate_skill_certification_records_list_result_count: " . $candidate_skill_certification_records_list_result_count);
								
								$input_primary_keys_array = array();
								if ((is_array($client_spoc_details_input)) && ((count($client_spoc_details_input) > 0) || ($candidate_skill_certification_records_list_result_count > 0))) {
										
									//Do process each record (Education Qualification)
									foreach ($client_spoc_details_input as $skill_related_all_certifications_input_row) {
										
										$received_skill_certification_name_fields_array = array();
										$collected_content_with_result_array = array();
										
										$collected_content_with_result_array = $skill_related_all_certifications_input_row;
										
										
										$collected_content_with_result_array_json_encoded = json_encode($collected_content_with_result_array);
										$eventLog->log("collected_array row initial stage: " . $collected_content_with_result_array_json_encoded);	
										
										$skill_related_all_certifications_input_row_count = count($skill_related_all_certifications_input_row);
										//$eventLog->log("collected_array row initial stage count: " . $all_education_qualifications_input_row_count);
										
										if (count($skill_related_all_certifications_input_row) == 3) {
											
											$eventLog->log("inside count(skill_related_all_certifications_input_row) == 4 if condition: ");
											
											
											//[{"ceq_id":"","degree":"dgfgf","field_of_study":"ghghgh","institution_name":"fhghghg","university_name":"","board_name":"","year_of_passout":"2012","earned_percentage":"65","earned_grade":""},{"ceq_id":"","degree":"gfgfg","field_of_study":"fgdfgf","institution_name":"fgdfg","university_name":"","board_name":"","year_of_passout":"2011","earned_percentage":"75","earned_grade":""}]
										
											$company_client_spoc_name_id = trim(isset($skill_related_all_certifications_input_row["company_client_spoc_name_id"]) ? filter_var($skill_related_all_certifications_input_row["company_client_spoc_name_id"], FILTER_SANITIZE_NUMBER_INT) : '');
											$spoc_fullname = trim(isset($skill_related_all_certifications_input_row["spoc_fullname"]) ? filter_var($skill_related_all_certifications_input_row["spoc_fullname"], FILTER_SANITIZE_STRING) : '');
											
											$spoc_email= trim(isset($skill_related_all_certifications_input_row["spoc_email"]) ? filter_var($skill_related_all_certifications_input_row["spoc_email"], FILTER_SANITIZE_EMAIL) : '');
											
											//Do process the record (Education Qualification), and Collect "degree" and "field of study" field values of received Education Qualification
										//	$received_skill_Certification_name_fields_array["spoc_fullname"] = $skill_related_all_certifications_input_row["spoc_fullname"];
											//$received_degree_fieldofstudy_fields_array["educational_program"] = $all_skillsets_input_row["educational_program"];
											
											//Check if all record level inputs are received, as part of "all_education_qualifications" field, and correctly from the REST Web Service
											if ($spoc_fullname == "") {
												//Empty Degree Field
												
												$collected_content_with_result_array["spoc_fullname"] = $spoc_fullname;
												
												$eventLog->log("Empty Spoc Fullname input");
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
											} else if ($spoc_email == "") {
												//Empty Field of Study Field
												
												$collected_content_with_result_array["spoc_email"] = $spoc_email;
												
												$eventLog->log("Empty Spoc Email input");
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
											}  else {
												//All skill related Certification Details are valid, for this record
												
												$eventLog->log("All skill related Certification Details are valid, for this record");
												
												/* COMMENTED TO PLACE THIS OUTSIDE CONDITION //Do process the record (Education Qualification), and Collect "degree" and "field of study" field values of received Education Qualification
												$received_degree_fieldofstudy_fields_array["degree"] = $all_education_qualifications_input_row["degree"];
												$received_degree_fieldofstudy_fields_array["field_of_study"] = $all_education_qualifications_input_row["field_of_study"];
												*/
									
												//Do Duplicate Check, for each Education Qualification, in the application, w.r.t. the particular candidate's sm_memb_id
												$eventLog->log("before duplicate check");
                                                 $candidate_skill_certification_details_duplicate_check_result = array();
												if($company_client_spoc_name_id != ""){
												$candidate_skill_certification_details_duplicate_check_result = company_client_spoc_details_duplicate_check_based_on_id($company_client_spoc_name_id);
												}
												
												if (count($candidate_skill_certification_details_duplicate_check_result) > 0) {
													//Do get the reference id
													$company_client_spoc_name_id_from_db = $candidate_skill_certification_details_duplicate_check_result["company_client_spoc_name_id"];
													$eventLog->log("company_client_spoc_name_id_from_db :");
													$eventLog->log($company_client_spoc_name_id_from_db);
													$eventLog->log("company_client_spoc_name_id:");
													$eventLog->log($company_client_spoc_name_id);
													
													$input_primary_keys_array[] = $company_client_spoc_name_id;
													//Check, if ceq_id, is same, from both sources (received from api & from db source, after duplicate check)
													if ($company_client_spoc_name_id == $company_client_spoc_name_id_from_db) {
														//ceq_id from the api request and from db source are matching, for particular education qualification record.
														$eventLog->log("company_client_spoc_name_id from the api request and from db source are matching, for particular education qualification record.");
														
														
														$collected_content_with_result_array["company_client_spoc_name_id"] = $company_client_spoc_name_id;
													
														$eventLog->log("Update Query Condition");
														
														//Do Update Query to existing record
														$candidate_skill_certification_update_result = update_client_spoc_details_based_on_id($spoc_fullname,$spoc_email,$ea_extracted_jwt_token_sub, $event_datetime, $current_epoch, $company_client_spoc_name_id);
														
														if ($candidate_skill_certification_update_result) {
															//Update Success
															$collected_content_with_result_array["query_type"] = "UPDATE-QUERY";
															$collected_content_with_result_array["query_status"] = "SUCCESS";
															$collected_content_with_result_array["input_record_validity_status"] = "VALID";
															
															$eventLog->log("Update Query Condition - Success");
															
														} else {
															$collected_content_with_result_array["query_type"] = "UPDATE-QUERY";
															$collected_content_with_result_array["query_status"] = "FAILURE";
															$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
											
															$eventLog->log("Update Query Condition - Failure");
															
														}//close of else of if ($candidate_education_qualification_update_result) {
														
														
													} else {
														//ceq_id from the api request and from db source are different, for particular education qualification record.
														$eventLog->log("company_client_spoc_name_id from the api request and from db source are different, for particular education qualification record.");
														
														$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
														
													}//close of else of if ($ceq_id == $ceq_id_from_db) {
														
													
												} else {
													
													$eventLog->log("Insert Query Condition");
													
													
													//Find the Sequence Number, for the New Education Qualification
												/*	$candidate_skill_certification_count_result = candidate_skill_certification_record_count($user_id_input);
													$eventLog->log("candidate_skill_certification_count_result");
													if (isset($candidate_skill_certification_count_result["current_count"])) {
														
														$candidate_current_skill_certification_set_count = $candidate_skill_certification_count_result["current_count"];
														
													} else {
														$candidate_skill_certification_count_result = 0;
														
													}//close of else of if (isset($candidate_current_education_qualification_count_result["current_count"])) { */
														
													
													$eventLog->log("before insert");


													$candidate_fullname_result = array();
														
														$candidate_fullname_result = company_client_spoc_details_duplicate_check($company_client_id_input,$company_id_input,$spoc_fullname,$spoc_email);
														
														
														if(count($candidate_fullname_result) > 0 ) {
															
															
																$collected_content_with_result_array["company_client_spoc_name_id"] = null;
																$collected_content_with_result_array["query_type"] = "INSERT-QUERY";
																$collected_content_with_result_array["query_status"] = "FAILURE";
																$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
															
															
														} else {
														
														
															
															
													//Do Insert Query to add a new record
													$candidate_skill_certification_insert_result = insert_company_client_spoc_details($company_client_id_input,$company_id_input,$spoc_fullname, $spoc_email,$ea_extracted_jwt_token_sub, $event_datetime, $current_epoch);
													
													
													if (isset($candidate_skill_certification_insert_result["last_inserted_id"])) {

														$input_primary_keys_array[] = $candidate_skill_certification_insert_result;
														//Insert Success
														$collected_content_with_result_array["company_client_spoc_name_id"] = $candidate_skill_certification_insert_result["last_inserted_id"];
														$collected_content_with_result_array["query_type"] = "INSERT-QUERY";
														$collected_content_with_result_array["query_status"] = "SUCCESS";
														$collected_content_with_result_array["input_record_validity_status"] = "VALID";
														
														$eventLog->log("Insert Query Condition - Success");
														//$additional_document_ref_input = $degree . " (" . $field_of_study . ")";
														$last_inserted_candidate_skill_certification_id = $candidate_skill_certification_insert_result["last_inserted_id"];
														//candidate_education_qualification_rel_file_upload_references_insert($user_id_input,"education-qualifications",$last_inserted_candidate_education_qualification_id,$additional_document_ref_input,null,null,$ea_extracted_jwt_token_user_type,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
														
														//COMMENTED< PLEASE CHANGE THIS FUNCTION candidate_vrpi_rel_file_upload_references_insert($user_id_input,"visa-rel-personal-info",$last_inserted_candidate_visa_rel_personal_info_id,$additional_document_ref,null,null,$ea_extracted_jwt_token_user_type,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
														
													} else {
														//Insert Failure
														$collected_content_with_result_array["company_client_spoc_name_id"] = null;
														$collected_content_with_result_array["query_type"] = "INSERT-QUERY";
														$collected_content_with_result_array["query_status"] = "FAILURE";
														$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
										
														$eventLog->log("Insert Query Condition - Failure");
														
													}//close of else of if (isset($candidate_education_qualification_insert_result["last_inserted_id"])) {
													}	
													
												}//close of else of if (count($candidate_education_qualification_check_result) > 0) {
												
												$collected_content_with_result_array_json_encoded = json_encode($collected_content_with_result_array);
												//$eventLog->log("collected_array row next stage: " . $collected_content_with_result_array_json_encoded);	
												
												
											}//close of else of if ($degree == "") {
												
											
										} else {
											
											$eventLog->log("inside count(all_education_qualifications_input_row) == 10 else condition: ");
											
											//Expected number of fields are not sent, per education qualification record
											$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
										
										}//close of else of if (count($all_education_qualifications_input_row)) {
										
										$all_received_skill_related_certification_fields_array[] = $received_skill_certification_name_fields_array;
										$all_collected_content_with_result_array[] = $collected_content_with_result_array;
																								
										
									}//close of foreach ($all_education_qualifications_input as $all_education_qualifications_input_row) {
									
									$all_collected_content_with_result_array_json_encoded = json_encode($all_collected_content_with_result_array);
									$eventLog->log("collected_array final (didnot enter foreach loop): " . $all_collected_content_with_result_array_json_encoded);	
									
									
									/*//Collect All Education Qualifications List, of particular candidate, to extract degree and field of study field content
									$candidate_education_qualification_records_list_result = candidate_education_qualification_records_list($user_id_input);
									$candidate_education_qualification_records_list_result_count = count($candidate_education_qualification_records_list_result);
									
									$eventLog->log("candidate_education_qualification_records_list_result_count: " . $candidate_education_qualification_records_list_result_count);
									*/
									
									if (count($candidate_skill_certification_records_list_result) > 0) {
										//One or More than one Education Qualification is received from the Database.
										
										$all_received_skill_certification_name_fields_array_json_encoded = json_encode($all_received_skill_related_certification_fields_array);
										$eventLog->log("all_received_skill_certification_name_fields_array_json_encoded : " . $all_received_skill_certification_name_fields_array_json_encoded);	
										
										//Do process the record (Education Qualification), and Collect "degree" and "field of study" field values of received Education Qualification
										foreach($candidate_skill_certification_records_list_result as $candidate_skill_certification_records_list_result_row) {
											
											$db_company_client_spoc_name_id = $candidate_skill_certification_records_list_result_row["company_client_spoc_name_id"];
											if (in_array($db_company_client_spoc_name_id, $input_primary_keys_array)) {
												
												
											
											} else {
												
												
												$delete_result = delete_company_client_spoc_details_based_on_id_input($db_company_client_spoc_name_id);
									
											}
											
										}//close of foreach($candidate_education_qualification_records_list_result as $candidate_education_qualification_records_list_result_row) {
										
										
									} else {
										//Education Qualifications of the Candidate are not available in the Database, this means, Nothing to Remove.
										$eventLog->log("Education Qualifications of the Candidate are not available in the Database, this means, Nothing to Remove.");
										
									}//close of else of if (count($candidate_education_qualification_records_list_result) > 0) {
										
									
									$all_collected_content_with_result_array_json_encoded = json_encode($all_collected_content_with_result_array);
									$eventLog->log("all_collected_content_with_result_array_json_encoded: " . $all_collected_content_with_result_array_json_encoded);
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = $all_collected_content_with_result_array;
									$response['status'] = "company-client-spoc-data-ingestion-result";
									$response['status_description'] = "All Company Client Spoc Data is processed.";
									
									$eventLog->log("company-client-spoc-data-ingestion-result: All Company Client Spoc Data is processed.");	
									
								} else {
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "no-records-received";
									$response['status_description'] = "Please Submit at least One Spoc Name.";
									
									$eventLog->log("no-records-received: Please Submit at least One Skill certification.");	
									
								}//close of else of if ((is_array($all_education_qualifications_input)) && ((count($all_education_qualifications_input) > 0) || ($candidate_education_qualification_records_list_result_count > 0))) {
								
								
							} catch (Exception $e) {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "company-client-spoc-dtails-processing-error";
								$response['status_description'] = "Error occurred when processing Company Client Spoc Details.";
								
								$eventLog->log("company-client-spoc-dtails-processing-error: Error occurred when processing Company Client Spoc Details.");	
								
							} 
					/*	} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-subject-platform-scope-user";
							$response['status_description'] = "Insufficient Set of Permissions, to complete this action";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-subject-platform-scope-user: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-different-company-user-id";
							$response['status_description'] = "Insufficient Set of Permissions, as the User belongs to a different company";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-INSUFFICIENT-PERMISSIONS") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions, as this process will not be allowed any further";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-self-action";
							$response['status_description'] = "Insufficient Set of Permissions, as self action is Dis-allowed";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-disallowed-self-action: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-ACTION-SUBJECT-IS-A-DIFFERENT-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-action-on-different-user";
							$response['status_description'] = "Insufficient Set of Permissions, as action subject is a different user, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-disallowed-action-on-different-user: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						}//close of if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
						
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "Invalid User Account that is attempted to be edited";
						
						$eventLog->log("invalid-user-account: The User Account, that is attempted to be edited, is invalid, please check and try again.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
                          */						
					
				}//close of else of if ($user_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token";
			
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");	
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input";
	
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));	
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){


function company_client_spoc_details_duplicate_check($company_client_id_input,$company_id_input,$spoc_fullname,$spoc_email) {
	
	global $dbcon;
	
	$constructed_array = array();
	
	
	$candidate_skill_certificate_check_sql = "SELECT * FROM `company_client_spoc_names` WHERE `company_id`=:company_id AND  `spoc_fullname` LIKE :spoc_fullname  AND`spoc_email` LIKE :spoc_email AND `company_client_id`=:company_client_id";
	$candidate_skill_certificate_check_select_query = $dbcon->prepare($candidate_skill_certificate_check_sql);
	$candidate_skill_certificate_check_select_query->bindValue(":company_client_id",$company_client_id_input);
    $candidate_skill_certificate_check_select_query->bindValue(":company_id",$company_id_input);		
	$candidate_skill_certificate_check_select_query->bindValue(":spoc_fullname",$spoc_fullname);
	$candidate_skill_certificate_check_select_query->bindValue(":spoc_email",$spoc_email);
	$candidate_skill_certificate_check_select_query->execute(); 
	
	if($candidate_skill_certificate_check_select_query->rowCount() > 0) {
		$candidate_skill_certificate_check_select_query_result = $candidate_skill_certificate_check_select_query->fetch();
	    return $candidate_skill_certificate_check_select_query_result;
	
	}//close of if($candidate_education_qualification_check_select_query->rowCount() > 0) {
	return $constructed_array;
	
}


function company_client_spoc_details_duplicate_check_based_on_id($company_client_spoc_name_id) {
	
	global $dbcon;
	
	$constructed_array = array();
	
	
	
	$candidate_skill_certificate_check_sql = "SELECT * FROM `company_client_spoc_names` WHERE `company_client_spoc_name_id`=:company_client_spoc_name_id";
	$candidate_skill_certificate_check_select_query = $dbcon->prepare($candidate_skill_certificate_check_sql);
	$candidate_skill_certificate_check_select_query->bindValue(":company_client_spoc_name_id",$company_client_spoc_name_id);
   
	$candidate_skill_certificate_check_select_query->execute(); 
	
	if($candidate_skill_certificate_check_select_query->rowCount() > 0) {
		$candidate_skill_certificate_check_select_query_result = $candidate_skill_certificate_check_select_query->fetch();
	    return $candidate_skill_certificate_check_select_query_result;
	
	}//close of if($candidate_education_qualification_check_select_query->rowCount() > 0) {
	return $constructed_array;
	
}


function update_client_spoc_details_based_on_id($spoc_fullname,$spoc_email,$ea_extracted_jwt_token_sub, $event_datetime, $current_epoch, $company_client_spoc_name_id) {
	global $dbcon;
	
	$candidate_skill_certification_details_update_sql = "UPDATE `company_client_spoc_names` SET `spoc_fullname`=:spoc_fullname,`spoc_email`=:spoc_email,`last_updated_by_sm_memb_id`=:last_updated_by_sm_memb_id,`last_updated_datetime`=:last_updated_datetime,`last_updated_datetime_epoch`=:last_updated_datetime_epoch WHERE `company_client_spoc_name_id`=:company_client_spoc_name_id";	
	
	$candidate_skill_certification_details_update_query = $dbcon->prepare($candidate_skill_certification_details_update_sql);		
	$candidate_skill_certification_details_update_query->bindValue(":spoc_fullname",$spoc_fullname);
	$candidate_skill_certification_details_update_query->bindValue(":spoc_email",$spoc_email);
	$candidate_skill_certification_details_update_query->bindValue(":last_updated_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$candidate_skill_certification_details_update_query->bindValue(":last_updated_datetime",$event_datetime);
	$candidate_skill_certification_details_update_query->bindValue(":last_updated_datetime_epoch",$current_epoch);
	$candidate_skill_certification_details_update_query->bindValue(":company_client_spoc_name_id",$company_client_spoc_name_id);
	
	if ($candidate_skill_certification_details_update_query->execute()) {
		return true;
		
	}//close of if ($candidate_education_qualification_details_update_query->execute()) {
	
	return false;
}



function insert_company_client_spoc_details($company_client_id_input,$company_id_input,$spoc_fullname, $spoc_email,$ea_extracted_jwt_token_sub, $event_datetime, $current_epoch) {
	
	global $dbcon;
	
	$constructed_array = array();
	
	//Do Insert Query

	$candidate_skill_certification_insert_sql = "INSERT INTO `company_client_spoc_names`(`company_id`, `company_client_id`, `spoc_fullname`, `spoc_email`, `added_by_sm_memb_id`, `added_datetime`, `added_datetime_epoch`,`is_active_status`) VALUES (:company_id,:company_client_id,:spoc_fullname,:spoc_email,:added_by_sm_memb_id,:added_datetime,:added_datetime_epoch,:is_active_status)";
	
	$candidate_skill_certification_insert_query = $dbcon->prepare($candidate_skill_certification_insert_sql);
	$candidate_skill_certification_insert_query->bindValue(":company_client_id",$company_client_id_input);
	$candidate_skill_certification_insert_query->bindValue(":company_id",$company_id_input);
	$candidate_skill_certification_insert_query->bindValue(":spoc_fullname",$spoc_fullname);
	$candidate_skill_certification_insert_query->bindValue(":spoc_email",$spoc_email);
	
	$candidate_skill_certification_insert_query->bindValue(":added_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$candidate_skill_certification_insert_query->bindValue(":added_datetime",$event_datetime);
	$candidate_skill_certification_insert_query->bindValue(":added_datetime_epoch",$current_epoch);
	$candidate_skill_certification_insert_query->bindValue(":is_active_status",1);
	
	if ($candidate_skill_certification_insert_query->execute()) {
		
		$last_inserted_id = $dbcon->lastInsertId();			
		//$eventLog->log("record inserted successfully");
		
		$constructed_array["last_inserted_id"] = $last_inserted_id;
		
		return $constructed_array;
					
	} else {
		//$eventLog->log("Error occurred during process. Please try again");						
			return $constructed_array;						
	}

}

function candidate_skill_certification_record_count($user_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	
	$candidate_skill_certification_record_count_sql = "SELECT COUNT(*) AS count FROM `candidate_rel_skillset_certifications` WHERE `sm_memb_id`=:sm_memb_id";
	$candidate_skill_certification_record_count_select_query = $dbcon->prepare($candidate_skill_certification_record_count_sql);
	$candidate_skill_certification_record_count_select_query->bindValue(":sm_memb_id",$user_id_input);			
	$candidate_skill_certification_record_count_select_query->execute(); 
	
	if($candidate_skill_certification_record_count_select_query->rowCount() > 0) {
		$candidate_skill_certification_record_count_select_query_result = $candidate_skill_certification_record_count_select_query->fetch();
		$current_count = $candidate_skill_certification_record_count_select_query_result["count"];
		
		$constructed_array["current_count"] = $current_count;
		
	    return $constructed_array;
	
	} else {
		$constructed_array["current_count"] = 0;
		
		return $constructed_array;
		
	}//close of if($candidate_education_qualification_record_count_select_query->rowCount() > 0) {
	
	
}


function company_client_spocs_list($company_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	
	$candidate_skill_certification_records_list_sql = "SELECT * FROM `company_client_spoc_names` WHERE `company_id`=:company_id";
	$candidate_skill_certification_records_list_select_query = $dbcon->prepare($candidate_skill_certification_records_list_sql);
	$candidate_skill_certification_records_list_select_query->bindValue(":company_id",$company_id_input);			
	$candidate_skill_certification_records_list_select_query->execute(); 
	
	if($candidate_skill_certification_records_list_select_query->rowCount() > 0) {
		$candidate_skill_certification_records_list_select_query_result = $candidate_skill_certification_records_list_select_query->fetchAll();
		return $candidate_skill_certification_records_list_select_query_result;
	
	}//close of if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
	return $constructed_array;
}

function delete_company_client_spoc_details_based_on_id_input($company_client_spoc_name_id) {
	global $dbcon;
	
	$candidate_educational_qualification_record_delete_sql = "DELETE FROM `company_client_spoc_names` WHERE `company_client_spoc_name_id`=:company_client_spoc_name_id";
	$candidate_educational_qualification_record_delete_query = $dbcon->prepare($candidate_educational_qualification_record_delete_sql);
	$candidate_educational_qualification_record_delete_query->bindValue(":company_client_spoc_name_id",$company_client_spoc_name_id);		
	if ($candidate_educational_qualification_record_delete_query->execute()) {
		return true;
		
	}//close of if ($candidate_professional_summary_record_delete_query->execute()) {
	
	return false;
	
}

exit;
?>