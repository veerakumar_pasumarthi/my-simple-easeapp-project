<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Companies, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "5")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
				    if (isset($ea_received_rest_ws_raw_array_input['job_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_id'] . "\r\n";
				    }//close of if (isset($ea_received_rest_ws_raw_array_input['job_id']))
						
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
				    }//close of if (isset($ea_received_rest_ws_raw_array_input['company_id']))
						
                    if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_client_id'] . "\r\n";
				    }//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id']))
						
					if (isset($ea_received_rest_ws_raw_array_input['consider_user_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['consider_user_id'] . "\r\n";
				    }//close of if (isset($ea_received_rest_ws_raw_array_input['consider_user_id']))
                    				
                    if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))						
				
				//Filter Inputs
			
				$job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$company_client_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_client_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_client_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				
				$consider_user_id_input= trim(isset($ea_received_rest_ws_raw_array_input['consider_user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['consider_user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
			
			
				
				//Check if all inputs are received correctly from the REST Web Service
				if ($job_id_input == "") {
					//Invalid company Status scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-job-id";
					$response['status_description'] = "Invalid job id submitted, please check and try again.";
					
					$eventLog->log("Please provide a valid job id .");
				
				}else if ($consider_user_id_input == "") {
					//Invalid company Status scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-consider-user-id";
					$response['status_description'] = "Invalid consider user id submitted, please check and try again.";
					
					$eventLog->log("invalid-consider-user-id.");
					
				}else if ($company_id_input == "") {
					//Invalid company Status scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-company-id";
					$response['status_description'] = "Invalid company id submitted, please check and try again.";
					
					$eventLog->log("Please provide a valid company id .");
					
				}else if ($company_client_id_input == "") {
					//Invalid company Status scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-company-client-id";
					$response['status_description'] = "Invalid company client id submitted, please check and try again.";
					
					$eventLog->log("Please provide a valid company client id .");	
					
					
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-additional-information";
					$response['status_description'] = "Some Additional Information like IP Address (IPv4) is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");
							  
				} else {
					//All inputs are Valid
				
					$eventLog->log("All inputs are valid.");
					
					
					$job_candidate_consider_duplicate_check_result = job_candidate_consider_duplicate_check_based_on_job_id($job_id_input,$company_id_input,$company_client_id_input,$consider_user_id_input);
					
					
					if (count($job_candidate_consider_duplicate_check_result) > 0) {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "candidate-already-considered-for-this-job";
						$response['status_description'] = "Candidate already Considered for this job";
						
                        $eventLog->log("candidate-already-considered-for-this-job: Candidate already Considered for this job");						
					
					} else {
						
					//Event Time, as per Indian Standard Time
					 $event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
					 
					 $job_applicant_invite_next_step = "";
						
						if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$job_applicant_invite_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$job_applicant_invite_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
						} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
						}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
							
						if ($job_applicant_invite_next_step == "PROCEED-TO-NEXT-STEP") {
							try { 
							  
									$last_inserted_id = job_rel_considered_candidate_insert($company_id_input,$company_client_id_input,$job_id_input,$consider_user_id_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
					 
									if ($last_inserted_id != "") {
										
										    $response['data'] = array();
											$response['status'] = "candidate-considered-successfully-for-this-job";
											$response['status_description'] = "Considered successfully.";
										
											$eventLog->log("Candidate considered successfully for this Job.");   
										
									} else {
										//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
										$response['data'] = array();
										$response['status'] = "consider-request-insert-error";
										$response['status_description'] = "Consider error.";
										
										$eventLog->log("Error occurred when considering the candidate.");
										
									}//close of else of if (count($user_ass_oc_validation_result) > 0) {
									
							         
									
								
							}catch (Exception $e) {
								
								//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
								$response['data'] = array();
								$response['status'] = "consider-request-insert-error";
								$response['status_description'] = "No records found.";
								
								$eventLog->log("Error occurred when considering the candidate.");
							}
						}//close of if ($job_applicant_invite_next_step == "PROCEED-TO-NEXT-STEP") {
							
						
					}
				}//close of else of if ($email_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
		}				
					 
	} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){

function job_candidate_consider_duplicate_check_based_on_job_id($job_id_input,$company_id_input,$company_client_id_input,$consider_user_id_input) {

	global $dbcon;
	$constructed_array = array();
	$job_candidate_consider_duplicate_check_sql = "SELECT * FROM `job_applicant_consider_proposals` WHERE `job_id`= :job_id AND `company_id`=:company_id AND`company_client_id`=:company_client_id AND `considered_candidate_user_id`=:considered_candidate_user_id";
	$job_candidate_consider_details_select_query = $dbcon->prepare($job_candidate_consider_duplicate_check_sql);
	$job_candidate_consider_details_select_query->bindValue(":job_id",$job_id_input);
	$job_candidate_consider_details_select_query->bindValue(":company_id",$company_id_input);
	$job_candidate_consider_details_select_query->bindValue(":company_client_id",$company_client_id_input);
	$job_candidate_consider_details_select_query->bindValue(":considered_candidate_user_id",$consider_user_id_input);
	$job_candidate_consider_details_select_query->execute();

	if($job_candidate_consider_details_select_query->rowCount() > 0) {
		$job_candidate_consider_details_select_query_result = $job_candidate_consider_details_select_query->fetchAll();
	     return $job_candidate_consider_details_select_query_result;

	}//close of if($job_applicant_invite_details_select_query_result ->rowCount() > 0) {
	return $constructed_array;

}

function job_rel_considered_candidate_insert($company_id_input,$company_client_id_input,$job_id_input,$consider_user_id_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch){
	global $dbcon,$eventLog;
	$is_active_status = '1';
	$consider_proposal_initiated_by = '1';

	$job_rel_considered_candidate_insert_sql = 	"INSERT INTO `job_applicant_consider_proposals`(`company_id`, `company_client_id`, `job_id`, `consider_proposal_initiated_by`, `consideration_initiated_by_user_id`, `considered_candidate_user_id`, `event_date_time`, `event_date_time_epoch`,`is_active_status`) VALUES (:company_id,:company_client_id,:job_id,:consider_proposal_initiated_by,:consideration_initiated_by_user_id,:considered_candidate_user_id,:event_date_time,:event_date_time_epoch,:is_active_status)";

	
	
	$job_rel_considered_candidate_insert_query = $dbcon->prepare($job_rel_considered_candidate_insert_sql);
	$job_rel_considered_candidate_insert_query->bindValue(":company_id",$company_id_input);
	$job_rel_considered_candidate_insert_query->bindValue(":company_client_id",$company_client_id_input);
	$job_rel_considered_candidate_insert_query->bindValue(":job_id",$job_id_input);
	$job_rel_considered_candidate_insert_query->bindValue(":considered_candidate_user_id",$consider_user_id_input);
	$job_rel_considered_candidate_insert_query->bindValue(":consider_proposal_initiated_by",$consider_proposal_initiated_by);
	$job_rel_considered_candidate_insert_query->bindValue(":consideration_initiated_by_user_id",$ea_extracted_jwt_token_sub);
	$job_rel_considered_candidate_insert_query->bindValue(":event_date_time",$event_datetime);
    $job_rel_considered_candidate_insert_query->bindValue(":event_date_time_epoch",$current_epoch);
	$job_rel_considered_candidate_insert_query->bindValue(":is_active_status",$is_active_status);

		if ($job_rel_considered_candidate_insert_query->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			$eventLog->log("record inserted successfully");

			return $last_inserted_id;

		} else {
		    $eventLog->log("Error occurred during process. Please try again");
				return "";
	    }
}

exit;
?>