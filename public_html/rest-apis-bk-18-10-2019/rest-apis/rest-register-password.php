<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * REST API for registering password
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
 
if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "4")) {
				
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		if (is_array($ea_received_rest_ws_raw_array_input)) {
			$content = "";
			
			if (isset($ea_received_rest_ws_raw_array_input['password_request_code'])) {
				$content .= $ea_received_rest_ws_raw_array_input['password_request_code'] . ":::::";
			}//close of if (isset($ea_received_rest_ws_raw_array_input['password_request_code'])) {
			
			if (isset($ea_received_rest_ws_raw_array_input['password'])) {
				$content .= $ea_received_rest_ws_raw_array_input['password'] . ":::::";
			}//close of if (isset($ea_received_rest_ws_raw_array_input['password'])) {
			
			if (isset($ea_received_rest_ws_raw_array_input['password_confirmation'])) {
				$content .= $ea_received_rest_ws_raw_array_input['password_confirmation'] . ":::::";
			}//close of if (isset($ea_received_rest_ws_raw_array_input['password_confirmation'])) {
				
			if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
				$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
			}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {

			$eventLog->log("Received Inputs => ".$content);
			
			//DO WRITE REST WEB SERVICE AUTHORIZATION CHECK, for ALL REST WEB SERVICES, IN HERE.
			
		}//close of if ($ea_received_rest_ws_raw_array_input != "") {
			
		//Filter Inputs	
		$password_request_code_input = trim(isset($ea_received_rest_ws_raw_array_input['password_request_code']) ? filter_var($ea_received_rest_ws_raw_array_input['password_request_code'], FILTER_SANITIZE_STRING) : '');
		$password_input = trim(isset($ea_received_rest_ws_raw_array_input['password']) ? filter_var($ea_received_rest_ws_raw_array_input['password'], FILTER_SANITIZE_STRING) : '');
		$password_confirmation_input = trim(isset($ea_received_rest_ws_raw_array_input['password_confirmation']) ? filter_var($ea_received_rest_ws_raw_array_input['password_confirmation'], FILTER_SANITIZE_STRING) : '');
		
		//Check if the IP Address Input is a Valid IPv4 Address
		if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
			//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
			$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
		} else {
			$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
			$ip_address_input = '';
		}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
		
		
		//Check if all inputs are received correctly from the REST Web Service
		if ($password_request_code_input == "") {
			//Invalid Password Request Code scenario
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "empty-password-request-code";
			$response['status_description'] = "Empty Password Request Code Submitted, please submit a Valid Password Request Code.";
			
			$eventLog->log("Empty Password Request Code Submitted, please submit a Valid Password Request Code.");
			
		} else if ((($password_input == "") || ($password_confirmation_input == "") || (mb_strlen($password_input) < $password_min_length)) || ($password_input != $password_confirmation_input)) {
			//Password Mis-match scenario
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "password-inputs-mis-matched";
			$response['status_description'] = "Password inputs are mis-matching, please check and try again.";
			
			$eventLog->log("Password inputs are mis-matching, please check and try again.");
			
		} else if ((mb_strlen($password_input) < $password_min_length) || (mb_strlen($password_confirmation_input) < $password_min_length)) {
			//Invalid Password Length scenario
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-password-length";
			$response['status_description'] = "Invalid Password Length, please check and try again with apassword that is >=" . $password_min_length . " Characters in length.";
			
			$eventLog->log("Invalid Password Length, please check and try again with a password that is >=" . $password_min_length . " Characters in length.");
			
		} else if ($ip_address_input == "") {
			//One or More Inputs are Missing!!!
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "missing-additional-information";
			$response['status_description'] = "Some Additional Information like Password and / or IP Address (IPv4) is missing, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
			$eventLog->log("Please provide all information.");
					  
		} else {	
		    //All inputs are Valid
			
			//Event Time, as per Indian Standard Time
			$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
									
									
			$eventLog->log("All inputs are valid.");
			//$eventLog->log("Received Inputs - " . $content);
			try {
				
				//Get Details of Password Request Code
				$password_request_code_details_result = ea_get_user_account_password_request_details_based_on_password_request_code($password_request_code_input);
				
				if (count($password_request_code_details_result) > 0) {
					
					$password_setup_modification_request_id = $password_request_code_details_result["password_setup_modification_request_id"];
					$action_subject_sm_memb_id = $password_request_code_details_result["sm_memb_id"];
					$request_expiry_datetime_epoch = $password_request_code_details_result["request_expiry_datetime_epoch"];
					$is_active_status = $password_request_code_details_result["is_active_status"];
					
					//Check the request status
					if ($is_active_status == "1") {
						//Password Request is Active
						
						//Check, if particular Password Request Code is Valid / Expired
						if ($current_epoch > $request_expiry_datetime_epoch) {
							//Password Request Code got Expired
							
							$user_account_password_request_status_update_result = ea_update_user_account_password_request_status_based_on_is_active_status($event_datetime, $current_epoch, $action_subject_sm_memb_id, "3", $password_setup_modification_request_id);
							
							if ($user_account_password_request_status_update_result) {
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "password-request-code-expired";
								$response['status_description'] = "This Password Request Code got expired and cannot be used anymore, please check and try with a different Password Request Code.";
								
								$eventLog->log("This Password Request Code got expired and cannot be used anymore, please check and try with a different Password Request Code.");
								
							}//close of if ($user_account_password_request_status_update_result) {
								
							
						} else {
							//Password Request is Active
							
							//Create Password hash
							$password_hash = password_hash($password_input, PASSWORD_DEFAULT);
							
							$user_account_password_update_result = ea_update_user_account_password_based_on_sm_memb_id($password_hash, $event_datetime, $current_epoch, $action_subject_sm_memb_id);
							
							if ($user_account_password_update_result) {
								//Successful Password update
								
								//Do Update Password Request Status
								$user_account_password_request_status_update_result = ea_update_user_account_password_request_status_based_on_is_active_status($event_datetime, $current_epoch, $action_subject_sm_memb_id, "2", $password_setup_modification_request_id);
							
								if ($user_account_password_request_status_update_result) {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "password-request-successfully-utilized";
									$response['status_description'] = "Password is Successfully Updated.";
									
									$eventLog->log("Password is Successfully Updated.");
									
								} else {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "password-request-fulfillment-status-update-error";
									$response['status_description'] = "There occurred an error when updating the password request fulfillment status, please check and try again.";
									
									$eventLog->log("There occurred an error when updating the password request fulfillment status, please check and try again.");
									
								}//close of else of if ($user_account_password_request_status_update_result) {
									
							} else {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "password-request-fulfillment-error";
								$response['status_description'] = "There occurred an error when updating the account with the new password, please check and try again.";
								
								$eventLog->log("There occurred an error when updating the account with the new password, please check and try again.");
								
							}//close of else of if ($user_account_password_update_result) {
								
							
						}//close of else of if ($current_epoch > $request_expiry_datetime_epoch) {
							
						
					} else if ($is_active_status == "2") {
						//Password Request is Fulfilled
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "password-request-code-already-utilized";
						$response['status_description'] = "This Password Request Code is already utilized and cannot be used again, please check and try with a different Password Request Code.";
						
						$eventLog->log("This Password Request Code is already utilized and cannot be used again, please check and try with a different Password Request Code.");
						
					} else if ($is_active_status == "3") {
						//Password Request got Expired
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "password-request-code-expired";
						$response['status_description'] = "This Password Request Code got expired and cannot be used anymore, please check and try with a different Password Request Code.";
						
						$eventLog->log("This Password Request Code got expired and cannot be used anymore, please check and try with a different Password Request Code.");
						
					} else if ($is_active_status == "0") {
						//Password Request is made In-active
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "password-request-code-in-active";
						$response['status_description'] = "This Password Request Code is made In-active and cannot be used at this moment, please check and try with a different Password Request Code.";
						
						$eventLog->log("This Password Request Code is made In-active and cannot be used at this moment, please check and try with a different Password Request Code.");
						
					} else {
						//Invalid Password Request Status received
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-password-request-code";
						$response['status_description'] = "This Password Request Code is Invalid, please check and try with a valid Password Request Code.";
						
						$eventLog->log("This Password Request Code is Invalid, please check and try with a valid Password Request Code.");
						
					}//close of else of if ($is_active_status == "1") {
						
						
					
				} else {
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-password-request-code";
					$response['status_description'] = "This Password Request Code is Invalid, please check and try with a valid Password Request Code.";
					
					$eventLog->log("This Password Request Code is Invalid, please check and try with a valid Password Request Code.");
					
				}//close of else of if (count($password_request_code_details_result) > 0) {
				
				
			} catch (Exception $e) {
				$eventLog->log("Exception -> " . html_escaped_output($e->getMessage())); 
				//addLog($logFile, "Exception -> ".$e->getMessage());	
				
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "password-request-processing-error";
				$response['status_description'] = "There occurred an error when processing the new password, please check and try again.";
				
				$eventLog->log("There occurred an error when processing the new password, please check and try again.");
				
			}//close of catch (Exception $e) { 
			
			$eventLog->logNewSeperator();			
		}//close of else of if ($user_unique_identifier_string_setting == "") {
	}//close of if ($ea_maintenance_mode == false) {
	
} else {
	
	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	$response['jwt-audience'] = array();
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "4")) {

//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, with Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){

exit;
?>