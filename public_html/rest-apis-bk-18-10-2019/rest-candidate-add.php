<?php 
defined('START') or die; // to start the api

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */
 $eventLogFileName = $route_filename . "-log"; 
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "24"))

	{
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {  
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['salutation'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['salutation'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['salutation'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['firstname'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['firstname'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['firstname'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['middlename'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['middlename'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['middlename'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['lastname'])) {  
						$content .= $ea_received_rest_ws_raw_array_input['lastname'] . "\r\n"; 
					}//close of if (isset($ea_received_rest_ws_raw_array_input['lastname']))
					
					if (isset($ea_received_rest_ws_raw_array_input['mobile_number'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['mobile_number'] . "\r\n"; 
					}//close of if (isset($ea_received_rest_ws_raw_array_input['mobile_number']))
						
					if (isset($ea_received_rest_ws_raw_array_input['mobile_phone_country_code'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['mobile_phone_country_code'] . "\r\n"; 
					}//close of if (isset($ea_received_rest_ws_raw_array_input['mobile_phone_country_code'])) 
					
                    if (isset($ea_received_rest_ws_raw_array_input['work_phone_number'])) {
						$content .= $ea_received_rest_ws_raw_array_input['work_phone_number'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['work_phone_number']))
					
					if (isset($ea_received_rest_ws_raw_array_input['work_phone_country_code'])) {
						$content .= $ea_received_rest_ws_raw_array_input['work_phone_country_code'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['work_phone_country_code']))
					
                    if (isset($ea_received_rest_ws_raw_array_input['home_phone_number'])) {
						$content .= $ea_received_rest_ws_raw_array_input['home_phone_number'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['home_phone_number']))
						
					if (isset($ea_received_rest_ws_raw_array_input['home_phone_country_code'])) {
						$content .= $ea_received_rest_ws_raw_array_input['home_phone_country_code'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['home_phone_country_code']))	
						
					if (isset($ea_received_rest_ws_raw_array_input['fax_number'])) {
						$content .= $ea_received_rest_ws_raw_array_input['fax_number'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['fax_number']))
						
				    if (isset($ea_received_rest_ws_raw_array_input['fax_number_country_code'])) {
						$content .= $ea_received_rest_ws_raw_array_input['fax_number_country_code'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['fax_number_country_code']))
						
					if (isset($ea_received_rest_ws_raw_array_input['email_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['email_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['email_id']))
						
					if (isset($ea_received_rest_ws_raw_array_input['alternate_email_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['alternate_email_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['alternate_email_id']))	
						
					if (isset($ea_received_rest_ws_raw_array_input['address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['address']))
						
					if (isset($ea_received_rest_ws_raw_array_input['city'])) {
						$content .= $ea_received_rest_ws_raw_array_input['city'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['city']))	
						
					if (isset($ea_received_rest_ws_raw_array_input['state'])) {
						$content .= $ea_received_rest_ws_raw_array_input['state'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['state']))
					
					if (isset($ea_received_rest_ws_raw_array_input['country'])) {
						$content .= $ea_received_rest_ws_raw_array_input['country'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['country']))
					
				    if (isset($ea_received_rest_ws_raw_array_input['zipcode'])) {
						$content .= $ea_received_rest_ws_raw_array_input['zipcode'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['zipcode']))
						
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id']))
						
					if (isset($ea_received_rest_ws_raw_array_input['user_type'])) {
						$content .= $ea_received_rest_ws_raw_array_input['user_type'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['user_type']))
						
					if (isset($ea_received_rest_ws_raw_array_input['notes'])) {
						$content .= $ea_received_rest_ws_raw_array_input['notes'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['notes']))
						
				    if (isset($ea_received_rest_ws_raw_array_input['profile_source_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['profile_source_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['profile_source_id']))
						
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
						
					
					$eventLog->log("Received Inputs => ".$content);
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				
				$salutation_input = trim(isset($ea_received_rest_ws_raw_array_input['salutation']) ? filter_var($ea_received_rest_ws_raw_array_input['salutation'], FILTER_SANITIZE_STRING) : '');
				
				$firstname_input = trim(isset($ea_received_rest_ws_raw_array_input['firstname']) ? filter_var($ea_received_rest_ws_raw_array_input['firstname'], FILTER_SANITIZE_STRING) : '');
				
				$middlename_input = trim(isset($ea_received_rest_ws_raw_array_input['middlename']) ? filter_var($ea_received_rest_ws_raw_array_input['middlename'], FILTER_SANITIZE_STRING) : '');
				
				$lastname_input = trim(isset($ea_received_rest_ws_raw_array_input['lastname']) ? filter_var($ea_received_rest_ws_raw_array_input['lastname'], FILTER_SANITIZE_STRING) : '');
				
				$mobile_number_input = trim(isset($ea_received_rest_ws_raw_array_input['mobile_number']) ? filter_var($ea_received_rest_ws_raw_array_input['mobile_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$mobile_phone_country_code_input = trim(isset($ea_received_rest_ws_raw_array_input['mobile_phone_country_code']) ? filter_var($ea_received_rest_ws_raw_array_input['mobile_phone_country_code'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$work_phone_number_input = trim(isset($ea_received_rest_ws_raw_array_input['work_phone_number']) ? filter_var($ea_received_rest_ws_raw_array_input['work_phone_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$work_phone_country_code_input = trim(isset($ea_received_rest_ws_raw_array_input['work_phone_country_code']) ? filter_var($ea_received_rest_ws_raw_array_input['work_phone_country_code'], FILTER_SANITIZE_NUMBER_INT) : '');
				$home_phone_number_input = trim(isset($ea_received_rest_ws_raw_array_input['home_phone_number']) ? filter_var($ea_received_rest_ws_raw_array_input['home_phone_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$home_phone_country_code_input = trim(isset($ea_received_rest_ws_raw_array_input['home_phone_country_code']) ? filter_var($ea_received_rest_ws_raw_array_input['home_phone_country_code'], FILTER_SANITIZE_NUMBER_INT) : '');
				$fax_number_input = trim(isset($ea_received_rest_ws_raw_array_input['fax_number']) ? filter_var($ea_received_rest_ws_raw_array_input['fax_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$fax_number_country_code_input = trim(isset($ea_received_rest_ws_raw_array_input['fax_number_country_code']) ? filter_var($ea_received_rest_ws_raw_array_input['fax_number_country_code'], FILTER_SANITIZE_NUMBER_INT) : '');
				$address_input = trim(isset($ea_received_rest_ws_raw_array_input['address']) ? filter_var($ea_received_rest_ws_raw_array_input['address'], FILTER_SANITIZE_STRING) : '');
				
				
				$email_id_input = trim(isset($ea_received_rest_ws_raw_array_input['email_id']) ? filter_var($ea_received_rest_ws_raw_array_input['email_id'], FILTER_SANITIZE_EMAIL) : '');	
				
				$alternate_email_id_input = trim(isset($ea_received_rest_ws_raw_array_input['alternate_email_id']) ? filter_var($ea_received_rest_ws_raw_array_input['alternate_email_id'], FILTER_SANITIZE_EMAIL) : '');
				
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');	
				
				$user_type_input = trim(isset($ea_received_rest_ws_raw_array_input['user_type']) ? filter_var($ea_received_rest_ws_raw_array_input['user_type'], FILTER_SANITIZE_STRING) : '');
				
                $city_input = trim(isset($ea_received_rest_ws_raw_array_input['city']) ? filter_var($ea_received_rest_ws_raw_array_input['city'], FILTER_SANITIZE_STRING) : '');
				
				$state_input = trim(isset($ea_received_rest_ws_raw_array_input['state']) ? filter_var($ea_received_rest_ws_raw_array_input['state'], FILTER_SANITIZE_STRING) : '');
				
                $country_input = trim(isset($ea_received_rest_ws_raw_array_input['country']) ? filter_var($ea_received_rest_ws_raw_array_input['country'], FILTER_SANITIZE_STRING) : '');
				
				$zipcode_input = trim(isset($ea_received_rest_ws_raw_array_input['zipcode']) ? filter_var($ea_received_rest_ws_raw_array_input['zipcode'], FILTER_SANITIZE_NUMBER_INT) : '');
				if($zipcode_input == "") {
					
					$zipcode_input = null;
				}
				
				$notes_input = trim(isset($ea_received_rest_ws_raw_array_input['notes']) ? filter_var($ea_received_rest_ws_raw_array_input['notes'], FILTER_SANITIZE_STRING) : '');
				
				$profile_source_id_input = trim(isset($ea_received_rest_ws_raw_array_input['profile_source_id']) ? filter_var($ea_received_rest_ws_raw_array_input['profile_source_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$profile_source_name_input = trim(isset($ea_received_rest_ws_raw_array_input['profile_source_name']) ? filter_var($ea_received_rest_ws_raw_array_input['profile_source_name'], FILTER_SANITIZE_STRING) : '');
				
				$profile_source_seo_name_input = strtolower_utf8_extended(create_seo_name($profile_source_name_input));
				
				if($salutation_input == "")
				{  
			        $salutation_input = null;
				}
					
				
				//$eventLog->log("User Type Before strtolower : " . $user_type_input);
				
				$user_type_input = strtolower_utf8_extended( $user_type_input); 
				
				//$eventLog->log("company_id a : " . $company_id_input);
				//$eventLog->log("User Type After strtolower : " . $user_type_input);
				
				
				//check if the company_id, that is provided in the api request, belongs to a valid one or not
				$api_request_related_company_details_result = get_company_details_based_on_company_id($company_id_input);
				$api_request_related_company_details_result_count = count($api_request_related_company_details_result);
				$eventLog->log("company id count - " . $api_request_related_company_details_result_count);
				
				if (!filter_var($email_id_input, FILTER_VALIDATE_EMAIL) == true) { 
				
					
					$eventLog->log($email_id_input . " - Not a Valid Email Address"); 
					$email_id_input = "";
					
				} else if (($mobile_number_input == '0') || (!ctype_digit($mobile_number_input))) {
					
					$eventLog->log($mobile_number_input . " - Not a Valid Mobile Number");
					$mobile_number_input = "";
						
				} else if (($company_id_input == '') || ($api_request_related_company_details_result_count == 0)) {
					
					
					$eventLog->log($company_id_input . " - Not a Valid company id");
					
					$company_id_input = "";

				} else if (!filter_var($alternate_email_id_input, FILTER_VALIDATE_EMAIL) == true) {
					$eventLog->log($alternate_email_id_input . " - Not a Valid Alternate Email Address");
					$alternate_email_id_input = "";
				}//close of else if of if (!filter_var($email_id_input, FILTER_VALIDATE_EMAIL) == true) {
						
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				
				//Check if all inputs are received correctly from the REST Web Service
				//misiing  email id scenario
				if ($email_id_input == "") {
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array(); 
					$response['status'] = "missing-email-id";
					$response['status_description'] = "missing email_id";
					$eventLog->log("missing-email-id: Please provide a valid email address.");
					
				} else if ($mobile_number_input == "") {
					//missing Mobile Number scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-mobile-number";
					$response['status_description'] = "missing mobile number";
					
					$eventLog->log("missing-mobile-number: Please provide a valid mobile number.");
					
				
				} else if ($firstname_input == "") {
					//missing First name scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-firstname";
					$response['status_description'] = "missing firstname";
					
					$eventLog->log("missing-firstname: Please provide a valid firstname.");
				
				} else if ($lastname_input == "") {
					//missing Last name scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-lastname";
					$response['status_description'] = "missing lastname";
					
					$eventLog->log("missing-lastname: Please provide a valid lastname.");	
					
				} else if ($company_id_input == "") {
					//missing Company ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "missing company id";
					
					$eventLog->log("missing-company-id: Please provide a valid company id.");

				} else if ($ip_address_input == "") {
					////missing additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Missing Some Additional Information";
					
					$eventLog->log("missing-some-additional-information: Please provide all information.");
					
				
				} else if ($alternate_email_id_input == "") {
					////missing additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-alternate-email-id";
					$response['status_description'] = "Missing alternate email id";
					
					$eventLog->log("missing-alternate-email-id: Please provide a valid alternate email id.");
					
				
				} else if ($user_type_input != 'member') {
				   
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-user-type-information";
					$response['status_description'] = "invalid user type";
					
					$eventLog->log("invalid-user-type-information: Please provide valid user type.");			
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					$user_account_duplicate_check_result = user_account_duplicate_check_based_on_email_id_input($email_id_input,$company_id_input);
					if (count($user_account_duplicate_check_result) > 0) {
						$eventLog->log("after duplicate check.");
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "user-already-exists";
						$response['status_description'] = "A User already exists with this Email ID";
						
                        $eventLog->log("user-already-exists: user email_id already exists");				
					
					} else {
						
						//Event Time, as per Indian Standard Time
						$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
						$event_expiry_datetime_epoch = $current_epoch+$password_resetting_link_expiry_period;	
						//$valid_from_date_input = '2018-11-10 16:18:20';
						$email_activation_code = create_email_act_code($hash_algorithm, $current_epoch, "25");
						
						$candidate_add_next_step = "";
						$eventLog->log("before candidate add.");
						if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$candidate_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$candidate_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							$eventLog->log("after candidate add.");
							
						} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
						}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
						/* $profile_source_details_get_result = profile_source_details_based_on_profile_source_id($profile_source_id_input);
						$profile_source_name_input = $profile_source_details_get_result["profile_source_name"];
						$profile_source_seo_name_input = $profile_source_details_get_result["profile_source_seo_name"]; */
						
						$profile_source_details_get_result = profile_source_details_based_on_profile_source_id($profile_source_id_input);
						$profile_source_name_input = $profile_source_details_get_result["profile_source_name"];
						$profile_source_seo_name_input = $profile_source_details_get_result["profile_source_seo_name"];
						
						if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							try {
								
								//Check if the User, whose sm_memb_id is received from "sub" of JWT Auth Token exists
								if (count($ea_token_based_sub_user_id_details) > 0) {
									
									//$eventLog->log($ea_token_based_sub_user_id_details["sm_lastname"]);
										//insert in site_members_db_table	

                                       $sm_job_experience_level_input=" ";
                                       $eventLog->log("before insert.");
										$last_inserted_id = user_account_basic_details_insert($email_id_input,$mobile_phone_country_code_input,$mobile_number_input,$alternate_email_id_input,$salutation_input,$firstname_input,$middlename_input,$lastname_input,$work_phone_country_code_input,$work_phone_number_input,$home_phone_country_code_input,$home_phone_number_input,$fax_number_country_code_input,$fax_number_input,$address_input,$city_input,$state_input,$country_input,$zipcode_input,$company_id_input,$user_type_input,"0", "member", $ea_token_based_sub_user_id_details['sm_memb_id'], $ea_token_based_sub_user_id_details['sm_firstname'], $ea_token_based_sub_user_id_details['sm_middlename'], $ea_token_based_sub_user_id_details['sm_lastname'], $event_datetime, $current_epoch, $email_activation_code, "0");
									
								} else {
									$eventLog->log("before else.");
									$last_inserted_id = user_account_basic_details_insert($email_id_input,$mobile_phone_country_code_input,$mobile_number_input,$alternate_email_id_input,$salutation_input,$firstname_input,$middlename_input,$lastname_input,$work_phone_country_code_input,$work_phone_number_input,$home_phone_country_code_input,$home_phone_number_input,$fax_number_country_code_input,$fax_number_input,$address_input,$city_input,$state_input,$country_input,$zipcode_input,$company_id_input,$user_type_input,"0", "member", null, null, null, null, $event_datetime, $current_epoch, $email_activation_code, "0");
								    
								}//close of else of if (count($ea_token_based_sub_user_id_details) > 0) {
								
								$eventLog->log("after else.");
								//duplicate check `candidate_rel_additional_info`
								$candidate_rel_additional_info_details_get_result = candidate_rel_additional_info_details_get($last_inserted_id);
								if (count($candidate_rel_additional_info_details_get_result) > 0) {
									
									$response['data'] = array();
						            $response['status'] = "candidate-profile-source-detils-already-exists";
						            $response['status_description'] = "A candidate already exists with this information";
						
                                     $eventLog->log("candidate-profile-source-details-already-exists: candidate already exists with this information");			
									
								} else {
									
									$candidate_rel_additional_details_info_insert_result = candidate_rel_additional_details_information_insert($last_inserted_id,$profile_source_id_input,$profile_source_name_input,$profile_source_seo_name_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
								}
								
								
								
								$sm_classification_detail_id_input = '1';
								
								//duplicate check  in sm_site_member_classification_associations
								$user_ass_oc_validation_result = user_association_record_check($last_inserted_id,$sm_classification_detail_id_input);
								
								if (count($user_ass_oc_validation_result) > 0) {
									//do update when there is a duplicate in sm_site_member_classification_associations
									$sm_site_member_classification_association_id = $user_ass_oc_validation_result("sm_site_member_classification_association_id");
									$user_ass_oc_update = user_association_record_update($sm_site_member_classification_association_id);
								} else {
										//insert into sm_site_member_classification_associations
									$user_ass_oc_last_inserted_id = user_association_record_insert($last_inserted_id,$sm_classification_detail_id_input,$event_datetime);
									
									if ($candidate_rel_additional_details_info_insert_result != ""){
											
										if ($user_ass_oc_last_inserted_id != "") {
											//Use Privilege inserted successfully, for the user, in sm_site_member_classification_associations db table
											if($last_inserted_id != "") {
												
											    if($notes_input!= "") {
													
											        $candidate_rel_internal_notes_last_inserted_id = candidate_rel_internal_notes_insert($company_id_input,$last_inserted_id,$notes_input,$ea_extracted_jwt_token_sub,$ea_token_based_sub_user_id_details['sm_firstname'],$ea_token_based_sub_user_id_details['sm_lastname'],$event_datetime,$current_epoch);
											   
												}
											   //generate password request code do duplicate and insert into sm_site_member_classification_associations
											   $request_purpose = "activation-email";
											   $request_destination_input = "primary-email";
											   $last_generated_password_setup_mod_request_result = ea_generate_user_password_setup_modification_request_ref_code($ea_extracted_jwt_token_sub, $last_inserted_id, $request_destination_input, $email_id_input, $event_datetime, $current_epoch, $event_expiry_datetime_epoch,$request_purpose);
											   $eventLog->log("after password record insert");
											   
											   if($last_generated_password_setup_mod_request_result > 0) {
												$response['data'] = $last_inserted_id;
												$response['status'] = "user-insert-successful";
												$response['status_description'] = "A User is added Successfully.";
											
												$eventLog->log("user-insert-successful: A User is added Successfully.");
											   }
											}
											
										}
										
									}else {
										//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
										$response['data'] = array();
										$response['status'] = "user-insert-error";
										$response['status_description'] = "Error occurred when adding the User in the Database.";
										
										$eventLog->log("user-insert-error: There is an error, when adding the User in the Database.");
										
									}
									
								}//close of else of if (count($user_ass_oc_validation_result) > 0) {
									
							         
									
								
							} catch (Exception $e) {
								
								//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
								$response['data'] = array();
								$response['status'] = "user-insert-error";
								$response['status_description'] = "Error occurred when adding the User in the Database.";
								
								$eventLog->log("user-insert-error: There is an error, when adding the User in the Database.");
								$eventLog->log("Email ID: " . $email_id_input);
							}
						}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							
						
					}
				}//close of else of if ($email_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
    
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
			
	
	}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){

exit;
?>