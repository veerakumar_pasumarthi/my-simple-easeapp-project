<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "8")) {

	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
			    $user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
			
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
			    $page_number_input = trim(isset($ea_received_rest_ws_raw_array_input['page_number']) ? filter_var($ea_received_rest_ws_raw_array_input['page_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				$number_of_records_input = trim(isset($ea_received_rest_ws_raw_array_input['number_of_records']) ? filter_var($ea_received_rest_ws_raw_array_input['number_of_records'], FILTER_SANITIZE_NUMBER_INT) : '');
				$sort_field_input = trim(isset($ea_received_rest_ws_raw_array_input['sort_field']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_field'], FILTER_SANITIZE_STRING) : '');
				$sort_order_input = trim(isset($ea_received_rest_ws_raw_array_input['sort_order']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_order'], FILTER_SANITIZE_STRING) : '');
				$search_criteria_input = trim(isset($ea_received_rest_ws_raw_array_input['search_criteria']) ? filter_var($ea_received_rest_ws_raw_array_input['search_criteria'], FILTER_SANITIZE_STRING) : '');
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {

                if ($user_id_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-user-id";
					$response['status_description'] = "Missing User Id";
					
					$eventLog->log("missing-user-id: Missing User Id.");
					
				/* } else if ($company_id_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company Id";
					
					$eventLog->log("missing-company-id: Missing Company Id."); */
					
				} else if ($page_number_input == "0") {
					//Invalid Page Number scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-page-number";
					$response['status_description'] = "Invalid Page Number info submitted, please check and try again.";
					
					$eventLog->log("invalid-page-number: Invalid Page Number info submitted, please check and try again.");
					
				} else if (($sort_order_input != "ASC") && ($sort_order_input != "DESC") && ($sort_order_input != "")) {
					//Invalid Sorting Order scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-Sorting-order";
					$response['status_description'] = "Invalid Sorting Order info submitted, please check and try again.";
					
					$eventLog->log("invalid-Sorting-order: Invalid Sorting Order info submitted, please check and try again.");				
				
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else {
					//all inputs are valid
					try {
						
						if ($ea_extracted_jwt_token_user_company_id == "") {
							//action taker is of platform scope - super admin / site admin scenario
							$candiate_considered_proposals_list_info_result = get_candiate_considered_proposals_list_info_with_pagination_inputs($company_id_input,$user_id_input,$page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input);
							
						
						} else {
						   //action taker is of company scope - company admin, legal / immigration, internal admin etc scenario
							$candiate_considered_proposals_list_info_result = get_candiate_considered_proposals_list_info_with_pagination_inputs($ea_extracted_jwt_token_user_company_id,$user_id_input,$page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input);
							//get_candidates_list_with_pagination_inputs($ea_extracted_jwt_token_user_company_id,$sm_user_status_input, $sm_user_type_input, $page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input);
						   
						}//close of else of if ($ea_extracted_jwt_token_user_company_id == "") {
							//$candidate_list_info = get_candidate_list($company_id_input, "member");
						
						$candiate_considered_proposals_list_info_result_count = count($candiate_considered_proposals_list_info_result);
						
						$eventLog->log("Count -> " . $candiate_considered_proposals_list_info_result_count);
						
						if ($candiate_considered_proposals_list_info_result_count > "0") {
							$response['data'] = $candiate_considered_proposals_list_info_result;
							$response['status'] = "candidate-considered-proposals-list-successfully-fetched";
							$response['status_description'] = "Candidate considered profiles List Successfully Received";
					 
						} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "no-considered-proposals-for-this-candidate";
							$response['status_description'] = "No Considered Proposals for this Candidate please check and try again.";
							
						}		//No Active company list Exist
						
							
					} catch (Exception $e){
						$response['data'] = array();
						$response['status'] = "candidate-considered-proposals-list-fetching-error";
						$response['status_description'] = "Candidate considered proposals List Fetching Error";
						
					}
				}
					
			}
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	

function get_candiate_considered_proposals_list_info_with_pagination_inputs($company_id_input,$user_id_input,$page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input) {

    global $dbcon, $default_number_of_records_pagination; //app/core/main-config.php
	global $eventLog;
	$constructed_array = array();
	$eventLog->log("before is_null concept");

	if ($search_criteria_input == "") {
		$search_criteria_input = null;
	}//close of if ($search_criteria_input == "") {
	if ($sort_field_input == "") {
		$sort_field_input = null;
	}

	if (!is_null($search_criteria_input)) {
		//When Search Criteria input is not empty
		$search_criteria_variable = "%" . $search_criteria_input . "%";
		

	    //AND ((jai.job_id LIKE :job_id_search_keyword) OR (jai.company_client_id LIKE :company_client_id_search_keyword) OR (j.job_title LIKE :job_title_search_keyword) OR (jai.invitation_sent_by_user_id LIKE :invitation_sent_by_user_id) OR (jai.event_date_time LIKE :invitation_initiate_at_search_keyword) OR (jai.email_sent_date_time LIKE :invite_email_sent_at_search_keyword) OR (jai.sms_sent_date_time LIKE :invite_sms_sent_at_search_keyword) OR (jai.is_active_status LIKE :status_search_keyword) OR (jai.invite_ref_code LIKE :invite_ref_code_search_keyword))  ORDER BY jai.job_applicant_invite_id ASC
		$search_criteria_in_query_with_where_keyword = " WHERE ((jacp.job_id LIKE :job_id_search_keyword) OR (jacp.company_client_id LIKE :company_client_id_search_keyword) OR (j.job_title LIKE :job_title_search_keyword) OR  (jacp.consider_proposal_initiated_by LIKE :proposal_initiated_by_search_keyword) OR (sm.sm_firstname LIKE :proposal_initiated_by_user_first_name_search_keyword) OR (sm.sm_middlename LIKE :proposal_initiated_by_user_middle_name_search_keyword) OR (sm.sm_lastname LIKE :proposal_initiated_by_user_last_name_search_keyword) OR (jacp.consider_fulfillment_status LIKE :proposal_fulfillment_status_search_keyword) OR (jacp.event_date_time LIKE :event_date_time_search_keyword)  OR (jacp.is_active_status LIKE :status_search_keyword)) ";
		
		$eventLog->log("after search criteria concept");
		$search_criteria_in_query_without_where_keyword = " AND ((jacp.job_id LIKE :job_id_search_keyword) OR (jacp.company_client_id LIKE :company_client_id_search_keyword) OR (j.job_title LIKE :job_title_search_keyword) OR  (jacp.consider_proposal_initiated_by LIKE :proposal_initiated_by_search_keyword) OR (sm.sm_firstname LIKE :proposal_initiated_by_user_first_name_search_keyword) OR (sm.sm_middlename LIKE :proposal_initiated_by_user_middle_name_search_keyword) OR (sm.sm_lastname LIKE :proposal_initiated_by_user_last_name_search_keyword) OR (jacp.consider_fulfillment_status LIKE :proposal_fulfillment_status_search_keyword) OR (jacp.event_date_time LIKE :event_date_time_search_keyword)  OR (jacp.is_active_status LIKE :status_search_keyword)) ";
	} else {
		//When Search Criteria input is empty
		$search_criteria_in_query_with_where_keyword = " WHERE ";

		$search_criteria_in_query_without_where_keyword = "";

	}//close of else of if (!is_null($search_criteria_input)) {
	$eventLog->log("before sort order concept");
	
	
	if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {

		if (!is_null($sort_field_input)) {
			//When the Sort Field input is not empty

			if ($sort_field_input == "job_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY jacp.job_id " . $sort_order_input;

			} else if ($sort_field_input == "company_client_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY cc.company_client_id " . $sort_order_input;

			} else if ($sort_field_input == "job_title") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY j.job_title " . $sort_order_input;

			} else if ($sort_field_input == "proposal_initiated_by") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY jacp.consider_proposal_initiated_by " . $sort_order_input;

			} else if ($sort_field_input == "proposal_initiated_by_user_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_firstname " . $sort_order_input;

			} else if ($sort_field_input == "proposal_initiate_at") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY jacp.event_date_time " . $sort_order_input;

			} else if ($sort_field_input == "proposal_fulfillment_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY jacp.consider_fulfillment_status " . $sort_order_input;

			} else if ($sort_field_input == "status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY jacp.is_active_status " . $sort_order_input;

			
			}
             $eventLog->log("after sort order input");
		} else {
			//When the Sort Field input is empty, primary key has to be taken, with provided Sorting Order input
			$sort_details_in_query = " ORDER BY job_applicant_consider_id " . $sort_order_input;

		}//close of if (!is_null($sort_field_input)) {


	} else {
		//When the Sort Field input is empty, primary key has to be taken, with ASC Order, by default
		$sort_details_in_query = " ORDER BY job_applicant_consider_id ASC ";

	}//close of else of if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {
       $eventLog->log("before else condition for sort details");

	$limit_offset_value = 0;

	if (($page_number_input == "") && ($number_of_records_input == "")) {
		//Give All Data
		//No LIMIT Clause in the Query
		$limit_offset_in_query = "";

	} else if (($page_number_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than page_number_input, till the number of records, as defined in the $default_number_of_records_pagination

		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $default_number_of_records_pagination;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$default_number_of_records_pagination;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $default_number_of_records_pagination;
		}//close of else if of if ($page_number_input == "1") {



	} else if (($page_number_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records
		$limit_offset_in_query = " LIMIT " . $number_of_records_input;

	} else {
		//Give Data, from given Record, till the requested number of records
		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $number_of_records_input;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$number_of_records_input;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $number_of_records_input;
		}//close of else if of if ($page_number_input == "1") {

	}//close of else of if (($page_number_input == "") && ($number_of_records_input == "")) {
		
    $eventLog->log("before search crieteria null concept");
    if (!is_null($search_criteria_input)) {
		
		
			$eventLog->log("after search is_null concept");
			
		    $candiate_considered_proposals_named_parameters_values_array_input = array(":company_id" => $company_id_input,":considered_candidate_user_id" => $user_id_input,":job_id_search_keyword" => $search_criteria_variable,":job_title_search_keyword" => $search_criteria_variable, ":company_client_id_search_keyword" => $search_criteria_variable, ":proposal_initiated_by_search_keyword" => $search_criteria_variable,":proposal_fulfillment_status_search_keyword" => $search_criteria_variable, ":event_date_time_search_keyword" => $search_criteria_variable, ":status_search_keyword" => $search_criteria_variable);    
          
			//Get Jobs List Count
			
			$candiate_considered_proposals_list_count_get_sql = "SELECT COUNT(*) AS count FROM `job_applicant_consider_proposals` jacp JOIN `jobs` j  ON jacp.job_id=j.job_id JOIN `company_clients` cc ON jacp.company_client_id = cc.company_client_id JOIN `site_members` sm ON jacp.consider_proposal_initiated_by = sm.sm_memb_id WHERE j.company_id=:company_id AND jacp.considered_candidate_user_id=:considered_candidate_user_id " . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;
			$eventLog->log($candiate_considered_proposals_list_count_get_sql);
			$candiate_considered_proposals_list_count_get_select_query = $dbcon->prepare($candiate_considered_proposals_list_count_get_sql);
			$candiate_considered_proposals_list_count_get_select_query->execute($candiate_considered_proposals_named_parameters_values_array_input);

			//Get Jobs List
			
			$candiate_considered_proposals_list_info_sql = "SELECT * FROM `job_applicant_consider_proposals` jacp JOIN `jobs` j  ON jacp.job_id=j.job_id JOIN `company_clients` cc ON jacp.company_client_id = cc.company_client_id JOIN `site_members` sm ON jacp.consider_proposal_initiated_by = sm.sm_memb_id WHERE j.company_id=:company_id AND jacp.considered_candidate_user_id=:considered_candidate_user_id " . $search_criteria_in_query_without_where_keyword . $sort_details_in_query . $limit_offset_in_query;
            $eventLog->log($candiate_considered_proposals_list_info_sql);
			$candiate_considered_proposals_list_get_select_query = $dbcon->prepare($candiate_considered_proposals_list_info_sql);
			$candiate_considered_proposals_list_get_select_query->execute($candiate_considered_proposals_named_parameters_values_array_input);

	} else {
			
			$candiate_considered_proposals_named_parameters_values_array_input = array(":company_id" => $company_id_input,":considered_candidate_user_id" => $user_id_input);
			
			//$candiate_invited_jobs_list_count_get_sql = "SELECT COUNT(*) FROM `job_applicant_invites` jai JOIN `jobs` j  ON jai.job_id=j.job_id WHERE j.company_id=:company_id AND jai.invitation_received_by_user_id=:invitation_received_by_user_id " . $sort_details_in_query;
			$candiate_considered_proposals_list_count_get_sql = "SELECT COUNT(*) AS count FROM `job_applicant_consider_proposals` jacp JOIN `jobs` j  ON jacp.job_id=j.job_id JOIN `company_clients` cc ON jacp.company_client_id = cc.company_client_id JOIN `site_members` sm ON jacp.consider_proposal_initiated_by = sm.sm_memb_id WHERE j.company_id=:company_id AND jacp.considered_candidate_user_id=:considered_candidate_user_id " . $sort_details_in_query;
			$eventLog->log($candiate_considered_proposals_list_count_get_sql);
			$candiate_considered_proposals_list_count_get_select_query = $dbcon->prepare($candiate_considered_proposals_list_count_get_sql);
			$candiate_considered_proposals_list_count_get_select_query->execute($candiate_considered_proposals_named_parameters_values_array_input);

			//Give List of Jobs, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			//$candiate_invited_jobs_list_info_sql = "SELECT * FROM `job_applicant_invites` jai JOIN `jobs` j  ON jai.job_id=j.job_id WHERE j.company_id=:company_id AND jai.invitation_received_by_user_id=:invitation_received_by_user_id " . $sort_details_in_query . $limit_offset_in_query;
			$candiate_considered_proposals_list_info_sql = "SELECT * FROM `job_applicant_consider_proposals` jacp JOIN `jobs` j  ON jacp.job_id=j.job_id JOIN `company_clients` cc ON jacp.company_client_id = cc.company_client_id JOIN `site_members` sm ON jacp.consider_proposal_initiated_by = sm.sm_memb_id WHERE j.company_id=:company_id AND jacp.considered_candidate_user_id=:considered_candidate_user_id " . $sort_details_in_query . $limit_offset_in_query;
			$candiate_considered_proposals_list_get_select_query = $dbcon->prepare($candiate_considered_proposals_list_info_sql);
			$candiate_considered_proposals_list_get_select_query->execute($candiate_considered_proposals_named_parameters_values_array_input);

	}//close of else of if (!is_null($search_criteria_input)) {

	

	//Process / Get Companies Count
	if($candiate_considered_proposals_list_count_get_select_query->rowCount() > 0) {
	    $candiate_considered_proposals_list_count_get_select_query_result = $candiate_considered_proposals_list_count_get_select_query->fetch();
	    //print_r($jobs_list_count_get_select_query_result);

		//$total_candiate_considred_proposals_count = $candiate_considered_proposals_list_count_get_select_query_result["count"];
		$total_candiate_considered_proposals_count = $candiate_considered_proposals_list_count_get_select_query_result["count"];
		$constructed_array["total_records_count"] = $total_candiate_considered_proposals_count;

	}//close of if($jobs_list_count_get_select_query->rowCount() > 0) {

	//Process / Fetch Companies List
	if($candiate_considered_proposals_list_get_select_query->rowCount() > 0) {
	    $candiate_considered_proposals_list_get_select_query_result = $candiate_considered_proposals_list_get_select_query->fetchAll();
	    //print_r($companies_list_get_select_query_result);

		foreach ($candiate_considered_proposals_list_get_select_query_result as $candiate_considered_proposals_list_get_select_query_result_row) {

			$temp_row_array = array();
			$temp_row_array["job_applicant_consider_id"] = $candiate_considered_proposals_list_get_select_query_result_row["job_applicant_consider_id"];
		    $temp_row_array["company_id"] = $candiate_considered_proposals_list_get_select_query_result_row["company_id"];
		    $temp_row_array["company_client_id"] = $candiate_considered_proposals_list_get_select_query_result_row["company_client_id"];
			$temp_row_array["company_client_name"] = $candiate_considered_proposals_list_get_select_query_result_row["client_company_name"];
			$temp_row_array["job_id"] = $candiate_considered_proposals_list_get_select_query_result_row["job_id"];
			$temp_row_array["job_title"] = $candiate_considered_proposals_list_get_select_query_result_row["job_title"];
			$proposal_initated_by = $candiate_considered_proposals_list_get_select_query_result_row["consider_proposal_initiated_by"];
			if($proposal_initated_by == '1') {
				$temp_row_array["proposal_initated_by"] = "Person";
				$temp_row_array["proposal_initated_by_id"] = $candiate_considered_proposals_list_get_select_query_result_row["consideration_initiated_by_user_id"];
				$temp_row_array["sm_firstname"] = $candiate_considered_proposals_list_get_select_query_result_row["sm_firstname"];
				$temp_row_array["sm_middlename"] = $candiate_considered_proposals_list_get_select_query_result_row["sm_middlename"];
				$temp_row_array["sm_lastname"] = $candiate_considered_proposals_list_get_select_query_result_row["sm_lastname"];
				$temp_row_array["proposal_initated_by_name"] = $temp_row_array["sm_firstname"] . " " . $temp_row_array["sm_middlename"] . " " .$temp_row_array["sm_lastname"];
				
			} else {
				$temp_row_array["proposal_initated_by"] = "System Generated";
				$temp_row_array["proposal_initated_by_id"] = null;
			}
			
			$temp_row_array["proposal_initiate_at"] = $candiate_considered_proposals_list_get_select_query_result_row["event_date_time"];
			
			$temp_row_array["proposal_fulfillment_status"] = $candiate_considered_proposals_list_get_select_query_result_row["consider_fulfillment_status"];
			$temp_row_array["status"] = $candiate_considered_proposals_list_get_select_query_result_row["is_active_status"];
			$constructed_array["list"][] = $temp_row_array;
	        //$constructed_array[] = $temp_row_array;
	    }
		    //$constructed_array["list"][] = $temp_row_array;
	}//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {
	return $constructed_array;
}


exit;
?>