<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * REST API for candidate all education qualifications collect
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['all_download_requests'])) {
						$content .= json_encode($ea_received_rest_ws_raw_array_input['all_download_requests']) . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['all_download_requests'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					//$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$multiple_download_requests_input = $ea_received_rest_ws_raw_array_input['multiple_download_requests'];
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//Check if all inputs are received correctly from the REST Web Service
				if ($company_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company ID";
					
					$eventLog->log("missing-company-id: Please provide a valid Company ID.");
					
				} else if ($multiple_download_requests_input == "") {	
					//Invalid Education Qualifications Received scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "empty-multiple-download-requests-input";
					$response['status_description'] = "Empty Multiple Download Requests Input";
					
					$eventLog->log("empty-multiple-download-requests-input: Empty Multiple Download Requests Input, Please check and try again.");
					
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Missing Some Additional Information";
					
					$eventLog->log("missing-some-additional-information: Some Additional Information is missing, please check and try again.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					$ea_action_specific_auth_check_result = "";
					if ($ea_extracted_jwt_token_user_type == "admin") {
							
						if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
							$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
							
						} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
							//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
							
							$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
							
						} else {
							//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-different-company-user-id";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
		
							$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
							
						}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							$eventLog->log("after candidate add.");
							
					} else {
							
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "insufficient-permissions";
						$response['status_description'] = "Insufficient Set of Permissions";
						
						//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
						header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
		
						$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
					
					}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
							
					if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
						try {
							
							//Event Time, as per Indian Standard Time
					
							$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
					
							if ((is_array($multiple_download_requests_input)) && (count($multiple_download_requests_input) > 0)) {
									
								//Do process each record (Education Qualification)
							    
								foreach ($multiple_download_requests_input as $multiple_download_requests_input_row) {
									
									$sm_memb_id = trim(isset($multiple_download_requests_input_row["sm_memb_id"]) ? filter_var($multiple_download_requests_input_row["sm_memb_id"], FILTER_SANITIZE_NUMBER_INT) : '');
									$crur_id = trim(isset($multiple_download_requests_input_row["crur_id"]) ? filter_var($multiple_download_requests_input_row["crur_id"], FILTER_SANITIZE_NUMBER_INT) : '');
									
									
									if ($sm_memb_id == "") {
										
										$eventLog->log("Empty user id input");
										
									} else if ($crur_id == "") {
					
										$eventLog->log("Empty Field of Study input");
										
									} else {
										
										$eventLog->log("Download requests valid, for this record");
										$current_epoch_with_micro_seconds = microseconds();
										$constructed_candidate_id_name = $ea_extracted_jwt_token_sub;
										$constructed_zip_file_name = $ea_extracted_jwt_token_sub . "_" . $current_epoch_with_micro_seconds . ".zip";
										
										$candidate_rel_required_resume_details_result = candidate_rel_uploaded_resumes_based_on_sm_memb_id_input($sm_memb_id);
										//$eventLog->log(var_dump($candidate_rel_required_resume_details_result));
										
										if (count($candidate_rel_required_resume_details_result) > 0) {
											
											//Create Absolute Path for Archive File, with Referencing Filename
											//$archive_file_absolute_path = $site_home_path . "uploaded-documents-archive/" . $ea_extracted_jwt_token_sub . "_" . uniqid(rand(), TRUE) . ".zip";
											$archive_file_absolute_path = $site_home_path . "uploaded-documents-archive/" . $constructed_zip_file_name;
											//echo "archive_file_absolute_path (with Referencing Filename): " . $archive_file_absolute_path . "<br>";
											
											//Create Relative Path for Archive File, with Generated Filename
											//$archive_file_relative_path_generated_filename = $candidate_document_archive_main_folder_rel_download_path . $ea_extracted_jwt_token_sub . "_" . uniqid(rand(), TRUE) . ".zip";
											$archive_file_relative_path_generated_filename = $candidate_document_archive_main_folder_rel_download_path . $constructed_zip_file_name;
											//echo "archive_file_relative_path (with Generated Filename): " . $archive_file_relative_path_generated_filename . "<br>";
											
											//Do Initiate Archive File Creation
											//https://stackoverflow.com/q/11540339
											$zip = new ZipArchive();
											//$zip->open($archive_file_absolute_path,  ZipArchive::CREATE);
											if ($zip->open($archive_file_absolute_path, ZIPARCHIVE::CREATE )=== TRUE) {
												$eventLog->log("zip file opened");
												
												$crur_id = $candidate_rel_required_resume_details_result["crur_id"];
												$sm_memb_id = $candidate_rel_required_resume_details_result["sm_memb_id"];
												$generated_filename = $candidate_rel_required_resume_details_result["generated_filename"];
												
												if ((isset($generated_filename)) && ($generated_filename != "")) {
												
													
													//$crur_id_folder_name = create_seo_name(strtolower_utf8_extended($ea_extracted_jwt_token_sub));
									
													//Create Full Folder related Absolute Path
													$crur_id_full_folder_absolute_path = $site_home_path . "uploaded-documents" ;
													
													//echo "crauvd_id_full_folder_absolute_path: " . $crauvd_id_full_folder_absolute_path . "<br>";
													
													//Create File related Absolute Path
													$crur_id_file_absolute_path = $crur_id_full_folder_absolute_path . "/" . $generated_filename;
													
													//echo "crur_id_file_absolute_path: " . $crur_id_file_absolute_path . "<br>";
													
													//Create Archive Folder Structure
													//$archive_folder_structure_path = $constructed_candidate_id_name . "/" . $crauvd_id_folder_name . "/" . $generated_filename;
													$archive_folder_structure_path = $generated_filename;
												
													
													//echo "archive_folder_structure_path: " . $archive_folder_structure_path . "<br>";
													
													//Do Add a File to the Archive File
													//$zip->addFile("{$crur_id_file_absolute_path}");   
													//http://php.net/manual/en/zip.examples.php	
													$eventLog->log($crur_id_file_absolute_path);
													$eventLog->log($archive_folder_structure_path);
													
													if($zip->addFile($crur_id_file_absolute_path,$archive_folder_structure_path)) {
														$eventLog->log($archive_folder_structure_path);
													} else {
														$eventLog->log($crur_id_file_absolute_path);
													}
												
													//$zip->addFile($crur_id_file_absolute_path,$archive_folder_structure_path);
													
												}//close of if ((isset($generated_filename)) && ($generated_filename != "")) {
													
											}	else {
												
												$eventLog->log("zip not opened");
												
											}
												
									    }//close of foreach ($candidate_rel_required_documents_list_result as $candidate_rel_required_documents_list_result_row) {
											
											//Do Close Archive File Creation
											$zip->close();
									}
						        }
								
								$sm_memb_id_array = array();
								foreach ($multiple_download_requests_input as $multiple_download_requests_input_row) {
									$sm_memb_id_list[] = $multiple_download_requests_input_row["sm_memb_id"];
								
								}
								
								$sm_memb_id_array = json_encode($sm_memb_id_list);
								$resumes_list = null;
								$zip_file_name = $constructed_zip_file_name;
								$candidate_rel_all_document_download_request_insert_result = candidate_rel_all_document_download_request_insert($resumes_list,$sm_memb_id_array,$zip_file_name,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);

								if (isset($candidate_rel_all_document_download_request_insert_result["last_inserted_id"])) {
									
									$candidate_rel_all_document_download_request_last_inserted_id = $candidate_rel_all_document_download_request_insert_result["last_inserted_id"];
									$eventLog->log($candidate_rel_all_document_download_request_last_inserted_id);
									
									$link_expiry_time = $current_epoch+$expiring_links_lifetime;
									$downloaded_file_url_result = create_uploaded_file_zip_link_with_signature($ea_extracted_jwt_token_sub, $candidate_rel_all_document_download_request_last_inserted_id,"https", $site_hostname_value, $link_expiry_time, $expiring_link_hash_algorithm, $expiring_link_secret_key);
									//print_r($downloaded_file_url_result);
								
			                        
									if(count($downloaded_file_url_result)>0) {
										$response['data'] = $downloaded_file_url_result;
										$response['status'] = "zip-file-download-link-created-successfully";
										$response['status_description'] = "Zip File Download link successfully created";
								    } else {
										$response['data'] = array();
										$response['status'] = "zip-file-download-link-created-successfully";
										$response['status_description'] = "Zip File Download link successfully created";
										
									}
								}
							}	
						
					    } catch (Exception $e) {
						//$eventLog->log("Empty user id input");
									
					    }
				    }
					
				}	
			
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
			

/* function candidate_rel_uploaded_resumes_based_on_sm_memb_id_input($sm_memb_id) {
    global $dbcon;
	$constructed_array = array();
	
	$candidate_rel_uploaded_resumes_based_on_sm_memb_id_sql = "SELECT * FROM `candidate_rel_uploaded_resumes` WHERE `sm_memb_id`=:sm_memb_id ORDER BY `added_date_time_epoch` DESC LIMIT 1";
	$candidate_rel_uploaded_resumes_based_on_sm_memb_id_query = $dbcon->prepare($candidate_rel_uploaded_resumes_based_on_sm_memb_id_sql);
	$candidate_rel_uploaded_resumes_based_on_sm_memb_id_query->bindValue(":sm_memb_id",$sm_memb_id);	
	$candidate_rel_uploaded_resumes_based_on_sm_memb_id_query->execute(); 
	
	if($candidate_rel_uploaded_resumes_based_on_sm_memb_id_query->rowCount() > 0) {
		$candidate_rel_uploaded_resumes_based_on_sm_memb_id_query_result = $candidate_rel_uploaded_resumes_based_on_sm_memb_id_query->fetch();
	     return $candidate_rel_uploaded_resumes_based_on_sm_memb_id_query_result;
	
	}//close of if($candidate_rel_uploaded_resumes_based_on_sm_memb_id_query->rowCount() > 0) {
	return $constructed_array;
}
 */
function candidate_rel_all_document_download_request_insert($resumes_list,$sm_memb_id_array,$zip_file_name,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch){
    global $dbcon;
	
	$constructed_array = array();
	
	$candidate_rel_all_document_download_request_insert_sql = "INSERT INTO `created_resume_zip_file_refs`(`resumes_list`, `sm_memb_id_list`, `zip_file_name`, `added_by_user_id`, `added_date_time`, `added_date_time_epoch`) VALUES (:resumes_list,:sm_memb_id_list,:zip_file_name,:added_by_user_id,:added_by_date_time,:added_by_date_time_epoch)";
	$candidate_rel_all_document_download_request_insert_query = $dbcon->prepare($candidate_rel_all_document_download_request_insert_sql);
	$candidate_rel_all_document_download_request_insert_query->bindValue(":resumes_list",$resumes_list);
	$candidate_rel_all_document_download_request_insert_query->bindValue(":sm_memb_id_list",$sm_memb_id_array);
	$candidate_rel_all_document_download_request_insert_query->bindValue(":zip_file_name",$zip_file_name);
	$candidate_rel_all_document_download_request_insert_query->bindValue(":added_by_user_id",$ea_extracted_jwt_token_sub);
	$candidate_rel_all_document_download_request_insert_query->bindValue(":added_by_date_time",$event_datetime);
	$candidate_rel_all_document_download_request_insert_query->bindValue(":added_by_date_time_epoch",$current_epoch);
	
	
	if ($candidate_rel_all_document_download_request_insert_query->execute()) {
		$last_inserted_id = $dbcon->lastInsertId();			
		//$eventLog->log("record inserted successfully");
		
		$constructed_array["last_inserted_id"] = $last_inserted_id;
		
		return $constructed_array;
					
	} else {
		//$eventLog->log("Error occurred during process. Please try again");						
		return $constructed_array;						
	}//close of else of if ($candidate_rel_all_document_download_request_insert_query->execute()) {
		
}

function create_uploaded_file_zip_link_with_signature($viewer_user_id_input, $candidate_rel_all_document_download_request_last_inserted_id, $uploaded_file_url_protocol_input, $uploaded_file_url_hostname_input, $expiring_link_lifetime_input, $expiring_link_hash_algorithm_input, $expiring_link_secret_key_input) {
	global $dbcon, $eventLog;
	
	$constructed_array = array();
	
	//Use Active JWT Token Details of the Document Viewing User (Admin user / Company User / Candidate thyself)
	$viewing_user_rel_active_jwt_token_details_result = ea_get_user_rel_active_jwt_token_details($viewer_user_id_input);
	
	if (count($viewing_user_rel_active_jwt_token_details_result) > 0) {
		
		foreach($viewing_user_rel_active_jwt_token_details_result as $viewing_user_rel_active_jwt_token_details_result_key => $viewing_user_rel_active_jwt_token_details_result_value) {
				
			//$user_auth_token_id = $viewing_user_rel_active_jwt_token_details_result_row["user_auth_token_id"];
			$user_auth_token_id = $viewing_user_rel_active_jwt_token_details_result_key;
			
			
			$candidate_resumes_zip_download_details_result = candidate_rel_uploaded_resume_details_based_on_zip_file_id($candidate_rel_all_document_download_request_last_inserted_id);
			
			if (count($candidate_resumes_zip_download_details_result) > 0) {
				
				$zip_file_name = $candidate_resumes_zip_download_details_result["zip_file_name"];
				
				//Create Uploaded File URL
				//https://api-devjobs.securitywonks.net/viewer/1/candidate-resumes/28/download/expires/1564156681/link-signature/2TSTv-pM7itv0sMvpJ5qPbFLlk6BCQjOEsQgmaO1QGY
				
				
				$uploaded_file_url_without_signature = $uploaded_file_url_protocol_input . "://" . $uploaded_file_url_hostname_input . "/viewer/" . $viewer_user_id_input . "/candidate-resumes/" . $candidate_rel_all_document_download_request_last_inserted_id . "/download/expires/" . $expiring_link_lifetime_input;
				
				$eventLog->log("uploaded_file_url (without signature): " . $uploaded_file_url_without_signature);
				
				/*//Creating Link Signature
				$link_signature = hash('sha256', $user_auth_token_id . $uploaded_file_url_without_signature);
				*/
				
				//Base64 Decode the Base64 Encoded Expiring Link related Secret Key
				$expiring_link_rel_secret_base64_encoded = $expiring_link_secret_key_input;
				$expiring_link_rel_secret_base64_decoded = base64_decode($expiring_link_rel_secret_base64_encoded);
				
				//Create Link Signature using sha256
				$created_link_signature = hash_hmac($expiring_link_hash_algorithm_input, $user_auth_token_id . $uploaded_file_url_without_signature, $expiring_link_rel_secret_base64_decoded, true);
				
				//Base64 URL Encode the Created Token Signature
				$created_link_signature_base64_urlencoded = base64url_encode($created_link_signature); //from /app/includes/other-functions-api.php
				
				//remove padding (=), from Base64 URL Encoded Token Signature
				$created_link_signature_base64_urlencoded_after_removing_padding = str_replace("=", "", $created_link_signature_base64_urlencoded);
				
				$uploaded_file_url_with_signature = $uploaded_file_url_without_signature . "/link-signature/" . $created_link_signature_base64_urlencoded_after_removing_padding;
				
				$constructed_array["zip_file_downloaded_url"] = $uploaded_file_url_with_signature;
				
				$eventLog->log("uploaded_file_url (with signature): " . $uploaded_file_url_with_signature);
				
				return $constructed_array;
				
			} else {
				$eventLog->log("Uploaded file ref id is not Valid!!!!");	
			}//close of else of if (count($candidate_rel_uploaded_visa_document_details_result) > 0) {
				
			
			
		}//close of foreach($viewing_user_rel_active_jwt_token_details_result as $viewing_user_rel_active_jwt_token_details_result_key => $viewing_user_rel_active_jwt_token_details_result_value) {
		
	} else {
		$eventLog->log("Active Token Details of Viewing User is not received");
	}//close of else of if (count($viewing_user_rel_active_jwt_token_details_result) > 0) {
	
	return $constructed_array;
}

function candidate_rel_uploaded_resume_details_based_on_zip_file_id($zip_file_id_input) {
    global $dbcon;
	$constructed_array = array();
	
	$candidate_rel_uploaded_resume_details_based_on_zip_file_id_sql = "SELECT * FROM `created_resume_zip_file_refs` WHERE `crzfr_id` = :crzfr_id";
	$candidate_rel_uploaded_resume_details_based_on_zip_file_id_query = $dbcon->prepare($candidate_rel_uploaded_resume_details_based_on_zip_file_id_sql);
	$candidate_rel_uploaded_resume_details_based_on_zip_file_id_query->bindValue(":crzfr_id",$zip_file_id_input);	
	$candidate_rel_uploaded_resume_details_based_on_zip_file_id_query->execute(); 
	
	if($candidate_rel_uploaded_resume_details_based_on_zip_file_id_query->rowCount() > 0) {
		$candidate_rel_uploaded_resume_details_based_on_zip_file_id_query_result = $candidate_rel_uploaded_resume_details_based_on_zip_file_id_query->fetch();
	     return $candidate_rel_uploaded_resume_details_based_on_zip_file_id_query_result;
	
	}//close of if($candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query->rowCount() > 0) {
	return $constructed_array;
}

			
												
									
												
exit;
?>