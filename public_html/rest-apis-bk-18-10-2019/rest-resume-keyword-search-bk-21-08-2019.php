<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "5")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['page_number'])) {
						$content .= $ea_received_rest_ws_raw_array_input['page_number'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['page_number']))
					
					if (isset($ea_received_rest_ws_raw_array_input['number_of_records'])) {
						$content .= $ea_received_rest_ws_raw_array_input['number_of_records'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['number_of_records']))
					
					if (isset($ea_received_rest_ws_raw_array_input['search_criteria'])) {
						$content .= $ea_received_rest_ws_raw_array_input['search_criteria'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['search_criteria']))
						
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
				
				//Filter Inputs	
				
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$page_number_input= trim(isset($ea_received_rest_ws_raw_array_input['page_number']) ? filter_var($ea_received_rest_ws_raw_array_input['page_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				$number_of_records_input = trim(isset($ea_received_rest_ws_raw_array_input['number_of_records']) ? filter_var($ea_received_rest_ws_raw_array_input['number_of_records'], FILTER_SANITIZE_NUMBER_INT) : '');
				$search_criteria_input = trim(isset($ea_received_rest_ws_raw_array_input['search_criteria']) ? filter_var($ea_received_rest_ws_raw_array_input['search_criteria'], FILTER_SANITIZE_STRING) : '');
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {

                if ($search_criteria_input == "") {
					//Invalid company Status scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-search-criteria-input";
					$response['status_description'] = "Missing Search Criteria Input";
					
					$eventLog->log("Please provide a valid search criteria info.");
					
				} else if ($company_id_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company Id Input, Please check and try again.";	
					
				
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else {
					//all inputs are valid
					
					try{
						
					   
						
					       $candidate_list_info_result = array();
						   
					    if ($ea_extracted_jwt_token_user_company_id == "") {
						    //action taker is of platform scope - super admin / site admin scenario
						    $candidates_resumes_keyword_search_info_result = candidates_resumes_keyword_search_info($company_id_input,$search_criteria_input,$page_number_input,$number_of_records_input);
					
					    } else {
						   //action taker is of company scope - company admin, legal / immigration, internal admin etc scenario
						    $candidates_resumes_keyword_search_info_result = candidates_resumes_keyword_search_info($ea_extracted_jwt_token_user_company_id,$search_criteria_input,$page_number_input,$number_of_records_input);
					       
					    }//close of else of if ($ea_extracted_jwt_token_user_company_id == "") {
					    //$candidate_list_info = get_candidate_list($company_id_input, "member");
						$candidates_resumes_keyword_search_info_result_count = count($candidates_resumes_keyword_search_info_result);
						
						$eventLog->log("Count -> " . $candidates_resumes_keyword_search_info_result_count);
						
					    if ($candidates_resumes_keyword_search_info_result_count > "0") {
					       $response['data'] = $candidates_resumes_keyword_search_info_result;
					       $response['status'] = "candidates-resumes-received";
					       $response['status_description'] = "Candidates resumes is Successfully Received";
							
					     $candidate_list_info_json_encoded = json_encode($candidates_resumes_keyword_search_info_result);
						
					       //eventLog->log("Quick User Info -> " . $candidate_list_info_json_encoded); 
					    }else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "no-resumes-found-for-this-search";
							$response['status_description'] = "No Resumes found for this Search criteria, please check and try again.";
							
					    }		
							
				    }catch (Exception $e){
						$eventLog->log("Exception -> " . html_escaped_output($e->getMessage())); 
						//addLog($logFile, "Exception -> ".$e->getMessage());	
					}//close of  catch (Exception $e){	}//close of else of if ($company_list_result_count > "0") {					
                  
				}//close of else of if ($user_type_input != 'admin') {
					
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
	
/*function candidates_resumes_keyword_search_info($company_id_input,$search_criteria_input){
	global $dbcon;
	$constructed_array = array();

	$candidates_resumes_keyword_search_info_sql = "SELECT * FROM `resume_extracted_info` WHERE MATCH(`resume_text`) AGAINST('".$search_criteria_input."' IN NATURAL LANGUAGE MODE)";
	$candidates_resumes_keyword_search_info_query = $dbcon->prepare($candidates_resumes_keyword_search_info_sql);
	//$candidates_resumes_keyword_search_info_query->bindValue(":search_criteria_input",$search_criteria_input);
    //$candidates_resumes_keyword_search_info_query->bindValue(":sm_user_status","4");
	$candidates_resumes_keyword_search_info_query->execute();

	if($candidates_resumes_keyword_search_info_query->rowCount() > 0) {
		$candidates_resumes_keyword_search_info_query_result = $candidates_resumes_keyword_search_info_query->fetchAll();
	     return $candidates_resumes_keyword_search_info_query_result;

	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;

}*/

function candidates_resumes_keyword_search_info($company_id_input,$search_criteria_input,$page_number_input,$number_of_records_input){
	global $dbcon, $elasticsearch_index_name,$elasticsearch_index_type,$elasticsearch_client , $ea_extracted_jwt_token_sub, $current_epoch, $expiring_link_lifetime, $expiring_link_secret_key, $expiring_link_hash_algorithm, $site_hostname_value ,$default_number_of_records_pagination,$eventLog;
	$constructed_array = array();
	
	/*$searchparams = [
						'index' => $elasticsearch_index_name,
						'type' => $elasticsearch_index_type,
						'body' => [
							'query' => [
								'match' => [
									'resume_text' => $search_criteria_input
								]
							]
						]
					];
	*/
	//$limit_offset_value = 0;

	if (($page_number_input == "") && ($number_of_records_input == "")) {
		//Give All Data
		//No LIMIT Clause in the Query
		//$limit_offset_in_query = "";
		$size_value = $default_number_of_records_pagination;
	    $from_value = 0;

	} else if (($page_number_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than page_number_input, till the number of records, as defined in the $default_number_of_records_pagination

		if ($page_number_input == "1") {
			$size_value = $default_number_of_records_pagination;
			$from_value = 0;
			//$limit_offset_in_query = " LIMIT 0, " . $default_number_of_records_pagination;
		} else if ($page_number_input >= "2") {
			$size_value = $default_number_of_records_pagination;
			$from_value = ($page_number_input-1)*$default_number_of_records_pagination;
			
			/* $limit_offset_value = ($page_number_input-1)*$default_number_of_records_pagination;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $default_number_of_records_pagination; */
		}//close of else if of if ($page_number_input == "1") {



	} else if (($page_number_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records
		//$limit_offset_in_query = " LIMIT " . $number_of_records_input;
		$size_value = $number_of_records_input;
		$from_value = 0;

	} else {
		//Give Data, from given Record, till the requested number of records
		if ($page_number_input == "1") {
			
			//$limit_offset_in_query = " LIMIT 0, " . $number_of_records_input;
			$size_value = $number_of_records_input;
			$from_value = 0;
			$eventLog->log("size:".$size_value);
		} else if ($page_number_input >= "2") {
			$size_value = $number_of_records_input;
			$from_value = ($page_number_input-1)*$number_of_records_input;
			//$limit_offset_value = ($page_number_input-1)*$number_of_records_input;
			//$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $number_of_records_input;
		}//close of else if of if ($page_number_input == "1") {

	}//close of else of if (($page_number_input == "") && ($number_of_records_input == "")) {
	
	$querystring_search_params = [
									'index' => $elasticsearch_index_name,
									'type' => $elasticsearch_index_type,
									'from' => $from_value,
									'size' => $size_value,
									/*'sort' => [
												 "sm_mobile" => "desc"
											  ],*/
								   'sort' => [
												"sm_mobile" => [
												 "order" => "desc" 
												 ]
											],
																					
									'body' => [
									
									   'query' => [
	
											"bool" => [
												'filter' => [
													'terms' => [ 
														'company_id' => [$company_id_input]
													]   
												],
											  "must" => [
												'query_string' => [
													//"default_operator" => "AND",
													"fields" => ["resume_text","sm_email","sm_mobile"],
													"query" => $search_criteria_input
													 ]
												]
										   
											]
										]
									]
										   
								];
								
								/* $querystring_search_params['body']['sort'] = [
													['sm_email' => ['order' => 'asc']]
								]; 
									 */		 

					$search_response = $elasticsearch_client->search($querystring_search_params);
					echo "<pre>";
					print_r($search_response); 

					//$duplicate_records_count = $search_response["hits"]["total"];
					
				     
					/******************
					 $result_rows_array = array();
        
        
					foreach ($search_response['hits']['hits'] as $response_data_row) {
						/* print_r($response_data_row);
						echo "<br>";
						echo "_index: " . $response_data_row["_index"] . "<br>";
						echo "name field value, from _source: " . $response_data_row["_source"]["name"] . "<br>";
						 ***********
							
						foreach ($response_data_row as $response_data_row_key => $response_data_row_value) {
							
							
							if ($response_data_row_key == "_source") {
								$temp = array();
								
								foreach ($response_data_row_value as $response_data_row_value_key => $response_data_row_value_details) {
									$temp[$response_data_row_value_key] = $response_data_row_value_details;
									
								}//close of foreach ($response_data_row_value as $response_data_row_value_details) {
								$result_rows_array[] = $temp;
								
								foreach ($result_rows_array as $response_data_row_value) {

									$temp_row_array = array();
									$temp_row_array["resume_extracted_info_id"] = $response_data_row_value["resume_extracted_info_id"];
									$temp_row_array["company_id"] = $response_data_row_value["company_id"];
									$temp_row_array["sm_memb_id"] = $response_data_row_value["sm_memb_id"];
									$temp_row_array["crur_id"] = $response_data_row_value["crur_id"];
									$temp_row_array["sm_salutation"] = $response_data_row_value["sm_salutation"];
									$temp_row_array["sm_firstname"] = $response_data_row_value["sm_firstname"];
									$temp_row_array["sm_middlename"] = $response_data_row_value["sm_middlename"];
									$temp_row_array["sm_lastname"] = $response_data_row_value["sm_lastname"];
									$temp_row_array["sm_fullname"] = $response_data_row_value["sm_salutation"]." ".$response_data_row_value["sm_firstname"]." ".$response_data_row_value["sm_middlename"]." ".$response_data_row_value["sm_lastname"];
									$temp_row_array["sm_email"] = $response_data_row_value["sm_email"];
									$temp_row_array["sm_alternate_email"] = $response_data_row_value["sm_alternate_email"];
									$temp_row_array["sm_mobile"] = $response_data_row_value["sm_mobile"];
									$temp_row_array["sm_city"] = $response_data_row_value["sm_city"];
									$temp_row_array["sm_state"] = $response_data_row_value["sm_state"];
									$temp_row_array["sm_country"] = $response_data_row_value["sm_country"];
									$temp_row_array["sm_job_experience_level"] = $response_data_row_value["sm_job_experience_level"];
									$string= $response_data_row_value["resume_text"];
									//$temp_row_array["resume_html_text"]= str_replace(array("\r\n", "\r", "\n"), "<br>", $string);
									$temp_row_array["resume_html_text"]= $string;
									//$temp_row_array["resume_html_text"] = str_ireplace($search_criteria_input, '<span style="color: #daa732;">'.$search_criteria_input.'</span>', $string);
									
									$link_expiry_time = $current_epoch+$expiring_link_lifetime;
									$uploaded_file_link_with_signature_result = create_uploaded_file_link_with_signature($ea_extracted_jwt_token_sub, $temp_row_array["sm_memb_id"], $temp_row_array["crur_id"], "https", $site_hostname_value, $link_expiry_time, $expiring_link_hash_algorithm, $expiring_link_secret_key);
									if (isset($uploaded_file_link_with_signature_result["uploaded_file_url"])) {
									$temp_row_array["uploaded_file_url"] = $uploaded_file_link_with_signature_result["uploaded_file_url"];
										
									} else {
										$temp_row_array["uploaded_file_url"] = null;
									}//close of if (isset($uploaded_file_link_with_signature_result["uploaded_file_url"])) {
										
									$download_file_link_with_signature_result = create_download_file_link_with_signature($ea_extracted_jwt_token_sub, $temp_row_array["sm_memb_id"], $temp_row_array["crur_id"], "https", $site_hostname_value, $link_expiry_time, $expiring_link_hash_algorithm, $expiring_link_secret_key);
									if (isset($download_file_link_with_signature_result["download_file_url"])) {
									$temp_row_array["download_file_url"] = $download_file_link_with_signature_result["download_file_url"];
										
									} else {
										$temp_row_array["download_file_url"] = null;
									}//close of if (isset($download_file_link_with_signature_result["uploaded_file_url"])) {
										
									
									$constructed_array[] = $temp_row_array;
									
									
									
								}//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {
		
								
							} else {
								
								
							}//close of else of if ($response_data_row_key == "_source") {
							
											
							/* echo "<br>";
							echo "response_data_row_key: " . $response_data_row_key . "<br>";
							echo "response_data_row_value: <br>";
							print_r($response_data_row_value); ************
							
						}//close of foreach ($response_data_row as $response_data_row_key => $response_data_row_value) {
							
						//exit;    
					}//close of foreach ($response['hits']['hits'] as $response_data_row) {

	
			return $constructed_array;
	//}
	//return $constructed_array;*/
	
	return $search_response;
}



	

exit;
?>