
<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "4")) {

	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
			    $job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
			    
				$job_applicant_request_info_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {

                if ($job_id_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-id";
					$response['status_description'] = "Missing Job Id";
					
					$eventLog->log("missing-user-id: Missing Job Id.");
					
				} else if ($company_id_input == "") {
				
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company Id";
					
					$eventLog->log("missing-company-id: Missing Company Id."); 
					
				} else if ($job_applicant_request_info_id_input == "") {
					
					$response['data'] = array();
					$response['status'] = "missing-job-applicant-request-info-id";
					$response['status_description'] = "Missing Job applicant request info Id";
					
					$eventLog->log("missing-job-applicant-request-info-id: Please provide a valid Job applicant request info Id.");
					
				
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else {
					//all inputs are valid
					try {
						
					
						$job_rel_screening_levels_list_info_result = job_rel_candidature_screening_levels_list($job_id_input,$company_id_input,$job_applicant_request_info_id_input);
				
						$job_rel_screening_levels_list_info_result_count = count($job_rel_screening_levels_list_info_result);
						
						$eventLog->log("Count -> " . $job_rel_screening_levels_list_info_result_count);
						
						if (count($job_rel_screening_levels_list_info_result) > 0) {
						
						$response['data'] = $job_rel_screening_levels_list_info_result;
						$response['status'] = "job-related-screening-levels-list-successfully-fetched";
						$response['status_description'] = "Job related screening levels List Successfully Received";
					
						} else {
							
							$response['data'] = array();
							$response['status'] = "no-screening levels-defined-for-this-job";
							$response['status_description'] = "No screening levels defined found for this job";
							
						}
							
							
					} catch (Exception $e){
						
						$response['data'] = array();
						$response['status'] = "job-related-screening-levels-list-fetching-error";
						$response['status_description'] = "Job related screening levels List Fetching Error";
					}
					
					
				}
					
			}
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
	
		
function jsl_applicant_evaluation_info_add($company_id_input,$company_client_id_input,$job_id_input,$jsl_classification_detail_id_input,$jsl_classification_detail_name_input,$jsl_classification_detail_seo_name_input,$jsl_sub_classification_detail_id_input,$jsl_sub_classification_detail_name_input,$jsl_sub_classification_detail_seo_name_input,$jsl_info_id_input,$job_applicant_request_info_id_input,$job_applicant_sm_memb_id_input,$scheduled_event_datetime_input,$scheduled_event_datetime_epoch_input,$online_interview_link_input,$telephonic_interview_number_input,$in_person_interview_location_address_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch) {
	
	
	global $dbcon,$eventLog;
	$is_active_status = '1';
	$event_status_input = '1';
    
	
	$jsl_applicant_evaluation_info_add_sql = "INSERT INTO `jsl_applicant_evaluation_info`(`company_id`, `company_client_id`, `job_id`, `jsl_classification_detail_id`, `jsl_classification_detail_name`, `jsl_classification_detail_seo_name`, `jsl_sub_classification_detail_id`, `jsl_sub_classification_detail_name`, `jsl_sub_classification_detail_seo_name`, `jsl_info_id`, `job_applicant_request_info_id`, `job_applicant_sm_memb_id`, `scheduled_event_datetime`, `scheduled_event_datetime_epoch`, `online_interview_link`, `telephonic_interview_number`, `in_person_interview_location_address`, `added_datetime`, `added_datetime_epoch`, `added_by_sm_memb_id`, `event_status`, `is_active_status`) VALUES (:company_id,:company_client_id,:job_id,:jsl_classification_detail_id,:jsl_classification_detail_name,:jsl_classification_detail_seo_name,:jsl_sub_classification_detail_id,:jsl_sub_classification_detail_name,:jsl_sub_classification_detail_seo_name,:jsl_info_id,:job_applicant_request_info_id,:job_applicant_sm_memb_id,:scheduled_event_datetime,:scheduled_event_datetime_epoch,:online_interview_link,:telephonic_interview_number,:in_person_interview_location_address,:added_datetime,:added_datetime_epoch,:added_by_sm_memb_id,:event_status,:is_active_status)";

	$jsl_applicant_evaluation_info_add_query = $dbcon->prepare($jsl_applicant_evaluation_info_add_sql);
	$jsl_applicant_evaluation_info_add_query->bindValue(":company_id",$company_id_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":company_client_id",$company_client_id_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":job_id",$job_id_input);	
	$jsl_applicant_evaluation_info_add_query->bindValue(":jsl_classification_detail_id",$jsl_classification_detail_id_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":jsl_classification_detail_name",$jsl_classification_detail_name_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":jsl_classification_detail_seo_name",$jsl_classification_detail_seo_name_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":jsl_sub_classification_detail_id",$jsl_sub_classification_detail_id_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":jsl_sub_classification_detail_name",$jsl_sub_classification_detail_name_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":jsl_sub_classification_detail_seo_name",$jsl_sub_classification_detail_seo_name_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":jsl_info_id",$jsl_info_id_input);
    $jsl_applicant_evaluation_info_add_query->bindValue(":job_applicant_request_info_id",$job_applicant_request_info_id_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":job_applicant_sm_memb_id",$job_applicant_sm_memb_id_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":scheduled_event_datetime",$scheduled_event_datetime_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":scheduled_event_datetime_epoch",$scheduled_event_datetime_epoch_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":online_interview_link",$online_interview_link_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":telephonic_interview_number",$telephonic_interview_number_input);
	$jsl_applicant_evaluation_info_add_query->bindValue(":in_person_interview_location_address",$in_person_interview_location_address_input);

	$jsl_applicant_evaluation_info_add_query->bindValue(":added_datetime",$event_datetime);
	$jsl_applicant_evaluation_info_add_query->bindValue(":added_datetime_epoch",$current_epoch);
	$jsl_applicant_evaluation_info_add_query->bindValue(":added_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$jsl_applicant_evaluation_info_add_query->bindValue(":event_status",$event_status_input);
	
	$jsl_applicant_evaluation_info_add_query->bindValue(":is_active_status",$is_active_status);
	
	if ($jsl_applicant_evaluation_info_add_query->execute()) {
          $eventLog->log("after execute.");
            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");
           
			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}

function job_screening_level_details_based_on_jsl_info_id($jsl_info_id_input) {
	
	global $dbcon;
	$constructed_array = array();
	
	$job_screening_levels_list_info_sql = "SELECT * FROM `jsl_info` WHERE `jsl_info_id` =:jsl_info_id";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":jsl_info_id",$jsl_info_id_input);		
	$job_rel_screening_levels_list_info_query->execute(); 
	
	if($job_rel_screening_levels_list_info_query->rowCount() > 0) {
		$job_rel_screening_levels_list_info_query_result = $job_rel_screening_levels_list_info_query->fetch();
	     return $job_rel_screening_levels_list_info_query_result;
	
	}
	return $constructed_array;

}

function candidate_education_qualification_records_list($user_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	
	$candidate_education_qualification_records_list_sql = "SELECT * FROM `candidate_education_qualifications` WHERE `sm_memb_id`=:sm_memb_id";
	$candidate_education_qualification_records_list_select_query = $dbcon->prepare($candidate_education_qualification_records_list_sql);
	$candidate_education_qualification_records_list_select_query->bindValue(":sm_memb_id",$user_id_input);			
	$candidate_education_qualification_records_list_select_query->execute(); 
	
	if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
		$candidate_education_qualification_records_list_select_query_result = $candidate_education_qualification_records_list_select_query->fetchAll();
		
		foreach($candidate_education_qualification_records_list_select_query_result as $candidate_education_qualification_records_list_select_query_result_row) {
			
			$temp = array();		
			$temp["ceq_id"] = $candidate_education_qualification_records_list_select_query_result_row["ceq_id"];
			//$temp["sm_memb_id"] = $candidate_education_qualification_records_list_select_query_result_row["sm_memb_id"];
			$temp["degree"] = $candidate_education_qualification_records_list_select_query_result_row["degree"];
			$temp["field_of_study"] = $candidate_education_qualification_records_list_select_query_result_row["field_of_study"];
			$temp["institution_name"] = $candidate_education_qualification_records_list_select_query_result_row["institution_name"];
			$temp["university_name"] = $candidate_education_qualification_records_list_select_query_result_row["university_name"];
			$temp["board_name"] = $candidate_education_qualification_records_list_select_query_result_row["board_name"];
			$temp["year_of_passout"] = $candidate_education_qualification_records_list_select_query_result_row["year_of_passout"];
			$temp["earned_percentage"] = $candidate_education_qualification_records_list_select_query_result_row["earned_percentage"];
			if($temp["earned_percentage"] == 0.00)
			  $temp["earned_percentage"] = null;
			$temp["earned_points"] = $candidate_education_qualification_records_list_select_query_result_row["earned_points"];
			if($temp["earned_points"] == 0.00)
			  $temp["earned_points"] = null;
			$temp["earned_grade"] = $candidate_education_qualification_records_list_select_query_result_row["earned_grade"];
			$constructed_array[] = $temp;
		}//close of foreach($candidate_education_qualification_records_list_select_query_result as $candidate_education_qualification_records_list_select_query_result_row) {
			
		
		return $constructed_array;
	
	}//close of if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
	return $constructed_array;
}

function candidature_quick_summary_info($jsl_info_id_input,$job_applicant_request_info_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	$candidate_education_qualifications_array = array();
	$uploaded_file_url = "";
					
	$candidate_education_qualification_records_list_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `jsl_info_id`=:jsl_info_id AND `job_applicant_request_info_id` = :job_applicant_request_info_id";
	$candidate_education_qualification_records_list_select_query = $dbcon->prepare($candidate_education_qualification_records_list_sql);
	$candidate_education_qualification_records_list_select_query->bindValue(":jsl_info_id",$jsl_info_id_input);
	$candidate_education_qualification_records_list_select_query->bindValue(":job_applicant_request_info_id",$job_applicant_request_info_id_input);
	$candidate_education_qualification_records_list_select_query->execute(); 
	
	if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
		$candidate_education_qualification_records_list_select_query_result = $candidate_education_qualification_records_list_select_query->fetchAll();
		
		foreach($candidate_education_qualification_records_list_select_query_result as $candidate_education_qualification_records_list_select_query_result_row) {
			
			$temp1 = array();
			$temp1 = $candidate_education_qualification_records_list_select_query_result_row;
			
			if ($temp1["earned_percentage"] == 0.00) {
				$temp1["earned_percentage"] = null;
			}//close of if ($temp1["earned_percentage"] == 0.00) {
			  
			if ($temp1["earned_points"] == 0.00) {
				$temp1["earned_points"] = null;
			}//close of if ($temp1["earned_points"] == 0.00) {
				
			    
			$candidate_education_qualifications_array[] = $temp1;
		}//close of foreach($candidate_education_qualification_records_list_select_query_result as $candidate_education_qualification_records_list_select_query_result_row) {
			
	}//close of if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {

	
	if (count($candidate_education_qualifications_array) > 0) {
		
		foreach ($candidate_education_qualifications_array as $candidate_education_qualifications_array_row) {
			
			//Extract required info from candidate_education_qualifications DB table 
			$ceq_id = $candidate_education_qualifications_array_row["ceq_id"];
			$sm_memb_id = $candidate_education_qualifications_array_row["sm_memb_id"];
			
			$activity_ref_input = '%'.'education_qualifications'.'%';
			
			//Do Query candidate_rel_all_uploaded_visa_documents DB table, for corresponding Uploaded File Information
			$candidate_education_qualification_records_rel_uploaded_file_info_sql = "SELECT * FROM `candidate_rel_all_uploaded_visa_documents` WHERE `sm_memb_id` =:sm_memb_id AND `activity_ref` LIKE :activity_ref AND `activity_ref_id` =:activity_ref_id";
			$candidate_education_qualification_records_rel_uploaded_file_info_select_query = $dbcon->prepare($candidate_education_qualification_records_rel_uploaded_file_info_sql);
			$candidate_education_qualification_records_rel_uploaded_file_info_select_query->bindValue(":sm_memb_id",$sm_memb_id);			
			$candidate_education_qualification_records_rel_uploaded_file_info_select_query->bindValue(":activity_ref",$activity_ref_input);			
			$candidate_education_qualification_records_rel_uploaded_file_info_select_query->bindValue(":activity_ref_id",$ceq_id);			
			$candidate_education_qualification_records_rel_uploaded_file_info_select_query->execute(); 
			
			if($candidate_education_qualification_records_rel_uploaded_file_info_select_query->rowCount() > 0) {
				$candidate_education_qualification_records_rel_uploaded_file_info_select_query_result = $candidate_education_qualification_records_rel_uploaded_file_info_select_query->fetchAll();
				
				foreach($candidate_education_qualification_records_rel_uploaded_file_info_select_query_result as $candidate_education_qualification_records_rel_uploaded_file_info_select_query_result_row) {
					
					$uploaded_file_rel_crauvd_id = $candidate_education_qualification_records_rel_uploaded_file_info_select_query_result_row["crauvd_id"];
					$uploaded_file_rel_additional_document_ref = $candidate_education_qualification_records_rel_uploaded_file_info_select_query_result_row["additional_document_ref"];
					$uploaded_file_rel_original_filename = $candidate_education_qualification_records_rel_uploaded_file_info_select_query_result_row["original_filename"];
					$uploaded_file_rel_generated_filename = $candidate_education_qualification_records_rel_uploaded_file_info_select_query_result_row["generated_filename"];
					$uploaded_file_rel_file_size_bytes = $candidate_education_qualification_records_rel_uploaded_file_info_select_query_result_row["file_size_bytes"];
					$uploaded_file_rel_file_size_kilo_bytes = $candidate_education_qualification_records_rel_uploaded_file_info_select_query_result_row["file_size_kilo_bytes"];
					$uploaded_file_rel_file_size_mega_bytes = $candidate_education_qualification_records_rel_uploaded_file_info_select_query_result_row["file_size_mega_bytes"];
					$uploaded_file_rel_is_active_status = $candidate_education_qualification_records_rel_uploaded_file_info_select_query_result_row["is_active_status"];
					
					//Link Expiry Time
					$link_expiry_time = $current_epoch+$expiring_link_lifetime;//from /app/core/main-config.php
				
					
					//Create Uploaded File Link with Signature
					$uploaded_file_link_with_signature_result = create_uploaded_file_link_with_signature($ea_extracted_jwt_token_sub, $user_id_input, $uploaded_file_rel_crauvd_id, "https", $site_hostname_value, $link_expiry_time, $expiring_link_hash_algorithm, $expiring_link_secret_key);
					
					if (isset($uploaded_file_link_with_signature_result["uploaded_file_url"])) {
						$uploaded_file_url = $uploaded_file_link_with_signature_result["uploaded_file_url"];
						
					}//close of if (isset($uploaded_file_link_with_signature_result["uploaded_file_url"])) {
						
						if (($uploaded_file_rel_is_active_status == "2") && (!is_null($uploaded_file_rel_generated_filename))) {
						//Uploaded File is Confirmed by the Company Management / Super Admin / Site Admin (even though it is expected to be done by the Company Management)
						
							$temp2 = array();

							$temp2["uploaded_file_crauvd_id"] = $uploaded_file_rel_crauvd_id;
							$temp2["uploaded_file_additional_document_ref"] = $uploaded_file_rel_additional_document_ref;
							$temp2["uploaded_file_original_filename"] = $uploaded_file_rel_original_filename;
							$temp2["uploaded_file_generated_name"] = $uploaded_file_rel_generated_filename;
							$temp2["uploaded_file_size_bytes"] = $uploaded_file_rel_file_size_bytes;
							$temp2["uploaded_file_size_kilo_bytes"] = $uploaded_file_rel_file_size_kilo_bytes;
							$temp2["uploaded_file_size_mega_bytes"] = $uploaded_file_rel_file_size_mega_bytes;
							$temp2["uploaded_file_url"] = $uploaded_file_url;
							$temp2["file_upload_status"] = "2";
							
							
							//$candidate_education_qualifications_array_row[] = $temp2;
							//array_push($candidate_education_qualifications_array_row, $temp2);
							$candidate_education_qualifications_array_row["file"] = $temp2;
							
						} else if (($uploaded_file_rel_is_active_status == "1") && (!is_null($uploaded_file_rel_generated_filename))) {
							//Uploaded File is Awaiting Confirmation from the Company Management / Super Admin / Site Admin (even though it is expected to be done by the Company Management)
							
							$temp3 = array();

							$temp3["uploaded_file_crauvd_id"] = $uploaded_file_rel_crauvd_id;
							$temp3["uploaded_file_additional_document_ref"] = $uploaded_file_rel_additional_document_ref;
							$temp3["uploaded_file_original_filename"] = $uploaded_file_rel_original_filename;
							$temp3["uploaded_file_generated_name"] = $uploaded_file_rel_generated_filename;
							$temp3["uploaded_file_size_bytes"] = $uploaded_file_rel_file_size_bytes;
							$temp3["uploaded_file_size_kilo_bytes"] = $uploaded_file_rel_file_size_kilo_bytes;
							$temp3["uploaded_file_size_mega_bytes"] = $uploaded_file_rel_file_size_mega_bytes;
							$temp3["uploaded_file_url"] = $uploaded_file_url;
							$temp3["file_upload_status"] = "1";
							
							
							//$candidate_education_qualifications_array_row[] = $temp3;
							//array_push($candidate_education_qualifications_array_row, $temp3);
							$candidate_education_qualifications_array_row["file"] = $temp3;
							
						} else {
							//The File is yet to be Uploaded by the particular Candidate
							if (($uploaded_file_rel_is_active_status != "0") && ($uploaded_file_rel_is_active_status != "3") && ($uploaded_file_rel_is_active_status != "4")) {
								//array_push($candidate_education_qualifications_array_row, $uploaded_file_info_default_array);
								//File Upload related info
								$uploaded_file_info_default_array = array();
								$uploaded_file_info_default_array["uploaded_file_crauvd_id"] = $uploaded_file_rel_crauvd_id;
								$uploaded_file_info_default_array["uploaded_file_additional_document_ref"] = $uploaded_file_rel_additional_document_ref;
								$uploaded_file_info_default_array["uploaded_file_original_filename"] = "";
								$uploaded_file_info_default_array["uploaded_file_generated_name"] = "";
								$uploaded_file_info_default_array["uploaded_file_size_bytes"] = "";
								$uploaded_file_info_default_array["uploaded_file_size_kilo_bytes"] = "";
								$uploaded_file_info_default_array["uploaded_file_size_mega_bytes"] = "";
								$uploaded_file_info_default_array["uploaded_file_url"] = "";
								$uploaded_file_info_default_array["file_upload_status"] = "0";
								
								$candidate_education_qualifications_array_row["file"] = $uploaded_file_info_default_array;
							}//close of if (($uploaded_file_rel_is_active_status != "0") && ($uploaded_file_rel_is_active_status != "3") && ($uploaded_file_rel_is_active_status != "4")) {
							
						}//close of else of if (($uploaded_file_rel_is_active_status == "2") && (!is_null($uploaded_file_rel_is_active_status))) {
						
				
						
					
					
				}//close of foreach($candidate_education_qualification_records_list_select_query_result as $candidate_education_qualification_records_list_select_query_result_row) {
					
			}//close of if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
			
			//array_push($candidate_education_qualifications_array_row, $uploaded_file_info_default_array);
			
			$constructed_array[] = $candidate_education_qualifications_array_row;
			
		}//close of foreach ($candidate_education_qualifications_array as $candidate_education_qualifications_array_row) {
		
		return $constructed_array;
		
	} else {
		//Education Qualifications are not entered for this particular Candidate
		
		return $candidate_education_qualifications_array;
		
	}//close of else of if (count($candidate_education_qualifications_array) > 0) {
		
}
function job_rel_candidature_screening_levels_list($job_id_input,$company_id_input,$job_applicant_request_info_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	$assignment_status = "1";
	
	$job_rel_candidature_screening_levels_list_sql = "SELECT * FROM `jsl_info` WHERE `job_id` =:job_id AND `company_id` =:company_id";
	$job_rel_candidature_screening_levels_list_query = $dbcon->prepare($job_rel_candidature_screening_levels_list_sql);
	$job_rel_candidature_screening_levels_list_query->bindValue(":job_id",$job_id_input);		
	$job_rel_candidature_screening_levels_list_query->bindValue(":company_id",$company_id_input);
	$job_rel_candidature_screening_levels_list_query->execute(); 
	
	if($job_rel_candidature_screening_levels_list_query->rowCount() > 0) {
		$job_rel_candidature_screening_levels_list_query_result = $job_rel_candidature_screening_levels_list_query->fetchAll();
		
		foreach($job_rel_candidature_screening_levels_list_query_result as $job_rel_candidature_screening_levels_list_query_result_row) {
			
			$temp_array = array();		
			
			$jsl_info_id_input= $job_rel_candidature_screening_levels_list_query_result_row["jsl_info_id"];
			$jsl_title_input= $job_rel_candidature_screening_levels_list_query_result_row["jsl_title"];
		
			$temp_array = jsl_specific_candidature_details($jsl_info_id_input,$jsl_title_input, $job_applicant_request_info_id_input);
			
			
			
			
			$constructed_array[] = $temp_array;
		}
		
		return $constructed_array;
	
	}//close of if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
	return $constructed_array;
}


function jsl_specific_candidature_details($jsl_info_id_input, $jsl_title_input, $job_applicant_request_info_id_input) {
	global $dbcon;
	$constructed_array = array();
	
	$constructed_array["jsl_title"] = $jsl_title_input;
	
	$jma_roles_array = array();
	$jma_roles_array["status"] = "Yet to be scheduled";
	//$jma_roles_array["scheduled_event_datetime"] = "";
	
	
	$jsl_specific_candidature_details_sql = "SELECT * FROM `jsl_applicant_evaluation_info` jaei LEFT JOIN `jsl_applicant_evaluation_info_result_notes` jaeirn  ON jaei.jsl_applicant_evaluation_info_id = jaeirn.jsl_applicant_evaluation_info_id WHERE jaei.jsl_info_id =:jsl_info_id AND jaei.job_applicant_request_info_id =:job_applicant_request_info_id";
	$jsl_specific_candidature_details_query = $dbcon->prepare($jsl_specific_candidature_details_sql);
	$jsl_specific_candidature_details_query->bindValue(":jsl_info_id",$jsl_info_id_input);
    $jsl_specific_candidature_details_query->bindValue(":job_applicant_request_info_id",$job_applicant_request_info_id_input);
	$jsl_specific_candidature_details_query->execute();
	
	if($jsl_specific_candidature_details_query->rowCount() > 0) {
		$jsl_specific_candidature_details_query_result = $jsl_specific_candidature_details_query->fetchAll();
		
		foreach ($jsl_specific_candidature_details_query_result as $jsl_specific_candidature_details_query_result_row) {
			
			
			$event_status = $jsl_specific_candidature_details_query_result_row["event_status"];
			$scheduled_event_datetime = $jsl_specific_candidature_details_query_result_row["scheduled_event_datetime"];
			$notes = $jsl_specific_candidature_details_query_result_row["notes"];
				
			
			if ($event_status == "1") {
				
				$jma_roles_array["status"] = "Scheduled";
				$jma_roles_array["scheduled_event_datetime"] = $scheduled_event_datetime;
				$jma_roles_array["notes"] = $notes;	
				
			} else if ($event_status == "2") {
		
				$jma_roles_array["status"] = "Re-Scheduled";
				$jma_roles_array["scheduled_event_datetime"] = $scheduled_event_datetime;
				$jma_roles_array["notes"] = $notes;	
						
			} else if ($event_status == "3") {
		
				$jma_roles_array["status"] = "Skipped Temporarily";
				$jma_roles_array["scheduled_event_datetime"] = $scheduled_event_datetime;
				$jma_roles_array["notes"] = $notes;	
				
			} else if ($event_status == "4") {
		
				$jma_roles_array["status"] = "Shortlisted";
				$jma_roles_array["scheduled_event_datetime"] = $scheduled_event_datetime;
				$jma_roles_array["notes"] = $notes;	
				
			} else if ($event_status == "5") {
		
				$jma_roles_array["status"] = "On-hold";
				$jma_roles_array["scheduled_event_datetime"] = $scheduled_event_datetime;
				$jma_roles_array["notes"] = $notes;	
				
			} else if ($event_status == "6") {
		
				$jma_roles_array["status"] = "Rejected";
				$jma_roles_array["scheduled_event_datetime"] = $scheduled_event_datetime;
				$jma_roles_array["notes"] = $notes;	
				
			} else if ($event_status == "7") {
		
				$jma_roles_array["status"] = "Selected";
				$jma_roles_array["scheduled_event_datetime"] = $scheduled_event_datetime;
				$jma_roles_array["notes"] = $notes;	
				
			}
			
		}//close of foreach ($job_management_assignees_get_select_query_result as $job_management_assignees_get_select_query_result_row) {
		
		$constructed_array["quick_summary"] = $jma_roles_array;
		
	} else {
		$constructed_array["quick_summary"] = $jma_roles_array;
	}
	return $constructed_array;

}


exit;
?>