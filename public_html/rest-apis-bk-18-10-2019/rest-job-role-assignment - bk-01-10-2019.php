<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * REST API for candidate all education qualifications collect
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "6")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['user_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['user_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['user_id']))
                    
                    if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id']))

					if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_client_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id']))
					
					if (isset($ea_received_rest_ws_raw_array_input['job_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_id']))
					
					if (isset($ea_received_rest_ws_raw_array_input['all_job_rel_assignees'])) {
						$content .= json_encode($ea_received_rest_ws_raw_array_input['all_job_rel_assignees']) . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['all_job_rel_assignees'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					//$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$company_client_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_client_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_client_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$all_job_rel_assignees_input = $ea_received_rest_ws_raw_array_input['all_job_rel_assignees'];
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//Check if all inputs are received correctly from the REST Web Service
				if ($user_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-user-id";
					$response['status_description'] = "Missing User ID";
					
					$eventLog->log("missing-user-id: Please provide a valid User ID.");
					
				} else if ($company_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company ID";
					
					$eventLog->log("missing-company-id: Please provide a valid Company ID.");
					
				} else if ($company_client_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-client-id";
					$response['status_description'] = "Missing Company Client ID";
					
					$eventLog->log("missing-company-client-id: Please provide a valid Company Client ID.");
					
				} else if ($job_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-id";
					$response['status_description'] = "Missing Job ID";
					
					$eventLog->log("missing-job-id: Please provide a valid Job ID.");
					
					
				} else if ($all_job_rel_assignees_input == "") {	
					//Invalid Education Qualifications Received scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "empty-job-rel-assignees-input";
					$response['status_description'] = "Empty Job rel assignees Input";
					
					$eventLog->log("empty-job-rel-assignees-input: Empty Job rel assignees Input, Please check and try again.");
					
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Missing Some Additional Information";
					
					$eventLog->log("missing-some-additional-information: Some Additional Information is missing, please check and try again.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					$ea_action_specific_auth_check_result = "";
					
					//User Details, based on User ID input, through api request
					$user_basic_details_result = user_basic_details_check_based_on_user_id($user_id_input);
					
					if (count($user_basic_details_result) > 0) {
						
						$user_rel_company_id = $user_basic_details_result["company_id"];
						$user_rel_sm_user_status = $user_basic_details_result["sm_user_status"];
						
						$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
						
						
						if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
								$eventLog->log("inter into try");
								//Event Time, as per Indian Standard Time
								$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
								
									
								//Define an array, to Collect "degree" and "field of study" field values of all received Education Qualification info.
								$all_received_job_id_assigned_user_id_assigned_user_role_fields_array = array();
								
								//Define an array, to Collect "degree" and "field of study" field values of all Education Qualification info, from the Database.
								$all_db_based_job_id_assigned_user_id_assigned_user_role_fields_array = array();
								
								
								
								$all_collected_content_with_result_array = array();
								$eventLog->log("before count");
				
								$job_rel_assignees_records_list_result = job_rel_assignees_records_list($job_id_input);
								$eventLog->log("after count");
								$job_rel_assignees_records_list_result_count = count($job_rel_assignees_records_list_result);
								
								$eventLog->log("job_rel_assignees_records_list_result_count: " . $job_rel_assignees_records_list_result_count);
								
								
								if ((is_array($all_job_rel_assignees_input)) && ((count($all_job_rel_assignees_input) > 0) || ($job_rel_assignees_records_list_result_count > 0))) {
										
									//Do process each record (Education Qualification)
									foreach ($all_job_rel_assignees_input as $all_job_rel_assignees_input_row) {
										
										$received_job_id_assigned_user_id_assigned_user_role_fields_array = array();
										$collected_content_with_result_array = array();
										
										$collected_content_with_result_array = $all_job_rel_assignees_input_row;
										
										
										$collected_content_with_result_array_json_encoded = json_encode($collected_content_with_result_array);
										$eventLog->log("collected_array row initial stage: " . $collected_content_with_result_array_json_encoded);	
										
										$all_job_rel_assignees_input_row_count = count($all_job_rel_assignees_input_row);
										//$eventLog->log("collected_array row initial stage count: " . $all_education_qualifications_input_row_count);
										
										if (count($all_job_rel_assignees_input_row) == 6) {
											
											$eventLog->log("inside count(all_job_rel_assignees_input_row) == 10 if condition: ");
											
										
											$jma_id = trim(isset($all_job_rel_assignees_input_row["jma_id"]) ? filter_var($all_job_rel_assignees_input_row["jma_id"], FILTER_SANITIZE_NUMBER_INT) : '');
											$assigned_user_sm_memb_id = trim(isset($all_job_rel_assignees_input_row["assigned_user_sm_memb_id"]) ? filter_var($all_job_rel_assignees_input_row["assigned_user_sm_memb_id"], FILTER_SANITIZE_NUMBER_INT) : '');
											$is_primary_recruiter = trim(isset($all_job_rel_assignees_input_row["is_primary_recruiter"]) ? filter_var($all_job_rel_assignees_input_row["is_primary_recruiter"], FILTER_SANITIZE_NUMBER_INT) : '');
											$is_recruiter = trim(isset($all_job_rel_assignees_input_row["is_recruiter"]) ? filter_var($all_job_rel_assignees_input_row["is_recruiter"], FILTER_SANITIZE_NUMBER_INT) : '');
											$is_primary_sales = trim(isset($all_job_rel_assignees_input_row["is_primary_sales"]) ? filter_var($all_job_rel_assignees_input_row["is_primary_sales"], FILTER_SANITIZE_NUMBER_INT) : '');
											$is_sales = trim(isset($all_job_rel_assignees_input_row["is_sales"]) ? filter_var($all_job_rel_assignees_input_row["is_sales"], FILTER_SANITIZE_NUMBER_INT) : '');
											
											$received_job_id_assigned_user_id_assigned_user_role_fields_array["assigned_user_sm_memb_id"] = $all_job_rel_assignees_input_row["assigned_user_sm_memb_id"];
											
											$job_rel_assigned_roles_array = array();
											//$job_rel_assigned_roles_array["is_primary_recruiter"] = $is_primary_recruiter;
											$job_rel_assigned_roles_array["3"] = $is_primary_recruiter;
											//$job_rel_assigned_roles_array["is_recruiter"] = $is_recruiter;
											$job_rel_assigned_roles_array["4"] = $is_recruiter;
											//$job_rel_assigned_roles_array["is_primary_sales"] = $is_primary_sales;
											$job_rel_assigned_roles_array["1"] = $is_primary_sales;
											//$job_rel_assigned_roles_array["is_sales"] = $is_sales;
											$job_rel_assigned_roles_array["2"] = $is_sales;
											
				
											
											if ($assigned_user_sm_memb_id == "") {
												//Empty Degree Field
												
												$collected_content_with_result_array["assigned_user_sm_memb_id"] = $assigned_user_sm_memb_id;
												
												$eventLog->log("Empty Assigned User sm_memb_id input");
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
											} else if ($is_primary_recruiter == "") {
												//Empty Field of Study Field
												
												$collected_content_with_result_array["is_primary_recruiter"] = $is_primary_recruiter;
												
												$eventLog->log("Empty Is Primary Recruiter input");
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
											} else if ($is_recruiter == "") {
												//three fields are Empty
												
												$collected_content_with_result_array["is_recruiter"] = $is_recruiter;
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
												$eventLog->log("Empty Is Recruiter Input");
												
											} else if ($is_primary_sales == "") {
												//Empty Field of Study Field
												
												$collected_content_with_result_array["is_primary_sales"] = $is_primary_sales;
												
												$eventLog->log("Empty Is Primary Sales input");
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
											} else if ($is_sales == "") {
												//three fields are Empty
												
												$collected_content_with_result_array["is_sales"] = $is_sales;
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
												$eventLog->log("Empty Is Sales Input");
											
							
											} else {
												
												
												$eventLog->log("All Education Qualification Details are valid, for this record");
												
												foreach ($job_rel_assigned_roles_array as $job_rel_assigned_roles_array_key => $job_rel_assigned_roles_array_row) {
													
													if($job_rel_assigned_roles_array_row == 1) {
														
									
														//Do Duplicate Check, for each Education Qualification, in the application, w.r.t. the particular candidate's sm_memb_id
														$job_rel_assignees_duplicate_check_result = job_rel_assignees_duplicate_check($job_id_input, $assigned_user_sm_memb_id, $job_rel_assigned_roles_array_key, $company_id_input);
														
														if (count($job_rel_assignees_duplicate_check_result) > 0) {
															//Do get the reference id
															$jma_id_from_db = $job_rel_assignees_duplicate_check_result["jma_id"];
															$eventLog->log("jma_id_from_db :");
															$eventLog->log($jma_id_from_db);
															$eventLog->log("jma_id:");
															$eventLog->log($jma_id);
															
															
														
														} else {
													
												
													
															$eventLog->log("Insert Query Condition");
															//echo $job_rel_assigned_roles_array_key;
															$eventLog->log("job_rel_assigned_roles_array_key: " . $job_rel_assigned_roles_array_key);
															$eventLog->log("user_id_input: " . $user_id_input);
															$eventLog->log("company_id_input: " . $company_id_input);
															$eventLog->log("company_client_id_input: " . $company_client_id_input);
															$eventLog->log("job_id_input: " . $job_id_input);
															$eventLog->log("assigned_user_sm_memb_id: " . $assigned_user_sm_memb_id);
												
															$eventLog->log("ea_extracted_jwt_token_sub: " . $ea_extracted_jwt_token_sub);
															$eventLog->log("event_datetime: " . $event_datetime);
															$eventLog->log("current_epoch: " . $current_epoch);
													
															
															$eventLog->log("before insert");
															//Do Insert Query to add a new record
															$job_rel_assignees_insert_result = job_rel_assignees_insert($company_id_input, $company_client_id_input, $job_id_input, $assigned_user_sm_memb_id, $job_rel_assigned_roles_array_key, $ea_extracted_jwt_token_sub, $event_datetime, $current_epoch);
															
															if (isset($job_rel_assignees_insert_result["last_inserted_id"])) {
																//Insert Success
																$collected_content_with_result_array["jma_id"] = $job_rel_assignees_insert_result["last_inserted_id"];
																$collected_content_with_result_array["query_type"] = "INSERT-QUERY";
																$collected_content_with_result_array["query_status"] = "SUCCESS";
																$collected_content_with_result_array["input_record_validity_status"] = "VALID";
																
																$eventLog->log("Insert Query Condition - Success");
																//$additional_document_ref_input = $degree . " (" . $field_of_study . ")";
																$last_inserted_job_rel_assignees_id = $job_rel_assignees_insert_result["last_inserted_id"];
																//candidate_education_qualification_rel_file_upload_references_insert($user_id_input,"education-qualifications",$last_inserted_candidate_education_qualification_id,$additional_document_ref_input,null,null,$ea_extracted_jwt_token_user_type,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
																
																//COMMENTED< PLEASE CHANGE THIS FUNCTION candidate_vrpi_rel_file_upload_references_insert($user_id_input,"visa-rel-personal-info",$last_inserted_candidate_visa_rel_personal_info_id,$additional_document_ref,null,null,$ea_extracted_jwt_token_user_type,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
																
															} else {
																//Insert Failure
																$collected_content_with_result_array["creq_id"] = null;
																$collected_content_with_result_array["query_type"] = "INSERT-QUERY";
																$collected_content_with_result_array["query_status"] = "FAILURE";
																$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
																$eventLog->log("Insert Query Condition - Failure");
																
															}//close of else of if (isset($candidate_education_qualification_insert_result["last_inserted_id"])) {
														
													
														}//close of else of if (count($candidate_education_qualification_check_result) > 0) {
												
														$collected_content_with_result_array_json_encoded = json_encode($collected_content_with_result_array);
														//$eventLog->log("collected_array row next stage: " . $collected_content_with_result_array_json_encoded);	
														
													} else {
														
														//Do Duplicate Check, for each Education Qualification, in the application, w.r.t. the particular candidate's sm_memb_id
														$job_rel_assignees_duplicate_check_result = job_rel_assignees_duplicate_check($job_id_input, $assigned_user_sm_memb_id, $job_rel_assigned_roles_array_key, $company_id_input);
														
														if (count($job_rel_assignees_duplicate_check_result) > 0) {
															//Do get the reference id
															$jma_id_from_db = $job_rel_assignees_duplicate_check_result["jma_id"];
															$eventLog->log("jma_id_from_db :");
															$eventLog->log($jma_id_from_db);
															
																$collected_content_with_result_array["jma_id"] = $jma_id_from_db;
															
																$eventLog->log("Update Query Condition");
																
																//Do Update Query to existing record
																$job_rel_assignees_update_result = update_job_rel_assignees_based_on_jma_id($jma_id_from_db, $ea_extracted_jwt_token_sub, $event_datetime, $current_epoch);
																
																if ($job_rel_assignees_update_result) {
																	//Update Success
																	$collected_content_with_result_array["query_type"] = "UPDATE-QUERY";
																	$collected_content_with_result_array["query_status"] = "SUCCESS";
																	$collected_content_with_result_array["input_record_validity_status"] = "VALID";
																	
																	$eventLog->log("Update Query Condition - Success");
																	
																} else {
																	$collected_content_with_result_array["query_type"] = "UPDATE-QUERY";
																	$collected_content_with_result_array["query_status"] = "FAILURE";
																	$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
													
																	$eventLog->log("Update Query Condition - Failure");
																	
																}//close of else of if ($candidate_education_qualification_update_result) {
																
																
															} else {
																//ceq_id from the api request and from db source are different, for particular education qualification record.
																$eventLog->log("jma_id from the api request and from db source are different, for particular job assignees record.");
																
																$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
																
															}//close of else of if ($ceq_id == $ceq_id_from_db) {
															
														
														
													}
													
												}
											}//close of else of if ($degree == "") {
												
											
										} else {
											
											$eventLog->log("inside count(all_education_qualifications_input_row) == 10 else condition: ");
											
											//Expected number of fields are not sent, per education qualification record
											$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
										
										}//close of else of if (count($all_education_qualifications_input_row)) {
										
										$all_received_job_id_assigned_user_id_assigned_user_role_fields_array[] = $received_job_id_assigned_user_id_assigned_user_role_fields_array;
										$all_collected_content_with_result_array[] = $collected_content_with_result_array;
																								
										
									}//close of foreach ($all_education_qualifications_input as $all_education_qualifications_input_row) {
									
									$all_collected_content_with_result_array_json_encoded = json_encode($all_collected_content_with_result_array);
									$eventLog->log("collected_array final (didnot enter foreach loop): " . $all_collected_content_with_result_array_json_encoded);	
									
								}	
							
									/* if (count($job_rel_assignees_records_list_result) > 0) {
										//One or More than one Education Qualification is received from the Database.
										
										$all_received_job_id_assigned_user_id_assigned_user_role_fields_array_json_encoded = json_encode($all_received_job_id_assigned_user_id_assigned_user_role_fields_array);
										$eventLog->log("all_received_degree_fieldofstudy_fields_array_json_encoded : " . $all_received_job_id_assigned_user_id_assigned_user_role_fields_array_json_encoded);	
										
										//Do process the record (Education Qualification), and Collect "degree" and "field of study" field values of received Education Qualification
										foreach($job_rel_assignees_records_list_result as $job_rel_assignees_records_list_result_row) {
											
											$db_based_ceq_id_field_value = $job_rel_assignees_records_list_result_row["creq_id"];
											$db_based_degree_field_value = $job_rel_assignees_records_list_result_row["major"];
											$db_based_fieldofstudy_field_value = $job_rel_assignees_records_list_result_row["educational_program"];
											//$db_based_additional_document_ref = $db_based_degree_field_value . " (" . $db_based_fieldofstudy_field_value . ")";
											
											$eventLog->log("db_based_degree_field_value: ". $db_based_degree_field_value);
											$eventLog->log("db_based_fieldofstudy_field_value: ". $db_based_fieldofstudy_field_value);
											
											$record_deletion_status = "not-matched";
											
											foreach ($all_received_degree_fieldofstudy_fields_array as $all_received_degree_fieldofstudy_fields_array_row) {
												$all_received_row_based_degree_field_value = $all_received_degree_fieldofstudy_fields_array_row["major"];
												$all_received_row_based_fieldofstudy_field_value = $all_received_degree_fieldofstudy_fields_array_row["educational_program"];
												
												$eventLog->log("all_received_row_based_degree_field_value: ". $all_received_row_based_degree_field_value);
												$eventLog->log("all_received_row_based_fieldofstudy_field_value: ". $all_received_row_based_fieldofstudy_field_value);
											
												if (($all_received_row_based_degree_field_value == $db_based_degree_field_value) && ($all_received_row_based_fieldofstudy_field_value == $db_based_fieldofstudy_field_value)) {
													//Delete this matching Record, from the Form
													
													$eventLog->log("Match Found with: Degree: " . $all_received_row_based_degree_field_value . " Field of Study: " . $all_received_row_based_fieldofstudy_field_value);
													
													$record_deletion_status = "matched";
													
													break;
													
												}//close of if (($all_received_row_based_degree_field_value != $db_based_degree_field_value) && ($all_received_row_based_fieldofstudy_field_value != $db_based_fieldofstudy_field_value)) {
												 */	
											//}//close of foreach ($all_received_degree_fieldofstudy_fields_array as $all_received_degree_fieldofstudy_fields_array_row) {
											
											/* -----------------if ($record_deletion_status == "not-matched") {
												
												$candidate_education_qualification_unused_file_record_delete_status_result = delete_candidate_unused_file_record_based_on_activity_ref_id_input($db_based_ceq_id_field_value,$db_based_additional_document_ref);
												
												$eventLog->log("After delete query");
												
												$values = json_encode($candidate_education_qualification_unused_file_record_delete_status_result);
												
												$eventLog->log($values);
											
												if (count($candidate_education_qualification_unused_file_record_delete_status_result)>0) {
													//Candidate specific Professional Summary record is Deleted Successfully.
													$eventLog->log("Candidate specific education qualification unused file record is Deleted Successfully.");
											
													$candidate_education_qualification_record_delete_status_result = delete_candidate_education_qualification_record_based_on_ceq_id_input($db_based_ceq_id_field_value);
													
													if($candidate_education_qualification_record_delete_status_result) {
																$eventLog->log("Candidate specific Educational Qualification record is Deleted Successfully.");
													}	
												
												} else {
													//Error Deleting the Candidate specific Professional Summary record, that is not received in the API Request.
													$eventLog->log("Error Deleting the Candidate specific Educational Qualification record, that is not received in the API Request.");
													
												}//close of else of if ($candidate_professional_summary_record_delete_status_result) {
												
												
											}//close of if ($record_deletion_status == "not-matched") { ---------------*/
												
											
										//}//close of foreach($candidate_education_qualification_records_list_result as $candidate_education_qualification_records_list_result_row) {
										
										
											/* } else {
										//Education Qualifications of the Candidate are not available in the Database, this means, Nothing to Remove.
										$eventLog->log("Education Qualifications of the Candidate are not available in the Database, this means, Nothing to Remove.");
										
									}//close of else of if (count($candidate_education_qualification_records_list_result) > 0) {
										 */
						
									$all_collected_content_with_result_array_json_encoded = json_encode($all_collected_content_with_result_array);
									$eventLog->log("all_collected_content_with_result_array_json_encoded: " . $all_collected_content_with_result_array_json_encoded);
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = $all_collected_content_with_result_array;
									$response['status'] = "job-related-assignees-data-ingestion-result";
									$response['status_description'] = "All Job related assignees Data is processed.";
									
									$eventLog->log("job-related-assignees-data-ingestion-result: All Job related assignees Data is processed");	
									
								/* else {
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "no-records-received";
									$response['status_description'] = "Please Submit at least One Education Qualification.";
									
									$eventLog->log("no-records-received: Please Submit at least One Education Qualification.");	
									
								}//close of else of if ((is_array($all_education_qualifications_input)) && ((count($all_education_qualifications_input) > 0) || ($candidate_education_qualification_records_list_result_count > 0))) {
								 */
								
							} catch (Exception $e) {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "job-related-assignees-data-processing-error";
								$response['status_description'] = "Error occurred when processing All Job related assignees Data.";
								
								$eventLog->log("Error occurred when processing All Job related assignees Data.");	
								
							}
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-subject-platform-scope-user";
							$response['status_description'] = "Insufficient Set of Permissions, to complete this action";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-subject-platform-scope-user: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-different-company-user-id";
							$response['status_description'] = "Insufficient Set of Permissions, as the User belongs to a different company";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-INSUFFICIENT-PERMISSIONS") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions, as this process will not be allowed any further";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-self-action";
							$response['status_description'] = "Insufficient Set of Permissions, as self action is Dis-allowed";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-disallowed-self-action: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-ACTION-SUBJECT-IS-A-DIFFERENT-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-action-on-different-user";
							$response['status_description'] = "Insufficient Set of Permissions, as action subject is a different user, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-disallowed-action-on-different-user: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						}//close of if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
						
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "Invalid User Account that is attempted to be edited";
						
						$eventLog->log("invalid-user-account: The User Account, that is attempted to be edited, is invalid, please check and try again.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
						
					
				}//close of else of if ($user_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token";
			
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");	
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input";
	
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));	
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){


function job_rel_assignees_duplicate_check($job_id_input, $assigned_user_sm_memb_id, $job_rel_assigned_roles_array_key, $company_id_input){
	
	global $dbcon;
	
	$constructed_array = array();
	
	
	$job_rel_assignees_check_sql = "SELECT * FROM `job_management_assignees` WHERE `job_id`=:job_id AND `assigned_user_sm_memb_id`=:assigned_user_sm_memb_id AND `assigned_job_specific_role` = :assigned_job_specific_role AND `company_id`=:company_id";
	$job_rel_assignees_check_select_query = $dbcon->prepare($job_rel_assignees_check_sql);
	$job_rel_assignees_check_select_query->bindValue(":job_id",$job_id_input);
    $job_rel_assignees_check_select_query->bindValue(":company_id",$company_id_input);		
	$job_rel_assignees_check_select_query->bindValue(":assigned_user_sm_memb_id",$assigned_user_sm_memb_id);	
	$job_rel_assignees_check_select_query->bindValue(":assigned_job_specific_role",$job_rel_assigned_roles_array_key);		
	$job_rel_assignees_check_select_query->execute(); 
	
	if($job_rel_assignees_check_select_query->rowCount() > 0) {
		$job_rel_assignees_check_select_query_result = $job_rel_assignees_check_select_query->fetch();
	    return $job_rel_assignees_check_select_query_result;
	
	}//close of if($candidate_education_qualification_check_select_query->rowCount() > 0) {
	return $constructed_array;
	
}

function update_job_rel_assignees_based_on_jma_id($jma_id_from_db, $ea_extracted_jwt_token_sub, $event_datetime, $current_epoch) {
	global $dbcon;
	$assignment_status = "0";
	
	$update_job_rel_assignees_based_on_jma_id_sql = "UPDATE `job_management_assignees` SET `assignment_status`=:assignment_status,`assignment_last_updated_by_sm_memb_id`=:assignment_last_updated_by_sm_memb_id,`last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch WHERE `jma_id`=:jma_id";	
	$update_job_rel_assignees_based_on_jma_id_query = $dbcon->prepare($update_job_rel_assignees_based_on_jma_id_sql);		
	$update_job_rel_assignees_based_on_jma_id_query->bindValue(":assignment_status",$assignment_status);
	$update_job_rel_assignees_based_on_jma_id_query->bindValue(":assignment_last_updated_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$update_job_rel_assignees_based_on_jma_id_query->bindValue(":jma_id",$jma_id_from_db);
	$update_job_rel_assignees_based_on_jma_id_query->bindValue(":last_updated_date_time",$event_datetime);
	$update_job_rel_assignees_based_on_jma_id_query->bindValue(":last_updated_date_time_epoch",$current_epoch);
	
	
	if ($update_job_rel_assignees_based_on_jma_id_query->execute()) {
		return true;
		
	}//close of if ($update_job_rel_assignees_based_on_jma_id->execute()) {
	
	return false;
}

function job_rel_assignees_insert($company_id_input, $company_client_id_input, $job_id_input, $assigned_user_sm_memb_id, $job_rel_assigned_roles_array_key, $ea_extracted_jwt_token_sub, $event_datetime, $current_epoch){
	global $dbcon,$eventLog;
	
	$constructed_array = array();
	
	//Do Insert Query
    $eventLog->log("enter into function");
	$job_rel_assignees_insert_sql = "INSERT INTO `job_management_assignees`(`company_id`, `company_client_id`, `job_id`, `assigned_user_sm_memb_id`, `assigned_job_specific_role`, `assignment_added_by_sm_memb_id`, `added_date_time`, `added_date_time_epoch`) VALUES (:company_id,:company_client_id,:job_id,:assigned_user_sm_memb_id,:assigned_job_specific_role,:assignment_added_by_sm_memb_id,:added_date_time,:added_date_time_epoch)";
	$eventLog->log($job_rel_assignees_insert_sql);
	$job_rel_assignees_insert_query = $dbcon->prepare($job_rel_assignees_insert_sql);
	$job_rel_assignees_insert_query->bindValue(":job_id",$job_id_input);
	$eventLog->log("job_id->".$job_id_input);
	$job_rel_assignees_insert_query->bindValue(":company_id",$company_id_input);
	$eventLog->log("company_id->".$company_id_input);
	$job_rel_assignees_insert_query->bindValue(":company_client_id",$company_client_id_input);
	$eventLog->log("company_client_id->".$company_client_id_input);
	$job_rel_assignees_insert_query->bindValue(":assigned_user_sm_memb_id",$assigned_user_sm_memb_id);
	$eventLog->log("assigned_user_sm_memb_id->".$assigned_user_sm_memb_id);
	$job_rel_assignees_insert_query->bindValue(":assigned_job_specific_role",$job_rel_assigned_roles_array_key);
	$eventLog->log("assigned_job_specific_role->".$job_rel_assigned_roles_array_key);
	//$job_rel_assignees_insert_query->bindValue(":assignment_status",'1');
	$job_rel_assignees_insert_query->bindValue(":assignment_added_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$eventLog->log("assignment_added_by_sm_memb_id->".$ea_extracted_jwt_token_sub);
	$job_rel_assignees_insert_query->bindValue(":added_date_time",$event_datetime);
	$eventLog->log("added_date_time->".$event_datetime);
	$job_rel_assignees_insert_query->bindValue(":added_date_time_epoch",$current_epoch);
	$eventLog->log("added_date_time_epoch->".$current_epoch);
	
	if ($job_rel_assignees_insert_query->execute()) {
		
		$last_inserted_id = $dbcon->lastInsertId();			
		
		$constructed_array["last_inserted_id"] = $last_inserted_id;
		
		$eventLog->log("enter if");
		
		return $constructed_array;
					
	} else {
			
			$eventLog->log("enter if else");
			return $constructed_array;						
	}

}

function candidate_education_qualification_record_count($user_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	
	$candidate_education_qualification_record_count_sql = "SELECT COUNT(*) AS count FROM `candidate_rel_education_qualifications` WHERE `sm_memb_id`=:sm_memb_id";
	$candidate_education_qualification_record_count_select_query = $dbcon->prepare($candidate_education_qualification_record_count_sql);
	$candidate_education_qualification_record_count_select_query->bindValue(":sm_memb_id",$user_id_input);			
	$candidate_education_qualification_record_count_select_query->execute(); 
	
	if($candidate_education_qualification_record_count_select_query->rowCount() > 0) {
		$candidate_education_qualification_record_count_select_query_result = $candidate_education_qualification_record_count_select_query->fetch();
		$current_count = $candidate_education_qualification_record_count_select_query_result["count"];
		
		$constructed_array["current_count"] = $current_count;
		
	    return $constructed_array;
	
	} else {
		$constructed_array["current_count"] = 0;
		
		return $constructed_array;
		
	}//close of if($candidate_education_qualification_record_count_select_query->rowCount() > 0) {
	
	
}

function job_rel_assignees_records_list($job_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	
	$job_rel_assignees_records_list_sql = "SELECT * FROM `job_management_assignees` WHERE `job_id`=:job_id";
	$job_rel_assignees_records_list_select_query = $dbcon->prepare($job_rel_assignees_records_list_sql);
	$job_rel_assignees_records_list_select_query->bindValue(":job_id",$job_id_input);			
	$job_rel_assignees_records_list_select_query->execute(); 
	
	if($job_rel_assignees_records_list_select_query->rowCount() > 0) {
		$job_rel_assignees_records_list_select_query_result = $job_rel_assignees_records_list_select_query->fetchAll();
		return $job_rel_assignees_records_list_select_query_result;
	
	}
	return $constructed_array;
}

function delete_candidate_education_qualification_record($user_id_input, $candidate_degree_input, $candidate_field_of_study_input) {
	global $dbcon;
	
	$candidate_degree_input = '%'.$candidate_degree_input.'%';
	$candidate_field_of_study_input = '%'.$candidate_field_of_study_input.'%';
	
	$candidate_education_qualification_delete_sql = "DELETE FROM `candidate_education_qualifications` WHERE `sm_memb_id`=:sm_memb_id AND `degree` LIKE :degree AND `field_of_study` LIKE :field_of_study";
	$candidate_education_qualification_delete_query = $dbcon->prepare($candidate_education_qualification_delete_sql);
	$candidate_education_qualification_delete_query->bindValue(":sm_memb_id",$user_id_input);	
	$candidate_education_qualification_delete_query->bindValue(":degree",$candidate_degree_input);	
	$candidate_education_qualification_delete_query->bindValue(":field_of_study",$candidate_field_of_study_input);		
	$candidate_education_qualification_delete_query->execute(); 
	
	if ($candidate_education_qualification_delete_query->execute()) {
		return true;
		
	}//close of if ($candidate_education_qualification_delete_query->execute()) {
	
	return false;
	
}

function delete_candidate_education_qualification_record_based_on_ceq_id_input($ceq_id_input) {
	global $dbcon;
	
	$candidate_educational_qualification_record_delete_sql = "DELETE FROM `candidate_education_qualifications` WHERE `ceq_id`=:ceq_id";
	$candidate_educational_qualification_record_delete_query = $dbcon->prepare($candidate_educational_qualification_record_delete_sql);
	$candidate_educational_qualification_record_delete_query->bindValue(":ceq_id",$ceq_id_input);		
	if ($candidate_educational_qualification_record_delete_query->execute()) {
		return true;
		
	}//close of if ($candidate_professional_summary_record_delete_query->execute()) {
	
	return false;
	
}

function candidate_education_qualification_rel_file_upload_references_insert($user_id_input,$activity_ref_input,$activity_ref_id_input,$additional_document_ref_input,$original_filename_input,$generated_filename_input,$ea_extracted_jwt_token_user_type,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch){
    global $dbcon;
	
	$constructed_array = array();
	
	$candidate_education_qualification_rel_file_upload_references_insert_sql = "INSERT INTO `candidate_rel_all_uploaded_visa_documents`(`sm_memb_id`, `activity_ref`, `activity_ref_id`, `additional_document_ref`, `original_filename`, `generated_filename`, `added_by_user_type`, `added_by_user_id`, `added_date_time`, `added_date_time_epoch`) VALUES (:sm_memb_id,:activity_ref,:activity_ref_id,:additional_document_ref,:original_filename,:generated_filename,:added_by_user_type,:added_by_user_id,:added_date_time,:added_date_time_epoch)";
	$candidate_education_qualification_rel_file_upload_references_insert_q = $dbcon->prepare($candidate_education_qualification_rel_file_upload_references_insert_sql);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":sm_memb_id",$user_id_input);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":activity_ref",$activity_ref_input);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":activity_ref_id",$activity_ref_id_input);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":additional_document_ref",$additional_document_ref_input);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":original_filename",$original_filename_input);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":generated_filename",$generated_filename_input);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":added_by_user_type",$ea_extracted_jwt_token_user_type);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":added_by_user_id",$ea_extracted_jwt_token_sub);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":added_date_time",$event_datetime);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":added_date_time_epoch",$current_epoch);
	
	if ($candidate_education_qualification_rel_file_upload_references_insert_q->execute()) {
		$last_inserted_id = $dbcon->lastInsertId();			
		//$eventLog->log("record inserted successfully");
		
		$constructed_array["last_inserted_id"] = $last_inserted_id;
		
		return $constructed_array;
					
	} else {
		//$eventLog->log("Error occurred during process. Please try again");						
		return $constructed_array;						
	}//close of else of if ($candidate_education_qualification_rel_personal_info_insert_q->execute()) {
		
}

exit;
?>