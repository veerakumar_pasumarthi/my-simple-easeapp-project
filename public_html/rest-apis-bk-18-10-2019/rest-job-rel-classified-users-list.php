<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "4")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
				$user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {


				if ($user_id_input == "") {
				
					$response['data'] = array();
					$response['status'] = "missing-user-id";
					$response['status_description'] = "Missing User ID";
					
					$eventLog->log("missing-user-id: Please provide a valid User ID.");
					
				} else if ($job_id_input == "") {
					
					$response['data'] = array();
					$response['status'] = "missing-job-id-input";
					$response['status_description'] = "Missing Job Id";
					
					$eventLog->log("missing-job-id-input: Please provide valid Job Id.");
					
					
				} else if ($ip_address_input == "") {
				
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Missing IP-Address";
					
					$eventLog->log("missing-ip-address: Please provide valid ip_address.");
					
				} else {
					//all inputs are valid
					$eventLog->log("All inputs are valid.");
					
					$ea_action_specific_auth_check_result = "";
					
					//User Details, based on User ID input, through api request
					$user_basic_details_result = user_basic_details_check_based_on_user_id($user_id_input);
					
					if (count($user_basic_details_result) > 0) {
						
						/* //$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
						$ea_action_specific_auth_check_result = ea_check_action_specific_user_privileges_different_user_levels_reset_password_feature($ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_user_company_id,  $ea_extracted_jwt_token_sub, $user_id_input, $ea_extracted_jwt_token_user_privileges_list, $user_basic_details_result["company_id"], $page_filename);					
						 */
						//echo "ea_action_specific_auth_check_result: " . $ea_action_specific_auth_check_result;					
						
							
						
						$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
						
						if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
								
								$eventLog->log("try condition");	
							
							    $job_rel_classified_users_array = array();
								$assigned_job_specific_role = "primary-recruiters";
								$job_rel_classified_users_array["primary_recruiters"] = job_rel_classified_users_list($company_id_input,$job_id_input,$assigned_job_specific_role);
								$eventLog->log("try condition");	
								$assigned_job_specific_role = "recruiters";
								$job_rel_classified_users_array["recruiters"] = job_rel_classified_users_list($company_id_input,$job_id_input,$assigned_job_specific_role);
								$eventLog->log("try condition");	
								$assigned_job_specific_role = "primary-sales";
								$job_rel_classified_users_array["primary_sales"] = job_rel_classified_users_list($company_id_input,$job_id_input,$assigned_job_specific_role);
								$eventLog->log("try condition");	
								$assigned_job_specific_role = "sales";
								$job_rel_classified_users_array["sales"] = job_rel_classified_users_list($company_id_input,$job_id_input,$assigned_job_specific_role);
								$eventLog->log("try condition");	
			
								$response['data'] = $job_rel_classified_users_array;
								$response['status'] = "job-rel-classified-users-summary-result";
								$response['status_description'] = "Job rel classified users list successfully fetched";
								
								$eventLog->log("Job rel classified users list successfully fetched");	
								
							} catch (Exception $e) {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "job-rel-classified-users-summary-fetching-error";
								$response['status_description'] = "Error occurred when fetching job rel classified users Summary";
								
								$eventLog->log("Error occurred when fetching job rel classified users Summary.");	
								
							}
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-subject-platform-scope-user";
							$response['status_description'] = "Insufficient Set of Permissions, to complete this action";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-subject-platform-scope-user: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-different-company-user-id";
							$response['status_description'] = "Insufficient Set of Permissions, as the User belongs to a different company";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-INSUFFICIENT-PERMISSIONS") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions, as this process will not be allowed any further";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-self-action";
							$response['status_description'] = "Insufficient Set of Permissions, as self action is Dis-allowed";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-disallowed-self-action: Insufficient Set of Permissions, as self action is Dis-allowed, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-ACTION-SUBJECT-IS-A-DIFFERENT-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-action-on-different-user";
							$response['status_description'] = "Insufficient Set of Permissions, as action subject is a different user";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-disallowed-action-on-different-user: Insufficient Set of Permissions, as action subject is a different user, please check and try again.");	
							
						}//close of if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
						
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "Invalid User Account, that is attempted to be edited";
						
						$eventLog->log("invalid-user-account:Invalid User Account, that is attempted to be edited.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
					

				}//close of else of if ($user_type_input != 'admin') {
					
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted";
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input";
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){


function job_rel_classified_users_list($company_id_input,$job_id_input,$assigned_job_specific_role) {
	global $dbcon,$eventLog;
	
	$constructed_array = array();

	if($assigned_job_specific_role == "primary-recruiters") {
	   $assigned_job_specific_role_input = 3;
	} 
	if($assigned_job_specific_role == "recruiters") {
	   $assigned_job_specific_role_input = 4;
	} 
	if($assigned_job_specific_role == "primary-sales") {
	   $assigned_job_specific_role_input = 1;
	} 
	if($assigned_job_specific_role == "sales") {
	   $assigned_job_specific_role_input = 2;
	} 
	
    $job_rel_classified_users_list_sql = "SELECT * FROM job_management_assignees jma JOIN site_members sm ON jma.assigned_user_sm_memb_id = sm.sm_memb_id WHERE jma.job_id = :job_id AND jma.company_id =:company_id AND jma.assigned_job_specific_role = :assigned_job_specific_role";
	$job_rel_classified_users_list_select_query = $dbcon->prepare($job_rel_classified_users_list_sql);
	$job_rel_classified_users_list_select_query->bindValue(":company_id",$company_id_input);
	$job_rel_classified_users_list_select_query->bindValue(":assigned_job_specific_role",$assigned_job_specific_role_input);
	$job_rel_classified_users_list_select_query->bindValue(":job_id",$job_id_input);
	//$job_rel_classified_users_list_select_query->bindValue(":valid_to_date",$valid_to_date);
	$job_rel_classified_users_list_select_query->execute(); 
	
	if($job_rel_classified_users_list_select_query->rowCount() > 0) {
		$job_rel_classified_users_list_select_query_result = $job_rel_classified_users_list_select_query->fetchAll();
		
		foreach($job_rel_classified_users_list_select_query_result as $job_rel_classified_users_list_select_query_result_row) {
			
			$temp = array();		
			$temp["sm_memb_id"] = $job_rel_classified_users_list_select_query_result_row["sm_memb_id"];
			$temp["sm_salutation"] = $job_rel_classified_users_list_select_query_result_row["sm_salutation"];
			$temp["sm_firstname"] = $job_rel_classified_users_list_select_query_result_row["sm_firstname"];
			$temp["sm_middlename"] = $job_rel_classified_users_list_select_query_result_row["sm_middlename"];
			$temp["sm_lastname"] = $job_rel_classified_users_list_select_query_result_row["sm_lastname"];
			$temp["sm_email"] = $job_rel_classified_users_list_select_query_result_row["sm_email"];
			$temp["company_id"] = $job_rel_classified_users_list_select_query_result_row["company_id"];
			
			$constructed_array[] = $temp;
		}//close of foreach($candidate_education_qualification_records_list_select_query_result as $candidate_education_qualification_records_list_select_query_result_row) {
			
		
		return $constructed_array;
	
	}//close of if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
	return $constructed_array;
}

exit;
?>