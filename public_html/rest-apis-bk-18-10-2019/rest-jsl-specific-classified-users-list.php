<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "4")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
				$job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$jsl_owned_by_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_owned_by']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_owned_by'], FILTER_SANITIZE_NUMBER_INT) : '');
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {


				
				if ($job_id_input == "") {
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-id";
					$response['status_description'] = "missing job id .";
					
					$eventLog->log("Please provide Job Id.");
					
					
				} else if ($jsl_owned_by_input == "") {
			
					$response['data'] = array();
					$response['status'] = "missing-jsl-owned-by-id";
					$response['status_description'] = "missing JSL owned By Id.";
					
					$eventLog->log("Please provide all information.");
					
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else {
					try {
						    if($jsl_owned_by_input == "1") {
								
							    $job_rel_classified_users_array = array();
								$assigned_job_specific_role = "primary-recruiters";
								$job_rel_classified_users_array["primary_recruiters"] = job_rel_classified_users_list($company_id_input,$job_id_input,$assigned_job_specific_role);
								$eventLog->log("try condition");	
								$assigned_job_specific_role = "recruiters";
								$job_rel_classified_users_array["recruiters"] = job_rel_classified_users_list($company_id_input,$job_id_input,$assigned_job_specific_role);
								$eventLog->log("try condition");	
								$assigned_job_specific_role = "primary-sales";
								$job_rel_classified_users_array["primary_sales"] = job_rel_classified_users_list($company_id_input,$job_id_input,$assigned_job_specific_role);
								$eventLog->log("try condition");	
								$assigned_job_specific_role = "sales";
								$job_rel_classified_users_array["sales"] = job_rel_classified_users_list($company_id_input,$job_id_input,$assigned_job_specific_role);
								$eventLog->log("try condition");	
			
								$response['data'] = $job_rel_classified_users_array;
								$response['status'] = "job-rel-classified-users-summary-result";
								$response['status_description'] = "Job rel classified users list successfully fetched";
								
								$eventLog->log("Job rel classified users list successfully fetched");
								
							} else {
								
								$job_info_get_result = job_info_get($job_id_input);
								$response['data'] = $job_info_get_result;
								$response['status'] = "job-rel-classified-users-summary-result";
								$response['status_description'] = "Job rel classified users list successfully fetched";
								
								$eventLog->log("Job rel classified users list successfully fetched");

                            }							
						} catch (Exception $e) {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "job-rel-classified-users-summary-fetching-error";
								$response['status_description'] = "Error occurred when fetching job rel classified users Summary";
								
								$eventLog->log("Error occurred when fetching job rel classified users Summary.");	
								
						}
				}
					
			}
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
	
function job_rel_classified_users_list($company_id_input,$job_id_input,$assigned_job_specific_role) {
	global $dbcon,$eventLog;
	
	$constructed_array = array();

	if($assigned_job_specific_role == "primary-recruiters") {
	   $assigned_job_specific_role_input = 3;
	} 
	if($assigned_job_specific_role == "recruiters") {
	   $assigned_job_specific_role_input = 4;
	} 
	if($assigned_job_specific_role == "primary-sales") {
	   $assigned_job_specific_role_input = 1;
	} 
	if($assigned_job_specific_role == "sales") {
	   $assigned_job_specific_role_input = 2;
	} 
	
    $job_rel_classified_users_list_sql = "SELECT * FROM job_management_assignees jma JOIN site_members sm ON jma.assigned_user_sm_memb_id = sm.sm_memb_id WHERE jma.job_id = :job_id AND jma.company_id =:company_id AND jma.assigned_job_specific_role = :assigned_job_specific_role";
	$job_rel_classified_users_list_select_query = $dbcon->prepare($job_rel_classified_users_list_sql);
	$job_rel_classified_users_list_select_query->bindValue(":company_id",$company_id_input);
	$job_rel_classified_users_list_select_query->bindValue(":assigned_job_specific_role",$assigned_job_specific_role_input);
	$job_rel_classified_users_list_select_query->bindValue(":job_id",$job_id_input);
	//$job_rel_classified_users_list_select_query->bindValue(":valid_to_date",$valid_to_date);
	$job_rel_classified_users_list_select_query->execute(); 
	
	if($job_rel_classified_users_list_select_query->rowCount() > 0) {
		$job_rel_classified_users_list_select_query_result = $job_rel_classified_users_list_select_query->fetchAll();
		
		foreach($job_rel_classified_users_list_select_query_result as $job_rel_classified_users_list_select_query_result_row) {
			
			$temp = array();		
			$temp["sm_memb_id"] = $job_rel_classified_users_list_select_query_result_row["sm_memb_id"];
			$temp["sm_salutation"] = $job_rel_classified_users_list_select_query_result_row["sm_salutation"];
			$temp["sm_firstname"] = $job_rel_classified_users_list_select_query_result_row["sm_firstname"];
			$temp["sm_middlename"] = $job_rel_classified_users_list_select_query_result_row["sm_middlename"];
			$temp["sm_lastname"] = $job_rel_classified_users_list_select_query_result_row["sm_lastname"];
			$temp["sm_email"] = $job_rel_classified_users_list_select_query_result_row["sm_email"];
			$temp["sm_name"] = $temp["sm_firstname"] ." ".$temp["sm_middlename"] ." ".$temp["sm_lastname"];
			$temp["company_id"] = $job_rel_classified_users_list_select_query_result_row["company_id"];
			
			$constructed_array[] = $temp;
		}//close of foreach($candidate_education_qualification_records_list_select_query_result as $candidate_education_qualification_records_list_select_query_result_row) {
			
		
		return $constructed_array;
	
	}//close of if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
	return $constructed_array;
}

function job_info_get($job_id_input) {
	global $dbcon,$eventLog;
	
	$constructed_array = array();

    $job_info_get_sql = "SELECT cc.company_client_id,cc.client_company_name FROM jobs j JOIN company_clients cc ON j.company_client_id = cc.company_client_id WHERE j.job_id = :job_id";
	$job_info_get_query = $dbcon->prepare($job_info_get_sql);
	$job_info_get_query->bindValue(":job_id",$job_id_input);
	$job_info_get_query->execute(); 
	
	if($job_info_get_query->rowCount() > 0) {
		$job_info_get_query_result = $job_info_get_query->fetch();
		
		
	}//close of if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
	return $job_info_get_query_result;
}
exit;
?>