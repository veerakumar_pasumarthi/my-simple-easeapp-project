<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "2")) {

	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
			    $job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
			    
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {

                if ($job_id_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-id";
					$response['status_description'] = "Missing Job Id";
					
					$eventLog->log("missing-user-id: Missing Job Id.");
					
		/* 		} else if ($company_id_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company Id";
					
					$eventLog->log("missing-company-id: Missing Company Id."); */
					
				
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else {
					//all inputs are valid
					try {
						
					
						$job_rel_screening_levels_list_info_result = job_rel_screening_levels_list_info($job_id_input);
				
						$job_rel_screening_levels_list_info_result_count = count($job_rel_screening_levels_list_info_result);
						
						$eventLog->log("Count -> " . $job_rel_screening_levels_list_info_result_count);
						
						if (count($job_rel_screening_levels_list_info_result) > 0) {
						
						$response['data'] = $job_rel_screening_levels_list_info_result;
						$response['status'] = "job-related-screening-levels-list-successfully-fetched";
						$response['status_description'] = "Job related screening levels List Successfully Received";
					
						} else {
							
							$response['data'] = array();
							$response['status'] = "no-screening levels-defined-for-this-job";
							$response['status_description'] = "No screening levels defined found for this job";
							
						}
							
							
					} catch (Exception $e){
						
						$response['data'] = array();
						$response['status'] = "job-related-screening-levels-list-fetching-error";
						$response['status_description'] = "Job related screening levels List Fetching Error";
					}
					
					
				}
					
			}
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
	


function  job_rel_screening_levels_list_info($job_id_input) {
global $dbcon;
	$constructed_array = array();
	
	$job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_info` WHERE `job_id` =:job_id ";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":job_id",$job_id_input);		
	$job_rel_screening_levels_list_info_query->execute(); 
	
	if($job_rel_screening_levels_list_info_query->rowCount() > 0) {
		$job_rel_screening_levels_list_info_query_result = $job_rel_screening_levels_list_info_query->fetchAll();
		
		foreach ($job_rel_screening_levels_list_info_query_result as $job_rel_screening_levels_list_info_query_result_row) {

			$temp_row_array = array();
			
		    $temp_row_array["jsl_info_id"] = $job_rel_screening_levels_list_info_query_result_row["jsl_info_id"];
		    $temp_row_array["company_id"] = $job_rel_screening_levels_list_info_query_result_row["company_id"];
			$temp_row_array["company_client_id"] = $job_rel_screening_levels_list_info_query_result_row["company_client_id"];
		    $temp_row_array["job_id"] = $job_rel_screening_levels_list_info_query_result_row["job_id"];
		    $temp_row_array["jsl_title"] = $job_rel_screening_levels_list_info_query_result_row["jsl_title"];
			$temp_row_array["jsl_seo_title"] = $job_rel_screening_levels_list_info_query_result_row["jsl_seo_title"];
			$temp_row_array["jsl_classification_detail_id"] = $job_rel_screening_levels_list_info_query_result_row["jsl_classification_detail_id"];
			$temp_row_array["jsl_classification_detail_name"] = $job_rel_screening_levels_list_info_query_result_row["jsl_classification_detail_name"];
			$temp_row_array["jsl_classification_detail_seo_name"] = $job_rel_screening_levels_list_info_query_result_row["jsl_classification_detail_seo_name"];
			$temp_row_array["jsl_sub_classification_detail_id"] = $job_rel_screening_levels_list_info_query_result_row["jsl_sub_classification_detail_id"];
			$temp_row_array["jsl_sub_classification_detail_name"] = $job_rel_screening_levels_list_info_query_result_row["jsl_sub_classification_detail_name"];
			$temp_row_array["jsl_sub_classification_detail_seo_name"] = $job_rel_screening_levels_list_info_query_result_row["jsl_sub_classification_detail_seo_name"];
			$temp_row_array["jsl_owned_by"] = $job_rel_screening_levels_list_info_query_result_row["jsl_owned_by"];
			$temp_row_array["jsl_j_s_candidate_email_notification_setting"] = $job_rel_screening_levels_list_info_query_result_row["jsl_j_s_candidate_email_notification_setting"];
			$temp_row_array["jsl_j_s_candidate_sms_notification_setting"] = $job_rel_screening_levels_list_info_query_result_row["jsl_j_s_candidate_sms_notification_setting"];
			$temp_row_array["jsl_j_s_recruiter_email_notification_setting"] = $job_rel_screening_levels_list_info_query_result_row["jsl_j_s_recruiter_email_notification_setting"];
			$temp_row_array["jsl_j_s_recruiter_sms_notification_setting"] = $job_rel_screening_levels_list_info_query_result_row["jsl_j_s_recruiter_sms_notification_setting"];
			$temp_row_array["jsl_event_deadline_requirement_status"] = $job_rel_screening_levels_list_info_query_result_row["jsl_event_deadline_requirement_status"];
			$temp_row_array["jsl_event_deadline_datetime"] = $job_rel_screening_levels_list_info_query_result_row["jsl_event_deadline_datetime"];
			$temp_row_array["jsl_event_deadline_datetime_epoch"] = $job_rel_screening_levels_list_info_query_result_row["jsl_event_deadline_datetime_epoch"];
			$temp_row_array["jsl_sequence_order"] = $job_rel_screening_levels_list_info_query_result_row["jsl_sequence_order"];
			$temp_row_array["added_datetime"] = $job_rel_screening_levels_list_info_query_result_row["added_datetime"];
			$temp_row_array["added_datetime_epoch"] = $job_rel_screening_levels_list_info_query_result_row["added_datetime_epoch"];
			$temp_row_array["added_by_sm_memb_id"] = $job_rel_screening_levels_list_info_query_result_row["added_by_sm_memb_id"];
			$temp_row_array["last_updated_datetime"] = $job_rel_screening_levels_list_info_query_result_row["last_updated_datetime"];
			$temp_row_array["last_updated_datetime_epoch"] = $job_rel_screening_levels_list_info_query_result_row["last_updated_datetime_epoch"];
	
			$jsl_info_id = $temp_row_array["jsl_info_id"] ;
			
				
			$candiates_count_result = candidates_list_count_based_jsl_level($jsl_info_id);
			
			$temp_row_array["no_of_candidates"] = $candiates_count_result["count"];
		
			$temp_row_array["last_updated_by_sm_memb_id"] = $job_rel_screening_levels_list_info_query_result_row["last_updated_by_sm_memb_id"];
			$temp_row_array["is_active_status"] = $job_rel_screening_levels_list_info_query_result_row["is_active_status"];
			
			
			

		    $constructed_array[] = $temp_row_array;
	    }//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {
	     return $constructed_array;
	
	}
	return $constructed_array;
}

function candidates_list_count_based_jsl_level($jsl_info_id){
	
	global $dbcon;
	$constructed_array = array();
	
	$job_rel_screening_levels_list_info_sql = "SELECT COUNT(*) AS count FROM `jsl_applicant_evaluation_info` WHERE `jsl_info_id` =:jsl_info_id ";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":jsl_info_id",$jsl_info_id);		
	$job_rel_screening_levels_list_info_query->execute(); 
	
	if($job_rel_screening_levels_list_info_query->rowCount() > 0) {
		$job_rel_screening_levels_list_info_query_result = $job_rel_screening_levels_list_info_query->fetch();
	     return $job_rel_screening_levels_list_info_query_result;
	
	}
	return $constructed_array;
	
}
exit;
?>