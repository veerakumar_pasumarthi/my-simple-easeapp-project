<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Edit Quick Admin Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['job_applicant_invite_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_applicant_invite_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_applicant_invite_id'])) 
					
					
					if (isset($ea_received_rest_ws_raw_array_input['new_status'])) {
						$content .= $ea_received_rest_ws_raw_array_input['new_status'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['new_status'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$job_applicant_invite_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_applicant_invite_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_applicant_invite_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				
				$new_status_input = trim(isset($ea_received_rest_ws_raw_array_input['new_status']) ? filter_var($ea_received_rest_ws_raw_array_input['new_status'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				
				if ($job_applicant_invite_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-applicant-invite-id";
					$response['status_description'] = "Missing job applicant invite id,please check and try agin.";
					
					$eventLog->log("missing-job-applicant-invite-id: Please provide a valid job applicant invite ID.");
					
				}  else if (($new_status_input != "0") && ($new_status_input != "1")) {
					//Invalid New Status scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-new-status-submitted";
					$response['status_description'] = "Invalid New Status Info";
					
					$eventLog->log("invalid-new-status-submitted: Invalid New Status Info is provided, Please check and try again.");
					
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Missing Some Additional Information";
					
					$eventLog->log("missing-some-additional-information: Some Additional Information is missing, please check and try again.");	
				
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//Event Time, as per Indian Standard Time
					$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
		             $candidate_invited_jobs_record_check_result = candidate_invite_jobs_record_check($job_applicant_invite_id_input);
					 $company_id_input = $candidate_invited_jobs_record_check_result["company_id"];
					 
					$candidate_invite_jobs_status_disable_next_step = "";
					
					if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$candidate_invite_jobs_status_disable_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$candidate_invite_jobs_status_disable_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
					} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
					}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
							
						if ($candidate_invite_jobs_status_disable_next_step == "PROCEED-TO-NEXT-STEP") {
						
                       //if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
								$eventLog->log("before enter into query.");
								
								      $candidate_invited_jobs_record_check_result = candidate_invite_jobs_record_check($job_applicant_invite_id_input);
									 
												if (count($candidate_invited_jobs_record_check_result) > 0) {
													
													$candidate_invite_jobs_change_status_result = candidate_invite_jobs_change_status_based_on_job_applicant_invite_id($job_applicant_invite_id_input,$new_status_input);
													
													if($candidate_invited_jobs_record_check_result == true){
													   
													   $response['data'] = array();
													   $response['status'] = "candidate-invite-jobs-status-changed-successfully";
													   $response['status_description'] = "Candidate invite jobs status changed Successfully.";
													
													    $eventLog->log("The candidate invite jobs status changed Successfully.");
													     
												    } else {
													
													$response['data'] = array();
													$response['status'] = "candidate-invite-jobs-status-change-error";
													$response['status_description'] = "There is an error, when changing  the candidate invite jobs status in the Database.";
													
													$eventLog->log("There is an error, when changing  the candidate invite jobs status in the Database.");
													
												}	
										    
									        } else {
												
										
												  $response['data'] = array();
												  $response['status'] = "no-candidate-invite-jobs-with-this-job-applicant-invite-id";
												  $response['status_description'] = "No candidate invite jobs found with job applicant invite Id";
													
												  $eventLog->log("No Company Office Address found with given Company Office Address Id");
												  
												}
									
										
										
								} catch (Exception $e) {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = " candidate-invite-jobs-status-change-error";
									$response['status_description'] = "Error occurred when changing  the Candidate invited jobs status ";
									
									$eventLog->log("Error occurred when changing  the Candidate invited jobs status .");	
									
								}
					   
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "The User Account, that is attempted to be edited, is invalid, please check and try again.";
						
						$eventLog->log("Please provide a Valid user ID.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
					
						
				}//close of else of if ($email_id_input == "") {
				
			}
			
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
    }//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function  candidate_invite_jobs_record_check($job_applicant_invite_id_input) {
	global $dbcon;
	$constructed_array = array();
	$candidate_invite_jobs_get_sql = "SELECT * FROM `job_applicant_invites` WHERE `job_applicant_invite_id`=:job_applicant_invite_id";
	$candidate_invite_jobs_get_select_query = $dbcon->prepare($candidate_invite_jobs_get_sql);
	$candidate_invite_jobs_get_select_query->bindValue(":job_applicant_invite_id",$job_applicant_invite_id_input);	
	$candidate_invite_jobs_get_select_query->execute(); 
	
	if($candidate_invite_jobs_get_select_query->rowCount() > 0) {
		$candidate_invite_jobs_get_select_query_result = $candidate_invite_jobs_get_select_query->fetch();
	     return $candidate_invite_jobs_get_select_query_result;
	
	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
	
}

function candidate_invite_jobs_change_status_based_on_job_applicant_invite_id($job_applicant_invite_id_input,$new_status_input) {
	global $dbcon, $eventLog;
	
	$eventLog->log("in function before query");
    
	$candidate_invite_jobs_change_status_update_sql = " UPDATE `job_applicant_invites` SET `is_active_status`=:is_active_status WHERE `job_applicant_invite_id`=:job_applicant_invite_id";
	$candidate_invite_jobs_change_status_update_query = $dbcon->prepare($candidate_invite_jobs_change_status_update_sql);
	$eventLog->log("in function before bind");
	
	$candidate_invite_jobs_change_status_update_query->bindValue(":job_applicant_invite_id",$job_applicant_invite_id_input);
	$candidate_invite_jobs_change_status_update_query->bindValue(":is_active_status",$new_status_input);
	
	$eventLog->log("in function after bind");

	if ($candidate_invite_jobs_change_status_update_query->execute()) {
		
		$eventLog->log("in function enter into if");

		return true;

	}
	return false;
}	

exit;
?>


