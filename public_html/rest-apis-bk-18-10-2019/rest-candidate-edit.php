<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Edit Quick Candidate Info, in the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "25")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['user_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['user_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['user_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['salutation'])) {
						$content .= $ea_received_rest_ws_raw_array_input['salutation'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['salutation'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['firstname'])) {
						$content .= $ea_received_rest_ws_raw_array_input['firstname'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['firstname'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['middlename'])) {
						$content .= $ea_received_rest_ws_raw_array_input['middlename'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['middlename'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['lastname'])) {
						$content .= $ea_received_rest_ws_raw_array_input['lastname'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['lastname'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['mobile_number'])) {
						$content .= $ea_received_rest_ws_raw_array_input['mobile_number'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['mobile_number'])) 
						

					if (isset($ea_received_rest_ws_raw_array_input['mobile_phone_country_code'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['mobile_phone_country_code'] . "\r\n"; 
					}//close of if (isset($ea_received_rest_ws_raw_array_input['mobile_phone_country_code'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['work_phone_number'])) {
						$content .= $ea_received_rest_ws_raw_array_input['work_phone_number'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['work_phone_number']))
					
					if (isset($ea_received_rest_ws_raw_array_input['work_phone_country_code'])) {
						$content .= $ea_received_rest_ws_raw_array_input['work_phone_country_code'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['work_phone_country_code']))
					
                    if (isset($ea_received_rest_ws_raw_array_input['home_phone_number'])) {
						$content .= $ea_received_rest_ws_raw_array_input['home_phone_number'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['home_phone_number']))
						
					if (isset($ea_received_rest_ws_raw_array_input['home_phone_country_code'])) {
						$content .= $ea_received_rest_ws_raw_array_input['home_phone_country_code'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['home_phone_country_code']))	
						
					if (isset($ea_received_rest_ws_raw_array_input['fax_number'])) {
						$content .= $ea_received_rest_ws_raw_array_input['fax_number'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['fax_number']))
						
				    if (isset($ea_received_rest_ws_raw_array_input['fax_number_country_code'])) {
						$content .= $ea_received_rest_ws_raw_array_input['fax_number_country_code'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['fax_number_country_code']))
						
					if (isset($ea_received_rest_ws_raw_array_input['email_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['email_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['email_id']))
						
					if (isset($ea_received_rest_ws_raw_array_input['alternate_email_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['alternate_email_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['alternate_email_id']))	
						
					if (isset($ea_received_rest_ws_raw_array_input['address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['address']))
						
					if (isset($ea_received_rest_ws_raw_array_input['city'])) {
						$content .= $ea_received_rest_ws_raw_array_input['city'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['city']))	
						
					if (isset($ea_received_rest_ws_raw_array_input['state'])) {
						$content .= $ea_received_rest_ws_raw_array_input['state'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['state']))
					
					if (isset($ea_received_rest_ws_raw_array_input['country'])) {
						$content .= $ea_received_rest_ws_raw_array_input['country'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['country']))
					
				    if (isset($ea_received_rest_ws_raw_array_input['zipcode'])) {
						$content .= $ea_received_rest_ws_raw_array_input['zipcode'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['zipcode']))
						
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id']))
						
					if (isset($ea_received_rest_ws_raw_array_input['user_type'])) {
						$content .= $ea_received_rest_ws_raw_array_input['user_type'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['user_type']))
						
					if (isset($ea_received_rest_ws_raw_array_input['notes'])) {
						$content .= $ea_received_rest_ws_raw_array_input['notes'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['notes']))
						
					if (isset($ea_received_rest_ws_raw_array_input['profile_source_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['profile_source_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['profile_source_id']))	
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$salutation_input = trim(isset($ea_received_rest_ws_raw_array_input['salutation']) ? filter_var($ea_received_rest_ws_raw_array_input['salutation'], FILTER_SANITIZE_STRING) : '');
				
				$firstname_input = trim(isset($ea_received_rest_ws_raw_array_input['firstname']) ? filter_var($ea_received_rest_ws_raw_array_input['firstname'], FILTER_SANITIZE_STRING) : '');
				
				$middlename_input = trim(isset($ea_received_rest_ws_raw_array_input['middlename']) ? filter_var($ea_received_rest_ws_raw_array_input['middlename'], FILTER_SANITIZE_STRING) : '');
				
				$lastname_input = trim(isset($ea_received_rest_ws_raw_array_input['lastname']) ? filter_var($ea_received_rest_ws_raw_array_input['lastname'], FILTER_SANITIZE_STRING) : '');
				
				$mobile_number_input = trim(isset($ea_received_rest_ws_raw_array_input['mobile_number']) ? filter_var($ea_received_rest_ws_raw_array_input['mobile_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$mobile_phone_country_code_input = trim(isset($ea_received_rest_ws_raw_array_input['mobile_phone_country_code']) ? filter_var($ea_received_rest_ws_raw_array_input['mobile_phone_country_code'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$work_phone_number_input = trim(isset($ea_received_rest_ws_raw_array_input['work_phone_number']) ? filter_var($ea_received_rest_ws_raw_array_input['work_phone_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$work_phone_country_code_input = trim(isset($ea_received_rest_ws_raw_array_input['work_phone_country_code']) ? filter_var($ea_received_rest_ws_raw_array_input['work_phone_country_code'], FILTER_SANITIZE_NUMBER_INT) : '');
				$home_phone_number_input = trim(isset($ea_received_rest_ws_raw_array_input['home_phone_number']) ? filter_var($ea_received_rest_ws_raw_array_input['home_phone_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$home_phone_country_code_input = trim(isset($ea_received_rest_ws_raw_array_input['home_phone_country_code']) ? filter_var($ea_received_rest_ws_raw_array_input['home_phone_country_code'], FILTER_SANITIZE_NUMBER_INT) : '');
				$fax_number_input = trim(isset($ea_received_rest_ws_raw_array_input['fax_number']) ? filter_var($ea_received_rest_ws_raw_array_input['fax_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$fax_number_country_code_input = trim(isset($ea_received_rest_ws_raw_array_input['fax_number_country_code']) ? filter_var($ea_received_rest_ws_raw_array_input['fax_number_country_code'], FILTER_SANITIZE_NUMBER_INT) : '');
				$address_input = trim(isset($ea_received_rest_ws_raw_array_input['address']) ? filter_var($ea_received_rest_ws_raw_array_input['address'], FILTER_SANITIZE_STRING) : '');
				
				
				$email_id_input = trim(isset($ea_received_rest_ws_raw_array_input['email_id']) ? filter_var($ea_received_rest_ws_raw_array_input['email_id'], FILTER_SANITIZE_EMAIL) : '');	
				
				$alternate_email_id_input = trim(isset($ea_received_rest_ws_raw_array_input['alternate_email_id']) ? filter_var($ea_received_rest_ws_raw_array_input['alternate_email_id'], FILTER_SANITIZE_EMAIL) : '');
				
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');	
				
				$user_type_input = trim(isset($ea_received_rest_ws_raw_array_input['user_type']) ? filter_var($ea_received_rest_ws_raw_array_input['user_type'], FILTER_SANITIZE_STRING) : '');
				
                $city_input = trim(isset($ea_received_rest_ws_raw_array_input['city']) ? filter_var($ea_received_rest_ws_raw_array_input['city'], FILTER_SANITIZE_STRING) : '');
				
				$state_input = trim(isset($ea_received_rest_ws_raw_array_input['state']) ? filter_var($ea_received_rest_ws_raw_array_input['state'], FILTER_SANITIZE_STRING) : '');
				
                $country_input = trim(isset($ea_received_rest_ws_raw_array_input['country']) ? filter_var($ea_received_rest_ws_raw_array_input['country'], FILTER_SANITIZE_STRING) : '');
				
				$zipcode_input = trim(isset($ea_received_rest_ws_raw_array_input['zipcode']) ? filter_var($ea_received_rest_ws_raw_array_input['zipcode'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				if($zipcode_input == "") {
					
					$zipcode_input = null;
				}
				
				
				$profile_source_id_input = trim(isset($ea_received_rest_ws_raw_array_input['profile_source_id']) ? filter_var($ea_received_rest_ws_raw_array_input['profile_source_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$notes_input = trim(isset($ea_received_rest_ws_raw_array_input['notes']) ? filter_var($ea_received_rest_ws_raw_array_input['notes'], FILTER_SANITIZE_STRING) : '');			
				if($salutation_input == "")
				{  
			        $salutation_input = null;
				}
				if (($mobile_number_input == '0') || (!ctype_digit($mobile_number_input))) {
						$eventLog->log($mobile_number_input . " - Not a Valid Mobile Number");
						$mobile_number_input = "";
				} else if (!filter_var($alternate_email_id_input, FILTER_VALIDATE_EMAIL) == true) {
					$eventLog->log($alternate_email_id_input . " - Not a Valid Alternate Email Address");
					$alternate_email_id_input = "";
				}//close of else if of if (($mobile_number_input == '0') || (!ctype_digit($mobile_number_input))) {
						
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//$eventLog->log("company_id b: " . $company_id_input);
				//Check if all inputs are received correctly from the REST Web Service
				if ($user_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-user-id";
					$response['status_description'] = "Missing User ID, please check and try again.";
					
					$eventLog->log("Please provide a valid User ID.");
					
				} else if ($mobile_number_input == "") {
					//Invalid Mobile Number scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-mobile-number";
					$response['status_description'] = "Invalid mobile number, please check and try again.";
					
					$eventLog->log("Please provide a valid mobile number.");
					
				} else if ($profile_source_id_input == "") {
					//Invalid Mobile Number scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-profile-source-d";
					$response['status_description'] = "Invalid profile source id number, please check and try again.";
					
					$eventLog->log("Please provide a valid profile source id.");
				} else if ($firstname_input == "") {
					//Invalid First name scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-firstname";
					$response['status_description'] = "Invalid firstname, please check and try again.";
					
					$eventLog->log("Please provide a valid firstname.");
				
				} else if ($lastname_input == "") {
					//Invalid Last name scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-lastname";
					$response['status_description'] = "Invalid lastname, please check and try again.";
					
					$eventLog->log("Please provide a valid lastname.");	
					
				} else if (($alternate_email_id_input == "") || ($ip_address_input == "")) {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Some Additional Information is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");
					
                }  else if ($company_id_input == "") {
					//missing Company ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "missing company id";
					
					$eventLog->log("missing-company-id: Please provide a valid company id.");
				
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//Event Time, as per Indian Standard Time
					$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
					
					$ea_action_specific_auth_check_result = "";
					
					//User Details, based on User ID input, through api request
					$user_basic_details_result = user_basic_details_check_based_on_user_id($user_id_input);
					$eventLog->log("before count.");
					if (count($user_basic_details_result) > 0) {
						
						//$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
					
						$ea_action_specific_auth_check_result = ea_check_action_specific_user_privileges_on_different_user_levels($ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_user_company_id,  $ea_extracted_jwt_token_sub, $user_id_input, $ea_extracted_jwt_token_user_privileges_list, $user_basic_details_result["company_id"], $page_filename);					
						
						//echo "ea_action_specific_auth_check_result: " . $ea_action_specific_auth_check_result;					
							
							$eventLog->log("before condition.");
							
						if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
								$eventLog->log("after condition.");
								$user_update_sql = "UPDATE `site_members` sm LEFT JOIN candidate_rel_internal_notes crin ON crin.candidate_id = sm.sm_memb_id SET sm.sm_email =:sm_email,sm.sm_mobile_phone_country_code =:sm_mobile_phone_country_code,sm.sm_mobile =:sm_mobile, sm.sm_alternate_email =:sm_alternate_email, sm.sm_salutation =:sm_salutation, sm.sm_firstname =:sm_firstname, sm.sm_middlename =:sm_middlename, sm.sm_lastname =:sm_lastname,sm.sm_work_phone_country_code =:sm_work_phone_country_code,sm.sm_work_phone =:sm_work_phone, sm.sm_home_phone_country_code =:sm_home_phone_country_code,sm.sm_home_phone =:sm_home_phone,sm.sm_fax_number_country_code =:sm_fax_number_country_code,sm.sm_fax_number =:sm_fax_number,sm.sm_address =:sm_address,sm.sm_city=:sm_city, sm.sm_state=:sm_state, sm.sm_country =:sm_country,sm.sm_zipcode =:sm_zipcode,sm.company_id=:company_id,sm.sm_user_type =:sm_user_type, crin.notes =:notes,sm.last_updated_date_time =:last_updated_date_time, sm.last_updated_date_time_epoch =:last_updated_date_time_epoch WHERE sm.sm_memb_id =:sm_memb_id";
								
								$eventLog->log("After update.");
								$user_update_values_array = array(":sm_memb_id" => $user_id_input,":sm_email" =>$email_id_input,"sm_mobile_phone_country_code" => $mobile_phone_country_code_input,":sm_mobile" => $mobile_number_input,":sm_alternate_email" => $alternate_email_id_input  ,":sm_salutation" => $salutation_input,":sm_firstname" => $firstname_input,":sm_middlename" => $middlename_input,":sm_lastname" => $lastname_input,":sm_work_phone_country_code" => $work_phone_country_code_input,":sm_work_phone" =>$work_phone_number_input,":sm_home_phone_country_code" => $home_phone_country_code_input,":sm_home_phone" => $home_phone_number_input,":sm_fax_number_country_code" => $fax_number_country_code_input,":sm_fax_number" => $fax_number_input,":sm_address" => $address_input,":sm_city" => $city_input,":sm_state" => $state_input,":sm_country" => $country_input,":sm_zipcode" => $zipcode_input,":company_id" => $company_id_input,"sm_user_type" => $user_type_input,":notes" => $notes_input,":last_updated_date_time" => $event_datetime, ":last_updated_date_time_epoch" => $current_epoch);
									$eventLog->log("after update values array.");
								$user_update_status = update_query_based_on_id($user_update_sql, $user_update_values_array);
									$eventLog->log("after update status.");
									$profile_source_details_get_result = profile_source_details_based_on_profile_source_id($profile_source_id_input);
									$profile_source_name_input = $profile_source_details_get_result["profile_source_name"];
									$profile_source_seo_name_input = $profile_source_details_get_result["profile_source_seo_name"];
												
								$profile_source_update_result = profile_source_update_based_on_sm_memb_id($user_id_input,$profile_source_id_input,$profile_source_name_input,$profile_source_seo_name_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);	
									
									//echo "user_update_status: " . var_dump($user_update_status);
									if (($user_update_status == true)&& ($profile_source_update_result == true)) {
										$eventLog->log("before status.");
										ea_update_user_rel_active_jwt_token_status_based_on_user_id($user_id_input);
										
										//Construct Content, that will be sent in Response body, of the REST Web Service
										$response['data'] = array();
										$response['status'] = "candidate-details-updated-successfully";
										$response['status_description'] = "Candidate Details Updated Successfully";
										
									} else {
										
										//Construct Content, that will be sent in Response body, of the REST Web Service
										$response['data'] = array();
										$response['status'] = "error-updating-candidate-details";
										$response['status_description'] = "There occurred an error when updating the Candidate's Details, Please check and try again later.";
										
									}//close of else of if ($user_update_status == true) {
									
								} catch (Exception $e) {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "error-updating-user-details";
									$response['status_description'] = "There occurred an error when updating the Candidate's Details, Please check and try again later.";
									
								}
							//close of if ($candidate_edit_next_step == "PROCEED-WITH-CANDIDATE-EDIT-ACTIVITY") {
								
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-subject-platform-scope-user";
							$response['status_description'] = "Insufficient Set of Permissions, to complete this action, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-different-company-user-id";
							$response['status_description'] = "Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-INSUFFICIENT-PERMISSIONS") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-self-action";
							$response['status_description'] = "Insufficient Set of Permissions, as self action is Dis-allowed, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						}//close of if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
						
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "The User Account, that is attempted to be edited, is invalid, please check and try again.";
						
						$eventLog->log("Please provide a Valid User ID.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
						
					
				}//close of else of if ($user_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
							
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "8")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
function profile_source_update_based_on_sm_memb_id($sm_memb_id_input,$profile_source_id_input,$profile_source_name_input,$profile_source_seo_name_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch){
	global $dbcon,$eventLog;
	$constructed_array = array();
	
	

	$user_profile_source_update_sql = "UPDATE `candidate_rel_additional_info` SET `profile_source_id`=:profile_source_id,`profile_source_name`=:profile_source_name,`profile_source_seo_name`=:profile_source_seo_name,`last_updated_by_user_id`=:last_updated_by_user_id,`last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch WHERE sm_memb_id=:sm_memb_id"; 
	
	$user_profile_source_update_q = $dbcon->prepare($user_profile_source_update_sql);
	$user_profile_source_update_q->bindValue(":sm_memb_id",$sm_memb_id_input);
	$user_profile_source_update_q->bindValue(":profile_source_id",$profile_source_id_input);
	$user_profile_source_update_q->bindValue(":profile_source_name",$profile_source_name_input);
	$user_profile_source_update_q->bindValue(":profile_source_seo_name",$profile_source_seo_name_input);
	$user_profile_source_update_q->bindValue(":last_updated_by_user_id",$ea_extracted_jwt_token_sub);
	$user_profile_source_update_q->bindValue(":last_updated_date_time",$event_datetime);
	$user_profile_source_update_q->bindValue(":last_updated_date_time_epoch",$current_epoch);
    
		if ($user_profile_source_update_q->execute()) {


			$eventLog->log("ass record updated successfully");
			return true;

		} else {
		    $eventLog->log("Error occurred during process. Please try again");
				return false;
	    }
}	


exit;
?>