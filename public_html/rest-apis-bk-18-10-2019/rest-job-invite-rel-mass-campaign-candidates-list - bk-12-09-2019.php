<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * REST API for candidate all education qualifications collect
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "5")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
						
                    if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_client_id'] . "\r\n";
				    }//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id']))
					
					if (isset($ea_received_rest_ws_raw_array_input['job_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_id'] . "\r\n";
				    }//close of if (isset($ea_received_rest_ws_raw_array_input['job_id']))
					
					if (isset($ea_received_rest_ws_raw_array_input['job_invite_rel_mass_campaign_candidates_list'])) {
						$content .= json_encode($ea_received_rest_ws_raw_array_input['job_invite_rel_mass_campaign_candidates_list']) . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_invite_rel_mass_campaign_candidates_list'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					//$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$job_invite_rel_mass_campaign_candidates_list_input = $ea_received_rest_ws_raw_array_input['job_invite_rel_mass_campaign_candidates_list'];
				
				$company_client_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_client_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_client_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//Check if all inputs are received correctly from the REST Web Service
				if ($company_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company ID";
					
					$eventLog->log("missing-company-id: Please provide a valid Company ID.");
					
				} else if ($company_client_id_input == "") {
					//Invalid company Status scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-company-client-id";
					$response['status_description'] = "Invalid company client id submitted, please check and try again.";
					
					$eventLog->log("Please provide a valid company client id .");	
					
				} else if ($job_id_input == "") {
					//Invalid company Status scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-job-id";
					$response['status_description'] = "Invalid job id submitted, please check and try again.";
					
					$eventLog->log("Please provide a valid job id .");
					
				} else if ($job_invite_rel_mass_campaign_candidates_list_input == "") {	
					//Invalid Education Qualifications Received scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "empty-job-invite-rel-mass-campaign-candidates-list-input";
					$response['status_description'] = "Empty Job invite rel mass campaign candidate list Input";
					
					$eventLog->log("Empty Job invite rel mass campaign candidate list Input, Please check and try again.");
					
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Missing Some Additional Information";
					
					$eventLog->log("missing-some-additional-information: Some Additional Information is missing, please check and try again.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					$ea_action_specific_auth_check_result = "";
					if ($ea_extracted_jwt_token_user_type == "admin") {
							
						if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
							$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
							
						} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
							//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
							
							$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
							
						} else {
							//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-different-company-user-id";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
		
							$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
							
						}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							$eventLog->log("after candidate add.");
							
					} else {
							
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "insufficient-permissions";
						$response['status_description'] = "Insufficient Set of Permissions";
						
						//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
						header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
		
						$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
					
					}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
							
					if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
						try {
							
							//Event Time, as per Indian Standard Time
							
							
					
							$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
					
							if ((is_array($job_invite_rel_mass_campaign_candidates_list_input)) && (count($job_invite_rel_mass_campaign_candidates_list_input) > 0)) {
									
										
									    
						        $sm_memb_id_array = array();
								$sm_email_array = array();
								/* foreach ($multiple_email_requests_input as $multiple_email_requests_input_row) {
									$sm_memb_id_list[] = $multiple_email_requests_input_row["sm_memb_id"];
									$sm_email_list[] = $multiple_email_requests_input_row["sm_email"];
								}
								 */
								
								$count = count($job_invite_rel_mass_campaign_candidates_list_input);
								$row_count = 0;
								foreach ($job_invite_rel_mass_campaign_candidates_list_input as $job_invite_rel_mass_campaign_candidates_list_input_row) {
									$sm_memb_id = $job_invite_rel_mass_campaign_candidates_list_input_row["sm_memb_id"];
									//$sm_email_id = $multiple_email_requests_input_row["sm_email"];
									
									
									$job_applicant_invite_duplicate_check_result = job_applicant_invite_duplicate_check_based_on_job_id($job_id_input,$company_id_input,$company_client_id_input,$sm_memb_id);
					
									$eventLog->log("job-applicant-invite-duplicate-check-result: job applicant invite duplicate check result");
									if (count($job_applicant_invite_duplicate_check_result) > 0) {
										
										//Construct Content, that will be sent in Response body, of the REST Web Service
										$response['data'] = array();
										$response['status'] = "job-applicant-invite-already-exists";
										$response['status_description'] = "A job applicant invite already exists with this Name";
										
										$eventLog->log("job-applicant-invite-already-exists: job applicant invite already exists");						
									
									} else {
										$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
										$event_expiry_datetime_epoch = $current_epoch+$job_invite_expiry_time_span;
										$event_expiry_date_time = df_convert_unix_timestamp_to_datetime_custom_timezone($event_expiry_datetime_epoch, "Europe/London");
										 
										$job_invite_ref_code = bin2hex(random_bytes($job_invite_ref_code_length));
								
										$eventLog->log("job-invite-ref-code->".$job_invite_ref_code);
										
										$job_invite_ref_code_length = mb_strlen($job_invite_ref_code);
										
										$eventLog->log("job-invite-ref-code-length->".$job_invite_ref_code_length);
										
										$last_inserted_id = job_applicant_invite_insert($company_id_input,$company_client_id_input,$job_id_input,$ea_extracted_jwt_token_sub,$sm_memb_id,$job_invite_ref_code,$event_datetime,$current_epoch,$event_expiry_date_time,$event_expiry_datetime_epoch);
							 
									
										$eventLog->log($last_inserted_id);
										$row_count++;
										$eventLog->log($row_count);
										
									}
								}
									
								if ($count == $row_count) {
									
										$response['data'] = array();
										$response['status'] = "job-applicant-invite-request-save-successful";
										$response['status_description'] = "A job applicant invite request  save is Successful.";
									
										$eventLog->log("job-applicant-invite-save-successful: A job applicant invite request save is Successful.");   
									
								} else {
									//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
									$response['data'] = array();
									$response['status'] = "job-application-invite-request-save-error";
									$response['status_description'] = "Error occurred when  adding the job applicant invite request in the Database.";
									
									$eventLog->log("job-applicant-invite-insert-error: There is an error, when saving the job applicant invite request in the Database.");
									
								}//close of else of if (count($user_ass_oc_validation_result) > 0) {
							
								/* }
								$sm_memb_id_array = json_encode($sm_memb_id_list);
								$sm_email_array = json_encode($sm_email_list);
								
								$candidates_job_specific_email_request_insert_result = candidates_job_specific_email_request_insert($sm_memb_id_array,$sm_email_array,$job_id,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);

								if (isset($candidate_rel_all_document_download_request_insert_result["last_inserted_id"])) {
									
									$candidate_rel_all_document_download_request_last_inserted_id = $candidate_rel_all_document_download_request_insert_result["last_inserted_id"];
				
									if(count($downloaded_file_url_result)>0) {
										
										$response['data'] = $downloaded_file_url_result;
										$response['status'] = "zip-file-download-link-created-successfully";
										$response['status_description'] = "Zip File Download link successfully created";
								    } else {
										$response['data'] = array();
										$response['status'] = "zip-file-download-link-created-successfully";
										$response['status_description'] = "Zip File Download link successfully created";
										
									}
								} */
							}	
						
					    } catch (Exception $e) {
						//$eventLog->log("Empty user id input");
									
					    }
				    }
					
				}	
			
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
										
												
exit;
?>