
<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "4")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['company_specific_internal_jsl_classifications'])) {
						$content .= json_encode($ea_received_rest_ws_raw_array_input['company_specific_internal_jsl_classifications']) . "\r\n";
					}
					
					if (isset($ea_received_rest_ws_raw_array_input['company_specific_external_jsl_classifications'])) {
						$content .= json_encode($ea_received_rest_ws_raw_array_input['company_specific_external_jsl_classifications']) . "\r\n";
					}
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
				}
					
					$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					$company_specific_internal_jsl_classifications_input = $ea_received_rest_ws_raw_array_input['company_specific_internal_jsl_classifications'];
					$company_specific_external_jsl_classifications_input = $ea_received_rest_ws_raw_array_input['company_specific_external_jsl_classifications'];
					
					if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
					} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
					}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						
					
					
					
					if ($company_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company ID";
					
					$eventLog->log("missing-company-id: Please provide a valid Company ID.");
					
					/* } else if ($company_specific_internal_jsl_classifications_input == "") {	
					//Invalid Education Qualifications Received scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "empty-job-rel-assignees-input";
					$response['status_description'] = "Empty Job rel assignees Input";
					
					$eventLog->log("empty-job-rel-assignees-input: Empty Job rel assignees Input, Please check and try again.");
					 */
					/* } else if ($company_specific_external_jsl_classifications_input == "") {	
					//Invalid Education Qualifications Received scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "empty-job-rel-assignees-input";
					$response['status_description'] = "Empty Job rel assignees Input";
					
					$eventLog->log("empty-job-rel-assignees-input: Empty Job rel assignees Input, Please check and try again.");
					 */
					
					} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Missing Some Additional Information";
					
					$eventLog->log("missing-some-additional-information: Some Additional Information is missing, please check and try again.");	
				
					
					
					
				    } else {	
						
							$job_rel_classification_details_add_next_step = "";
							if ($ea_extracted_jwt_token_user_type == "admin") {
								
								if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
									$job_rel_classification_details_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("before company id condition");
								/* } else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
									 $eventLog->log("job related condition");
									$job_rel_classification_details_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("job related condition"); */
								} else {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "insufficient-permissions-different-company-user-id";
									$response['status_description'] = "Insufficient Set of Permissions";
									
									//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
									header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
									$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
									
								}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								
								
								
							} else {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
								$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
							
							}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
								
							$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
							 $eventLog->log("before proceed to next next step condition");
							 
							 
							if ($job_rel_classification_details_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
								try {
							
							
									if ((is_array($company_specific_internal_jsl_classifications_input)) && (count($company_specific_internal_jsl_classifications_input) > 0)) {
											
													
										$count = count($company_specific_internal_jsl_classifications_input) + count($company_specific_external_jsl_classifications_input);
										$row_count = 0;
										foreach ($company_specific_internal_jsl_classifications_input as $company_specific_internal_jsl_classifications_input_row) {
											$jsl_classification_detail_id = $company_specific_internal_jsl_classifications_input_row["jsl_classification_detail_id"];
											
											$eventLog->log($jsl_classification_detail_id);
											$jsl_owned_by = '1';
											$company_specific_jsl_classification_duplicate_check_result = company_specific_jsl_classification_duplicate_check($company_id_input,$jsl_owned_by,$jsl_classification_detail_id);
							
											$eventLog->log("company_specific_jsl_classification_duplicate_check_result");
											
											if (count($company_specific_jsl_classification_duplicate_check_result) > 0) {
												
												
												$eventLog->log("jsl-classification-detail-id-already-exists for this id".$jsl_classification_detail_id);	
												
												//$eventLog->log("job-applicant-invite-already-exists: job applicant invite already exists");	
												$count--;
												continue;
											
											} else {
												
												$jsl_classifications_details_result = jsl_classification_details_based_on_jsl_classification_detail_id($jsl_classification_detail_id);
					
												$jsl_classification_detail_name = $jsl_classifications_details_result["jsl_classification_detail_name"];
												$jsl_classification_detail_seo_name = $jsl_classifications_details_result["jsl_classification_detail_seo_name"];
												
											
												$last_inserted_id = job_rel_classification_details_insert($company_id_input,$jsl_owned_by,$jsl_classification_detail_id,$jsl_classification_detail_name,$jsl_classification_detail_seo_name,$event_datetime,$current_epoch,$ea_extracted_jwt_token_sub);
									 
											
												$eventLog->log($last_inserted_id);
												$row_count++;
												$eventLog->log($row_count);
												
											}
											
										}
										
									}
									
									if ((is_array($company_specific_external_jsl_classifications_input)) && (count($company_specific_external_jsl_classifications_input) > 0)) {
											
													
										$count = count($company_specific_external_jsl_classifications_input);
										$row_count = 0;
										foreach ($company_specific_external_jsl_classifications_input as $company_specific_external_jsl_classifications_input_row) {
											$jsl_classification_detail_id = $company_specific_external_jsl_classifications_input_row["jsl_classification_detail_id"];
											
											$eventLog->log($jsl_classification_detail_id);
											$jsl_owned_by = '2';
											$company_specific_jsl_classification_duplicate_check_result = company_specific_jsl_classification_duplicate_check($company_id_input,$jsl_owned_by,$jsl_classification_detail_id);
							
											$eventLog->log("company_specific_jsl_classification_duplicate_check_result");
											
											if (count($company_specific_jsl_classification_duplicate_check_result) > 0) {
												
												
												$eventLog->log("jsl-classification-detail-id-already-exists for this id".$jsl_classification_detail_id);	
												
												//$eventLog->log("job-applicant-invite-already-exists: job applicant invite already exists");	
												$count--;
												continue;
											
											} else {
												
												$jsl_classifications_details_result = jsl_classification_details_based_on_jsl_classification_detail_id($jsl_classification_detail_id);
					
												$jsl_classification_detail_name = $jsl_classifications_details_result["jsl_classification_detail_name"];
												$jsl_classification_detail_seo_name = $jsl_classifications_details_result["jsl_classification_detail_seo_name"];
												
											
												$last_inserted_id = job_rel_classification_details_insert($company_id_input,$jsl_owned_by,$jsl_classification_detail_id,$jsl_classification_detail_name,$jsl_classification_detail_seo_name,$event_datetime,$current_epoch,$ea_extracted_jwt_token_sub);
									 
											
												$eventLog->log($last_inserted_id);
												$row_count++;
												$eventLog->log($row_count);
												
											}
											
										}
										
									}
										
										$eventLog->log("count".$count);
										$eventLog->log("row_count".$row_count);
										if ($count == $row_count) {
											
												$response['data'] = array();
												$response['status'] = "company-specific-jsl-classifications-insertion-successful";
												$response['status_description'] = "company specific JSL classifications added successfully";
											
												$eventLog->log("company specific JSL classifications added successfully.");   
											
										} else {
											//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
											$response['data'] = array();
											$response['status'] = "company-specific-jsl-classifications-insertion-error";
											$response['status_description'] = "Error occurred when  adding company specific JSL classifications";
											
											$eventLog->log("Error occurred when  adding company specific JSL classifications");
											
										}//close of else of if (count($user_ass_oc_validation_result) > 0) { 
										
									
										
								} catch (Exception $e) {
										$response['data'] = array();
										$response['status'] = "company-specific-jsl-classifications-insertion-error";
										$response['status_description'] = "Error occurred when  adding company specific JSL classifications";
										
										$eventLog->log("Error occurred when  adding company specific JSL classifications");
										
								}
							}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
							
						}
			//}//close of else of if ($email_id_input == "") {
					
		}//close of //close of if ($ea_is_user_page_access_authorized) {
					
				
	} else {
				
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "invalid-auth-token-submitted";
				$response['status_description'] = "Invalid Authentication";
				
				//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
				header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
				
				$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
				
			}//close of else of if ($ea_auth_token_validation_status) {
		
		}//close of if ($ea_maintenance_mode == false) {
		
} else {

		//Construct Content, that will be sent in Response body, of the REST Web Service
		$response['data'] = array();
		$response['status'] = "invalid-input";
		$response['status_description'] = "Invalid Input, Please check and provide all information.";
		
		//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
		
		$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
				
		
		}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




	//Check if Maintenance Mode is Turned On
	if ($ea_maintenance_mode) {	
		
		//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
		//header('Maintenance-Progress: true', false);
		//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
		$response['data'] = array();
		$response['status'] = "application-maintenance-in-progress";
		$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
		
		$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
		
	} else {	

		//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
		header('Content-Type: application/json');
		echo json_encode($response,JSON_PRETTY_PRINT);
		
	}//close of else of if ($ea_maintenance_mode){
		
function job_rel_classification_details_insert($company_id_input,$jsl_owned_by,$jsl_classification_detail_id,$jsl_classification_detail_name,$jsl_classification_detail_seo_name,$event_datetime,$current_epoch,$ea_extracted_jwt_token_sub) {
	
	
	global $dbcon,$eventLog;
	$is_active_status = '1';
	
    
	$job_rel_classification_details_insert_sql = "INSERT INTO `company_specific_jsl_classification_choices`(`company_id`, `jsl_owned_by`, `jsl_classification_detail_id`, `jsl_classification_detail_name`, `jsl_classification_detail_seo_name`, `added_datetime`, `added_datetime_epoch`, `added_by_sm_memb_id`, `is_active_status`) VALUES (:company_id,:jsl_owned_by,:jsl_classification_detail_id,:jsl_classification_detail_name,:jsl_classification_detail_seo_name,:added_datetime,:added_datetime_epoch,:added_by_sm_memb_id,:is_active_status)";

	$job_rel_classification_details_insert_query = $dbcon->prepare($job_rel_classification_details_insert_sql);
	$job_rel_classification_details_insert_query->bindValue(":company_id",$company_id_input);
	$job_rel_classification_details_insert_query->bindValue(":jsl_owned_by",$jsl_owned_by);
	$job_rel_classification_details_insert_query->bindValue(":jsl_classification_detail_id",$jsl_classification_detail_id);	
	$job_rel_classification_details_insert_query->bindValue(":jsl_classification_detail_name",$jsl_classification_detail_name);
	$job_rel_classification_details_insert_query->bindValue(":jsl_classification_detail_seo_name",$jsl_classification_detail_seo_name);
	$job_rel_classification_details_insert_query->bindValue(":added_datetime",$event_datetime);
	$job_rel_classification_details_insert_query->bindValue(":added_datetime_epoch",$current_epoch);
	$job_rel_classification_details_insert_query->bindValue(":added_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$job_rel_classification_details_insert_query->bindValue(":is_active_status",$is_active_status);
	
	if ($job_rel_classification_details_insert_query->execute()) {
          $eventLog->log("after execute.");
            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");
           
			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}


function company_specific_jsl_classification_duplicate_check($company_id_input,$jsl_owned_by,$jsl_classification_detail_id) {
	global $dbcon;
	$constructed_array = array();
	
	$company_specific_jsl_classification_duplicate_check_sql = "SELECT * FROM `company_specific_jsl_classification_choices` WHERE `company_id` = :company_id AND `jsl_owned_by` = :jsl_owned_by AND `jsl_classification_detail_id` = :jsl_classification_detail_id";
	$company_specific_jsl_classification_duplicate_check_query = $dbcon->prepare($company_specific_jsl_classification_duplicate_check_sql);	
	$company_specific_jsl_classification_duplicate_check_query->bindValue(":company_id",$company_id_input);	
	$company_specific_jsl_classification_duplicate_check_query->bindValue(":jsl_owned_by",$jsl_owned_by);
	$company_specific_jsl_classification_duplicate_check_query->bindValue(":jsl_classification_detail_id",$jsl_classification_detail_id);
		
	$company_specific_jsl_classification_duplicate_check_query->execute(); 
	
	if($company_specific_jsl_classification_duplicate_check_query->rowCount() > 0) {
		$company_specific_jsl_classification_duplicate_check_query_result = $company_specific_jsl_classification_duplicate_check_query->fetchAll();
	     return $company_specific_jsl_classification_duplicate_check_query_result;
	
	}
	return $constructed_array;
}



exit;
?>