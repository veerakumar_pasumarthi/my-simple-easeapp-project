
<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "2")) {

	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
			    $jsl_applicant_evaluation_info_id_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {

                if ($jsl_applicant_evaluation_info_id_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-id";
					$response['status_description'] = "Missing Job Id";
					
					$eventLog->log("missing-user-id: Missing Job Id.");
					
				
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else {
					//all inputs are valid
					try {
							$jsl_evaluation_info_result = jsl_evaluation_info_get($jsl_applicant_evaluation_info_id_input);
				
							$company_id_input = $jsl_evaluation_info_result["company_id"];
							$company_client_id_input = $jsl_evaluation_info_result["company_client_id"];
							$job_id_input = $jsl_evaluation_info_result["job_id"];
							$jsl_classification_detail_id = $jsl_evaluation_info_result["jsl_classification_detail_id"];
							$jsl_sub_classification_detail_id = $jsl_evaluation_info_result["jsl_sub_classification_detail_id"];
							$jsl_info_id = $jsl_evaluation_info_result["jsl_info_id"];
							$job_applicant_sm_memb_id_input = $jsl_evaluation_info_result["job_applicant_sm_memb_id"];
							$job_applicant_request_info_id_input = $jsl_evaluation_info_result["job_applicant_request_info_id"];
					
							$jsl_rel_required_docs_list_result = jsl_rel_required_docs_list($job_id_input,$company_id_input,$jsl_info_id,$jsl_applicant_evaluation_info_id_input);
					
							
							
							if (count($jsl_rel_required_docs_list_result) > 0) {
							
							$response['data'] = $jsl_rel_required_docs_list_result;
							$response['status'] = "job-related-screening-levels-list-successfully-fetched";
							$response['status_description'] = "Job related screening levels List Successfully Received";
						
							} else {
								
								$response['data'] = array();
								$response['status'] = "no-screening levels-defined-for-this-job";
								$response['status_description'] = "No screening levels defined found for this job";
								
							}
							
							
					} catch (Exception $e){
						
						$response['data'] = array();
						$response['status'] = "job-related-screening-levels-list-fetching-error";
						$response['status_description'] = "Job related screening levels List Fetching Error";
					}
					
					
				}
					
			}
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
	

function job_screening_level_details_based_on_jsl_info_id($jsl_info_id) {
	
	global $dbcon;
	$constructed_array = array();
	
	$job_screening_levels_list_info_sql = "SELECT * FROM `jsl_info` WHERE `jsl_info_id` =:jsl_info_id";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":jsl_info_id",$jsl_info_id);		
	$job_rel_screening_levels_list_info_query->execute(); 
	
	if($job_rel_screening_levels_list_info_query->rowCount() > 0) {
		$job_rel_screening_levels_list_info_query_result = $job_rel_screening_levels_list_info_query->fetch();
	     return $job_rel_screening_levels_list_info_query_result;
	
	}
	return $constructed_array;

}



function jsl_rel_required_docs_list($job_id_input,$company_id_input,$jsl_info_id,$jsl_applicant_evaluation_info_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	$assignment_status = "1";
	
	$job_rel_candidature_screening_levels_list_sql = "SELECT * FROM `jsl_rel_document_check_initial_criteria` WHERE `jsl_info_id` =:jsl_info_id AND `company_id` =:company_id";
	$job_rel_candidature_screening_levels_list_query = $dbcon->prepare($job_rel_candidature_screening_levels_list_sql);
	$job_rel_candidature_screening_levels_list_query->bindValue(":jsl_info_id",$jsl_info_id);		
	$job_rel_candidature_screening_levels_list_query->bindValue(":company_id",$company_id_input);
	$job_rel_candidature_screening_levels_list_query->execute(); 
	
	if($job_rel_candidature_screening_levels_list_query->rowCount() > 0) {
		$job_rel_candidature_screening_levels_list_query_result = $job_rel_candidature_screening_levels_list_query->fetchAll();
		
		foreach($job_rel_candidature_screening_levels_list_query_result as $job_rel_candidature_screening_levels_list_query_result_row) {
			
			$temp_array = array();		
			
			$temp_array["jrdcic_id"] = $job_rel_candidature_screening_levels_list_query_result_row["jrdcic_id"];
			$temp_array["jsl_rel_docu_check_initial_criteria_name"] = $job_rel_candidature_screening_levels_list_query_result_row["jsl_rel_docu_check_initial_criteria_name"];
			$temp_array["uploaded_docs"] = jsl_applicant_specific_list($temp_array["jrdcic_id"],$temp_array["jsl_rel_docu_check_initial_criteria_name"], $jsl_applicant_evaluation_info_id_input);
			
			
			
			
			$constructed_array[] = $temp_array;
		 
		}
		
		return $constructed_array;
	
	}//close of if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
	return $constructed_array;
}



function jsl_applicant_specific_list($jrdcic_id_input,$jsl_rel_docu_check_initial_criteria_name_input, $jsl_applicant_evaluation_info_id) {
	
	global $dbcon;
	$constructed_array = array();
	$jrdcic_id = "1";
	
	$job_screening_levels_list_info_sql = "SELECT * FROM `jsl_rel_criteria_applicant_evaluation_values` WHERE `jsl_applicant_evaluation_info_id` =:jsl_applicant_evaluation_info_id AND `jrdcic_id` =:jrdcic_id";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":jsl_applicant_evaluation_info_id",$jsl_applicant_evaluation_info_id);
	$job_rel_screening_levels_list_info_query->bindValue(":jrdcic_id",$jrdcic_id);	
	$job_rel_screening_levels_list_info_query->execute(); 
	
	if($job_rel_screening_levels_list_info_query->rowCount() > 0) {
		$job_rel_screening_levels_list_info_query_result = $job_rel_screening_levels_list_info_query->fetchAll();
		foreach ($job_rel_screening_levels_list_info_query_result as $jsl_specific_candidature_details_query_result_row) {
			
			$constructed_array["jrcaev_id"] = $jsl_specific_candidature_details_query_result_row["jrcaev_id"];
			$constructed_array["criteria_rel_doc_original_filename"]  = $jsl_specific_candidature_details_query_result_row["criteria_rel_doc_original_filename"];
			$constructed_array["criteria_rel_doc_generated_filename"] = $jsl_specific_candidature_details_query_result_row["criteria_rel_doc_generated_filename"];
			$constructed_array["jsl_applicant_evaluation_info_id"] = $jsl_specific_candidature_details_query_result_row["jsl_applicant_evaluation_info_id"];
			$constructed_array["criteria_rel_doc_original_filename"] = $jsl_specific_candidature_details_query_result_row["criteria_rel_doc_original_filename"];
			$constructed_array["criteria_rel_doc_generated_filename"] = $jsl_specific_candidature_details_query_result_row["criteria_rel_doc_generated_filename"];
			$constructed_array["added_by_sm_memb_id"] = $jsl_specific_candidature_details_query_result_row["added_by_sm_memb_id"];
			$constructed_array["uploaded_doc_url"] = null;
			/* $link_expiry_time = $current_epoch+$expiring_link_lifetime;
			$uploaded_file_link_with_signature_result = create_uploaded_file_link_with_signature($ea_extracted_jwt_token_sub, $constructed_array["added_by_sm_memb_id"], $temp_row_array["crur_id"], "https", $site_hostname_value, $link_expiry_time, $expiring_link_hash_algorithm, $expiring_link_secret_key);
			if (isset($uploaded_file_link_with_signature_result["uploaded_file_url"])) {
			$temp_row_array["uploaded_file_url"] = $uploaded_file_link_with_signature_result["uploaded_file_url"];
				
			} else {
				$temp_row_array["uploaded_file_url"] = null;
			}//close of if (isset($uploaded_file_link_with_signature_result["uploaded_file_url"])) {
			
			 */
			
		}//close of foreach ($job_management_assignees_get_select_query_result as $job_management_assignees_get_select_query_result_row) {
	     return $constructed_array;
	
	}
	return $constructed_array;
	
}



exit;
?>