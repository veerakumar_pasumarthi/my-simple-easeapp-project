<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Edit Quick Admin Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "12")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					
					if (isset($ea_received_rest_ws_raw_array_input['crai_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['crai_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['crai_id'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['sm_memb_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['sm_memb_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['sm_memb_id'])) 	
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					
					/* if (isset($ea_received_rest_ws_raw_array_input['profile_source_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['profile_source_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['profile_source_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['profile_source_name'])) {
						$content .= $ea_received_rest_ws_raw_array_input['profile_source_name'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['profile_source_name'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['profile_source_seo_name'])) {
						$content .= $ea_received_rest_ws_raw_array_input['profile_source_seo_name'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['profile_source_seo_name']))  */
						
					if (isset($ea_received_rest_ws_raw_array_input['sm_job_experience_level'])) {
						$content .= $ea_received_rest_ws_raw_array_input['sm_job_experience_level'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['sm_job_experience_level'])) 
						

					if (isset($ea_received_rest_ws_raw_array_input['experience_years'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['experience_years'] . "\r\n"; 
					}//close of if (isset($ea_received_rest_ws_raw_array_input['experience_years'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['experience_months'])) {
						$content .= $ea_received_rest_ws_raw_array_input['experience_months'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['experience_months']))
					
					if (isset($ea_received_rest_ws_raw_array_input['current_salary'])) {
						$content .= $ea_received_rest_ws_raw_array_input['current_salary'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['current_salary']))
					
                    if (isset($ea_received_rest_ws_raw_array_input['preferred_salary'])) {
						$content .= $ea_received_rest_ws_raw_array_input['preferred_salary'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['preferred_salary']))
						
					if (isset($ea_received_rest_ws_raw_array_input['on_notice_period'])) {
						$content .= $ea_received_rest_ws_raw_array_input['on_notice_period'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['on_notice_period']))	
						
					if (isset($ea_received_rest_ws_raw_array_input['notice_period'])) {
						$content .= $ea_received_rest_ws_raw_array_input['notice_period'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['notice_period']))
						
				    if (isset($ea_received_rest_ws_raw_array_input['notice_period_metric'])) {
						$content .= $ea_received_rest_ws_raw_array_input['notice_period_metric'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['notice_period_metric']))
						
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				
				$crai_id_input = trim(isset($ea_received_rest_ws_raw_array_input['crai_id']) ? filter_var($ea_received_rest_ws_raw_array_input['crai_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$sm_memb_id_input = trim(isset($ea_received_rest_ws_raw_array_input['sm_memb_id']) ? filter_var($ea_received_rest_ws_raw_array_input['sm_memb_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				/* $profile_source_id_input = trim(isset($ea_received_rest_ws_raw_array_input['profile_source_id']) ? filter_var($ea_received_rest_ws_raw_array_input['profile_source_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$profile_source_name_input = trim(isset($ea_received_rest_ws_raw_array_input['profile_source_name']) ? filter_var($ea_received_rest_ws_raw_array_input['profile_source_name'], FILTER_SANITIZE_STRING) : '');
				
				$profile_source_seo_name_input = trim(isset($ea_received_rest_ws_raw_array_input['profile_source_seo_name']) ? filter_var($ea_received_rest_ws_raw_array_input['profile_source_seo_name'], FILTER_SANITIZE_STRING) : ''); */
				
				$sm_job_experience_level_input = trim(isset($ea_received_rest_ws_raw_array_input['sm_job_experience_level']) ? filter_var($ea_received_rest_ws_raw_array_input['sm_job_experience_level'], FILTER_SANITIZE_STRING) : '');
				
				$experience_years_input = trim(isset($ea_received_rest_ws_raw_array_input['experience_years']) ? filter_var($ea_received_rest_ws_raw_array_input['experience_years'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$experience_months_input = trim(isset($ea_received_rest_ws_raw_array_input['experience_months']) ? filter_var($ea_received_rest_ws_raw_array_input['experience_months'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$current_salary_input = trim(isset($ea_received_rest_ws_raw_array_input['current_salary']) ? filter_var($ea_received_rest_ws_raw_array_input['current_salary'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$preferred_salary_input = trim(isset($ea_received_rest_ws_raw_array_input['preferred_salary']) ? filter_var($ea_received_rest_ws_raw_array_input['preferred_salary'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$on_notice_period_input = trim(isset($ea_received_rest_ws_raw_array_input['on_notice_period']) ? filter_var($ea_received_rest_ws_raw_array_input['on_notice_period'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$notice_period_input = trim(isset($ea_received_rest_ws_raw_array_input['notice_period']) ? filter_var($ea_received_rest_ws_raw_array_input['notice_period'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$notice_period_metric_input = trim(isset($ea_received_rest_ws_raw_array_input['notice_period_metric']) ? filter_var($ea_received_rest_ws_raw_array_input['notice_period_metric'], FILTER_SANITIZE_NUMBER_INT) : '');
						
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//$eventLog->log("company_id : " . $company_id_input);
				//Check if all inputs are received correctly from the REST Web Service
				if ($crai_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-crai-id";
					$response['status_description'] = "Missing Crai ID, please check and try again.";
					
					$eventLog->log("Please provide a valid Crai ID.");
					
				}  else if  ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Some Additional Information is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");
					
                }  else if ($company_id_input == "") {
					//missing Company ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "missing company id";
					
					$eventLog->log("missing-company-id: Please provide a valid company id.");
				
				
				}  else if ($sm_memb_id_input == "") {
					//missing Company ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-sm_memb-id";
					$response['status_description'] = "missing sm memb id";
					
					$eventLog->log("missing-company-id: Please provide a valid sm memb id.");
				
				
				}else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//Event Time, as per Indian Standard Time
					$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
		
					$candidate_additional_information_edit_next_step = "";
					
					if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$candidate_additional_information_edit_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$candidate_additional_information_edit_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
					} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
					}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
							
						if ($candidate_additional_information_edit_next_step == "PROCEED-TO-NEXT-STEP") {
						$user_additional_details_result = user_additional_details_check_based_on_crai_id($crai_id_input);
					
						if (count($user_additional_details_result) > 0) {
						
						
                       //if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
								$eventLog->log("before enter into query.");
								
								      
									  $candidate_additional_details_update = candidate_additional_details_update($crai_id_input,$sm_memb_id_input,$company_id_input,$sm_job_experience_level_input,$experience_years_input,$experience_months_input,$current_salary_input,$preferred_salary_input,$on_notice_period_input,$notice_period_input,$notice_period_metric_input,$event_datetime,$current_epoch);
									  
									  
									  
								      $eventLog->log("After update query.");
									  
												if ($candidate_additional_details_update== true) {
													$eventLog->log("enter into user ass if condition.");
													//Use Privilege details are updated successfully, for the user, in sm_site_member_classification_associations db table
													//Construct Content, that will be sent in Response body, of the REST Web Service
													$response['data'] = array();
													$response['status'] = "candidate-additional-details-updated-successfully";
													$response['status_description'] = "candidate additional details are edited Successfully.";
													
													$eventLog->log("The candidate additional details are edited Successfully.");
													$eventLog->log("before update status.");
												} else {
													//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
													$response['data'] = array();
													$response['status'] = "candidate-additional-data-editing-error";
													$response['status_description'] = "There is an error, when editing the candidate aadditional data in the Database.";
													
													$eventLog->log("There is an error, when editing the candidate additional data in the Database.");
													
												}
										
										
								} catch (Exception $e) {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "candidate-additional-data-editing-error";
									$response['status_description'] = "There is an error, when editing the candidate aadditional data in the Database";
									
									$eventLog->log("There is an error, when editing the candidate additional data in the Database.");

									
								}
					   
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "The User Account, that is attempted to be edited, is invalid, please check and try again.";
						
						$eventLog->log("Please provide a Valid User ID.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
					
						
				}//close of else of if ($email_id_input == "") {
				
			}
			
		}	
			
	} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function candidate_additional_details_update($crai_id_input,$sm_memb_id_input,$company_id_input,$sm_job_experience_level_input,$experience_years_input,$experience_months_input,$current_salary_input,$preferred_salary_input,$on_notice_period_input,$notice_period_input,$notice_period_metric_input,$event_datetime,$current_epoch) {
	global $dbcon, $eventLog;
	
	$eventLog->log("in function before query");
    
	$candidate_additional_details_update_sql = "UPDATE `candidate_rel_additional_info` SET `sm_memb_id`=:sm_memb_id,`company_id`=:company_id,`sm_job_experience_level`=:sm_job_experience_level,`experience_years`=:experience_years,`experience_months`=:experience_months,`current_salary`=:current_salary,`preferred_salary`=:preferred_salary,`on_notice_period`=:on_notice_period,`notice_period`=:notice_period,`notice_period_metric`=:notice_period_metric,`last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch WHERE `crai_id`=:crai_id";
	
	
	$candidate_additional_details_update_query = $dbcon->prepare($candidate_additional_details_update_sql);
	$eventLog->log("in function before bind");
	
	$candidate_additional_details_update_query->bindValue(":crai_id",$crai_id_input);
	$candidate_additional_details_update_query->bindValue(":sm_memb_id",$sm_memb_id_input);
	$candidate_additional_details_update_query->bindValue(":company_id",$company_id_input);
	
	$candidate_additional_details_update_query->bindValue(":sm_job_experience_level",$sm_job_experience_level_input);
	$candidate_additional_details_update_query->bindValue(":experience_years",$experience_years_input);
	$candidate_additional_details_update_query->bindValue(":experience_months",$experience_months_input);
	$candidate_additional_details_update_query->bindValue(":current_salary",$current_salary_input);
	$candidate_additional_details_update_query->bindValue(":preferred_salary",$preferred_salary_input);
	$candidate_additional_details_update_query->bindValue(":on_notice_period",$on_notice_period_input);
	$candidate_additional_details_update_query->bindValue(":notice_period",$notice_period_input);
	$candidate_additional_details_update_query->bindValue(":notice_period_metric",$notice_period_metric_input);
	$candidate_additional_details_update_query->bindValue(":last_updated_date_time",$event_datetime);
	$candidate_additional_details_update_query->bindValue(":last_updated_date_time_epoch",$current_epoch);
	
	$eventLog->log("in function after bind");

	if ($candidate_additional_details_update_query->execute()) {
		
		$eventLog->log("in function enter into if");

		return true;

	}
	return false;
}
function user_additional_details_check_based_on_crai_id($crai_id_input) {
	global $dbcon;
	$constructed_array = array();
	$candidate_rel_additional_details_check_sql = "SELECT * FROM `candidate_rel_additional_info` WHERE `crai_id` = :crai_id";
	$candidate_details_check_select_query = $dbcon->prepare($candidate_rel_additional_details_check_sql);
	$candidate_details_check_select_query->bindValue(":crai_id",$crai_id_input);
	
	$candidate_details_check_select_query->execute();

	if($candidate_details_check_select_query->rowCount() > 0) {
		$candidate_details_check_select_query_result = $candidate_details_check_select_query->fetch();
	     return $candidate_details_check_select_query_result;

	}//close of if($rest_user_check_select_query->rowCount() > 0) {
	return $constructed_array;

}	

exit;
?>


