
<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "32")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['job_posted_by_sm_memb_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_posted_by_sm_memb_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_posted_by_sm_memb_id'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_client_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) 
						
	                if (isset($ea_received_rest_ws_raw_array_input['job_title'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_title'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_title'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['job_summary'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_summary'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_summary'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['job_full_description'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_full_description'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_full_description'])) 	
					
					if (isset($ea_received_rest_ws_raw_array_input['job_type'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_type'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_type'])) 
							
					if (isset($ea_received_rest_ws_raw_array_input['min_experience_period'])) {
						$content .= $ea_received_rest_ws_raw_array_input['min_experience_period'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['min_experience_period'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['max_experience_period'])) {
						$content .= $ea_received_rest_ws_raw_array_input['max_experience_period'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['max_experience_period']))
					
					if (isset($ea_received_rest_ws_raw_array_input['bill_rate_value'])) {
						$content .= $ea_received_rest_ws_raw_array_input['bill_rate_value'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['bill_rate_value'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['bill_rate_currency'])) {
						$content .= $ea_received_rest_ws_raw_array_input['bill_rate_currency'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['bill_rate_currency'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['bill_rate_job_compensation_period'])) {
						$content .= $ea_received_rest_ws_raw_array_input['bill_rate_job_compensation_period'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['bill_rate_job_compensation_period'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['pay_rate_currency'])) {
						$content .= $ea_received_rest_ws_raw_array_input['pay_rate_currency'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['pay_rate_currency'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['pay_rate_job_compensation_period'])) {
						$content .= $ea_received_rest_ws_raw_array_input['pay_rate_job_compensation_period'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['pay_rate_job_compensation_period'])) 
					
					
					if (isset($ea_received_rest_ws_raw_array_input['pay_rate_min_value'])) {
						$content .= $ea_received_rest_ws_raw_array_input['pay_rate_min_value'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['pay_rate_min_value'])) 

					if (isset($ea_received_rest_ws_raw_array_input['pay_rate_max_value'])) {
						$content .= $ea_received_rest_ws_raw_array_input['pay_rate_max_value'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['pay_rate_max_value'])) 

					if (isset($ea_received_rest_ws_raw_array_input['salary_display_options'])) {
						$content .= $ea_received_rest_ws_raw_array_input['salary_display_options'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['salary_display_options'])) 
					
                    if (isset($ea_received_rest_ws_raw_array_input['job_industry'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_industry'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_industry'])) 

					if (isset($ea_received_rest_ws_raw_array_input['job_work_location_requirement'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_work_location_requirement'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_work_location_requirement_id']))
						
					if (isset($ea_received_rest_ws_raw_array_input['address_line_1'])) {
						$content .= $ea_received_rest_ws_raw_array_input['address_line_1'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['address_line_1']))
					
					if (isset($ea_received_rest_ws_raw_array_input['address_line_2'])) {
						$content .= $ea_received_rest_ws_raw_array_input['address_line_2'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['address_line_2']))
					
					if (isset($ea_received_rest_ws_raw_array_input['city'])) {
						$content .= $ea_received_rest_ws_raw_array_input['city'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['city']))
						
					if (isset($ea_received_rest_ws_raw_array_input['state'])) {
						$content .= $ea_received_rest_ws_raw_array_input['state'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['state']))
						
					if (isset($ea_received_rest_ws_raw_array_input['country'])) {
						$content .= $ea_received_rest_ws_raw_array_input['country'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['country']))
					
					if (isset($ea_received_rest_ws_raw_array_input['zipcode'])) {
						$content .= $ea_received_rest_ws_raw_array_input['zipcode'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['zipcode']))
						
					if (isset($ea_received_rest_ws_raw_array_input['no_of_openings'])) {
						$content .= $ea_received_rest_ws_raw_array_input['no_of_openings'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['no_of_openings']))
					
					
					
					if (isset($ea_received_rest_ws_raw_array_input['expected_job_start_date'])) {
						$content .= $ea_received_rest_ws_raw_array_input['expected_job_start_date'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['expected_job_start_date']))
					
					if (isset($ea_received_rest_ws_raw_array_input['expected_job_end_date'])) {
						$content .= $ea_received_rest_ws_raw_array_input['expected_job_end_date'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['expected_job_end_date']))
					
					if (isset($ea_received_rest_ws_raw_array_input['maximum_allowed_submittals_count'])) {
						$content .= $ea_received_rest_ws_raw_array_input['maximum_allowed_submittals_count'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['maximum_allowed_submittals_count']))
						
					if (isset($ea_received_rest_ws_raw_array_input['job_recruitment_status'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_recruitment_status'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_recruitment_status']))
					
					if (isset($ea_received_rest_ws_raw_array_input['resume_submission_end_date'])) {
						$content .= $ea_received_rest_ws_raw_array_input['resume_submission_end_date'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['resume_submission_end_date']))
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
					}//close of if ($ea_received_rest_ws_raw_array_input != "") 
						
					//Filter Inputs	
					$job_posted_by_sm_memb_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_posted_by_sm_memb_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_posted_by_sm_memb_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					$bill_rate_value_input = trim(isset($ea_received_rest_ws_raw_array_input['bill_rate_value']) ? filter_var($ea_received_rest_ws_raw_array_input['bill_rate_value'], FILTER_SANITIZE_NUMBER_INT) : '');
					$maximum_allowed_submittals_count_input = trim(isset($ea_received_rest_ws_raw_array_input['maximum_allowed_submittals_count']) ? filter_var($ea_received_rest_ws_raw_array_input['maximum_allowed_submittals_count'], FILTER_SANITIZE_NUMBER_INT) : '');
					$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					$company_client_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_client_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_client_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					$job_title_input = trim(isset($ea_received_rest_ws_raw_array_input['job_title']) ? filter_var($ea_received_rest_ws_raw_array_input['job_title'], FILTER_SANITIZE_STRING) : '');
					$min_experience_period_input = trim(isset($ea_received_rest_ws_raw_array_input['min_experience_period']) ? filter_var($ea_received_rest_ws_raw_array_input['min_experience_period'], FILTER_SANITIZE_NUMBER_INT) : '');
					$max_experience_period_input = trim(isset($ea_received_rest_ws_raw_array_input['max_experience_period']) ? filter_var($ea_received_rest_ws_raw_array_input['max_experience_period'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					
					
					$job_seo_title_input = strtolower(create_seo_name($job_title_input));
					
					$job_summary_input = trim(isset($ea_received_rest_ws_raw_array_input['job_summary']) ? filter_var($ea_received_rest_ws_raw_array_input['job_summary'], FILTER_SANITIZE_STRING) : '');
					
					if ($job_summary_input == "") {
						$job_summary_input = null;
					}
					
					$job_summary_all_input = trim($ea_received_rest_ws_raw_array_input["job_summary"]);
					$job_summary_safe_html_input = get_cleaned_safe_html_content_input($job_summary_all_input, $page_content_file_config);
					
					$job_full_description_input = trim(isset($ea_received_rest_ws_raw_array_input['job_full_description']) ? filter_var($ea_received_rest_ws_raw_array_input['job_full_description'], FILTER_SANITIZE_STRING) : '');
					if ($job_full_description_input == "") {
						$job_full_description_input = null;
					} 
					
					
					$job_full_description_all_input = trim($ea_received_rest_ws_raw_array_input["job_full_description"]);
					$job_full_description_safe_html_input = get_cleaned_safe_html_content_input($job_full_description_all_input, $page_content_file_config);
					
					$job_type_input = trim(isset($ea_received_rest_ws_raw_array_input['job_type']) ? filter_var($ea_received_rest_ws_raw_array_input['job_type'], FILTER_SANITIZE_STRING) : '');
					
					$eventLog->log("job_type_input => ".$job_type_input);
					
					$job_type_input_exploded = explode(":::::",$job_type_input);
					
					$job_type_input_exploded_count = count($job_type_input_exploded);
					
					$job_type_id_input = null;
						
					$job_type_name_input = null;
					
					$job_type_seo_name_input = null;
					
					if ($job_type_input_exploded_count == "3") {
						
						$job_type_id_input = trim(isset($job_type_input_exploded[0]) ? filter_var($job_type_input_exploded[0], FILTER_SANITIZE_NUMBER_INT) : '');
						
						$job_type_name_input = trim(isset($job_type_input_exploded[1]) ? filter_var($job_type_input_exploded[1], FILTER_SANITIZE_STRING) : '');
						
						$job_type_seo_name_input = trim(isset($job_type_input_exploded[2]) ? filter_var($job_type_input_exploded[2], FILTER_SANITIZE_STRING) : '');
						
						}//close of if ($job_type_input_exploded_count != "3") {
						
					
	
						
				    $bill_rate_currency_input = trim(isset($ea_received_rest_ws_raw_array_input['bill_rate_currency']) ? filter_var($ea_received_rest_ws_raw_array_input['bill_rate_currency'], FILTER_SANITIZE_STRING) : '');
						
						$eventLog->log("bill_rate_currency_input => ".$bill_rate_currency_input);
						
						$bill_rate_currency_input_exploded = explode(":::::",$bill_rate_currency_input);
						
						$bill_rate_currency_input_exploded_count = count($bill_rate_currency_input_exploded);
						$eventLog->log("bill_rate_currency_input_exploded_count => ".$bill_rate_currency_input_exploded_count);
						
						
						
						$bill_rate_currency_id_input = null;
							
						$bill_rate_currency_name_input = null;
						
						$bill_rate_currency_seo_name_input = null;
						
						$bill_rate_currency_three_lettered_code_input = null;
						$eventLog->log("bill_rate_currency_three_lettered_code_input => ".$bill_rate_currency_three_lettered_code_input);
						
					
					
					
					if ($bill_rate_currency_input_exploded_count == "4") {
						
						$bill_rate_currency_id_input = trim(isset($bill_rate_currency_input_exploded[0]) ? filter_var($bill_rate_currency_input_exploded[0], FILTER_SANITIZE_NUMBER_INT) : '');
						
						$bill_rate_currency_name_input = trim(isset($bill_rate_currency_input_exploded[1]) ? filter_var($bill_rate_currency_input_exploded[1], FILTER_SANITIZE_STRING) : '');
						
						$bill_rate_currency_seo_name_input = trim(isset($bill_rate_currency_input_exploded[2]) ? filter_var($bill_rate_currency_input_exploded[2], FILTER_SANITIZE_STRING) : '');
						
						$bill_rate_currency_three_lettered_code_input = trim(isset($bill_rate_currency_input_exploded[3]) ? filter_var($bill_rate_currency_input_exploded[3], FILTER_SANITIZE_STRING) : '');
						$eventLog->log("bill_rate_currency_three_lettered_code_input => ".$bill_rate_currency_three_lettered_code_input);
						
						
						
					}//close of if ($currency_input_exploded_count != "3") {
						
					$pay_rate_currency_input = trim(isset($ea_received_rest_ws_raw_array_input['pay_rate_currency']) ? filter_var($ea_received_rest_ws_raw_array_input['pay_rate_currency'], FILTER_SANITIZE_STRING) : '');
						
						$eventLog->log("pay_rate_currency_input => ".$pay_rate_currency_input);
						
						$pay_rate_currency_input_exploded = explode(":::::",$pay_rate_currency_input);
						
						$pay_rate_currency_input_exploded_count = count($pay_rate_currency_input_exploded);
						$eventLog->log("pay_rate_currency_input_exploded_count => ".$pay_rate_currency_input_exploded_count);
						
						
						$pay_rate_currency_id_input = null;
							
						$pay_rate_currency_name_input = null;
						
						$pay_rate_currency_seo_name_input = null;
						
						$pay_rate_currency_three_lettered_code_input = null;
						
					
					
					
					if ($pay_rate_currency_input_exploded_count == "4") {
						
						$pay_rate_currency_id_input = trim(isset($pay_rate_currency_input_exploded[0]) ? filter_var($pay_rate_currency_input_exploded[0], FILTER_SANITIZE_NUMBER_INT) : '');
						
						$pay_rate_currency_name_input = trim(isset($pay_rate_currency_input_exploded[1]) ? filter_var($pay_rate_currency_input_exploded[1], FILTER_SANITIZE_STRING) : '');
						
						$pay_rate_currency_seo_name_input = trim(isset($pay_rate_currency_input_exploded[2]) ? filter_var($pay_rate_currency_input_exploded[2], FILTER_SANITIZE_STRING) : '');
						
						$pay_rate_currency_three_lettered_code_input = trim(isset($pay_rate_currency_input_exploded[3]) ? filter_var($pay_rate_currency_input_exploded[3], FILTER_SANITIZE_STRING) : '');
						
						$eventLog->log("pay_rate_currency_three_lettered_code_input => ".$pay_rate_currency_three_lettered_code_input);
						
						
						
					}//close of if ($currency_input_exploded_count != "3") {
						
					
					$pay_rate_min_value_input = trim(isset($ea_received_rest_ws_raw_array_input['pay_rate_min_value']) ? filter_var($ea_received_rest_ws_raw_array_input['pay_rate_min_value'], FILTER_SANITIZE_STRING) : '');
					if ($pay_rate_min_value_input == "") {
						$pay_rate_min_value_input = null;
					} 
					 
					
					$pay_rate_max_value_input = trim(isset($ea_received_rest_ws_raw_array_input['pay_rate_max_value']) ? filter_var($ea_received_rest_ws_raw_array_input['pay_rate_max_value'], FILTER_SANITIZE_STRING) : '');
					if ($pay_rate_max_value_input == "") {
						$pay_rate_max_value_input = null;
					} 
									
					$salary_display_options_input = trim(isset($ea_received_rest_ws_raw_array_input['salary_display_options']) ? filter_var($ea_received_rest_ws_raw_array_input['salary_display_options'], FILTER_SANITIZE_STRING) : '');
					if ($salary_display_options_input == "") {
						$salary_display_options_input = null;
					} 
						
					$bill_rate_job_compensation_period_input = trim(isset($ea_received_rest_ws_raw_array_input['bill_rate_job_compensation_period']) ? filter_var($ea_received_rest_ws_raw_array_input['bill_rate_job_compensation_period'], FILTER_SANITIZE_STRING) : '');
					
					$eventLog->log("bill_rate_job_compensation_period_input => ".$bill_rate_job_compensation_period_input);
					
					$bill_rate_job_compensation_period_input_exploded = explode(":::::",$bill_rate_job_compensation_period_input);
					
					$bill_rate_job_compensation_period_input_exploded_count = count($bill_rate_job_compensation_period_input_exploded);
					
					$bill_rate_job_compensation_period_id_input = null;
						
					$bill_rate_job_compensation_period_name_input = null;
					
					$bill_rate_job_compensation_period_seo_name_input = null;
					
					if ($bill_rate_job_compensation_period_input_exploded_count == "3") {
						
						$bill_rate_job_compensation_period_id_input = trim(isset($bill_rate_job_compensation_period_input_exploded[0]) ? filter_var($bill_rate_job_compensation_period_input_exploded[0], FILTER_SANITIZE_NUMBER_INT) : '');
						
						$bill_rate_job_compensation_period_name_input = trim(isset($bill_rate_job_compensation_period_input_exploded[1]) ? filter_var($bill_rate_job_compensation_period_input_exploded[1], FILTER_SANITIZE_STRING) : '');
						
						$bill_rate_job_compensation_period_seo_name_input = trim(isset($bill_rate_job_compensation_period_input_exploded[2]) ? filter_var($bill_rate_job_compensation_period_input_exploded[2], FILTER_SANITIZE_STRING) : '');
						
					}//close of if ($job_compensation_period_input_exploded_count != "3") {
						
					$pay_rate_job_compensation_period_input = trim(isset($ea_received_rest_ws_raw_array_input['pay_rate_job_compensation_period']) ? filter_var($ea_received_rest_ws_raw_array_input['pay_rate_job_compensation_period'], FILTER_SANITIZE_STRING) : '');
					
					$eventLog->log("pay_rate_job_compensation_period_input => ".$pay_rate_job_compensation_period_input);
					
					$pay_rate_job_compensation_period_input_exploded = explode(":::::",$pay_rate_job_compensation_period_input);
					
					$pay_rate_job_compensation_period_input_exploded_count = count($pay_rate_job_compensation_period_input_exploded);
					
					$pay_rate_job_compensation_period_id_input = null;
						
					$pay_rate_job_compensation_period_name_input = null;
					
					$pay_rate_job_compensation_period_seo_name_input = null;
					
					if ($pay_rate_job_compensation_period_input_exploded_count == "3") {
						
						$pay_rate_job_compensation_period_id_input = trim(isset($pay_rate_job_compensation_period_input_exploded[0]) ? filter_var($pay_rate_job_compensation_period_input_exploded[0], FILTER_SANITIZE_NUMBER_INT) : '');
						
						$pay_rate_job_compensation_period_name_input = trim(isset($pay_rate_job_compensation_period_input_exploded[1]) ? filter_var($pay_rate_job_compensation_period_input_exploded[1], FILTER_SANITIZE_STRING) : '');
						
						$pay_rate_job_compensation_period_seo_name_input = trim(isset($pay_rate_job_compensation_period_input_exploded[2]) ? filter_var($pay_rate_job_compensation_period_input_exploded[2], FILTER_SANITIZE_STRING) : '');
						
					}//close of if ($job_compensation_period_input_exploded_count != "3") {
						
				
					$job_industry_input = trim(isset($ea_received_rest_ws_raw_array_input['job_industry']) ? filter_var($ea_received_rest_ws_raw_array_input['job_industry'], FILTER_SANITIZE_STRING) : '');
					
					$eventLog->log("job_industry_input => ".$job_industry_input);
					
					$job_industry_input_exploded = explode(":::::",$job_industry_input);
					
					$job_industry_input_exploded_count = count($job_industry_input_exploded);
					
					$job_industry_id_input = null;
						
					$job_industry_name_input = null;
					
					$job_industry_seo_name_input = null;
					
					if ($job_industry_input_exploded_count == "3") {
						
						$job_industry_id_input = trim(isset($job_industry_input_exploded[0]) ? filter_var($job_industry_input_exploded[0], FILTER_SANITIZE_NUMBER_INT) : '');
						
						$job_industry_name_input = trim(isset($job_industry_input_exploded[1]) ? filter_var($job_industry_input_exploded[1], FILTER_SANITIZE_STRING) : '');
						
						$job_industry_seo_name_input = trim(isset($job_industry_input_exploded[2]) ? filter_var($job_industry_input_exploded[2], FILTER_SANITIZE_STRING) : '');
						
					}//close of if ($job_industry_input_exploded_count != "3") {
					
				   $job_work_location_requirement_input = trim(isset($ea_received_rest_ws_raw_array_input['job_work_location_requirement']) ? filter_var($ea_received_rest_ws_raw_array_input['job_work_location_requirement'], FILTER_SANITIZE_STRING) : '');
				  
				  
				  $eventLog->log("job_work_location_requirement_input => ".$job_work_location_requirement_input);
					
				  $job_work_location_requirement_input_exploded = explode(":::::",$job_work_location_requirement_input);
					
				  $job_work_location_requirement_input_exploded_count = count($job_work_location_requirement_input_exploded);
					
				  $job_work_location_requirement_id_input = null;
						
				  $job_work_location_requirement_name_input = null;
					
				  $job_work_location_requirement_seo_name_input = null;
					
				   if ($job_work_location_requirement_input_exploded_count == "3") {
							
					$job_work_location_requirement_id_input = trim(isset($job_work_location_requirement_input_exploded[0]) ? filter_var($job_work_location_requirement_input_exploded[0], FILTER_SANITIZE_NUMBER_INT) : '');
						
					$job_work_location_requirement_name_input = trim(isset($job_work_location_requirement_input_exploded[1]) ? filter_var($job_work_location_requirement_input_exploded[1], FILTER_SANITIZE_STRING) : '');
						
					$job_work_location_requirement_seo_name_input = trim(isset($job_work_location_requirement_input_exploded[2]) ? filter_var($job_work_location_requirement_input_exploded[2], FILTER_SANITIZE_STRING) : '');
						
				}//close of if ($job_work_location_requirement_input_exploded_count != "3") {
					
					$address_line_1_input = trim(isset($ea_received_rest_ws_raw_array_input['address_line_1']) ? filter_var($ea_received_rest_ws_raw_array_input['address_line_1'], FILTER_SANITIZE_STRING) : '');
					if ($address_line_1_input == "") {
						$address_line_1_input = null;
					}
					
					$address_line_2_input = trim(isset($ea_received_rest_ws_raw_array_input['address_line_2']) ? filter_var($ea_received_rest_ws_raw_array_input['address_line_2'], FILTER_SANITIZE_STRING) : '');
					if ($address_line_2_input == "") {
						$address_line_2_input = null;
					}
					
					$city_input = trim(isset($ea_received_rest_ws_raw_array_input['city']) ? filter_var($ea_received_rest_ws_raw_array_input['city'], FILTER_SANITIZE_STRING) : '');
					if ($city_input == "") {
						$city_input = null;
					}
					  
					
					$state_input = trim(isset($ea_received_rest_ws_raw_array_input['state']) ? filter_var($ea_received_rest_ws_raw_array_input['state'], FILTER_SANITIZE_STRING) : '');
					if ($state_input == "") {
						$state_input = null;
					}
					 
					$country_input = trim(isset($ea_received_rest_ws_raw_array_input['country']) ? filter_var($ea_received_rest_ws_raw_array_input['country'], FILTER_SANITIZE_STRING) : '');
					if ($country_input == "") {
						$country_input = null;
					}
					$zipcode_input = trim(isset($ea_received_rest_ws_raw_array_input['zipcode']) ? filter_var($ea_received_rest_ws_raw_array_input['zipcode'], FILTER_SANITIZE_STRING) : '');
					if ($zipcode_input == "") {
						$zipcode_input = null;
					}
					
					//$notes_input = trim(isset($ea_received_rest_ws_raw_array_input['notes']) ? filter_var($ea_received_rest_ws_raw_array_input['notes'], FILTER_SANITIZE_STRING) : '');
					
					  
					  
					$no_of_openings_input = trim(isset($ea_received_rest_ws_raw_array_input['no_of_openings']) ? filter_var($ea_received_rest_ws_raw_array_input['no_of_openings'], FILTER_SANITIZE_NUMBER_INT) : '');
					 if ($no_of_openings_input == "") {
						 $no_of_openings_input = null;
					} 
					
					if ($max_experience_period_input == "") {
						$max_experience_period_input = null;
					} 
					
					$experience_period_name_input = '2';
					
					if ($min_experience_period_input == "") {
						$min_experience_period_input = null;
					} 
					
					if ($bill_rate_value_input == "") {
						$bill_rate_value_input = null;
					} 
					if ($maximum_allowed_submittals_count_input == "") {
						$maximum_allowed_submittals_count_input = 0;
					} 
					
					$expected_job_start_date_input = trim(isset($ea_received_rest_ws_raw_array_input['expected_job_start_date']) ? filter_var($ea_received_rest_ws_raw_array_input['expected_job_start_date'], FILTER_SANITIZE_STRING) : '');
					$expected_job_end_date_input = trim(isset($ea_received_rest_ws_raw_array_input['expected_job_end_date']) ? filter_var($ea_received_rest_ws_raw_array_input['expected_job_end_date'], FILTER_SANITIZE_STRING) : '');
					$resume_submission_end_date_input = trim(isset($ea_received_rest_ws_raw_array_input['resume_submission_end_date']) ? filter_var($ea_received_rest_ws_raw_array_input['resume_submission_end_date'], FILTER_SANITIZE_STRING) : '');
					$job_recruitment_status_input = trim(isset($ea_received_rest_ws_raw_array_input['job_recruitment_status']) ? filter_var($ea_received_rest_ws_raw_array_input['job_recruitment_status'], FILTER_SANITIZE_NUMBER_INT) : '');
					  
					//$resume_submission_end_date_input = $resume_submission_end_date_input . ":00";
					$resume_submission_end_date_epoch_input = df_convert_date_to_unix_timestamp($resume_submission_end_date_input);
					
					if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
					} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
					}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						

					
					  //invalid company_id scenario
					
					if ($company_id_input == "") {
				
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "missing-company-id";
						$response['status_description'] = "missing company id";
						
						$eventLog->log("missing-company-id: Please provide a valid Company id.");
						
					} else if ($company_client_id_input == "") {
						//Invalid Mobile Number scenario
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						//$response['status'] = "missing-company_client_id";
						$response['status'] = "missing-company-client-id";
						$response['status_description'] = "Missing Company client id";
						
						$eventLog->log("missing-company-client-id: Please provide a valid Company client id.");
						
					
					} else if ($job_title_input == "") {
						//Invalid Mobile Number scenario
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						//$response['status'] = "missing-job-title";
						$response['status'] = "missing-job-title";
						$response['status_description'] = "Missing job title";
						
						$eventLog->log("missing-job-title: Please provide a valid job title.");
						
					} else if ($job_full_description_input == "") {
						//Invalid Mobile Number scenario
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						//$response['status'] = "missing-job-title";
						$response['status'] = "missing-job-full-description";
						$response['status_description'] = "Missing job full description";
						
						$eventLog->log("missing-job-full-description: Please provide a valid job full description.");
				/* 	} else if ($no_of_openings_input == "") {
						//Invalid Mobile Number scenario
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						//$response['status'] = "missing-avialable-poistions.";
						$response['status'] = "missing-available-positions";
						$response['status_description'] = "Missing available positions";
						
						$eventLog->log("missing-available_positions: Please provide available positions.");	
					
					}else if ((is_null($job_type_id_input))||(is_null($job_type_name_input))||(is_null($job_type_seo_name_input))){
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-job-type-info";
						$response['status_description'] = "Invalid job Type Info provided, Please check and try again.";
						
						$eventLog->log("invalid-job-type-info: Invalid job Type Info provided, Please check and try again."); */
					
					/* } else if ((is_null($job_compensation_period_id_input))||(is_null($job_compensation_period_name_input))||(is_null($job_compensation_period_seo_name_input))) {
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-job-compensation-period-info";
						$response['status_description'] = "Invalid job compensation period Info provided, Please check and try again.";
						
						$eventLog->log("invalid-job-compensation-period-info: Invalid job compensation period Info provided, Please check and try again.");	 */
						
					/* } else if ((is_null($job_industry_id_input))||(is_null($job_industry_name_input))||(is_null($job_industry_seo_name_input))) {
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-job-industry-info";
						$response['status_description'] = "Invalid job industry Info provided, Please check and try again.";
						
						$eventLog->log("invalid-job-industry-info: Invalid job industry Info provided, Please check and try again.");
					
					} else if ((is_null($job_work_location_requirement_id_input))||(is_null($job_work_location_requirement_name_input))||(is_null($job_work_location_requirement_seo_name_input))) {
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-job-work-location-info";
						$response['status_description'] = "Invalid job work location Info provided, Please check and try again.";
						
						$eventLog->log("invalid-job-work-location-info: Invalid job work location Info provided, Please check and try again.");	
						
					} else if ($resume_submission_end_date_input == "") {
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-resume-submission-end-date-info";
						$response['status_description'] = "Invalid resume submission end date Info provided, Please check and try again.";
						
						$eventLog->log("invalid-resume-submission-end-date-info: Invalid resume submission end date Info provided, Please check and try again."); */	
					
					
					} else {	
						//All inputs are Valid
						
						$eventLog->log("All inputs are valid.");
						//$eventLog->log("Received Inputs - " . $content);
						
						$from_epoch = $current_epoch-$job_duplicate_check_timeout_time_in_seconds; 
						$job_posted_date_time_from_input = df_convert_unix_timestamp_to_datetime_custom_timezone($from_epoch, "Europe/London");
						
						
						$job_posted_date_time_to_input = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
						  
						$job_duplicate_check_result = job_details_duplicate_check_based_on_company_id($company_id_input,$company_client_id_input,$job_title_input,$job_posted_date_time_from_input,$job_posted_date_time_to_input);
						
						if (count($job_duplicate_check_result) > 0) {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "job-already-exists";
							$response['status_description'] = "A job already exists with this Name";
							
							$eventLog->log("job-already-exists: job already exists");						
						
						} else {
							$eventLog->log("no-duplicate-job-exists\n");	
							//Event Time, as per Indian Standard Time
							$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
							//$event_expiry_datetime_epoch = $current_epoch+$password_resetting_link_expiry_period;	
							//$valid_from_date_input = '2018-11-10 16:18:20';
							
							 $eventLog->log("before enter into add");
							$job_add_next_step = "";
							
							if ($ea_extracted_jwt_token_user_type == "admin") {
								
								if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
									$job_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("before company id condition");
								} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
									
									$job_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("after add condition");
								} else {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "insufficient-permissions-different-company-user-id";
									$response['status_description'] = "Insufficient Set of Permissions";
									
									//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
									header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
									$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
									
								}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								
								
								
							} else {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
								$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
							
							}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
								
							if ($job_add_next_step == "PROCEED-TO-NEXT-STEP") {
								try {
									
									$eventLog->log("company_id_input => ".$company_id_input);
									$eventLog->log("company_client_id_input => ".$company_client_id_input);
									$eventLog->log("job_title_input => ".$job_title_input);
									$eventLog->log("job_seo_title_input => ".$job_seo_title_input);
									$eventLog->log("job_summary_input => ".$job_summary_input);
									$eventLog->log("job_summary_safe_html => ".$job_summary_safe_html_input);
									$eventLog->log("job_full_description_input => ".$job_full_description_input);
									$eventLog->log("job_full_description_safe_html => ".$job_full_description_safe_html_input);
									$eventLog->log("job_type_id_input => ".$job_type_id_input);
									$eventLog->log("job_type_name_input => ".$job_type_name_input);
									$eventLog->log("job_type_seo_name_input => ".$job_type_seo_name_input);
									/* $eventLog->log("job_experience_level_id_input => ".$job_experience_level_id_input);
									$eventLog->log("job_experience_level_name_input => ".$job_experience_level_name_input);
									$eventLog->log("job_experience_level_seo_name_input => ".$job_experience_level_seo_name_input); */
									//$eventLog->log("currency_id_input => ".$currency_id_input);
									//$eventLog->log("currency_name_input => ".$currency_name_input);
									//$eventLog->log("currency_seo_name_input => ".$currency_seo_name_input);
									//$eventLog->log("currency_three_lettered_code_input => ".$currency_three_lettered_code_input);
									/* $eventLog->log("min_compensation_value_input => ".$min_compensation_value_input);
									$eventLog->log("max_compensation_value_input => ".$max_compensation_value_input);
									$eventLog->log("salary_display_options_input => ".$salary_display_options_input); */
									/* $eventLog->log("job_compensation_period_id_input => ".$job_compensation_period_id_input);
									$eventLog->log("job_compensation_period_name_input => ".$job_compensation_period_name_input);
									$eventLog->log("job_compensation_period_seo_name_input => ".$job_compensation_period_seo_name_input); */
									$eventLog->log("job_industry_id_input => ".$job_industry_id_input);
									$eventLog->log("job_industry_name_input => ".$job_industry_name_input);
									$eventLog->log("job_industry_seo_name_input => ".$job_industry_seo_name_input);
									$eventLog->log("job_work_location_requirement_id_input => ".$job_work_location_requirement_id_input);
									$eventLog->log("job_work_location_requirement_name_input => ".$job_work_location_requirement_name_input);
									$eventLog->log("job_work_location_requirement_seo_name_input => ".$job_work_location_requirement_seo_name_input);
									$eventLog->log("city_input => ".$city_input);
									$eventLog->log("state_input => ".$state_input);
									$eventLog->log("country_input => ".$country_input);
									//$eventLog->log("available_positions_input => ".$available_positions_input);
									$eventLog->log("resume_submission_end_date_input => ".$resume_submission_end_date_input);
									$eventLog->log("resume_submission_end_date_epoch_input => ".$resume_submission_end_date_epoch_input);
									$eventLog->log("job_posted_date_time_to_input => ".$job_posted_date_time_to_input);
									$eventLog->log("current_epoch => ".$current_epoch);
									
									$user_classification_details_result=ea_get_specific_user_classification_details($ea_extracted_jwt_token_sub);
									$user_classification_details_result_count=count($user_classification_details_result);
									 $eventLog->log("before condition of result");
									if ($user_classification_details_result_count > 0) {
										 $eventLog->log("after result condition");
										$user_classification_details_result_json_encoded = json_encode($user_classification_details_result);
										//$eventLog->log('user_classification_details_result_json_encoded: ' . $user_classification_details_result_json_encoded);
										$sm_site_member_classification_detail_id = $user_classification_details_result["sm_site_member_classification_detail_id"];
										$eventLog->log('sm_site_member_classification_detail_id: ' . $sm_site_member_classification_detail_id);
									}
									 $eventLog->log("before insert function");
									$last_inserted_id = /* job_basic_details_insert($company_id_input,$company_client_id_input,$sm_site_member_classification_detail_id,$ea_extracted_jwt_token_sub,$job_title_input,$job_seo_title_input,$job_summary_input,$job_summary_safe_html_input,$job_full_description_input,$job_full_description_safe_html_input,$job_type_id_input,$job_type_name_input,$job_type_seo_name_input,$min_experience_period_input,$pay_rate_min_value_input,$pay_rate_max_value_input,$salary_display_options_input,$job_industry_id_input,$job_industry_name_input,$job_industry_seo_name_input,$job_work_location_requirement_id_input,$job_work_location_requirement_name_input,$job_work_location_requirement_seo_name_input,$address_line_1_input,$address_line_2_input,$city_input,$state_input,$country_input,$zipcode_input,$notes_input,$no_of_openings_input,$resume_submission_end_date_input,$resume_submission_end_date_epoch_input,$job_posted_date_time_to_input,$current_epoch,$experience_period_name_input,$bill_rate_currency_three_lettered_code_input,$bill_rate_value_input,$pay_rate_currency_three_lettered_code_input,$expected_job_start_date_input,$expected_job_end_date_input,$bill_rate_job_compensation_period_id_input,$bill_rate_job_compensation_period_name_input,$bill_rate_job_compensation_period_seo_name_input,$pay_rate_job_compensation_period_id_input,$pay_rate_job_compensation_period_name_input,$pay_rate_job_compensation_period_seo_name_input,$maximum_allowed_submittals_count_input,$max_experience_period_input); */
									job_basic_details_insert($company_id_input,$company_client_id_input,$sm_site_member_classification_detail_id,$ea_extracted_jwt_token_sub,$job_title_input,$job_seo_title_input,$job_summary_input,$job_summary_safe_html_input,$job_full_description_input,$job_full_description_safe_html_input,$job_type_id_input,$job_type_name_input,$job_type_seo_name_input,$min_experience_period_input,$pay_rate_min_value_input,$pay_rate_max_value_input,$salary_display_options_input,$job_industry_id_input,$job_industry_name_input,$job_industry_seo_name_input,$job_work_location_requirement_id_input,$job_work_location_requirement_name_input,$job_work_location_requirement_seo_name_input,$address_line_1_input,$address_line_2_input,$city_input,$state_input,$country_input,$zipcode_input,$no_of_openings_input,$resume_submission_end_date_input,$resume_submission_end_date_epoch_input,$job_posted_date_time_to_input,$current_epoch,$experience_period_name_input,$bill_rate_currency_three_lettered_code_input,$bill_rate_value_input,$pay_rate_currency_three_lettered_code_input,$expected_job_start_date_input,$expected_job_end_date_input,$bill_rate_job_compensation_period_id_input,$bill_rate_job_compensation_period_name_input,$bill_rate_job_compensation_period_seo_name_input,$pay_rate_job_compensation_period_id_input,$pay_rate_job_compensation_period_name_input,$pay_rate_job_compensation_period_seo_name_input,$maximum_allowed_submittals_count_input,$max_experience_period_input,$job_recruitment_status_input);

									
										
									 $eventLog->log("after insert function");
									 
									 
									 
									
									 $eventLog->log($last_inserted_id);
									 $job_rel_data = array();
									 $job_rel_data["job_id"] = $last_inserted_id;
									 $job_rel_data["company_id"] = $company_id_input;
									 $job_rel_data["company_client_id"] = $company_client_id_input;
											
									
									
										if ($last_inserted_id != "") {
											
												$response['data'] = $job_rel_data;
												$response['status'] = "job-insertion-successful";
												$response['status_description'] = "A job is added Successfully.";
											
												$eventLog->log("job-insert-successful: A job is added Successfully.");   
											
										} else {
											//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
											$response['data'] = array();
											$response['status'] = "job-insertion-error";
											$response['status_description'] = "Error occurred when adding the job in the Database.";
											
											$eventLog->log("job-insert-error: There is an error, when adding the job in the Database.");
											
										}
										
									//close of else of if (count($user_ass_oc_validation_result) > 0) {
										
										 
										
									
								} catch (Exception $e) {
									
									//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
									$response['data'] = array();
									$response['status'] = "job-insertion-error";
									$response['status_description'] = "Error occurred when adding the job in the Database.";
									
									$eventLog->log("job-insertion-error: There is an error, when adding the job in the Database.");
								}
							}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
							
						}
					}//close of else of if ($email_id_input == "") {
					
				}//close of //close of if ($ea_is_user_page_access_authorized) {
					
				
			} else {
				
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "invalid-auth-token-submitted";
				$response['status_description'] = "Invalid Authentication";
				
				//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
				header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
				
				$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
				
			}//close of else of if ($ea_auth_token_validation_status) {
		
		}//close of if ($ea_maintenance_mode == false) {
		
	} else {

		//Construct Content, that will be sent in Response body, of the REST Web Service
		$response['data'] = array();
		$response['status'] = "invalid-input";
		$response['status_description'] = "Invalid Input, Please check and provide all information.";
		
		//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
		
		$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
				
		
		}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




	//Check if Maintenance Mode is Turned On
	if ($ea_maintenance_mode) {	
		
		//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
		//header('Maintenance-Progress: true', false);
		//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
		$response['data'] = array();
		$response['status'] = "application-maintenance-in-progress";
		$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
		
		$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
		
	} else {	

		//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
		header('Content-Type: application/json');
		echo json_encode($response,JSON_PRETTY_PRINT);
		
	}//close of else of if ($ea_maintenance_mode){
		

function job_basic_details_insert($company_id_input,$company_client_id_input,$sm_site_member_classification_detail_id,$ea_extracted_jwt_token_sub,$job_title_input,$job_seo_title_input,$job_summary_input,$job_summary_safe_html_input,$job_full_description_input,$job_full_description_safe_html_input,$job_type_id_input,$job_type_name_input,$job_type_seo_name_input,$min_experience_period_input,$pay_rate_min_value_input,$pay_rate_max_value_input,$salary_display_options_input,$job_industry_id_input,$job_industry_name_input,$job_industry_seo_name_input,$job_work_location_requirement_id_input,$job_work_location_requirement_name_input,$job_work_location_requirement_seo_name_input,$address_line_1_input,$address_line_2_input,$city_input,$state_input,$country_input,$zipcode_input,$no_of_openings_input,$resume_submission_end_date_input,$resume_submission_end_date_epoch_input,$job_posted_date_time_input,$current_epoch,$experience_period_name_input,$bill_rate_currency_three_lettered_code_input,$bill_rate_value_input,$pay_rate_currency_three_lettered_code_input,$expected_job_start_date_input,$expected_job_end_date_input,$bill_rate_job_compensation_period_id_input,$bill_rate_job_compensation_period_name_input,$bill_rate_job_compensation_period_seo_name_input,$pay_rate_job_compensation_period_id_input,$pay_rate_job_compensation_period_name_input,$pay_rate_job_compensation_period_seo_name_input,$maximum_allowed_submittals_count_input,$max_experience_period_input,$job_recruitment_status_input) {
	
	
	global $dbcon,$eventLog;
	$is_active_status = '1';
	$resume_acceptance_status = '1';
	//$job_recruitment_status = '1';
    
	$job_details_insert_sql = "INSERT INTO `jobs`(`company_id`, `company_client_id`, `job_posted_by_member_classification_detail_id`, `job_posted_by_sm_memb_id`, `job_title`, `job_seo_title`, `job_summary`, `job_summary_safe_html`, `job_full_description`, `job_full_description_safe_html`, `job_type_id`, `job_type_name`, `job_type_seo_name`, `min_experience_period`,`max_experience_period`, `experience_period_name`, `bill_rate_currency_three_lettered_code`, `bill_rate_value`, `bill_rate_job_compensation_period_id`, `bill_rate_job_compensation_period_name`, `bill_rate_job_compensation_period_seo_name`, `pay_rate_currency_three_lettered_code`, `pay_rate_min_value`, `pay_rate_max_value`, `pay_rate_job_compensation_period_id`, `pay_rate_job_compensation_period_name`, `pay_rate_job_compensation_period_seo_name`, `salary_display_options`, `maximum_allowed_submittals_count`, `expected_job_start_date`, `expected_job_end_date`, `job_industry_id`, `job_industry_name`, `job_industry_seo_name`, `job_work_location_requirement_id`, `job_work_location_requirement_name`, `job_work_location_requirement_seo_name`, `address_line_1`, `address_line_2`, `city`, `state`, `country`, `zipcode`, `no_of_openings`, `available_positions`, `resume_submission_end_date`, `resume_submission_end_date_epoch`, `resume_acceptance_status`, `job_recruitment_status`, `job_posted_date_time`, `job_posted_date_time_epoch`,`is_active_status`) VALUES(:company_id,:company_client_id,:job_posted_by_member_classification_detail_id,:sm_memb_id,:job_title,:job_seo_title,:job_summary,:job_summary_safe_html,:job_full_description,:job_full_description_safe_html,:job_type_id,:job_type_name,:job_type_seo_name,:min_experience_period,:max_experience_period,:experience_period_name,:bill_rate_currency_three_lettered_code,:bill_rate_value,:bill_rate_job_compensation_period_id,:bill_rate_job_compensation_period_name,:bill_rate_job_compensation_period_seo_name,:pay_rate_currency_three_lettered_code,:pay_rate_min_value,:pay_rate_max_value,:pay_rate_job_compensation_period_id,:pay_rate_job_compensation_period_name,:pay_rate_job_compensation_period_seo_name,:salary_display_options,:maximum_allowed_submittals_count,:expected_job_start_date,:expected_job_end_date,:job_industry_id,:job_industry_name,:job_industry_seo_name,:job_work_location_requirement_id,:job_work_location_requirement_name,:job_work_location_requirement_seo_name,:address_line_1,:address_line_2,:city,:state,:country,:zipcode,:no_of_openings,:available_positions,:resume_submission_end_date,:resume_submission_end_date_epoch,:resume_acceptance_status,:job_recruitment_status,:job_posted_date_time,:job_posted_date_time_epoch,:is_active_status)";

	$job_details_insert_query = $dbcon->prepare($job_details_insert_sql);
	$job_details_insert_query->bindValue(":company_id",$company_id_input);
	$eventLog->log("company_id_input->".$company_id_input);
	$job_details_insert_query->bindValue(":company_client_id",$company_client_id_input);
	$eventLog->log("company_client_id_input->".$company_client_id_input);
	$job_details_insert_query->bindValue(":job_posted_by_member_classification_detail_id",$sm_site_member_classification_detail_id);
	$eventLog->log("sm_site_member_classification_detail_id->".$sm_site_member_classification_detail_id);
	$job_details_insert_query->bindValue(":sm_memb_id",$ea_extracted_jwt_token_sub);
	$eventLog->log("sm_memb_id_input->".$ea_extracted_jwt_token_sub);
	
	$job_details_insert_query->bindValue(":job_title",$job_title_input);
	$eventLog->log("job_title_input->".$job_title_input);
	$job_details_insert_query->bindValue(":job_seo_title",$job_seo_title_input);
	$eventLog->log("job_seo_title_input->".$job_seo_title_input);
	$job_details_insert_query->bindValue(":job_summary",$job_summary_input);
	$eventLog->log("job_summary_input->".$job_summary_input);
	$job_details_insert_query->bindValue(":job_summary_safe_html",$job_summary_safe_html_input);
	$eventLog->log("job_summary_safe_html_input->".$job_summary_safe_html_input);
	$job_details_insert_query->bindValue(":job_full_description",$job_full_description_input);
	$eventLog->log("job_full_description_input->".$job_full_description_input);
	$job_details_insert_query->bindValue(":job_full_description_safe_html",$job_full_description_safe_html_input);
	$eventLog->log("job_full_description_safe_html_input->".$job_full_description_safe_html_input);
	
    $job_details_insert_query->bindValue(":job_type_id",$job_type_id_input);
	$eventLog->log("job_type_id_input->".$job_type_id_input);
	$job_details_insert_query->bindValue(":job_type_name",$job_type_name_input);
	$eventLog->log("job_type_name_input->".$job_type_name_input);
	$job_details_insert_query->bindValue(":job_type_seo_name",$job_type_seo_name_input);
	$eventLog->log("job_type_seo_name_input->".$job_type_seo_name_input);
	
	$job_details_insert_query->bindValue(":min_experience_period",$min_experience_period_input);
	$eventLog->log("min_experience_period_input->".$min_experience_period_input);
	$job_details_insert_query->bindValue(":max_experience_period",$max_experience_period_input);
	$eventLog->log("max_experience_period_input->".$max_experience_period_input);
	$job_details_insert_query->bindValue(":experience_period_name",$experience_period_name_input);
	$eventLog->log("experience_period_name_input->".$experience_period_name_input);
	
	$job_details_insert_query->bindValue(":bill_rate_currency_three_lettered_code",$bill_rate_currency_three_lettered_code_input);
	$eventLog->log("bill_rate_currency_three_lettered_code_input->".$bill_rate_currency_three_lettered_code_input);
	$job_details_insert_query->bindValue(":bill_rate_value",$bill_rate_value_input);
	$eventLog->log("bill_rate_value_input->".$bill_rate_value_input);
	$job_details_insert_query->bindValue(":pay_rate_currency_three_lettered_code",$pay_rate_currency_three_lettered_code_input);
	$eventLog->log("pay_rate_currency_three_lettered_code_input->".$pay_rate_currency_three_lettered_code_input);
	
	$job_details_insert_query->bindValue(":bill_rate_job_compensation_period_id",$bill_rate_job_compensation_period_id_input);
	$eventLog->log("bill_rate_job_compensation_period_id_input->".$bill_rate_job_compensation_period_id_input);
	$job_details_insert_query->bindValue(":bill_rate_job_compensation_period_name",$bill_rate_job_compensation_period_name_input);
	$eventLog->log("bill_rate_job_compensation_period_name_input->".$bill_rate_job_compensation_period_name_input);

	
	$job_details_insert_query->bindValue(":bill_rate_job_compensation_period_seo_name",$bill_rate_job_compensation_period_seo_name_input);
	$eventLog->log("bill_rate_job_compensation_period_seo_name_input->".$bill_rate_job_compensation_period_seo_name_input);
	$job_details_insert_query->bindValue(":pay_rate_job_compensation_period_id",$pay_rate_job_compensation_period_id_input);
	$eventLog->log("pay_rate_job_compensation_period_id_input->".$pay_rate_job_compensation_period_id_input);
	$job_details_insert_query->bindValue(":pay_rate_job_compensation_period_name",$pay_rate_job_compensation_period_name_input);
	$eventLog->log("pay_rate_job_compensation_period_name_input->".$pay_rate_job_compensation_period_name_input);
	$job_details_insert_query->bindValue(":pay_rate_job_compensation_period_seo_name",$pay_rate_job_compensation_period_seo_name_input);
	$eventLog->log("pay_rate_job_compensation_period_seo_name_input->".$pay_rate_job_compensation_period_seo_name_input);

	$job_details_insert_query->bindValue(":maximum_allowed_submittals_count",$maximum_allowed_submittals_count_input);
	$eventLog->log("maximum_allowed_submittals_count_input->".$maximum_allowed_submittals_count_input);
	$job_details_insert_query->bindValue(":expected_job_start_date",$expected_job_start_date_input);
	$eventLog->log("expected_job_start_date_input->".$expected_job_start_date_input);
	$job_details_insert_query->bindValue(":expected_job_end_date",$expected_job_end_date_input);
	$eventLog->log("expected_job_end_date_input->".$expected_job_end_date_input);

	$job_details_insert_query->bindValue(":pay_rate_min_value",$pay_rate_min_value_input);
	$eventLog->log("pay_rate_min_value_input->".$pay_rate_min_value_input);
	$job_details_insert_query->bindValue(":pay_rate_max_value",$pay_rate_max_value_input);
	$eventLog->log("pay_rate_max_value_input->".$pay_rate_max_value_input);
	$job_details_insert_query->bindValue(":salary_display_options",$salary_display_options_input);
	$eventLog->log("salary_display_options_input->".$salary_display_options_input);

	$job_details_insert_query->bindValue(":job_industry_id",$job_industry_id_input);
	$eventLog->log("job_industry_id_input->".$job_industry_id_input);
	$job_details_insert_query->bindValue(":job_industry_name",$job_industry_name_input);
	$eventLog->log("job_industry_name_input->".$job_industry_name_input);
	$job_details_insert_query->bindValue(":job_industry_seo_name",$job_industry_seo_name_input);
	$eventLog->log("job_industry_seo_name_input->".$job_industry_seo_name_input);
	$job_details_insert_query->bindValue(":job_work_location_requirement_id",$job_work_location_requirement_id_input);
	$eventLog->log("job_work_location_requirement_id_input->".$job_work_location_requirement_id_input);
	$job_details_insert_query->bindValue(":job_work_location_requirement_name",$job_work_location_requirement_name_input);
	$eventLog->log("job_work_location_requirement_name_input->".$job_work_location_requirement_name_input);
	$job_details_insert_query->bindValue(":job_work_location_requirement_seo_name",$job_work_location_requirement_seo_name_input);
	$eventLog->log("job_work_location_requirement_seo_name_input->".$job_work_location_requirement_seo_name_input);
	$job_details_insert_query->bindValue(":address_line_1",$address_line_1_input);
	$eventLog->log("address_line_1_input->".$address_line_1_input);
	$job_details_insert_query->bindValue(":address_line_2",$address_line_2_input);
	$eventLog->log("address_line_2_input->".$address_line_2_input);
	$job_details_insert_query->bindValue(":city",$city_input);
	$eventLog->log("city_input->".$city_input);
	$job_details_insert_query->bindValue(":state",$state_input);
	$eventLog->log("state_input->".$state_input);
	$job_details_insert_query->bindValue(":country",$country_input);
	$eventLog->log("country_input->".$country_input);
	$job_details_insert_query->bindValue(":zipcode",$zipcode_input);
	$eventLog->log("zipcode_input->".$zipcode_input);
	/* $job_details_insert_query->bindValue(":notes",$notes_input);
	$eventLog->log("notes_input->".$notes_input); */
	$job_details_insert_query->bindValue(":no_of_openings",$no_of_openings_input);
	$eventLog->log("no_of_openings->".$no_of_openings_input);
	$job_details_insert_query->bindValue(":available_positions",$no_of_openings_input);
	$eventLog->log("available_positions_input->".$no_of_openings_input);
	$job_details_insert_query->bindValue(":resume_submission_end_date",$resume_submission_end_date_input);
	$eventLog->log("resume_submission_end_date_input->".$resume_submission_end_date_input);
	$job_details_insert_query->bindValue(":resume_submission_end_date_epoch",$resume_submission_end_date_epoch_input);
	$eventLog->log("resume_submission_end_date_epoch_input->".$resume_submission_end_date_epoch_input);
	$job_details_insert_query->bindValue(":job_posted_date_time",$job_posted_date_time_input);
	$eventLog->log("job_posted_date_time_input->".$job_posted_date_time_input);
	$job_details_insert_query->bindValue(":job_posted_date_time_epoch",$current_epoch);
	$eventLog->log("job_posted_date_time_epoch_input->".$current_epoch);
	$job_details_insert_query->bindValue(":is_active_status",$is_active_status);
	$eventLog->log("is_active_status->".$is_active_status);
	$job_details_insert_query->bindValue(":resume_acceptance_status",$resume_acceptance_status);
	$eventLog->log("resume_acceptance_status->".$resume_acceptance_status);
	$job_details_insert_query->bindValue(":job_recruitment_status",$job_recruitment_status_input);
	$eventLog->log("job_recruitment_status->".$job_recruitment_status_input);
	
       $eventLog->log("after bind value.");
	if ($job_details_insert_query->execute()) {
          $eventLog->log("after execute.");
            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");
           
			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}
	exit;
	?>