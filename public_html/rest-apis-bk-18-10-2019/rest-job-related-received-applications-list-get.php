<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {

	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
			    $job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
			    $classified_user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['classified_user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['classified_user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
			    $page_number_input = trim(isset($ea_received_rest_ws_raw_array_input['page_number']) ? filter_var($ea_received_rest_ws_raw_array_input['page_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				$number_of_records_input = trim(isset($ea_received_rest_ws_raw_array_input['number_of_records']) ? filter_var($ea_received_rest_ws_raw_array_input['number_of_records'], FILTER_SANITIZE_NUMBER_INT) : '');
				$sort_field_input = trim(isset($ea_received_rest_ws_raw_array_input['sort_field']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_field'], FILTER_SANITIZE_STRING) : '');
				$sort_order_input = trim(isset($ea_received_rest_ws_raw_array_input['sort_order']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_order'], FILTER_SANITIZE_STRING) : '');
				$eventLog->log($sort_order_input);
				$search_criteria_input = trim(isset($ea_received_rest_ws_raw_array_input['search_criteria']) ? filter_var($ea_received_rest_ws_raw_array_input['search_criteria'], FILTER_SANITIZE_STRING) : '');
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {

                if ($job_id_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-id";
					$response['status_description'] = "Missing Job Id";
					
					$eventLog->log("missing-user-id: Missing Job Id.");
					
				/* } else if ($company_id_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company Id";
					
					$eventLog->log("missing-company-id: Missing Company Id."); */
					
				} else if ($page_number_input == "0") {
					//Invalid Page Number scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-page-number";
					$response['status_description'] = "Invalid Page Number info submitted, please check and try again.";
					
					$eventLog->log("invalid-page-number: Invalid Page Number info submitted, please check and try again.");
					
				} else if (($sort_order_input != "ASC") && ($sort_order_input != "DESC") && ($sort_order_input != "")) {
					//Invalid Sorting Order scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-Sorting-order";
					$response['status_description'] = "Invalid Sorting Order info submitted, please check and try again.";
					
					$eventLog->log("invalid-Sorting-order: Invalid Sorting Order info submitted, please check and try again.");				
				
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else {
					try {
						
						if ($ea_extracted_jwt_token_user_company_id == "") {
							//action taker is of platform scope - super admin / site admin scenario
							$job_related_received_applications_list_info_result = get_job_related_received_applications_list_info_with_pagination_inputs($company_id_input,$job_id_input,$classified_user_id_input,$page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input);
							
						
						} else {
						   //action taker is of company scope - company admin, legal / immigration, internal admin etc scenario
							$job_related_received_applications_list_info_result = get_job_related_received_applications_list_info_with_pagination_inputs($ea_extracted_jwt_token_user_company_id,$job_id_input,$classified_user_id_input,$page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input);
							//get_candidates_list_with_pagination_inputs($ea_extracted_jwt_token_user_company_id,$sm_user_status_input, $sm_user_type_input, $page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input);
						   
						}
						
						$job_related_received_applications_list_info_result_count = count($job_related_received_applications_list_info_result);
						
						$eventLog->log("Count -> " . $job_related_received_applications_list_info_result_count);
						
						if (count($job_related_received_applications_list_info_result) > 0) {
							
						$response['data'] = $job_related_received_applications_list_info_result;
						$response['status'] = "job-related-received-applications-list-successfully-fetched";
						$response['status_description'] = "Job related received applications List Successfully Received";
					
						} else {
							
							$response['data'] = array();
							$response['status'] = "no-received-applications-found";
							$response['status_description'] = "No received applications found for this job";
							
						}
							
							
					} catch (Exception $e){
						
							
						$response['data'] = array();
						$response['status'] = "job-related-received-applications-list-fetching-error";
						$response['status_description'] = "Job related received applications List Fetching Error";
					}
					
					
				}
					
			}
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
	
/* function candiate_invited_jobs_list_info($company_id_input,$user_id_input) {
    global $dbcon;
	$constructed_array = array();
	
	$candiate_invited_jobs_list_info_sql = "SELECT * FROM `job_applicant_invites` jai JOIN `jobs` j  ON jai.job_id=j.job_id WHERE j.company_id=:company_id AND jai.invitation_received_by_user_id=:invitation_received_by_user_id ";
	
	$candiate_invited_jobs_list_info_query = $dbcon->prepare($candiate_invited_jobs_list_info_sql);
	$candiate_invited_jobs_list_info_query->bindValue(":company_id",$company_id_input);
    $candiate_invited_jobs_list_info_query->bindValue(":invitation_received_by_user_id",$user_id_input);	
	$candiate_invited_jobs_list_info_query->execute(); 
	
	if($candiate_invited_jobs_list_info_query->rowCount() > 0) {
		$candiate_invited_jobs_list_info_query_result = $candiate_invited_jobs_list_info_query->fetchAll();
		foreach ($candiate_invited_jobs_list_info_query_result as $candiate_invited_jobs_list_info_query_result_row) {

			$temp_row_array = array();
			$temp_row_array["job_applicant_invite_id"] = $candiate_invited_jobs_list_info_query_result_row["job_applicant_invite_id"];
		    $temp_row_array["company_id"] = $candiate_invited_jobs_list_info_query_result_row["company_id"];
		    $temp_row_array["company_client_id"] = $candiate_invited_jobs_list_info_query_result_row["company_client_id"];
			$temp_row_array["job_id"] = $candiate_invited_jobs_list_info_query_result_row["job_id"];
			$temp_row_array["job_title"] = $candiate_invited_jobs_list_info_query_result_row["job_title"];
			
		    $temp_row_array["invitation_sent_by_user_id"] = $candiate_invited_jobs_list_info_query_result_row["invitation_sent_by_user_id"];
			$basic_details_result = user_basic_details_check_based_on_user_id($temp_row_array["invitation_sent_by_user_id"]);
			$temp_row_array["invitation_sent_by_user_firstname"] = $basic_details_result["sm_firstname"];
			$temp_row_array["invitation_sent_by_user_lastname"] = $basic_details_result["sm_lastname"];
			$temp_row_array["invitation_initiate_at"] = $candiate_invited_jobs_list_info_query_result_row["event_date_time"];
			$temp_row_array["invite_email_sent_at"] = $candiate_invited_jobs_list_info_query_result_row["email_sent_date_time"];
			$temp_row_array["invite_sms_sent_at"] = $candiate_invited_jobs_list_info_query_result_row["sms_sent_date_time"];
			$temp_row_array["invitation_fulfillment_status"] = $candiate_invited_jobs_list_info_query_result_row["invite_fulfillment_status"];
			$temp_row_array["status"] = $candiate_invited_jobs_list_info_query_result_row["is_active_status"];
			$temp_row_array["invite_ref_code"] = $candiate_invited_jobs_list_info_query_result_row["invite_ref_code"];
	        $constructed_array[] = $temp_row_array;
	    }
		return $constructed_array;
	}
	return $constructed_array;
} */

function get_job_related_received_applications_list_info_with_pagination_inputs($company_id_input,$job_id_input,$classified_user_id_input,$page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input) {

    global $dbcon, $default_number_of_records_pagination; //app/core/main-config.php
	global $ea_extracted_jwt_token_sub,$current_epoch,$expiring_link_lifetime, $expiring_link_secret_key, $expiring_link_hash_algorithm, $site_hostname_value,$eventLog;
	$constructed_array = array();
	
	$eventLog->log("before is_null concept");
	$eventLog->log("sort_order_input");
	$eventLog->log($sort_order_input);
	$eventLog->log("sort_field_input");
	$eventLog->log($sort_field_input);
	

	if ($search_criteria_input == "") {
		$search_criteria_input = null;
	}//close of if ($search_criteria_input == "") {
	if ($sort_field_input == "") {
		$sort_field_input = null;
	}	
	if ($classified_user_id_input == "") {
		
		$classified_user_add_sub_query = "";
		
	} else {
		
		$classified_user_add_sub_query = " AND jari.added_by = :classified_user_id ";
	}
	

	if (!is_null($search_criteria_input)) {
		//When Search Criteria input is not empty
		$search_criteria_variable = "%" . $search_criteria_input . "%";
		
		$search_criteria_in_query_with_where_keyword = " WHERE ((jari.job_id LIKE :job_id_search_keyword) OR (jari.company_client_id LIKE :company_client_id_search_keyword) OR (sm.sm_firstname LIKE :candidate_first_name_search_keyword) OR (sm.sm_middlename LIKE :candidate_middle_name_search_keyword) OR (sm.sm_lastname LIKE :candidate_last_name_search_keyword) OR (jari.current_ctc LIKE :current_ctc_search_keyword) OR (jari.expected_ctc LIKE :expected_ctc_search_keyword) OR 
		(jari.is_salary_negotiable LIKE :is_salary_negotiable_search_keyword) OR (jari.resume_generated_file_name LIKE :cover_letter_search_keyword) OR 
		(jari.event_date_time LIKE :applied_at_search_keyword)) ";
		
		$eventLog->log("after search criteria concept");
		$search_criteria_in_query_without_where_keyword = " AND ((jari.job_id LIKE :job_id_search_keyword) OR (jari.company_client_id LIKE :company_client_id_search_keyword) OR (sm.sm_firstname LIKE :candidate_first_name_search_keyword) OR (sm.sm_middlename LIKE :candidate_middle_name_search_keyword) OR (sm.sm_lastname LIKE :candidate_last_name_search_keyword) OR (jari.current_ctc LIKE :current_ctc_search_keyword) OR (jari.expected_ctc LIKE :expected_ctc_search_keyword) OR 
		(jari.is_salary_negotiable LIKE :is_salary_negotiable_search_keyword) OR (jari.resume_generated_file_name LIKE :cover_letter_search_keyword) OR 
		(jari.event_date_time LIKE :applied_at_search_keyword)) ";
	} else {
		//When Search Criteria input is empty
		$search_criteria_in_query_with_where_keyword = " WHERE ";

		$search_criteria_in_query_without_where_keyword = "";

	}//close of else of if (!is_null($search_criteria_input)) {
	
	
	if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {
			$eventLog->log("inside if condition of sort order concept");
        
        
		
		if (!is_null($sort_field_input)) {
			//When the Sort Field input is not empty
			$eventLog->log("inside if condition of sort field concept");

			if ($sort_field_input == "job_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY jari.job_id " . $sort_order_input;

			} else if ($sort_field_input == "company_client_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY cc.company_client_id " . $sort_order_input;

			} else if ($sort_field_input == "candidate_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_firstname " . $sort_order_input;

			} else if ($sort_field_input == "current_ctc") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY jari.current_ctc " . $sort_order_input;

			} else if ($sort_field_input == "expected_ctc") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY jari.expected_ctc " . $sort_order_input;

			} else if ($sort_field_input == "is_salary_negotiable") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY jari.is_salary_negotiable " . $sort_order_input;

			} else if ($sort_field_input == "cover_letter") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY jari.resume_generated_file_name " . $sort_order_input;

			}else if ($sort_field_input == "applied_at") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY jari.event_date_time " . $sort_order_input;

			}
             $eventLog->log("after sort order input");
		} else {
			//When the Sort Field input is empty, primary key has to be taken, with provided Sorting Order input
			$sort_details_in_query = " ORDER BY jari.job_applicant_request_info_id " . $sort_order_input;
            
			$eventLog->log($sort_details_in_query);
		}//close of if (!is_null($sort_field_input)) {


	} else {
		$eventLog->log(" else condition of after sort order concept");
		//When the Sort Field input is empty, primary key has to be taken, with ASC Order, by default
		$sort_details_in_query = " ORDER BY jari.job_applicant_request_info_id ASC ";
		$eventLog->log($sort_details_in_query);

	}//close of else of if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {
 

	$limit_offset_value = 0;

	if (($page_number_input == "") && ($number_of_records_input == "")) {
		//Give All Data
		//No LIMIT Clause in the Query
		$limit_offset_in_query = "";

	} else if (($page_number_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than page_number_input, till the number of records, as defined in the $default_number_of_records_pagination

		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $default_number_of_records_pagination;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$default_number_of_records_pagination;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $default_number_of_records_pagination;
		}//close of else if of if ($page_number_input == "1") {



	} else if (($page_number_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records
		$limit_offset_in_query = " LIMIT " . $number_of_records_input;

	} else {
		//Give Data, from given Record, till the requested number of records
		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $number_of_records_input;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$number_of_records_input;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $number_of_records_input;
		}//close of else if of if ($page_number_input == "1") {

	}//close of else of if (($page_number_input == "") && ($number_of_records_input == "")) {
		
    $eventLog->log("before search crieteria null concept");
    if (!is_null($search_criteria_input)) {
			
			
		    $job_related_received_applications_named_parameters_values_array_input = array(":company_id" => $company_id_input,":job_id" => $job_id_input,":job_id_search_keyword" =>$search_criteria_variable,":company_client_id_search_keyword" => $search_criteria_variable,":candidate_first_name_search_keyword" => $search_criteria_variable,
			":candidate_middle_name_search_keyword" => $search_criteria_variable,":candidate_last_name_search_keyword" => $search_criteria_variable,
			":current_ctc_search_keyword" => $search_criteria_variable, ":expected_ctc_search_keyword" => $search_criteria_variable, ":is_salary_negotiable_search_keyword" => $search_criteria_variable,":cover_letter_search_keyword" => $search_criteria_variable, ":applied_at_search_keyword" => $search_criteria_variable);    

			if($classified_user_id_input != ""){
				$job_related_received_applications_named_parameters_values_array_input[":classified_user_id"] = $classified_user_id_input;
			}
			
			$job_related_received_applications_list_count_get_sql = "SELECT COUNT(*) AS count FROM `job_applicant_requests_info` jari JOIN `candidate_rel_uploaded_resumes` crur  ON jari.job_applied_by_user_id = crur. sm_memb_id JOIN `jobs` j  ON jari.job_id=j.job_id  JOIN `company_clients` cc ON jari.company_client_id = cc.company_client_id JOIN `site_members` sm  ON jari.job_applied_by_user_id = sm.sm_memb_id WHERE jari.company_id=:company_id AND jari.job_id=:job_id " . $classified_user_add_sub_query . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;
			
			
			
			
			$eventLog->log($job_related_received_applications_list_count_get_sql);
			$job_related_received_applications_list_count_get_select_query = $dbcon->prepare($job_related_received_applications_list_count_get_sql);
			$job_related_received_applications_list_count_get_select_query->execute($job_related_received_applications_named_parameters_values_array_input);

			//Get Jobs List
			
			$job_related_received_applications_list_info_sql = "SELECT * FROM `job_applicant_requests_info` jari JOIN `candidate_rel_uploaded_resumes` crur  ON jari.job_applied_by_user_id = crur. sm_memb_id JOIN `jobs` j  ON jari.job_id=j.job_id  JOIN `company_clients` cc ON jari.company_client_id = cc.company_client_id JOIN `site_members` sm  ON jari.job_applied_by_user_id = sm.sm_memb_id  WHERE jari.company_id=:company_id AND jari.job_id=:job_id" . $classified_user_add_sub_query .$search_criteria_in_query_without_where_keyword . $sort_details_in_query . $limit_offset_in_query;
            $eventLog->log($job_related_received_applications_list_info_sql);
			$job_related_received_applications_list_get_select_query = $dbcon->prepare($job_related_received_applications_list_info_sql);
			$job_related_received_applications_list_get_select_query->execute($job_related_received_applications_named_parameters_values_array_input);

	} else {
			
			$job_related_received_applications_named_parameters_values_array_input = array(":company_id" => $company_id_input,":job_id" => $job_id_input);
			
			if($classified_user_id_input != ""){
				$job_related_received_applications_named_parameters_values_array_input[":classified_user_id"] = $classified_user_id_input;
			}
			
			//$candiate_invited_jobs_list_count_get_sql = "SELECT COUNT(*) FROM `job_applicant_invites` jai JOIN `jobs` j  ON jai.job_id=j.job_id WHERE j.company_id=:company_id AND jai.invitation_received_by_user_id=:invitation_received_by_user_id " . $sort_details_in_query;
			$job_related_received_applications_list_count_get_sql = "SELECT COUNT(*) AS count FROM `job_applicant_requests_info` jari JOIN `candidate_rel_uploaded_resumes` crur  ON jari.job_applied_by_user_id = crur. sm_memb_id JOIN `jobs` j  ON jari.job_id=j.job_id  JOIN `company_clients` cc ON jari.company_client_id = cc.company_client_id JOIN `site_members` sm  ON jari.job_applied_by_user_id = sm.sm_memb_id  WHERE jari.company_id=:company_id AND jari.job_id=:job_id" . $classified_user_add_sub_query . $sort_details_in_query;
			$eventLog->log($job_related_received_applications_list_count_get_sql);
			$job_related_received_applications_list_count_get_select_query = $dbcon->prepare($job_related_received_applications_list_count_get_sql);
			$job_related_received_applications_list_count_get_select_query->execute($job_related_received_applications_named_parameters_values_array_input);

			//Give List of Jobs, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			//$candiate_invited_jobs_list_info_sql = "SELECT * FROM `job_applicant_invites` jai JOIN `jobs` j  ON jai.job_id=j.job_id WHERE j.company_id=:company_id AND jai.invitation_received_by_user_id=:invitation_received_by_user_id " . $sort_details_in_query . $limit_offset_in_query;
			$job_related_received_applications_list_info_sql = "SELECT * FROM `job_applicant_requests_info` jari JOIN `candidate_rel_uploaded_resumes` crur  ON jari.job_applied_by_user_id = crur. sm_memb_id JOIN `jobs` j  ON jari.job_id=j.job_id  JOIN `company_clients` cc ON jari.company_client_id = cc.company_client_id JOIN `site_members` sm  ON jari.job_applied_by_user_id = sm.sm_memb_id WHERE jari.company_id=:company_id AND jari.job_id=:job_id " .$classified_user_add_sub_query . $sort_details_in_query . $limit_offset_in_query;
			$job_related_received_applications_list_get_select_query = $dbcon->prepare($job_related_received_applications_list_info_sql);
			$job_related_received_applications_list_get_select_query->execute($job_related_received_applications_named_parameters_values_array_input);

	}//close of else of if (!is_null($search_criteria_input)) {

	

	//Process / Get Companies Count
	if($job_related_received_applications_list_count_get_select_query->rowCount() > 0) {
	    $job_related_received_applications_list_count_get_select_query_result = $job_related_received_applications_list_count_get_select_query->fetch();
	    //print_r($jobs_list_count_get_select_query_result);

		//$total_candiate_considred_proposals_count = $candiate_considered_proposals_list_count_get_select_query_result["count"];
		$total_job_related_received_applications_count = $job_related_received_applications_list_count_get_select_query_result["count"];
		$constructed_array["total_records_count"] = $total_job_related_received_applications_count;

	}//close of if($jobs_list_count_get_select_query->rowCount() > 0) {

	//Process / Fetch Companies List
	if($job_related_received_applications_list_get_select_query->rowCount() > 0) {
	    $job_related_received_applications_list_get_select_query_result = $job_related_received_applications_list_get_select_query->fetchAll();
	    //print_r($companies_list_get_select_query_result);

		foreach ($job_related_received_applications_list_get_select_query_result as $job_related_received_applications_list_get_select_query_result_row) {

			$temp_row_array = array();
			$temp_row_array["job_applicant_request_info_id"] = $job_related_received_applications_list_get_select_query_result_row["job_applicant_request_info_id"];
		    $temp_row_array["company_id"] = $job_related_received_applications_list_get_select_query_result_row["company_id"];
		    $temp_row_array["company_client_id"] = $job_related_received_applications_list_get_select_query_result_row["company_client_id"];
			$temp_row_array["company_client_name"] = $job_related_received_applications_list_get_select_query_result_row["client_company_name"];
			$temp_row_array["job_id"] = $job_related_received_applications_list_get_select_query_result_row["job_id"];
			$temp_row_array["job_title"] = $job_related_received_applications_list_get_select_query_result_row["job_title"];
			$temp_row_array["candidate_name"] = $job_related_received_applications_list_get_select_query_result_row["sm_firstname"];
			$temp_row_array["current_ctc"] = $job_related_received_applications_list_get_select_query_result_row["current_ctc_amount"];
			$temp_row_array["expected_ctc"] = $job_related_received_applications_list_get_select_query_result_row["expected_ctc_amount"];
			$temp_row_array["is_salary_negotiable"] = $job_related_received_applications_list_get_select_query_result_row["is_salary_negotiable"];
			$temp_row_array["cover_letter"] = $job_related_received_applications_list_get_select_query_result_row["generated_filename"];
			$temp_row_array["applied_at"] = $job_related_received_applications_list_get_select_query_result_row["event_date_time"];
			$temp_row_array["crur_id"] = $job_related_received_applications_list_get_select_query_result_row["crur_id"];
			$temp_row_array["sm_memb_id"] = $job_related_received_applications_list_get_select_query_result_row["sm_memb_id"];
			
			$temp_row_array["candidature_status"] = applicant_screening_status_get($temp_row_array["job_applicant_request_info_id"],$temp_row_array["job_id"]);
			
			$link_expiry_time = $current_epoch+$expiring_link_lifetime;
			$uploaded_file_link_with_signature_result = create_uploaded_file_link_with_signature($ea_extracted_jwt_token_sub, $temp_row_array["sm_memb_id"], $temp_row_array["crur_id"], "https", $site_hostname_value, $link_expiry_time, $expiring_link_hash_algorithm, $expiring_link_secret_key);
			if (isset($uploaded_file_link_with_signature_result["uploaded_file_url"])) {
			$temp_row_array["uploaded_file_url"] = $uploaded_file_link_with_signature_result["uploaded_file_url"];
				
			} else {
				$temp_row_array["uploaded_file_url"] = null;
			}

			
			
			
	       // $constructed_array[] = $temp_row_array;
			$constructed_array["list"][] = $temp_row_array;
	    }
		
		return $constructed_array;
			
	}//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {
	return $constructed_array;
}
function applicant_screening_status_get($job_applicant_request_info_id,$job_id) {
	global $dbcon;
	
	$job_screening_levels_count_result = job_screening_levels_count_get($job_id);
	$job_screening_levels_count = $job_screening_levels_count_result["count"];
	$applicant_job_screening_levels_count_result = applicant_job_screening_levels_count_get($job_applicant_request_info_id,$job_id);
	$applicant_job_screening_levels_count = $applicant_job_screening_levels_count_result["count"];
	
	if($job_screening_levels_count == 0) {
		$candidature_status = "screening-levels-not-defined";
		return $candidature_status;
		
	} else {
		
		if($applicant_job_screening_levels_count == 0 ) {
			
			$candidature_status = "screening-levels-not-defined";
			return $candidature_status;
			
		} else { 
				
				if(count($applicant_job_screening_levels_get_result) > 0)  {
				
				  $candidature_status = "selected";
				  return $candidature_status;
				  
				} 
			//if($job_screening_levels_count > $applicant_job_screening_levels_count) {
				
				$event_status = "7";
				$applicant_job_screening_levels_get_result = applicant_job_screening_levels_get($job_applicant_request_info_id,$job_id,$event_status);
				
				$event_status = "6";
				$applicant_job_screening_levels_get_result = applicant_job_screening_levels_get($job_applicant_request_info_id,$job_id,$event_status);
				
				if(count($applicant_job_screening_levels_get_result) > 0)  {
				
				  $candidature_status = "rejected";
				  return $candidature_status;
				  
				}
				$event_status = "5";
				$applicant_job_screening_levels_get_result = applicant_job_screening_levels_get($job_applicant_request_info_id,$job_id,$event_status);
				
				if(count($applicant_job_screening_levels_get_result) > 0)  {
				
				  $candidature_status = "on-hold";
				  return $candidature_status;
				  
				}
				$event_status = "4";
				$applicant_job_screening_levels_get_result = applicant_job_screening_levels_get($job_applicant_request_info_id,$job_id,$event_status);
				
				if(count($applicant_job_screening_levels_get_result) > 0)  {
				
				  $candidature_status = "shortlisted";
				  return $candidature_status;
				  
				}
				$event_status = "1";
				$applicant_job_screening_levels_get_result = applicant_job_screening_levels_get($job_applicant_request_info_id,$job_id,$event_status);
				
				if(count($applicant_job_screening_levels_get_result) > 0)  {
				
				  $candidature_status = "scheduled";
				  return $candidature_status;
				  
				}
				
			
			
		    }
	}	
	
	return "";
	
}

function job_screening_levels_count_get($job_id) {
	global $dbcon;
	$constructed_array = array();
	
	$job_screening_levels_count_get_sql = "SELECT COUNT(*) AS count FROM `jsl_info` WHERE `job_id`=:job_id";
	$job_screening_levels_count_get_query = $dbcon->prepare($job_screening_levels_count_get_sql);
    $job_screening_levels_count_get_query->bindValue(":job_id",$job_id);


	$job_screening_levels_count_get_query->execute();

	if($job_screening_levels_count_get_query->rowCount() > 0) {
		$job_screening_levels_count_get_query_result = $job_screening_levels_count_get_query->fetch();
	     return $job_screening_levels_count_get_query_result;

	}//close of if($company_office_addresses_details_select_query_result->rowCount() > 0) {
	return $constructed_array;
	
}

function applicant_job_screening_levels_count_get($job_applicant_request_info_id,$job_id) {
	global $dbcon;
	$constructed_array = array();
	
	$applicant_job_screening_levels_count_get_sql = "SELECT COUNT(*) AS count FROM `jsl_applicant_evaluation_info` WHERE `job_id`=:job_id AND `job_applicant_request_info_id` =:job_applicant_request_info_id";
	$applicant_job_screening_levels_count_get_query = $dbcon->prepare($applicant_job_screening_levels_count_get_sql);
    $applicant_job_screening_levels_count_get_query->bindValue(":job_id",$job_id);
	$applicant_job_screening_levels_count_get_query->bindValue(":job_applicant_request_info_id",$job_applicant_request_info_id);

	$applicant_job_screening_levels_count_get_query->execute();

	if($applicant_job_screening_levels_count_get_query->rowCount() > 0) {
		$applicant_job_screening_levels_count_get_query_result = $applicant_job_screening_levels_count_get_query->fetch();
	     return $applicant_job_screening_levels_count_get_query_result;

	}//close of if($company_office_addresses_details_select_query_result->rowCount() > 0) {
	return $constructed_array;
	
}
function applicant_job_screening_levels_get($job_applicant_request_info_id,$job_id,$event_status) {
	global $dbcon;
	$constructed_array = array();
	
	$applicant_job_screening_levels_count_get_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `job_id`=:job_id AND `job_applicant_request_info_id` =:job_applicant_request_info_id AND `event_status` =:event_status";
	$applicant_job_screening_levels_count_get_query = $dbcon->prepare($applicant_job_screening_levels_count_get_sql);
    $applicant_job_screening_levels_count_get_query->bindValue(":job_id",$job_id);
	$applicant_job_screening_levels_count_get_query->bindValue(":job_applicant_request_info_id",$job_applicant_request_info_id);
	$applicant_job_screening_levels_count_get_query->bindValue(":event_status",$event_status);

	$applicant_job_screening_levels_count_get_query->execute();

	if($applicant_job_screening_levels_count_get_query->rowCount() > 0) {
		$applicant_job_screening_levels_count_get_query_result = $applicant_job_screening_levels_count_get_query->fetch();
	     return $applicant_job_screening_levels_count_get_query_result;

	}//close of if($company_office_addresses_details_select_query_result->rowCount() > 0) {
	return $constructed_array;
	
}

exit;
?>