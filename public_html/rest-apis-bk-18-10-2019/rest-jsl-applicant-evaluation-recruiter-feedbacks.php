
<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "8")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'])) 
				
					if (isset($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'])) 
				
					if (isset($ea_received_rest_ws_raw_array_input['recruiter_sm_memb_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['recruiter_sm_memb_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['recruiter_sm_memb_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['recruiter_feedback_note'])) {
						$content .= $ea_received_rest_ws_raw_array_input['recruiter_feedback_note'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['recruiter_feedback_note'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['rating_by_recruiter'])) {
						$content .= $ea_received_rest_ws_raw_array_input['rating_by_recruiter'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['rating_by_recruiter'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['file_rel_original_filename'])) {
						$content .= $ea_received_rest_ws_raw_array_input['file_rel_original_filename'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['file_rel_original_filename']))
					
					if (isset($ea_received_rest_ws_raw_array_input['file_content'])) {
						//$content .= $ea_received_rest_ws_raw_array_input['file_content'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['file_content']))
					
					
				}	
				
					$jsl_applicant_evaluation_info_id_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					
					$job_applicant_request_info_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					$recruiter_sm_memb_id_input = trim(isset($ea_received_rest_ws_raw_array_input['recruiter_sm_memb_id']) ? filter_var($ea_received_rest_ws_raw_array_input['recruiter_sm_memb_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					$rating_by_recruiter_input = trim(isset($ea_received_rest_ws_raw_array_input['rating_by_recruiter']) ? filter_var($ea_received_rest_ws_raw_array_input['rating_by_recruiter'], FILTER_SANITIZE_NUMBER_INT) : '');
					$recruiter_feedback_note_input = trim(isset($ea_received_rest_ws_raw_array_input['recruiter_feedback_note']) ? filter_var($ea_received_rest_ws_raw_array_input['recruiter_feedback_note'], FILTER_SANITIZE_STRING) : '');
					
					$file_rel_original_filename_input = trim(isset($ea_received_rest_ws_raw_array_input['file_rel_original_filename']) ? filter_var($ea_received_rest_ws_raw_array_input['file_rel_original_filename'], FILTER_SANITIZE_STRING) : '');
				
					$file_content_input = trim(isset($ea_received_rest_ws_raw_array_input['file_content']) ? filter_var($ea_received_rest_ws_raw_array_input['file_content'], FILTER_SANITIZE_STRING) : '');
					
					$recruiter_feedback_note_safe_html_input = get_cleaned_safe_html_content_input($recruiter_feedback_note_input, $page_content_file_config);
					
					$jsl_evaluation_info_result = jsl_evaluation_info_get($jsl_applicant_evaluation_info_id_input);
				
					$company_id_input = $jsl_evaluation_info_result["company_id"];
					$company_client_id_input = $jsl_evaluation_info_result["company_client_id"];
					$job_id_input = $jsl_evaluation_info_result["job_id"];
					$job_applicant_sm_memb_id_input = $jsl_evaluation_info_result["job_applicant_sm_memb_id"];
					$jsl_info_id_input = $jsl_evaluation_info_result["jsl_info_id"];
					$jsl_classification_detail_id_input = $jsl_evaluation_info_result["jsl_classification_detail_id"];
					$jsl_classification_detail_name_input = $jsl_evaluation_info_result["jsl_classification_detail_name"];
					$jsl_classification_detail_seo_name_input = $jsl_evaluation_info_result["jsl_classification_detail_seo_name"];
					$jsl_sub_classification_detail_id_input = $jsl_evaluation_info_result["jsl_sub_classification_detail_id"];
					$jsl_sub_classification_detail_name_input = $jsl_evaluation_info_result["jsl_sub_classification_detail_name"];
					$jsl_sub_classification_detail_seo_name_input = $jsl_evaluation_info_result["jsl_sub_classification_detail_seo_name"];
					
					
					if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
					} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
					}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						

					
					  //invalid company_id scenario
					
					if ($company_id_input == "") {
				
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "missing-company-id";
						$response['status_description'] = "missing company id";
						
						$eventLog->log("missing-company-id: Please provide a valid Company id.");
						
					} else if ($company_client_id_input == "") {
						//Invalid Mobile Number scenario
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "missing-company-client-id";
						$response['status_description'] = "Missing Company client id";
						
						$eventLog->log("missing-company-client-id: Please provide a valid Company client id.");
						
					} else if ($job_id_input == "") {
						//Invalid Mobile Number scenario
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "missing-job-id";
						$response['status_description'] = "Missing Job id";
						
						$eventLog->log("missing-job-id: Please provide a valid Job id.");
						
					} else if ($company_client_id_input == "") {
						//Invalid Mobile Number scenario
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						//$response['status'] = "missing-company_client_id";
						$response['status'] = "missing-company-client-id";
						$response['status_description'] = "Missing Company client id";
						
						$eventLog->log("missing-company-client-id: Please provide a valid Company client id.");
						
					
					} else if ($job_applicant_request_info_id_input == "") {
						//Invalid Mobile Number scenario
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "missing-job-applicant-request-info-id";
						$response['status_description'] = "Missing Job applicant request info Id";
						
						$eventLog->log("missing-job-applicant-request-info-id: Please provide a valid Job applicant request info Id.");
						
					} else if ($jsl_classification_detail_id_input == "") {
						
						$response['data'] = array();
						$response['status'] = "missing-job-classification-detail-id";
						$response['status_description'] = "Missing job classification detail id";
						
						$eventLog->log("missing-job-classification-detail-id: Please provide a valid job classification detail id.");
					
					} else if ($jsl_sub_classification_detail_id_input == "") {
						
						$response['data'] = array();
						$response['status'] = "missing-job-sub-classification-detail-id";
						$response['status_description'] = "Missing job sub classification detail id";
						
						$eventLog->log("missing-job-sub-classification-detail-id: Please provide a valid job sub classification detail id.");
					
					
					} else if ($job_applicant_sm_memb_id_input == "") {
						
						$response['data'] = array();
						$response['status'] = "missing-job-applicant-sm_memb_id-input";
						$response['status_description'] = "Missing job applicant sm_memb_id input";
						
						$eventLog->log("missing-job-applicant-sm_memb_id-input: Please provide a valid job applicant sm_memb_id input.");
					
					} else if ($jsl_info_id_input == "") {
						
						$response['data'] = array();
						$response['status'] = "missing-jsl-info-id-input";
						$response['status_description'] = "Missing JSL info id input";
						
						$eventLog->log("missing-jsl-info-id-input: Please provide a valid JSL info id input.");
						
					} else if ($recruiter_feedback_note_input == "" && $file_rel_original_filename_input =="" ) {
						
						$response['data'] = array();
						$response['status'] = "missing-recruiter-feedback-note-input";
						$response['status_description'] = "Missing Recruiter Feedback Note input";
						
						$eventLog->log("missing-recruiter-feedback-note-input: Please provide a valid Recruiter Feedback Note input.");
					
					} else {	
						
							$job_rel_screening_level_add_next_step = "";
							if ($ea_extracted_jwt_token_user_type == "admin") {
								
								if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
									$job_rel_screening_level_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("before company id condition");
								} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
									
									$job_rel_screening_level_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("after add condition");
								} else {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "insufficient-permissions-different-company-user-id";
									$response['status_description'] = "Insufficient Set of Permissions";
									
									//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
									header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
									$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
									
								}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								
								
								
							} else {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
								$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
							
							}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
								
							$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
							
							if ($job_rel_screening_level_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
								try {
									
									
									$last_inserted_id =
									jsl_applicant_evaluation_recruiter_feedback_info_add($jsl_applicant_evaluation_info_id_input,$company_id_input,$company_client_id_input,$job_id_input,$jsl_classification_detail_id_input,$jsl_classification_detail_name_input,$jsl_classification_detail_seo_name_input,$jsl_sub_classification_detail_id_input,$jsl_sub_classification_detail_name_input,$jsl_sub_classification_detail_seo_name_input,$jsl_info_id_input,$job_applicant_request_info_id_input,$job_applicant_sm_memb_id_input,$recruiter_sm_memb_id_input,$rating_by_recruiter_input,$recruiter_feedback_note_input,$recruiter_feedback_note_safe_html_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch,$file_rel_original_filename_input,$file_content_input);
									
									if ($last_inserted_id != "") {
										
											$response['data'] = array();
											$response['status'] = "jsl-applicant-evaluation-recruiter-feedback-insertion-successful";
											$response['status_description'] = "JSL applicant evaluation recruiter feedback added Successfully.";
									
											$eventLog->log("JSL applicant evaluation recruiter feedback added Successfully.");   
										
										
									} else {
											
										$response['data'] = array();
										$response['status'] = "jsl-applicant-evaluation-recruiter-feedback-insertion-error";
										$response['status_description'] = "Error occurred when adding the JSL applicant evaluation recruiter feedback.";
										
										$eventLog->log("Error occurred when adding the JSL applicant evaluation recruiter feedback.");
										
									}
										
									
								} catch (Exception $e) {
									
									$response['data'] = array();
									$response['status'] = "jsl-applicant-evaluation-recruiter-feedback-insertion-error";
									$response['status_description'] = "Error occurred when adding the JSL applicant evaluation recruiter feedback.";
									
									$eventLog->log("Error occurred when adding the JSL applicant evaluation recruiter feedback.");
										
								}
							}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
							
						}
			//}//close of else of if ($email_id_input == "") {
					
		}//close of //close of if ($ea_is_user_page_access_authorized) {
					
				
	} else {
				
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "invalid-auth-token-submitted";
				$response['status_description'] = "Invalid Authentication";
				
				//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
				header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
				
				$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
				
			}//close of else of if ($ea_auth_token_validation_status) {
		
		}//close of if ($ea_maintenance_mode == false) {
		
} else {

		//Construct Content, that will be sent in Response body, of the REST Web Service
		$response['data'] = array();
		$response['status'] = "invalid-input";
		$response['status_description'] = "Invalid Input, Please check and provide all information.";
		
		//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
		
		$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
				
		
		}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




	//Check if Maintenance Mode is Turned On
	if ($ea_maintenance_mode) {	
		
		//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
		//header('Maintenance-Progress: true', false);
		//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
		$response['data'] = array();
		$response['status'] = "application-maintenance-in-progress";
		$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
		
		$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
		
	} else {	

		//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
		header('Content-Type: application/json');
		echo json_encode($response,JSON_PRETTY_PRINT);
		
	}//close of else of if ($ea_maintenance_mode){
		
function jsl_applicant_evaluation_recruiter_feedback_info_add($jsl_applicant_evaluation_info_id_input,$company_id_input,$company_client_id_input,$job_id_input,$jsl_classification_detail_id_input,$jsl_classification_detail_name_input,$jsl_classification_detail_seo_name_input,$jsl_sub_classification_detail_id_input,$jsl_sub_classification_detail_name_input,$jsl_sub_classification_detail_seo_name_input,$jsl_info_id_input,$job_applicant_request_info_id_input,$job_applicant_sm_memb_id_input,$recruiter_sm_memb_id_input,$rating_by_recruiter_input,$recruiter_feedback_note_input,$recruiter_feedback_note_safe_html_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch,$file_rel_original_filename_input,$file_content_input) {
	
	
	global $dbcon,$eventLog;
	$is_active_status = '1';
    
	$jsl_applicant_evaluation_recruiter_feedback_info_add_sql = "INSERT INTO `jsl_applicant_evaluation_recruiter_feedbacks`(`jsl_applicant_evaluation_info_id`, `company_id`, `company_client_id`, `job_id`, `jsl_classification_detail_id`, `jsl_classification_detail_name`, `jsl_classification_detail_seo_name`, `jsl_sub_classification_detail_id`, `jsl_sub_classification_detail_name`, `jsl_sub_classification_detail_seo_name`, `jsl_info_id`, `job_applicant_request_info_id`, `job_applicant_sm_memb_id`, `recruiter_sm_memb_id`, `recruiter_feedback_note`, `recruiter_feedback_note_safe_html`, `rating_by_recruiter`, `added_datetime`, `added_datetime_epoch`, `added_by_sm_memb_id`, `is_active_status`) VALUES (:jsl_applicant_evaluation_info_id,:company_id,:company_client_id,:job_id,:jsl_classification_detail_id,:jsl_classification_detail_name,:jsl_classification_detail_seo_name,:jsl_sub_classification_detail_id,:jsl_sub_classification_detail_name,:jsl_sub_classification_detail_seo_name,:jsl_info_id,:job_applicant_request_info_id,:job_applicant_sm_memb_id,:recruiter_sm_memb_id,:recruiter_feedback_note,:recruiter_feedback_note_safe_html,:rating_by_recruiter,:added_datetime,:added_datetime_epoch,:added_by_sm_memb_id,:is_active_status)";

	$jsl_applicant_evaluation_recruiter_feedback_info_add_query = $dbcon->prepare($jsl_applicant_evaluation_recruiter_feedback_info_add_sql);
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":jsl_applicant_evaluation_info_id",$jsl_applicant_evaluation_info_id_input);
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":company_id",$company_id_input);
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":company_client_id",$company_client_id_input);
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":job_id",$job_id_input);	
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":jsl_classification_detail_id",$jsl_classification_detail_id_input);
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":jsl_classification_detail_name",$jsl_classification_detail_name_input);
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":jsl_classification_detail_seo_name",$jsl_classification_detail_seo_name_input);
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":jsl_sub_classification_detail_id",$jsl_sub_classification_detail_id_input);
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":jsl_sub_classification_detail_name",$jsl_sub_classification_detail_name_input);
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":jsl_sub_classification_detail_seo_name",$jsl_sub_classification_detail_seo_name_input);
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":jsl_info_id",$jsl_info_id_input);
    $jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":job_applicant_request_info_id",$job_applicant_request_info_id_input);
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":job_applicant_sm_memb_id",$job_applicant_sm_memb_id_input);
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":recruiter_sm_memb_id",$recruiter_sm_memb_id_input);
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":recruiter_feedback_note",$recruiter_feedback_note_input);
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":recruiter_feedback_note_safe_html",$recruiter_feedback_note_safe_html_input);
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":rating_by_recruiter",$rating_by_recruiter_input);
	
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":added_datetime",$event_datetime);
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":added_datetime_epoch",$current_epoch);
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":added_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	
	$jsl_applicant_evaluation_recruiter_feedback_info_add_query->bindValue(":is_active_status",$is_active_status);
	
	if ($jsl_applicant_evaluation_recruiter_feedback_info_add_query->execute()) {
          $eventLog->log("after execute.");
            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");
           
			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}


exit;
?>