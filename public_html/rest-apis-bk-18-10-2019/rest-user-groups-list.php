<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Admin User Groups, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "2")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				//Filter Inputs	
				//$user_type_input = trim(isset($ea_received_rest_ws_raw_array_input['user_type']) ? filter_var($ea_received_rest_ws_raw_array_input['user_type'], FILTER_SANITIZE_STRING) : '');
				
				$user_group_status_input = trim(isset($ea_received_rest_ws_raw_array_input['user_group_status']) ? filter_var($ea_received_rest_ws_raw_array_input['user_group_status'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
			
				//$eventLog->log("user_type_input -> " . $user_type_input);
				$eventLog->log("user_group_status_input -> " . $user_group_status_input);
				$eventLog->log("ip_address_input -> " . $ip_address_input);
				$user_type_input="admin";
				//Check if all inputs are received correctly from the REST Web Service
				if (($user_type_input != "member") && ($user_type_input != "admin")) {
					//Invalid User Type scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-user-type";
					$response['status_description'] = "Invalid User Type";
					
					$eventLog->log("invalid-user-type: Invalid User Type info submitted, please check and try again.");
					
				} else if (($user_group_status_input != "0") && ($user_group_status_input != "1") && ($user_group_status_input != "")) {
					//Invalid User Group Status scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-user-group-status";
					$response['status_description'] = "Invalid User Group Status";
					
					$eventLog->log("invalid-user-group-status: Invalid User Group Status info submitted, please check and try again.");
					
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-additional-information";
					$response['status_description'] = "Missing Some Additional Information";
					
					$eventLog->log("missing-additional-information: Some Additional Information like IP Address (IPv4) is missing, please check and try again.");
							  
				} else {
					//All inputs are Valid
				
					$eventLog->log("All inputs are valid.");
				
					try { 
						$user_groups_list_result = array();
						
						if ((isset($ea_extracted_jwt_token_user_company_id)) && ($ea_extracted_jwt_token_user_company_id != "")) {
							//Company scope
							//Do Get User Groups List, from sm_site_member_classification_details db table
							//$user_groups_list_result = ea_get_user_groups_list_in_company_scope($user_type_input, $user_group_status_input);
							$user_groups_list_result = ea_get_user_groups_list_in_company_scope($user_type_input, $user_group_status_input);
						
						} else {
							//Platform (Super Admin / Site Admin) Scope
							//Do Get User Groups List, from sm_site_member_classification_details db table
							$user_groups_list_result = ea_get_user_groups_list($user_type_input, $user_group_status_input);
						}
						$user_groups_list_result_json_encoded = json_encode($user_groups_list_result);
						
						$eventLog->log("user_groups_list_result_json_encoded -> " . $user_groups_list_result_json_encoded);
						
						$user_groups_list_result_count = count($user_groups_list_result);
						
						$eventLog->log("Count -> " . $user_groups_list_result_count); 
						
						if ($user_groups_list_result_count > "0") {
							//One or More User Groups Exist and Active
							
							$response['data'] = $user_groups_list_result;
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['status'] = "user-groups-details-received";
							$response['status_description'] = "User Group Details Successfully Received";
							
							$user_groups_list_result_json_encoded = json_encode($user_groups_list_result);
						
							$eventLog->log("User Groups List -> " . $user_groups_list_result_json_encoded); 
							
						} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "active-user-groups-doesnot-exist";
							$response['status_description'] = "User Groups List -> No Active User Groups Exist";
							
							//No Active User Groups Exist
							$eventLog->log("active-user-groups-doesnot-exist: User Groups List -> No Active User Groups Exist, please check and try again."); 
						}//close of else of if ($user_group_list_result_count > "0") {
						
					} catch (Exception $e){
						$eventLog->log("Exception -> " . html_escaped_output($e->getMessage())); 
						//addLog($logFile, "Exception -> ".$e->getMessage());	
					}//close of  catch (Exception $e){
					
				
				}//close of else of if (($user_type_input != "member") || ($user_type_input != "admin")) {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");	
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
    $eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");	
	}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function user_reg_allowed_list($user_classification_id) {
	
	global $dbcon;
	
	$constructed_array = array();
	
	$user_groups_list_sql = "SELECT `company_specific_subject_classifications`  FROM `page_api_sm_site_user_classification_associations` WHERE `page_filename` LIKE 'rest-quick-onboarding-flow-admin-user-add.php' AND `sm_site_member_classification_detail_id` = :sm_site_member_classification_detail_id";
	
	$user_groups_list_query = $dbcon->prepare($user_groups_list_sql);
	$user_groups_list_query->bindValue(":sm_site_member_classification_detail_id",$user_classification_id);

	$user_groups_list_query->execute();
	
	
	
}
	
function user_groups_list($sm_user_type_input, $user_group_status_input) {
	
	global $dbcon;
	
	$constructed_array = array();
	
	$sm_user_role_input = '%'.'super-admin'.'%';
	


	$user_groups_list_sql = "SELECT * FROM `sm_site_member_classification_details` WHERE `sm_user_type`=:sm_user_type AND `sm_user_role` NOT LIKE :sm_user_role AND `is_active_status`=:is_active_status ORDER BY `sm_user_level` ASC";
	
	$user_groups_list_query = $dbcon->prepare($user_groups_list_sql);
	$user_groups_list_query->bindValue(":sm_user_type",$sm_user_type_input);
	$user_groups_list_query->bindValue(":sm_user_role",$sm_user_role_input);
	$user_groups_list_query->bindValue(":is_active_status","1");
	$user_groups_list_query->execute();
	
	
	if($user_groups_list_query->rowCount() > 0) {
	    $user_groups_list_query_result = $user_groups_list_query->fetchAll();
	    //print_r($user_classification_details_get_select_query_result);
	    
		foreach ($user_groups_list_query_result as $user_groups_list_query_result_row) {
			
			$temp_row_array = array();
		    $temp_row_array["user_classification_detail_id"] = $user_groups_list_query_result_row["sm_site_member_classification_detail_id"];
		    $temp_row_array["user_type"] = $user_groups_list_query_result_row["sm_user_type"];
		    $temp_row_array["user_level"] = $user_groups_list_query_result_row["sm_user_level"];
		    $temp_row_array["user_role"] = $user_groups_list_query_result_row["sm_user_role"];
		    $temp_row_array["user_department"] = $user_groups_list_query_result_row["department"];
		    $temp_row_array["user_privilege_summary"] = $user_groups_list_query_result_row["user_privilege_summary"];
			$temp_row_array["concurrent_token_based_logins_local_setting"] = $user_groups_list_query_result_row["concurrent_token_based_logins_local_setting"];
			$temp_row_array["max_allowed_active_concurrent_tokens_count"] = $user_groups_list_query_result_row["max_allowed_active_concurrent_tokens_count"];
		    $constructed_array[] = $temp_row_array;
	    }//close of foreach ($user_classification_details_get_select_query_result as $user_classification_details_get_select_query_result_row) {
			
		return $constructed_array;
	}
	return $constructed_array;
	
}

exit;
?>