<?php
ob_start();
/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 */
//headers to enable cors in php for one or more websites
//header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Origin: http://www.remotewebsite.com');
 
//To prevent direct access to a file inside public root or public_html or www folder, 
define("START", "No Direct Access", true);

//Filtered $_SERVER Variables (required ones)
include "../app/core/server-var-info.php";

//Header Functions
include "../app/includes/header-functions.php";

//Issues CORS headers for the application
ea_cors_headers();

//Issues Security Headers for the application
ea_application_security_headers();

//Main Application Configuration File
include "../app/core/main-config.php";

//include timer class file and create object
include "../app/class/Timer.php";
$load_time1 = new Timer();

//For Web Applications, Timer Start, to know how much time, an application request consumed 
if ($page_is_ajax != "1") {
  // calculate the time it takes to run page load time using timer #1 start
  $load_time1->start();
}//close of if ($page_is_ajax != "1") {
	
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

if(function_exists("date_default_timezone_set")) {
//Define the Default timezone.	
date_default_timezone_set($date_default_timezone_set); // $date_default_timezone_set from /app/core/main-config.php
}//close of if(function_exists("date_default_timezone_set")) {
	

//HTMLawed Library to purify and filter HTML (http://www.bioinformatics.org/phplabware/internal_utilities/htmLawed/)
include "../app/includes/htmLawed-v1241.php"; 

//Few Validation and Sanitizing Functions
include "../app/includes/validate-sanitize-functions.php";

//This holds all Session specific and Session Checking Functions
include "../app/core/session-check-functions.php";

//This does the pre-defined host name, thus observing the host of the script, if it is Dev / Live Environment
include "../app/core/hostname-check.php";

//This is to automatically define a finite list of Routing Engine Variables (20 will be created in this scenario) (http://stackoverflow.com/a/2922688)
for ($i = 0; $i <= 20; $i++) {
    $routing_eng_var = 'routing_eng_var_' . $i;
    //echo $routing_eng_var . "<br>";
    $$routing_eng_var = ""; //make column stuff, first time this will be xm0, then xm1, etc.
}//for ($i = 0; $i <= 20; $i++) {
	
//This is the simple routing engine of Easeapp Framework that is based on $_SERVER["REQUEST_URI"]
include "../app/core/routing-engine.php";





//This is where the core routing engine rules are defined
include "../app/core/routing-engine-rules.php";

//This is where the user defined routing engine rules are defined
include "../app/core/routing-engine-user-rules.php";

//This is where the route specific functions, that handles route parsing, pass request data to models, inject model data in to views and for finally generating the responses
include "../app/includes/route-functions.php";

//This is where the route parser lies, that routes the request for the particular virtual resource
include "../app/core/route-parser.php";

//Ajax Requests / REST Web Services: Token based Authentication / Session based Authentication
/*
Note 1: This is useful to Create a REST API, with JWT Token based Authentication, for other Applications to Consume.
Note 2: This can be used as a Regular Web Application, that uses Session based Authentication.
Note 3: This can be used as a Regular Web Application, that uses JWT Token based Authentication, from an external Source.
*/
if (($page_is_ajax == "1") && ($page_is_frontend == "3")) {
   
    if ($page_filename != "not-found.php") {
	   
	   if ((($page_is_web_service_endpoint == "0") || ($page_is_web_service_endpoint == "1") || ($page_is_web_service_endpoint == "2")) && ($token_based_authentication == "0") && ($session_based_authentication == "1")) {
			//With Session based Authentication: Pure Ajax Call Scenario / Web Service Endpoint Scenario / Either Ajax Call or Web Service Endpoint Scenario (Common for BOTH)
	        
			//Session Settings
			ea_application_session_settings();

			//Start Session
			session_start();

			//Define User Session with pre-login scenario and conventions
			if ((!isset($_SESSION['loggedin'])) && ($_SESSION['loggedin'] != "yes")) {
			   $_SESSION['loggedin'] = "no";
			   $_SESSION['sm_user_type'] = "";
			} else if ((isset($_SESSION['loggedin'])) && ($_SESSION['loggedin'] != "yes")) {
			   $_SESSION['loggedin'] = "no";
			   $_SESSION['sm_user_type'] = "";
			}//close of else if of if ((!isset($_SESSION['loggedin'])) && ($_SESSION['loggedin'] != "yes")) {
			
  	    } else if ((($page_is_web_service_endpoint == "0") || ($page_is_web_service_endpoint == "1") || ($page_is_web_service_endpoint == "2")) && ($token_based_authentication == "1") && ($session_based_authentication == "0")) {
			//With Token based Authentication: Pure Ajax Call Scenario / Web Service Endpoint Scenario / Either Ajax Call or Web Service Endpoint Scenario (Common for BOTH)
			
			
  	    }//close of else if of if ((($page_is_web_service_endpoint == "0") || ($page_is_web_service_endpoint == "1") || ($page_is_web_service_endpoint == "2")) && ($token_based_authentication == "0") && ($session_based_authentication == "1")) {
			
  	}//close of if ($page_filename != "not-found.php") {
		
} else {
	
	if ($page_filename != "not-found.php") {
	   
	    if (($token_based_authentication == "0") && ($session_based_authentication == "1")) {
			//With Session based Authentication: Regular Web Application Scenario
	        
			//Session Settings
			ea_application_session_settings();

			//Start Session
			session_start();

			//Define User Session with pre-login scenario and conventions
			if ((!isset($_SESSION['loggedin'])) && ($_SESSION['loggedin'] != "yes")) {
			   $_SESSION['loggedin'] = "no";
			   $_SESSION['sm_user_type'] = "";
			} else if ((isset($_SESSION['loggedin'])) && ($_SESSION['loggedin'] != "yes")) {
			   $_SESSION['loggedin'] = "no";
			   $_SESSION['sm_user_type'] = "";
			}//close of else if of if ((!isset($_SESSION['loggedin'])) && ($_SESSION['loggedin'] != "yes")) {
			
  	    } else if (($token_based_authentication == "1") && ($session_based_authentication == "0")) {
			//With Token based Authentication: Regular Web Application Scenario
			
			 
  	    }//close of else if of if ((($page_is_web_service_endpoint == "0") || ($page_is_web_service_endpoint == "1") || ($page_is_web_service_endpoint == "2")) && ($token_based_authentication == "0") && ($session_based_authentication == "1")) {
			
  	}//close of if ($page_filename != "not-found.php") {
	
}//close of else of if (($page_is_ajax == "1") && ($page_is_frontend == "3")) {
	
/* commented on 29-01-2019
//Session Settings
ea_application_session_settings();

//Start Session
session_start();

//Define User Session with pre-login scenario and conventions
if ((!isset($_SESSION['loggedin'])) && ($_SESSION['loggedin'] != "yes")) {
   $_SESSION['loggedin'] = "no";
   $_SESSION['sm_user_type'] = "";
} else if ((isset($_SESSION['loggedin'])) && ($_SESSION['loggedin'] != "yes")) {
   $_SESSION['loggedin'] = "no";
   $_SESSION['sm_user_type'] = "";
}//close of else if of if ((!isset($_SESSION['loggedin'])) && ($_SESSION['loggedin'] != "yes")) {
*/

//Issues Browser Cache specific Headers for the Application	
ea_application_browser_cache_headers();

//require . __DIR__ . "/vendor/autoload.php");
//include dirname(__FILE__) . "/vendor/autoload.php";
require dirname(dirname(__FILE__)) . "/vendor/autoload.php";

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

//$uuid4 = Uuid::uuid4();
//echo "UUID4: " . $uuid4->toString() . "\n"; // i.e. 25769c6c-d34d-4bfe-ba98-e0ee856f3e7a

//Site Construction / Maintenance
if ($app_site_status == "construction") {
  //echo "<center>This website is under rapid construction sessions, please visit us again, thank you</center>";
  exit;
} else if ($app_site_status == "maintenance") {
  //echo "<center>This website is taken down for maintenance, please visit us again, thank you</center>";
  exit;
}//close of else if of if ($app_site_status == "construction") {
	

//Debug Settings 
if ($debug_mode == "ON") {
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  //ini_set('display_errors', true);
  //echo "debug mode on";
} elseif($debug_mode == "OFF") {
  ini_set('display_errors', 0);
  ini_set('display_startup_errors', 0);
  error_reporting(0);
  //echo "debug mode off";
}//close of else if of if ($debug_mode == "ON") {
	
//This holds all User Authorization Functions that ensures if a particular access level is allowed on the particular route or not
include "../app/core/user-authorization-functions.php";

//PHPMailer Library: This is to send Email through SMTP / Sendmail in PHP Scripts
/* include "../app/includes/phpmailer-v605/src/Exception.php";
include "../app/includes/phpmailer-v605/src/PHPMailer.php";
include "../app/includes/phpmailer-v605/src/SMTP.php"; */
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

//Define Sendmail Transport
$phpmailer_sendmail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
$phpmailer_sendmail->IsSendmail(); // telling the class to use SendMail transport
$phpmailer_sendmail->CharSet="utf-8";

//Define SMTP Transport
$phpmailer_smtp = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
//Server settings
/*$phpmailer_smtp->SMTPDebug = 2;                                 // Enable verbose debug output
$phpmailer_smtp->isSMTP();                                      // Set mailer to use SMTP
//$phpmailer_smtp->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
$phpmailer_smtp->Host = 'vdmail.securitywonks.net';  // Specify main and backup SMTP servers
$phpmailer_smtp->SMTPAuth = true;                               // Enable SMTP authentication
$phpmailer_smtp->Username = 'notifications@vdmail.securitywonks.net';                 // SMTP username
$phpmailer_smtp->Password = 'notifications';                           // SMTP password
//$phpmailer_smtp->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
//$phpmailer_smtp->Port = 465;                                    // TCP port to connect to
$phpmailer_smtp->Port = 26;                                    // TCP port to connect to*/

$phpmailer_smtp->SMTPDebug = $ea_smtp_debug_setting;                                 // Enable verbose debug output
$phpmailer_smtp->isSMTP();                                      // Set mailer to use SMTP
//$phpmailer_smtp->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
$phpmailer_smtp->Host = $ea_smtp_host;  // Specify main and backup SMTP servers
$phpmailer_smtp->SMTPAuth = $ea_smtp_auth_setting;                               // Enable SMTP authentication
$phpmailer_smtp->Username = $ea_smtp_username;                 // SMTP username
$phpmailer_smtp->Password = $ea_smtp_password;                           // SMTP password
	

if ($ea_is_smtp_secure_enabled == "1") {
	//Secure SMTP Mode
	
	if ($ea_smtp_secure_setting == "tls") {
		//TLS Mode
		$phpmailer_smtp->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$phpmailer_smtp->Port = $ea_smtp_secure_tls_port_setting;       // TCP port to connect to
		
	} else if ($ea_smtp_secure_setting == "ssl") {
		//SSL Mode
		$phpmailer_smtp->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		$phpmailer_smtp->Port = $ea_smtp_secure_ssl_port_setting;       // TCP port to connect to
		
	}//close of else if of if ($ea_smtp_secure_setting == "tls") {
	
	
} else {
	//In Secure SMTP Mode
	$phpmailer_smtp->Port = 26;                                    // TCP port to connect to
	
}//close of else if of if ($ea_is_smtp_secure_enabled == "0") {


//This is a compilation of date related custom functions
include "../app/includes/date-functions.php";

//This is a wrapper to count number of SQL Queries
include "../app/class/PDOEx.php";

//This hosts the actual Database details of both dev and production environments along with corresponding db connections
include "../app/core/db-connect-main.php";

//This contains some of the basic PDO Prepared Statements based DB Functions, for MySQL & MariaDB Databases.
include "../app/includes/mysql-mariadb-database-functions.php";

//Include a Logger
include "../app/class/EALogger.php";

//Include a DB Manager, for MySQL and MariaDB Databases
include "../app/class/EASQLDBManager.php";

//This hosts any generic application related functions for the application that uses this framework
include "../app/includes/other-functions.php";

//This is a generic class to get headers using curl
include "../app/class/get_header_info_curl.php";

//EasyPHPThumbnail: This is a fully functional image processing and thumbnail creating library
include "../app/includes/easyphpthumbnail.class.php";


//FQDN and Sub Domain Name Parsing by jeremykendall-php-domain-parser start https://github.com/jeremykendall/php-domain-parser
include "../app/includes/jeremykendall-php-domain-parser/src/Pdp/PublicSuffixListManager.php";
include "../app/includes/jeremykendall-php-domain-parser/src/Pdp/PublicSuffixList.php";
include "../app/includes/jeremykendall-php-domain-parser/src/Pdp/Parser.php";
include "../app/includes/jeremykendall-php-domain-parser/src/pdp-parse-url.php";
include "../app/includes/jeremykendall-php-domain-parser/src/Pdp/Uri/Url/Host.php";
include "../app/includes/jeremykendall-php-domain-parser/src/Pdp/Uri/Url.php";

use Pdp\PublicSuffixListManager;
use Pdp\Parser;
// Obtain an instance of the parser
$pslManager = new PublicSuffixListManager();
$parser = new Parser($pslManager->getList());
//FQDN and Sub Domain Name Parsing by jeremykendall-php-domain-parser end https://github.com/jeremykendall/php-domain-parser

//For UUID Generation
include "../app/includes/uuid.php";

//For Email and Links Extraction
include "../app/includes/extract-links-emails-from-html.php";

//This hosts any miscellaneous set of functions that are useful and that do not fit elsewhere
include "../app/includes/misc-functions.php";

//FPDF to Create PDF Files
include "../app/includes/fpdf-181/fpdf.php";

		
if (version_compare(PHP_VERSION, '7.2.0') >= 0) {
	//echo 'I am at least PHP version 7.2.0, my version: ' . PHP_VERSION . "\n<br><br>";
	//Include Constant Time Encoding Library v2.0 from Paragonie
	include "../app/includes/constant-time-encoding-v20/constant-time-encoding-v20-includes.php";
	//Include Halite Library from Paragonie
	include "../app/includes/halite-v441/halite-v441-includes.php";
	
	/*//Check if Libsodium is setup correctly, result will be bool(true), if all is good, https://stackoverflow.com/a/37687595
	//var_dump(ParagonIE\Halite\Halite::isLibsodiumSetupCorrectly());
	//echo ParagonIE\Halite\Halite::isLibsodiumSetupCorrectly(true);	
	//echo "<br><br>";
	//Check the installed versions of Libsodium
	echo "<br>var_dump result<br>";
	var_dump([
		SODIUM_LIBRARY_MAJOR_VERSION,
		SODIUM_LIBRARY_MINOR_VERSION,
		SODIUM_LIBRARY_VERSION
	]);
	echo "<br>print_r result<br>";
	print_r([
		SODIUM_LIBRARY_MAJOR_VERSION,
		SODIUM_LIBRARY_MINOR_VERSION,
		SODIUM_LIBRARY_VERSION
	]);
	echo "<br><hr><br><pre>";
	*/

	
	//Check, if Libsodium is setup correctly
	if (ParagonIE\Halite\Halite::isLibsodiumSetupCorrectly() === true) {
		//echo "true<br>";
		/*//Retrieve the previous saved Symmetric Encryption key from the file
		$pg_symmetric_encryption_key = \ParagonIE\Halite\KeyFactory::loadEncryptionKey($site_home_path . $pg_generated_enc_keys_folder_name. $pg_symmetric_encryption_key_filename);*/

		//Retrieve the previous saved Asymmetric Anonymous Encryption key from the file
		$pg_asymmetric_anonymous_encryption_keypair = \ParagonIE\Halite\KeyFactory::loadEncryptionKeyPair($site_home_path . $pg_generated_enc_keys_folder_name. $pg_asymmetric_anonymous_encryption_keypair_filename);

		$pg_asymmetric_anonymous_encryption_secret_key = $pg_asymmetric_anonymous_encryption_keypair->getSecretKey();
		$pg_asymmetric_anonymous_encryption_public_key = $pg_asymmetric_anonymous_encryption_keypair->getPublicKey();
		
		//Retrieve the previous saved Asymmetric Anonymous Encryption key for Logs from the file
		$pg_asymmetric_anonymous_encryption_logs_keypair = \ParagonIE\Halite\KeyFactory::loadEncryptionKeyPair($site_home_path . $pg_generated_enc_keys_folder_name. $pg_asymmetric_anonymous_encryption_logs_keypair_filename);

		$pg_asymmetric_anonymous_encryption_logs_secret_key = $pg_asymmetric_anonymous_encryption_logs_keypair->getSecretKey();
		$pg_asymmetric_anonymous_encryption_logs_public_key = $pg_asymmetric_anonymous_encryption_logs_keypair->getPublicKey();

		//Retrieve the previous saved Asymmetric Authentication key from the file
		$pg_asymmetric_authentication_keypair = \ParagonIE\Halite\KeyFactory::loadSignatureKeyPair($site_home_path . $pg_generated_enc_keys_folder_name. $pg_asymmetric_authentication_keypair_filename);

		$pg_asymmetric_authentication_secret_key = $pg_asymmetric_authentication_keypair->getSecretKey();
		$pg_asymmetric_authentication_public_key = $pg_asymmetric_authentication_keypair->getPublicKey();
		
		//Retrieve the previous saved Asymmetric Authentication key for Logs from the file
		$pg_asymmetric_authentication_logs_keypair = \ParagonIE\Halite\KeyFactory::loadSignatureKeyPair($site_home_path . $pg_generated_enc_keys_folder_name. $pg_asymmetric_authentication_logs_keypair_filename);

		$pg_asymmetric_authentication_logs_secret_key = $pg_asymmetric_authentication_logs_keypair->getSecretKey();
		$pg_asymmetric_authentication_logs_public_key = $pg_asymmetric_authentication_logs_keypair->getPublicKey();
		
				
		/* sample codes start*/
		/*
		$message = "1 start";

		$sealed = \ParagonIE\Halite\Asymmetric\Crypto::seal(
			new ParagonIE\Halite\HiddenString(
				$message
			),
			$pg_asymmetric_anonymous_encryption_public_key
		);
		echo "sealed: <br>" . $sealed . "<br><hr><br>";


		$opened = \ParagonIE\Halite\Asymmetric\Crypto::unseal(
			$sealed,
			$pg_asymmetric_anonymous_encryption_secret_key
		);

		echo "opened: <br>" . $opened . "<br><hr><br>";

		$signature = \ParagonIE\Halite\Asymmetric\Crypto::sign(
			$sealed,
			$pg_asymmetric_authentication_secret_key
		);
		echo "signature: <br>" . $signature . "<br><hr><br>";

		$valid = \ParagonIE\Halite\Asymmetric\Crypto::verify(
			$sealed,
			$pg_asymmetric_authentication_public_key,
			$signature
		);
		echo "Signature Verification Status: <br>" . $valid . "<br><hr><br>";
		*/
		/*sample code end*/
		//exit;

		
		
	}//close of if (ParagonIE\Halite\Halite::isLibsodiumSetupCorrectly() === true) {
		
		
	
}//close of if (version_compare(PHP_VERSION, '7.2.0') >= 0) {

//Include a Custome Halite Operation
include "../app/class/EAHalite.php";
$objEAHalite = new EAHalite();

$db = new DB();

//This hosts user defined REST Web Service API Functions, when and if REST Web Services are offered
include "../app/includes/other-functions-api.php";

//This hosts User Authentication and Info Functions
include "../app/includes/user-authentication-info-functions.php";

//This holds all JSON Web Token Creating / Checking Functions
include "../app/includes/json-web-token-functions.php";

//This holds creating and link verification related Functions
include "../app/includes/expiring-links.php";

//Elasticsearch

use Elasticsearch\ClientBuilder;

//Elasticsearch Client Connection
$elasticsearch_client = ClientBuilder::create()
						->setHosts($elasticsearch_host_array)
						->setRetries(0)
						->build();

						
//Elasticsearch: Define Index name
$elasticsearchCandidateIndexParams['index']  = $elasticsearch_candidates_index;   

//Elasticsearch: Index Exist Check
$elasticsearch_candidate_db_existence_status_result = $elasticsearch_client->indices()->exists($elasticsearchCandidateIndexParams);

//Check if index exists Result is true, and Create Index, if it isn't
if ($elasticsearch_candidate_db_existence_status_result === false) {
	//Create index (as noun / Database)
	
	$candidateMapping = array(
		'properties' => array(
			'sm_memb_id' => array(
				'type' => 'integer'
			),
			'badges' => array(
				'type' => 'integer'
			),
			'sm_email' => array(
				'type' => 'keyword'
			),
			'sm_alternate_email' => array(
				'type' => 'keyword'
			),
			'sm_mobile' => array(
				'type' => 'keyword'
			),
			'sm_salutation' => array(
				'type' => 'keyword'
			),
			'sm_middlename' => array(
				'type' => 'keyword'
			),
			'sm_lastname' => array(
				'type' => 'keyword'
			),
			'sm_fullname' => array(
				'type' => 'keyword'
			),
			'sm_city' => array(
				'type' => 'keyword'
			),
			'sm_state' => array(
				'type' => 'keyword'
			),
			'sm_country' => array(
				'type' => 'keyword'
			),
			'company_id' => array(
				'type' => 'keyword'
			),
			'crur_id' => array(
				'type' => 'integer'
			),
			'original_filename' => array(
				'type' => 'keyword'
			),
			'resume_extracted_info_id' => array(
				'type' => 'integer'
			),
			'resume_text' => array(
				'type' => 'text'
			)
		)
	);
	
	$elasticsearchCandidateIndexParams['body']['mappings'] = $candidateMapping;
	//http://www.searchly.com/docs/php
	//Create Index (Database)
	$elasticsearch_client->indices()->create($elasticsearchCandidateIndexParams);

}//close of if ($elasticsearch_candidate_db_existence_status_result === false) {

//Elasticsearch: Define Index name
$elasticsearchJobIndexParams['index']  = $elasticsearch_jobs_index;   

//Elasticsearch: Index Exist Check
$elasticsearch_jobs_db_existence_status_result = $elasticsearch_client->indices()->exists($elasticsearchJobIndexParams);

//Check if index exists Result is true, and Create Index, if it isn't
if ($elasticsearch_jobs_db_existence_status_result === false) {
	//Create index (as noun / Database)
	
	$jobMapping = array(
		'properties' => array(
			'job_id' => array(
				'type' => 'keyword'
			),
			'badges' => array(
				'type' => 'integer'
			),
			'company_id' => array(
				'type' => 'keyword'
			),
			'company_client_id' => array(
				'type' => 'keyword'
			),
			/* 'job_posted_by_member_classification_detail_id' => array(
				'type' => 'keyword'
			),
			'job_posted_by_sm_memb_id' => array(
				'type' => 'keyword'
			), */
			'job_title' => array(
				'type' => 'text'
			),
			'job_summary' => array(
				'type' => 'text'
			),
			'job_full_description' => array(
				'type' => 'text'
			),
			'job_type_name' => array(
				'type' => 'keyword'
			),
			'job_industry_name' => array(
				'type' => 'keyword'
			),
			'job_work_location_requirement_name' => array(
				'type' => 'keyword'
			),
			'job_recruitment_status' => array(
				'type' => 'keyword'
			),
			'resume_submission_end_date' => array(
				'type' => 'date'
			),
			'no_of_openings' => array(
				'type' => 'keyword'
			),
			'available_positions' => array(
				'type' => 'keyword'
			),
			'maximum_allowed_submittals_count' => array(
				'type' => 'keyword'
			),
			'address_line_1' => array(
				'type' => 'text'
			),
			'address_line_2' => array(
				'type' => 'text'
			),
			'city' => array(
				'type' => 'text'
			),
			'state' => array(
				'type' => 'text'
			),
			'country' => array(
				'type' => 'text'
			),
			/* 'job_posted_date_time' => array(
				'type' => 'date'
			), */
			'expected_job_start_date' => array(
				'type' => 'date'
			),
			
			'zipcode' => array(
				'type' => 'keyword'
			),
			'primary_recruiter_id' => array(
				'type' => 'keyword'
			),
			'primary_recruiter_name' => array(
				'type' => 'text'
			),
			'ot' => array(
				'type' => 'keyword'
			),
			'job_references' => array(
				'type' => 'keyword'
			),
			'travel' => array(
				'type' => 'keyword'
			),
			'drug_test' => array(
				'type' => 'keyword'
			),
			'background_check' => array(
				'type' => 'keyword'
			),
			'security_clearance' => array(
				'type' => 'keyword'
			)
		)
	);
	
	$elasticsearchJobIndexParams['body']['mappings'] = $jobMapping;
	//http://www.searchly.com/docs/php
	//Create Index (Database)
	$elasticsearch_client->indices()->create($elasticsearchJobIndexParams);

}//close of if ($elasticsearch_jobs_db_existence_status_result === false) {
		
//Load RSA 4096 bit Encryption rel Certificate and Digital Sign related Private Key
$enc_certificate = openssl_get_publickey(file_get_contents('../rsa-4096-enc-keys/enc-certificate.pem'));
$passphrase = '';
$ds_private = openssl_get_privatekey(file_get_contents('../rsa-4096-sign-keys/ds-private-key.pem'), $passphrase);	

//Load RSA	4096 bit Encryption rel Private Key and Digital Sign related Public Key
$enc_private = openssl_get_privatekey(file_get_contents('../rsa-4096-enc-keys/enc-private-key.pem'), $passphrase);
$ds_public = openssl_get_publickey(file_get_contents('../rsa-4096-sign-keys/ds-public-key.pem'));	

/* //Encrypt & Sign Start
$message = "This is the message that is to be encrypted";
echo "<br>message: \n<br>" . $message;
echo "\n\n\n";


// Encrypt the data to $encrypted using the public key
openssl_public_encrypt($message, $encrypted_data, $enc_certificate, OPENSSL_PKCS1_OAEP_PADDING);

echo "<br>Encrypted Data that will be stored in the Database: \n<br>" . $encrypted_data;
echo "\n\n\n";
//Converting the Binary String to Hex String
$hex_encoded_encrypted_data = bin2hex($encrypted_data);
echo "<br>Hex Encoded Encrypted data: \n<br>" . $hex_encoded_encrypted_data;
echo "\n\n\n";

// compute signature
openssl_sign($hex_encoded_encrypted_data, $binary_signature, $ds_private, OPENSSL_ALGO_SHA512);

echo "<br>Hex Encoded Encrypted data's Signature: \n<br>" . $binary_signature;
echo "\n\n\n<br>";

//Converting the Binary Signature to Hex String
$hex_encoded_binary_signature = bin2hex($binary_signature);
echo "<br>Hex Encoded Binary Signature: \n<br>" . $hex_encoded_binary_signature;
echo "\n\n\n<br>";


//Free Keys from the Memory
openssl_pkey_free ($enc_certificate);
openssl_pkey_free ($ds_private);
//Encrypt & Sign End */


/* //Verify Sign & Decrypt Start
$retrieved_hex_encoded_encrypted_data = "73a84cd81f355fea5867cc5c05bfaa4abeae5c43179068bd5c70531dda2d7cd6a361ba1e011fb7bc8215983ed31b1c8c0b70203b0133a401a4eb8582e463f9352914616b2b8334222fa2680e332b0286e5f39f0cfb15b19fa82eff95496a277c1fa9191258ef29a5f168e2717e47bd766a8aab85695cceca221055a515aa5f048191680eb3838d97e10b7fe7e1116f2ec08e0433afe93a3ebb8fedb62954f9beafc82481c0584cc1d56489768765766c46fab106680fc4d9b69034134366680702aaf454df4a2fb732332e47178c408209731ae1411d7758c0fb16bf49d84928c2d67112d5b77a57a166a60260baeea1c5e44e0541d320571ac169a908408a7188e8749403ae8e027f868e68b13634ceba5931b9e359040bb2f2a90e83ad94f56667386627f8301fea5f0689b356823b78fe2c4ff41373d2d0e12eaa8dbe5e8a8da674e3450bcc2bbcd359e40911d07ccd32aba00f956a29c00b9b9b049783fb11f8f352f1c03b067e8f4008075af21f9134bc1a1777c278ebd62e8b60a25f70885a48251477798b161392fd2c8df9dd8ba5e5770909e2e10b5aeddc1fa84175710c358fa20d1a0a7384fc23c524d527eb22274afd15b8fa2860d54738421873d54bbe281ae051f9314ea86144dbf7273b4e5b11c177adeadf58077c958e5e086071c9608a8173c9dd2db059a7beccb2bb881a00f589b9259539b4360c196afc";
$retrieved_hex_encoded_binary_signature = "98c5c250e7a2af4c5162a4f802410a52a5016e8c097cd2fde77788343704054222dd3da6f2437f5cd9d7ab12e7a9bb55deb4f82f2eeb8cf27aaddcef503e20145ae8302b269cb544d0d1f2d4b92a1a6126491ef75729dd9b66e0c79790ffd39c79f87961bc3eae1ce7b19184dd58560d228444a784e68dc8d9b7e63d55b621f17f91dbc59d2b3498daead2f9079828b44273c078b075a075cf3271979f35bb3b4f977822ccfcc0143bd863f2a74416704881dfc494e972c249e13b6afc68fad21654bd3ecc4ea5e6e07d455bf1a8a432ae4b58028c97df7a037b9913ae9ec23c58b9ecafeb92636e0cc6829b4fc2cb31e61e02b53b78dee976e8ce5b48c2026ae360870d57b5f162c033ef590b27cc9923eac767051c2e51546902474786f3c580e50a5194ceede94a41403eac1267571983efbb9233b6d10eff0dc8a3925cd9155385dd9ea9dd7e447111e2b99bea8b12873110c77a52927bff1b816cce6feca5c4379b1503a912c48b6a15337f18af8a75853f7c740b0622340ae484030502a540680ab55517a5b25f0c40ee592aa04436a8d10ba92b606e1f06eb85f8d3a15542a64db3a1efd6ea33836b45bcc5708a692b89ab8d0fbdfd2d8f62d5a12a2697ccf1f3cf55b9418c1ac4b1efa001c9c9a4dcdafb3fbeeae2dcb42c78a04e8f929afde9846880a8647bfd08ff391b91bf9dfa74768fe72e574995058a5770e3";
//Hex Decode Stored Signature
$binary_signature = hex2bin($retrieved_hex_encoded_binary_signature);
echo "<br>Binary Signature: \n<br>" . $binary_signature;
echo "\n\n\n<br>";

//verify signature
$signature_verification_result = openssl_verify($retrieved_hex_encoded_encrypted_data, $binary_signature, $ds_public, OPENSSL_ALGO_SHA512);

if ($signature_verification_result == 1) {
    echo "<br><br>signature ok (as it should be)\n<br>";
	
	//Converting Hex String to Binary String
	$encrypted_data = hex2bin($retrieved_hex_encoded_encrypted_data);

	echo "<br>Encrypted data: \n<br>" . $encrypted_data;
	echo "\n\n\n";

	// Decrypt the data using the private key and store the results in $decrypted
	openssl_private_decrypt($encrypted_data, $decrypted_content, $enc_private, OPENSSL_PKCS1_OAEP_PADDING);

	echo "<br>\n\ndecrypted_content: \n<br>" . $decrypted_content . "\n\n<br>";
} elseif ($signature_verification_result == 0) {
    echo "bad (there's something wrong)\n";
} else {
    echo "ugly, error checking signature\n";
}



//Free Keys from the Memory
openssl_pkey_free ($enc_private);
openssl_pkey_free ($ds_public);
//Verify Sign & Decrypt	End  */
	
/*
$eventLogFileName = "test-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
//$eventLog->log("Content-type => " . $_SERVER["CONTENT_TYPE"];
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
$eventLog->log("REQUEST URI => " . $_SERVER['REQUEST_URI']);
*/
//Include a Template Engine here, if integrating some template engine
// 1) Init
/* include '../app/template-library/krupa.php';

//Configure krupa:
$get_position_names = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/templates/' . $chosen_template . '/user-defined-positions.txt');
$get_position_names_arr = explode(",", $get_position_names);
print_r($get_position_names_arr);
$template_config = array(
	'templateDir' => $_SERVER['DOCUMENT_ROOT'].'/templates/' . $chosen_template . '/',
  'snippet_dir' => $_SERVER['DOCUMENT_ROOT'].'/templates/' . $chosen_template . '/snippets/',
  'user_defined_positions' => $get_position_names_arr,
	'useCache' => false,
	'cacheDir' => $_SERVER['DOCUMENT_ROOT'].'/templates/' . $chosen_template . '/cache/',
	'cacheTTL' => 3);
*/  
/*  $config = array(
                 //"tpl_dir"       => "templates/raintpl3/",
                 "tpl_dir"       => "templates/" . $chosen_template . "/",
                  "cache"        => "cache/",
                 "debug"         => true // set to false to improve the speed
); */

//Ajax Requests / REST Web Services: This does the loading of the respective resource for ajax / REST Web Service Requests
if (($page_is_ajax == "1") && ($page_is_frontend == "3")) {
    if ($page_filename != "not-found.php"){
	    if ($page_is_web_service_endpoint == "0"){
			//Pure Ajax Call Scenario
	        include "ajax-pages/" . $page_filename;
  	    } else if ($page_is_web_service_endpoint == "1"){
			//Web Service Endpoint Scenario
			include "rest-apis/" . "web-service-endpoint-header.php";
	        include "rest-apis/" . $page_filename;
  	    } else if ($page_is_web_service_endpoint == "2"){
			//Either Ajax Call or Web Service Endpoint Scenario (Common for BOTH)
	        include "ajax-common/" . $page_filename;
  	    } else if ($page_is_web_service_endpoint == "3"){
			//Is not an ajax request
	        include "ajax-common/" . "incorrect-route-ajax-settings.php";
  	    }
	  //include "ajax-pages/" . $page_filename;
  	}
}

//$k_te = new krupa($template_config);

//$k_te->start();

//Web Applications: This does the loading of the Modal Aspect (logic with db interaction) respective resource for regular web application requests 
if ($page_is_ajax != "1") {
include "../app/core/model-body.php";
}
// Step 5: Parse the Template

//$k_te->parseTemplate($route_filename);

// Step 6: Stop the Template Engine

//$k_te->stop();

//$t->draw($route_filename);
if ($page_is_ajax != "1") {
if (($page_filename != "rss-feeds-view.php") && ($page_is_frontend == "1")) {
   //frontend related page /template to be loaded
   include "templates/" . $chosen_frontend_template . "/header-top.php";
   
   include "../app/core/additional-config.php";
   include "templates/" . $chosen_frontend_template . "/header.php";     
 } elseif (($page_filename != "rss-feeds-view.php") && ($page_is_frontend == "0")) {
   //admin related pages / template to be loaded
   include "templates/" . $chosen_template . "/header-top.php";
   
   include "../app/core/additional-config.php";
   include "templates/" . $chosen_template . "/header.php";     
 } elseif (($page_filename != "rss-feeds-view.php") && ($page_is_frontend == "2")) {
   if (isset($_SESSION['sm_user_type']) && ($_SESSION['sm_user_type'] == "member")) {
   //frontend related page /template to be loaded
    include "templates/" . $chosen_frontend_template . "/header-top.php";
   
   include "../app/core/additional-config.php";
   include "templates/" . $chosen_frontend_template . "/header.php";
    //echo "member";
   } elseif (isset($_SESSION['sm_user_type']) && (($_SESSION['sm_user_type'] == "admin") || ($_SESSION['sm_user_type'] == "super_admin"))) {
   //admin related pages / template to be loaded 
    include "templates/" . $chosen_template . "/header-top.php";
   
   include "../app/core/additional-config.php";
   include "templates/" . $chosen_template . "/header.php";
    //echo "admin or super admin";
   } else {
   //show member related pages / template to be loaded 
    include "templates/" . $chosen_frontend_template . "/header-top.php";
   
   include "../app/core/additional-config.php";
   include "templates/" . $chosen_frontend_template . "/header.php";
    //echo "else condition";
   }     
 } elseif (($page_filename != "rss-feeds-view.php") && (($page_is_frontend != "0") || ($page_is_frontend != "1") || ($page_is_frontend != "2")) && ($page_is_frontend == "3")) {
   //inappropriate template settings reporting page
    //include "templates/incorrect-template-settings.php";
    echo "<h1>Wrong Template Settings</h1>";     
 }
 
include "../app/core/body.php";

if (($page_filename != "rss-feeds-view.php") && ($page_is_frontend == "1")) {
   //frontend related page /template to be loaded
   include "templates/" . $chosen_frontend_template . "/footer.php";     
 } elseif (($page_filename != "rss-feeds-view.php") && ($page_is_frontend == "0")) {
   //admin related pages / template to be loaded 
   include "templates/" . $chosen_template . "/footer.php";     
 } elseif (($page_filename != "rss-feeds-view.php") && ($page_is_frontend == "2")) {
   if (isset($_SESSION['sm_user_type']) && ($_SESSION['sm_user_type'] == "member")) {
   //frontend related page /template to be loaded
    include "templates/" . $chosen_frontend_template . "/footer.php";
    //echo "member";
   } elseif (isset($_SESSION['sm_user_type']) && (($_SESSION['sm_user_type'] == "admin") || ($_SESSION['sm_user_type'] == "super_admin"))) {
   //admin related pages / template to be loaded 
    include "templates/" . $chosen_template . "/footer.php";
    //echo "admin or super admin";
   } else {
   //show member related pages / template to be loaded 
    include "templates/" . $chosen_frontend_template . "/footer.php";
    //echo "else condition";
   }     
 } elseif (($page_filename != "rss-feeds-view.php") && (($page_is_frontend != "0") || ($page_is_frontend != "1") || ($page_is_frontend != "2")) && ($page_is_frontend == "3")) {
   //inappropriate template settings reporting page
    //include "templates/incorrect-template-settings.php";
    echo "<h4>Site Management</h4>";     
 }
}

ob_flush();
?>