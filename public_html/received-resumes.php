<?php

//To prevent direct access to a file inside public root or public_html or www folder, 
define("START", "No Direct Access", true);

include(dirname(dirname(__FILE__)) . "/app/core/server-var-info.php");
include(dirname(dirname(__FILE__)) . "/app/core/main-config.php");
include(dirname(dirname(__FILE__)) . "/app/core/hostname-check.php");
include(dirname(dirname(__FILE__)) . "/app/class/PDOEx.php");
include(dirname(dirname(__FILE__)) . "/app/core/db-connect-main.php");
include(dirname(dirname(__FILE__)) . "/app/includes/date-functions.php");
include(dirname(dirname(__FILE__)) . "/app/includes/other-functions-api.php");
include(dirname(dirname(__FILE__)) . "/app/includes/validate-sanitize-functions.php");

/*$stored_file = "received-input.txt";
$data = json_encode($_POST) . "\n\n";
file_put_contents($stored_file, $data, FILE_APPEND | LOCK_EX);

$stored_files = "received-files.txt";
$files_data = json_encode($_FILES) . "\n\n";
file_put_contents($stored_files, $files_data, FILE_APPEND | LOCK_EX);*/


//https://sendgrid.com/docs/for-developers/parsing-email/setting-up-the-inbound-parse-webhook/#testing

//Collect Form Post content
$email_content_array = array();

$headers = $_POST["headers"];
$dkim = $_POST["dkim"];
$content_ids = $_POST["content-ids"];
$to = $_POST["to"];
$html = $_POST["html"];
$from = $_POST["from"];
$sender_ip = $_POST["sender_ip"];
$spam_report = $_POST["spam_report"];
$envelope = $_POST["envelope"];
$attachments = $_POST["attachments"];
$subject = $_POST["subject"];
$spam_score = $_POST["spam_score"];
$attachment_info = $_POST["attachment-info"];
$charsets = $_POST["charsets"];
$spf = $_POST["SPF"];

/*$stored_file_post = "received-input-post.txt";
//$data_post = "From: " . $from . "\n" . "To: " . $to . "\n" . "HTML: " . $html . "\n";
$data_post = "From: " . $from . "\n" . "To: " . $to . "\n" . "HTML: " . $html . "\n" . "Attachments: " . $attachments . "\n" . "Attachments-info: " . $attachment_info . "\n";
//$data_post = "From: " . $from . "\n";
file_put_contents($stored_file_post, $data_post, FILE_APPEND | LOCK_EX);
*/

if ($is_pdf_format_supported == "0"){
	//Remove PDF Extension from the Allowed list of File Types
	unset($mime_types_array["pdf"]);
}

if ($is_docx_format_supported == "0"){
	//Remove DOCX Extension from the Allowed list of File Types
	unset($mime_types_array["docx"]);
}

 
//Create Numeric Index array of Mime Type Values
$mime_types_array_num_index = array_values($mime_types_array);

//Create Numeric Index array of Mime Type Keys
$mime_types_array_keys_num_index = array_keys($mime_types_array);

//Prepend a DOT (.), before each item in the Mime Type Keys array
function test_alter(&$item1, $key, $prefix)
{
	$item1 = "$prefix$item1";
}
array_walk($mime_types_array_keys_num_index, 'test_alter', '.');

//Create a Comma Separated Mime Type Keys array
$file_formats_imploded = implode(",",$mime_types_array_keys_num_index);

//echo "<pre>";
//print_r($_FILES);
//Get Count of Uploaded Files
//$files_array_count = count($_FILES['file']['name']); // multiple files upload count purpose
$files_array_count = count($_FILES);
//echo "files_array_count: " . $files_array_count . "\n<br>";

if (!empty($_FILES)) {
	for ($i=1; $i<=$files_array_count; $i++) {
		$file_ref = 'attachment' . $i;
		/*$filename = $_FILES['file']['name'][$i];
		$filedata = $_FILES['file']['tmp_name'][$i];     
		$filesize = $_FILES['file']['size'][$i];*/
		
		$filename = $_FILES[$file_ref]['name'];
		$filedata = $_FILES[$file_ref]['tmp_name'];     
		$filesize = $_FILES[$file_ref]['size'];
		$fileerror = $_FILES[$file_ref]['error'];
		//echo "filename: " . $filename . "\n<br>";
		/* $files_data .= "filename: " . $filename . "\n";
		$files_data .= "filedata: " . $filedata . "\n";
		$files_data .= "filesize: " . $filesize . "\n";
		$files_data .= "fileerror: " . $fileerror . "\n";
		 */
		//Calculate File Size in KB
		$filesize_in_kb = (int)$filesize*1024;
		
		//Calculate File Size in MB
		$filesize_in_mb = (int)$filesize*1024*1024;
		
		
		
		//Get Uploaded File Extension
		if ($filename != "") {
			//http://stackoverflow.com/a/5427229
			$file_extension = end((explode(".", $filename))); # extra () to prevent notice
		} else {
			$file_extension = "";
		}
		//echo "file_extension: " . $file_extension . "<br>";
		$files_data .= "file_extension: " . $file_extension . "\n";
		
		//Create an internal File name
		$generated_file_name = hash('sha256', uniqid(rand(), TRUE));
		$generated_file_name = $generated_file_name . "." . $file_extension;
		//echo "generated_file_name: " . $generated_file_name . "\n<br>";
		
		//if($_FILES['file']['error'][$i]==0) { 
		if($fileerror==0) { 
			if ($filedata != '') {
				$finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
				$file_mime_type = finfo_file($finfo, $filedata) . "\n";
				finfo_close($finfo);
		
				//Check Mime type of the Uploaded file
				if (in_array(trim($file_mime_type), $mime_types_array_num_index, true)) {
					//$files_data .= "mime type matched \n";
					
					/* $resume_folder_absolute_path = dirname(dirname(__FILE__)) . "/uploaded-documents/";
					$resume_folder_relative_path = "uploaded-documents/"; */

					$resume_file =  $resume_folder_absolute_path. $generated_file_name;  //from /app/core/main-config.php
					//$files_data .= "resume_file: " . $resume_file . " \n";
					
					//Uploaded Time, as per Indian Standard Time
					$uploaded_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");

					echo "before insert query\n<br>";
					//$files_data .= "before insert query: \n";
					$company_details_result = get_company_details_based_on_company_recruiter_purposed_email($to);
					
					if (count($company_details_result) > 0) {
						
						//$files_data .= "before moving uploaded file: \n";
						
						//Company Details Exist
						$company_id = $company_details_result["company_id"];
						$company_recruiter_purposed_email = $company_details_result["company_recruiter_purposed_email"];
						//$files_data .= "company_id: " . $company_id . " company_recruiter_purposed_email: " . $company_recruiter_purposed_email . " \n";
						
						//Move Uploaded File, from /tmp folder, into designated /uploaded-documents folder.
						move_uploaded_file($filedata,$resume_file);
						//$files_data .= "after moving uploaded file: \n";
						
						//Do Insert Query
						$last_inserted_id = candidate_rel_document_file_info_insert($company_id, "1", "2", null, $filesize, $filesize_in_kb, $filesize_in_mb, $original_filename_input, $generated_file_name, $_SESSION["sm_memb_id"], $uploaded_datetime, $current_epoch);
						
					} else {
						//Company Details Does not Exist. This received resume will be discarded
						//$files_data .= "when company details are not found w.r.t. : \n";
					}//close of else of if (count($company_details_result) > 0) {
						
					
					header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 200 OK');
				} else {
					//this mime type is not allowed
					echo "The file with the chosen extension is not allowed to be uploaded on this system.";	
					//$files_data .= "The file with the chosen extension is not allowed to be uploaded on this system. : \n";					
				} 	
			
			} else {
				echo "Please select a file to upload.\n";
				//$files_data .= "Please select a file to upload.\n";	
			}
		} else {
		  //for logging
		  //echo $error_types[$_FILES['file']['error']];
		  echo "There is a problem when uploading the file";
		 // $files_data .= "There is a problem when uploading the file\n";	
		}
		
	}//close of for ($i=0; $i<$files_array_count; $i++) {
	 
} else {
	echo "Files are not uploaded or files data is not collected.";
	//$files_data .= "Files are not uploaded or files data is not collected.\n";	
}//close of if (!empty($_FILES)) {
//file_put_contents($stored_files, $files_data, FILE_APPEND | LOCK_EX);



?>