<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Companies, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "4")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs
			
				 $company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : ''); 
				$page_number_input= trim(isset($ea_received_rest_ws_raw_array_input['page_number']) ? filter_var($ea_received_rest_ws_raw_array_input['page_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				$number_of_records_input= trim(isset($ea_received_rest_ws_raw_array_input['number_of_records']) ? filter_var($ea_received_rest_ws_raw_array_input['number_of_records'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
			
				$eventLog->log("company_id_input -> " . $company_id_input); 
				$eventLog->log("page_number_input -> " . $page_number_input);
				/* $eventLog->log("sort_field_input -> " . $sort_field_input); */
				
				$eventLog->log("ip_address_input -> " . $ip_address_input);
				/* $eventLog->log("sort_order_input -> " . $sort_order_input);
				$eventLog->log("search_criteria_input -> " . $search_criteria_input); */
				$eventLog->log("number_of_records_input -> " . $number_of_records_input);
				
				
				
				//Check if all inputs are received correctly from the REST Web Service
				
				 if ($company_id_input == "") {
					//Invalid company Status scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing company id.";
					
					$eventLog->log("Please provide a valid Company id.");
					 
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-additional-information";
					$response['status_description'] = "Some Additional Information like IP Address (IPv4) is missing, please check and try again.";
				} else {
					//All inputs are Valid
				
					$eventLog->log("All inputs are valid.");
				
					try { 
					    
						
						
							
						$dashboard_company_admin_candidates_invited_list_result = get_dashboard_company_admin_candidates_invited_list_with_pagination_inputs($company_id_input,$page_number_input,$number_of_records_input);
							
							
							
						
						if (count($dashboard_company_admin_candidates_invited_list_result) > 0) {
							//One or More User Groups Exist and Active
							
							$response['data'] = $dashboard_company_admin_candidates_invited_list_result;
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['status'] = "company-related-invited-candidates-list-received";
							$response['status_description'] = "company related invited candidates List Successfully Received";
							
							$eventLog->log("company-related-invited-candidates-list-received -> company related invited candidates List Successfully Received"); 
							
							 
							
						} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "active-company-related-invited-candidates-doesnot-exist";
							$response['status_description'] = "No Active invited candidates  Exist for your company, please check and try again.";
							
							//No Active company list Exist
							$eventLog->log("active-company-related-invited-candidates-doesnot-exist -> No Active invited candidates  Exist for your company, please check and try again."); 
						}//close of else of if ($company_list_result_count > "0") {
						
					} catch (Exception $e){
						$eventLog->log("Exception -> " . html_escaped_output($e->getMessage())); 
						//addLog($logFile, "Exception -> ".$e->getMessage());	
					}//close of  catch (Exception $e){
					
				
				}//close of if (($company_status_input != "0") && ($company_status_input != "1") && ($company_status_input != "")) {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
function get_dashboard_company_admin_candidates_invited_list_with_pagination_inputs($company_id_input,$page_number_input,$number_of_records_input) {

    global $dbcon, $default_number_of_records_pagination; //app/core/main-config.php
	global $eventLog;
	$constructed_array = array();
	$eventLog->log("before is_null concept");
	$job_recruitment_status = '1';

	$limit_offset_value = 0;

	if (($page_number_input == "") && ($number_of_records_input == "")) {
		//Give All Data
		//No LIMIT Clause in the Query
		$limit_offset_in_query = "";

	} else if (($page_number_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than page_number_input, till the number of records, as defined in the $default_number_of_records_pagination

		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $default_number_of_records_pagination;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$default_number_of_records_pagination;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $default_number_of_records_pagination;
		}//close of else if of if ($page_number_input == "1") {


		$eventLog->log("before page number concept");
	} else if (($page_number_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records
		$limit_offset_in_query = " LIMIT " . $number_of_records_input;

	} else {
		//Give Data, from given Record, till the requested number of records
		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $number_of_records_input;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$number_of_records_input;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $number_of_records_input;
		}//close of else if of if ($page_number_input == "1") {

	}//close of else of if (($page_number_input == "") && ($number_of_records_input == "")) {
	

    $company_admin_invited_candidates_list_count_get_sql = "SELECT COUNT(DISTINCT invitation_received_by_user_id) AS count  FROM `job_applicant_invites` WHERE `company_id`=:company_id";
	$company_admin_invited_candidates_list_count_get_select_query = $dbcon->prepare($company_admin_invited_candidates_list_count_get_sql);
	$company_admin_invited_candidates_list_count_get_select_query->bindValue(":company_id",$company_id_input);
	
	
	$company_admin_invited_candidates_list_count_get_select_query->execute();

    $company_admin_invited_candidates_list_get_sql = "SELECT DISTINCT invitation_received_by_user_id FROM `job_applicant_invites` WHERE `company_id`=:company_id" . $limit_offset_in_query;
    $company_admin_invited_candidates_list_get_select_query = $dbcon->prepare($company_admin_invited_candidates_list_get_sql);
	$company_admin_invited_candidates_list_get_select_query->bindValue(":company_id",$company_id_input);
	
	$company_admin_invited_candidates_list_get_select_query->execute();
	
		


	//Process / Get Company clients Count
	$eventLog->log("get the row count value");
	if($company_admin_invited_candidates_list_count_get_select_query->rowCount() > 0) {
	    $company_admin_invited_candidates_list_count_get_select_query_result = $company_admin_invited_candidates_list_count_get_select_query->fetch();
	    //print_r($company_clients_list_count_get_select_query_result);

		$total_company_admin_invited_candidates_count = $company_admin_invited_candidates_list_count_get_select_query_result["count"];
		$constructed_array["total_records_count"] = $total_company_admin_invited_candidates_count;

	}//close of if($company_clients_list_count_get_select_query->rowCount() > 0) {


	//Process / Fetch Companies List
	if($company_admin_invited_candidates_list_get_select_query->rowCount() > 0) {
	    $company_admin_invited_candidates_list_get_select_query_result = $company_admin_invited_candidates_list_get_select_query->fetchAll();
	    //print_r($companies_list_get_select_query_result);
	    $eventLog->log("retrive the details ");
		foreach ($company_admin_invited_candidates_list_get_select_query_result as $company_admin_invited_candidates_list_get_select_query_result_row) {

			$temp_row_array = array();
            $temp_row_array["company_id"] =$company_id_input;
		    $temp_row_array["invitation_received_by_user_id"] = $company_admin_invited_candidates_list_get_select_query_result_row["invitation_received_by_user_id"];
			$invitation_received_by_user_id = $temp_row_array["invitation_received_by_user_id"];
			$candidate_details_result = get_candidate_details($invitation_received_by_user_id);
			
			if(count($candidate_details_result)>0)
			{	
			 $temp_row_array["invitation_received_by_user_first_name"] = $candidate_details_result["sm_firstname"];
			 $temp_row_array["invitation_received_by_user_middle_name"] = $candidate_details_result["sm_middlename"];
			 $temp_row_array["invitation_received_by_user_last_name"] = $candidate_details_result["sm_lastname"];
			} else {
				
				$temp_row_array["invitation_received_by_user_first_name"] = "";
				$temp_row_array["invitation_received_by_user_middle_name"] = "";
				$temp_row_array["invitation_received_by_user_last_name"] = "";
			} 
			if($temp_row_array["invitation_received_by_user_first_name"] == ""){
				
				$temp_row_array["invitation_received_by_user_first_name"] = null;
				
			}
			if($temp_row_array["invitation_received_by_user_middle_name"] == ""){
				
				$temp_row_array["invitation_received_by_user_middle_name"] = null;
				
			}
			if($temp_row_array["invitation_received_by_user_last_name"] == ""){
				
				$temp_row_array["invitation_received_by_user_last_name"] = null;
				
			}
			$temp_row_array["invitation_received_by_user_full_name"] = $temp_row_array["invitation_received_by_user_first_name"] ." ". $temp_row_array["invitation_received_by_user_middle_name"] ."".
			$temp_row_array["invitation_received_by_user_last_name"];
			
			$candidate_details_get = candidate_details_get_based_on_id($invitation_received_by_user_id);
			$temp_row_array["invitation_status"] = $candidate_details_get["is_active_status"];
			$invitation_status = $temp_row_array["invitation_status"];
			if($invitation_status == "1")
			{ 
		     $temp_row_array["invitation_status"] ="invite enabled";
			}  
			 else if($invitation_status == "0")
			{ 
		     $temp_row_array["invitation_status"] ="invite disabled";
			} 
			
			$temp_row_array["invitation_sent_by_user_id"] = $candidate_details_get["invitation_sent_by_user_id"]; 
			$invitation_sent_by_user_id = $temp_row_array["invitation_sent_by_user_id"];
			 $candidate_values = get_candidate_details_based_on_user_id($invitation_sent_by_user_id);
			if(count($candidate_values)>0)
			{	
			 $temp_row_array["invitation_sent_by_user_first_name"] = $candidate_values["sm_firstname"];
			 $temp_row_array["invitation_sent_by_user_middle_name"] = $candidate_values["sm_middlename"];
			 $temp_row_array["invitation_sent_by_user_last_name"] = $candidate_values["sm_lastname"];
			} else {
				
				$temp_row_array["invitation_sent_by_user_first_name"] = "";
				$temp_row_array["invitation_sent_by_user_middle_name"] = "";
				$temp_row_array["invitation_sent_by_user_last_name"] = "";
			} 
			if($temp_row_array["invitation_sent_by_user_first_name"] == ""){
				
				$temp_row_array["invitation_sent_by_user_first_name"] = null;
				
			}
			if($temp_row_array["invitation_sent_by_user_middle_name"] == ""){
				
				$temp_row_array["invitation_sent_by_user_middle_name"] = null;
				
			}
			if($temp_row_array["invitation_sent_by_user_last_name"] == ""){
				
				$temp_row_array["invitation_sent_by_user_last_name"] = null;
				
			} 
             $temp_row_array["invitation_sent_by_user_full_name"] = $temp_row_array["invitation_sent_by_user_first_name"] ." ". $temp_row_array["invitation_sent_by_user_middle_name"] ."".
			$temp_row_array["invitation_sent_by_user_last_name"];
		    $constructed_array["list"][] = $temp_row_array;
	    }//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

	}//close of if($companies_list_get_select_query->rowCount() > 0) {
		$eventLog->log("all input values are recives");
	return $constructed_array;
}
function get_candidate_details($invitation_received_by_user_id) {
	global $dbcon, $eventLog;
	$constructed_array = array();
    
	$candidate_invited_jobs_get_sql = "SELECT * FROM `site_members` WHERE `sm_memb_id` =:sm_memb_id";
	$candidate_invited_jobs_select_query = $dbcon->prepare($candidate_invited_jobs_get_sql);
    $candidate_invited_jobs_select_query->bindValue(":sm_memb_id",$invitation_received_by_user_id);
	
      $candidate_invited_jobs_select_query->execute();
	  
	if($candidate_invited_jobs_select_query->rowCount() > 0) {
		$candidate_invited_jobs_select_query_result = $candidate_invited_jobs_select_query->fetch();
		return $candidate_invited_jobs_select_query_result;
	
	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
	
	
}
function get_candidate_details_based_on_user_id($invitation_sent_by_user_id) {
	global $dbcon, $eventLog;
	$constructed_array = array();
    
	$candidate_invited_jobs_get_sql = "SELECT * FROM `site_members` WHERE `sm_memb_id` =:sm_memb_id";
	$candidate_invited_jobs_select_query = $dbcon->prepare($candidate_invited_jobs_get_sql);
    $candidate_invited_jobs_select_query->bindValue(":sm_memb_id",$invitation_sent_by_user_id);
	
      $candidate_invited_jobs_select_query->execute();
	  
	if($candidate_invited_jobs_select_query->rowCount() > 0) {
		$candidate_invited_jobs_select_query_result = $candidate_invited_jobs_select_query->fetch();
		return $candidate_invited_jobs_select_query_result;
	
	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
	
	
}
function candidate_details_get_based_on_id($invitation_received_by_user_id) {
	global $dbcon, $eventLog;
	$constructed_array = array();
    
	$candidate_invited_jobs_get_sql = "SELECT * FROM `job_applicant_invites` WHERE `invitation_received_by_user_id` =:invitation_received_by_user_id";
	$candidate_invited_jobs_select_query = $dbcon->prepare($candidate_invited_jobs_get_sql);
    $candidate_invited_jobs_select_query->bindValue(":invitation_received_by_user_id",$invitation_received_by_user_id);
	
      $candidate_invited_jobs_select_query->execute();
	  
	if($candidate_invited_jobs_select_query->rowCount() > 0) {
		$candidate_invited_jobs_select_query_result = $candidate_invited_jobs_select_query->fetch();
		return $candidate_invited_jobs_select_query_result;
	
	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
	
	
}


exit;
?>