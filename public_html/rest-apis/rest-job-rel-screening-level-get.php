<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Edit Quick Admin Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "2")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_info_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_info_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_info_id'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$jsl_info_id_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_info_id']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_info_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				
				if ($jsl_info_id_input == "") {
					//Invalid Mobile Number scenario
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-jsl-info-id";
					$response['status_description'] = "invalid JSL info id, please check and try again.";
					
					$eventLog->log("Please provide a valid jsl info id.");
					
				
				
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Some Additional Information is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//Event Time, as per Indian Standard Time
					$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
		
					$company_proceed_next_step = "";
					
					
					if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$company_proceed_next_step = "PROCEED-TO-NEXT-STEP";
								
							//} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {	
							} else if (isset($ea_extracted_jwt_token_user_company_id)) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$company_proceed_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
					} else {
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
					}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
							
					if ($company_proceed_next_step == "PROCEED-TO-NEXT-STEP") {
						
							try {
								
								$jsl_info_array = array();
								$jsl_info_array["screening_level_details"] = jsl_info_get($jsl_info_id_input);
								$jsl_info_array["screening_level_rel_documents"] = jsl_document_check($jsl_info_id_input);
								$jsl_info_array["screening_level_rel_assignees"] = jsl_assignees_check($jsl_info_id_input);
								//$jsl_info_result = jsl_document_check($jsl_info_id_input);	
								
								    //$jsl_info_result = jsl_info_get($jsl_info_id_input);
								    if(count($jsl_info_array) > 0){
										  
										$response['data'] = $jsl_info_array;
										$response['status'] = "screening-level-details-successfully-fetched";
										$response['status_description'] = "screening level details fetched Successfully.";

										$eventLog->log("screening level details fetched Successfully.");
													     
									} else {
									
										$response['data'] = array();
										$response['status'] = "no-screening-level-details-found";
										$response['status_description'] = "No Screening Level details found for this Job.";
										
										$eventLog->log("No job details found for this Job.");
													
									}			
								} catch (Exception $e) {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "job-screening-level-details-fetching-error";
									$response['status_description'] = "There is an error, when fetching job screening level details.";
									
									$eventLog->log("There is an error, when fetching job screening level details.");	
									
								}
					   
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "The User Account, that is attempted to be edited, is invalid, please check and try again.";
						
						$eventLog->log("Please provide a Valid user ID.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
					
						
				}//close of else of if ($email_id_input == "") {
				
			}
			
		}	
			
	} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function jsl_info_get($jsl_info_id) {
	global $dbcon;
	$constructed_array = array();
	$job_details_get_sql = "SELECT * FROM `jsl_info` WHERE jsl_info_id =:jsl_info_id";
	$job_details_ge_select_query = $dbcon->prepare($job_details_get_sql);
	$job_details_ge_select_query->bindValue(":jsl_info_id",$jsl_info_id);	
	$job_details_ge_select_query->execute(); 
	
	if($job_details_ge_select_query->rowCount() > 0) {
		$job_details_ge_select_query_result = $job_details_ge_select_query->fetchAll();
		foreach ($job_details_ge_select_query_result as $job_details_ge_select_query_result_row) {
		
			$constructed_array["jsl_info_id"] = $job_details_ge_select_query_result_row["jsl_info_id"];
			$constructed_array["company_id"] = $job_details_ge_select_query_result_row["company_id"];
			$constructed_array["company_client_id"] = $job_details_ge_select_query_result_row["company_client_id"];
		    $constructed_array["job_id"] = $job_details_ge_select_query_result_row["job_id"];
			$constructed_array["jsl_title"] = $job_details_ge_select_query_result_row["jsl_title"];
			$constructed_array["jsl_seo_title"] = $job_details_ge_select_query_result_row["jsl_seo_title"];
			$constructed_array["jsl_classification_detail_id"] = $job_details_ge_select_query_result_row["jsl_classification_detail_id"];
			$constructed_array["jsl_classification_detail_name"] = $job_details_ge_select_query_result_row["jsl_classification_detail_name"];
			
			$constructed_array["jsl_classification_detail_seo_name"] = $job_details_ge_select_query_result_row["jsl_classification_detail_seo_name"];
		  
			$constructed_array["jsl_sub_classification_detail_id"] = $job_details_ge_select_query_result_row["jsl_sub_classification_detail_id"];
			$constructed_array["jsl_sub_classification_detail_name"] = $job_details_ge_select_query_result_row["jsl_sub_classification_detail_name"];
			$constructed_array["jsl_sub_classification_detail_seo_name"] = $job_details_ge_select_query_result_row["jsl_sub_classification_detail_seo_name"];
			$constructed_array["jsl_owned_by"] = $job_details_ge_select_query_result_row["jsl_owned_by"];
			
			$classification_details_get_result = classification_details_get($constructed_array["jsl_classification_detail_id"]);
			if(count($classification_details_get_result) > 0) {
				$constructed_array["jsl_cen_job_specific_override_allowance"] = $classification_details_get_result["jsl_cen_job_specific_override_allowance"];
				$constructed_array["jsl_csn_job_specific_override_allowance"] = $classification_details_get_result["jsl_csn_job_specific_override_allowance"];
				$constructed_array["jsl_ren_job_specific_override_allowance"] = $classification_details_get_result["jsl_ren_job_specific_override_allowance"];
				$constructed_array["jsl_rsn_job_specific_override_allowance"] = $classification_details_get_result["jsl_rsn_job_specific_override_allowance"];
				
            } else {
				
				$constructed_array["jsl_cen_job_specific_override_allowance"] = null;
				$constructed_array["jsl_csn_job_specific_override_allowance"] = null;
				$constructed_array["jsl_ren_job_specific_override_allowance"] = null;
				$constructed_array["jsl_rsn_job_specific_override_allowance"] = null;

			}
			$constructed_array["jsl_j_s_candidate_email_notification_setting"] = $job_details_ge_select_query_result_row["jsl_j_s_candidate_email_notification_setting"];
			$constructed_array["jsl_j_s_candidate_sms_notification_setting"] = $job_details_ge_select_query_result_row["jsl_j_s_candidate_sms_notification_setting"];
			$constructed_array["jsl_j_s_recruiter_email_notification_setting"] = $job_details_ge_select_query_result_row["jsl_j_s_recruiter_email_notification_setting"];
			$constructed_array["jsl_j_s_recruiter_sms_notification_setting"] = $job_details_ge_select_query_result_row["jsl_j_s_recruiter_sms_notification_setting"];
			$constructed_array["jsl_event_deadline_requirement_status"] = $job_details_ge_select_query_result_row["jsl_event_deadline_requirement_status"];
			$constructed_array["jsl_event_deadline_requirement_status"] = $job_details_ge_select_query_result_row["jsl_event_deadline_requirement_status"];
			
			$constructed_array["jsl_event_deadline_datetime"] = $job_details_ge_select_query_result_row["jsl_event_deadline_datetime"];
			$constructed_array["jsl_event_deadline_datetime_epoch"] = $job_details_ge_select_query_result_row["jsl_event_deadline_datetime_epoch"];
			
			$constructed_array["jsl_event_deadline_country_two_lett_code"] = $job_details_ge_select_query_result_row["jsl_event_deadline_country_two_lett_code"];
			$constructed_array["jsl_event_deadline_iana_timezone"] = $job_details_ge_select_query_result_row["jsl_event_deadline_iana_timezone"];
			$constructed_array["jsl_event_deadline_def_datetime"] = $job_details_ge_select_query_result_row["jsl_event_deadline_def_datetime"];
			
			
			$constructed_array["jsl_sequence_order"] = $job_details_ge_select_query_result_row["jsl_sequence_order"];
			$constructed_array["added_datetime"] = $job_details_ge_select_query_result_row["added_datetime"];
			$constructed_array["added_datetime_epoch"] = $job_details_ge_select_query_result_row["added_datetime_epoch"];
			$constructed_array["added_by_sm_memb_id"] = $job_details_ge_select_query_result_row["added_by_sm_memb_id"];
			$constructed_array["is_active_status"] = $job_details_ge_select_query_result_row["is_active_status"];
		
	    }

		return $constructed_array;
	}
	return $constructed_array;
}

function jsl_document_check($jsl_info_id) {
	global $dbcon;
	$constructed_array = array();
	$is_active_status = "1";
	$job_details_get_sql = "SELECT * FROM `jsl_rel_document_check_initial_criteria` WHERE jsl_info_id =:jsl_info_id AND `is_active_status`=:is_active_status";
	$job_details_ge_select_query = $dbcon->prepare($job_details_get_sql);
	$job_details_ge_select_query->bindValue(":jsl_info_id",$jsl_info_id);	
	$job_details_ge_select_query->bindValue(":is_active_status",$is_active_status);
	$job_details_ge_select_query->execute(); 
	
	if($job_details_ge_select_query->rowCount() > 0) {
		$job_details_ge_select_query_result = $job_details_ge_select_query->fetchAll();
		
		return $job_details_ge_select_query_result;
	}
	return $constructed_array;
}
function classification_details_get($jsl_classification_detail_id) {
	
	global $dbcon;
	$constructed_array = array();
	$is_active_status = "1";
	$job_details_get_sql = "SELECT * FROM `jsl_classification_details` WHERE `jsl_classification_detail_id` = :jsl_classification_detail_id AND `is_active_status`=:is_active_status";
	$job_details_ge_select_query = $dbcon->prepare($job_details_get_sql);
	$job_details_ge_select_query->bindValue(":jsl_classification_detail_id",$jsl_classification_detail_id);	
	$job_details_ge_select_query->bindValue(":is_active_status",$is_active_status);
	$job_details_ge_select_query->execute(); 
	
	if($job_details_ge_select_query->rowCount() > 0) {
		$job_details_ge_select_query_result = $job_details_ge_select_query->fetch();
		
		return $job_details_ge_select_query_result;
	}
	return $constructed_array;
	
}

function jsl_assignees_check($jsl_info_id) {
	global $dbcon;
	$constructed_array = array();
	$is_active_status = "1";
	$job_details_get_sql = "SELECT * FROM `jsl_recruiter_mapping` WHERE jsl_info_id =:jsl_info_id AND `is_active_status`=:is_active_status";
	$job_details_ge_select_query = $dbcon->prepare($job_details_get_sql);
	$job_details_ge_select_query->bindValue(":jsl_info_id",$jsl_info_id);	
	$job_details_ge_select_query->bindValue(":is_active_status",$is_active_status);
	$job_details_ge_select_query->execute(); 
	
	if($job_details_ge_select_query->rowCount() > 0) {
		$job_details_ge_select_query_result = $job_details_ge_select_query->fetchAll();
		
		return $job_details_ge_select_query_result;
	}
	return $constructed_array;
	
}

	

exit;
?>


