<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * REST API for change password
 *
 */
$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "5")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				if (is_array($ea_received_rest_ws_raw_array_input)) {
				$content = "";
				
					if (isset($ea_received_rest_ws_raw_array_input['user_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['user_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['user_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['current_password'])) {
						$content .= $ea_received_rest_ws_raw_array_input['current_password'] . ":::::";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['current_password'])) {
					
					if (isset($ea_received_rest_ws_raw_array_input['new_password'])) {
						$content .= $ea_received_rest_ws_raw_array_input['new_password'] . ":::::";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['new_password'])) {
					
					if (isset($ea_received_rest_ws_raw_array_input['confirm_new_password'])) {
						$content .= $ea_received_rest_ws_raw_array_input['confirm_new_password'] . ":::::";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['confirm_new_password'])) {
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$current_password_input = trim(isset($ea_received_rest_ws_raw_array_input['current_password']) ? filter_var($ea_received_rest_ws_raw_array_input['current_password'], FILTER_SANITIZE_STRING) : '');
				
				$new_password_input = trim(isset($ea_received_rest_ws_raw_array_input['new_password']) ? filter_var($ea_received_rest_ws_raw_array_input['new_password'], FILTER_SANITIZE_STRING) : '');
				
				$confirm_new_password_input = trim(isset($ea_received_rest_ws_raw_array_input['confirm_new_password']) ? filter_var($ea_received_rest_ws_raw_array_input['confirm_new_password'], FILTER_SANITIZE_STRING) : '');
			
				if (($user_id_input == '0') || (!ctype_digit($user_id_input))) {
						$eventLog->log($user_id_input . " - Not a Valid User ID");
						$user_id_input = "";
				}//close of else if of if (($user_id_input == '0') || (!ctype_digit($user_id_input))) {
						
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//Check if all inputs are received correctly from the REST Web Service
				if ($user_id_input == "") {
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-user-id";
					$response['status_description'] = "Invalid user id, please check and try again.";
					
					$eventLog->log("Please provide a Valid User ID.");
					
				} else if ($current_password_input == "") {
					//Invalid Current Password scenario
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-current-password";
					$response['status_description'] = "Invalid current password, please check and try again.";
					
					$eventLog->log("Please provide a valid current password.");
					
				} else if (mb_strlen($new_password_input) < $password_min_length) {
					//Shorter New Password Length scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "shorter-new-password-length";
					$response['status_description'] = "The submitted New Password is < " . $password_min_length . " Characters, Please use a Password that is >= " . $password_min_length . " Characters.";
					
					$eventLog->log("The submitted New Password is < " . $password_min_length . " Characters, Please use a Password that is >= " . $password_min_length . " Characters.");	
					
				} else if ($new_password_input != $confirm_new_password_input) {
					//Mis-matching New Password / Confirm New Password Inputs scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "mis-matching-new-password-and-confirmation";
					$response['status_description'] = "Mis-matching New Password and it's Confirmation, please check and try again.";
					
					$eventLog->log("Please provide Matching New Password and it's Confirmation inputs.");
				
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-additional-information";
					$response['status_description'] = "Some Additional Information like IP Address (IPv4) is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");
							  
				} else if ($user_id_input != $ea_extracted_jwt_token_sub) {
					//Mis-matching Submitted User ID / User ID from the JWT Token scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "in-correct-user-id-submitted";
					$response['status_description'] = "In-correct User ID is Submitted, please check and try again.";
					
					$eventLog->log("In-correct User ID is Submitted, please check and try again.");
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					$ea_action_specific_auth_check_result = "";
					
					//User Details, based on User ID input, through api request
					$user_basic_details_result = user_basic_details_check_based_on_user_id($user_id_input);
					
					if (count($user_basic_details_result) > 0) {
						
						$user_rel_company_id = $user_basic_details_result["company_id"];
						$user_rel_sm_user_status = $user_basic_details_result["sm_user_status"];
						
						//$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
						$ea_action_specific_auth_check_result = ea_check_action_specific_user_privileges_on_different_user_levels($ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_user_company_id, $ea_extracted_jwt_token_sub, $user_id_input, $ea_extracted_jwt_token_user_privileges_list, $user_rel_company_id, $page_filename);					
						
						//echo "ea_action_specific_auth_check_result: " . $ea_action_specific_auth_check_result;					
							
						if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
					
							//Check if provided Current Password is Valid, for the received user_id input
					        $user_account_current_password_verification_status_result = ea_check_user_current_password_based_on_user_id($user_id_input, $current_password_input);
					
							if ((isset($user_account_current_password_verification_status_result["user_status"])) && ($user_account_current_password_verification_status_result["user_status"] == "1")) {
						
							//Check the User Account's Current Password Verification Status
							if ((isset($user_account_current_password_verification_status_result["current_password_verification_status"])) && ($user_account_current_password_verification_status_result["current_password_verification_status"] == "1")) {
							
							//Create Password hash of the New Password, w.r.t. the User Account, as per the process
							$created_password_hash = password_hash($new_password_input, PASSWORD_DEFAULT);
							
							//Event Time, as per Indian Standard Time
							$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
							
							
							try {
								
								$user_password_update_sql = "UPDATE `site_members` SET `sm_password`=:sm_password,`last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch WHERE `sm_memb_id`=:sm_memb_id";
								
								$user_password_update_values_array = array(":sm_password" => $created_password_hash, ":last_updated_date_time" => $event_datetime, ":last_updated_date_time_epoch" => $current_epoch, ":sm_memb_id" => $user_id_input);
								
								$user_password_update_status = update_query_based_on_id($user_password_update_sql, $user_password_update_values_array);
								
								//echo "user_password_update_status: " . var_dump($user_password_update_status);
								if ($user_password_update_status == true) {
									
									ea_update_user_rel_active_jwt_token_status_based_on_user_id($user_id_input);
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "password-changed-successfully";
									$response['status_description'] = "Password changed successfully.";
									
								} else {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "error-password-change-process.";
									$response['status_description'] = "There occurred an error when Password is being Changed w.r.t. particular User Account. Please check and try again later.";
									
								}//close of else of if ($user_password_update_status == true) {
								
							} catch (Exception $e) {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "error-password-change-process";
								$response['status_description'] = "Password change error.";
								
							}
							
							
						} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "in-correct-current-password-submitted";
							$response['status_description'] = "In-valid current password.";
							
						}//close of else of if ((isset($user_account_current_password_verification_status_result["current_password_verification_status"])) && ($user_account_current_password_verification_status_result["current_password_verification_status"] == "1")) {
						
						
					} else if ((isset($user_account_current_password_verification_status_result["user_status"])) && ($user_account_current_password_verification_status_result["user_status"] == "0")) {
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "in-active-user-account-current-feature-not-authorized";
						$response['status_description'] = "Change Password feature is not authorized for this User, as User Account is still In-active at this moment, please check and try again after activating the Account.";
						
						//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application
						header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
						
					} else if ((isset($user_account_current_password_verification_status_result["user_status"])) && ($user_account_current_password_verification_status_result["user_status"] == "2")) {
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "suspended-user-account-current-feature-not-authorized";
						$response['status_description'] = "Change Password feature is not authorized for this User, as User Account is still Suspended at this moment.";
						
						//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application
						header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
						
					} else if ((isset($user_account_current_password_verification_status_result["user_status"])) && ($user_account_current_password_verification_status_result["user_status"] == "3")) {
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "banned-user-account-current-feature-not-authorized";
						$response['status_description'] = "Change Password feature is not authorized for this User, as User Account is still Banned at this moment.";
						
						//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application
					
					    header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
				
					}
				} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-subject-platform-scope-user";
							$response['status_description'] = "Insufficient Set of Permissions, to complete this action, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
				} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-different-company-user-id";
							$response['status_description'] = "Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.");	
							
				} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-INSUFFICIENT-PERMISSIONS") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.");	
							
				} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-self-action";
							$response['status_description'] = "Insufficient Set of Permissions, as self action is Dis-allowed, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
				} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-ACTION-SUBJECT-IS-A-DIFFERENT-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-action-on-different-user";
							$response['status_description'] = "Insufficient Set of Permissions, as action subject is a different user, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						}//close of if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							
				} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "The User Account is Invalid, please check and try again.";
						
						//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application
						header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
						
					}//close of else of if ((isset($user_account_current_password_verification_status_result["user_status"])) && ($user_account_current_password_verification_status_result["user_status"] == "1")) {
						
					
					
					
				}//close of else of if ($user_id_input == "") {
				
			}//close of if ($ea_is_user_page_access_authorized) {
			
	    } else {
			
			/*//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['status'] = "access-forbidden";
			$response['status_description'] = "Resources that require a different set of access permissions are requested, please contact admin, if access to these resources are required.";
			
			//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			*/
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
	
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "5")) {


//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){

exit;
?>