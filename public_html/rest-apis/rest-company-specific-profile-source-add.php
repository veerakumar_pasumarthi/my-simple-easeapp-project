
<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['company_specific_profile_source'])) {
						$content .= json_encode($ea_received_rest_ws_raw_array_input['company_specific_profile_source']) . "\r\n";
					}
					
					/* if (isset($ea_received_rest_ws_raw_array_input['company_specific_external_jsl_classifications'])) {
						$content .= json_encode($ea_received_rest_ws_raw_array_input['company_specific_external_jsl_classifications']) . "\r\n";
					} */
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
				}
					
					$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					if($ea_received_rest_ws_raw_array_input['company_specific_profile_source'] == ""){
						$company_specific_profile_source_input = array();
					} else{
					$company_specific_profile_source_input = $ea_received_rest_ws_raw_array_input['company_specific_profile_source'];
				}
					
					
					if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
					} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
					}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						
					
					
					
					if ($company_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company ID";
					
					$eventLog->log("missing-company-id: Please provide a valid Company ID.");
					
					 } else if (count($company_specific_profile_source_input) == 0) {	
					//Invalid Education Qualifications Received scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "empty-company-specific-profile-source-input";
					$response['status_description'] = "Empty company specific profile source input,Please provide atleast one input ";
					
					$eventLog->log("empty-company-specific-profile-source-input: Empty company specific profile source input.");
					 
					/* } else if ($company_specific_external_jsl_classifications_input == "") {	
					//Invalid Education Qualifications Received scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "empty-job-rel-assignees-input";
					$response['status_description'] = "Empty Job rel assignees Input";
					
					$eventLog->log("empty-job-rel-assignees-input: Empty Job rel assignees Input, Please check and try again.");
					 */
					
					} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Missing Some Additional Information";
					
					$eventLog->log("missing-some-additional-information: Some Additional Information is missing, please check and try again.");	
				
					
					
					
				    } else {	
						
				        $company_specific_profile_source_add_next_step = "";
						
							if ($ea_extracted_jwt_token_user_type == "admin") {
								
								if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
									$company_specific_profile_source_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("before company id condition");
								} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
									 $eventLog->log("job related condition");
									$company_specific_profile_source_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("job related condition"); 
								} else {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "insufficient-permissions-different-company-user-id";
									$response['status_description'] = "Insufficient Set of Permissions";
									
									//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
									header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
									$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
									
								}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								
								
								
							} else {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
								$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
							
							}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
								
							$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
							 $eventLog->log("before proceed to next next step condition");
							 
							 
							if ($company_specific_profile_source_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
								try {
							
									if(count($company_specific_profile_source_input) > 0 ){
											$count = count($company_specific_profile_source_input);
										} else {
											$count = 0 ;
										}

									if ((is_array($company_specific_profile_source_input)) && (count($company_specific_profile_source_input) > 0)) {
										
										$eventLog->log("enter into condition");	
													
										/* $count = count($company_specific_profile_source_input);
										$row_count = 0; */
										/* foreach ($company_specific_profile_source_input as $company_specific_profile_source_input_row) {
											$profile_source_id = $company_specific_profile_source_input_row["profile_source_id"]; */
										/*	if(count($company_specific_profile_source_input) > 0 ){
											$count = count($company_specific_profile_source_input);
										} else {
											$count = 0 ;
										} */
										$row_count = 0;
										for($i = 0;$i< count($company_specific_profile_source_input);$i++) {
											
											$profile_source_name = $company_specific_profile_source_input[$i];
											$eventLog->log($profile_source_name);
											
											$company_specific_profile_source_duplicate_check_result = company_specific_profile_source_duplicate_check($company_id_input,$profile_source_name);
							
											$eventLog->log("company_specific_profile_source_duplicate_check_result");
											
											if (count($company_specific_profile_source_duplicate_check_result) > 0) {
												$is_active_status =$company_specific_profile_source_duplicate_check_result["is_active_status"];
												
												
												if($is_active_status == "0") {
													
													$cspf_id =$company_specific_profile_source_duplicate_check_result["cspf_id"];
													
													$count--;
													
													$is_active_status = "1";
													$company_specific_profile_source_change_status_result = company_specific_profile_source_change_status($cspf_id,$is_active_status);
													
													if($company_specific_profile_source_change_status_result == true) {
														$eventLog->log($cspf_id);
														$eventLog->log("update case dealed successfully");
														
													} else {
														
														$eventLog->log("update case dealing error");
													}
													
													
													
													
												} else {
													
													$eventLog->log("company-specific-profile-source-already-exists for this id".$profile_source_name);	
													
													//$eventLog->log("job-applicant-invite-already-exists: job applicant invite already exists");	
													$count--;
													continue;
													
												}
											
											} else {
												
												$company_specific_profile_source_details_result =company_specific_profile_source_details_based_on_profile_source_name($profile_source_name);
					
												$profile_source_id = $company_specific_profile_source_details_result["profile_source_id"];
												$profile_source_seo_name = $company_specific_profile_source_details_result["profile_source_seo_name"];
												
											
												$last_inserted_id = company_specific_profile_source_details_insert($company_id_input,$profile_source_id,$profile_source_name,$profile_source_seo_name);
									 
											
												$eventLog->log($last_inserted_id);
												$row_count++;
												$eventLog->log($row_count);
												
											}
											
										}
										$db_collected_company_specific_profile_source_list_result = company_specific_profile_source_list($company_id_input);

											foreach($db_collected_company_specific_profile_source_list_result as $db_collected_company_specific_profile_source_list_result_row) {
										
												$db_profile_source_name = $db_collected_company_specific_profile_source_list_result_row["profile_source_name"];
												$cspf_id = $db_collected_company_specific_profile_source_list_result_row["cspf_id"];
												
												if(in_array($db_profile_source_name,$company_specific_profile_source_input)){

															$eventLog->log("NO need to delete");
															
												} else {
													
													$is_active_status = "0";
													$company_specific_profile_source_change_status_result = company_specific_profile_source_change_status($cspf_id,$is_active_status);
													
													if($company_specific_profile_source_change_status_result == true) {
														$eventLog->log($cspf_id);
														$eventLog->log("delete case dealed successfully");
														
													} else {
														
														$eventLog->log("delete case dealing error");
													}
													
													
												}
												
											}
										}											
										
									//	$eventLog->log("count".$count);
									//	$eventLog->log("row_count".$row_count);
										if ($count == $row_count) {
											
												$response['data'] = array();
												$response['status'] = "company-specific-profile-source-insertion-successful";
												$response['status_description'] = "company specific profile source is added successfully";
											
												$eventLog->log("company specific profile source is added successfully.");   
											
										} else {
											//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
											$response['data'] = array();
											$response['status'] = "company-specific-profile-source-insertion-error";
											$response['status_description'] = "Error occurred when  adding company specific profile source";
											
											$eventLog->log("Error occurred when  adding company specific JSL classifications");
											
										}//close of else of if (count($user_ass_oc_validation_result) > 0) { 
										
										
									/* } else {
											$response['data'] = array();
											$response['status'] = "company-specific-profile-source-insertion-error";
											$response['status_description'] = "Error occurred when  adding company specific profile source";
											
											$eventLog->log("Error occurred when  adding company specific JSL classifications");
										
										
									} */
									
										
										
									
										
								} catch (Exception $e) {
										$response['data'] = array();
										$response['status'] = "company-specific-profile-source-insertion-error";
										$response['status_description'] = "Error occurred when  adding company profile source";
										
										$eventLog->log("Error occurred when  adding company specific profile");
										
								}
							}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
							
						}
			//}//close of else of if ($email_id_input == "") {
					
		}//close of //close of if ($ea_is_user_page_access_authorized) {
					
				
	} else {
				
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "invalid-auth-token-submitted";
				$response['status_description'] = "Invalid Authentication";
				
				//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
				header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
				
				$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
				
			}//close of else of if ($ea_auth_token_validation_status) {
		
		}//close of if ($ea_maintenance_mode == false) {
		
} else {

		//Construct Content, that will be sent in Response body, of the REST Web Service
		$response['data'] = array();
		$response['status'] = "invalid-input";
		$response['status_description'] = "Invalid Input, Please check and provide all information.";
		
		//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
		
		$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
				
		
		}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




	//Check if Maintenance Mode is Turned On
	if ($ea_maintenance_mode) {	
		
		//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
		//header('Maintenance-Progress: true', false);
		//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
		$response['data'] = array();
		$response['status'] = "application-maintenance-in-progress";
		$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
		
		$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
		
	} else {	

		//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
		header('Content-Type: application/json');
		echo json_encode($response,JSON_PRETTY_PRINT);
		
	}//close of else of if ($ea_maintenance_mode){
		
function company_specific_profile_source_details_insert($company_id_input,$profile_source_id,$profile_source_name,$profile_source_seo_name) {
	
	
	global $dbcon,$eventLog;
	
	
    
	$company_specific_profile_source_insert_sql = "INSERT INTO `company_specific_profile_sources`(`company_id`, `profile_source_id`, `profile_source_name`, `profile_source_seo_name`) VALUES (:company_id,:profile_source_id,:profile_source_name,:profile_source_seo_name)";

	$company_specific_profile_source_insert_query = $dbcon->prepare($company_specific_profile_source_insert_sql);
	$company_specific_profile_source_insert_query->bindValue(":company_id",$company_id_input);
	$company_specific_profile_source_insert_query->bindValue(":profile_source_id",$profile_source_id);
	$company_specific_profile_source_insert_query->bindValue(":profile_source_name",$profile_source_name);	
	$company_specific_profile_source_insert_query->bindValue(":profile_source_seo_name",$profile_source_seo_name);
	
	if ($company_specific_profile_source_insert_query->execute()) {
          $eventLog->log("after execute.");
            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");
           
			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}


function company_specific_profile_source_duplicate_check($company_id_input,$profile_source_name) {
	global $dbcon;
	$constructed_array = array();
	
	$company_specific_profile_source_duplicate_check_sql = "SELECT * FROM `company_specific_profile_sources` WHERE `company_id` = :company_id AND `profile_source_name` = :profile_source_name";
	$company_specific_profile_source_duplicate_check_sql_query = $dbcon->prepare($company_specific_profile_source_duplicate_check_sql);	
	$company_specific_profile_source_duplicate_check_sql_query->bindValue(":company_id",$company_id_input);	
	$company_specific_profile_source_duplicate_check_sql_query->bindValue(":profile_source_name",$profile_source_name);
	
		
	$company_specific_profile_source_duplicate_check_sql_query->execute(); 
	
	if($company_specific_profile_source_duplicate_check_sql_query->rowCount() > 0) {
		$company_specific_profile_source_duplicate_check_sql_query_result = $company_specific_profile_source_duplicate_check_sql_query->fetch();
	     return $company_specific_profile_source_duplicate_check_sql_query_result;
	
	}
	return $constructed_array;
}
function company_specific_profile_source_details_based_on_profile_source_name($profile_source_name) {

	global $dbcon, $eventLog;
	$constructed_array = array();
    
	$profile_source_details_get_sql = "SELECT * FROM `profile_sources` WHERE `profile_source_name` =:profile_source_name";
	$profile_source_details_select_query = $dbcon->prepare($profile_source_details_get_sql);
    $profile_source_details_select_query->bindValue(":profile_source_name",$profile_source_name);
	
      $profile_source_details_select_query->execute();
	  
	if($profile_source_details_select_query->rowCount() > 0) {
		$profile_source_details_select_query_result = $profile_source_details_select_query->fetch();
		return $profile_source_details_select_query_result;
	
	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
	
}
function company_specific_profile_source_change_status($cspf_id,$is_active_status) {
	
	global $dbcon,$eventLog;
	
    
	$company_specific_profile_source_update_sql = "UPDATE `company_specific_profile_sources` SET  `is_active_status`= :is_active_status WHERE `cspf_id` = :cspf_id ";

	$company_specific_profile_source_update_query = $dbcon->prepare($company_specific_profile_source_update_sql);
	$company_specific_profile_source_update_query->bindValue(":cspf_id",$cspf_id);
	$company_specific_profile_source_update_query->bindValue(":is_active_status",$is_active_status);
	
	if ($company_specific_profile_source_update_query->execute()) {
          $eventLog->log("after execute.");
          return true;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

	
	
	
}

function company_specific_profile_source_list($company_id_input) {
	global $dbcon;
	$constructed_array = array();
	$is_active_status = "1";
	
	$company_specific_profile_source_list_sql = "SELECT * FROM `company_specific_profile_sources` WHERE `company_id` = :company_id AND  `is_active_status` = :is_active_status";
	$company_specific_profile_source_list_query = $dbcon->prepare($company_specific_profile_source_list_sql);	
	$company_specific_profile_source_list_query->bindValue(":company_id",$company_id_input);	
	
	$company_specific_profile_source_list_query->bindValue(":is_active_status",$is_active_status);
		
	$company_specific_profile_source_list_query->execute(); 
	
	if($company_specific_profile_source_list_query->rowCount() > 0) {
		$company_specific_profile_source_list_query_result = $company_specific_profile_source_list_query->fetchAll();
	     return $company_specific_profile_source_list_query_result;
	
	}
	return $constructed_array;
	
	
}


exit;
?>