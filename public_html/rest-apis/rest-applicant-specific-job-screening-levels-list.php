<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {

	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
			    $job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$job_applicant_request_info_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'], FILTER_SANITIZE_NUMBER_INT) : '');
			    
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {

                if ($job_id_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-id";
					$response['status_description'] = "Missing Job Id";
					
					$eventLog->log("missing-user-id: Missing Job Id.");
					
		        } else if ($job_applicant_request_info_id_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-applicant-request-info-id";
					$response['status_description'] = "Missing Job applicant request info Id";
					
					$eventLog->log("Missing Job applicant request info Id.");
					
				
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else {
					//all inputs are valid
					try {
						
					
						$applicant_specific_job_screening_levels_list_info_result = applicant_specific_job_screening_levels_list_info($job_id_input,$job_applicant_request_info_id_input);
				
						$applicant_specific_job_screening_levels_list_info_result_count = count($applicant_specific_job_screening_levels_list_info_result);
						
						$eventLog->log("Count -> " . $applicant_specific_job_screening_levels_list_info_result_count);
						
						if (count($applicant_specific_job_screening_levels_list_info_result) > 0) {
						
						$response['data'] = $applicant_specific_job_screening_levels_list_info_result;
						$response['status'] = "applicant-specific-job-screening-levels-list-successfully-fetched";
						$response['status_description'] = "Applicant specific Job screening levels List Successfully Received";
					
						} else {
							
							$response['data'] = array();
							$response['status'] = "no-screening levels-defined-for-this-job";
							$response['status_description'] = "No screening levels defined found for this job";
							
						}
							
							
					} catch (Exception $e){
						
						$response['data'] = array();
						$response['status'] = "applicant-specific-job-screening-levels-list-fetching-error";
						$response['status_description'] = "Applicant Specific Job screening levels List Fetching Error";
					}
					
					
				}
					
			}
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
	


function  applicant_specific_job_screening_levels_list_info($job_id_input,$job_applicant_request_info_id_input) {
global $dbcon;
	$constructed_array = array();
	
	$applicant_specific_job_screening_levels_list_info_sql = "SELECT * FROM `jsl_info` WHERE `job_id` = :job_id AND `jsl_info_id` NOT IN (SELECT DISTINCT `jsl_info_id` FROM `jsl_applicant_evaluation_info` WHERE `event_status` IN (1,4,5,6,7) AND `job_applicant_request_info_id` = :job_applicant_request_info_id)";
	$applicant_specific_job_screening_levels_list_info_query = $dbcon->prepare($applicant_specific_job_screening_levels_list_info_sql);
	$applicant_specific_job_screening_levels_list_info_query->bindValue(":job_id",$job_id_input);
	$applicant_specific_job_screening_levels_list_info_query->bindValue(":job_applicant_request_info_id",$job_applicant_request_info_id_input);	
	$applicant_specific_job_screening_levels_list_info_query->execute(); 
	
	if($applicant_specific_job_screening_levels_list_info_query->rowCount() > 0) {
		$applicant_specific_job_screening_levels_list_info_query_result = $applicant_specific_job_screening_levels_list_info_query->fetchAll();
	     return $applicant_specific_job_screening_levels_list_info_query_result;
	
	}
	return $constructed_array;
}
exit;
?>