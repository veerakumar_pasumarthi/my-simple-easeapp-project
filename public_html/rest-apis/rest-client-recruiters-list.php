<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "6")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$client_id_input = trim(isset($ea_received_rest_ws_raw_array_input['client_id']) ? filter_var($ea_received_rest_ws_raw_array_input['client_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$period_type_input = trim(isset($ea_received_rest_ws_raw_array_input['period_type']) ? filter_var($ea_received_rest_ws_raw_array_input['period_type'], FILTER_SANITIZE_STRING) : '');
				$period_type_data_input = trim(isset($ea_received_rest_ws_raw_array_input['period_type_data']) ? filter_var($ea_received_rest_ws_raw_array_input['period_type_data'], FILTER_SANITIZE_NUMBER_INT) : '');
				$period_data_from_date_input = trim(isset($ea_received_rest_ws_raw_array_input['period_data_from_date']) ? filter_var($ea_received_rest_ws_raw_array_input['period_data_from_date'], FILTER_SANITIZE_STRING) : '');
				
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {


				if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else if ($company_id_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company Id.";
					
					$eventLog->log("Please provide valid Company Id.");
					
				} else if ($period_type_input != "" && $period_type_data_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
				
					$response['data'] = array();
					$response['status'] = "missing-period-type-data";
					$response['status_description'] = "Missing Period Type Data.";
					
					$eventLog->log("Please provide valid Period Type Data.");
				
					
				} else {
					//all inputs are valid


					if($client_id_input != ""){

						
					$client_based_list_info_result = client_based_list_info($client_id_input,$company_id_input);
						

					if (count($client_based_list_info_result) > 0) {
						
						$response['data'] = $client_based_list_info_result;
						$response['status'] = "client-based-recruiter-list-successfully-fetched";
						$response['status_description'] = "Client Based Recruiter List Successfully Received";
					} else {
						$response['data'] = $client_based_list_info_result;
						$response['status'] = "client-based-recruiter-list-not-fetched";
						$response['status_description'] = "Client Based Recruiter List Successfully Not Received";
					}
				
									} 	
					
				}
					
			}
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function client_based_list_info($company_client_id_input,$company_id_input) {
	global $dbcon;
	$constructed_array = array();
	
	$client_based_work_list_get_sql = "SELECT * FROM `job_management_assignees` WHERE `company_id` = :company_id AND `assignment_status`=:assignment_status AND `company_client_id`=:company_client_id AND `assigned_job_specific_role` IN(3,4)";
		
		$client_based_work_list_get_select_query = $dbcon->prepare($client_based_work_list_get_sql);
		$client_based_work_list_get_select_query->bindValue(":assignment_status",1);
		$client_based_work_list_get_select_query->bindValue(":company_client_id",$company_client_id_input);
		$client_based_work_list_get_select_query->bindValue(":company_id",$company_id_input);
		
		
	 
		$client_based_work_list_get_select_query->execute();
		if($client_based_work_list_get_select_query->rowCount() > 0) {
	    $client_based_work_list_get_select_query_result = $client_based_work_list_get_select_query->fetchAll();
	    //print_r($user_classification_details_get_select_query_result);
	    
		foreach ($client_based_work_list_get_select_query_result as $phonecodes_list_get_select_query_result_row) {
			
			//$temp_row_array = array();
			$assigned_user_sm_memb_id = $phonecodes_list_get_select_query_result_row["assigned_user_sm_memb_id"];
		    $assigned_user_sm_memb_ids[] = $assigned_user_sm_memb_id;
		    //var_dump($assigned_user_sm_memb_id);
		    //$constructed_array[] = $temp_row_array;
	    }//close of foreach ($countries_list_get_select_query_result as $countries_list_get_select_query_result_row) {
			
		//return $constructed_array;
	} else {
		$assigned_user_sm_memb_ids[] = null;
	}

	//$constructed_array = array();
		$phonecodes_list_get_sql = "SELECT * FROM `site_members` WHERE `sm_memb_id`IN ('".implode("','",$assigned_user_sm_memb_ids)."')";
		
		$phonecodes_list_get_select_query = $dbcon->prepare($phonecodes_list_get_sql);
		//$phonecodes_list_get_select_query->bindValue(":company_id",$company_id_input);
		$phonecodes_list_get_select_query->execute();
		//echo $phonecodes_list_get_select_query->rowCount();
		
	if($phonecodes_list_get_select_query->rowCount() > 0) {
	    $phonecodes_list_get_select_query_result = $phonecodes_list_get_select_query->fetchAll();
	    //print_r($user_classification_details_get_select_query_result);
	    
		foreach ($phonecodes_list_get_select_query_result as $phonecodes_list_get_select_query_result_row) {
			
			
			$temp_row_array = array();
			$user_id = $phonecodes_list_get_select_query_result_row["sm_memb_id"];
			$first_name = $phonecodes_list_get_select_query_result_row["sm_firstname"];
		    $middle_name = $phonecodes_list_get_select_query_result_row["sm_middlename"];
		    $last_name = $phonecodes_list_get_select_query_result_row["sm_lastname"];
		    $temp_row_array["Recruiter_name"] = $first_name." ".$middle_name." ".$last_name ;
	       
	       $rest_client_interviews_check_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `company_client_id` = :company_client_id AND `jsl_classification_detail_name` =:jsl_classification_detail_name AND `added_by_sm_memb_id`=:added_by_sm_memb_id";
	$rest_client_interviews_check_sql_query = $dbcon->prepare($rest_client_interviews_check_sql);
	$rest_client_interviews_check_sql_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_interviews_check_sql_query->bindValue(":jsl_classification_detail_name","Interview");
	$rest_client_interviews_check_sql_query->bindValue(":added_by_sm_memb_id",$user_id);
	$rest_client_interviews_check_sql_query->execute();
	$temp_row_array['no_of_interviews'] = $rest_client_interviews_check_sql_query->rowCount();

	$rest_client_submission_check_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `company_client_id` = :company_client_id AND `added_by_sm_memb_id`=:added_by_sm_memb_id";
	$rest_client_submission_check_sql_query = $dbcon->prepare($rest_client_submission_check_sql);
	$rest_client_submission_check_sql_query->bindValue(":company_client_id",$company_client_id_input);
	
	$rest_client_submission_check_sql_query->bindValue(":added_by_sm_memb_id",$user_id);
	$rest_client_submission_check_sql_query->execute();
	$temp_row_array['no_of_submissions'] = $rest_client_submission_check_sql_query->rowCount();

	$rest_client_rejected_check_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `company_client_id` = :company_client_id AND `event_status` =:event_status AND `added_by_sm_memb_id`=:added_by_sm_memb_id";
	$rest_client_rejected_check_sql_query = $dbcon->prepare($rest_client_rejected_check_sql);
	$rest_client_rejected_check_sql_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_rejected_check_sql_query->bindValue(":event_status",6);
	$rest_client_rejected_check_sql_query->bindValue(":added_by_sm_memb_id",$user_id);
	$rest_client_rejected_check_sql_query->execute();
	$temp_row_array['no_of_rejected'] = $rest_client_rejected_check_sql_query->rowCount();

	$rest_client_offer_check_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `company_client_id` = :company_client_id AND `event_status` =:event_status AND `added_by_sm_memb_id`=:added_by_sm_memb_id";
	$rest_client_offer_check_sql_query = $dbcon->prepare($rest_client_offer_check_sql);
	$rest_client_offer_check_sql_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_offer_check_sql_query->bindValue(":event_status",7);
	$rest_client_offer_check_sql_query->bindValue(":added_by_sm_memb_id",$user_id);
	$rest_client_offer_check_sql_query->execute();
	$temp_row_array['no_of_offer_letters'] = $rest_client_offer_check_sql_query->rowCount();
		    
		    $constructed_array[] = $temp_row_array;
	    }//close of foreach ($countries_list_get_select_query_result as $countries_list_get_select_query_result_row) {
			
		    
		
		return $constructed_array;
	}
	return $constructed_array;
}
	
	

exit;
?>