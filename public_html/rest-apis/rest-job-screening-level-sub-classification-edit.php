<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Edit Quick Admin Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "5")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_id'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_name'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_name'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_name'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_description'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_description'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_description'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['jsl_classification_detail_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_classification_detail_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_classification_detail_id'])) 
					 
						
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$jsl_sub_classification_detail_id_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_id']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$jsl_sub_classification_detail_name_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_name']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_name'], FILTER_SANITIZE_STRING) : '');
					
					$jsl_sub_classification_detail_description_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_description']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_description'], FILTER_SANITIZE_STRING) : '');
					
					$jsl_classification_detail_id_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_classification_detail_id']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_classification_detail_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					
					
					
					
					$jsl_sub_classification_detail_seo_name_input = strtolower(create_seo_name($jsl_sub_classification_detail_name_input));
					
					$jsl_sub_classification_detail_description_all_input = trim($ea_received_rest_ws_raw_array_input["jsl_sub_classification_detail_description"]);
					$jsl_sub_classification_detail_description_safe_html_input = get_cleaned_safe_html_content_input($jsl_sub_classification_detail_description_all_input, $page_content_file_config);

				
						
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//$eventLog->log("company_id : " . $company_id_input);
				//Check if all inputs are received correctly from the REST Web Service
				if ($jsl_sub_classification_detail_name_input == "") {
				
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "missing-job-screening-level-sub-classification-detail-name";
						$response['status_description'] = "missing job Screening level sub classification detail name";
						
						$eventLog->log("missing-jsl-sub-classification-detail-name: Missing jsl  sub classification detail name.");
						
					} else if ($jsl_classification_detail_id_input == "") {
						//Invalid Mobile Number scenario
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "missing-job-screening-level-classification-detail-id";
						$response['status_description'] = "Missing job screening level classification detail id.";
						
						$eventLog->log("missing-job-screening-level-classification-detail-id: Missing job screening level classification detail id.");
				
					
				    } else if ($jsl_sub_classification_detail_id_input == "") {
						//Invalid Mobile Number scenario
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "missing-job-screening-level-sub-classification-detail-id";
						$response['status_description'] = "Missing job screening level sub classification detail id.";
						
						$eventLog->log("missing-job-screening-level-sub-classification-detail-id: Missing job screening level sub classification detail id.");
					} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//Event Time, as per Indian Standard Time
					$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
		
					$job_screening_level_edit_next_step = "";
					
					if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$job_screening_level_edit_next_step = "PROCEED-TO-NEXT-STEP";
								
							/*}  else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$company_edit_next_step = "PROCEED-TO-NEXT-STEP";
								 */
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
					} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
					}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
							
						if ($job_screening_level_edit_next_step == "PROCEED-TO-NEXT-STEP") {
						$job_screening_level_sub_classification_duplicate_check_result = job_screening_level_sub_classification_duplicate_check($jsl_sub_classification_detail_id_input);
					    
						if (count($job_screening_level_sub_classification_duplicate_check_result) > 0) {
						
						
                       //if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
								$eventLog->log("before enter into query.");
								
								      
									  $job_screening_level_sub_classification_details_update_result = job_screening_level_sub_classification_details_update($jsl_sub_classification_detail_id_input,$jsl_sub_classification_detail_name_input,$jsl_sub_classification_detail_seo_name_input,$jsl_sub_classification_detail_description_input,$jsl_sub_classification_detail_description_safe_html_input,$jsl_classification_detail_id_input,$event_datetime,$current_epoch,$ea_extracted_jwt_token_sub);
									  
									  
									  
								      $eventLog->log("After update query.");
									  
												if ($job_screening_level_sub_classification_details_update_result== true) {
													$eventLog->log("enter into user ass if condition.");
													//Use Privilege details are updated successfully, for the user, in sm_site_member_classification_associations db table
													//Construct Content, that will be sent in Response body, of the REST Web Service
													$response['data'] = array();
													$response['status'] = "job-screening-level-sub-classification-details-updated-successfully";
													$response['status_description'] = "Job screening level sub classification details are edited Successfully.";
													
													$eventLog->log("Job screening level sub classification details are edited Successfully.");
													$eventLog->log("before update status.");
												} else {
													//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
													$response['data'] = array();
													$response['status'] = "job-screening-level-sub-classification-details-editing-error";
													$response['status_description'] = "There is an error, when editing the job screening level sub classification details in the Database.";
													
													$eventLog->log("There is an error, when editing the job screening level sub classification details in the Database.");
													
												}
										
										
								} catch (Exception $e) {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "job-screening-level-sub-classification-updating-error";
									$response['status_description'] = "Error occurred when updating the job screening level sub classification details.";
									
									$eventLog->log("Error occurred when updating the job screening level sub classification details.");	
									
								}
					   
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "The User Account, that is attempted to be edited, is invalid, please check and try again.";
						
						$eventLog->log("Please provide a Valid User ID.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
					
						
				}//close of else of if ($email_id_input == "") {
				
			}
			
		}	
			
	} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function job_screening_level_sub_classification_details_update($jsl_sub_classification_detail_id_input,$jsl_sub_classification_detail_name_input,$jsl_sub_classification_detail_seo_name_input,$jsl_sub_classification_detail_description_input,$jsl_sub_classification_detail_description_safe_html_input,$jsl_classification_detail_id_input,$event_datetime,$current_epoch,$ea_extracted_jwt_token_sub) {
	global $dbcon, $eventLog;
	
	$is_active_status = '1';
	$eventLog->log("in function before query");
    
	$job_screening_level_sub_classification_details_update_sql = "UPDATE `jsl_sub_classification_details` SET `jsl_sub_classification_detail_name`=:jsl_sub_classification_detail_name,`jsl_sub_classification_detail_seo_name`=:jsl_sub_classification_detail_seo_name,`jsl_sub_classification_detail_description`=:jsl_sub_classification_detail_description,`jsl_sub_classification_detail_description_safe_html`=:jsl_sub_classification_detail_description_safe_html,`jsl_classification_detail_id`=:jsl_classification_detail_id,`last_updated_datetime`=:last_updated_datetime,`last_updated_datetime_epoch`=:last_updated_datetime_epoch,`last_updated_by_sm_memb_id`=:last_updated_by_sm_memb_id,`is_active_status`=:is_active_status WHERE `jsl_sub_classification_detail_id`=:jsl_sub_classification_detail_id";
	$job_screening_level_sub_classification_details_update_query = $dbcon->prepare($job_screening_level_sub_classification_details_update_sql);
	$eventLog->log("in function before bind");
	
	$job_screening_level_sub_classification_details_update_query->bindValue(":jsl_sub_classification_detail_id",$jsl_sub_classification_detail_id_input);
	$job_screening_level_sub_classification_details_update_query->bindValue(":jsl_sub_classification_detail_name",$jsl_sub_classification_detail_name_input);
	$job_screening_level_sub_classification_details_update_query->bindValue(":jsl_sub_classification_detail_seo_name",$jsl_sub_classification_detail_seo_name_input);
	$job_screening_level_sub_classification_details_update_query->bindValue(":jsl_sub_classification_detail_description",$jsl_sub_classification_detail_description_input);
	$job_screening_level_sub_classification_details_update_query->bindValue(":jsl_sub_classification_detail_description_safe_html",$jsl_sub_classification_detail_description_safe_html_input);
	$job_screening_level_sub_classification_details_update_query->bindValue(":jsl_classification_detail_id",$jsl_classification_detail_id_input);
	$job_screening_level_sub_classification_details_update_query->bindValue(":last_updated_datetime",$event_datetime);
	$job_screening_level_sub_classification_details_update_query->bindValue(":last_updated_datetime_epoch",$current_epoch);
	$job_screening_level_sub_classification_details_update_query->bindValue(":last_updated_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$job_screening_level_sub_classification_details_update_query->bindValue(":is_active_status",$is_active_status);
	
	
	$eventLog->log("in function after bind");

	if ($job_screening_level_sub_classification_details_update_query->execute()) {
		
		$eventLog->log("in function enter into if");

		return true;

	}
	return false;
}	
function job_screening_level_sub_classification_duplicate_check($jsl_sub_classification_detail_id_input) {
	global $dbcon;
	$constructed_array = array();
	
	$job_screening_level_sub_classification_duplicate_check_sql = "SELECT * FROM `jsl_sub_classification_details` WHERE  `jsl_sub_classification_detail_id` = :jsl_sub_classification_detail_id";
	$job_screening_level_sub_classification_duplicate_check_q = $dbcon->prepare($job_screening_level_sub_classification_duplicate_check_sql);	
	$job_screening_level_sub_classification_duplicate_check_q->bindValue(":jsl_sub_classification_detail_id",$jsl_sub_classification_detail_id_input);	
		
	$job_screening_level_sub_classification_duplicate_check_q->execute(); 
	
	if($job_screening_level_sub_classification_duplicate_check_q->rowCount() > 0) {
		$job_screening_level_sub_classification_duplicate_check_q_result = $job_screening_level_sub_classification_duplicate_check_q->fetchAll();
	     return $job_screening_level_sub_classification_duplicate_check_q_result;
	
	}
	return $constructed_array;
}
exit;
?>


