<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "8")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['salutation'])) {
						$content .= $ea_received_rest_ws_raw_array_input['salutation'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
				
					if (isset($ea_received_rest_ws_raw_array_input['full_name'])) {
						$content .= $ea_received_rest_ws_raw_array_input['full_name'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['premise_name'])) 
 
                    if (isset($ea_received_rest_ws_raw_array_input['company_name'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_name'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['address_line_1'])) 

					if (isset($ea_received_rest_ws_raw_array_input['email'])) {
						$content .= $ea_received_rest_ws_raw_array_input['email'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['address_line_2'])) 

					if (isset($ea_received_rest_ws_raw_array_input['mobile_number'])) {
						$content .= $ea_received_rest_ws_raw_array_input['mobile_number'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['city'])) 

					
                    if (isset($ea_received_rest_ws_raw_array_input['purpose'])) {
						$content .= $ea_received_rest_ws_raw_array_input['purpose'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['country'])) 

					if (isset($ea_received_rest_ws_raw_array_input['message'])) {
						$content .= $ea_received_rest_ws_raw_array_input['message'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['pincode'])) 
					if (isset($ea_received_rest_ws_raw_array_input['country_name'])) {
						$content .= $ea_received_rest_ws_raw_array_input['country_name'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['pincode'])) 
					
					
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$salutation_input = trim(isset($ea_received_rest_ws_raw_array_input['salutation']) ? filter_var($ea_received_rest_ws_raw_array_input['salutation'], FILTER_SANITIZE_STRING) : '');
				$full_name_input = trim(isset($ea_received_rest_ws_raw_array_input['full_name']) ? filter_var($ea_received_rest_ws_raw_array_input['full_name'], FILTER_SANITIZE_STRING) : '');
				$company_name_input = trim(isset($ea_received_rest_ws_raw_array_input['company_name']) ? filter_var($ea_received_rest_ws_raw_array_input['company_name'], FILTER_SANITIZE_STRING) : '');
				$email_input = trim(isset($ea_received_rest_ws_raw_array_input['email']) ? filter_var($ea_received_rest_ws_raw_array_input['email'], FILTER_SANITIZE_EMAIL) : '');
				$mobile_number_input = trim(isset($ea_received_rest_ws_raw_array_input['mobile_number']) ? filter_var($ea_received_rest_ws_raw_array_input['mobile_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$purpose_input = trim(isset($ea_received_rest_ws_raw_array_input['purpose']) ? filter_var($ea_received_rest_ws_raw_array_input['purpose'], FILTER_SANITIZE_STRING) : '');
				$message_input = trim(isset($ea_received_rest_ws_raw_array_input['message']) ? filter_var($ea_received_rest_ws_raw_array_input['message'], FILTER_SANITIZE_STRING) : '');
				$country_name_input = trim(isset($ea_received_rest_ws_raw_array_input['country_name']) ? filter_var($ea_received_rest_ws_raw_array_input['country_name'], FILTER_SANITIZE_STRING) : '');
						
							
				
				if ($full_name_input == "") {
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-full-name";
					$response['status_description'] = "missing full name";
					
					$eventLog->log("missing-full-name: Please provide a valid full Name.");
					
				} else if ($mobile_number_input == "") {
					//Invalid Mobile Number scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-mobile-number";
					$response['status_description'] = "Missing Mobile Number";
					
					$eventLog->log("missing-mobile-number: Please provide a valid Mobile Number.");
					
				} else if ($email_input == "") {
					//Invalid Salutation scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-email";
					$response['status_description'] = "Missing Email";
					
					$eventLog->log("missing-email: Missing Email.");			
				
				} else if ($purpose_input == "") {
					//Invalid Salutation scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-purpose";
					$response['status_description'] = "Missing Purpose";
								
				
				}  else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
						
						//Event Time, as per Indian Standard Time
						$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
						$company_address_add_next_step = "";
						
						
							try {

								$contact_page_purpose_check_result = contact_page_purpose_id_get($purpose_input);
								if(count($contact_page_purpose_check_result)>0){
                                    $purpose_id_input = $contact_page_purpose_check_result['purpose_id'];
								}else{
									$purpose_id_input = null ;

								}

								$company_office_address_duplicate_check_result = contact_page_duplicate_check($full_name_input,$email_input,$purpose_input);
								
								if (count($company_office_address_duplicate_check_result) > 0) { 

                                      $time_epoch = $company_office_address_duplicate_check_result['added_date_time_epoch'];
                                      $time_epoch_interval = $time_epoch+2;

									if($time_epoch_interval > $current_epoch){
									
										    $response['data'] = array();
											$response['status'] = "contact-details-already-exists";
											$response['status_description'] = "Contact Details already exist";
										
											$eventLog->log("contact-details-already-exists: Contact Details already exist.");
									
								} else {
									//echo "inset2";
									$client_details_inserted_id = contact_details_record_insert($salutation_input, $full_name_input, $company_name_input, $email_input, $mobile_number_input, $purpose_id_input, $purpose_input, $message_input, $country_name_input, $event_datetime, $current_epoch);
									
									if ($client_details_inserted_id != "") {
										
										    $response['data'] = array();
											$response['status'] = "contact_details-insertion-successful";
											$response['status_description'] = "Submitted successfully.";
										
											$eventLog->log("contact_details-insertion-successfu:Submitted Successfully.");   
										
									} else {
										//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
										$response['data'] = array();
										$response['status'] = "contact-details-insertion-error";
										$response['status_description'] = "Contact Details error.";
										
										$eventLog->log("contact-details-insertion-error: There is an error, when adding the Contact Details in the Database.");
										
									}
									
								}//close of else of if (count($user_ass_oc_validation_result) > 0) {	
								}	else {
									//echo "inset1";
									$client_details_inserted_id = contact_details_record_insert($salutation_input, $full_name_input, $company_name_input, $email_input, $mobile_number_input, $purpose_id_input, $purpose_input, $message_input, $country_name_input, $event_datetime, $current_epoch);
									
									if ($client_details_inserted_id != "") {
										
										    $response['data'] = array();
											$response['status'] = "contact_details-insertion-successful";
											$response['status_description'] = "Submitted successfully.";
										
											$eventLog->log("contact_details-insertion-successfu:Submitted Successfully.");   
										
									} else {
										//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
										$response['data'] = array();
										$response['status'] = "contact-details-insertion-error";
										$response['status_description'] = "Contact Details error.";
										
										$eventLog->log("contact-details-insertion-error: There is an error, when adding the Contact Details in the Database.");
										
									}
									
								}
								
							} catch (Exception $e) {
								
								//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
								$response['data'] = array();
										$response['status'] = "contact-details-insertion-error";
										$response['status_description'] = "Contact Details error.";
										
										$eventLog->log("contact-details-insertion-error: There is an error, when adding the Contact Details in the Database.");
							} 
						}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
			
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
		}
		
		//}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
    
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
			
	
	}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {

	//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){




function contact_page_duplicate_check($full_name_input,$email_input,$purpose_input) {
	global $dbcon;

	$constructed_array = array();
	$contact_page_duplicate_check_sql = "SELECT * FROM `contact_page_queries` WHERE `full_name` = :full_name AND `email` = :email AND `purpose` = :purpose";
	$company_office_address_select_query = $dbcon->prepare($contact_page_duplicate_check_sql);
	$company_office_address_select_query->bindValue(":full_name",$full_name_input);
  
    $company_office_address_select_query->bindValue(":email",$email_input);
    $company_office_address_select_query->bindValue(":purpose",$purpose_input);
    
	$company_office_address_select_query->execute();

	if($company_office_address_select_query->rowCount() > 0) {
		$company_office_address_select_query_result = $company_office_address_select_query->fetch();
	     return $company_office_address_select_query_result;

	}//close of if($company_office_address_select_query->rowCount() > 0) {
	return $constructed_array;

}


function contact_details_record_insert($salutation_input, $full_name_input, $company_name_input, $email_input, $mobile_number_input, $purpose_id_input, $purpose_input, $message_input,$country_name_input, $event_datetime, $current_epoch){
	global $dbcon;
	$is_active_status = '1';
	$company_office_address_insert_sql = "INSERT INTO `contact_page_queries` (salutation,full_name,company_name,email,mobile_number,purpose_id,purpose,message,country_name,added_date_time,added_date_time_epoch,is_active_status) VALUES(:salutation, :full_name, :company_name, :email, :mobile_number, :purpose_id, :purpose, :message, :country_name, :added_date_time, :added_date_time_epoch, :is_active_status)";
	$company_office_address_insert_query = $dbcon->prepare($company_office_address_insert_sql);
	$company_office_address_insert_query->bindValue(":salutation",$salutation_input);
	$company_office_address_insert_query->bindValue(":full_name",$full_name_input);
	$company_office_address_insert_query->bindValue(":company_name",$company_name_input);
	$company_office_address_insert_query->bindValue(":email",$email_input);
	$company_office_address_insert_query->bindValue(":mobile_number",$mobile_number_input);
	$company_office_address_insert_query->bindValue(":purpose_id",$purpose_id_input);
	$company_office_address_insert_query->bindValue(":purpose",$purpose_input);
	$company_office_address_insert_query->bindValue(":message",$message_input);
	$company_office_address_insert_query->bindValue(":country_name",$country_name_input);
	$company_office_address_insert_query->bindValue(":added_date_time",$event_datetime);
    $company_office_address_insert_query->bindValue(":added_date_time_epoch",$current_epoch);
	$company_office_address_insert_query->bindValue(":is_active_status",$is_active_status);

		if ($company_office_address_insert_query->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");

			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }
}


function contact_page_purpose_id_get($purpose_input) {
	global $dbcon;

	$constructed_array = array();
	$contact_page_duplicate_check_sql = "SELECT * FROM `purposes` WHERE `purpose` = :purpose";
	$company_office_address_select_query = $dbcon->prepare($contact_page_duplicate_check_sql);
	$company_office_address_select_query->bindValue(":purpose",$purpose_input);
  
    
    
	$company_office_address_select_query->execute();

	if($company_office_address_select_query->rowCount() > 0) {
		$company_office_address_select_query_result = $company_office_address_select_query->fetch();
	     return $company_office_address_select_query_result;

	}//close of if($company_office_address_select_query->rowCount() > 0) {
	return $constructed_array;

}




exit;
?>