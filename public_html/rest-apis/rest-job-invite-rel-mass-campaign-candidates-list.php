<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * REST API for candidate all education qualifications collect
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "7")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
						
                    if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_client_id'] . "\r\n";
				    }//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id']))
					
					if (isset($ea_received_rest_ws_raw_array_input['job_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_id'] . "\r\n";
				    }//close of if (isset($ea_received_rest_ws_raw_array_input['job_id']))
					
					if (isset($ea_received_rest_ws_raw_array_input['job_rel_mass_email_recipient_list_type'])) {
						$content .= json_encode($ea_received_rest_ws_raw_array_input['job_rel_mass_email_recipient_list_type']) . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_rel_mass_email_recipient_list_type'])) 
			
			        if (isset($ea_received_rest_ws_raw_array_input['job_rel_search_string'])) {
						$content .= json_encode($ea_received_rest_ws_raw_array_input['job_rel_search_string']) . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_rel_search_string'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['job_invite_rel_mass_campaign_candidates_list'])) {
						$content .= json_encode($ea_received_rest_ws_raw_array_input['job_invite_rel_mass_campaign_candidates_list']) . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_invite_rel_mass_campaign_candidates_list'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					//$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$job_rel_mass_email_recipient_list_type_input = trim(isset($ea_received_rest_ws_raw_array_input['job_rel_mass_email_recipient_list_type']) ? filter_var($ea_received_rest_ws_raw_array_input['job_rel_mass_email_recipient_list_type'], FILTER_SANITIZE_STRING) : '');
				
				$job_rel_search_string_input = trim(isset($ea_received_rest_ws_raw_array_input['job_rel_search_string']) ? filter_var($ea_received_rest_ws_raw_array_input['job_rel_search_string'], FILTER_SANITIZE_STRING) : '');
				
				$job_invite_rel_mass_campaign_candidates_list_input = $ea_received_rest_ws_raw_array_input['job_invite_rel_mass_campaign_candidates_list'];
				
				$company_client_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_client_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_client_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//Check if all inputs are received correctly from the REST Web Service
				if ($company_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company ID";
					
					$eventLog->log("missing-company-id: Please provide a valid Company ID.");
					
				} else if ($company_client_id_input == "") {
					//Invalid company Status scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-company-client-id";
					$response['status_description'] = "Invalid company client id submitted, please check and try again.";
					
					$eventLog->log("Please provide a valid company client id .");	
					
				} else if ($job_id_input == "") {
					//Invalid company Status scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-job-id";
					$response['status_description'] = "Invalid job id submitted, please check and try again.";
					
					$eventLog->log("Please provide a valid job id .");
					
				} else if ($job_rel_mass_email_recipient_list_type_input == "specific" && $job_invite_rel_mass_campaign_candidates_list_input == "") {	
					//Invalid Education Qualifications Received scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "empty-job-invite-rel-mass-campaign-candidates-list-input";
					$response['status_description'] = "Empty Job invite rel mass campaign candidate list Input";
					
					$eventLog->log("Empty Job invite rel mass campaign candidate list Input, Please check and try again.");
				
				} else if ($job_rel_mass_email_recipient_list_type_input == "all" && $job_rel_search_string_input == "") {	
					//Invalid Education Qualifications Received scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "empty-job-rel-search-string-input";
					$response['status_description'] = "Empty Job related search string Input";
					
					$eventLog->log("Empty Job related search string Input, Please check and try again.");
					
				
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Missing Some Additional Information";
					
					$eventLog->log("missing-some-additional-information: Some Additional Information is missing, please check and try again.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					$ea_action_specific_auth_check_result = "";
					if ($ea_extracted_jwt_token_user_type == "admin") {
							
						if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
							$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
							
						} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
							//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
							
							$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
							
						} else {
							//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-different-company-user-id";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
		
							$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
							
						}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							$eventLog->log("after candidate add.");
							
					} else {
							
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "insufficient-permissions";
						$response['status_description'] = "Insufficient Set of Permissions";
						
						//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
						header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
		
						$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
					
					}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
							
					if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
						try {
							
							if ($job_rel_mass_email_recipient_list_type_input == "all" && $job_rel_search_string_input != "") {
							
								$job_rel_resume_keyword_search_info_result = job_rel_resume_keyword_search_info($company_id_input,$job_rel_search_string_input);
								$job_invite_rel_mass_campaign_candidates_list_input = $job_rel_resume_keyword_search_info_result["list"];
								//$count = $job_rel_resume_keyword_search_info_result["count"];
							
					        }
							$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
					
							if ((is_array($job_invite_rel_mass_campaign_candidates_list_input)) && (count($job_invite_rel_mass_campaign_candidates_list_input) > 0)) {
									
										
									    
						        $sm_memb_id_array = array();
								$sm_email_array = array();
							
								
								$count = count($job_invite_rel_mass_campaign_candidates_list_input);
								$row_count = 0;
								$campaign_name_input = "mass email invite";
								
								$last_inserted_id = mass_campaigns_insert($company_id_input,$campaign_name_input,$company_client_id_input,$job_id_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
								
								foreach ($job_invite_rel_mass_campaign_candidates_list_input as $job_invite_rel_mass_campaign_candidates_list_input_row) {
									$sm_memb_id = $job_invite_rel_mass_campaign_candidates_list_input_row["sm_memb_id"];
									//$sm_email_id = $multiple_email_requests_input_row["sm_email"];
									
									$eventLog->log($sm_memb_id);
									$job_applicant_invite_duplicate_check_result = job_applicant_invite_duplicate_check_based_on_job_id($job_id_input,$company_id_input,$company_client_id_input,$sm_memb_id);
					
									$eventLog->log("job-applicant-invite-duplicate-check-result: job applicant invite duplicate check result");
									if (count($job_applicant_invite_duplicate_check_result) > 0) {
										
										/* //Construct Content, that will be sent in Response body, of the REST Web Service
										$response['data'] = array();
										$response['status'] = "job-applicant-invite-already-exists";
										$response['status_description'] = "A job applicant invite already exists with this Name"; */
										
										$eventLog->log("job-applicant-invite-already-exists for this id".$sm_memb_id);	
										
										//$eventLog->log("job-applicant-invite-already-exists: job applicant invite already exists");	
										$count--;
										continue;
									
									} else {
										$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
										$event_expiry_datetime_epoch = $current_epoch+$job_invite_expiry_time_span;
										$event_expiry_date_time = df_convert_unix_timestamp_to_datetime_custom_timezone($event_expiry_datetime_epoch, $date_default_timezone_set);
										 
										$job_invite_ref_code = bin2hex(random_bytes($job_invite_ref_code_length));
								
										$eventLog->log("job-invite-ref-code->".$job_invite_ref_code);
										
										$job_invite_ref_code_length = mb_strlen($job_invite_ref_code);
										
										$eventLog->log("job-invite-ref-code-length->".$job_invite_ref_code_length);
										
										$job_applicant_mass_invite_insert_result = job_applicant_mass_invite_insert($company_id_input,$company_client_id_input,$job_id_input,$ea_extracted_jwt_token_sub,$sm_memb_id,$job_invite_ref_code,$last_inserted_id,$event_datetime,$current_epoch,$event_expiry_date_time,$event_expiry_datetime_epoch);
							 
									
										$eventLog->log($last_inserted_id);
										$row_count++;
										$eventLog->log($row_count);
										
									}
									
								}
								
								$eventLog->log("count".$count);
								$eventLog->log("row_count".$row_count);
								if ($count == $row_count) {
									
										$response['data'] = array();
										$response['status'] = "job-applicant-invite-request-save-successful";
										$response['status_description'] = "A job applicant invite request  save is Successful.";
									
										$eventLog->log("job-applicant-invite-save-successful: A job applicant invite request save is Successful.");   
									
								} else {
									//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
									$response['data'] = array();
									$response['status'] = "job-application-invite-request-save-error";
									$response['status_description'] = "Error occurred when  adding the job applicant invite request in the Database.";
									
									$eventLog->log("job-applicant-invite-insert-error: There is an error, when saving the job applicant invite request in the Database.");
									
								}//close of else of if (count($user_ass_oc_validation_result) > 0) {
								
							}
								
					    } catch (Exception $e) {
								$response['data'] = array();
								$response['status'] = "job-application-invite-request-save-error";
								$response['status_description'] = "Error occurred when  adding the job applicant invite request in the Database.";
								
								$eventLog->log("job-applicant-invite-insert-error: There is an error, when saving the job applicant invite request in the Database.");
								
									
					    }
				    }
					
				}	
			
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function job_rel_resume_keyword_search_info($company_id_input,$job_rel_search_string_input) {
	global $dbcon, $elasticsearch_index_name,$elasticsearch_index_type,$elasticsearch_client ,$eventLog;
	$constructed_array = array();
	
	$querystring_search_params = [
									'index' => $elasticsearch_index_name,
									'type' => $elasticsearch_index_type,
									
									'body' => [
									
									   'query' => [
	
											"bool" => [
												'filter' => [
													'terms' => [ 
														'company_id' => [$company_id_input]
													]   
												],
											  "must" => [
												'query_string' => [
													//"default_operator" => "AND",
													"fields" => ["resume_text","sm_email","sm_mobile"],
													"query" => $job_rel_search_string_input
													 ]
												]
										   
											]
										],
									]
										   
								];
											 

					$search_response = $elasticsearch_client->search($querystring_search_params);
					//echo "<pre>";

					
					 $result_rows_array = array();
					 $count = 0;
		
		
					foreach ($search_response['hits']['hits'] as $response_data_row) {
						
							
						foreach ($response_data_row as $response_data_row_key => $response_data_row_value) {
							
							
							if ($response_data_row_key == "_source") {
								$temp = array();
								
								foreach ($response_data_row_value as $response_data_row_value_key => $response_data_row_value_details) {
									$temp[$response_data_row_value_key] = $response_data_row_value_details;
									
								}//close of foreach ($response_data_row_value as $response_data_row_value_details) {
								$result_rows_array[] = $temp;
								
								foreach ($result_rows_array as $response_data_row_value) {

									$temp_row_array = array();
									$count++;
									$temp_row_array["sm_email"] = $response_data_row_value["sm_email"];
									//$temp_row_array["company_id"] = $response_data_row_value["company_id"];
									$temp_row_array["sm_memb_id"] = $response_data_row_value["sm_memb_id"];
																
									
									$constructed_array["list"][] = $temp_row_array;
									
									
									
								}//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {
									$constructed_array["count"] = $count;
								
							} else {
								
								
							}//close of else of if ($response_data_row_key == "_source") {
							
							
						}//close of foreach ($response_data_row as $response_data_row_key => $response_data_row_value) {
							
						//exit;    
					}//close of foreach ($response['hits']['hits'] as $response_data_row) {

	
			return $constructed_array;
	//}
	//return $constructed_array;
}
function mass_campaigns_insert($company_id_input,$campaign_name_input,$company_client_id_input,$job_id_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch) {
	
	
	global $dbcon,$eventLog;
	
	
    
	$mass_campaigns_insert_sql = "INSERT INTO `mass_campaigns`(`company_id`, `campaign_name`, `campaign_communication_mode`, `campaign_type`, `company_client_id`, `job_id`, `campaign_scope`, `initiated_by_user_id`, `added_date_time`, `added_date_time_epoch`) VALUES (:company_id,:campaign_name,:campaign_communication_mode,:campaign_type,:company_client_id,:job_id,:campaign_scope,:initiated_by_user_id,:added_date_time,:added_date_time_epoch)";

	$mass_campaigns_insert_query = $dbcon->prepare($mass_campaigns_insert_sql);
	$mass_campaigns_insert_query->bindValue(":company_id",$company_id_input);
	$mass_campaigns_insert_query->bindValue(":campaign_name",$campaign_name_input);
	$mass_campaigns_insert_query->bindValue(":campaign_communication_mode","1");
	$mass_campaigns_insert_query->bindValue(":campaign_type","1");
	$mass_campaigns_insert_query->bindValue(":company_client_id",$company_client_id_input);
	$mass_campaigns_insert_query->bindValue(":job_id",$job_id_input);
	$mass_campaigns_insert_query->bindValue(":campaign_scope","2");
	$mass_campaigns_insert_query->bindValue(":initiated_by_user_id",$ea_extracted_jwt_token_sub);
	$mass_campaigns_insert_query->bindValue(":added_date_time",$event_datetime);
	$mass_campaigns_insert_query->bindValue(":added_date_time_epoch",$current_epoch);
	
	
	if ($mass_campaigns_insert_query->execute()) {
          $eventLog->log("after execute.");
            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");
           
			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}		
function job_applicant_mass_invite_insert($company_id_input,$company_client_id_input,$job_id_input,$invitation_sent_by_user_id,$invitation_received_by_user_id_input,$job_invite_ref_code,$last_inserted_id,$event_datetime, $current_epoch,$event_expiry_date_time,$event_expiry_datetime_epoch){
	global $dbcon;
	$is_active_status = '1';
	$is_mass_job_invite = '1';

	$job_applicant_mass_invite_insert_sql = "INSERT INTO `job_applicant_invites` (company_id,company_client_id,job_id,invitation_sent_by_user_id,invitation_received_by_user_id,invite_ref_code,is_mass_job_invite,mc_id,event_date_time,event_date_time_epoch,invite_expiry_date_time,invite_expiry_date_time_epoch,is_active_status) VALUES(:company_id, :company_client_id, :job_id, :invitation_sent_by_user_id, :invitation_received_by_user_id,:invite_ref_code,:is_mass_job_invite,:mc_id,:event_date_time,:event_date_time_epoch,:invite_expiry_date_time,:invite_expiry_date_time_epoch, :is_active_status)";
	$job_applicant_mass_invite_insert_query = $dbcon->prepare($job_applicant_mass_invite_insert_sql);
	$job_applicant_mass_invite_insert_query->bindValue(":company_id",$company_id_input);
	$job_applicant_mass_invite_insert_query->bindValue(":company_client_id",$company_client_id_input);
	$job_applicant_mass_invite_insert_query->bindValue(":job_id",$job_id_input);
	$job_applicant_mass_invite_insert_query->bindValue(":invitation_sent_by_user_id",$invitation_sent_by_user_id);
	$job_applicant_mass_invite_insert_query->bindValue(":invitation_received_by_user_id",$invitation_received_by_user_id_input);
	$job_applicant_mass_invite_insert_query->bindValue(":invite_ref_code",$job_invite_ref_code);
	$job_applicant_mass_invite_insert_query->bindValue(":is_mass_job_invite",$is_mass_job_invite);
	$job_applicant_mass_invite_insert_query->bindValue(":mc_id",$last_inserted_id);
	$job_applicant_mass_invite_insert_query->bindValue(":event_date_time",$event_datetime);
    $job_applicant_mass_invite_insert_query->bindValue(":event_date_time_epoch",$current_epoch);
	$job_applicant_mass_invite_insert_query->bindValue(":invite_expiry_date_time",$event_expiry_date_time);
	$job_applicant_mass_invite_insert_query->bindValue(":invite_expiry_date_time_epoch",$event_expiry_datetime_epoch);
	$job_applicant_mass_invite_insert_query->bindValue(":is_active_status",$is_active_status);

		if ($job_applicant_mass_invite_insert_query->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");

			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }
}												
exit;
?>