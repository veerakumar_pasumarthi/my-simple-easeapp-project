<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Edit Quick Admin Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_office_address_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_office_address_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_office_address_id'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['premise_name'])) {
						$content .= $ea_received_rest_ws_raw_array_input['premise_name'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['premise_name']))
						
					if (isset($ea_received_rest_ws_raw_array_input['address_line_1'])) {
						$content .= $ea_received_rest_ws_raw_array_input['address_line_1'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['address_line_1'])) 	
					
					if (isset($ea_received_rest_ws_raw_array_input['address_line_2'])) {
						$content .= $ea_received_rest_ws_raw_array_input['address_line_2'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['address_line_2'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['city'])) {
						$content .= $ea_received_rest_ws_raw_array_input['city'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['city'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['state'])) {
						$content .= $ea_received_rest_ws_raw_array_input['state'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['state'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['country'])) {
						$content .= $ea_received_rest_ws_raw_array_input['country'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['country'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['pincode'])) {
						$content .= $ea_received_rest_ws_raw_array_input['pincode'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['pincode'])) 	
						
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$company_office_address_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_office_address_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_office_address_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$premise_name_input = trim(isset($ea_received_rest_ws_raw_array_input['premise_name']) ? filter_var($ea_received_rest_ws_raw_array_input['premise_name'], FILTER_SANITIZE_STRING) : '');
				$address_line_1_input = trim(isset($ea_received_rest_ws_raw_array_input['address_line_1']) ? filter_var($ea_received_rest_ws_raw_array_input['address_line_1'], FILTER_SANITIZE_STRING) : '');
				$address_line_2_input = trim(isset($ea_received_rest_ws_raw_array_input['address_line_2']) ? filter_var($ea_received_rest_ws_raw_array_input['address_line_2'], FILTER_SANITIZE_STRING) : '');
				$city_input = trim(isset($ea_received_rest_ws_raw_array_input['city']) ? filter_var($ea_received_rest_ws_raw_array_input['city'], FILTER_SANITIZE_STRING) : '');
				$state_input = trim(isset($ea_received_rest_ws_raw_array_input['state']) ? filter_var($ea_received_rest_ws_raw_array_input['state'], FILTER_SANITIZE_STRING) : '');
				$country_input = trim(isset($ea_received_rest_ws_raw_array_input['country']) ? filter_var($ea_received_rest_ws_raw_array_input['country'], FILTER_SANITIZE_STRING) : '');
				$pincode_input = trim(isset($ea_received_rest_ws_raw_array_input['pincode']) ? filter_var($ea_received_rest_ws_raw_array_input['pincode'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//$eventLog->log("company_id : " . $company_id_input);
				//Check if all inputs are received correctly from the REST Web Service
				if ($company_office_address_id_input == "") {
					//Invalid Mobile Number scenario
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-office-address-id";
					$response['status_description'] = "invalid company office address id, please check and try again.";
					
					$eventLog->log("Please provide a company office address id.");
					
				}  else if ($address_line_1_input == "") {
					//Invalid First name scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-address-line-1";
					$response['status_description'] = "invalid address line 1, please check and try again.";
					
					$eventLog->log("Please provide a valid address line 1.");
				
				} /* else if ($address_line_2_input == "") {
					//Invalid Last name scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-address-line-2";
					$response['status_description'] = "invalid address line 2, please check and try again.";
					
					$eventLog->log("Please provide a valid address line 2.");	
					
				} */ else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Some Additional Information is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");	
				} else if ($city_input == "") {
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-city-input";
					$response['status_description'] = "Invalid City, Please check and try again.";
					
					$eventLog->log("Please provide a valid city.");
					
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//Event Time, as per Indian Standard Time
					$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
		
					$company_office_address_edit_next_step = "";
					
					if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$company_office_address_edit_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else if (isset($ea_extracted_jwt_token_user_company_id)) {
							// else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$company_office_address_edit_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
					} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
					}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
							
						if ($company_office_address_edit_next_step == "PROCEED-TO-NEXT-STEP") {
						$company_office_address_basic_details_result = company_office_address_basic_details_check_based_on_company_office_address_id($company_office_address_id_input);
					
						if (count($company_office_address_basic_details_result) > 0) {
						
						
                       //if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
								$eventLog->log("before enter into query.");
								
								      
									  $company_office_address_basic_details_result_update = company_office_address_basic_details_result_update_based_on_company_office_address_id($company_office_address_id_input,$premise_name_input,$address_line_1_input,$address_line_2_input,$city_input,$state_input,$country_input,$pincode_input,$event_datetime,$current_epoch);
									  
									  
									  
								      $eventLog->log("After update query.");
									  
												if ($company_office_address_basic_details_result_update== true) {
													$eventLog->log("enter into user ass if condition.");
													//Use Privilege details are updated successfully, for the user, in sm_site_member_classification_associations db table
													//Construct Content, that will be sent in Response body, of the REST Web Service
													$response['data'] = array();
													$response['status'] = "company-office-address-details-updated-successfully";
													$response['status_description'] = "company office address details are edited Successfully.";
													
													$eventLog->log("The company office address details are edited Successfully.");
													$eventLog->log("before update status.");
												} else {
													//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
													$response['data'] = array();
													$response['status'] = "company-office-address-data-editing-error";
													$response['status_description'] = "There is an error, when editing the company office address in the Database.";
													
													$eventLog->log("There is an error, when editing the company office address in the Database.");
													
												}
										
										
								} catch (Exception $e) {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "company-office-address-edit-error";
									$response['status_description'] = "Error occurred when Editing the company office address details.";
									
									$eventLog->log("Error occurred when editing the company office address details.");	
									
								}
					   
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "The User Account, that is attempted to be edited, is invalid, please check and try again.";
						
						$eventLog->log("Please provide a Valid User ID.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
					
						
				}//close of else of if ($email_id_input == "") {
				
			}
			
		}	
			
	} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function company_office_address_basic_details_result_update_based_on_company_office_address_id($company_office_address_id_input,$premise_name_input,$address_line_1_input,$address_line_2_input,$city_input,$state_input,$country_input,$pincode_input,$event_datetime,$current_epoch) {
	global $dbcon, $eventLog;
	
	$eventLog->log("in function before query");
    
	$company_office_address_basic_details_update_sql = "UPDATE `company_office_addresses` SET `premise_name`=:premise_name,`address_line_1`=:address_line_1,`address_line_2`=:address_line_2,`city`=:city,`state`=:state,`country`=:country,`pincode`=:pincode,`last_updated_datetime`=:last_updated_datetime,`last_updated_datetime_epoch`=:last_updated_datetime_epoch WHERE `company_office_address_id`=:company_office_address_id";
	$company_office_address_basic_details_update_query = $dbcon->prepare($company_office_address_basic_details_update_sql);
	$eventLog->log("in function before bind");
	
	$company_office_address_basic_details_update_query->bindValue(":company_office_address_id",$company_office_address_id_input);
	$company_office_address_basic_details_update_query->bindValue(":premise_name",$premise_name_input);
	$company_office_address_basic_details_update_query->bindValue(":address_line_1",$address_line_1_input);
	$company_office_address_basic_details_update_query->bindValue(":address_line_2",$address_line_2_input);
	$company_office_address_basic_details_update_query->bindValue(":city",$city_input);
	$company_office_address_basic_details_update_query->bindValue(":state",$state_input);
	$company_office_address_basic_details_update_query->bindValue(":country",$country_input);
	$company_office_address_basic_details_update_query->bindValue(":pincode",$pincode_input);
	$company_office_address_basic_details_update_query->bindValue(":last_updated_datetime",$event_datetime);
	$company_office_address_basic_details_update_query->bindValue(":last_updated_datetime_epoch",$current_epoch);
	
	$eventLog->log("in function after bind");

	if ($company_office_address_basic_details_update_query->execute()) {
		
		$eventLog->log("in function enter into if");

		return true;

	}
	return false;
}
 function company_office_address_basic_details_check_based_on_company_office_address_id($company_office_address_id_input){
	global $dbcon, $eventLog;
	
	$eventLog->log("in function before query");
	$constructed_array = array();
	$rest_company_office_address_check_sql = "SELECT * FROM `company_office_addresses` WHERE `company_office_address_id` = :company_office_address_id";
	$rest_company_office_address_check_select_query = $dbcon->prepare($rest_company_office_address_check_sql);
	$rest_company_office_address_check_select_query->bindValue(":company_office_address_id",$company_office_address_id_input);
	$rest_company_office_address_check_select_query->execute();

	if($rest_company_office_address_check_select_query->rowCount() > 0) {
		$rest_company_office_address_check_select_query_result = $rest_company_office_address_check_select_query->fetch();
	     return $rest_company_office_address_check_select_query_result;

	}//close of if($rest_client_company_check_select_query->rowCount() > 0) {
	return $constructed_array;

} 

exit;
?>


