
<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'])) 	
							
					if (isset($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'])) 
					
				}	
	
					$jsl_applicant_evaluation_info_id_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_applicant_evaluation_info_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					$job_applicant_request_info_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
					} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
					}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						

					if ($jsl_applicant_evaluation_info_id_input == "") {
				
						
						$response['data'] = array();
						$response['status'] = "missing-jsl-applicant-evaluation-info-id";
						$response['status_description'] = "missing JSL applicant evaluation info id";
						
						$eventLog->log("missing JSL applicant evaluation info id.");
						
					} else if ($job_applicant_request_info_id_input == "") {
						
						$response['data'] = array();
						$response['status'] = "missing-job-applicant-request-info-id";
						$response['status_description'] = "Missing Job applicant request info Id";
						
						$eventLog->log("Missing Job applicant request info Id");
					
					} else {	
					
							$jsl_evaluation_info_result = jsl_evaluation_info_get($jsl_applicant_evaluation_info_id_input);
						
							$company_id_input = $jsl_evaluation_info_result["company_id"];
							$company_client_id_input = $jsl_evaluation_info_result["company_client_id"];
							$job_id_input = $jsl_evaluation_info_result["job_id"];
							$jsl_classification_detail_id_input = $jsl_evaluation_info_result["jsl_classification_detail_id"];
							$jsl_classification_detail_name_input = $jsl_evaluation_info_result["jsl_classification_detail_name"];
							$jsl_classification_detail_seo_name_input = $jsl_evaluation_info_result["jsl_classification_detail_seo_name"];
							$jsl_sub_classification_detail_id_input = $jsl_evaluation_info_result["jsl_sub_classification_detail_id"];
							$jsl_sub_classification_detail_name_input = $jsl_evaluation_info_result["jsl_sub_classification_detail_name"];
							$jsl_sub_classification_detail_seo_name_input = $jsl_evaluation_info_result["jsl_sub_classification_detail_seo_name"];
							$jsl_info_id_input = $jsl_evaluation_info_result["jsl_info_id"];
							$job_applicant_sm_memb_id_input = $jsl_evaluation_info_result["job_applicant_sm_memb_id"];
							$event_status_input = $jsl_evaluation_info_result["event_status"];
							
							$jsl_info_details = jsl_info_get($jsl_info_id_input);
							
							if(count($jsl_info_details) > 0){
								$screening_level_owned_by = $jsl_info_details["jsl_owned_by"];
							} 
							
							
							$eventLog->log("jsl_info_id_input");
							$eventLog->log($jsl_info_id_input);
							$eventLog->log($screening_level_owned_by);
						
							$job_rel_screening_level_add_next_step = "";
							if ($ea_extracted_jwt_token_user_type == "admin") {
								
								if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
									$job_rel_screening_level_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("before company id condition");
								} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
									
									$job_rel_screening_level_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("after add condition");
								} else {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "insufficient-permissions-different-company-user-id";
									$response['status_description'] = "Insufficient Set of Permissions";
									
									//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
									header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
									$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
									
								}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								
								
								
							} else {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
								$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
							
							}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
								
								
							
								
							$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
							
							
							
							
							
						
							
							if ($job_rel_screening_level_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
								$job_pool_specific_existence_check_result = job_pool_specific_existence_check($ea_extracted_jwt_token_sub,$job_id_input);
								
								//if(count($job_pool_specific_existence_check_result) > 0) {
								
									try {
										
										//if($event_status_input == '1') {
											
											$jsl_owned_by_input = "1";
											$job_internal_screening_levels_list = job_screening_levels_owned_by_list($job_id_input,$jsl_owned_by_input,$job_applicant_request_info_id_input);
											$internal_screening_levels_count = count($job_internal_screening_levels_list);
											$eventLog->log("internal_screening_levels_count");
											$eventLog->log($internal_screening_levels_count);
											$jsl_owned_by_input = "2";
											$job_external_screening_levels_list = job_screening_levels_owned_by_list($job_id_input,$jsl_owned_by_input,$job_applicant_request_info_id_input);
											$external_screening_levels_count = count($job_external_screening_levels_list);
											$eventLog->log("external_screening_levels_count");
											$eventLog->log($external_screening_levels_count);
											/* echo "screening count value=> .$screening_level_owned_by"; */
											/* $eventLog->log("screening count =>".$screening_level_owned_by); */
											if($screening_level_owned_by == "1") {
												
												if($internal_screening_levels_count > 0 && $external_screening_levels_count > 0) {
													
													$job_statuses_list = array("shortlisted","on-hold","rejected");
													
												} else if($internal_screening_levels_count == "1" &&$external_screening_levels_count == "0") {
													$job_statuses_list = array("selected","on-hold","rejected");	
												}  else {
													$job_statuses_list = array("shortlisted","selected","on-hold","rejected");
													
												} 
												
												$eventLog->log("internal case");
												
											} else if($screening_level_owned_by == "2"){
													
												if($external_screening_levels_count == "1") {
													$job_statuses_list = array("selected","on-hold","rejected");
													
												}else if($external_screening_levels_count > 1) {
													$job_statuses_list = array("selected","shortlisted","on-hold","rejected");
													
												}else if($external_screening_levels_count == "0") {
													$job_statuses_list = array("selected","on-hold","rejected");
													
												} else {
													
													$job_statuses_list = array("shortlisted","selected","on-hold","rejected");
												}
												$eventLog->log("external case");

											} else {
												
												$job_statuses_list = array("shortlisted","selected","on-hold","rejected");

											} 
												
											//$eventLog->log("job status list=>".$job_statuses_list);
											$response['data'] = $job_statuses_list;
											$response['status'] = "applicant-specific-jsl-statuses-list-received-successfully";
											$response['status_description'] = "Applicant specific Job screening level statuses list received Successfully.";
									
											$eventLog->log("Applicant specific Job screening level statuses list received Successfully.");   
											
											
											
											
										/* } else {
											
											$response['data'] = array();
											$response['status'] = "screening-level-was-not-currently-scheduled";
											$response['status_description'] = "screening level is not current schedule screening level for this applicant.";
												
											$eventLog->log("screening level is not current schedule screening level for this applicant.");
											
										} */
										
									} catch (Exception $e) {
										
										$response['data'] = array();
										$response['status'] = "job-related-screening-level-insertion-error";
										$response['status_description'] = "Error occurred when adding the job related screening level.";
											
										$eventLog->log("job-related-screening-level-insertion-error: Error occurred when adding the job related screening level.");
											
									}
									
								/* } else {
									
									$response['data'] = array();
									$response['status'] = "insufficient-permissions";
									$response['status_description'] = "Insufficient Set of Permissions";
									
									//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
									header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
					
									$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
									
								} */
							}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
							
						}
			//}//close of else of if ($email_id_input == "") {
					
		}//close of //close of if ($ea_is_user_page_access_authorized) {
					
				
	} else {
				
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "invalid-auth-token-submitted";
				$response['status_description'] = "Invalid Authentication";
				
				//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
				header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
				
				$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
				
			}//close of else of if ($ea_auth_token_validation_status) {
		
		}//close of if ($ea_maintenance_mode == false) {
		
} else {

		//Construct Content, that will be sent in Response body, of the REST Web Service
		$response['data'] = array();
		$response['status'] = "invalid-input";
		$response['status_description'] = "Invalid Input, Please check and provide all information.";
		
		//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
		
		$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
				
		
		}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




	//Check if Maintenance Mode is Turned On
	if ($ea_maintenance_mode) {	
		
		//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
		//header('Maintenance-Progress: true', false);
		//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
		$response['data'] = array();
		$response['status'] = "application-maintenance-in-progress";
		$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
		
		$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
		
	} else {	

		//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
		header('Content-Type: application/json');
		echo json_encode($response,JSON_PRETTY_PRINT);
		
	}//close of else of if ($ea_maintenance_mode){
		

		
function job_screening_levels_owned_by_list($job_id_input,$jsl_owned_by_input,$job_applicant_request_info_id_input){

	global $dbcon,$eventLog;
	$event_status_input = '1';
	$constructed_array = array();
	
	$job_screening_levels_owned_by_list_sql = "SELECT * FROM `jsl_info` WHERE `job_id` = :job_id AND `jsl_owned_by` = :jsl_owned_by AND `jsl_info_id` NOT IN (SELECT `jsl_info_id` FROM `jsl_applicant_evaluation_info` WHERE `job_id` = :job_id_input AND `job_applicant_request_info_id` = :job_applicant_request_info_id AND `event_status` NOT IN (3,8))";
	
	$job_screening_levels_owned_by_list_query = $dbcon->prepare($job_screening_levels_owned_by_list_sql);
	$job_screening_levels_owned_by_list_query->bindValue(":job_id",$job_id_input);
	$job_screening_levels_owned_by_list_query->bindValue(":job_id_input",$job_id_input);
	$job_screening_levels_owned_by_list_query->bindValue(":jsl_owned_by",$jsl_owned_by_input);
	$job_screening_levels_owned_by_list_query->bindValue(":job_applicant_request_info_id",$job_applicant_request_info_id_input);
	
	$job_screening_levels_owned_by_list_query->execute(); 
	
	if($job_screening_levels_owned_by_list_query->rowCount() > 0) {
		$job_screening_levels_owned_by_list_query_result = $job_screening_levels_owned_by_list_query->fetchAll();
	     return $job_screening_levels_owned_by_list_query_result;
	
	}
	return $constructed_array;


}

function job_pool_specific_existence_check($ea_extracted_jwt_token_sub,$job_id_input) {
	
	global $dbcon,$eventLog;
	$assignment_status_input = '1';
	$constructed_array = array();
	
	$job_pool_specific_existence_check_sql = "SELECT * FROM `job_management_assignees` WHERE `job_id` = :job_id AND `assigned_user_sm_memb_id` = :assigned_user_sm_memb_id AND `assignment_status` = :assignment_status";
	
	$job_pool_specific_existence_check_query = $dbcon->prepare($job_pool_specific_existence_check_sql);
	$job_pool_specific_existence_check_query->bindValue(":job_id",$job_id_input);
	$job_pool_specific_existence_check_query->bindValue(":assignment_status",$assignment_status_input);
	$job_pool_specific_existence_check_query->bindValue(":assigned_user_sm_memb_id",$ea_extracted_jwt_token_sub);
	
	$job_pool_specific_existence_check_query->execute(); 
	
	if($job_pool_specific_existence_check_query->rowCount() > 0) {
		$job_pool_specific_existence_check_query_result = $job_pool_specific_existence_check_query->fetchAll();
	     return $job_pool_specific_existence_check_query_result;
	
	}
	return $constructed_array;
}

function jsl_info_get($jsl_info_id_input){
	
	global $dbcon,$eventLog;
	$constructed_array = array();
	
	$job_pool_specific_existence_check_sql = "SELECT * FROM `jsl_info` WHERE `jsl_info_id` = :jsl_info_id";
	
	$job_pool_specific_existence_check_query = $dbcon->prepare($job_pool_specific_existence_check_sql);
	$job_pool_specific_existence_check_query->bindValue(":jsl_info_id",$jsl_info_id_input);
	
	$job_pool_specific_existence_check_query->execute(); 
	
	if($job_pool_specific_existence_check_query->rowCount() > 0) {
		$job_pool_specific_existence_check_query_result = $job_pool_specific_existence_check_query->fetch();
	     return $job_pool_specific_existence_check_query_result;
	
	}
	return $constructed_array;
	
	
}

exit;
?>


