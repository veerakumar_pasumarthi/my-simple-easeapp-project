<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * REST API for Checking the Status of Password Setup Request Ref Code
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
 
if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "2")) {
				
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		if (is_array($ea_received_rest_ws_raw_array_input)) {
			$content = "";
			
			if (isset($ea_received_rest_ws_raw_array_input['upload_ref_code'])) {
				$content .= $ea_received_rest_ws_raw_array_input['upload_ref_code'] . ":::::";
			}//close of if (isset($ea_received_rest_ws_raw_array_input['upload_ref_code'])) {
			
			if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
				$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
			}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
			
			$eventLog->log("Received Inputs => ".$content);
			
			//DO WRITE REST WEB SERVICE AUTHORIZATION CHECK, for ALL REST WEB SERVICES, IN HERE.
			
		}//close of if ($ea_received_rest_ws_raw_array_input != "") {
			
		//Filter Inputs	
		$upload_ref_code_input = trim(isset($ea_received_rest_ws_raw_array_input['upload_ref_code']) ? filter_var($ea_received_rest_ws_raw_array_input['upload_ref_code'], FILTER_SANITIZE_STRING) : '');
		
		//Check if the IP Address Input is a Valid IPv4 Address
		if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
			//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
			$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
		} else {
			$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
			$ip_address_input = '';
		}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
		
		
		//Check if all inputs are received correctly from the REST Web Service
		if ($upload_ref_code_input == "") {
			//Invalid Password Request Code scenario
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "empty-upload-request-code";
			$response['status_description'] = "Empty upload Request Code Submitted, please submit a Valid Password Request Code.";
			
			$eventLog->log("Empty upload Request Code Submitted, please submit a Valid Password Request Code.");
			
		} else if ($ip_address_input == "") {
			//One or More Inputs are Missing!!!
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "missing-additional-information";
			$response['status_description'] = "Some Additional Information like Password and / or IP Address (IPv4) is missing, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
			$eventLog->log("Please provide all information.");
					  
		} else {	
		    //All inputs are Valid
			
			//Event Time, as per Indian Standard Time
			$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
									
									
			$eventLog->log("All inputs are valid.");
			//$eventLog->log("Received Inputs - " . $content);
			
				
				//Get Details of Password Request Code
				$upload_ref_code_result = get_ref_code_details($upload_ref_code_input);
				
				if (count($upload_ref_code_result) > 0) {
					
					$response['data'] = $upload_ref_code_result;
						$response['status'] = "upload-ref-code-already-exists";
						$response['status_description'] = "this upload ref code already exists,please check and try with a different Password Request Code";
						
						$eventLog->log("this upload ref code already exists, please check and try with a different Password Request Code.");
				
					
				} else {
					$response['data'] = array();
					$response['status'] = "upload-ref-code-not-exists";
					$response['status_description'] = "upload ref code not exists";
						
						$eventLog->log("this upload ref code already exists, please check and try with a different Password Request Code.");
				}
				
			
			
			$eventLog->logNewSeperator();			
		}//close of else of if ($user_unique_identifier_string_setting == "") {
	}//close of if ($ea_maintenance_mode == false) {
	
} else {
	
	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	//$response['jwt-audience'] = array();
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "2")) {

//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, with Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
function get_ref_code_details($upload_ref_code_input) {
	
	global $dbcon;
	
	$constructed_array = array();
	
	$user_account_password_request_details_get_sql = "SELECT * FROM  `jsl_applicant_evaluation_info` WHERE `upload_ref_code` =:upload_ref_code";	
	$user_account_password_request_details_get_select_query = $dbcon->prepare($user_account_password_request_details_get_sql);		
	$user_account_password_request_details_get_select_query->bindValue(":upload_ref_code",$upload_ref_code_input);
	$user_account_password_request_details_get_select_query->execute();
	if($user_account_password_request_details_get_select_query->rowCount() > 0) {
		$user_account_password_request_details_get_select_query_result = $user_account_password_request_details_get_select_query->fetch();
		
		return $user_account_password_request_details_get_select_query_result;
		
	}//close of if($user_account_password_request_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
}
exit;
?>