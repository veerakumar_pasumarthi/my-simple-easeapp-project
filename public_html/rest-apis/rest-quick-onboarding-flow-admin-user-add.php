<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Admin Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "18")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['email_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['email_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['email_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['mobile_number'])) {
						$content .= $ea_received_rest_ws_raw_array_input['mobile_number'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['mobile_number'])) 
					if (isset($ea_received_rest_ws_raw_array_input['sm_mobile_phone_country_code'])) {
						$content .= $ea_received_rest_ws_raw_array_input['sm_mobile_phone_country_code'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['sm_mobile_phone_country_code'])) 	
					
					if (isset($ea_received_rest_ws_raw_array_input['alternate_email_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['alternate_email_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['alternate_email_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['salutation'])) {
						$content .= $ea_received_rest_ws_raw_array_input['salutation'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['salutation'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['firstname'])) {
						$content .= $ea_received_rest_ws_raw_array_input['firstname'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['firstname'])) 

					if (isset($ea_received_rest_ws_raw_array_input['middlename'])) {
						$content .= $ea_received_rest_ws_raw_array_input['middlename'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['middlename']))
					
					if (isset($ea_received_rest_ws_raw_array_input['lastname'])) {
						$content .= $ea_received_rest_ws_raw_array_input['lastname'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['lastname']))
					
					if (isset($ea_received_rest_ws_raw_array_input['dob'])) {
						$content .= $ea_received_rest_ws_raw_array_input['dob'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['dob']))
						
					if (isset($ea_received_rest_ws_raw_array_input['address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['address']))	
					
					if (isset($ea_received_rest_ws_raw_array_input['city'])) {
						$content .= $ea_received_rest_ws_raw_array_input['city'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['city']))	
					
                    if (isset($ea_received_rest_ws_raw_array_input['state'])) {
						$content .= $ea_received_rest_ws_raw_array_input['state'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['state']))
						
					if (isset($ea_received_rest_ws_raw_array_input['country'])) {
						$content .= $ea_received_rest_ws_raw_array_input['country'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['country']))
						
                    	
					if (isset($ea_received_rest_ws_raw_array_input['designation'])) {
						$content .= $ea_received_rest_ws_raw_array_input['designation'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['designation']))	
					
                    if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id']))				
						
					if (isset($ea_received_rest_ws_raw_array_input['user_type'])) {
						$content .= $ea_received_rest_ws_raw_array_input['user_type'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['user_type']))
					
					if (isset($ea_received_rest_ws_raw_array_input['admin_classification'])) {
						$content .= $ea_received_rest_ws_raw_array_input['admin_classification'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['admin_classification']))
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$dob_input = trim(isset($ea_received_rest_ws_raw_array_input['dob']) ? filter_var($ea_received_rest_ws_raw_array_input['dob'], FILTER_SANITIZE_STRING) : '');
				$salutation_input = trim(isset($ea_received_rest_ws_raw_array_input['salutation']) ? filter_var($ea_received_rest_ws_raw_array_input['salutation'], FILTER_SANITIZE_STRING) : '');
				$firstname_input = trim(isset($ea_received_rest_ws_raw_array_input['firstname']) ? filter_var($ea_received_rest_ws_raw_array_input['firstname'], FILTER_SANITIZE_STRING) : '');
				
				$middlename_input = trim(isset($ea_received_rest_ws_raw_array_input['middlename']) ? filter_var($ea_received_rest_ws_raw_array_input['middlename'], FILTER_SANITIZE_STRING) : '');
				$lastname_input = trim(isset($ea_received_rest_ws_raw_array_input['lastname']) ? filter_var($ea_received_rest_ws_raw_array_input['lastname'], FILTER_SANITIZE_STRING) : '');
				$sm_mobile_phone_country_code_input = trim(isset($ea_received_rest_ws_raw_array_input['sm_mobile_phone_country_code']) ? filter_var($ea_received_rest_ws_raw_array_input['sm_mobile_phone_country_code'], FILTER_SANITIZE_NUMBER_INT) : '');
				$mobile_number_input = trim(isset($ea_received_rest_ws_raw_array_input['mobile_number']) ? filter_var($ea_received_rest_ws_raw_array_input['mobile_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				$address_input = trim(isset($ea_received_rest_ws_raw_array_input['address']) ? filter_var($ea_received_rest_ws_raw_array_input['address'], FILTER_SANITIZE_STRING) : '');
				$city_input = trim(isset($ea_received_rest_ws_raw_array_input['city']) ? filter_var($ea_received_rest_ws_raw_array_input['city'], FILTER_SANITIZE_STRING) : '');
				$state_input = trim(isset($ea_received_rest_ws_raw_array_input['state']) ? filter_var($ea_received_rest_ws_raw_array_input['state'], FILTER_SANITIZE_STRING) : '');
				$country_input = trim(isset($ea_received_rest_ws_raw_array_input['country']) ? filter_var($ea_received_rest_ws_raw_array_input['country'], FILTER_SANITIZE_STRING) : '');
				$designation_input = trim(isset($ea_received_rest_ws_raw_array_input['designation']) ? filter_var($ea_received_rest_ws_raw_array_input['designation'], FILTER_SANITIZE_STRING) : '');
				$email_id_input = trim(isset($ea_received_rest_ws_raw_array_input['email_id']) ? filter_var($ea_received_rest_ws_raw_array_input['email_id'], FILTER_SANITIZE_EMAIL) : '');	
				$alternate_email_id_input = trim(isset($ea_received_rest_ws_raw_array_input['alternate_email_id']) ? filter_var($ea_received_rest_ws_raw_array_input['alternate_email_id'], FILTER_SANITIZE_EMAIL) : '');
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');	
				$user_type_input = trim(isset($ea_received_rest_ws_raw_array_input['user_type']) ? filter_var($ea_received_rest_ws_raw_array_input['user_type'], FILTER_SANITIZE_STRING) : '');
				$admin_classification_input = trim(isset($ea_received_rest_ws_raw_array_input['admin_classification']) ? filter_var($ea_received_rest_ws_raw_array_input['admin_classification'], FILTER_SANITIZE_NUMBER_INT) : '');				
				//$eventLog->log("User Type Before strtolower : " . $user_type_input);
				if($salutation_input == "")
				{  
			        $salutation_input = null;
				}
				
				
				$user_type_input = strtolower_utf8_extended($user_type_input);
				
				//$eventLog->log("company_id a : " . $company_id_input);
				//$eventLog->log("User Type After strtolower : " . $user_type_input);
				
				if (!filter_var($email_id_input, FILTER_VALIDATE_EMAIL) == true) {
					$eventLog->log($email_id_input . " - Not a Valid Email Address");
					$email_id_input = "";
				} else if (($mobile_number_input == '0') || (!ctype_digit($mobile_number_input))) {
						$eventLog->log($mobile_number_input . " - Not a Valid Mobile Number");
						$mobile_number_input = "";
				} else if (($company_id_input == '')) {
						$eventLog->log($company_id_input . " - Not a Valid company id");
						//$company_id_input = "";
				} else if (!filter_var($alternate_email_id_input, FILTER_VALIDATE_EMAIL) == true) {
					$eventLog->log($alternate_email_id_input . " - Not a Valid Alternate Email Address");
					$alternate_email_id_input = "";
				}//close of else if of if (!filter_var($email_id_input, FILTER_VALIDATE_EMAIL) == true) {
						
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//$eventLog->log("company_id b: " . $company_id_input);
				//Check if all inputs are received correctly from the REST Web Service
				if ($email_id_input == "") {
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-email-id";
					$response['status_description'] = "Invalid email id, please check and try again.";
					
					$eventLog->log("Please provide a valid email address.");
					
				} /* else if ($mobile_number_input == "") {
					//Invalid Mobile Number scenario
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-mobile-number";
					$response['status_description'] = "invalid mobile number, please check and try again.";
					
					$eventLog->log("Please provide a valid mobile number.");
					
				} */ else if ($firstname_input == "") {
					//Invalid First name scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-firstname";
					$response['status_description'] = "invalid firstname, please check and try again.";
					
					$eventLog->log("Please provide a valid firstname.");
				
				} /* else if ($lastname_input == "") {
					//Invalid Last name scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-lastname";
					$response['status_description'] = "invalid lastname, please check and try again.";
					
					$eventLog->log("Please provide a valid lastname.");	
					
				} */ else if ($admin_classification_input != "12" && $company_id_input == "") {
					//Invalid Last name scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "invalid Company Id, please check and try again.";
					
					$eventLog->log("Please provide a valid Company Id.");
					
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Some Additional Information is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");	
					
				} /* else if ($alternate_email_id_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-alternate-email-id";
					$response['status_description'] = "Alternate Email Id is missing, please check and try again.";
					
					$eventLog->log("Alternate Email Id is missing, please check and try again.");	
					
				
				} */ else if ($user_type_input != 'admin') {
				   
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-user-type-information";
					$response['status_description'] = "invalid user type, please check and try again.";
					
					$eventLog->log("Please provide valid user type.");
					
				} else if ($admin_classification_input == "") {
				   
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-admin-classification-input-information";
					$response['status_description'] = "invalid admin classification input, please check and try again.";
					
					$eventLog->log("Please provide valid .");
								
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					$user_au_validation_result = user_register_basic_details_insert_duplicate_check($email_id_input);
					
					if (count($user_au_validation_result) > 0) {
			
						$eventLog->log("user email_id already exists");
						$response['data'] = array();
						$response['status'] = "user-already-exists";
						$response['status_description'] = "A User already exists with this Email ID, Please check and try again.";
						
					} else {
						$check = "";
						
						//Event Time, as per Indian Standard Time
						$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
						$event_expiry_datetime_epoch = $current_epoch+$password_resetting_link_expiry_period;	
						//$valid_from_date_input = '2018-11-10 16:18:20';
						$email_activation_code = create_email_act_code($hash_algorithm, $current_epoch, "25");
						
						//Check if the User, whose sm_memb_id is received from "sub" of JWT Auth Token exists
						if (count($ea_token_based_sub_user_id_details) > 0) {
							
							
							if (($admin_classification_input == "3") || ($admin_classification_input == "5") || ($admin_classification_input == "6") || ($admin_classification_input == "7") || ($admin_classification_input == "4") || ($admin_classification_input == "9")) { 
								
								$admin_user_level = 1;
								$check = "DO-PROCEED";
								$last_inserted_id = admin_user_register_basic_details_insert_value($email_id_input,$sm_mobile_phone_country_code_input,$mobile_number_input,$alternate_email_id_input,$salutation_input,$firstname_input,$middlename_input,$lastname_input,$dob_input,$address_input,$city_input,$state_input,$country_input,$designation_input,$company_id_input,$user_type_input,$admin_user_level, "admin_user", $ea_token_based_sub_user_id_details['sm_memb_id'], $ea_token_based_sub_user_id_details['sm_firstname'], $ea_token_based_sub_user_id_details['sm_middlename'], $ea_token_based_sub_user_id_details['sm_lastname'], $event_datetime, $current_epoch, $email_activation_code, "0");
							
							} else if ($admin_classification_input == "12") {
								$company_id_input = null;
								$admin_user_level = 2;
								$check = "DO-PROCEED";
								$last_inserted_id = admin_user_register_basic_details_insert_value($email_id_input,$sm_mobile_phone_country_code_input,$mobile_number_input,$alternate_email_id_input,$salutation_input,$firstname_input,$middlename_input,$lastname_input,$dob_input,$address_input,$city_input,$state_input,$country_input,$designation_input,$company_id_input,$user_type_input,$admin_user_level, "admin_user", $ea_token_based_sub_user_id_details['sm_memb_id'], $ea_token_based_sub_user_id_details['sm_firstname'], $ea_token_based_sub_user_id_details['sm_middlename'], $ea_token_based_sub_user_id_details['sm_lastname'], $event_datetime, $current_epoch, $email_activation_code, "0");
							
							} else {
								//$eventLog->log("A User is added into sm_members table Successfully.");
								$eventLog->log("This Admin User is not Inserted.");
							}
							
						} else {
							
							if (($admin_classification_input == "3") ||  ($admin_classification_input == "5") || ($admin_classification_input == "6") || ($admin_classification_input == "7") || ($admin_classification_input == "8") || ($admin_classification_input == "9")) { 
								
								$admin_user_level = 1;
								$check = "DO-PROCEED";
								$last_inserted_id = admin_user_register_basic_details_insert_value($email_id_input,$sm_mobile_phone_country_code_input,$mobile_number_input,$alternate_email_id_input,$salutation_input,$firstname_input,$middlename_input,$lastname_input,$dob_input,$address_input,$city_input,$state_input,$country_input,$designation_input,$company_id_input,$user_type_input,$admin_user_level, "admin_user", null, null, null, null, $event_datetime, $current_epoch, $email_activation_code, "0");
							
							} else if ($admin_classification_input == "12") {
								
								$admin_user_level = 2;
								$check = "DO-PROCEED";
								$last_inserted_id = admin_user_register_basic_details_insert_value($email_id_input,$sm_mobile_phone_country_code_input,$mobile_number_input,$alternate_email_id_input,$salutation_input,$firstname_input,$middlename_input,$lastname_input,$dob_input,$address_input,$city_input,$state_input,$country_input,$designation_input,$company_id_input,$user_type_input,$admin_user_level, "admin_user", null, null, null, null, $event_datetime, $current_epoch, $email_activation_code, "0");
							
							} else {
								
								//$eventLog->log("A User is added into sm_members table Successfully.");
								$eventLog->log("This Admin User is not Inserted.");
							}
							
						}//close of else of if (count($ea_token_based_sub_user_id_details) > 0) {
						
						
						if ($check == "DO-PROCEED") {					
							
							//$valid_from_date_input = '2018-11-10 16:18:20';
							$sm_classification_detail_id_input = $admin_classification_input;
							
							$user_ass_au_validation_result = user_association_record_check($last_inserted_id,$admin_classification_input);
							
							if (count($user_ass_au_validation_result) > 0) {
								
								$sm_site_member_classification_association_id = $user_ass_au_validation_result("sm_site_member_classification_association_id");
								$user_ass_au_update = user_association_record_update($sm_site_member_classification_association_id);
								
							} else {
					
								//$user_ass_au_last_inserted_id = user_association_record_insert($last_inserted_id,$sm_classification_detail_id_input,$valid_from_date_input);
								$user_ass_au_last_inserted_id = user_association_record_insert($last_inserted_id,$sm_classification_detail_id_input,$event_datetime);
								
								if ($user_ass_au_last_inserted_id != "") {
									if($last_inserted_id != "") {
								          
								           $request_purpose = "activation-email";
									       $request_destination_input = "primary-email";
									       $last_generated_password_setup_mod_request_result = ea_generate_user_password_setup_modification_request_ref_code($ea_extracted_jwt_token_sub, $last_inserted_id, $request_destination_input, $email_id_input, $event_datetime, $current_epoch, $event_expiry_datetime_epoch,$request_purpose);
							               $eventLog->log("after password record insert");
										   if($last_generated_password_setup_mod_request_result > 0) {
										    $response['data'] = array();
											$response['status'] = "user-insert-successful";
											$response['status_description'] = "A User is added Successfully.";
										
											$eventLog->log("user-insert-successful: A User is added Successfully.");
										   }
										}
									
								} else {
									//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
									$response['data'] = array();
									$response['status'] = "user-insert-error";
									$response['status_description'] = "There is an error, when adding the User in the Database.";
									
									$eventLog->log("There is an error, when adding the User in the Database.");
									
								}
								
							}//close of else of if (count($user_ass_au_validation_result) > 0) {
							
						}//close of if ($check == "DO-PROCEED") {	
						
					}//close of else of if (count($user_au_validation_result) > 0) {
						
				}//close of else of if ($email_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "11")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){

exit;
?>