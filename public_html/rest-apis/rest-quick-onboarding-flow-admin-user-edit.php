<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Edit Quick Admin Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "17")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['user_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['user_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['user_id'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['mobile_number'])) {
						$content .= $ea_received_rest_ws_raw_array_input['mobile_number'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['mobile_number']))
						
					if (isset($ea_received_rest_ws_raw_array_input['sm_mobile_phone_country_code'])) {
						$content .= $ea_received_rest_ws_raw_array_input['sm_mobile_phone_country_code'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['sm_mobile_phone_country_code']))	
						
					if (isset($ea_received_rest_ws_raw_array_input['alternate_email'])) {
						$content .= $ea_received_rest_ws_raw_array_input['alternate_email'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['alternate_email'])) 	
					
					if (isset($ea_received_rest_ws_raw_array_input['salutation'])) {
						$content .= $ea_received_rest_ws_raw_array_input['salutation'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['salutation'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['firstname'])) {
						$content .= $ea_received_rest_ws_raw_array_input['firstname'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['firstname'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['middlename'])) {
						$content .= $ea_received_rest_ws_raw_array_input['middlename'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['middlename'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['lastname'])) {
						$content .= $ea_received_rest_ws_raw_array_input['lastname'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['lastname'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['dob'])) {
						$content .= $ea_received_rest_ws_raw_array_input['dob'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['dob'])) 	
						
					if (isset($ea_received_rest_ws_raw_array_input['address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['address'])) 

					if (isset($ea_received_rest_ws_raw_array_input['city'])) {
						$content .= $ea_received_rest_ws_raw_array_input['city'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['city']))
						
					if (isset($ea_received_rest_ws_raw_array_input['state'])) {
						$content .= $ea_received_rest_ws_raw_array_input['state'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['state']))
						
					if (isset($ea_received_rest_ws_raw_array_input['country'])) {
						$content .= $ea_received_rest_ws_raw_array_input['country'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['country']))	
						
					if (isset($ea_received_rest_ws_raw_array_input['designation'])) {
						$content .= $ea_received_rest_ws_raw_array_input['designation'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['designation']))
					
                    if (isset($ea_received_rest_ws_raw_array_input['admin_classification'])) {
						$content .= $ea_received_rest_ws_raw_array_input['admin_classification'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['admin_classification']))				
						
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id']))	
						
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$mobile_number_input = trim(isset($ea_received_rest_ws_raw_array_input['mobile_number']) ? filter_var($ea_received_rest_ws_raw_array_input['mobile_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				$sm_mobile_phone_country_code_input = trim(isset($ea_received_rest_ws_raw_array_input['sm_mobile_phone_country_code']) ? filter_var($ea_received_rest_ws_raw_array_input['sm_mobile_phone_country_code'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$salutation_input = trim(isset($ea_received_rest_ws_raw_array_input['salutation']) ? filter_var($ea_received_rest_ws_raw_array_input['salutation'], FILTER_SANITIZE_STRING) : '');
				$firstname_input = trim(isset($ea_received_rest_ws_raw_array_input['firstname']) ? filter_var($ea_received_rest_ws_raw_array_input['firstname'], FILTER_SANITIZE_STRING) : '');
				$middlename_input = trim(isset($ea_received_rest_ws_raw_array_input['middlename']) ? filter_var($ea_received_rest_ws_raw_array_input['middlename'], FILTER_SANITIZE_STRING) : '');
				$lastname_input = trim(isset($ea_received_rest_ws_raw_array_input['lastname']) ? filter_var($ea_received_rest_ws_raw_array_input['lastname'], FILTER_SANITIZE_STRING) : '');
				
				$dob_input = trim(isset($ea_received_rest_ws_raw_array_input['dob']) ? filter_var($ea_received_rest_ws_raw_array_input['dob'], FILTER_SANITIZE_STRING) : '');
				$address_input = trim(isset($ea_received_rest_ws_raw_array_input['address']) ? filter_var($ea_received_rest_ws_raw_array_input['address'], FILTER_SANITIZE_STRING) : '');
				$city_input = trim(isset($ea_received_rest_ws_raw_array_input['city']) ? filter_var($ea_received_rest_ws_raw_array_input['city'], FILTER_SANITIZE_STRING) : '');
				$state_input = trim(isset($ea_received_rest_ws_raw_array_input['state']) ? filter_var($ea_received_rest_ws_raw_array_input['state'], FILTER_SANITIZE_STRING) : '');
				$country_input = trim(isset($ea_received_rest_ws_raw_array_input['country']) ? filter_var($ea_received_rest_ws_raw_array_input['country'], FILTER_SANITIZE_STRING) : '');
				$designation_input = trim(isset($ea_received_rest_ws_raw_array_input['designation']) ? filter_var($ea_received_rest_ws_raw_array_input['designation'], FILTER_SANITIZE_STRING) : '');
				$admin_classification_input = trim(isset($ea_received_rest_ws_raw_array_input['admin_classification']) ? filter_var($ea_received_rest_ws_raw_array_input['admin_classification'], FILTER_SANITIZE_NUMBER_INT) : '');
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$alternate_email_id_input = trim(isset($ea_received_rest_ws_raw_array_input['alternate_email']) ? filter_var($ea_received_rest_ws_raw_array_input['alternate_email'], FILTER_SANITIZE_EMAIL) : '');
					
                if($salutation_input == "")
				{  
			        $salutation_input = null;
				}		

				if($admin_classification_input == "12"){
					
					$company_id_input = null;
					
				}
				
				//$eventLog->log("company_id a : " . $company_id_input);
				//$eventLog->log("User Type After strtolower : " . $user_type_input);
				
				if (($mobile_number_input == '0') || (!ctype_digit($mobile_number_input))) {
						$eventLog->log($mobile_number_input . " - Not a Valid Mobile Number");
						$mobile_number_input = "";
				} else if (!filter_var($alternate_email_id_input, FILTER_VALIDATE_EMAIL) == true) {
					$eventLog->log($alternate_email_id_input . " - Not a Valid Alternate Email Address");
					$alternate_email_id_input = "";
				}//close of else if of if (!filter_var($email_id_input, FILTER_VALIDATE_EMAIL) == true) {
						
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//$eventLog->log("company_id b: " . $company_id_input);
				//Check if all inputs are received correctly from the REST Web Service
				/* if ($email_id_input == "") {
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-email-id";
					$response['status_description'] = "Invalid email id, please check and try again.";
					
					$eventLog->log("Please provide a valid email address.");
					
				} */ /* else if ($mobile_number_input == "") {
					//Invalid Mobile Number scenario
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-mobile-number";
					$response['status_description'] = "invalid mobile number, please check and try again.";
					
					$eventLog->log("Please provide a valid mobile number.");
					
				} */  if ($firstname_input == "") {
					//Invalid First name scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-firstname";
					$response['status_description'] = "invalid firstname, please check and try again.";
					
					$eventLog->log("Please provide a valid firstname.");
				
				} /* else if ($lastname_input == "") {
					//Invalid Last name scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-lastname";
					$response['status_description'] = "invalid lastname, please check and try again.";
					
					$eventLog->log("Please provide a valid lastname.");	
					
				} */ else if ($admin_classification_input != "12" && $company_id_input == "") {
					//Invalid Last name scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "invalid Company Id, please check and try again.";
					
					$eventLog->log("Please provide a valid Company Id.");
					
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Some Additional Information is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");	
					
				}  /* else if ($user_type_input != 'admin') {
				   
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-user-type-information";
					$response['status_description'] = "invalid user type, please check and try again.";
					
					$eventLog->log("Please provide valid user type.");
					
				} */ else if ($admin_classification_input == "") {
				   
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-admin-classification-input-information";
					$response['status_description'] = "invalid admin classification input, please check and try again.";
					
					$eventLog->log("Please provide valid .");
								
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//Event Time, as per Indian Standard Time
					$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
					
					$ea_action_specific_auth_check_result = "";
					
					//User Details, based on User ID input, through api request
					$user_basic_details_result = user_basic_details_check_based_on_user_id($user_id_input);
					
					if (count($user_basic_details_result) > 0) {
						
						//$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
					
						$ea_action_specific_auth_check_result = ea_check_action_specific_user_privileges_different_user_levels_reset_password_feature($ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_user_company_id,  $ea_extracted_jwt_token_sub, $user_id_input, $ea_extracted_jwt_token_user_privileges_list, $user_basic_details_result["company_id"], $page_filename);					
						
						//echo "ea_action_specific_auth_check_result: " . $ea_action_specific_auth_check_result;					
							
							
						

						
						if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
								
								$user_update_sql = "UPDATE `site_members` SET   `sm_mobile`=:sm_mobile,`sm_mobile_phone_country_code`=:sm_mobile_phone_country_code,`sm_alternate_email`=:sm_alternate_email,`sm_salutation`=:sm_salutation,`sm_firstname`=:sm_firstname,`sm_middlename`=:sm_middlename,`sm_lastname`=:sm_lastname,`sm_dob`=:sm_dob,`sm_address`=:sm_address,`sm_city`=:sm_city,`sm_state`=:sm_state,`sm_country`=:sm_country,`sm_designation`=:sm_designation,`company_id`=:company_id,`last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch WHERE `sm_memb_id`=:sm_memb_id";
									
								$user_update_values_array = array(":sm_mobile" => $mobile_number_input,"sm_mobile_phone_country_code" => $sm_mobile_phone_country_code_input,":sm_alternate_email" => $alternate_email_id_input, ":sm_salutation" => $salutation_input, ":sm_firstname" => $firstname_input, ":sm_middlename" => $middlename_input, ":sm_lastname" => $lastname_input,":sm_dob" => $dob_input,":sm_address" => $address_input,":sm_city" => $city_input,":sm_state" => $state_input,":sm_country" => $country_input,":sm_designation" => $designation_input,":company_id" => $company_id_input,":last_updated_date_time" => $event_datetime, ":last_updated_date_time_epoch" => $current_epoch, ":sm_memb_id" => $user_id_input);
								
								$user_update_status = update_query_based_on_id($user_update_sql, $user_update_values_array);
								
								//echo "user_update_status: " . var_dump($user_update_status);
								if ($user_update_status == true) {
									$eventLog->log("updated site_members db table.");	
									
									$user_classification_association_details_result = user_classifiation_association_record_details_get($user_id_input);
									
									if (count($user_classification_association_details_result)) {
										$sm_site_member_classification_association_id = $user_classification_association_details_result["sm_site_member_classification_association_id"];
										$sm_site_member_classification_detail_id = $user_classification_association_details_result["sm_site_member_classification_detail_id"];
												$eventLog->log("enter into if condition.");
										
										if ($admin_classification_input != $sm_site_member_classification_detail_id) {
											//Do Disable the record
											update_user_classification_association_status_based_on_sm_site_member_classification_association_id($sm_site_member_classification_association_id, "0", $event_datetime, $sm_site_member_classification_detail_id);
											
											$eventLog->log("enter into second if  condition.");
											
											//Do Insert new classification association
											$user_ass_au_last_inserted_id = user_association_record_insert($user_id_input,$admin_classification_input,$event_datetime);
											
											if ($user_ass_au_last_inserted_id != "") {
												$eventLog->log("enter into user ass if condition.");
												//Use Privilege details are updated successfully, for the user, in sm_site_member_classification_associations db table
												//Construct Content, that will be sent in Response body, of the REST Web Service
												$response['data'] = array();
												$response['status'] = "user-details-updated-successfully";
												$response['status_description'] = "The User details are edited Successfully.";
												
												$eventLog->log("The User details are edited Successfully, along with change of admin classification details (admin access level).");
												
											} else {
												//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
												$response['data'] = array();
												$response['status'] = "user-data-editing-error";
												$response['status_description'] = "There is an error, when editing the User in the Database.";
												
												$eventLog->log("There is an error, when editing the User in the Database.");
												
											}
											
										} else {
											//Construct Content, that will be sent in Response body, of the REST Web Service
											$response['data'] = array();
											$response['status'] = "user-details-updated-successfully";
											$response['status_description'] = "The User details are edited Successfully.";
											
											$eventLog->log("The User details are edited Successfully.");
											
										}//close of else of if ($admin_classification_input != $sm_site_member_classification_detail_id) {
											
									}//close of if (count($user_classification_association_details_result)) {
										
									
									
									
									
								} else {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "error-updating-user-details";
									$response['status_description'] = "There occurred an error when updating the Candidate's Details, Please check and try again later.";
									
								}//close of else of if ($user_update_status == true) {
								
									
							} catch (Exception $e) {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "admin-user-edit-error";
								$response['status_description'] = "Error occurred when Editing the Admin User Account.";
								
								$eventLog->log("Error occurred when registering the Password Reset Request, w.r.t. particular User's Email Address.");	
								
							}
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-subject-platform-scope-user";
							$response['status_description'] = "Insufficient Set of Permissions, to complete this action, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-different-company-user-id";
							$response['status_description'] = "Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-INSUFFICIENT-PERMISSIONS") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-self-action";
							$response['status_description'] = "Insufficient Set of Permissions, as self action is Dis-allowed, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-ACTION-SUBJECT-IS-A-DIFFERENT-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-action-on-different-user";
							$response['status_description'] = "Insufficient Set of Permissions, as action subject is a different user, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						}//close of if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
						
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "The User Account, that is attempted to be edited, is invalid, please check and try again.";
						
						$eventLog->log("Please provide a Valid User ID.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
					
						
				}//close of else of if ($email_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){


function user_classifiation_association_record_details_get($sm_memb_id_input) {
	global $dbcon;
	$constructed_array = array();
	$valid_to_date_input = '%'.'present'.'%';
	$is_active_status_input = '1';
	
	$user_association_record_check_sql = "SELECT * FROM sm_site_member_classification_associations WHERE sm_memb_id = :sm_memb_id AND valid_to_date LIKE :valid_to_date AND is_active_status = :is_active_status"; 
	$user_association_record_check_q = $dbcon->prepare($user_association_record_check_sql);
	$user_association_record_check_q->bindValue(":sm_memb_id",$sm_memb_id_input);
	$user_association_record_check_q->bindValue(":valid_to_date",$valid_to_date_input);
	$user_association_record_check_q->bindValue(":is_active_status",$is_active_status_input);
	$user_association_record_check_q->execute(); 
	
	if($user_association_record_check_q->rowCount() > 0) {
		$user_association_record_check_q_result = $user_association_record_check_q->fetch();
	     return $user_association_record_check_q_result;
	
	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
	
}

function update_user_classification_association_status_based_on_sm_site_member_classification_association_id($sm_site_member_classification_association_id_input, $is_active_status_input, $valid_to_date_input, $sm_site_member_classification_detail_id_input) {
	
	global $dbcon;
	
	$user_classification_association_details_update_sql = "UPDATE `sm_site_member_classification_associations` SET `sm_site_member_classification_detail_id`=:sm_site_member_classification_detail_id, `is_active_status`=:is_active_status, `valid_to_date`=:valid_to_date WHERE `sm_site_member_classification_association_id`=:sm_site_member_classification_association_id";	
	$user_classification_association_details_update_query = $dbcon->prepare($user_classification_association_details_update_sql);		
	$user_classification_association_details_update_query->bindValue(":sm_site_member_classification_detail_id",$sm_site_member_classification_detail_id_input);
	$user_classification_association_details_update_query->bindValue(":valid_to_date",$valid_to_date_input);
	$user_classification_association_details_update_query->bindValue(":is_active_status",$is_active_status_input);
	$user_classification_association_details_update_query->bindValue(":sm_site_member_classification_association_id",$sm_site_member_classification_association_id_input);
	
	if ($user_classification_association_details_update_query->execute()) {
		return true;
		
	}//close of if ($user_classification_association_details_update_query->execute()) {
	
	return false;
}
function ea_check_action_specific_user_privileges_different_user_levels_reset_password_feature($jwt_token_user_type_input, $jwt_token_user_company_id_input, $jwt_token_user_id_input, $action_subject_user_id_input, $jwt_user_privilege_list_input, $action_subject_user_company_id_input, $page_filename_input) {
	
	global $dbcon;
	
	$next_step = "";
	
	//Do Get the User Classification Details, based on User ID			
	//$page_api_user_classification_association_details_result = ea_get_page_api_user_classification_association_details($page_filename_input);
	$page_api_user_classification_association_details_result = ea_get_page_api_user_classification_association_details_based_on_upl_input($page_filename_input, $jwt_user_privilege_list_input);
	
	
	$platform_specific_subject_classifications_array = array();
	$company_specific_subject_classifications_array = array();
	$self_action_allowance_status_array = array();
	
	foreach($page_api_user_classification_association_details_result as $page_api_user_classification_association_details_result_row) {
		$page_api_sm_site_user_classification_association_id = $page_api_user_classification_association_details_result_row["page_api_sm_site_user_classification_association_id"];
		$sm_site_member_classification_detail_id = $page_api_user_classification_association_details_result_row["sm_site_member_classification_detail_id"];
		$platform_specific_subject_classifications = $page_api_user_classification_association_details_result_row["platform_specific_subject_classifications"];
		$company_specific_subject_classifications = $page_api_user_classification_association_details_result_row["company_specific_subject_classifications"];
		$self_action_allowance_status = $page_api_user_classification_association_details_result_row["self_action_allowance_status"];
		
		$self_action_allowance_status_array[] = $self_action_allowance_status;
				
		//Explode Platform specific subject user classifications
		$platform_specific_subject_classifications_exploded = explode(",", $platform_specific_subject_classifications);
		
		//$platform_specific_subject_classifications_array[] = $platform_specific_subject_classifications_exploded;
		
		//Explode Company specific subject user classifications
		$company_specific_subject_classifications_exploded = explode(",", $company_specific_subject_classifications);
		
		//$company_specific_subject_classifications_array[] = $company_specific_subject_classifications_exploded;
		//array_push($company_specific_subject_classifications_array,$company_specific_subject_classifications_exploded);
		
		/* 
		//Do Collect User Privileges List, from the Database, based on Page Filename
		$platform_specific_subject_classifications_array[] = $platform_specific_subject_classifications;
		 */
	}//close of foreach($page_api_user_classification_association_details_result as $page_api_user_classification_association_details_result_row) {
	
	//$platform_specific_subject_classifications_array = array_unique($platform_specific_subject_classifications_array);
	//echo "<pre>";
	//print_r($platform_specific_subject_classifications_array);
	//$company_specific_subject_classifications_array = array_unique($company_specific_subject_classifications_array);
	//$self_action_allowance_status_array = array_unique($self_action_allowance_status_array);
	
	$action_subject_user_classification_details_result = ea_get_user_classification_details($action_subject_user_id_input);
	
	$user_classification_name_association_cdi_array = array();
	foreach($action_subject_user_classification_details_result as $action_subject_user_classification_details_result_row) {
		$sm_site_member_classification_detail_id = $action_subject_user_classification_details_result_row["sm_site_member_classification_detail_id"];
		$sm_user_role = $action_subject_user_classification_details_result_row["sm_user_role"];
		$user_privilege_summary = $action_subject_user_classification_details_result_row["user_privilege_summary"];
		
		$user_classification_name_association_cdi_array[] = $sm_site_member_classification_detail_id;
		
	}//close of foreach($action_subject_user_classification_details_result as $action_subject_user_classification_details_result_row) {
	
	//print_r($user_classification_name_association_cdi_array);
	
	//print_r($company_specific_subject_classifications_exploded);
	//print_r($user_classification_name_association_cdi_array);
	//echo "action_subject_user_company_id_input: " . $action_subject_user_company_id_input . "<br>";
	//var_dump($action_subject_user_company_id_input);
	//echo "jwt_token_user_company_id_input: " . $jwt_token_user_company_id_input . "<br>";
	//echo "<pre> self action allowance status: ";
	//print_r($self_action_allowance_status_array);
	
	if ($jwt_token_user_type_input == "admin") {
		
		//Do Check, if at least one entry of the JWT Auth Token based User Privileges, matches with User Privileges from the Database Source
		if (((count(array_intersect($platform_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) > 0) || (count(array_intersect($company_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) > 0)) && ($jwt_token_user_company_id_input == "")) {		
			
			if ((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) {
				
				//echo "matched self check condition (platform)";
				//User of the Admin User Group, who is attempting to do this Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
				$next_step = "PROCEED-TO-NEXT-STEP";
				
			} else if ((in_array("0", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) {
				
				//echo "does not match self check condition";
				$next_step = "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED";
				
			} else if ((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input != $action_subject_user_id_input)) {
				
				//echo "matched different user condition (platform)";
				//User of the Admin User Group, who is attempting to do this Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
				$next_step = "PROCEED-TO-NEXT-STEP";
				
			} else if ((in_array("0", $self_action_allowance_status_array)) && ($jwt_token_user_id_input != $action_subject_user_id_input)) {
				
				//echo "matched different user condition (platform)";
				//User of the Admin User Group, who is attempting to do this Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
				$next_step = "PROCEED-TO-NEXT-STEP";
				
			}//close of else if of if (((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) || ((!in_array("1", $self_action_allowance_status_array)))) {
				
			
		} else if (((count(array_intersect($platform_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) == 0) && (count(array_intersect($company_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) == 0)) && (is_null($action_subject_user_company_id_input))) {
			
			//User of the Admin User Group, who is attempting to do this Activity, belongs to a Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...) and the Company ID of the Action Subject is EMPTY. This means action subject is among the Admin Users in Platform scope.
				
			$next_step = "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER";
			
		} else if (((count(array_intersect($platform_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) == 0) && (count(array_intersect($company_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) > 0)) && ($jwt_token_user_company_id_input != "") && ($action_subject_user_company_id_input == $jwt_token_user_company_id_input)) {
			
			if ((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) {
				
				//echo "matched self check condition (company)";
				//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
				
				$next_step = "PROCEED-TO-NEXT-STEP";
				
			} else if ((in_array("0", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) {
				
				//echo "does not match self check condition";
				$next_step = "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED";
				
			} else if ((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input != $action_subject_user_id_input)) {
				
				//echo "matched different user condition (company)";
				//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
				
				$next_step = "PROCEED-TO-NEXT-STEP";
				
			} else if ((in_array("0", $self_action_allowance_status_array)) && ($jwt_token_user_id_input != $action_subject_user_id_input)) {
				
				//echo "matched different user condition (company)";
				//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
				
				$next_step = "PROCEED-TO-NEXT-STEP";
				
			}//close of else if of if (((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) || ((!in_array("1", $self_action_allowance_status_array)))) {
			
			
		} else {
			
			//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
			
			$next_step = "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID";
				
			
		}//close of else of if ((in_array("Super Administrator", $jwt_token_user_privileges_list_array_input)) || (in_array("Site Administrator", $jwt_token_user_privileges_list_array_input))) {
		
		
		
	} else if ($jwt_token_user_type_input == "member") {
		
		//Do Check, if at least one entry of the JWT Auth Token based User Privileges, matches with User Privileges from the Database Source
		if (((count(array_intersect($platform_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) == 0) && ((count(array_intersect($company_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) > 0) || (count(array_intersect($company_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) == 0))) && (is_null($action_subject_user_company_id_input))) {
			
			//User of the Admin User Group, who is attempting to do this Activity, belongs to a Company. The scope includes Company Administrator, Members of Company specific Admin User Teams and the Company ID of the Action Subject is EMPTY. This means action subject is among the Admin Users in Platform scope.
				
			$next_step = "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER";
			
		} else if (((count(array_intersect($platform_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) == 0) && (count(array_intersect($company_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) > 0)) && ($jwt_token_user_company_id_input != "") && ($action_subject_user_company_id_input == $jwt_token_user_company_id_input)) {
			
			if ((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) {
				
				//echo "matched self check condition (company)";
				//User of the Member User Type, who is attempting to do this Activity, belongs to the Same Company and is performing on Own Account. This applies to Candidates.
				
				$next_step = "PROCEED-TO-NEXT-STEP";
				
			} else if ((in_array("0", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) {
				
				//echo "does not match self check condition";
				$next_step = "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED";
				
			} else if ((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input != $action_subject_user_id_input)) {
				
				//echo "matched different user condition (company)";
				//User of the Member User Type, who is attempting to do this Activity, belongs to the Same Company and is performing on a Different User's Account. This applies to Candidates.
				
				$next_step = "DONOT-PROCEED-TO-NEXT-STEP-AS-ACTION-SUBJECT-IS-A-DIFFERENT-USER";
				
			} else if ((in_array("0", $self_action_allowance_status_array)) && ($jwt_token_user_id_input != $action_subject_user_id_input)) {
				
				//echo "matched different user condition (company)";
				//User of the Member User Type, who is attempting to do this Activity, belongs to the Same Company and is performing on a Different User's Account. This applies to Candidates.
				
				$next_step = "DONOT-PROCEED-TO-NEXT-STEP-AS-ACTION-SUBJECT-IS-A-DIFFERENT-USER";
				
			}//close of else if of if (((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) || ((!in_array("1", $self_action_allowance_status_array)))) {
			
			
		} else {
			
			//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
			
			$next_step = "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID";
				
			
		}//close of else of if ((in_array("Super Administrator", $jwt_token_user_privileges_list_array_input)) || (in_array("Site Administrator", $jwt_token_user_privileges_list_array_input))) {
		
		
		
	} else {
		
		$next_step = "DONOT-PROCEED-TO-NEXT-STEP-INSUFFICIENT-PERMISSIONS";
		
	}//close of else of if ($jwt_token_user_type_input == "admin") {
	
	return $next_step;
	
}

exit;
?>


