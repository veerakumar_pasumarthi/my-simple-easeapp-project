<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
				$jsl_info_id_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_info_id']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_info_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
	
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {


				
				if ($jsl_info_id_input == "") {
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-jsl-id";
					$response['status_description'] = "missing jsl id .";
					
					$eventLog->log("Please provide Jsl Id.");
					
					
				} else if ($company_id_input == "") {
			
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "missing Company Id.";
					
					$eventLog->log("Please provide Company Id.");
					
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else {
					$job_rel_screening_level_add_next_step = "";
							if ($ea_extracted_jwt_token_user_type == "admin") {
								
								if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
									$job_rel_screening_level_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("before company id condition");
								} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
									
									$job_rel_screening_level_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("after add condition");
								} else {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "insufficient-permissions-different-company-user-id";
									$response['status_description'] = "Insufficient Set of Permissions";
									
									//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
									header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
									$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
									
								}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								
								
								
							} else {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
								$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
							
							}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
								
					
						    if ($job_rel_screening_level_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
								/* $job_pool_specific_existence_check_result = job_pool_specific_existence_check($ea_extracted_jwt_token_sub,$job_id_input);
								
								if(count($job_pool_specific_existence_check_result) > 0) { */
								
									try {
							
											$jsl_specific_candidates_list_result = jsl_specific_candidates_list($jsl_info_id_input,$company_id_input);
								
											if(count($jsl_specific_candidates_list_result) > 0) {
												
												$response['data'] = $jsl_specific_candidates_list_result;
												$response['status'] = "jsl-specific-candidates-list-fetched-successfully";
												$response['status_description'] = "JSL specific candidates list successfully fetched";
												
												$eventLog->log("JSL specific candidates list successfully fetched");
													
											} else {
												
												$response['data'] = array();
												$response['status'] = "no-candidates-scheduled";
												$response['status_description'] = "No candidates scheduled for this screening level";
												
												$eventLog->log("No candidates scheduled for this screening level");
												
											}	
										
									} catch (Exception $e) {
										
										$response['data'] = array();
										$response['status'] = "jsl-specific-candidates-list-fetching-error";
										$response['status_description'] = "Error occurred when fetching JSL specific candidates list.";
											
										$eventLog->log("job-related-screening-level-insertion-error: Error occurred when adding the job related screening level.");
											
									}
									
								/* } else {
									
									$response['data'] = array();
									$response['status'] = "insufficient-permissions";
									$response['status_description'] = "Insufficient Set of Permissions";
									
									//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
									header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
					
									$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
									
								} */
							}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
							
						}
			//}//close of else of if ($email_id_input == "") {
					
		}//close of //close of if ($ea_is_user_page_access_authorized) {
					
				
	} else {
				
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "invalid-auth-token-submitted";
				$response['status_description'] = "Invalid Authentication";
				
				//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
				header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
				
				$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
				
			}//close of else of if ($ea_auth_token_validation_status) {
		
		}//close of if ($ea_maintenance_mode == false) {
		
} else {

		//Construct Content, that will be sent in Response body, of the REST Web Service
		$response['data'] = array();
		$response['status'] = "invalid-input";
		$response['status_description'] = "Invalid Input, Please check and provide all information.";
		
		//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
		
		$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
				
		
		}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




	//Check if Maintenance Mode is Turned On
	if ($ea_maintenance_mode) {	
		
		//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
		//header('Maintenance-Progress: true', false);
		//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
		$response['data'] = array();
		$response['status'] = "application-maintenance-in-progress";
		$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
		
		$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
		
	} else {	

		//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
		header('Content-Type: application/json');
		echo json_encode($response,JSON_PRETTY_PRINT);
		
	}//close of else of if ($ea_maintenance_mode){
		

	
function jsl_specific_candidates_list($jsl_info_id_input,$company_id_input) {
	global $dbcon,$eventLog;
	
	$constructed_array = array();

    $jsl_specific_candidates_list_sql = "SELECT DISTINCT sm_memb_id,sm_salutation,sm_firstname,sm_middlename,sm_lastname,sm_email,sm.company_id FROM jsl_applicant_evaluation_info jaei JOIN site_members sm ON jaei.job_applicant_sm_memb_id = sm.sm_memb_id WHERE jaei.company_id = :company_id AND jaei.jsl_info_id = :jsl_info_id";
	$jsl_specific_candidates_list_query = $dbcon->prepare($jsl_specific_candidates_list_sql);
	$jsl_specific_candidates_list_query->bindValue(":company_id",$company_id_input);
	$jsl_specific_candidates_list_query->bindValue(":jsl_info_id",$jsl_info_id_input);
	$jsl_specific_candidates_list_query->execute(); 
	
	if($jsl_specific_candidates_list_query->rowCount() > 0) {
		$jsl_specific_candidates_list_query_result = $jsl_specific_candidates_list_query->fetchAll();
		
		foreach($jsl_specific_candidates_list_query_result as $jsl_specific_candidates_list_query_result_row) {
			
			$temp = array();		
			$temp["sm_memb_id"] = $jsl_specific_candidates_list_query_result_row["sm_memb_id"];
			$temp["sm_salutation"] = $jsl_specific_candidates_list_query_result_row["sm_salutation"];
			$temp["sm_firstname"] = $jsl_specific_candidates_list_query_result_row["sm_firstname"];
			$temp["sm_middlename"] = $jsl_specific_candidates_list_query_result_row["sm_middlename"];
			$temp["sm_lastname"] = $jsl_specific_candidates_list_query_result_row["sm_lastname"];
			$temp["sm_email"] = $jsl_specific_candidates_list_query_result_row["sm_email"];
			$temp["sm_name"] = $temp["sm_firstname"] ." ".$temp["sm_middlename"] ." ".$temp["sm_lastname"];
			$temp["company_id"] = $jsl_specific_candidates_list_query_result_row["company_id"];
			
			$constructed_array[] = $temp;
		}//close of foreach($candidate_education_qualification_records_list_select_query_result as $candidate_education_qualification_records_list_select_query_result_row) {
			
		
		return $constructed_array;
	
	}//close of if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
	return $constructed_array;
}

exit;
?>