<?php 
defined('START') or die; 
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */
$eventLogFileName = $route_filename . "-log";  
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "7"))
	{
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) 
	{
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {  
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					if (isset($ea_received_rest_ws_raw_array_input['recruiter_id'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['recruiter_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['company_client_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					if (isset($ea_received_rest_ws_raw_array_input['pdf_type'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['pdf_type'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					if (isset($ea_received_rest_ws_raw_array_input['pdf_type_data'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['pdf_type_data'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					if (isset($ea_received_rest_ws_raw_array_input['from_date_data'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['from_date_data'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if(isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				//  to filter the raw input by using trim , isset() ,FILTER_SANITIZE methods & terminal operator
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');

				$recruiter_id_input = trim(isset($ea_received_rest_ws_raw_array_input['recruiter_id']) ? filter_var($ea_received_rest_ws_raw_array_input['recruiter_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$company_client_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_client_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_client_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$pdf_type_input = trim(isset($ea_received_rest_ws_raw_array_input['pdf_type']) ? filter_var($ea_received_rest_ws_raw_array_input['pdf_type'], FILTER_SANITIZE_STRING) : '');
				
				$pdf_type_data_input = trim(isset($ea_received_rest_ws_raw_array_input['pdf_type_data']) ? filter_var($ea_received_rest_ws_raw_array_input['pdf_type_data'], FILTER_SANITIZE_STRING) : '');
				
				$from_date_data_input = trim(isset($ea_received_rest_ws_raw_array_input['from_date_data']) ? filter_var($ea_received_rest_ws_raw_array_input['from_date_data'], FILTER_SANITIZE_STRING) : '');
				
		       if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4))
					{
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                
				
				//Check if all inputs are received correctly from the REST Web Service
				//misiing  company id scenario
				if ($company_id_input == "") {
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array(); 
					$response['status'] = "missing-company-id";
					$response['status_description'] = "missing company id";
					$eventLog->log("missing_company_id: Please provide a valid company id.");
					
				} else if ($recruiter_id_input == "") {
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array(); 
					$response['status'] = "missing-recruiter-id";
					$response['status_description'] = "missing recruiter id";
					$eventLog->log("missing_recruiter_id: Please provide a valid recruiter id.");
					
				}else if (($pdf_type_input !="")&&($pdf_type_data_input == "")) {
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array(); 
					$response['status'] = "missing-pdf-type-data";
					$response['status_description'] = "missing pdf type data";
					$eventLog->log("missing_recruiter_id: Please provide a valid pdf type data.");
					
				} else if ($ip_address_input == "") {
					//missing client company name scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "missing ip address";
					
					$eventLog->log("missing-ip-address: Please provide a valid ip address.");
					
				}  else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//  else {
						
						//Event Time, as per Indian Standard Time
						$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
					//	$event_expiry_datetime_epoch = $current_epoch+$password_resetting_link_expiry_period;	
						//$valid_from_date_input = '2018-11-10 16:18:20';
						
						
						
							//try {
								
								//Check if the User, whose sm_memb_id is received from "sub" of JWT Auth Token exists
						
					
				if ($company_client_id_input != "") {
							
					$get_job_id  =get_job_id_from_job_assignees_table($company_id_input,$recruiter_id_input,$company_client_id_input,$pdf_type_input,$pdf_type_data_input,$from_date_data_input);
					
					


					if(count($get_job_id)>0) {

						
                         $response['data'] = $get_job_id;
						$response['status'] = "recruiter-list-found-succeesfully";
						$response['status_description'] = "recruiter list found successfully.";


					}else {
						$response['data'] = array();
						$response['status'] = "job-id-not-found";
						$response['status_description'] = "job id not found.";
					}

														
					
					}	else {	

							$get_job_id_without_client_id = get_job_id_without_client_id($company_id_input,$recruiter_id_input,$pdf_type_input,$pdf_type_data_input,$from_date_data_input);
							
							

                      if(count($get_job_id_without_client_id)>0) {

						
                          $response['data'] = $get_job_id_without_client_id;
						$response['status'] = "recruiter-list-found-succeesfully";
						$response['status_description'] = "recruiter list found successfully.";

					}else {
						$response['data'] = array();
						$response['status'] = "job-id-not-found";
						$response['status_description'] = "job id not found.";
					}
                            
							
								} //close of else of if (count($ea_token_based_sub_user_id_details) > 0) {
								
							/*} catch (Exception $e) {
								
								//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
								$response['data'] = array();
								$response['status'] = "job-list-fetching-error";
								$response['status_description'] = "job list fetching error.";
								
								$eventLog->log("company-api-key-insertion-error: There is an error, when adding the client Company in the Database.");
							}*/
						//}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							
						
					//}
				}//close of else of if ($email_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
    
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
			
	
	}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "7")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}
  
function get_job_id_from_job_assignees_table($company_id_input,$recruiter_id_input,$company_client_id_input,$pdf_type_input,$pdf_type_data_input,$from_date_data_input) {
	global $dbcon;
	$constructed_array = array();
	$assignment_status = "1";

	
	$candidate_preferred_job_location_record_get_sql = "SELECT * FROM `job_management_assignees` WHERE `company_id` = :company_id AND `assigned_user_sm_memb_id` = :assigned_user_sm_memb_id AND `company_client_id` = :company_client_id AND `assignment_status` = :assignment_status";

	$candidate_preferred_job_location_record_duplicate_check_q = $dbcon->prepare($candidate_preferred_job_location_record_get_sql);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":company_id",$company_id_input);
    
    $candidate_preferred_job_location_record_duplicate_check_q->bindValue(":assigned_user_sm_memb_id",$recruiter_id_input);

    $candidate_preferred_job_location_record_duplicate_check_q->bindValue(":company_client_id",$company_client_id_input);

    $candidate_preferred_job_location_record_duplicate_check_q->bindValue(":assignment_status",$assignment_status);

	$candidate_preferred_job_location_record_duplicate_check_q->execute();
  
	if($candidate_preferred_job_location_record_duplicate_check_q->rowCount() > 0) {
		$candidate_preferred_job_location_record_duplicate_check_q_result = $candidate_preferred_job_location_record_duplicate_check_q->fetchAll();
		
		 foreach ($candidate_preferred_job_location_record_duplicate_check_q_result as $candidate_preferred_job_location_record_duplicate_check_q_result_row) {

			//$temp_array=array();
			//$company_client_ids=array();
			$company_client_id = $candidate_preferred_job_location_record_duplicate_check_q_result_row["company_client_id"];
            
			 
			 $company_client_ids[] = $company_client_id;
			 
			}
		}
		else {
			$company_client_ids=array();
		}

	 $candidate_preferred_job_location_record_get_sql = "SELECT * FROM `company_clients` WHERE `company_client_id` IN ('".implode("','",$company_client_ids)."')";

	$candidate_preferred_job_location_record_duplicate_check_q = $dbcon->prepare($candidate_preferred_job_location_record_get_sql);

	$candidate_preferred_job_location_record_duplicate_check_q->execute();
	
  
	if($candidate_preferred_job_location_record_duplicate_check_q->rowCount() > 0) {
		$candidate_preferred_job_location_record_duplicate_check_q_result = $candidate_preferred_job_location_record_duplicate_check_q->fetchAll();
		
		foreach ($candidate_preferred_job_location_record_duplicate_check_q_result as $candidate_preferred_job_location_record_duplicate_check_q_result_row) {
        
			$temp_array=array();
			
			$temp_array["client_company_name"] = $candidate_preferred_job_location_record_duplicate_check_q_result_row["client_company_name"];
       if($pdf_type_input == "Week"){

		$job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `company_client_id` =:company_client_id AND `added_by_sm_memb_id` =:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m-%d') BETWEEN CAST(:added_datetime_one AS DATE) AND DATE_ADD(CAST(:added_datetime_two AS DATE), INTERVAL 7 DAY)";

	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":company_client_id",$company_client_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_one",$pdf_type_data_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_two",$pdf_type_data_input);		
	$job_rel_screening_levels_list_info_query->execute(); 
	$temp_array['no_of_submissions'] = $job_rel_screening_levels_list_info_query->rowCount();

	//$jsl_classification_detail_name = "Interview";
	
	$job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `jsl_classification_detail_name` =:jsl_classification_detail_name AND `added_by_sm_memb_id` =:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m-%d') BETWEEN CAST(:added_datetime_one AS DATE) AND DATE_ADD(CAST(:added_datetime_two AS DATE), INTERVAL 7 DAY)";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":jsl_classification_detail_name","Interview");
	$job_rel_screening_levels_list_info_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_one",$pdf_type_data_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_two",$pdf_type_data_input);		
	$job_rel_screening_levels_list_info_query->execute(); 
	$temp_array["no_of_interviews"]= $job_rel_screening_levels_list_info_query->rowCount();

	$job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `event_status` =:event_status AND `added_by_sm_memb_id` =:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m-%d') BETWEEN CAST(:added_datetime_one AS DATE) AND DATE_ADD(CAST(:added_datetime_two AS DATE), INTERVAL 7 DAY)";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":event_status",6);
	$job_rel_screening_levels_list_info_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_one",$pdf_type_data_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_two",$pdf_type_data_input);		
	$job_rel_screening_levels_list_info_query->execute();
	$temp_array["no_of_rejected"]=$job_rel_screening_levels_list_info_query->rowCount(); 

   $job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `event_status` =:event_status AND `added_by_sm_memb_id` =:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m-%d') BETWEEN CAST(:added_datetime_one AS DATE) AND DATE_ADD(CAST(:added_datetime_two AS DATE), INTERVAL 7 DAY)";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":event_status",7);
	$job_rel_screening_levels_list_info_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_one",$pdf_type_data_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_two",$pdf_type_data_input);		
	$job_rel_screening_levels_list_info_query->execute();
	$temp_array["no_of_offer_letters"]=$job_rel_screening_levels_list_info_query->rowCount(); 
	$temp_array['From_range'] = $from_date_data_input;
$temp_array['To_range'] = $pdf_type_data_input;
} else if($pdf_type_input == "Month"){
		
     $rest_client_interviews_check_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `company_client_id` = :company_client_id AND `jsl_classification_detail_name` =:jsl_classification_detail_name AND `added_by_sm_memb_id`=:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m') = :added_datetime_one";
	$rest_client_interviews_check_sql_query = $dbcon->prepare($rest_client_interviews_check_sql);
	$rest_client_interviews_check_sql_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_interviews_check_sql_query->bindValue(":jsl_classification_detail_name","Interview");
	$rest_client_interviews_check_sql_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	
	$rest_client_interviews_check_sql_query->bindValue(":added_datetime_one",$pdf_type_data_input);

	$rest_client_interviews_check_sql_query->execute();
	$temp_array['no_of_interviews'] = $rest_client_interviews_check_sql_query->rowCount();

	$rest_client_submission_check_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `company_client_id` = :company_client_id AND `added_by_sm_memb_id`=:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m') = :added_datetime_one";
	$rest_client_submission_check_sql_query = $dbcon->prepare($rest_client_submission_check_sql);
	$rest_client_submission_check_sql_query->bindValue(":company_client_id",$company_client_id_input);
	
	$rest_client_submission_check_sql_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	
	$rest_client_submission_check_sql_query->bindValue(":added_datetime_one",$pdf_type_data_input);
	$rest_client_submission_check_sql_query->execute();
	$temp_array['no_of_submissions'] = $rest_client_submission_check_sql_query->rowCount();

	$rest_client_rejected_check_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `company_client_id` = :company_client_id AND `event_status` =:event_status AND `added_by_sm_memb_id`=:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m') = :added_datetime_one";
	$rest_client_rejected_check_sql_query = $dbcon->prepare($rest_client_rejected_check_sql);
	$rest_client_rejected_check_sql_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_rejected_check_sql_query->bindValue(":event_status",6);
	$rest_client_rejected_check_sql_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	
	$rest_client_rejected_check_sql_query->bindValue(":added_datetime_one",$pdf_type_data_input);
	$rest_client_rejected_check_sql_query->execute();
	$temp_array['no_of_rejected'] = $rest_client_rejected_check_sql_query->rowCount();

	$rest_client_offer_check_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `company_client_id` = :company_client_id AND `event_status` =:event_status AND `added_by_sm_memb_id`=:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m') = :added_datetime_one";
	$rest_client_offer_check_sql_query = $dbcon->prepare($rest_client_offer_check_sql);
	$rest_client_offer_check_sql_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_offer_check_sql_query->bindValue(":event_status",7);
	$rest_client_offer_check_sql_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	$rest_client_offer_check_sql_query->bindValue(":added_datetime_one",$pdf_type_data_input);
	//$rest_client_offer_check_sql_query->bindValue(":added_datetime_two",$to_input);
	$rest_client_offer_check_sql_query->execute();
	$temp_array['no_of_offer_letters'] = $rest_client_offer_check_sql_query->rowCount();
	$temp_array['From_range'] = $pdf_type_data_input."-01";
		    $temp_array['To_range'] = todate($pdf_type_data_input);	
	} else {
	$job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `company_client_id` =:company_client_id AND `added_by_sm_memb_id` =:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m-%d') BETWEEN :added_datetime_one AND :added_datetime_two";

	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":company_client_id",$company_client_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_one",$from_date_data_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_two",$pdf_type_data_input);		
	$job_rel_screening_levels_list_info_query->execute(); 
	$temp_array['no_of_submissions'] = $job_rel_screening_levels_list_info_query->rowCount();

	//$jsl_classification_detail_name = "Interview";
	
	$job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `jsl_classification_detail_name` =:jsl_classification_detail_name AND `added_by_sm_memb_id` =:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m-%d') BETWEEN :added_datetime_one AND :added_datetime_two";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":jsl_classification_detail_name","Interview");
	$job_rel_screening_levels_list_info_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_one",$from_date_data_input);
	
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_two",$pdf_type_data_input);		
	$job_rel_screening_levels_list_info_query->execute(); 
	$temp_array["no_of_interviews"]= $job_rel_screening_levels_list_info_query->rowCount();

	$job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `event_status` =:event_status AND `added_by_sm_memb_id` =:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m-%d') BETWEEN :added_datetime_one AND :added_datetime_two";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":event_status",6);
	$job_rel_screening_levels_list_info_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_one",$from_date_data_input);	
	
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_two",$pdf_type_data_input);	
	$job_rel_screening_levels_list_info_query->execute();
	$temp_array["no_of_rejected"]=$job_rel_screening_levels_list_info_query->rowCount(); 

   $job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `event_status` =:event_status AND `added_by_sm_memb_id` =:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m-%d') BETWEEN :added_datetime_one AND :added_datetime_two";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":event_status",7);
	$job_rel_screening_levels_list_info_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_one",$from_date_data_input);	
	
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_two",$pdf_type_data_input);	
	$job_rel_screening_levels_list_info_query->execute();
	$temp_array["no_of_offer_letters"]=$job_rel_screening_levels_list_info_query->rowCount();
	$temp_array['From_range'] = $from_date_data_input;
$temp_array['To_range'] = $pdf_type_data_input;
}


	$constructed_array[]=$temp_array;

}
return $constructed_array;
}
return $constructed_array;
	


}

function get_job_id_without_client_id($company_id_input,$recruiter_id_input,$pdf_type_input,$pdf_type_data_input,$from_date_data_input) {

	global $dbcon;
	$constructed_array = array();
	$assignment_status = "1";
	
	$candidate_preferred_job_location_record_get_sql = "SELECT * FROM `job_management_assignees` WHERE `company_id` = :company_id AND `assigned_user_sm_memb_id` = :assigned_user_sm_memb_id AND `assignment_status` = :assignment_status";

	$candidate_preferred_job_location_record_duplicate_check_q = $dbcon->prepare($candidate_preferred_job_location_record_get_sql);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":company_id",$company_id_input);
    
    $candidate_preferred_job_location_record_duplicate_check_q->bindValue(":assigned_user_sm_memb_id",$recruiter_id_input);

    
    $candidate_preferred_job_location_record_duplicate_check_q->bindValue(":assignment_status",$assignment_status);

	$candidate_preferred_job_location_record_duplicate_check_q->execute();
  
	if($candidate_preferred_job_location_record_duplicate_check_q->rowCount() > 0) {
		$candidate_preferred_job_location_record_duplicate_check_q_result = $candidate_preferred_job_location_record_duplicate_check_q->fetchAll();
		
		 foreach ($candidate_preferred_job_location_record_duplicate_check_q_result as $candidate_preferred_job_location_record_duplicate_check_q_result_row) {

			//$temp_array=array();
			$company_client_id= $candidate_preferred_job_location_record_duplicate_check_q_result_row["company_client_id"];
			//$company_client_id_input = $temp_array["company_client_id"];
			$company_client_ids[] = $company_client_id;
   			//$constructed_array=$temp_array;
			//echo "hi2";
			//var_dump($temp_array);
		}

	}
	else {
		$company_client_ids = array();
	}
			 $candidate_preferred_job_location_record_get_sql = "SELECT * FROM `company_clients` WHERE `company_client_id` IN ('".implode("','",$company_client_ids)."')";

	$candidate_preferred_job_location_record_duplicate_check_q = $dbcon->prepare($candidate_preferred_job_location_record_get_sql);

	$candidate_preferred_job_location_record_duplicate_check_q->execute();
  
	if($candidate_preferred_job_location_record_duplicate_check_q->rowCount() > 0) {
		$candidate_preferred_job_location_record_duplicate_check_q_result = $candidate_preferred_job_location_record_duplicate_check_q->fetchAll();
		
		 foreach ($candidate_preferred_job_location_record_duplicate_check_q_result as $candidate_preferred_job_location_record_duplicate_check_q_result_row) {

			$temp_array=array();
			$temp_array["client_company_name"] = $candidate_preferred_job_location_record_duplicate_check_q_result_row["client_company_name"];
			//$temp_array["company_client_id"] = $candidate_preferred_job_location_record_duplicate_check_q_result_row["company_client_id"];
			$company_client_id_input= $candidate_preferred_job_location_record_duplicate_check_q_result_row["company_client_id"];

		if($pdf_type_input == "Week"){

		$job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `company_client_id` =:company_client_id AND `added_by_sm_memb_id` =:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m-%d') BETWEEN CAST(:added_datetime_one AS DATE) AND DATE_ADD(CAST(:added_datetime_two AS DATE), INTERVAL 7 DAY)";

	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":company_client_id",$company_client_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_one",$pdf_type_data_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_two",$pdf_type_data_input);		
	$job_rel_screening_levels_list_info_query->execute(); 
	$temp_array['no_of_submissions'] = $job_rel_screening_levels_list_info_query->rowCount();

	//$jsl_classification_detail_name = "Interview";
	
	$job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `jsl_classification_detail_name` =:jsl_classification_detail_name AND `added_by_sm_memb_id` =:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m-%d') BETWEEN CAST(:added_datetime_one AS DATE) AND DATE_ADD(CAST(:added_datetime_two AS DATE), INTERVAL 7 DAY)";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":jsl_classification_detail_name","Interview");
	$job_rel_screening_levels_list_info_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_one",$pdf_type_data_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_two",$pdf_type_data_input);		
	$job_rel_screening_levels_list_info_query->execute(); 
	$temp_array["no_of_interviews"]= $job_rel_screening_levels_list_info_query->rowCount();

	$job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `event_status` =:event_status AND `added_by_sm_memb_id` =:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m-%d') BETWEEN CAST(:added_datetime_one AS DATE) AND DATE_ADD(CAST(:added_datetime_two AS DATE), INTERVAL 7 DAY)";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":event_status",6);
	$job_rel_screening_levels_list_info_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_one",$pdf_type_data_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_two",$pdf_type_data_input);		
	$job_rel_screening_levels_list_info_query->execute();
	$temp_array["no_of_rejected"]=$job_rel_screening_levels_list_info_query->rowCount(); 

   $job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `event_status` =:event_status AND `added_by_sm_memb_id` =:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m-%d') BETWEEN CAST(:added_datetime_one AS DATE) AND DATE_ADD(CAST(:added_datetime_two AS DATE), INTERVAL 7 DAY)";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":event_status",7);
	$job_rel_screening_levels_list_info_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_one",$pdf_type_data_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_two",$pdf_type_data_input);		
	$job_rel_screening_levels_list_info_query->execute();
	$temp_array["no_of_offer_letters"]=$job_rel_screening_levels_list_info_query->rowCount(); 
	$temp_array['From_range'] = $from_date_data_input;
$temp_array['To_range'] = $pdf_type_data_input;
} else if($pdf_type_input == "Month"){
		
     $rest_client_interviews_check_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `company_client_id` = :company_client_id AND `jsl_classification_detail_name` =:jsl_classification_detail_name AND `added_by_sm_memb_id`=:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m') = :added_datetime_one";
	$rest_client_interviews_check_sql_query = $dbcon->prepare($rest_client_interviews_check_sql);
	$rest_client_interviews_check_sql_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_interviews_check_sql_query->bindValue(":jsl_classification_detail_name","Interview");
	$rest_client_interviews_check_sql_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	
	$rest_client_interviews_check_sql_query->bindValue(":added_datetime_one",$pdf_type_data_input);

	$rest_client_interviews_check_sql_query->execute();
	$temp_array['no_of_interviews'] = $rest_client_interviews_check_sql_query->rowCount();

	$rest_client_submission_check_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `company_client_id` = :company_client_id AND `added_by_sm_memb_id`=:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m') = :added_datetime_one";
	$rest_client_submission_check_sql_query = $dbcon->prepare($rest_client_submission_check_sql);
	$rest_client_submission_check_sql_query->bindValue(":company_client_id",$company_client_id_input);
	
	$rest_client_submission_check_sql_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	
	$rest_client_submission_check_sql_query->bindValue(":added_datetime_one",$pdf_type_data_input);
	$rest_client_submission_check_sql_query->execute();
	$temp_array['no_of_submissions'] = $rest_client_submission_check_sql_query->rowCount();

	$rest_client_rejected_check_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `company_client_id` = :company_client_id AND `event_status` =:event_status AND `added_by_sm_memb_id`=:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m') = :added_datetime_one";
	$rest_client_rejected_check_sql_query = $dbcon->prepare($rest_client_rejected_check_sql);
	$rest_client_rejected_check_sql_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_rejected_check_sql_query->bindValue(":event_status",6);
	$rest_client_rejected_check_sql_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	
	$rest_client_rejected_check_sql_query->bindValue(":added_datetime_one",$pdf_type_data_input);
	$rest_client_rejected_check_sql_query->execute();
	$temp_array['no_of_rejected'] = $rest_client_rejected_check_sql_query->rowCount();

	$rest_client_offer_check_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `company_client_id` = :company_client_id AND `event_status` =:event_status AND `added_by_sm_memb_id`=:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m') = :added_datetime_one";
	$rest_client_offer_check_sql_query = $dbcon->prepare($rest_client_offer_check_sql);
	$rest_client_offer_check_sql_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_offer_check_sql_query->bindValue(":event_status",7);
	$rest_client_offer_check_sql_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	$rest_client_offer_check_sql_query->bindValue(":added_datetime_one",$pdf_type_data_input);
	//$rest_client_offer_check_sql_query->bindValue(":added_datetime_two",$to_input);
	$rest_client_offer_check_sql_query->execute();
	$temp_array['no_of_offer_letters'] = $rest_client_offer_check_sql_query->rowCount();
	$temp_array['From_range'] = $period_type_data_input."-01";
		    $temp_array['To_range'] = todate($period_type_data_input);	
	} else {
	$job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `company_client_id` =:company_client_id AND `added_by_sm_memb_id` =:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m-%d') BETWEEN :added_datetime_one AND :added_datetime_two";

	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":company_client_id",$company_client_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_one",$from_date_data_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_two",$pdf_type_data_input);		
	$job_rel_screening_levels_list_info_query->execute(); 
	$temp_array['no_of_submissions'] = $job_rel_screening_levels_list_info_query->rowCount();

	//$jsl_classification_detail_name = "Interview";
	
	$job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `jsl_classification_detail_name` =:jsl_classification_detail_name AND `added_by_sm_memb_id` =:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m-%d') BETWEEN :added_datetime_one AND :added_datetime_two";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":jsl_classification_detail_name","Interview");
	$job_rel_screening_levels_list_info_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_one",$from_date_data_input);
	
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_two",$pdf_type_data_input);		
	$job_rel_screening_levels_list_info_query->execute(); 
	$temp_array["no_of_interviews"]= $job_rel_screening_levels_list_info_query->rowCount();

	$job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `event_status` =:event_status AND `added_by_sm_memb_id` =:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m-%d') BETWEEN :added_datetime_one AND :added_datetime_two";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":event_status",6);
	$job_rel_screening_levels_list_info_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
		
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_one",$from_date_data_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_two",$pdf_type_data_input);	
	$job_rel_screening_levels_list_info_query->execute();
	$temp_array["no_of_rejected"]=$job_rel_screening_levels_list_info_query->rowCount(); 

   $job_rel_screening_levels_list_info_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `event_status` =:event_status AND `added_by_sm_memb_id` =:added_by_sm_memb_id AND DATE_FORMAT(`added_datetime`, '%Y-%m-%d') BETWEEN :added_datetime_one AND :added_datetime_two";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_rel_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":event_status",7);
	$job_rel_screening_levels_list_info_query->bindValue(":added_by_sm_memb_id",$recruiter_id_input);
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_one",$from_date_data_input);	
	
	$job_rel_screening_levels_list_info_query->bindValue(":added_datetime_two",$pdf_type_data_input);	
	$job_rel_screening_levels_list_info_query->execute();
	$temp_array["no_of_offer_letters"]=$job_rel_screening_levels_list_info_query->rowCount();
	$temp_array['From_range'] = $from_date_data_input;
$temp_array['To_range'] = $pdf_type_data_input;
}



	$constructed_array[]=$temp_array;

}
return $constructed_array;
}
return $constructed_array;
	


}

function todate($period_type_data_input){

	//$string = "MICROSOFT CORP CIK#: 0000789019 (see all company filings)";
$newString = substr($period_type_data_input, 5);
$newYear = substr($period_type_data_input,0,4);
$Year = (int)$newYear ;

if($newString == "01" || $newString == "03" || $newString == "05" || $newString == "07" || $newString == "08" || $newString == "10" || $newString == "12") {

	$date = $period_type_data_input."-31";
} else if($newString == "02" && ($Year % 4 == 0)) {

	$date = $period_type_data_input."-29";
} else if($newString == "02" && ($Year % 4 != 0)) {

	$date = $period_type_data_input."-28";
} else {
	$date = $period_type_data_input."-30";
}
return $date;

}	



exit;
?>