<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * REST API for IP Address
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);

$server_variables_json_encoded = json_encode($_SERVER['SERVER_PROTOCOL']);
$eventLog->log("server variables => " . $server_variables_json_encoded);
$eventLog->log("headers => " . $ea_received_request_headers_json_encoded);
 

				
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {	
		    
			try {
					
					$ip = "";
					
				    if (!empty($_SERVER["HTTP_CLIENT_IP"])) {

                        //check for ip from share internet

                        $ip = $_SERVER["HTTP_CLIENT_IP"];

                    } elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {

                        // Check for the Proxy User

                        $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];

                    } else {

                        $ip = $_SERVER["REMOTE_ADDR"];
                    }
					
					// This will print user's real IP Address

                    // does't matter if user using proxy or not.
					//Check if the IP Address Input is a Valid IPv4 Address
					
					if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						$ip_address = trim($ip);
						$ip_address_array["ipv4"] = $ip_address;
						$ip_address_array["ipv6"] = null;
						
						//$response['data'] = $ip_address;
						$response['data'] = $ip_address_array;
						$response['status'] = "ip-address-fetched-successfully";
						$response['status_description'] = "IP Address Successfully fetched";
						$eventLog->log($ip_address. " - valid IPv4 address");
						
		            } else {
			            $eventLog->log($ip_address. " - not a valid IPv4 address");
			            $ip_address = '';
						
						//$response['data'] = $ip_address;
						$response['data'] = array();
						$response['status'] = "invalid-ip-address";
						$response['status_description'] = "IP Address is Invalid";
						
		            }//close of else of if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) {
					
                   
					
			    } catch (Exception $e) {
								
					$eventLog->log("Unable to to retrieve IP address");	
					$response['data'] = array();
					$response['status'] = "unable-to-retrieve-ip-address";
					$response['status_description'] = "Unable to to retrieve IP address";
					
				}
    }//close of if ($ea_maintenance_mode == false) {


//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, with Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){

exit;
?>