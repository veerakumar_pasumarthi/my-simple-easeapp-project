<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "8")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$page_number_input= trim(isset($ea_received_rest_ws_raw_array_input['page_number']) ? filter_var($ea_received_rest_ws_raw_array_input['page_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				$sm_user_status_input = trim(isset($ea_received_rest_ws_raw_array_input['sm_user_status']) ? filter_var($ea_received_rest_ws_raw_array_input['sm_user_status'], FILTER_SANITIZE_NUMBER_INT) : '');
				$number_of_records_input = trim(isset($ea_received_rest_ws_raw_array_input['number_of_records']) ? filter_var($ea_received_rest_ws_raw_array_input['number_of_records'], FILTER_SANITIZE_NUMBER_INT) : '');
				$sort_field_input= trim(isset($ea_received_rest_ws_raw_array_input['sort_field']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_field'], FILTER_SANITIZE_STRING) : '');
				$sort_order_input = trim(isset($ea_received_rest_ws_raw_array_input['sort_order']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_order'], FILTER_SANITIZE_STRING) : '');
				$search_criteria_input = trim(isset($ea_received_rest_ws_raw_array_input['search_criteria']) ? filter_var($ea_received_rest_ws_raw_array_input['search_criteria'], FILTER_SANITIZE_STRING) : '');
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {

                if (($sm_user_status_input != "0") && ($sm_user_status_input != "1") && ($sm_user_status_input != "")) {
					//Invalid company Status scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-user-status";
					$response['status_description'] = "Invalid user Status info submitted, please check and try again.";
					
					$eventLog->log("Please provide a valid user Status info.");
				
				
				}else if (($sort_order_input != "ASC") && ($sort_order_input != "DESC") && ($sort_order_input != "")) {
					//Invalid Sorting Order scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-Sorting-order";
					$response['status_description'] = "Invalid Sorting Order info submitted, please check and try again.";
					
					$eventLog->log("invalid-Sorting-order: Invalid Sorting Order info submitted, please check and try again.");
					$eventLog->log("Please provide all information.");
				
                
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else {
					//all inputs are valid
					
					try{
						
					    if ($sm_user_status_input == "") {
							$sm_user_status_input = null;
							
						}//close of if ($sm_user_status_input == "") {
						
					       $candidate_list_info_result = array();
						   $sm_user_type_input = 'member';
						   
					    if ($ea_extracted_jwt_token_user_company_id == "") {
						    //action taker is of platform scope - super admin / site admin scenario
						    $candidate_list_info_result = get_candidates_list_with_pagination_inputs_list($company_id_input,$sm_user_status_input, $page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input);
							
					
					    } else {
						   //action taker is of company scope - company admin, legal / immigration, internal admin etc scenario
						    $candidate_list_info_result = get_candidates_list_with_pagination_inputs_list($ea_extracted_jwt_token_user_company_id, $sm_user_status_input, $page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input);
							//get_candidates_list_with_pagination_inputs($ea_extracted_jwt_token_user_company_id,$sm_user_status_input, $sm_user_type_input, $page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input);
					       
					    }//close of else of if ($ea_extracted_jwt_token_user_company_id == "") {
					    //$candidate_list_info = get_candidate_list($company_id_input, "member");
						$candidate_list_info_result_count = count($candidate_list_info_result);
						
						$eventLog->log("Count -> " . $candidate_list_info_result_count);
						
					    if ($candidate_list_info_result_count > "0") {
					       $response['data'] = $candidate_list_info_result;
					       $response['status'] = "candidate-list-received";
					       $response['status_description'] = "Candidates List is Successfully Received";
							
					     $candidate_list_info_json_encoded = json_encode($candidate_list_info_result);
						
					       //eventLog->log("Quick User Info -> " . $candidate_list_info_json_encoded); 
					    }else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "active-candidates-doesnot-exist";
							$response['status_description'] = "No Active Candidates list Exist for your company, please check and try again.";
							
					    }		//No Active company list Exist
							
					    $eventLog->log("Companies List -> No Active Candidates Exist for your company, please check and try again."); 	
				    
					}catch (Exception $e){
						$eventLog->log("Exception -> " . html_escaped_output($e->getMessage())); 
						//addLog($logFile, "Exception -> ".$e->getMessage());	
					}//close of  catch (Exception $e){	}//close of else of if ($company_list_result_count > "0") {					
                  
				}//close of else of if ($user_type_input != 'admin') {
					
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function get_candidates_list_with_pagination_inputs_list($company_id_input,$sm_user_status_input, $page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input) {

    global $dbcon, $default_number_of_records_pagination; //app/core/main-config.php
	global $eventLog,$ea_extracted_jwt_token_sub, $current_epoch, $expiring_link_lifetime, $expiring_link_secret_key, $expiring_link_hash_algorithm, $site_hostname_value;
	$constructed_array = array();
	$site_hostname_value = $site_hostname_value = "uploaded-files-live.resumecrab.com";
	$eventLog->log("before is_null concept");
	/* if ($company_id_input == "") {
		$get_candidate_list_sql = "SELECT * FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE ssmca.sm_site_member_classification_detail_id = '1' ORDER BY sm.sm_memb_id ASC";
		//$get_candidate_list_select_query = $dbcon->prepare($get_candidate_list_sql);
	} else {
		$get_candidate_list_sql = "SELECT * FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE ssmca.sm_site_member_classification_detail_id = '1' AND sm.company_id =:company_id ORDER BY sm.sm_memb_id ASC";
		//$get_candidate_list_select_query = $dbcon->prepare($get_candidate_list_sql);
		//$get_candidate_list_select_query->bindValue(":company_id",$company_id_input);
	}//close of else of if ($company_id_input == "") {
 */
	if ($search_criteria_input == "") {
		$search_criteria_input = null;
	}//close of if ($search_criteria_input == "") {


	if (!is_null($search_criteria_input)) {
		//When Search Criteria input is not empty
		$search_criteria_variable = "%" . $search_criteria_input . "%";
		//$search_criteria_in_query = "(`company_name` LIKE '%info%') OR (`company_seo_name` LIKE '%info%') OR (`company_brand_name` LIKE '%info%') OR (`is_active_status` LIKE '%info%')";

		$search_criteria_in_query_with_where_keyword = " WHERE ((sm.sm_memb_id LIKE :sm_memb_id_search_keyword) OR (sm.sm_email LIKE :sm_email_search_keyword) OR (sm.sm_mobile LIKE :sm_mobile_search_keyword) OR (sm.sm_alternate_email LIKE :sm_alternate_email_search_keyword) OR (sm.sm_salutation LIKE :sm_salutation_search_keyword) OR (sm.sm_firstname LIKE :sm_firstname_search_keyword) OR (sm.sm_middlename LIKE :sm_middlename_search_keyword) OR (sm.sm_lastname LIKE :sm_lastname_search_keyword) OR (sm.sm_user_status LIKE :sm_user_status_search_keyword)) ";
		$eventLog->log("after search criteria concept");
		$search_criteria_in_query_without_where_keyword = " AND ((sm.sm_memb_id LIKE :sm_memb_id_search_keyword) OR (sm.sm_email LIKE :sm_email_search_keyword) OR (sm.sm_mobile LIKE :sm_mobile_search_keyword) OR (sm.sm_alternate_email LIKE :sm_alternate_email_search_keyword) OR (sm.sm_salutation LIKE :sm_salutation_search_keyword) OR (sm.sm_firstname LIKE :sm_firstname_search_keyword) OR (sm.sm_middlename LIKE :sm_middlename_search_keyword) OR (sm.sm_lastname LIKE :sm_lastname_search_keyword) OR (sm.sm_user_status LIKE :sm_user_status_search_keyword)) ";
	    $eventLog->log("after search criteria without where concept");
	} else {
		//When Search Criteria input is empty
		$search_criteria_in_query_with_where_keyword = " WHERE ";

		$search_criteria_in_query_without_where_keyword = "";

	}//close of else of if (!is_null($search_criteria_input)) {
	$eventLog->log("before sort order concept");

	if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {

		if (!is_null($sort_field_input)) {
			//When the Sort Field input is not empty
			$eventLog->log("null condition  sort concept");

			if ($sort_field_input == "sm_memb_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_memb_id " . $sort_order_input;

			} else if ($sort_field_input == "sm_email") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_email " . $sort_order_input;

			} else if ($sort_field_input == "sm_mobile") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_mobile " . $sort_order_input;

			} else if ($sort_field_input == "sm_alternate_email") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_alternate_email " . $sort_order_input;

			} else if ($sort_field_input == "sm_salutation") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_salutation " . $sort_order_input;

            } else if ($sort_field_input == "sm_firstname") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_firstname " . $sort_order_input;

			}else if ($sort_field_input == "sm_middlename") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_middlename " . $sort_order_input;

			}else if ($sort_field_input == "sm_lastname") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_lastname " . $sort_order_input;

			} else if ($sort_field_input == "sm_user_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_user_status " . $sort_order_input;

			} else {
				//When Sort Field input, is not among the expected values, use primary key, with provided Sorting Order input
				$sort_details_in_query = " ORDER BY sm.sm_memb_id " . $sort_order_input;


			}
			$eventLog->log("after checking inputs concept");

		} else {
			//When the Sort Field input is empty, primary key has to be taken, with provided Sorting Order input
			$sort_details_in_query = " ORDER BY sm.sm_memb_id " . $sort_order_input;

		}//close of if (!is_null($sort_field_input)) {


	} else {
		//When the Sort Field input is empty, primary key has to be taken, with ASC Order, by default
		$sort_details_in_query = " ORDER BY sm.sm_memb_id ASC ";

	}//close of else of if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {


	$limit_offset_value = 0;

	if (($page_number_input == "") && ($number_of_records_input == "")) {
		//Give All Data
		//No LIMIT Clause in the Query
		$limit_offset_in_query = "";

	} else if (($page_number_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than page_number_input, till the number of records, as defined in the $default_number_of_records_pagination

		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $default_number_of_records_pagination;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$default_number_of_records_pagination;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $default_number_of_records_pagination;
		}//close of else if of if ($page_number_input == "1") {


		$eventLog->log("before page number concept");
	} else if (($page_number_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records
		$limit_offset_in_query = " LIMIT " . $number_of_records_input;

	} else {
		//Give Data, from given Record, till the requested number of records
		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $number_of_records_input;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$number_of_records_input;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $number_of_records_input;
		}//close of else if of if ($page_number_input == "1") {

	}//close of else of if (($page_number_input == "") && ($number_of_records_input == "")) {
	$eventLog->log("before sm_user_status concept");



	if (is_null($sm_user_status_input)) {
		$eventLog->log("after check candidate_status concept");
		if (!is_null($search_criteria_input)) {
			//Give List of Company clients, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$eventLog->log("before named parameters search_criteria concept");

			//Define Named parameters and corresponding Values
			$candidates_named_parameters_values_array_input = array(":company_id" => $company_id_input,":sm_memb_id_search_keyword" => $search_criteria_variable, ":sm_email_search_keyword" => $search_criteria_variable, ":sm_mobile_search_keyword" => $search_criteria_variable, ":sm_alternate_email_search_keyword" => $search_criteria_variable, ":sm_salutation_search_keyword" => $search_criteria_variable,":sm_firstname_search_keyword" => $search_criteria_variable,":sm_middlename_search_keyword" => $search_criteria_variable,":sm_lastname_search_keyword" => $search_criteria_variable,":sm_user_status_search_keyword" =>$search_criteria_variable,":ssmca_sm_site_member_classification_detail_id" => "1");
			
			$eventLog->log("before getting the content details");
			
			//Get Candidates List Count
			$candidates_list_count_get_sql = "SELECT COUNT(*) AS count FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE sm.company_id=:company_id AND ssmca.sm_site_member_classification_detail_id = :ssmca_sm_site_member_classification_detail_id " . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;
			$eventLog->log("candidates_list_count_get_sql-> " .$candidates_list_count_get_sql);
			$candidates_list_count_get_select_query = $dbcon->prepare($candidates_list_count_get_sql);
			$candidates_list_count_get_select_query->execute($candidates_named_parameters_values_array_input);
			
			$eventLog->log("after getting the candidates list concept");
			
			//Get Candidates List
			$candidates_list_get_sql = "SELECT * FROM  `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE sm.company_id=:company_id AND ssmca.sm_site_member_classification_detail_id = :ssmca_sm_site_member_classification_detail_id " . $search_criteria_in_query_without_where_keyword . $sort_details_in_query . $limit_offset_in_query;

			$candidates_list_get_select_query = $dbcon->prepare($candidates_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->execute();*/
			$candidates_list_get_select_query->execute($candidates_named_parameters_values_array_input);

		} else {
			$eventLog->log("else condition of user_status concept");
			//Define Named parameters and corresponding Values
			$candidates_named_parameters_values_array_input = array(":company_id" => $company_id_input, ":ssmca_sm_site_member_classification_detail_id" => "1");

			//Give List of Candidates Count, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$candidates_list_count_get_sql = "SELECT COUNT(*) AS count FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id  WHERE sm.company_id=:company_id AND ssmca.sm_site_member_classification_detail_id = :ssmca_sm_site_member_classification_detail_id " . $sort_details_in_query;
			$candidates_list_count_get_select_query = $dbcon->prepare($candidates_list_count_get_sql);
			$candidates_list_count_get_select_query->execute($candidates_named_parameters_values_array_input);


			//Give List of Candidates, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$candidates_list_get_sql = "SELECT * FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id  WHERE sm.company_id=:company_id AND ssmca.sm_site_member_classification_detail_id = :ssmca_sm_site_member_classification_detail_id " . $sort_details_in_query . $limit_offset_in_query;
			$candidates_list_get_select_query = $dbcon->prepare($candidates_list_get_sql);
			$candidates_list_get_select_query->execute($candidates_named_parameters_values_array_input);

		}//close of else of if (!is_null($search_criteria_input)) {


	} else {
		if (!is_null($search_criteria_input)) {
			$eventLog->log("check the search_criteria_input null concept");

			//Define Named parameters and corresponding Values
			$candidates_named_parameters_values_array_input = array(":company_id" => $company_id_input, ":sm_user_status" => $sm_user_status_input, ":sm_memb_id_search_keyword" => $search_criteria_variable, ":sm_email_search_keyword" => $search_criteria_variable, ":sm_mobile_search_keyword" => $search_criteria_variable, ":sm_alternate_email_search_keyword" => $search_criteria_variable, ":sm_salutation_search_keyword" => $search_criteria_variable,":sm_firstname_search_keyword" => $search_criteria_variable, ":sm_middlename_search_keyword" => $search_criteria_variable, ":sm_lastname_search_keyword" => $search_criteria_variable,":sm_user_status_search_keyword" => $search_criteria_variable, ":ssmca_sm_site_member_classification_detail_id" => "1");

			//$company_clients_named_parameters_values_array_input = array();

			//Get Company Clients List Count
			$candidates_list_count_get_sql = "SELECT COUNT(*) AS count FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE `company_id`=:company_id AND `sm_user_status`=:sm_user_status AND ssmca.sm_site_member_classification_detail_id = :ssmca_sm_site_member_classification_detail_id " . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;
			$eventLog->log("before query");
			$eventLog->log("candidates_list_count_get_sql-> " . $candidates_list_count_get_sql);
			$candidates_list_count_get_select_query = $dbcon->prepare($candidates_list_count_get_sql);
			$candidates_list_count_get_select_query->execute($candidates_named_parameters_values_array_input);
			$eventLog->log("after excute the value");


			//Give List of Company Clients, based on value of is_active_status
			$candidates_list_get_sql = "SELECT * FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE `company_id`=:company_id AND `sm_user_status`=:sm_user_status AND ssmca.sm_site_member_classification_detail_id = :ssmca_sm_site_member_classification_detail_id " . $search_criteria_in_query_without_where_keyword . $sort_details_in_query . $limit_offset_in_query;
		    $eventLog->log("candidates_list_count_get_sql-> " . $candidates_list_get_sql);
			$candidates_list_get_select_query = $dbcon->prepare($candidates_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);\
			$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->execute();*/
			$candidates_list_get_select_query->execute($candidates_named_parameters_values_array_input);

		} else {
			$eventLog->log("else condition for search_criteria_input null concept");
			//Define Named parameters and corresponding Values
			$candidates_named_parameters_values_array_input = array(":sm_user_status" => $sm_user_status_input, ":company_id" => $company_id_input, ":ssmca_sm_site_member_classification_detail_id" => "1");

			//Give List of candidates Count, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$candidates_list_count_get_sql = "SELECT COUNT(*) AS count FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE `company_id`=:company_id AND `sm_user_status`=:sm_user_status  AND ssmca.sm_site_member_classification_detail_id = :ssmca_sm_site_member_classification_detail_id " . $sort_details_in_query;
			$candidates_list_count_get_select_query = $dbcon->prepare($candidates_list_count_get_sql);
			$candidates_list_count_get_select_query->execute($candidates_named_parameters_values_array_input);


			//Give List of Candidates, based on value of is_active_status
			$candidates_list_get_sql = "SELECT * FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE `company_id`=:company_id AND `sm_user_status`=:sm_user_status  AND ssmca.sm_site_member_classification_detail_id = :ssmca_sm_site_member_classification_detail_id " . $sort_details_in_query . $limit_offset_in_query;

			$candidates_list_get_select_query = $dbcon->prepare($candidates_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->execute();*/
			$candidates_list_get_select_query->execute($candidates_named_parameters_values_array_input);

		}//close of else of if (!is_null($search_criteria_input)) {



	}//close of else of if (is_null($company_status_input)) {

	//Process  Get Candidates Count
	$eventLog->log("get the row count value");
	if($candidates_list_count_get_select_query->rowCount() > 0) {
	    $candidates_list_count_get_select_query_result = $candidates_list_count_get_select_query->fetch();
	    //print_r($candidates_list_count_get_select_query_result);

		$total_candidates_count = $candidates_list_count_get_select_query_result["count"];
		$constructed_array["total_records_count"] = $total_candidates_count;

	}//close of if($candidates_list_count_get_select_query->rowCount() > 0) {


	//Process / Fetch Companies List
	if($candidates_list_get_select_query->rowCount() > 0) {
	    $candidates_list_get_select_query_result = $candidates_list_get_select_query->fetchAll();
	    //print_r($companies_list_get_select_query_result);
	    $eventLog->log("retrive the details ");
		foreach ($candidates_list_get_select_query_result as $candidates_list_get_select_query_result_row) {

			$temp_row_array = array();
		    $temp_row_array["sm_memb_id"] = $candidates_list_get_select_query_result_row["sm_memb_id"];
		    $temp_row_array["sm_email"] = $candidates_list_get_select_query_result_row["sm_email"];
			$temp_row_array["sm_mobile"] = $candidates_list_get_select_query_result_row["sm_mobile"];
		    $temp_row_array["sm_alternate_email"] = $candidates_list_get_select_query_result_row["sm_alternate_email"];
		    $temp_row_array["sm_salutation"] = $candidates_list_get_select_query_result_row["sm_salutation"];
			$temp_row_array["sm_firstname"] = $candidates_list_get_select_query_result_row["sm_firstname"];
			$temp_row_array["sm_middlename"] = $candidates_list_get_select_query_result_row["sm_middlename"];
			$temp_row_array["sm_lastname"] = $candidates_list_get_select_query_result_row["sm_lastname"];
			$temp_row_array["company_id"] = $candidates_list_get_select_query_result_row["company_id"];
			//$temp_row_array["company_id"] = $candidates_list_get_select_query_result_row["company_id"];
			$temp_row_array["added_by_date"] = $candidates_list_get_select_query_result_row["added_date_time"];
            $temp_row_array["added_by_admin_user_id"] = $candidates_list_get_select_query_result_row["added_by_admin_user_id"];
            $temp_row_array["added_by_name"] = $candidates_list_get_select_query_result_row["added_by_admin_user_firstname"] . " " . $candidates_list_get_select_query_result_row["added_by_admin_user_middlename"] . " "
			. $candidates_list_get_select_query_result_row["added_by_admin_user_lastname"];
			
			if($temp_row_array["sm_salutation"] == "") {
				$salutation_string = "";
				
			} else {
				$salutation_string = $temp_row_array["sm_salutation"] . ".";
				
			}

			
			if (($temp_row_array["sm_firstname"] != "") && ($temp_row_array["sm_middlename"] != "") && ($temp_row_array["sm_lastname"] != "")) {
				$candidate_fullname = $salutation_string . $temp_row_array["sm_firstname"] . " " . $temp_row_array["sm_middlename"] . " " . $temp_row_array["sm_lastname"];

			} else if (($temp_row_array["sm_firstname"] == "") && ($temp_row_array["sm_middlename"] != "") && ($temp_row_array["sm_lastname"] == "")) {
				$candidate_fullname = $salutation_string . $temp_row_array["sm_firstname"] . " " . $temp_row_array["sm_middlename"];
		
			} else if (($temp_row_array["sm_firstname"] != "") && ($temp_row_array["sm_middlename"] == "") && ($temp_row_array["sm_lastname"] == "")) {
				$candidate_fullname = $salutation_string . $temp_row_array["sm_firstname"];
		
			} else if (($temp_row_array["sm_firstname"] != "") && ($temp_row_array["sm_middlename"] == "") && ($temp_row_array["sm_lastname"] != "")) {
				$candidate_fullname = $salutation_string . $temp_row_array["sm_firstname"] . " " . $temp_row_array["sm_lastname"];
			
			} else if (($temp_row_array["sm_firstname"] == "") && ($temp_row_array["sm_middlename"] != "") && ($temp_row_array["sm_lastname"] != "")) {
				$candidate_fullname = $salutation_string . $temp_row_array["sm_middlename"] . " " . $temp_row_array["sm_lastname"];
			
			} else if (($temp_row_array["sm_firstname"] == "") && ($temp_row_array["sm_middlename"] == "") && ($temp_row_array["sm_lastname"] != "")) {
				$candidate_fullname = $salutation_string . $temp_row_array["sm_lastname"];
			} else {
				$candidate_fullname = "";
			}
			
			$temp_row_array["sm_fullname"] = $candidate_fullname;
			$temp_row_array["sm_user_status"] = $candidates_list_get_select_query_result_row["sm_user_status"];
            
			$candidate_resume_details_result = candidate_rel_uploaded_resumes_based_on_sm_memb_id_input_one($temp_row_array["sm_memb_id"],$company_id_input);	    
			if(count($candidate_resume_details_result) > 0) {
			  $temp_row_array["crur_id"] = $candidate_resume_details_result["crur_id"];
			    
				$link_expiry_time = $current_epoch+$expiring_link_lifetime;
				$uploaded_file_link_with_signature_result = create_uploaded_file_link_with_signature($ea_extracted_jwt_token_sub, $temp_row_array["sm_memb_id"], $temp_row_array["crur_id"], "https", $site_hostname_value, $link_expiry_time, $expiring_link_hash_algorithm, $expiring_link_secret_key);
				if (isset($uploaded_file_link_with_signature_result["uploaded_file_url"])) {
				$temp_row_array["uploaded_file_url"] = $uploaded_file_link_with_signature_result["uploaded_file_url"];
					
				} else {
					$temp_row_array["uploaded_file_url"] = null;
				}//close of if (isset($uploaded_file_link_with_signature_result["uploaded_file_url"])) {
					
				$download_file_link_with_signature_result = create_download_file_link_with_signature($ea_extracted_jwt_token_sub, $temp_row_array["sm_memb_id"], $temp_row_array["crur_id"], "https", $site_hostname_value, $link_expiry_time, $expiring_link_hash_algorithm, $expiring_link_secret_key);
				if (isset($download_file_link_with_signature_result["download_file_url"])) {
				$temp_row_array["download_file_url"] = $download_file_link_with_signature_result["download_file_url"];
					
				} else {
					$temp_row_array["download_file_url"] = null;
				}//close of if (isset($download_file_link_with_signature_result["uploaded_file_url"])) {
			} else {
				$temp_row_array["crur_id"] = null;
				$temp_row_array["uploaded_file_url"] = null;
				$temp_row_array["download_file_url"] = null;
			}
			
			$constructed_array["list"][] = $temp_row_array;
	    }//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

	}//close of if($companies_list_get_select_query->rowCount() > 0) {
		$eventLog->log("all input values are received");
	return $constructed_array;
}



function candidate_rel_uploaded_resumes_based_on_sm_memb_id_input_one($sm_memb_id,$company_id) {
    global $dbcon;
	$constructed_array = array();
	
	$candidate_rel_uploaded_resumes_based_on_sm_memb_id_sql = "SELECT * FROM `candidate_rel_uploaded_resumes` WHERE `sm_memb_id`=:sm_memb_id AND `company_id`=:company_id LIMIT 1";
	$candidate_rel_uploaded_resumes_based_on_sm_memb_id_query = $dbcon->prepare($candidate_rel_uploaded_resumes_based_on_sm_memb_id_sql);
	$candidate_rel_uploaded_resumes_based_on_sm_memb_id_query->bindValue(":sm_memb_id",$sm_memb_id);
	$candidate_rel_uploaded_resumes_based_on_sm_memb_id_query->bindValue(":company_id",$company_id);	
	$candidate_rel_uploaded_resumes_based_on_sm_memb_id_query->execute(); 
	
	if($candidate_rel_uploaded_resumes_based_on_sm_memb_id_query->rowCount() > 0) {
		$candidate_rel_uploaded_resumes_based_on_sm_memb_id_query_result = $candidate_rel_uploaded_resumes_based_on_sm_memb_id_query->fetch();
	     return $candidate_rel_uploaded_resumes_based_on_sm_memb_id_query_result;
	
	}//close of if($candidate_rel_uploaded_resumes_based_on_sm_memb_id_query->rowCount() > 0) {
	return $constructed_array;
}

exit;
?>