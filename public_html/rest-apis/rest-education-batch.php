<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * REST API for candidate all education qualifications collect
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "4")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['user_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['user_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['user_id']))
                    
                    if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
					$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id']))					
					
					if (isset($ea_received_rest_ws_raw_array_input['all_education_qualifications'])) {
						$content .= json_encode($ea_received_rest_ws_raw_array_input['all_education_qualifications']) . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['all_education_qualifications'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					//$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$all_education_qualifications_input = $ea_received_rest_ws_raw_array_input['all_education_qualifications'];
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//Check if all inputs are received correctly from the REST Web Service
				if ($user_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-user-id";
					$response['status_description'] = "Missing User ID";
					
					$eventLog->log("missing-user-id: Please provide a valid User ID.");
					
				} else if ($company_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company ID";
					
					$eventLog->log("missing-company-id: Please provide a valid Company ID.");
					
					
				} else if ($all_education_qualifications_input == "") {	
					//Invalid Education Qualifications Received scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "empty-education-qualification-details-input";
					$response['status_description'] = "Empty Education Qualification details";
					
					$eventLog->log("empty-education-qualification-details-input: Education Qualifications are not received for this Candidate, Please check and try again.");
					
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Missing Some Additional Information";
					
					$eventLog->log("missing-some-additional-information: Some Additional Information is missing, please check and try again.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					$ea_action_specific_auth_check_result = "";
					
					//User Details, based on User ID input, through api request
					$user_basic_details_result = user_basic_details_check_based_on_user_id($user_id_input);
					
					if (count($user_basic_details_result) > 0) {
						
						$user_rel_company_id = $user_basic_details_result["company_id"];
						$user_rel_sm_user_status = $user_basic_details_result["sm_user_status"];
						
						$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
						//$ea_action_specific_auth_check_result = ea_check_action_specific_user_privileges_different_user_levels_reset_password_feature($ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_user_company_id, $ea_extracted_jwt_token_sub, $user_id_input, $ea_extracted_jwt_token_user_privileges_list, $user_rel_company_id, $page_filename);					
						
						//echo "ea_action_specific_auth_check_result: " . $ea_action_specific_auth_check_result;					
							
							
						
					
						
						if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
								$eventLog->log("inter into try");
								//Event Time, as per Indian Standard Time
								$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
								
									
								//Define an array, to Collect "degree" and "field of study" field values of all received Education Qualification info.
								$all_received_degree_fieldofstudy_fields_array = array();
								
								//Define an array, to Collect "degree" and "field of study" field values of all Education Qualification info, from the Database.
								$all_db_based_degree_fieldofstudy_fields_array = array();
								
								
								//Define an array, to Collect all received Education Qualification info, with corresponding record processing status and info
								$all_collected_content_with_result_array = array();
								$eventLog->log("before count");
								//Collect All Education Qualifications List, of particular candidate, to extract degree and field of study field content
								$candidate_education_qualification_records_list_result = candidate_education_qualification_records_list($user_id_input);
								$eventLog->log("after count");
								$candidate_education_qualification_records_list_result_count = count($candidate_education_qualification_records_list_result);
								
								$eventLog->log("candidate_education_qualification_records_list_result_count: " . $candidate_education_qualification_records_list_result_count);
								
								
								if ((is_array($all_education_qualifications_input)) && ((count($all_education_qualifications_input) > 0) || ($candidate_education_qualification_records_list_result_count > 0))) {
										
									//Do process each record (Education Qualification)
									foreach ($all_education_qualifications_input as $all_education_qualifications_input_row) {
										
										$received_degree_fieldofstudy_fields_array = array();
										$collected_content_with_result_array = array();
										
										$collected_content_with_result_array = $all_education_qualifications_input_row;
										
										
										$collected_content_with_result_array_json_encoded = json_encode($collected_content_with_result_array);
										$eventLog->log("collected_array row initial stage: " . $collected_content_with_result_array_json_encoded);	
										
										$all_education_qualifications_input_row_count = count($all_education_qualifications_input_row);
										//$eventLog->log("collected_array row initial stage count: " . $all_education_qualifications_input_row_count);
										
										if (count($all_education_qualifications_input_row) == 7) {
											
											$eventLog->log("inside count(all_education_qualifications_input_row) == 10 if condition: ");
											
											
											//[{"ceq_id":"","degree":"dgfgf","field_of_study":"ghghgh","institution_name":"fhghghg","university_name":"","board_name":"","year_of_passout":"2012","earned_percentage":"65","earned_grade":""},{"ceq_id":"","degree":"gfgfg","field_of_study":"fgdfgf","institution_name":"fgdfg","university_name":"","board_name":"","year_of_passout":"2011","earned_percentage":"75","earned_grade":""}]
										
											$creq_id = trim(isset($all_education_qualifications_input_row["creq_id"]) ? filter_var($all_education_qualifications_input_row["creq_id"], FILTER_SANITIZE_NUMBER_INT) : '');
											$educational_program = trim(isset($all_education_qualifications_input_row["educational_program"]) ? filter_var($all_education_qualifications_input_row["educational_program"], FILTER_SANITIZE_STRING) : '');
											$major = trim(isset($all_education_qualifications_input_row["major"]) ? filter_var($all_education_qualifications_input_row["major"], FILTER_SANITIZE_STRING) : '');
											
											$eventLog->log("major->");
											$eventLog->log($major);
											
											
											$school_name = trim(isset($all_education_qualifications_input_row["school_name"]) ? filter_var($all_education_qualifications_input_row["school_name"], FILTER_SANITIZE_STRING) : '');
											$year_of_passout = trim(isset($all_education_qualifications_input_row["year_of_passout"]) ? filter_var($all_education_qualifications_input_row["year_of_passout"], FILTER_SANITIZE_NUMBER_INT) : '');
											$earned_percentage = trim(isset($all_education_qualifications_input_row["earned_percentage"]) ? filter_var($all_education_qualifications_input_row["earned_percentage"], FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION) : '');
											$earned_points = trim(isset($all_education_qualifications_input_row["earned_points"]) ? filter_var($all_education_qualifications_input_row["earned_points"], FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION) : '');
											
											//Do process the record (Education Qualification), and Collect "degree" and "field of study" field values of received Education Qualification
											$received_degree_fieldofstudy_fields_array["major"] = $all_education_qualifications_input_row["major"];
											$received_degree_fieldofstudy_fields_array["educational_program"] = $all_education_qualifications_input_row["educational_program"];
											
											//Check if all record level inputs are received, as part of "all_education_qualifications" field, and correctly from the REST Web Service
											if ($educational_program == "") {
												//Empty Degree Field
												
												$collected_content_with_result_array["educational_program"] = $educational_program;
												
												$eventLog->log("Empty Degree input");
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
											} else if ($major == "") {
												//Empty Field of Study Field
												
												$collected_content_with_result_array["major"] = $major;
												
												$eventLog->log("Empty Field of Study input");
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
											} else if ($school_name == "") {
												//three fields are Empty
												
												$collected_content_with_result_array["school_name"] = $school_name;
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
												$eventLog->log("Empty Institution Name, University Name, Board Name inputs");
												
											} else if (strlen($year_of_passout) != "4") {
												//Invalid Year of Passout Input
												
												$collected_content_with_result_array["year_of_passout"] = $year_of_passout;
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
												$eventLog->log("Invalid Year of Passout input, as it is not of 4 characters length");
												
											} else if (($earned_percentage != "") && ((!filter_var($earned_percentage, FILTER_VALIDATE_FLOAT)) || ($earned_percentage > "100"))) {
												//Invalid Earned Percentage Input
												
												$collected_content_with_result_array["earned_percentage"] = $earned_percentage;
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
												$eventLog->log("Invalid Earned Percentage input, as it is not float");
												$eventLog->log($earned_points);
												
											} else if (($earned_points != "") && ((!filter_var($earned_points, FILTER_VALIDATE_FLOAT)) || ($earned_points > "10") )) {
												//Invalid Earned Points Input
												
												$collected_content_with_result_array["earned_points"] = $earned_points;
												
												$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
												
												$eventLog->log("Invalid Earned Points input, as it is not float");
							
											} else {
												//All Education Qualification Details are valid, for this record
												
												$eventLog->log("All Education Qualification Details are valid, for this record");
												
												/* COMMENTED TO PLACE THIS OUTSIDE CONDITION //Do process the record (Education Qualification), and Collect "degree" and "field of study" field values of received Education Qualification
												$received_degree_fieldofstudy_fields_array["degree"] = $all_education_qualifications_input_row["degree"];
												$received_degree_fieldofstudy_fields_array["field_of_study"] = $all_education_qualifications_input_row["field_of_study"];
												*/
									
												//Do Duplicate Check, for each Education Qualification, in the application, w.r.t. the particular candidate's sm_memb_id
												$candidate_education_qualification_check_result = candidate_education_qualification_details_duplicate_check($user_id_input, $educational_program, $major, $company_id_input);
												
												if (count($candidate_education_qualification_check_result) > 0) {
													//Do get the reference id
													$creq_id_from_db = $candidate_education_qualification_check_result["creq_id"];
													$eventLog->log("creq_id_from_db :");
													$eventLog->log($creq_id_from_db);
													$eventLog->log("creq_id:");
													$eventLog->log($creq_id);
													
													//Check, if ceq_id, is same, from both sources (received from api & from db source, after duplicate check)
													if ($creq_id == $creq_id_from_db) {
														//ceq_id from the api request and from db source are matching, for particular education qualification record.
														$eventLog->log("ceq_id from the api request and from db source are matching, for particular education qualification record.");
														
														
														$collected_content_with_result_array["creq_id"] = $creq_id;
													
														$eventLog->log("Update Query Condition");
														
														//Do Update Query to existing record
														$candidate_education_qualification_update_result = update_candidate_education_qualification_details_based_on_ceq_id($school_name, $year_of_passout, $earned_percentage, $earned_points, $ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_sub, $event_datetime, $current_epoch, $creq_id);
														
														if ($candidate_education_qualification_update_result) {
															//Update Success
															$collected_content_with_result_array["query_type"] = "UPDATE-QUERY";
															$collected_content_with_result_array["query_status"] = "SUCCESS";
															$collected_content_with_result_array["input_record_validity_status"] = "VALID";
															
															$eventLog->log("Update Query Condition - Success");
															
														} else {
															$collected_content_with_result_array["query_type"] = "UPDATE-QUERY";
															$collected_content_with_result_array["query_status"] = "FAILURE";
															$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
											
															$eventLog->log("Update Query Condition - Failure");
															
														}//close of else of if ($candidate_education_qualification_update_result) {
														
														
													} else {
														//ceq_id from the api request and from db source are different, for particular education qualification record.
														$eventLog->log("ceq_id from the api request and from db source are different, for particular education qualification record.");
														
														$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
														
													}//close of else of if ($ceq_id == $ceq_id_from_db) {
														
													
												} else {
													
													$eventLog->log("Insert Query Condition");
													
													$eventLog->log("user_id_input: " . $user_id_input);
													$eventLog->log("company_id_input: " . $company_id_input);
													$eventLog->log("major: " . $major);
													$eventLog->log("educational_program: " . $educational_program);
													$eventLog->log("school_name: " . $school_name);
													$eventLog->log("year_of_passout: " . $year_of_passout);
													$eventLog->log("earned_percentage: " . $earned_percentage);
													$eventLog->log("earned_points: " . $earned_points);
													$eventLog->log("sequence order: " . "1111");
													$eventLog->log("ea_extracted_jwt_token_user_type: " . $ea_extracted_jwt_token_user_type);
													$eventLog->log("ea_extracted_jwt_token_sub: " . $ea_extracted_jwt_token_sub);
													$eventLog->log("event_datetime: " . $event_datetime);
													$eventLog->log("current_epoch: " . $current_epoch);
													
													//Find the Sequence Number, for the New Education Qualification
													$candidate_current_education_qualification_count_result = candidate_education_qualification_record_count($user_id_input);
													$eventLog->log("candidate_current_education_qualification_count_result");
													if (isset($candidate_current_education_qualification_count_result["current_count"])) {
														
														$candidate_current_education_qualification_count = $candidate_current_education_qualification_count_result["current_count"];
														
													} else {
														$candidate_current_education_qualification_count = 0;
														
													}//close of else of if (isset($candidate_current_education_qualification_count_result["current_count"])) {
														
													$candidate_education_qualification_sequence_order = $candidate_current_education_qualification_count+1;
													
													$eventLog->log("before insert");
													//Do Insert Query to add a new record
													$candidate_education_qualification_insert_result = insert_candidate_education_qualification_details($user_id_input, $major, $educational_program, $school_name, $year_of_passout, $earned_percentage, $earned_points, $candidate_education_qualification_sequence_order, $ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_sub, $event_datetime, $current_epoch, $company_id_input);
													
													if (isset($candidate_education_qualification_insert_result["last_inserted_id"])) {
														//Insert Success
														$collected_content_with_result_array["creq_id"] = $candidate_education_qualification_insert_result["last_inserted_id"];
														$collected_content_with_result_array["query_type"] = "INSERT-QUERY";
														$collected_content_with_result_array["query_status"] = "SUCCESS";
														$collected_content_with_result_array["input_record_validity_status"] = "VALID";
														
														$eventLog->log("Insert Query Condition - Success");
														//$additional_document_ref_input = $degree . " (" . $field_of_study . ")";
														$last_inserted_candidate_education_qualification_id = $candidate_education_qualification_insert_result["last_inserted_id"];
														//candidate_education_qualification_rel_file_upload_references_insert($user_id_input,"education-qualifications",$last_inserted_candidate_education_qualification_id,$additional_document_ref_input,null,null,$ea_extracted_jwt_token_user_type,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
														
														//COMMENTED< PLEASE CHANGE THIS FUNCTION candidate_vrpi_rel_file_upload_references_insert($user_id_input,"visa-rel-personal-info",$last_inserted_candidate_visa_rel_personal_info_id,$additional_document_ref,null,null,$ea_extracted_jwt_token_user_type,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
														
													} else {
														//Insert Failure
														$collected_content_with_result_array["creq_id"] = null;
														$collected_content_with_result_array["query_type"] = "INSERT-QUERY";
														$collected_content_with_result_array["query_status"] = "FAILURE";
														$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
										
														$eventLog->log("Insert Query Condition - Failure");
														
													}//close of else of if (isset($candidate_education_qualification_insert_result["last_inserted_id"])) {
														
													
												}//close of else of if (count($candidate_education_qualification_check_result) > 0) {
												
												$collected_content_with_result_array_json_encoded = json_encode($collected_content_with_result_array);
												//$eventLog->log("collected_array row next stage: " . $collected_content_with_result_array_json_encoded);	
												
												
											}//close of else of if ($degree == "") {
												
											
										} else {
											
											$eventLog->log("inside count(all_education_qualifications_input_row) == 10 else condition: ");
											
											//Expected number of fields are not sent, per education qualification record
											$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
										
										}//close of else of if (count($all_education_qualifications_input_row)) {
										
										$all_received_degree_fieldofstudy_fields_array[] = $received_degree_fieldofstudy_fields_array;
										$all_collected_content_with_result_array[] = $collected_content_with_result_array;
																								
										
									}//close of foreach ($all_education_qualifications_input as $all_education_qualifications_input_row) {
									
									$all_collected_content_with_result_array_json_encoded = json_encode($all_collected_content_with_result_array);
									$eventLog->log("collected_array final (didnot enter foreach loop): " . $all_collected_content_with_result_array_json_encoded);	
									
									
									/*//Collect All Education Qualifications List, of particular candidate, to extract degree and field of study field content
									$candidate_education_qualification_records_list_result = candidate_education_qualification_records_list($user_id_input);
									$candidate_education_qualification_records_list_result_count = count($candidate_education_qualification_records_list_result);
									
									$eventLog->log("candidate_education_qualification_records_list_result_count: " . $candidate_education_qualification_records_list_result_count);
									*/
									if (count($candidate_education_qualification_records_list_result) > 0) {
										//One or More than one Education Qualification is received from the Database.
										
										$all_received_degree_fieldofstudy_fields_array_json_encoded = json_encode($all_received_degree_fieldofstudy_fields_array);
										$eventLog->log("all_received_degree_fieldofstudy_fields_array_json_encoded : " . $all_received_degree_fieldofstudy_fields_array_json_encoded);	
										
										//Do process the record (Education Qualification), and Collect "degree" and "field of study" field values of received Education Qualification
										foreach($candidate_education_qualification_records_list_result as $candidate_education_qualification_records_list_result_row) {
											
											$db_based_ceq_id_field_value = $candidate_education_qualification_records_list_result_row["creq_id"];
											if(count($candidate_education_qualification_records_list_result) > 0){
											$db_based_degree_field_value = $candidate_education_qualification_records_list_result_row["major"];
										} else {

											$db_based_degree_field_value = null;

										} 

										 if(count($candidate_education_qualification_records_list_result)>0){
											$db_based_fieldofstudy_field_value = $candidate_education_qualification_records_list_result_row["educational_program"];
										
										} else {
											//$db_based_degree_field_value = null;
											$db_based_fieldofstudy_field_value = null ;
										}
											$db_based_additional_document_ref = $db_based_degree_field_value . " (" . $db_based_fieldofstudy_field_value . ")";
											
											$eventLog->log("db_based_degree_field_value: ". $db_based_degree_field_value);
											$eventLog->log("db_based_fieldofstudy_field_value: ". $db_based_fieldofstudy_field_value);
											
											$record_deletion_status = "not-matched";
											
											foreach ($all_received_degree_fieldofstudy_fields_array as $all_received_degree_fieldofstudy_fields_array_row) {
												if(count($all_received_degree_fieldofstudy_fields_array_row)>0){
												$all_received_row_based_degree_field_value = $all_received_degree_fieldofstudy_fields_array_row["major"];
											} else{
												$all_received_row_based_degree_field_value = null ;
											} 

											  if(count($all_received_degree_fieldofstudy_fields_array_row)>0){
												$all_received_row_based_fieldofstudy_field_value = $all_received_degree_fieldofstudy_fields_array_row["educational_program"];
											
											} else {
												//$all_received_row_based_degree_field_value = null ;
												$all_received_row_based_fieldofstudy_field_value = null;
											}
												
												$eventLog->log("all_received_row_based_degree_field_value: ". $all_received_row_based_degree_field_value);
												$eventLog->log("all_received_row_based_fieldofstudy_field_value: ". $all_received_row_based_fieldofstudy_field_value);
											
												if (($all_received_row_based_degree_field_value == $db_based_degree_field_value) && ($all_received_row_based_fieldofstudy_field_value == $db_based_fieldofstudy_field_value)) {
													//Delete this matching Record, from the Form
													
													$eventLog->log("Match Found with: Degree: " . $all_received_row_based_degree_field_value . " Field of Study: " . $all_received_row_based_fieldofstudy_field_value);
													
													$record_deletion_status = "matched";
													
													break;
													
												}//close of if (($all_received_row_based_degree_field_value != $db_based_degree_field_value) && ($all_received_row_based_fieldofstudy_field_value != $db_based_fieldofstudy_field_value)) {
													
											}//close of foreach ($all_received_degree_fieldofstudy_fields_array as $all_received_degree_fieldofstudy_fields_array_row) {
											
											/* -----------------if ($record_deletion_status == "not-matched") {
												
												$candidate_education_qualification_unused_file_record_delete_status_result = delete_candidate_unused_file_record_based_on_activity_ref_id_input($db_based_ceq_id_field_value,$db_based_additional_document_ref);
												
												$eventLog->log("After delete query");
												
												$values = json_encode($candidate_education_qualification_unused_file_record_delete_status_result);
												
												$eventLog->log($values);
											
												if (count($candidate_education_qualification_unused_file_record_delete_status_result)>0) {
													//Candidate specific Professional Summary record is Deleted Successfully.
													$eventLog->log("Candidate specific education qualification unused file record is Deleted Successfully.");
											
													$candidate_education_qualification_record_delete_status_result = delete_candidate_education_qualification_record_based_on_ceq_id_input($db_based_ceq_id_field_value);
													
													if($candidate_education_qualification_record_delete_status_result) {
																$eventLog->log("Candidate specific Educational Qualification record is Deleted Successfully.");
													}	
												
												} else {
													//Error Deleting the Candidate specific Professional Summary record, that is not received in the API Request.
													$eventLog->log("Error Deleting the Candidate specific Educational Qualification record, that is not received in the API Request.");
													
												}//close of else of if ($candidate_professional_summary_record_delete_status_result) {
												
												
											}//close of if ($record_deletion_status == "not-matched") { ---------------*/
												
											
										}//close of foreach($candidate_education_qualification_records_list_result as $candidate_education_qualification_records_list_result_row) {
										
										
									} else {
										//Education Qualifications of the Candidate are not available in the Database, this means, Nothing to Remove.
										$eventLog->log("Education Qualifications of the Candidate are not available in the Database, this means, Nothing to Remove.");
										
									}//close of else of if (count($candidate_education_qualification_records_list_result) > 0) {
										
									
									$all_collected_content_with_result_array_json_encoded = json_encode($all_collected_content_with_result_array);
									$eventLog->log("all_collected_content_with_result_array_json_encoded: " . $all_collected_content_with_result_array_json_encoded);
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = $all_collected_content_with_result_array;
									$response['status'] = "candidate-education-qualification-data-ingestion-result";
									$response['status_description'] = "All Candidate submitted Education Qualification Data is processed.";
									
									$eventLog->log("candidate-education-qualification-data-ingestion-result: All Candidate submitted Education Qualification Data is processed.");	
									
								} else {
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "no-records-received";
									$response['status_description'] = "Please Submit at least One Education Qualification.";
									
									$eventLog->log("no-records-received: Please Submit at least One Education Qualification.");	
									
								}//close of else of if ((is_array($all_education_qualifications_input)) && ((count($all_education_qualifications_input) > 0) || ($candidate_education_qualification_records_list_result_count > 0))) {
								
								
							} catch (Exception $e) {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "candidate-education-qualifications-processing-error";
								$response['status_description'] = "Error occurred when processing candidate's Education Qualifications.";
								
								$eventLog->log("candidate-education-qualifications-processing-error: Error occurred when processing candidate's Education Qualifications.");	
								
							} 
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-subject-platform-scope-user";
							$response['status_description'] = "Insufficient Set of Permissions, to complete this action";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-subject-platform-scope-user: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-different-company-user-id";
							$response['status_description'] = "Insufficient Set of Permissions, as the User belongs to a different company";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-INSUFFICIENT-PERMISSIONS") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions, as this process will not be allowed any further";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-self-action";
							$response['status_description'] = "Insufficient Set of Permissions, as self action is Dis-allowed";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-disallowed-self-action: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-ACTION-SUBJECT-IS-A-DIFFERENT-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-action-on-different-user";
							$response['status_description'] = "Insufficient Set of Permissions, as action subject is a different user, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-disallowed-action-on-different-user: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						}//close of if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
						
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "Invalid User Account that is attempted to be edited";
						
						$eventLog->log("invalid-user-account: The User Account, that is attempted to be edited, is invalid, please check and try again.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
						
					
				}//close of else of if ($user_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token";
			
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");	
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input";
	
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));	
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){


function candidate_education_qualification_details_duplicate_check($user_id_input, $educational_program, $major, $company_id_input) {
	
	global $dbcon;
	
	$constructed_array = array();
	
	$candidate_major_input = '%'.$major.'%';
	$candidate_educational_program_input = '%'.$educational_program.'%';
	
	$candidate_education_qualification_check_sql = "SELECT * FROM `candidate_rel_education_qualifications` WHERE `sm_memb_id`=:sm_memb_id AND `major` LIKE :major AND `educational_program` LIKE :educational_program AND `company_id`=:company_id";
	$candidate_education_qualification_check_select_query = $dbcon->prepare($candidate_education_qualification_check_sql);
	$candidate_education_qualification_check_select_query->bindValue(":sm_memb_id",$user_id_input);
    $candidate_education_qualification_check_select_query->bindValue(":company_id",$company_id_input);		
	$candidate_education_qualification_check_select_query->bindValue(":major",$candidate_major_input);	
	$candidate_education_qualification_check_select_query->bindValue(":educational_program",$candidate_educational_program_input);		
	$candidate_education_qualification_check_select_query->execute(); 
	
	if($candidate_education_qualification_check_select_query->rowCount() > 0) {
		$candidate_education_qualification_check_select_query_result = $candidate_education_qualification_check_select_query->fetch();
	    return $candidate_education_qualification_check_select_query_result;
	
	}//close of if($candidate_education_qualification_check_select_query->rowCount() > 0) {
	return $constructed_array;
	
}

function update_candidate_education_qualification_details_based_on_ceq_id($school_name, $year_of_passout, $earned_percentage, $earned_points, $ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_sub, $event_datetime, $current_epoch, $creq_id) {
	global $dbcon;
	
	$candidate_education_qualification_details_update_sql = "UPDATE `candidate_rel_education_qualifications` SET `school_name`=:school_name,`year_of_passout`=:year_of_passout,`earned_percentage`=:earned_percentage,`earned_points`=:earned_points,`last_updated_by_user_id`=:last_updated_by_user_id,`last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch WHERE `creq_id`=:creq_id";	
	$candidate_education_qualification_details_update_query = $dbcon->prepare($candidate_education_qualification_details_update_sql);		
	$candidate_education_qualification_details_update_query->bindValue(":school_name",$school_name);
	$candidate_education_qualification_details_update_query->bindValue(":year_of_passout",$year_of_passout);
	if($earned_percentage == "") {
		$candidate_education_qualification_details_update_query->bindValue(":earned_percentage",null);
	} else {
		$candidate_education_qualification_details_update_query->bindValue(":earned_percentage",$earned_percentage);
		
	} if($earned_points == "") {
		$candidate_education_qualification_details_update_query->bindValue(":earned_points",null);
	} else {
		$candidate_education_qualification_details_update_query->bindValue(":earned_points",$earned_points);
	}
	
	//$candidate_education_qualification_details_update_query->bindValue(":last_updated_by_user_type",$ea_extracted_jwt_token_user_type);
	$candidate_education_qualification_details_update_query->bindValue(":last_updated_by_user_id",$ea_extracted_jwt_token_sub);
	$candidate_education_qualification_details_update_query->bindValue(":last_updated_date_time",$event_datetime);
	$candidate_education_qualification_details_update_query->bindValue(":last_updated_date_time_epoch",$current_epoch);
	$candidate_education_qualification_details_update_query->bindValue(":creq_id",$creq_id);
	
	if ($candidate_education_qualification_details_update_query->execute()) {
		return true;
		
	}//close of if ($candidate_education_qualification_details_update_query->execute()) {
	
	return false;
}

function insert_candidate_education_qualification_details($user_id_input, $major, $educational_program, $school_name, $year_of_passout, $earned_percentage, $earned_points, $candidate_education_qualification_sequence_order, $ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_sub, $event_datetime, $current_epoch, $company_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	
	//Do Insert Query

	$candidate_education_qualification_insert_sql = "INSERT INTO `candidate_rel_education_qualifications`(`sm_memb_id`, `company_id`, `school_name`, `educational_program`, `year_of_passout`, `major`, `earned_percentage`, `earned_points`, `sequence_order`, `added_by_user_id`, `added_date_time`, `added_date_time_epoch`) VALUES (:sm_memb_id,:company_id,:school_name,:educational_program,:year_of_passout,:major,:earned_percentage,:earned_points,:sequence_order,:added_by_user_id,:added_date_time,:added_date_time_epoch)";
	
	
	$candidate_education_qualification_insert_query = $dbcon->prepare($candidate_education_qualification_insert_sql);
	$candidate_education_qualification_insert_query->bindValue(":sm_memb_id",$user_id_input);
	$candidate_education_qualification_insert_query->bindValue(":company_id",$company_id_input);
	$candidate_education_qualification_insert_query->bindValue(":major",$major);
	$candidate_education_qualification_insert_query->bindValue(":educational_program",$educational_program);
	$candidate_education_qualification_insert_query->bindValue(":school_name",$school_name);
	$candidate_education_qualification_insert_query->bindValue(":year_of_passout",$year_of_passout);
	if($earned_percentage == "") {
		$candidate_education_qualification_insert_query->bindValue(":earned_percentage",null);
	} else {
		$candidate_education_qualification_insert_query->bindValue(":earned_percentage",$earned_percentage);
	} if($earned_points == "") {
		$candidate_education_qualification_insert_query->bindValue(":earned_points",null);
	} else {
		$candidate_education_qualification_insert_query->bindValue(":earned_points",$earned_points);
	}
	$candidate_education_qualification_insert_query->bindValue(":sequence_order",$candidate_education_qualification_sequence_order);
	/* $candidate_education_qualification_insert_query->bindValue(":added_by_user_type",$ea_extracted_jwt_token_user_type) */
	$candidate_education_qualification_insert_query->bindValue(":added_by_user_id",$ea_extracted_jwt_token_sub);
	$candidate_education_qualification_insert_query->bindValue(":added_date_time",$event_datetime);
	$candidate_education_qualification_insert_query->bindValue(":added_date_time_epoch",$current_epoch);
	
	if ($candidate_education_qualification_insert_query->execute()) {
		
		$last_inserted_id = $dbcon->lastInsertId();			
		//$eventLog->log("record inserted successfully");
		
		$constructed_array["last_inserted_id"] = $last_inserted_id;
		
		return $constructed_array;
					
	} else {
		//$eventLog->log("Error occurred during process. Please try again");						
			return $constructed_array;						
	}

}

function candidate_education_qualification_record_count($user_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	
	$candidate_education_qualification_record_count_sql = "SELECT COUNT(*) AS count FROM `candidate_rel_education_qualifications` WHERE `sm_memb_id`=:sm_memb_id";
	$candidate_education_qualification_record_count_select_query = $dbcon->prepare($candidate_education_qualification_record_count_sql);
	$candidate_education_qualification_record_count_select_query->bindValue(":sm_memb_id",$user_id_input);			
	$candidate_education_qualification_record_count_select_query->execute(); 
	
	if($candidate_education_qualification_record_count_select_query->rowCount() > 0) {
		$candidate_education_qualification_record_count_select_query_result = $candidate_education_qualification_record_count_select_query->fetch();
		$current_count = $candidate_education_qualification_record_count_select_query_result["count"];
		
		$constructed_array["current_count"] = $current_count;
		
	    return $constructed_array;
	
	} else {
		$constructed_array["current_count"] = 0;
		
		return $constructed_array;
		
	}//close of if($candidate_education_qualification_record_count_select_query->rowCount() > 0) {
	
	
}

function candidate_education_qualification_records_list($user_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	
	$candidate_education_qualification_records_list_sql = "SELECT * FROM `candidate_rel_education_qualifications` WHERE `sm_memb_id`=:sm_memb_id";
	$candidate_education_qualification_records_list_select_query = $dbcon->prepare($candidate_education_qualification_records_list_sql);
	$candidate_education_qualification_records_list_select_query->bindValue(":sm_memb_id",$user_id_input);			
	$candidate_education_qualification_records_list_select_query->execute(); 
	
	if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
		$candidate_education_qualification_records_list_select_query_result = $candidate_education_qualification_records_list_select_query->fetchAll();
		return $candidate_education_qualification_records_list_select_query_result;
	
	}//close of if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
	return $constructed_array;
}

function delete_candidate_education_qualification_record($user_id_input, $candidate_degree_input, $candidate_field_of_study_input) {
	global $dbcon;
	
	$candidate_degree_input = '%'.$candidate_degree_input.'%';
	$candidate_field_of_study_input = '%'.$candidate_field_of_study_input.'%';
	
	$candidate_education_qualification_delete_sql = "DELETE FROM `candidate_education_qualifications` WHERE `sm_memb_id`=:sm_memb_id AND `degree` LIKE :degree AND `field_of_study` LIKE :field_of_study";
	$candidate_education_qualification_delete_query = $dbcon->prepare($candidate_education_qualification_delete_sql);
	$candidate_education_qualification_delete_query->bindValue(":sm_memb_id",$user_id_input);	
	$candidate_education_qualification_delete_query->bindValue(":degree",$candidate_degree_input);	
	$candidate_education_qualification_delete_query->bindValue(":field_of_study",$candidate_field_of_study_input);		
	$candidate_education_qualification_delete_query->execute(); 
	
	if ($candidate_education_qualification_delete_query->execute()) {
		return true;
		
	}//close of if ($candidate_education_qualification_delete_query->execute()) {
	
	return false;
	
}

function delete_candidate_education_qualification_record_based_on_ceq_id_input($ceq_id_input) {
	global $dbcon;
	
	$candidate_educational_qualification_record_delete_sql = "DELETE FROM `candidate_education_qualifications` WHERE `ceq_id`=:ceq_id";
	$candidate_educational_qualification_record_delete_query = $dbcon->prepare($candidate_educational_qualification_record_delete_sql);
	$candidate_educational_qualification_record_delete_query->bindValue(":ceq_id",$ceq_id_input);		
	if ($candidate_educational_qualification_record_delete_query->execute()) {
		return true;
		
	}//close of if ($candidate_professional_summary_record_delete_query->execute()) {
	
	return false;
	
}

function candidate_education_qualification_rel_file_upload_references_insert($user_id_input,$activity_ref_input,$activity_ref_id_input,$additional_document_ref_input,$original_filename_input,$generated_filename_input,$ea_extracted_jwt_token_user_type,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch){
    global $dbcon;
	
	$constructed_array = array();
	
	$candidate_education_qualification_rel_file_upload_references_insert_sql = "INSERT INTO `candidate_rel_all_uploaded_visa_documents`(`sm_memb_id`, `activity_ref`, `activity_ref_id`, `additional_document_ref`, `original_filename`, `generated_filename`, `added_by_user_type`, `added_by_user_id`, `added_date_time`, `added_date_time_epoch`) VALUES (:sm_memb_id,:activity_ref,:activity_ref_id,:additional_document_ref,:original_filename,:generated_filename,:added_by_user_type,:added_by_user_id,:added_date_time,:added_date_time_epoch)";
	$candidate_education_qualification_rel_file_upload_references_insert_q = $dbcon->prepare($candidate_education_qualification_rel_file_upload_references_insert_sql);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":sm_memb_id",$user_id_input);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":activity_ref",$activity_ref_input);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":activity_ref_id",$activity_ref_id_input);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":additional_document_ref",$additional_document_ref_input);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":original_filename",$original_filename_input);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":generated_filename",$generated_filename_input);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":added_by_user_type",$ea_extracted_jwt_token_user_type);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":added_by_user_id",$ea_extracted_jwt_token_sub);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":added_date_time",$event_datetime);
	$candidate_education_qualification_rel_file_upload_references_insert_q->bindValue(":added_date_time_epoch",$current_epoch);
	
	if ($candidate_education_qualification_rel_file_upload_references_insert_q->execute()) {
		$last_inserted_id = $dbcon->lastInsertId();			
		//$eventLog->log("record inserted successfully");
		
		$constructed_array["last_inserted_id"] = $last_inserted_id;
		
		return $constructed_array;
					
	} else {
		//$eventLog->log("Error occurred during process. Please try again");						
		return $constructed_array;						
	}//close of else of if ($candidate_education_qualification_rel_personal_info_insert_q->execute()) {
		
}

exit;
?>