<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$jsl_owned_by_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_owned_by']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_owned_by'], FILTER_SANITIZE_NUMBER_INT) : '');
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {


				
				if ($company_id_input == "") {
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company_id";
					$response['status_description'] = "missing company id .";
					
					$eventLog->log("Please provide all information.");
					
					
				} else if ($jsl_owned_by_input == "") {
			
					$response['data'] = array();
					$response['status'] = "missing-jsl-owned-by-id";
					$response['status_description'] = "missing JSL owned By Id.";
					
					$eventLog->log("Please provide all information.");
					
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else {
					//all inputs are valid
					$company_specific_jsl_classifications_list_info_result = company_specific_jsl_classifications_list_info($company_id_input,$jsl_owned_by_input);
					if (count($company_specific_jsl_classifications_list_info_result) > 0) {
						
						$response['data'] = $company_specific_jsl_classifications_list_info_result;
						$response['status'] = "company-specific-jsl-classifications-list-successfully-fetched";
						$response['status_description'] = "Company specific JSL classifications list Successfully Received";
						
					} else {
						
						$response['data'] = array();
						$response['status'] = "no-jsl-classifications-set-by-this-company";
						$response['status_description'] = "Company specific JSL classifications list not defined Yet";

                    }					
					
				}
					
			}
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function company_specific_jsl_classifications_list_info($company_id_input,$jsl_owned_by_input) {
    global $dbcon;
	$constructed_array = array();
	$is_active_status = '1';
	
	$company_specific_jsl_classifications_list_info_sql = "SELECT * FROM `jsl_classification_details` jcd JOIN company_specific_jsl_classification_choices csjc USING (jsl_classification_detail_id) WHERE company_id = :company_id AND jsl_owned_by = :jsl_owned_by AND csjc.is_active_status = :is_active_status";
	$company_specific_jsl_classifications_list_info_sql_query = $dbcon->prepare($company_specific_jsl_classifications_list_info_sql);	
	$company_specific_jsl_classifications_list_info_sql_query->bindValue(":company_id",$company_id_input);
	$company_specific_jsl_classifications_list_info_sql_query->bindValue(":jsl_owned_by",$jsl_owned_by_input);
	$company_specific_jsl_classifications_list_info_sql_query->bindValue(":is_active_status",$is_active_status);
	$company_specific_jsl_classifications_list_info_sql_query->execute(); 
	
	if($company_specific_jsl_classifications_list_info_sql_query->rowCount() > 0) {
		$company_specific_jsl_classifications_list_info_sql_query_result = $company_specific_jsl_classifications_list_info_sql_query->fetchAll();
		foreach($company_specific_jsl_classifications_list_info_sql_query_result as $company_specific_jsl_classifications_list_info_sql_query_result_row) {
			
			$temp_array = array();	

			$temp_array["company_specific_jsl_classification_choice_id"] = $company_specific_jsl_classifications_list_info_sql_query_result_row["company_specific_jsl_classification_choice_id"];
			$temp_array["company_id"] = $company_specific_jsl_classifications_list_info_sql_query_result_row["company_id"];
			$temp_array["jsl_owned_by"] = $company_specific_jsl_classifications_list_info_sql_query_result_row["jsl_owned_by"];
			
			$temp_array["jsl_classification_detail_id"] = $company_specific_jsl_classifications_list_info_sql_query_result_row["jsl_classification_detail_id"];
			$temp_array["jsl_classification_detail_name"] = $company_specific_jsl_classifications_list_info_sql_query_result_row["jsl_classification_detail_name"];
			$temp_array["jsl_classification_detail_seo_name"] = $company_specific_jsl_classifications_list_info_sql_query_result_row["jsl_classification_detail_seo_name"];
			$temp_array["jsl_classification_detail_description"] = $company_specific_jsl_classifications_list_info_sql_query_result_row["jsl_classification_detail_description"];
			$temp_array["jsl_classification_detail_description_safe_html"] = $company_specific_jsl_classifications_list_info_sql_query_result_row["jsl_classification_detail_description_safe_html"];
			 
			$temp_array["jsl_candidate_email_notification_setting"] = $company_specific_jsl_classifications_list_info_sql_query_result_row["jsl_candidate_email_notification_setting"];
			$temp_array["jsl_cen_job_specific_override_allowance"] = $company_specific_jsl_classifications_list_info_sql_query_result_row["jsl_cen_job_specific_override_allowance"];
			$temp_array["jsl_candidate_sms_notification_setting"] = $company_specific_jsl_classifications_list_info_sql_query_result_row["jsl_candidate_sms_notification_setting"];
			$temp_array["jsl_csn_job_specific_override_allowance"] = $company_specific_jsl_classifications_list_info_sql_query_result_row["jsl_csn_job_specific_override_allowance"];
			$temp_array["jsl_recruiter_email_notification_setting"] = $company_specific_jsl_classifications_list_info_sql_query_result_row["jsl_recruiter_email_notification_setting"];
			$temp_array["jsl_ren_job_specific_override_allowance"] = $company_specific_jsl_classifications_list_info_sql_query_result_row["jsl_ren_job_specific_override_allowance"];
			$temp_array["jsl_recruiter_sms_notification_setting"] = $company_specific_jsl_classifications_list_info_sql_query_result_row["jsl_recruiter_sms_notification_setting"];
			$temp_array["jsl_rsn_job_specific_override_allowance"] = $company_specific_jsl_classifications_list_info_sql_query_result_row["jsl_rsn_job_specific_override_allowance"];
			
			$constructed_array[] = $temp_array;
		}
		
	
	     return $constructed_array;
	
	}
	return $constructed_array;
}


exit;
?>