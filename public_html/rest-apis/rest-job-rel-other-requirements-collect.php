<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_client_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['job_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['ot'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ot'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ot'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['job_references'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_references'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_references'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['travel'])) {
						$content .= $ea_received_rest_ws_raw_array_input['travel'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['travel'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['drug_test'])) {
						$content .= $ea_received_rest_ws_raw_array_input['drug_test'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['drug_test'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['background_check'])) {
						$content .= $ea_received_rest_ws_raw_array_input['background_check'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['background_check'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['security_clearance'])) {
						$content .= $ea_received_rest_ws_raw_array_input['security_clearance'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['security_clearance'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
			
				$company_client_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_client_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_client_id'], FILTER_SANITIZE_NUMBER_INT) : '');
			
				$job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
			    
				$ot_input = trim(isset($ea_received_rest_ws_raw_array_input['ot']) ? filter_var($ea_received_rest_ws_raw_array_input['ot'], FILTER_SANITIZE_NUMBER_INT) : '');
				if($ot_input == true)
				{
					$ot_input = 1;
				}
				if($ot_input == false){
					
					$ot_input = 0;
		        }
				
				$job_references_input = trim(isset($ea_received_rest_ws_raw_array_input['job_references']) ? filter_var($ea_received_rest_ws_raw_array_input['job_references'], FILTER_SANITIZE_NUMBER_INT) : '');
				if($job_references_input == true)
				{
					$job_references_input = 1;
				}
				if($job_references_input == false){
					
					$job_references_input = 0;
		        }
				
				$travel_input = trim(isset($ea_received_rest_ws_raw_array_input['travel']) ? filter_var($ea_received_rest_ws_raw_array_input['travel'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				if($travel_input == true)
				{
					$travel_input = 1;
				}
				if($travel_input == false){
					
					$travel_input = 0;
		        }
				
				$drug_test_input = trim(isset($ea_received_rest_ws_raw_array_input['drug_test']) ? filter_var($ea_received_rest_ws_raw_array_input['drug_test'], FILTER_SANITIZE_NUMBER_INT) : '');
				if($drug_test_input == true)
				{
					$drug_test_input = 1;
				}
				if($drug_test_input == false){
					
					$drug_test_input = 0;
		        }
				
				$background_check_input = trim(isset($ea_received_rest_ws_raw_array_input['background_check']) ? filter_var($ea_received_rest_ws_raw_array_input['background_check'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				if($background_check_input == true)
				{
					$background_check_input = 1;
				}
				if($background_check_input == false){
					
					$background_check_input = 0;
		        }
				
				$security_clearance_input = trim(isset($ea_received_rest_ws_raw_array_input['security_clearance']) ? filter_var($ea_received_rest_ws_raw_array_input['security_clearance'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				if($security_clearance_input == true)
				{
					$security_clearance_input = 1;
				}
				if($security_clearance_input == false){
					
					$security_clearance_input = 0;
		        }
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				if ($company_id_input == "") {
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "missing company id";
					
					$eventLog->log("missing-company-id: Please provide a valid Company Id.");
				
				} else if ($company_client_id_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-client-id";
					$response['status_description'] = "Missing Company Client Id";
					
					$eventLog->log("missing-company-client-id: Please provide a valid Company Client Id.");
					
				} else if ($job_id_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-id";
					$response['status_description'] = "Missing Job Id";
					
					$eventLog->log("missing-job-id: Please provide a valid Job Id.");
					
				} else if ($ot_input != "0" && $ot_input != "1") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-ot-input";
					$response['status_description'] = "Invalid OT input";
					
					$eventLog->log("invalid-ot-input: Please provide a valid OT input.");
					
				} else if ($job_references_input != "0" && $job_references_input != "1") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-job-references-input";
					$response['status_description'] = "Invalid Job References Input";
					
					$eventLog->log("invalid-job-references-input: Please provide a valid Job References Input.");
					
				} else if ($travel_input != "0" && $travel_input != "1") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-travel-input";
					$response['status_description'] = "Invalid Travel Input";
					
					$eventLog->log("invalid-travel-input: Please provide a valid Travel Input.");
					
				} else if ($drug_test_input != "0" && $drug_test_input != "1") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-drug-test-input";
					$response['status_description'] = "Invalid Drug Test Input";
					
					$eventLog->log("invalid-drug-test-input: Please provide a valid Drug Test Input.");
					
				} else if ($background_check_input != "0" && $background_check_input != "1") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-background-check-input";
					$response['status_description'] = "Invalid Background Check Input";
					
					$eventLog->log("invalid-background-check-input: Please provide a valid Background Check Input.");
					
				} else if ($security_clearance_input != "0" && $security_clearance_input != "1") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-security-clearance-input";
					$response['status_description'] = "Invalid Security Clearance Input";
					
					$eventLog->log("invalid-security-clearance-input: Please provide a valid Security Clearance Input.");
					
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					
						//Event Time, as per Indian Standard Time
						$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
						
						$candidate_add_next_step = "";
						
						if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$candidate_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$candidate_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
						} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
						}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
		
						if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							try {
								
								$job_rel_other_requirements_duplicate_check_result = job_rel_other_requirements_duplicate_check($job_id_input);
								
								
								if(count($job_rel_other_requirements_duplicate_check_result) > 0) {
									
									$job_rel_other_requirements_update_result = job_rel_other_requirements_update($company_id_input,$company_client_id_input,$job_id_input,$ot_input,$job_references_input,$travel_input,$drug_test_input,$background_check_input,$security_clearance_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
									
									if ($job_rel_other_requirements_update_result == true) {
										
										    $response['data'] = array();
											$response['status'] = "job-rel-other-requirements-updated-successfully";
											$response['status_description'] = "Job rel other requirements updated Successfully.";
										
											$eventLog->log("job-rel-other-requirements-updated-successfully:Job rel other requirements updated Successfully.");

											
										
									} else {
										//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
										$response['data'] = array();
										$response['status'] = "job-rel-other-requirements-updation-error";
										$response['status_description'] = "Error occurred when updating the job rel other requirements.";
										
										$eventLog->log("Error occurred when updating the job rel other requirements.");
										
									}
									
									
									
								} else {
									
									$last_inserted_id = job_rel_other_requirements_insert($company_id_input,$company_client_id_input,$job_id_input,$ot_input,$job_references_input,$travel_input,$drug_test_input,$background_check_input,$security_clearance_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
								
									$eventLog->log($last_inserted_id);
								
									
									if ($last_inserted_id != "") {
										
										    $response['data'] = array();
											$response['status'] = "job-rel-other-requirements-inserted-successfully";
											$response['status_description'] = "Job rel other requirements inserted Successfully.";
										
											$eventLog->log("job-rel-other-requirements-inserted-successfully:Job rel other requirements inserted Successfully.");

											
										
									} else {
										//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
										$response['data'] = array();
										$response['status'] = "job-rel-other-requirements-insertion-error";
										$response['status_description'] = "Error occurred when inserting the job rel other requirements.";
										
										$eventLog->log("Error occurred when inserting the job rel other requirements.");
										
									}
									
									
								}
								
								
								
									
							         
									
								
							} catch (Exception $e) {
								
								$response['data'] = array();
								$response['status'] = "job-rel-other-requirements-insertion-error";
								$response['status_description'] = "Error occurred when inserting the job rel other requirements.";
								
								$eventLog->log("Error occurred when inserting the job rel other requirements.");
							}
						}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							
						
					
				}//close of else of if ($email_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
    
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
			
	
	}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function job_rel_other_requirements_duplicate_check($job_id_input) {
	
	global $dbcon;
	$constructed_array = array();

	$job_rel_other_requirements_check_sql = "SELECT * FROM `job_rel_other_requirements` WHERE `job_id` = :job_id";
	$job_rel_other_requirements_check_query = $dbcon->prepare($job_rel_other_requirements_check_sql);
	$job_rel_other_requirements_check_query->bindValue(":job_id",$job_id_input);
	$job_rel_other_requirements_check_query->execute();

	if($job_rel_other_requirements_check_query->rowCount() > 0) {
		$job_rel_other_requirements_check_query_result = $job_rel_other_requirements_check_query->fetch();
	     return $job_rel_other_requirements_check_query_result;

	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;

}
	
	
function job_rel_other_requirements_insert($company_id_input,$company_client_id_input,$job_id_input,$ot_input,$job_references_input,$travel_input,$drug_test_input,$background_check_input,$security_clearance_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch){	

    global $dbcon,$eventLog;
    $job_rel_other_requirements_insert_sql ="INSERT INTO `job_rel_other_requirements`(`company_id`, `company_client_id`, `job_id`, `ot`, `job_references`, `travel`, `drug_test`, `background_check`, `security_clearance`,`added_date_time`, `added_date_time_epoch`, `added_by`) VALUES (:company_id,:company_client_id,:job_id,:ot,:job_references,:travel,:drug_test,:background_check,:security_clearance,:added_date_time,:added_date_time_epoch,:added_by)";
	
	
	$eventLog->log("after query");
	$job_rel_other_requirements_insert_query = $dbcon->prepare($job_rel_other_requirements_insert_sql);
	$job_rel_other_requirements_insert_query->bindValue(":company_id",$company_id_input);
	$job_rel_other_requirements_insert_query->bindValue(":company_client_id",$company_client_id_input);
	$job_rel_other_requirements_insert_query->bindValue(":job_id",$job_id_input);
	$job_rel_other_requirements_insert_query->bindValue(":ot",$ot_input);
	$job_rel_other_requirements_insert_query->bindValue(":job_references",$job_references_input);
	$job_rel_other_requirements_insert_query->bindValue(":travel",$travel_input);
	$job_rel_other_requirements_insert_query->bindValue(":drug_test",$drug_test_input);
	$job_rel_other_requirements_insert_query->bindValue(":background_check",$background_check_input);
	$job_rel_other_requirements_insert_query->bindValue(":security_clearance",$security_clearance_input);
	$job_rel_other_requirements_insert_query->bindValue(":added_by",$ea_extracted_jwt_token_sub);
	$job_rel_other_requirements_insert_query->bindValue(":added_date_time",$event_datetime);
	$job_rel_other_requirements_insert_query->bindValue(":added_date_time_epoch",$current_epoch);


		if ($job_rel_other_requirements_insert_query->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			$eventLog->log("record inserted successfully");

			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}	

function job_rel_other_requirements_update($company_id_input,$company_client_id_input,$job_id_input,$ot_input,$job_references_input,$travel_input,$drug_test_input,$background_check_input,$security_clearance_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch){	

    global $dbcon,$eventLog;
    $job_rel_other_requirements_update_sql ="UPDATE `job_rel_other_requirements` SET `company_id`=:company_id,`company_client_id`=:company_client_id,`ot`=:ot,`job_references`=:job_references,`travel`=:travel,`drug_test`=:drug_test,`background_check`=:background_check,`security_clearance`=:security_clearance,`last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch,`last_updated_by`=:last_updated_by WHERE `job_id` =:job_id";
	
	
	
	
	$eventLog->log("after query");
	$job_rel_other_requirements_update_query = $dbcon->prepare($job_rel_other_requirements_update_sql);
	$job_rel_other_requirements_update_query->bindValue(":company_id",$company_id_input);
	$job_rel_other_requirements_update_query->bindValue(":company_client_id",$company_client_id_input);
	$job_rel_other_requirements_update_query->bindValue(":job_id",$job_id_input);
	$job_rel_other_requirements_update_query->bindValue(":ot",$ot_input);
	$job_rel_other_requirements_update_query->bindValue(":job_references",$job_references_input);
	$job_rel_other_requirements_update_query->bindValue(":travel",$travel_input);
	$job_rel_other_requirements_update_query->bindValue(":drug_test",$drug_test_input);
	$job_rel_other_requirements_update_query->bindValue(":background_check",$background_check_input);
	$job_rel_other_requirements_update_query->bindValue(":security_clearance",$security_clearance_input);
	$job_rel_other_requirements_update_query->bindValue(":last_updated_by",$ea_extracted_jwt_token_sub);
	$job_rel_other_requirements_update_query->bindValue(":last_updated_date_time",$event_datetime);
	$job_rel_other_requirements_update_query->bindValue(":last_updated_date_time_epoch",$current_epoch);


		if ($job_rel_other_requirements_update_query->execute()) {
			
			return true;

		} else {
			
			return false;
	    }

}	

exit;
?>