<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "12")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['sm_memb_id'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['sm_memb_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['sm_memb_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['sm_job_experience_level'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['sm_job_experience_level'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['sm_job_experience_level'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['experience_years'])) {  
						$content .= $ea_received_rest_ws_raw_array_input['experience_years'] . "\r\n"; 
					}//close of if (isset($ea_received_rest_ws_raw_array_input['experience_years']))
					
					if (isset($ea_received_rest_ws_raw_array_input['experience_months'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['experience_months'] . "\r\n"; 
					}//close of if (isset($ea_received_rest_ws_raw_array_input['experience_months'])) 

					if (isset($ea_received_rest_ws_raw_array_input['current_salary'])) {
						$content .= $ea_received_rest_ws_raw_array_input['current_salary'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['current_salary']))
					
					if (isset($ea_received_rest_ws_raw_array_input['preferred_salary'])) {
						$content .= $ea_received_rest_ws_raw_array_input['preferred_salary'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['preferred_salary']))
					
                    if (isset($ea_received_rest_ws_raw_array_input['on_notice_period'])) {
						$content .= $ea_received_rest_ws_raw_array_input['on_notice_period'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['on_notice_period']))
					
					if (isset($ea_received_rest_ws_raw_array_input['notice_period'])) {
						$content .= $ea_received_rest_ws_raw_array_input['notice_period'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['notice_period']))
					
				    if (isset($ea_received_rest_ws_raw_array_input['notice_period_metric'])) {
						$content .= $ea_received_rest_ws_raw_array_input['notice_period_metric'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['notice_period_metric']))
						
					if (isset($ea_received_rest_ws_raw_array_input['preferred_job_location'])) {
						$content .= $ea_received_rest_ws_raw_array_input['preferred_job_location'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['preferred_job_location']))	
						
				
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
						
					
					$eventLog->log("Received Inputs => ".$content);
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
					
				$sm_memb_id_input = trim(isset($ea_received_rest_ws_raw_array_input['sm_memb_id']) ? filter_var($ea_received_rest_ws_raw_array_input['sm_memb_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$sm_job_experience_level_input = trim(isset($ea_received_rest_ws_raw_array_input['sm_job_experience_level']) ? filter_var($ea_received_rest_ws_raw_array_input['sm_job_experience_level'], FILTER_SANITIZE_STRING) : '');
				$experience_years_input = trim(isset($ea_received_rest_ws_raw_array_input['experience_years']) ? filter_var($ea_received_rest_ws_raw_array_input['experience_years'], FILTER_SANITIZE_NUMBER_INT) : '');
				$experience_months_input = trim(isset($ea_received_rest_ws_raw_array_input['experience_months']) ? filter_var($ea_received_rest_ws_raw_array_input['experience_months'], FILTER_SANITIZE_NUMBER_INT) : '');
				$current_salary_input = trim(isset($ea_received_rest_ws_raw_array_input['current_salary']) ? filter_var($ea_received_rest_ws_raw_array_input['current_salary'], FILTER_SANITIZE_NUMBER_INT) : '');	
				$preferred_salary_input = trim(isset($ea_received_rest_ws_raw_array_input['preferred_salary']) ? filter_var($ea_received_rest_ws_raw_array_input['preferred_salary'], FILTER_SANITIZE_NUMBER_INT) : '');	
				$on_notice_period_input = trim(isset($ea_received_rest_ws_raw_array_input['on_notice_period']) ? filter_var($ea_received_rest_ws_raw_array_input['on_notice_period'], FILTER_SANITIZE_NUMBER_INT) : '');	
				$notice_period_input = trim(isset($ea_received_rest_ws_raw_array_input['notice_period']) ? filter_var($ea_received_rest_ws_raw_array_input['notice_period'], FILTER_SANITIZE_NUMBER_INT) : '');
                $notice_period_metric_input = trim(isset($ea_received_rest_ws_raw_array_input['notice_period_metric']) ? filter_var($ea_received_rest_ws_raw_array_input['notice_period_metric'], FILTER_SANITIZE_NUMBER_INT) : '');
				$preferred_job_location_input = trim(isset($ea_received_rest_ws_raw_array_input['preferred_job_location']) ? filter_var($ea_received_rest_ws_raw_array_input['preferred_job_location'], FILTER_SANITIZE_STRING) : '');
				
				if($preferred_job_location_input!="")
					
				{
					$preferred_job_location_input_exploded = explode(":::::",$preferred_job_location_input);
					
					$preferred_job_location_input_exploded_count = count($preferred_job_location_input_exploded);
					
					$city_name_input = "";
					$state_name_input = "";
					$country_name_input = "";
					

					if($preferred_job_location_input_exploded_count>0)
					{   
						//print_r($preferred_job_location_input_exploded);
						
						for($i=0;$i<$preferred_job_location_input_exploded_count;$i++){
							
							$specific_preferred_job_location_exploded = explode(",",$preferred_job_location_input_exploded[$i]);
							if (count($specific_preferred_job_location_exploded) == "3") {
								$city_name_input = trim(isset($specific_preferred_job_location_exploded[0]) ? filter_var($specific_preferred_job_location_exploded[0], FILTER_SANITIZE_STRING) : '');
								$state_name_input = trim(isset($specific_preferred_job_location_exploded[1]) ? filter_var($specific_preferred_job_location_exploded[1], FILTER_SANITIZE_STRING) : '');
								$country_name_input = trim(isset($specific_preferred_job_location_exploded[2]) ? filter_var($specific_preferred_job_location_exploded[2], FILTER_SANITIZE_STRING) : '');
							
							
						                       
							}
							
						}
					     
					}
				}          
				  			if (is_null($city_name_input)) {
									
						         $city_name_input = null;
					        }
							if (is_null($state_name_input)) {
									
						         $state_name_input = null;
					        }
							if (is_null($country_name_input)) {
									
						         $country_name_input = null;
					        }
					
					
					if($company_id_input == "")
						{  
							$company_id_input = null;
						}
					if($current_salary_input == "")
						{
							$current_salary_input = null;
						} 
                	
				$eventLog->log("sm_memb_id : " . $sm_memb_id_input);
				$eventLog->log("company_id : " . $company_id_input);
				$eventLog->log("sm_job_experience_level : " . $sm_job_experience_level_input);
				$eventLog->log("experience_years : " . $experience_years_input);
				$eventLog->log("experience_months : " . $experience_months_input);
				$eventLog->log("current_salary : " . $current_salary_input);
				$eventLog->log("preferred_salary : " . $preferred_salary_input);
				$eventLog->log("on_notice_period : " . $on_notice_period_input);
				$eventLog->log("notice_period : " . $notice_period_input);
				$eventLog->log("notice_period_metric : " . $notice_period_metric_input);
				$eventLog->log("preferred_job_location : " . $preferred_job_location_input);
				
				
				
				//$user_type_input = strtolower_utf8_extended( $user_type_input); 
				
				//$eventLog->log("company_id a : " . $company_id_input);
				//$eventLog->log("User Type After strtolower : " . $user_type_input);
				
				
				//check if the company_id, that is provided in the api request, belongs to a valid one or not
				$api_request_related_company_details_result = get_company_details_based_on_company_id($company_id_input);
				$api_request_related_company_details_result_count = count($api_request_related_company_details_result);
				$eventLog->log("company id count - " . $api_request_related_company_details_result_count);
				
								
						
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {							
				
				if ($company_id_input == "") {
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array(); 
					$response['status'] = "missing-company-id";
					$response['status_description'] = "missing company_id";
					$eventLog->log("missing-email-id: Please provide a valid company id.");
					
				} else if ($current_salary_input == "") {
					//missing Mobile Number scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-current-salary";
					$response['status_description'] = "missing current salary";
					
					$eventLog->log("missing-current-salary: Please provide current salary.");
					
				
				} else if ($on_notice_period_input == 1) {
					if(($notice_period_input == "")||($notice_period_metric_input == "")){
					//missing Salutation scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-notice-period-details";
					$response['status_description'] = "misssing notice period details";
					
					$eventLog->log("missing-notice-period-details: Please provide notice period detils.");
					}
				} else if ($experience_years_input == "") {
					//missing Salutation scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-experience-years";
					$response['status_description'] = "misssing experience years";
					
					$eventLog->log("missing-experience-years: Please provide a valid experience years value.");
					
				} else if ($experience_months_input == "") {
					//missing Salutation scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-experience-months";
					$response['status_description'] = "misssing experience Months";
					
					$eventLog->log("missing-experience-months: Please provide a valid experience months value.");
				
				} else if ($sm_memb_id_input == "") {
					//missing First name scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-user-id";
					$response['status_description'] = "missing user id";
					
					$eventLog->log("missing-user-id: Please provide a valid user id.");
				
				} else if ($ip_address_input == "") {
					////missing additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Missing Some Additional Information";
					
					$eventLog->log("missing-some-additional-information: Please provide all information.");
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					//duplicate check  in candidate
				
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);

					
					if($preferred_salary_input == 0)
					{
					   $preferred_salary_input = null;
					}
					if($on_notice_period_input == 0)
					{
					   $notice_period_input = null;
					   $notice_period_metric_input = null;
					}
					
						//Event Time, as per Indian Standard Time
						$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
						
						$candidate_add_next_step = "";
						
						$eventLog->log("before proceed to add.");
						if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$candidate_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else if ((isset($ea_extracted_jwt_token_user_company_id)) /*&& ($company_id_input == $ea_extracted_jwt_token_user_company_id)*/) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$candidate_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
						} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
						}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
						
							$eventLog->log("before proceed to  equal condition of next step.");
						if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							try {
								
								$eventLog->log("enter into try.");
						            $candidate_additional_information_update_result = candidate_additional_information_update($sm_memb_id_input,$company_id_input,$sm_job_experience_level_input,$experience_years_input,$experience_months_input,$current_salary_input,$preferred_salary_input,$on_notice_period_input,$notice_period_input,$notice_period_metric_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
								
									$eventLog->log($candidate_additional_information_update_result);
									$eventLog->log("before enter into if preferred job location");
									if($preferred_job_location_input > 0) {
								         $eventLog->log("before duplicate check result");
										$candidate_preferred_job_location_record_duplicate_check_result = candidate_preferred_job_location_record_duplicate_check($sm_memb_id_input,$company_id_input,$city_name_input);
									$eventLog->log("after duplicate check");
									    if (count($candidate_preferred_job_location_record_duplicate_check_result) > 0) {
									
											$eventLog->log("before last insterted id empty");
									
											//Construct Content, that will be sent in Response body, of the REST Web Service
											$response['data'] = array();
											$response['status'] = "candidate-preffered-location-detils-already-exists";
											$response['status_description'] = "A candidate already exists with this information";
								
											 $eventLog->log("candidate-already-exists: candidate already exists");
											
										} else {
										
										
											$candidate_preferred_location_last_inserted_id = candidate_preferred_location_record_insert($sm_memb_id_input,$company_id_input,$city_name_input,$state_name_input,$country_name_input,$event_datetime,$current_epoch);
										
											
											if(($candidate_additional_information_update_result == true)&&($candidate_preferred_location_last_inserted_id != "")) {
										
												$response['data'] = array();
												$response['status'] = "user-additional-details-inserted-successfully";
												$response['status_description'] = "user additional details inserted Successfully.";
											
												$eventLog->log("user-additional-details-insert-successfully:user additional details inserted Successfully.");   
										
											} else {
												//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
												$response['data'] = array();
												$response['status'] = "user-additional-details-inserted-error";
												$response['status_description'] = "Error occurred when inserting the user details into the Database.";
												
												$eventLog->log("user-additional-details-insert-error: There is an error, when inserting the user details into the Database.");
												
											}
									
									    }
									}/*  else {
										
										if(($candidate_additional_information_update_result == true)) {
										
												$response['data'] = array();
												$response['status'] = "user-additional-details-inserted-successfully";
												$response['status_description'] = "user additional details inserted Successfully.";
											
												$eventLog->log("user-additional-details-insert-successfully:user additional details inserted Successfully.");   
										}
									} */
						            
							         
									
								
							} catch (Exception $e) {
								
								//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
								$response['data'] = array();
								$response['status'] = "user-additional-details-inserted-error";
								$response['status_description'] = "Error occurred when inserting the user details into the Database.";
								
								$eventLog->log("user-additional-details-insert-error: There is an error, when inserting the user details into the Database.");
							}
						}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							
						
					
				}//close of else of if ($email_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
    
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
			
	
	}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	

function candidate_additional_information_update($sm_memb_id_input,$company_id_input,$sm_job_experience_level_input,$experience_years_input,$experience_months_input,$current_salary_input,$preferred_salary_input,$on_notice_period_input,$notice_period_input,$notice_period_metric_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch) {
    global $dbcon,$eventLog;
    $candidate_additional_information_update_sql = "UPDATE `candidate_rel_additional_info` SET `company_id`=:company_id,`sm_job_experience_level`=:sm_job_experience_level,`experience_years`=:experience_years,`experience_months`=:experience_months,`current_salary`=:current_salary,`preferred_salary`=:preferred_salary,`on_notice_period`=:on_notice_period,`notice_period`=:notice_period,`notice_period_metric`=:notice_period_metric,`last_updated_by_user_id`=:last_updated_by_user_id,`last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch WHERE `sm_memb_id` =:sm_memb_id";
	
	$candidate_additional_information_update_q = $dbcon->prepare($candidate_additional_information_update_sql);
	$candidate_additional_information_update_q->bindValue(":sm_memb_id",$sm_memb_id_input);
	$candidate_additional_information_update_q->bindValue(":company_id",$company_id_input);
	$candidate_additional_information_update_q->bindValue(":sm_job_experience_level",$sm_job_experience_level_input);
	$candidate_additional_information_update_q->bindValue(":experience_years",$experience_years_input);$candidate_additional_information_update_q->bindValue(":experience_months",$experience_months_input);$candidate_additional_information_update_q->bindValue(":current_salary",$current_salary_input);
	$candidate_additional_information_update_q->bindValue(":preferred_salary",$preferred_salary_input);
	$candidate_additional_information_update_q->bindValue(":on_notice_period",$on_notice_period_input);
	$candidate_additional_information_update_q->bindValue(":notice_period",$notice_period_input);
	$candidate_additional_information_update_q->bindValue(":notice_period_metric",$notice_period_metric_input);
	$candidate_additional_information_update_q->bindValue(":last_updated_by_user_id",$ea_extracted_jwt_token_sub);
	$candidate_additional_information_update_q->bindValue(":last_updated_date_time",$event_datetime);
	$candidate_additional_information_update_q->bindValue(":last_updated_date_time_epoch",$current_epoch);

		
	if ($candidate_additional_information_update_q->execute()) {

			return true;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return false;
	    }

}	
function candidate_preferred_job_location_record_duplicate_check($sm_memb_id_input,$company_id_input,$city_name_input){
	

	
	global $dbcon;
	$constructed_array = array();
	
	$candidate_preferred_job_location_record_duplicate_check_sql = "SELECT * FROM `candidate_rel_preferred_job_locations` WHERE sm_memb_id = :sm_memb_id AND company_id = :company_id AND city_name = :city_name ";
	$candidate_preferred_job_location_record_duplicate_check_q = $dbcon->prepare($candidate_preferred_job_location_record_duplicate_check_sql);
	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":sm_memb_id",$sm_memb_id_input);
    $candidate_preferred_job_location_record_duplicate_check_q->bindValue(":company_id",$company_id_input);
	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":city_name",$city_name_input);
    
	$candidate_preferred_job_location_record_duplicate_check_q->execute();

	if($candidate_preferred_job_location_record_duplicate_check_q->rowCount() > 0) {
		$candidate_preferred_job_location_record_duplicate_check_q_result = $candidate_preferred_job_location_record_duplicate_check_q->fetch();
	     return $candidate_preferred_job_location_record_duplicate_check_q_result;

	}//close of if($candidate_preferred_location_record_duplicate_check_q_result->rowCount() > 0) {
	return $constructed_array;

}
function candidate_preferred_location_record_insert($sm_memb_id_input,$company_id_input,$city_name_input,$state_name_input,$country_name_input,$event_datetime,$current_epoch) {
	global $dbcon;
	$is_active_status = '1';
	candidate_preferred_location_record_insert($sm_memb_id_input,$company_id_input,$city_name_input,$state_name_input,$country_name_input,$event_datetime,$current_epoch);
	
	$candidate_preferred_location_details_sql = "INSERT INTO `candidate_rel_preferred_job_locations`(`sm_memb_id`, `company_id`, `city_name`, `state_name`, `country_name`, `added_date_time`, `added_date_time_epoch`, `is_active_status`) VALUES (:sm_memb_id,:company_id,:city_name,:state_name,:country_name,:added_date_time,:added_date_time_epoch,:is_active_status)";
	
	$candidate_preferred_location_details_q = $dbcon->prepare($candidate_preferred_location_details_sql);
	$candidate_preferred_location_details_q->bindValue(":sm_memb_id",$sm_memb_id_input);
	$candidate_preferred_location_details_q->bindValue(":company_id",$company_id_input);
	$candidate_preferred_location_details_q->bindValue(":city_name",$city_name_input);
	$candidate_preferred_location_details_q->bindValue(":state_name",$state_name_input);
	$candidate_preferred_location_details_q->bindValue(":country_name",$country_name_input);
    $candidate_preferred_location_details_q->bindValue(":added_date_time",$event_datetime);
	$candidate_preferred_location_details_q->bindValue(":added_date_time_epoch",$current_epoch);
	$candidate_preferred_location_details_q->bindValue(":is_active_status",$is_active_status);


		if ($candidate_preferred_location_details_q->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");
			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }
}

exit;
?>