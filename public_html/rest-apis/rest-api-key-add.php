<?php 
defined('START') or die; 
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */
$eventLogFileName = $route_filename . "-log";  
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "2"))
	{
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) 
	{
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {  
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					
					
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) { 
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if(isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				//  to filter the raw input by using trim , isset() ,FILTER_SANITIZE methods & terminal operator
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				
				
		       if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4))
					{
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                
				
				//Check if all inputs are received correctly from the REST Web Service
				//misiing  company id scenario
				if ($company_id_input == "") {
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array(); 
					$response['status'] = "missing-company-id";
					$response['status_description'] = "missing company id";
					$eventLog->log("missing_company_id: Please provide a valid company id.");
					
				}  else if ($ip_address_input == "") {
					//missing client company name scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "missing ip address";
					
					$eventLog->log("missing-ip-address: Please provide a valid ip address.");
					
				}  else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//  else {
						
						//Event Time, as per Indian Standard Time
						$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
					//	$event_expiry_datetime_epoch = $current_epoch+$password_resetting_link_expiry_period;	
						//$valid_from_date_input = '2018-11-10 16:18:20';
						
						
						
							try {
								
								//Check if the User, whose sm_memb_id is received from "sub" of JWT Auth Token exists
						$eventLog->log("before duplicate check.");
						$client_company_api_duplicate_check_result = C_duplicate_check_based_on_company_id_and_is_active_status($company_id_input);

					$eventLog->log("after duplicate check.");
					
				if (count($client_company_api_duplicate_check_result) > 0)
						{
							
					$client_company_api_update_check_result=C_update_based_on_company_id_and_is_active_status($company_id_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);

						$response['data'] = array();
						$response['status'] = "company-api-key-updated-successfully";
						$response['status_description'] = "A company api key already exists";
						
                        $eventLog->log("company-api-key-updated-successfully: company api key updated successfully");									
					
					}	else {	

							$last_inserted_id = company_basic_is_insert($company_id_input,$event_datetime,$current_epoch);

						$response['data'] = array();
						$response['status'] = "company-api-key-inserted-successfully";
						$response['status_description'] = "A company api key inserted successfully";
						
                        $eventLog->log("company-api-key-inserted-successfully: user inserted successfully");		
								} //close of else of if (count($ea_token_based_sub_user_id_details) > 0) {
								
								
								
								
								
								
							
								
							} catch (Exception $e) {
								
								//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
								$response['data'] = array();
								$response['status'] = "company-api-key-insertion-error";
								$response['status_description'] = "company api key insertion error.";
								
								$eventLog->log("company-api-key-insertion-error: There is an error, when adding the client Company in the Database.");
							}
						//}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							
						
					//}
				}//close of else of if ($email_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
    
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
			
	
	}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "7")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	function C_duplicate_check_based_on_company_id_and_is_active_status($company_id_input){
	global $dbcon;
	$constructed_array = array();
	$is_active_status= "1";
	$candidate_preferred_job_location_record_duplicate_check_sql = "SELECT * FROM `company_rel_api_keys` WHERE `company_id` = :company_id AND `is_active_status` = :is_active_status";

	$candidate_preferred_job_location_record_duplicate_check_q = $dbcon->prepare($candidate_preferred_job_location_record_duplicate_check_sql);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":company_id",$company_id_input);

	$candidate_preferred_job_location_record_duplicate_check_q->bindValue(":is_active_status",$is_active_status);

    
	$candidate_preferred_job_location_record_duplicate_check_q->execute();
  
	if($candidate_preferred_job_location_record_duplicate_check_q->rowCount() > 0) {
		$candidate_preferred_job_location_record_duplicate_check_q_result = $candidate_preferred_job_location_record_duplicate_check_q->fetch();
	     return $candidate_preferred_job_location_record_duplicate_check_q_result;
	    // echo $candidate_preferred_job_location_record_duplicate_check_q_result;

	} //close of if($candidate_preferred_location_record_duplicate_check_q_result->rowCount() > 0) {
	return $constructed_array;


}

function C_update_based_on_company_id_and_is_active_status($company_id_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch) {

 global $dbcon,$eventLog;
       $is_active_status='0';

    $candidate_additional_information_update_sql = "UPDATE `company_rel_api_keys` SET `is_active_status`=:is_active_status,`last_updated_datetime`=:last_updated_datetime,`last_updated_datetime_epoch`=:last_updated_datetime_epoch,`last_updated_by_sm_memb_id`=:last_updated_by_sm_memb_id WHERE `company_id` =:company_id";

	
	$candidate_additional_information_update_q = $dbcon->prepare($candidate_additional_information_update_sql);
	
	$candidate_additional_information_update_q->bindValue(":company_id",$company_id_input);

	$candidate_additional_information_update_q->bindValue(":is_active_status",$is_active_status);

	
	$candidate_additional_information_update_q->bindValue(":last_updated_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$candidate_additional_information_update_q->bindValue(":last_updated_datetime",$event_datetime);
	$candidate_additional_information_update_q->bindValue(":last_updated_datetime_epoch",$current_epoch);

		
	if ($candidate_additional_information_update_q->execute()) {

			return true;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return false;
	    }

}

function company_basic_is_insert($company_id_input,$event_datetime,$current_epoch) {
global $dbcon;
	$is_active_status = '1';
	$uuid4 = Uuid::uuid4();
	$api_key_input=$uuid4->toString();
	//$api_key_input="4";
	 $api_secret_input="null";
	 $is_shown_in_panel="0";
	$candidate_preferred_location_details_sql = "INSERT INTO `company_rel_api_keys`( `company_id`, `api_key`, `api_secret`,`is_shown_in_panel`,`added_datetime`, `added_datetime_epoch`, `is_active_status`) VALUES (:company_id,:api_key,:api_secret,:is_shown_in_panel,:added_datetime,:added_datetime_epoch,:is_active_status)";

	
	$candidate_preferred_location_details_q = $dbcon->prepare($candidate_preferred_location_details_sql);
	
	$candidate_preferred_location_details_q->bindValue(":company_id",$company_id_input);
	$candidate_preferred_location_details_q->bindValue(":api_key",$api_key_input);
	$candidate_preferred_location_details_q->bindValue(":api_secret",$api_secret_input);
	$candidate_preferred_location_details_q->bindValue(":is_shown_in_panel",$is_shown_in_panel);
	
    $candidate_preferred_location_details_q->bindValue(":added_datetime",$event_datetime);

	$candidate_preferred_location_details_q->bindValue(":added_datetime_epoch",$current_epoch);

	$candidate_preferred_location_details_q->bindValue(":is_active_status",$is_active_status);

       // $candidate_preferred_location_details_q->execute();
		if ($candidate_preferred_location_details_q->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");
			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }
}
exit;
?>