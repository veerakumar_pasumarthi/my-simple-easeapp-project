<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * REST API for reset password
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "2")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['user_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['user_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['user_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//Check if all inputs are received correctly from the REST Web Service
				if ($user_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-user-id";
					$response['status_description'] = "Missing User ID";
					
					$eventLog->log("missing-user-id: Please provide a valid User ID.");
					
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Missing Some Additional Information";
					
					$eventLog->log("missing-some-additional-information: Please provide all information.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					$ea_action_specific_auth_check_result = "";
					
					//User Details, based on User ID input, through api request
					$user_basic_details_result = user_basic_details_check_based_on_user_id($user_id_input);
					
					if (count($user_basic_details_result) > 0) {
						
						//$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
						$ea_action_specific_auth_check_result = ea_check_action_specific_user_privileges_on_different_user_levels($ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_user_company_id, $ea_extracted_jwt_token_sub, $user_id_input, $ea_extracted_jwt_token_user_privileges_list, $user_basic_details_result["company_id"], $page_filename);					
						
						//echo "ea_action_specific_auth_check_result: " . $ea_action_specific_auth_check_result;					
							
							
						

						
						if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
								
								$user_specific_request_recipient_email = "";
								$request_destination_input = "primary-email";
								
								//Primary Email scenario
								$user_specific_request_recipient_email = $user_basic_details_result["sm_email"];
								
								if (($user_specific_request_recipient_email != "") && ($request_destination_input)) {
									
										
									//Event Time, as per Indian Standard Time
									$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
									$event_expiry_datetime_epoch = $current_epoch+$password_resetting_link_expiry_period;
									$request_purpose = "re-send-activation-email";
									$last_generated_password_setup_mod_request_result = ea_generate_user_password_setup_modification_request_ref_code($ea_extracted_jwt_token_sub, $user_id_input, $request_destination_input, $user_specific_request_recipient_email, $event_datetime, $current_epoch, $event_expiry_datetime_epoch,$request_purpose);
									
									if ((isset($last_generated_password_setup_mod_request_result["last_inserted_id"])) && (isset($last_generated_password_setup_mod_request_result["password_request_code"]))) {
												
										$last_registered_password_setup_mod_request_ref_id = $last_generated_password_setup_mod_request_result["last_inserted_id"];
										$last_registered_password_setup_mod_request_ref_email_activation_code = $last_generated_password_setup_mod_request_result["password_request_code"];
										
										//EMAIL SENDING CODE 
										//http://127.0.0.1:8080/LAYOUT-2/STANDARD/#!/account/reset-password/user-pass-reset-code/yd665r5er65r56edd6565te65e56e56e56ter6er5775e57dgdfg4t45gdfgfd
										
										/*
										//Recipients
										$phpmailer_smtp->setFrom('notifications@vdmail.securitywonks.net', 'Mailer');
										$phpmailer_smtp->addAddress('raghuveer.dendukuri@gmail.com', 'raghuveer dendukuri');     // Add a recipient
										$phpmailer_smtp->addAddress('webmaster@securitywonks.org');               // Name is optional
										$phpmailer_smtp->addReplyTo('notifications@vdmail.securitywonks.net', 'Information');
										//$phpmailer_smtp->addCC('cc@example.com');
										//$phpmailer_smtp->addBCC('bcc@example.com');

										//Attachments
										//$phpmailer_smtp->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
										//$phpmailer_smtp->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

										//Content
										$phpmailer_smtp->isHTML(true);                                  // Set email format to HTML
										$phpmailer_smtp->Subject = 'Visadoc Project - User Account Activation Email';
										$phpmailer_smtp->Body    = '<b>Thanks for your interest in registering your account</b>';
										$phpmailer_smtp->AltBody = 'Thanks for your interest in registering your account';

										$phpmailer_smtp->send();
										echo 'Message has been sent';
										*/
										
										//echo "THE SUBJECT RELATED DATA CAN BE CHANGED BY THE ACTION TAKER<br>";
										//Construct Content, that will be sent in Response body, of the REST Web Service
										$response['data'] = array();
										$response['status'] = "activation-email-request-registered";
										$response['status_description'] = "Account Activation Email Request is registered, and will be delivered shortly, to respective User's Email Address.";
										
										$eventLog->log("activation-email-request-registered: Account Activation Email Request is registered, and will be delivered shortly, to respective User's Email Address.");	
										
									} else {
										
										//Construct Content, that will be sent in Response body, of the REST Web Service
										$response['data'] = array();
										$response['status'] = "account-activation-email-request-ref-code-generation-error";
										$response['status_description'] = "Error noticed when generating account activation related Request Ref Code";
										
										$eventLog->log("account-activation-email-request-ref-code-generation-error: Error noticed when generating account activation related Request Ref Code, please check and try again.");	
										
										
									}//close of else of if ((isset($last_generated_password_setup_mod_request_result["last_inserted_id"])) && (isset($last_generated_password_setup_mod_request_result["password_request_code"]))) {
									
									
									
								} else {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "missing-recipient-email-error";
									$response['status_description'] = "Error occurred as recipient user's email is missing";
									
									$eventLog->log("missing-recipient-email-error: Error occurred as recipient user's email is missing, please check and try again.");	
									
								}//close of else of if (($user_specific_request_recipient_email != "") && ($request_destination_input)) {
									
							} catch (Exception $e) {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "account-activation-email-request-initiating-error";
								$response['status_description'] = "Error occurred when registering the Account Activation Email Request";
								
								$eventLog->log("account-activation-email-request-initiating-error: Error occurred when registering the Account Activation Email Request, w.r.t. particular User's Email Address.");	
								
							}
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-subject-platform-scope-user";
							$response['status_description'] = "Insufficient Set of Permissions, to complete this action";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-subject-platform-scope-user: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-different-company-user-id";
							$response['status_description'] = "Insufficient Set of Permissions, as the User belongs to a different company";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-INSUFFICIENT-PERMISSIONS") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions, as this process will not be allowed any further";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-self-action";
							$response['status_description'] = "Insufficient Set of Permissions, as self action is Dis-allowed";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-disallowed-self-action: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						}//close of if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
						
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "Invalid User Account, that is attempted to be edited";
						
						$eventLog->log("invalid-user-account: User Account, that is attempted to be edited, is invalid, please check and try again.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
					
				}//close of else of if ($user_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token";
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");	
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input";
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "2")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){

exit;
?>