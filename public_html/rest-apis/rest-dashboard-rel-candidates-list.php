<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Companies, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs
			
				/* $company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : ''); */
				$page_number_input= trim(isset($ea_received_rest_ws_raw_array_input['page_number']) ? filter_var($ea_received_rest_ws_raw_array_input['page_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				$number_of_records_input= trim(isset($ea_received_rest_ws_raw_array_input['number_of_records']) ? filter_var($ea_received_rest_ws_raw_array_input['number_of_records'], FILTER_SANITIZE_NUMBER_INT) : '');
				/* $sort_field_input= trim(isset($ea_received_rest_ws_raw_array_input['sort_field']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_field'], FILTER_SANITIZE_STRING) : ''); */
				/* $client_company_status_input = trim(isset($ea_received_rest_ws_raw_array_input['client_company_status']) ? filter_var($ea_received_rest_ws_raw_array_input['client_company_status'], FILTER_SANITIZE_NUMBER_INT) : ''); */
				/* $sort_order_input = trim(isset($ea_received_rest_ws_raw_array_input['sort_order']) ? filter_var($ea_received_rest_ws_raw_array_input['sort_order'], FILTER_SANITIZE_STRING) : '');
				$search_criteria_input = trim(isset($ea_received_rest_ws_raw_array_input['search_criteria']) ? filter_var($ea_received_rest_ws_raw_array_input['search_criteria'], FILTER_SANITIZE_STRING) : ''); */
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
			
				/* $eventLog->log("company_id_input -> " . $company_id_input); */
				$eventLog->log("page_number_input -> " . $page_number_input);
				/* $eventLog->log("sort_field_input -> " . $sort_field_input); */
				
				$eventLog->log("ip_address_input -> " . $ip_address_input);
				/* $eventLog->log("sort_order_input -> " . $sort_order_input);
				$eventLog->log("search_criteria_input -> " . $search_criteria_input); */
				$eventLog->log("number_of_records_input -> " . $number_of_records_input);
				
				
				
				//Check if all inputs are received correctly from the REST Web Service
				
				/* if (($client_company_status_input != "0") && ($client_company_status_input != "1") && ($client_company_status_input != "")) {
					//Invalid company Status scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-company-status";
					$response['status_description'] = "Invalid Company Status info submitted, please check and try again.";
					
					$eventLog->log("Please provide a valid Company Status info.");
					 
				} else */ if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-additional-information";
					$response['status_description'] = "Some Additional Information like IP Address (IPv4) is missing, please check and try again.";
				}/* else if (($sort_order_input != "ASC") && ($sort_order_input != "DESC") && ($sort_order_input != "")) {
					//Invalid Sorting Order scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-Sorting-order";
					$response['status_description'] = "Invalid Sorting Order info submitted, please check and try again.";
					
					$eventLog->log("invalid-Sorting-order: Invalid Sorting Order info submitted, please check and try again.");
					$eventLog->log("Please provide all information.");
				
                } */ else {
					//All inputs are Valid
				
					$eventLog->log("All inputs are valid.");
				
					try { 
					    
						
						
							
						$dashboard_candidates_list_result = get_dashboard_candidates_list_with_pagination_inputs($page_number_input,$number_of_records_input);
							
							
							
						
						if (count($dashboard_candidates_list_result) > 0) {
							//One or More User Groups Exist and Active
							
							$response['data'] = $dashboard_candidates_list_result;
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['status'] = "candidates-list-received";
							$response['status_description'] = "Candidates List Successfully Received";
							
							$eventLog->log("candidates-list-received -> Candidates List Successfully Received"); 
							
							 
							
						} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "active-candidates-doesnot-exist";
							$response['status_description'] = "No Active candidates list Exist for your company, please check and try again.";
							
							//No Active company list Exist
							$eventLog->log("Jobs List -> No Active Candidates Exist for your company, please check and try again."); 
						}//close of else of if ($company_list_result_count > "0") {
						
					} catch (Exception $e){
						$eventLog->log("Exception -> " . html_escaped_output($e->getMessage())); 
						//addLog($logFile, "Exception -> ".$e->getMessage());	
					}//close of  catch (Exception $e){
					
				
				}//close of if (($company_status_input != "0") && ($company_status_input != "1") && ($company_status_input != "")) {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
function get_dashboard_candidates_list_with_pagination_inputs($page_number_input, $number_of_records_input) {

    global $dbcon, $default_number_of_records_pagination; //app/core/main-config.php
	global $eventLog;
	$constructed_array = array();
	$eventLog->log("before is_null concept");
    $sm_user_type = "member";
	

	$limit_offset_value = 0;

	if (($page_number_input == "") && ($number_of_records_input == "")) {
		//Give All Data
		//No LIMIT Clause in the Query
		$limit_offset_in_query = "";

	} else if (($page_number_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than page_number_input, till the number of records, as defined in the $default_number_of_records_pagination

		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $default_number_of_records_pagination;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$default_number_of_records_pagination;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $default_number_of_records_pagination;
		}//close of else if of if ($page_number_input == "1") {


		$eventLog->log("before page number concept");
	} else if (($page_number_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records
		$limit_offset_in_query = " LIMIT " . $number_of_records_input;

	} else {
		//Give Data, from given Record, till the requested number of records
		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $number_of_records_input;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$number_of_records_input;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $number_of_records_input;
		}//close of else if of if ($page_number_input == "1") {

	}//close of else of if (($page_number_input == "") && ($number_of_records_input == "")) {
	

    $candidates_list_count_get_sql = "SELECT COUNT(*) AS count FROM `site_members` s JOIN `candidate_rel_additional_info` crai ON s.sm_memb_id = crai.sm_memb_id WHERE s.sm_user_type = :sm_user_type";
	$candidates_list_count_get_select_query = $dbcon->prepare($candidates_list_count_get_sql);
	$candidates_list_count_get_select_query->bindValue(":sm_user_type",$sm_user_type);
	
	$candidates_list_count_get_select_query->execute();

    $candidates_list_get_sql = "SELECT * FROM `site_members` s JOIN `candidate_rel_additional_info` crai ON s.sm_memb_id = crai.sm_memb_id WHERE s.sm_user_type = :sm_user_type" . $limit_offset_in_query;
    $candidates_list_get_select_query = $dbcon->prepare($candidates_list_get_sql);
	$candidates_list_get_select_query->bindValue(":sm_user_type",$sm_user_type);
	$candidates_list_get_select_query->execute();
	
		


	//Process / Get Company clients Count
	$eventLog->log("get the row count value");
	if($candidates_list_count_get_select_query->rowCount() > 0) {
	    $candidates_list_count_get_select_query_result = $candidates_list_count_get_select_query->fetch();
	    //print_r($company_clients_list_count_get_select_query_result);

		$total_candidates_count = $candidates_list_count_get_select_query_result["count"];
		$constructed_array["total_records_count"] = $total_candidates_count;

	}//close of if($company_clients_list_count_get_select_query->rowCount() > 0) {


	//Process / Fetch Companies List
	if($candidates_list_get_select_query->rowCount() > 0) {
	    $candidates_list_get_select_query_result = $candidates_list_get_select_query->fetchAll();
	    //print_r($companies_list_get_select_query_result);
	    $eventLog->log("retrive the details ");
		foreach ($candidates_list_get_select_query_result as $candidates_list_get_select_query_result_row) {

			$temp_row_array = array();
			$temp_row_array["sm_firstname"] = $candidates_list_get_select_query_result_row["sm_firstname"];
			$temp_row_array["sm_middlename"] = $candidates_list_get_select_query_result_row["sm_middlename"];
			$temp_row_array["sm_lastname"] = $candidates_list_get_select_query_result_row["sm_lastname"];
			$temp_row_array["fullname"] = $temp_row_array["sm_firstname"] ." ". $temp_row_array["sm_middlename"] . " ".  $temp_row_array["sm_lastname"];
			$temp_row_array["company_id"]= $candidates_list_get_select_query_result_row["company_id"];
		    $company_id = $temp_row_array["company_id"];
			if($company_id != null)
			{
			$company_details_result = get_company_details($company_id);
			
			$temp_row_array["company_name"] = $company_details_result["company_name"];
			} else{
				$temp_row_array["company_name"] = "null";
			}
			$sm_memb_id = $candidates_list_get_select_query_result_row["sm_memb_id"];
			$temp_row_array["sm_memb_id"] = $sm_memb_id;
			$candidate_profile_source_details = get_profile_source_details($sm_memb_id);
			$temp_row_array["profile_source"] = $candidate_profile_source_details["profile_source_name"];
		    
			


		    $constructed_array["list"][] = $temp_row_array;
	    }//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

	}//close of if($companies_list_get_select_query->rowCount() > 0) {
		$eventLog->log("all input values are recives");
	return $constructed_array;
}
function get_company_details($company_id) {
	global $dbcon;
	$constructed_array = array();
	$company_details_check_sql = "SELECT * FROM `companies` WHERE `company_id` = :company_id";
	$company_details_check_select_query = $dbcon->prepare($company_details_check_sql);
	$company_details_check_select_query->bindValue(":company_id",$company_id);
	$company_details_check_select_query->execute();

	if($company_details_check_select_query->rowCount() > 0) {
		$company_details_check_select_query_result = $company_details_check_select_query->fetch();
	     return $company_details_check_select_query_result;

	}//close of if($company_details_check_select_query->rowCount() > 0) {
	return $constructed_array;
	}
function get_profile_source_details($sm_memb_id) {
	global $dbcon;
	$constructed_array = array();
	$profile_source_details_check_sql = "SELECT * FROM `candidate_rel_additional_info` WHERE `sm_memb_id` = :sm_memb_id";
	$profile_source_details_check_select_query = $dbcon->prepare($profile_source_details_check_sql);
	$profile_source_details_check_select_query->bindValue(":sm_memb_id",$sm_memb_id);
	$profile_source_details_check_select_query->execute();

	if($profile_source_details_check_select_query->rowCount() > 0) {
		$profile_source_details_check_select_query_result = $profile_source_details_check_select_query->fetch();
	     return $profile_source_details_check_select_query_result;

	}//close of if($company_details_check_select_query->rowCount() > 0) {
	return $constructed_array;
	}	



exit;
?>