<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Edit Quick Admin Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_id'])) 
					
					
					if (isset($ea_received_rest_ws_raw_array_input['new_status'])) {
						$content .= $ea_received_rest_ws_raw_array_input['new_status'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['new_status'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$jsl_sub_classification_detail_id_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_id']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_sub_classification_detail_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				
				$new_status_input = trim(isset($ea_received_rest_ws_raw_array_input['new_status']) ? filter_var($ea_received_rest_ws_raw_array_input['new_status'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				
				if ($jsl_sub_classification_detail_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-screening-level-sub_classification_detail_id";
					$response['status_description'] = "Missing job screening level sub classification details id,please check and try agin.";
					
					$eventLog->log("missing-job-screening-level-sub-classification-details-id: Missing job screening level sub classification details id,please check and try agin.");
					
				}  else if ($new_status_input == "")  {
					//Invalid New Status scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-new-status";
					$response['status_description'] = "Missing new status";
					
					$eventLog->log("missing-new-status: Missing new status, Please check and try again.");
					
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Missing Some Additional Information";
					
					$eventLog->log("missing-some-additional-information: Some Additional Information is missing, please check and try again.");	
				
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//Event Time, as per Indian Standard Time
					$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
		            /*  $candidate_invited_jobs_record_check_result = candidate_invite_jobs_record_check($job_applicant_invite_id_input);
					 $company_id_input = $candidate_invited_jobs_record_check_result["company_id"];*/
					 
					$candidate_invite_jobs_status_disable_next_step = "";
					
					if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$candidate_invite_jobs_status_disable_next_step = "PROCEED-TO-NEXT-STEP";
								
							
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
					} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
					}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") { 
							
						if ($candidate_invite_jobs_status_disable_next_step == "PROCEED-TO-NEXT-STEP") {
						
                       
							try {
								$eventLog->log("before enter into query.");
								
								      $jsl_sub_classification_record_check_result = jsl_sub_classification_record_check($jsl_sub_classification_detail_id_input);
									 
												if (count($jsl_sub_classification_record_check_result) > 0) {
													
													$jsl_related_sub_classification_details_change_status_result = jsl_related_sub_classification_details_change_status($jsl_sub_classification_detail_id_input,$new_status_input);
													
													if($jsl_related_sub_classification_details_change_status_result == true){
													   
													   $response['data'] = array();
													   $response['status'] = "job-screening-level-related-sub-classification-details-changed-successfully";
													   $response['status_description'] = "job screening level related sub classification details changed Successfully.";
													
													    $eventLog->log("job screening level related sub classification details changed Successfully.");
													     
												    } else {
													
													$response['data'] = array();
													$response['status'] = "job-screening-level-related-sub-classification-details-status-change-error";
													$response['status_description'] = "There is an error, when changing  the job screening level related sub classification details status in the Database.";
													
													$eventLog->log("There is an error, when changing  the job screening level related sub classification details status in the Database.");
													
												}	
										    
									        } else {
												
										
												  $response['data'] = array();
												  $response['status'] = "no-job-screening-level-related-sub-classification-details-with-this-job-screening-level-sub-classification-detail-id";
												  $response['status_description'] = "No job screening level related classification details found with job screening level sub classification detail id";
													
												  $eventLog->log("No sub classification details found with given jsl detail Id");
												  
												}
									
										
										
								} catch (Exception $e) {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "job-screening-level-related-classification-details-status-change-error";
									$response['status_description'] = "There is an error, when changing  the job screening level related sub classification details status in the Database.";
									
									$eventLog->log("There is an error, when changing  the job screening level related sub classification details status in the Database. .");	
									
								}
					   
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "The User Account, that is attempted to be edited, is invalid, please check and try again.";
						
						$eventLog->log("Please provide a Valid user ID.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
					
						
				}//close of else of if ($email_id_input == "") {
				
			}
			
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
    }//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function jsl_sub_classification_record_check($jsl_sub_classification_detail_id_input) {
	global $dbcon;
	$constructed_array = array();
	$jsl_sub_classificaion_record_check_sql = "SELECT * FROM `jsl_sub_classification_details` WHERE  `jsl_sub_classification_detail_id`=:jsl_sub_classification_detail_id";
	$jsl_sub_classificaion_record_check_select_query = $dbcon->prepare($jsl_sub_classificaion_record_check_sql);
	$jsl_sub_classificaion_record_check_select_query->bindValue(":jsl_sub_classification_detail_id",$jsl_sub_classification_detail_id_input);	
	$jsl_sub_classificaion_record_check_select_query->execute(); 
	
	if($jsl_sub_classificaion_record_check_select_query->rowCount() > 0) {
		$jsl_sub_classificaion_record_check_select_query_result = $jsl_sub_classificaion_record_check_select_query->fetch();
	     return $jsl_sub_classificaion_record_check_select_query_result;
	
	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
	
}

function jsl_related_sub_classification_details_change_status($jsl_sub_classification_detail_id_input,$new_status_input) {
	global $dbcon, $eventLog;
	
	$eventLog->log("in function before query");
    
	$jsl_related_sub_classification_details_change_status_update_sql = " UPDATE `jsl_sub_classification_details` SET `is_active_status`=:is_active_status WHERE `jsl_sub_classification_detail_id`=:jsl_sub_classification_detail_id";
	$jsl_related_sub_classification_details_change_status_update_query = $dbcon->prepare($jsl_related_sub_classification_details_change_status_update_sql);
	$eventLog->log("in function before bind");
	
	$jsl_related_sub_classification_details_change_status_update_query->bindValue(":jsl_sub_classification_detail_id",$jsl_sub_classification_detail_id_input);
	$jsl_related_sub_classification_details_change_status_update_query->bindValue(":is_active_status",$new_status_input);
	
	$eventLog->log("in function after bind");

	if ($jsl_related_sub_classification_details_change_status_update_query->execute()) {
		
		$eventLog->log("in function enter into if");

		return true;

	}
	return false;
}	

exit;
?>


