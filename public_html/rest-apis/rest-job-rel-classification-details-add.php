
<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "11")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_classification_detail_name'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_classification_detail_name'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_classification_detail_name'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_classification_detail_description'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_classification_detail_description'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_classification_detail_description'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['jsl_candidate_email_notification_setting'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_candidate_email_notification_setting'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_candidate_email_notification_setting'])) 
						
	                if (isset($ea_received_rest_ws_raw_array_input['jsl_cen_job_specific_override_allowance'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_cen_job_specific_override_allowance'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_cen_job_specific_override_allowance'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['jsl_candidate_sms_notification_setting'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_candidate_sms_notification_setting'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_candidate_sms_notification_setting'])) 
					
					 if (isset($ea_received_rest_ws_raw_array_input['jsl_csn_job_specific_override_allowance'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_csn_job_specific_override_allowance'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_csn_job_specific_override_allowance'])) 
						 
					if (isset($ea_received_rest_ws_raw_array_input['jsl_recruiter_email_notification_setting'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_recruiter_email_notification_setting'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_recruiter_email_notification_setting'])) 	
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_ren_job_specific_override_allowance'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_ren_job_specific_override_allowance'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_ren_job_specific_override_allowance'])) 
							
					if (isset($ea_received_rest_ws_raw_array_input['jsl_recruiter_sms_notification_setting'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_recruiter_sms_notification_setting'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_recruiter_sms_notification_setting'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['jsl_rsn_job_specific_override_allowance'])) {
						$content .= $ea_received_rest_ws_raw_array_input['jsl_rsn_job_specific_override_allowance'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['jsl_rsn_job_specific_override_allowance'])) 
					
					
					//Filter Inputs
						
					$jsl_classification_detail_name_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_classification_detail_name']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_classification_detail_name'], FILTER_SANITIZE_STRING) : '');
					
					$jsl_classification_detail_description_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_classification_detail_description']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_classification_detail_description'], FILTER_SANITIZE_STRING) : '');
					
					$jsl_candidate_email_notification_setting_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_candidate_email_notification_setting']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_candidate_email_notification_setting'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					$jsl_cen_job_specific_override_allowance_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_cen_job_specific_override_allowance']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_cen_job_specific_override_allowance'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					$jsl_candidate_sms_notification_setting_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_candidate_sms_notification_setting']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_candidate_sms_notification_setting'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					$jsl_csn_job_specific_override_allowance_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_csn_job_specific_override_allowance']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_csn_job_specific_override_allowance'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					$jsl_recruiter_email_notification_setting_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_recruiter_email_notification_setting']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_recruiter_email_notification_setting'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					$jsl_ren_job_specific_override_allowance_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_ren_job_specific_override_allowance']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_ren_job_specific_override_allowance'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					$jsl_recruiter_sms_notification_setting_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_recruiter_sms_notification_setting']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_recruiter_sms_notification_setting'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					$jsl_rsn_job_specific_override_allowance_input = trim(isset($ea_received_rest_ws_raw_array_input['jsl_rsn_job_specific_override_allowance']) ? filter_var($ea_received_rest_ws_raw_array_input['jsl_rsn_job_specific_override_allowance'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					
					
					$jsl_classification_detail_seo_name_input = strtolower(create_seo_name($jsl_classification_detail_name_input));
					
					$jsl_classification_detail_description_all_input = trim($ea_received_rest_ws_raw_array_input["jsl_classification_detail_description"]);
					$jsl_classification_detail_description_safe_html_input = get_cleaned_safe_html_content_input($jsl_classification_detail_description_all_input, $page_content_file_config);

					/* if($jsl_classification_detail_name_input == 0){
						
						$jsl_classification_detail_name_input = null;
					} */
					
					
					
					if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
					} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
					}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						

					
					  //invalid company_id scenario
					
					if ($jsl_classification_detail_name_input == "") {
				
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "missing-job-screening-level-classification-detail-name";
						$response['status_description'] = "missing job Screening level classification detail name";
						
						$eventLog->log("missing-jsl-classification-detail-name: Missing jsl classification detail name.");
						
					} else if ($jsl_cen_job_specific_override_allowance_input == "") {
						//Invalid Mobile Number scenario
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "missing-job-screening-level-candidate-email-notification-job-specific-override-allowance";
						$response['status_description'] = "Missing job screening level candidate email notificaion job specific override allowance.";
						
						$eventLog->log("missing-job-screening-level-candidate-email-notification-job-specific-override-allowance: Missing job screening level candidate email notificaion job specific override allowance.");
						
					} else if ($jsl_ren_job_specific_override_allowance_input == "") {
						//Invalid Mobile Number scenario
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "missing-job-screening-level-resume-email-notification-job-specific-override-allowance-input";
						$response['status_description'] = "Missing job screening level resume email notification job specific override allowance input";
						
						$eventLog->log("missing-job-screening-level-resume-email-notification-job-specific-override-allowance-input: Missing job screening level resume email notification job specific override allowance input.");
						
					
				    } else {	
						
							$job_rel_classification_details_add_next_step = "";
							if ($ea_extracted_jwt_token_user_type == "admin") {
								
								if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
									$job_rel_classification_details_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("before company id condition");
								/* } else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
									 $eventLog->log("job related condition");
									$job_rel_classification_details_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("job related condition"); */
								} else {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "insufficient-permissions-different-company-user-id";
									$response['status_description'] = "Insufficient Set of Permissions";
									
									//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
									header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
									$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
									
								}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								
								
								
							} else {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
								$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
							
							}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
								
							$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
							 $eventLog->log("before proceed to next next step condition");
							 
							 
							if ($job_rel_classification_details_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
								try {
									 $eventLog->log("in to try block");
									$job_rel_classification_details_add_duplicate_check_result = job_rel_classification_details_add_duplicate_check($jsl_classification_detail_name_input);
									if(count($job_rel_classification_details_add_duplicate_check_result) > 0)
									{
										 $eventLog->log("in if condition");
												$response['data'] = array();
												$response['status'] = "job-screening-level-related-classification-details-already-exisis";
												$response['status_description'] = "Job screening level related classification details already exists.";
									} else {
						             $eventLog->log("before last inserted id condition");
									
									$last_inserted_id =
									job_rel_classification_details_insert($jsl_classification_detail_name_input,$jsl_classification_detail_seo_name_input,$jsl_classification_detail_description_input,$jsl_classification_detail_description_safe_html_input,$jsl_candidate_email_notification_setting_input,$jsl_cen_job_specific_override_allowance_input,$jsl_candidate_sms_notification_setting_input,$jsl_csn_job_specific_override_allowance_input,$jsl_recruiter_email_notification_setting_input,$jsl_ren_job_specific_override_allowance_input,$jsl_recruiter_sms_notification_setting_input,$jsl_rsn_job_specific_override_allowance_input,$event_datetime,$current_epoch,$ea_extracted_jwt_token_sub);
									 $eventLog->log($last_inserted_id);
									 
									if ($last_inserted_id != "") {
										
										
												$response['data'] = array();
												$response['status'] = "job-screening-level-related-classification-details-insertion-successful";
												$response['status_description'] = "Job screening level related classification details are added Successfully.";
										
												$eventLog->log("Job screening level related classification details are added Successfully."); 
										
											} else {
												
												$response['data'] = array();
												$response['status'] = "job-screening-level-related-classification-details-insertion-error";
												$response['status_description'] = "Error occurred when adding the job screening level related classification details.";
												
												$eventLog->log("job-screening -level-related-classification-details-insertion-error: Error occurred when adding the job screening level related classification details.");
												
											}//close of else of if (count($user_ass_oc_validation_result) > 0) {
											
										
										}
									
										
										 
										
									
								} catch (Exception $e) {
									
									$response['data'] = array();
									$response['status'] = "job-screening-level-related-classification-details-insertion-error";
									$response['status_description'] = "Error occurred when adding the job screening level related classification details .";
										
									$eventLog->log("job- screening-level-related-classification-details-insertion-error: Error occurred when adding the job screening level related classification details.");
										
								}
							}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
							
						}
			}//close of else of if ($email_id_input == "") {
					
		}//close of //close of if ($ea_is_user_page_access_authorized) {
					
				
	} else {
				
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "invalid-auth-token-submitted";
				$response['status_description'] = "Invalid Authentication";
				
				//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
				header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
				
				$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
				
			}//close of else of if ($ea_auth_token_validation_status) {
		
		}//close of if ($ea_maintenance_mode == false) {
		
} else {

		//Construct Content, that will be sent in Response body, of the REST Web Service
		$response['data'] = array();
		$response['status'] = "invalid-input";
		$response['status_description'] = "Invalid Input, Please check and provide all information.";
		
		//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
		
		$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
				
		
		}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




	//Check if Maintenance Mode is Turned On
	if ($ea_maintenance_mode) {	
		
		//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
		//header('Maintenance-Progress: true', false);
		//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
		$response['data'] = array();
		$response['status'] = "application-maintenance-in-progress";
		$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
		
		$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
		
	} else {	

		//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
		header('Content-Type: application/json');
		echo json_encode($response,JSON_PRETTY_PRINT);
		
	}//close of else of if ($ea_maintenance_mode){
		
function job_rel_classification_details_insert($jsl_classification_detail_name_input,$jsl_classification_detail_seo_name_input,$jsl_classification_detail_description_input,$jsl_classification_detail_description_safe_html_input,$jsl_candidate_email_notification_setting_input,$jsl_cen_job_specific_override_allowance_input,$jsl_candidate_sms_notification_setting_input,$jsl_csn_job_specific_override_allowance_input,$jsl_recruiter_email_notification_setting_input,$jsl_ren_job_specific_override_allowance_input,$jsl_recruiter_sms_notification_setting_input,$jsl_rsn_job_specific_override_allowance_input,$event_datetime,$current_epoch,$ea_extracted_jwt_token_sub) {
	
	
	global $dbcon,$eventLog;
	$is_active_status = '1';
	
    
	$job_rel_classification_details_insert_sql = "INSERT INTO `jsl_classification_details`(`jsl_classification_detail_name`, `jsl_classification_detail_seo_name`, `jsl_classification_detail_description`, `jsl_classification_detail_description_safe_html`, `jsl_candidate_email_notification_setting`, `jsl_cen_job_specific_override_allowance`, `jsl_candidate_sms_notification_setting`, `jsl_csn_job_specific_override_allowance`, `jsl_recruiter_email_notification_setting`, `jsl_ren_job_specific_override_allowance`, `jsl_recruiter_sms_notification_setting`, `jsl_rsn_job_specific_override_allowance`, `added_datetime`, `added_datetime_epoch`, `added_by_sm_memb_id`, `is_active_status`) VALUES (:jsl_classification_detail_name,:jsl_classification_detail_seo_name,:jsl_classification_detail_description,:jsl_classification_detail_description_safe_html,:jsl_candidate_email_notification_setting,:jsl_cen_job_specific_override_allowance,:jsl_candidate_sms_notification_setting,:jsl_csn_job_specific_override_allowance,:jsl_recruiter_email_notification_setting,:jsl_ren_job_specific_override_allowance,:jsl_recruiter_sms_notification_setting,:jsl_rsn_job_specific_override_allowance,:added_datetime,:added_datetime_epoch,:added_by_sm_memb_id,:is_active_status)";

	$job_rel_classification_details_insert_query = $dbcon->prepare($job_rel_classification_details_insert_sql);
	$job_rel_classification_details_insert_query->bindValue(":jsl_classification_detail_name",$jsl_classification_detail_name_input);
	$job_rel_classification_details_insert_query->bindValue(":jsl_classification_detail_seo_name",$jsl_classification_detail_seo_name_input);
	$job_rel_classification_details_insert_query->bindValue(":jsl_classification_detail_description",$jsl_classification_detail_description_input);	
	$job_rel_classification_details_insert_query->bindValue(":jsl_classification_detail_description_safe_html",$jsl_classification_detail_description_safe_html_input);
	$job_rel_classification_details_insert_query->bindValue(":jsl_candidate_email_notification_setting",$jsl_candidate_email_notification_setting_input);
	$job_rel_classification_details_insert_query->bindValue(":jsl_cen_job_specific_override_allowance",$jsl_cen_job_specific_override_allowance_input);
	$job_rel_classification_details_insert_query->bindValue(":jsl_candidate_sms_notification_setting",$jsl_candidate_sms_notification_setting_input);
	$job_rel_classification_details_insert_query->bindValue(":jsl_csn_job_specific_override_allowance",$jsl_csn_job_specific_override_allowance_input);
	$job_rel_classification_details_insert_query->bindValue(":jsl_recruiter_email_notification_setting",$jsl_recruiter_email_notification_setting_input);
    $job_rel_classification_details_insert_query->bindValue(":jsl_ren_job_specific_override_allowance",$jsl_ren_job_specific_override_allowance_input);
	$job_rel_classification_details_insert_query->bindValue(":jsl_recruiter_sms_notification_setting",$jsl_recruiter_sms_notification_setting_input);
	$job_rel_classification_details_insert_query->bindValue(":jsl_rsn_job_specific_override_allowance",$jsl_rsn_job_specific_override_allowance_input);
	
	$job_rel_classification_details_insert_query->bindValue(":added_datetime",$event_datetime);
	$job_rel_classification_details_insert_query->bindValue(":added_datetime_epoch",$current_epoch);
	$job_rel_classification_details_insert_query->bindValue(":added_by_sm_memb_id",$ea_extracted_jwt_token_sub);
	$job_rel_classification_details_insert_query->bindValue(":is_active_status",$is_active_status);
	
	if ($job_rel_classification_details_insert_query->execute()) {
          $eventLog->log("after execute.");
            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");
           
			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}


function job_rel_classification_details_add_duplicate_check($jsl_classification_detail_name_input) {
	global $dbcon;
	$constructed_array = array();
	
	$job_rel_classification_details_add_duplicate_check_sql = "SELECT * FROM `jsl_classification_details` WHERE `jsl_classification_detail_name` = :jsl_classification_detail_name";
	$job_rel_classification_details_add_duplicate_check_query = $dbcon->prepare($job_rel_classification_details_add_duplicate_check_sql);	
	$job_rel_classification_details_add_duplicate_check_query->bindValue(":jsl_classification_detail_name",$jsl_classification_detail_name_input);	
		
	$job_rel_classification_details_add_duplicate_check_query->execute(); 
	
	if($job_rel_classification_details_add_duplicate_check_query->rowCount() > 0) {
		$job_rel_classification_details_add_duplicate_check_query_result = $job_rel_classification_details_add_duplicate_check_query->fetchAll();
	     return $job_rel_classification_details_add_duplicate_check_query_result;
	
	}
	return $constructed_array;
}



exit;
?>