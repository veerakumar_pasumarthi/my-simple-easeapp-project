<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * REST API for candidate all professional summary collect
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "4")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['user_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['user_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['user_id']))
                    
                    if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id']))					
					
					if (isset($ea_received_rest_ws_raw_array_input['all_professional_summary'])) {
						$content .= json_encode($ea_received_rest_ws_raw_array_input['all_professional_summary']) . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['all_professional_summary'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$all_professional_summary_input = $ea_received_rest_ws_raw_array_input['all_professional_summary'];
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//Check if all inputs are received correctly from the REST Web Service
				if ($user_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-user-id";
					$response['status_description'] = "Missing User ID";
					
					$eventLog->log("missing-user-id: Please provide a valid User ID.");
					
				} else if ($company_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing Company ID";
					
					$eventLog->log("missing-company-id: Please provide a valid Company ID.");
					
				} else if ($all_professional_summary_input == "") {
					//Invalid Education Qualifications Received scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "empty-professional-summary-details-input";
					$response['status_description'] = "Empty Professional summary details";
					
					$eventLog->log("empty-professional-summary-details-input: Professional summary are Empty, Please check and try again.");
					
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Missing Some Additional Information";
					
					$eventLog->log("missing-some-additional-information: Please provide all information.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					$ea_action_specific_auth_check_result = "";
					
					//User Details, based on User ID input, through api request
					$user_basic_details_result = user_basic_details_check_based_on_user_id($user_id_input);
					
					if (count($user_basic_details_result) > 0) {
						
						$user_rel_company_id = $user_basic_details_result["company_id"];
						$user_rel_sm_user_status = $user_basic_details_result["sm_user_status"];
						
						$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
						//$ea_action_specific_auth_check_result = ea_check_action_specific_user_privileges_different_user_levels_reset_password_feature($ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_user_company_id, $ea_extracted_jwt_token_sub, $user_id_input, $ea_extracted_jwt_token_user_privileges_list, $user_rel_company_id, $page_filename);					
						
						//echo "ea_action_specific_auth_check_result: " . $ea_action_specific_auth_check_result;					
							
							
						
					
						
						if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
								
								//echo "user_rel_sm_user_status: " . $user_rel_sm_user_status . "<br>";
								//echo "new_status_input: " . $new_status_input . "<br>";
								
								//Event Time, as per Indian Standard Time
								$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
								
								
								
									
								//Define an array, to Collect "degree" and "field of study" field values of all received Education Qualification info.
								$all_received_company_name_from_month_from_year_fields_array = array();
								
								//Define an array, to Collect "degree" and "field of study" field values of all Education Qualification info, from the Database.
								$all_db_based_company_name_from_month_from_year_fields_array = array();
								
								/*//Do process each record (Education Qualification), and Collect "degree" and "field of study" field values of all received Education Qualifications
								foreach ($all_education_qualifications_input as $all_education_qualifications_input_row) {
									
									$all_received_degree_fieldofstudy_fields_array["degree"] = $all_education_qualifications_input_row["degree"];
									$all_received_degree_fieldofstudy_fields_array["field_of_study"] = $all_education_qualifications_input_row["field_of_study"];
									
									
								}//close of foreach ($all_education_qualifications_input as $all_education_qualifications_input_row) {
								*/	
								
								//Define an array, to Collect all received Education Qualification info, with corresponding record processing status and info
								$all_collected_content_with_result_array = array();
								
								//Collect All Professional Summaries List, of particular candidate, to extract degree and field of study field content
								$candidate_professional_summary_records_list_result = candidate_professional_summary_records_list($user_id_input);
								$candidate_professional_summary_records_list_result_count = count($candidate_professional_summary_records_list_result);
								
								$eventLog->log("candidate_professional_summary_records_list_result_count: " . $candidate_professional_summary_records_list_result_count);
								
										$input_primary_keys_array = array();
								if ((is_array($all_professional_summary_input)) && ((count($all_professional_summary_input) > 0) || ($candidate_professional_summary_records_list_result_count > 0))) {
									
									//Do process each record (Education Qualification)
									foreach ($all_professional_summary_input as $all_professional_summary_input_row) {
										
										$received_company_name_from_month_from_year_fields_array = array();
										$collected_content_with_result_array = array();
										
										$collected_content_with_result_array = $all_professional_summary_input_row;
										
										
										$collected_content_with_result_array_json_encoded = json_encode($collected_content_with_result_array);
										$eventLog->log("collected_array row initial stage: " . $collected_content_with_result_array_json_encoded);	
										
										$all_professional_summary_input_row_count = count($all_professional_summary_input_row);
										//$eventLog->log("collected_array row initial stage count: " . $all_education_qualifications_input_row_count);
										
										if (count($all_professional_summary_input_row) == 11) {
											
											$eventLog->log("inside count(all_professional_summary_input_row) == 11 if condition: ");
											
											
											//[{"ceq_id":"","degree":"dgfgf","field_of_study":"ghghgh","institution_name":"fhghghg","university_name":"","board_name":"","year_of_passout":"2012","earned_percentage":"65","earned_grade":""},{"ceq_id":"","degree":"gfgfg","field_of_study":"fgdfgf","institution_name":"fgdfg","university_name":"","board_name":"","year_of_passout":"2011","earned_percentage":"75","earned_grade":""}]
										
											$crpe_id = trim(isset($all_professional_summary_input_row["crpe_id"]) ? filter_var($all_professional_summary_input_row["crpe_id"], FILTER_SANITIZE_NUMBER_INT) : '');
											/*$company_name = trim(isset($all_professional_summary_input_row["company_name"]) ? filter_var($all_professional_summary_input_row["company_name"], FILTER_SANITIZE_STRING) : '');*/
											$company_name = trim(isset($all_professional_summary_input_row["company_name"]) ? filter_var($all_professional_summary_input_row["company_name"], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES) : '');
											$designation = trim(isset($all_professional_summary_input_row["designation"]) ? filter_var($all_professional_summary_input_row["designation"], FILTER_SANITIZE_STRING) : '');
											$location_city = trim(isset($all_professional_summary_input_row["location_city"]) ? filter_var($all_professional_summary_input_row["location_city"], FILTER_SANITIZE_STRING) : '');
											$location_state = trim(isset($all_professional_summary_input_row["location_state"]) ? filter_var($all_professional_summary_input_row["location_state"], FILTER_SANITIZE_STRING) : '');
											$location_country = trim(isset($all_professional_summary_input_row["location_country"]) ? filter_var($all_professional_summary_input_row["location_country"], FILTER_SANITIZE_STRING) : '');
											$from_month = trim(isset($all_professional_summary_input_row["from_month"]) ? filter_var($all_professional_summary_input_row["from_month"], FILTER_SANITIZE_NUMBER_INT) : '');
											$from_year = trim(isset($all_professional_summary_input_row["from_year"]) ? filter_var($all_professional_summary_input_row["from_year"], FILTER_SANITIZE_NUMBER_INT) : '');
											$to_month = trim(isset($all_professional_summary_input_row["to_month"]) ? filter_var($all_professional_summary_input_row["to_month"], FILTER_SANITIZE_NUMBER_INT) : '');
											$to_year = trim(isset($all_professional_summary_input_row["to_year"]) ? filter_var($all_professional_summary_input_row["to_year"], FILTER_SANITIZE_NUMBER_INT) : '');
											$is_current_company = trim(isset($all_professional_summary_input_row["is_current_company"]) ? filter_var($all_professional_summary_input_row["is_current_company"], FILTER_SANITIZE_STRING) : '');
											if($is_current_company == true){
												$is_current_company = 1;
											}
											if($is_current_company == false){
												$is_current_company = 0;
											}
											
											$eventLog->log("to_month->".$to_month);
											$eventLog->log("to_year->".$to_year);
											//Do process the record (Education Qualification), and Collect "degree" and "field of study" field values of received Education Qualification
											$received_company_name_from_month_from_year_fields_array["company_name"] = $all_professional_summary_input_row["company_name"];
											$received_company_name_from_month_from_year_fields_array["from_month"] = $all_professional_summary_input_row["from_month"];
											$received_company_name_from_month_from_year_fields_array["from_year"] = $all_professional_summary_input_row["from_year"];
											
											//Check if all record level inputs are received, as part of "all_education_qualifications" field, and correctly from the REST Web Service
											if ($company_name == "") {
											//Empty Degree Field
												
												$collected_content_with_result_array["company_name"] = $company_name;
												
												$eventLog->log("Empty company name input");
										
											} else if ($designation == "") {
											//Empty Designation field
												
												$collected_content_with_result_array["designation"] = $designation;
												
												$eventLog->log("Empty designation input");
						
											} else if ($location_city == "") {
											//Empty Field of location city Field
												
												$collected_content_with_result_array["location_city"] = $location_city;
												
												$eventLog->log("Empty Field of location city");
										
											} else if ($location_state == "") {
											//Empty Field of location state field
												
												$collected_content_with_result_array["location_state"] = $location_state;
												
												$eventLog->log("Empty location state input");
									
											} else if ($location_country == "") {
											//Empty Field of location country field
												
												$collected_content_with_result_array["location-country"] = $location_country;
												
												$eventLog->log("Empty location country input");
											
		
											} else if ($from_month == "") {
											 //Empty Field of from month year field
												
												$collected_content_with_result_array["from_month"] = $from_month;
												
												$eventLog->log("Empty from month input");
											
											
											} else if ($from_year == ""){
											//Empty Field of from year field
												
												$collected_content_with_result_array["from_year"] = $from_year;
												
												$eventLog->log("Empty from year input");
						
											
											} else if ($is_current_company == "0" && $to_year == "") {
											//Empty Field of to year field
											   
												
												$collected_content_with_result_array["to_year"] = $to_year;
												
												$eventLog->log("Empty to year input");
						
											
											
											} else if ($is_current_company == "0" && $to_month == "") {
											//Empty Field of to month field
												
												
												$collected_content_with_result_array["to_month"] = $to_month;
												
												$eventLog->log("Empty to month input");
						
						   
												
										} else {
												//All Education Qualification Details are valid, for this record
												
												$eventLog->log("All professional summary Details are valid, for this record");
												
												/* COMMENTED TO PLACE THIS OUTSIDE CONDITION //Do process the record (Education Qualification), and Collect "degree" and "field of study" field values of received Education Qualification
												$received_company_name_from_month_from_year_fields_array["company_name"] = $all_professional_summary_input_row["company_name"];
												$received_company_name_from_month_from_year_fields_array["from_month"] = $all_professional_summary_input_row["from_month"];
												$received_company_name_from_month_from_year_fields_array["from_year"] = $all_professional_summary_input_row["from_year"];
												*/
												//Do Duplicate Check, for each Education Qualification, in the application, w.r.t. the particular candidate's sm_memb_id
												if($is_current_company == '1') {
													$to_month = null;
													$to_year = null;
												}
												/*$candidate_professional_summary_check_result = candidate_professional_summary_duplicate_check($user_id_input, $company_name, $from_month, $from_year, $company_id_input);
												 $eventLog->log("duplicate check."); */
								                  $candidate_professional_summary_check_result = array();
												if($crpe_id != ""){
												$candidate_professional_summary_check_result = company_client_spoc_details_duplicate_check_based_on_id($crpe_id);
												}
												if (count($candidate_professional_summary_check_result) > 0) {
													//Do get the reference id
													$eventLog->log("duplicate exists.");
													$crpe_id_from_db = $candidate_professional_summary_check_result["crpe_id"];
													$eventLog->log("crpe_id_from_db :");
													$eventLog->log($crpe_id_from_db);
													$eventLog->log("crpe_id:");
													$eventLog->log($crpe_id);
													$input_primary_keys_array[] = $crpe_id;
													//Check, if ceq_id, is same, from both sources (received from api & from db source, after duplicate check)
													if ($crpe_id == $crpe_id_from_db) {
														
														$eventLog->log("just for if condition checking");
														//ceq_id from the api request and from db source are matching, for particular education qualification record.
														$eventLog->log("crpe_id from the api request and from db source are matching, for particular professional summary record.");
														
														
														$collected_content_with_result_array["crpe_id"] = $crpe_id;
													
														$eventLog->log("Update Query Condition");
														
														//Do Update Query to existing record
														$candidate_professional_summary_update_result = update_candidate_professional_summary_based_on_cps_id($company_name, $designation, $location_city, $location_state, $location_country, $from_month, $from_year, $to_month, $to_year, $is_current_company, $ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_sub, $event_datetime, $current_epoch, $crpe_id);
														
														if ($candidate_professional_summary_update_result) {
															//Update Success
															$collected_content_with_result_array["query_type"] = "UPDATE-QUERY";
															$collected_content_with_result_array["query_status"] = "SUCCESS";
															$collected_content_with_result_array["input_record_validity_status"] = "VALID";
															
															$eventLog->log("Update Query Condition - Success");
															
														} else {
															$collected_content_with_result_array["query_type"] = "UPDATE-QUERY";
															$collected_content_with_result_array["query_status"] = "FAILURE";
															$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
											
															$eventLog->log("Update Query Condition - Failure");
															
														}//close of else of if ($candidate_education_qualification_update_result) {
														
														
													} else {
														//ceq_id from the api request and from db source are different, for particular education qualification record.
														$eventLog->log("cps_id from the api request and from db source are different, for particular professional summary record.");
														
														$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
														
													}//close of else of if ($ceq_id == $ceq_id_from_db) {
														
													
												} else {
													
													$eventLog->log("Insert Query Condition");
													
													$eventLog->log("user_id_input: " . $user_id_input);
													$eventLog->log("company_name: " . $company_name);
													$eventLog->log("designation: " . $designation);
													$eventLog->log("location_city: " . $location_city);
													$eventLog->log("location_state: " . $location_state);
													$eventLog->log("location_country: " . $location_country);
													$eventLog->log("from_month: " . $from_month);
													$eventLog->log("from_year: " . $from_year);
													$eventLog->log("to_month: " . $to_month);
													$eventLog->log("to_year: " . $to_year);
													$eventLog->log("is_current_company: " . $is_current_company);
													$eventLog->log("sequence order: " . "1111");
													$eventLog->log("ea_extracted_jwt_token_user_type: " . $ea_extracted_jwt_token_user_type);
													$eventLog->log("ea_extracted_jwt_token_sub: " . $ea_extracted_jwt_token_sub);
													$eventLog->log("event_datetime: " . $event_datetime);
													$eventLog->log("current_epoch: " . $current_epoch);
													
													
													//Find the Sequence Number, for the New Education Qualification
													$candidate_current_professional_summary_count_result = candidate_professional_summary_record_count($user_id_input);
						
													if (isset($candidate_current_professional_summary_count_result["current_count"])) {
														
														$candidate_current_professional_summary_count = $candidate_current_professional_summary_count_result["current_count"];
														
													} else {
														$candidate_current_professional_summary_count = 0;
														
													}//close of else of if (isset($candidate_current_education_qualification_count_result["current_count"])) {
														
													$candidate_professional_summary_sequence_order = $candidate_current_professional_summary_count+1;
													
													//Do Insert Query to add a new record
													$candidate_fullname_result = array();
														
														$candidate_fullname_result = candidate_professional_summary_duplicate_check($user_id_input, $company_name, $from_month, $from_year, $company_id_input);
														
														
														if(count($candidate_fullname_result) > 0 ) {
															
															
																$collected_content_with_result_array["crpe_id"] = null;
																$collected_content_with_result_array["query_type"] = "INSERT-QUERY";
																$collected_content_with_result_array["query_status"] = "FAILURE";
																$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
															
															
														} else {
														
													$candidate_professional_summary_insert_result = insert_candidate_professional_summary($user_id_input, $company_name, $designation, $location_city, $location_state, $location_country, $from_month, $from_year, $to_month, $to_year, $is_current_company, $candidate_professional_summary_sequence_order, $ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_sub, $event_datetime, $current_epoch, $company_id_input);
												
													if (isset($candidate_professional_summary_insert_result["last_inserted_id"])) {
														//Insert Success

														$input_primary_keys_array[] = $candidate_professional_summary_insert_result;
														$collected_content_with_result_array["crpe_id"] = $candidate_professional_summary_insert_result["last_inserted_id"];
														$collected_content_with_result_array["query_type"] = "INSERT-QUERY";
														$collected_content_with_result_array["query_status"] = "SUCCESS";
														$collected_content_with_result_array["input_record_validity_status"] = "VALID";
														$eventLog->log("Insert Query Condition - Success");
														
														//$additional_document_ref_input = $designation . " (" . $company_name . ")";
														//$last_inserted_candidate_professional_summary_id = $candidate_professional_summary_insert_result["last_inserted_id"];
														//candidate_professional_summary_rel_file_upload_references_insert($user_id_input,"professional-summary",$last_inserted_candidate_professional_summary_id,$additional_document_ref_input,null,null,$ea_extracted_jwt_token_user_type,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
														
														
													} else {
														//Insert Failure
														$collected_content_with_result_array["crpe_id"] = null;
														$collected_content_with_result_array["query_type"] = "INSERT-QUERY";
														$collected_content_with_result_array["query_status"] = "FAILURE";
														$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
										
														$eventLog->log("Insert Query Condition - Failure");
														
													}//close of else of if (isset($candidate_education_qualification_insert_result["last_inserted_id"])) {
													}	
													
												}//close of else of if (count($candidate_education_qualification_check_result) > 0) {
												
												$collected_content_with_result_array_json_encoded = json_encode($collected_content_with_result_array);
												//$eventLog->log("collected_array row next stage: " . $collected_content_with_result_array_json_encoded);	
												
												
											}//close of else of if ($degree == "") {
												
											
										} else {
											
											$eventLog->log("inside count(all_professional_summary_input_row) == 11 else condition: ");
											
											//Expected number of fields are not sent, per education qualification record
											$collected_content_with_result_array["input_record_validity_status"] = "INVALID";
										
										}//close of else of if (count($all_education_qualifications_input_row)) {
										
										$all_received_company_name_from_month_from_year_fields_array[] = $received_company_name_from_month_from_year_fields_array;
										$all_collected_content_with_result_array[] = $collected_content_with_result_array;
																								
										
									}//close of foreach ($all_education_qualifications_input as $all_education_qualifications_input_row) {
									
									$all_collected_content_with_result_array_json_encoded = json_encode($all_collected_content_with_result_array);
									$eventLog->log("collected_array final: " . $all_collected_content_with_result_array_json_encoded);	
									
									
									/*//Collect All Professional Summaries List, of particular candidate, to extract degree and field of study field content
									$candidate_professional_summary_records_list_result = candidate_professional_summary_records_list($user_id_input);
									*/
									
									if (count($candidate_professional_summary_records_list_result) > 0) {
										//One or More than one Professional Summary is received from the Database.
										
										$all_received_company_name_from_month_from_year_fields_array_json_encoded = json_encode($all_received_company_name_from_month_from_year_fields_array);
										$eventLog->log("all_received_company_name_from_month_from_year_fields_array_json_encoded : " . $all_received_company_name_from_month_from_year_fields_array_json_encoded);	
										
										//Do process the record (Professional Summary), and Collect "company_name", "from_month" and "from_year" field values of received Professional Summary
										foreach($candidate_professional_summary_records_list_result as $candidate_professional_summary_records_list_result_row) {
											
											$db_based_cps_id_field_value = $candidate_professional_summary_records_list_result_row["crpe_id"];
											if (in_array($db_based_cps_id_field_value, $input_primary_keys_array)) {
												
												
											
											} else {
												
												
												$delete_result = delete_candidate_professional_summary_record_based_on_cps_id_input($db_based_cps_id_field_value);
									
											}
											
										}//close of foreach($candidate_education_qualification_records_list_result as $candidate_education_qualification_records_list_result_row) {
										
									} else {
										//Education Qualifications of the Candidate are not available in the Database, this means, Nothing to Remove.
										$eventLog->log("Professional summary of the Candidate are not available in the Database, this means, Nothing to Remove.");
										
									}//close of else of if (count($candidate_professional_summary_records_list_result) > 0) {
										
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = $all_collected_content_with_result_array;
									$response['status'] = "candidate-professional-summary-data-ingestion-result";
									$response['status_description'] = "All Candidate submitted professional summary is processed.";
									
									$eventLog->log("candidate-professional-summary-data-ingestion-result: All Candidate submitted Professional summary Data is processed.");	
									
								} else {
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "no-records-received";
									$response['status_description'] = "Please Submit at least One Professional Summary.";
									
									$eventLog->log("no-records-received: Please Submit at least One Professional Summary.");	
									
								}//close of else of if ((is_array($all_professional_summary_input)) && ((count($all_professional_summary_input) > 0) || ($candidate_professional_summary_records_list_result_count > 0))) {
								
								
							} catch (Exception $e) {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "candidate-professional-summary-processing-error";
								$response['status_description'] = "Error occurred when processing candidate's professional summary.";
								
								$eventLog->log("candidate-professional-summary-processing-error: Error occurred when processing candidate's professional summary.");	
								
							}
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-subject-platform-scope-user";
							$response['status_description'] = "Insufficient Set of Permissions, to complete this action";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-subject-platform-scope-user: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-different-company-user-id";
							$response['status_description'] = "Insufficient Set of Permissions, as the User belongs to a different company";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-INSUFFICIENT-PERMISSIONS") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions, as this process will not be allowed any further";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-self-action";
							$response['status_description'] = "Insufficient Set of Permissions, as self action is Dis-allowed";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-disallowed-self-action: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-ACTION-SUBJECT-IS-A-DIFFERENT-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-action-on-different-user";
							$response['status_description'] = "Insufficient Set of Permissions, as action subject is a different user";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-disallowed-action-on-different-user: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						}//close of if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
						
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "Invalid User Account, that is attempted to be edited";
						
						$eventLog->log("invalid-user-account: The User Account, that is attempted to be edited, is invalid, please check and try again.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
						
					
				}//close of else of if ($user_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token";
			
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");	
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input";
	
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){

function candidate_professional_summary_duplicate_check($user_id_input, $company_name, $from_month, $from_year, $company_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	
	$candidate_company_name_input = '%'.$company_name.'%';
	
	$candidate_professional_summary_check_sql = "SELECT * FROM `candidate_rel_professional_experiences` WHERE `sm_memb_id`=:sm_memb_id AND `company_name` LIKE :company_name AND `from_month`=:from_month AND `from_year`=:from_year AND `company_id`=:company_id";
	$candidate_professional_summary_check_select_query = $dbcon->prepare($candidate_professional_summary_check_sql);
	$candidate_professional_summary_check_select_query->bindValue(":sm_memb_id",$user_id_input);	
	$candidate_professional_summary_check_select_query->bindValue(":company_name",$candidate_company_name_input);	
	$candidate_professional_summary_check_select_query->bindValue(":from_month",$from_month);
    $candidate_professional_summary_check_select_query->bindValue(":from_year",$from_year);
    $candidate_professional_summary_check_select_query->bindValue(":company_id",$company_id_input);		
	$candidate_professional_summary_check_select_query->execute(); 
	
	if($candidate_professional_summary_check_select_query->rowCount() > 0) {
		$candidate_professional_summary_check_select_query_result = $candidate_professional_summary_check_select_query->fetch();
	     return $candidate_professional_summary_check_select_query_result;
	
	}//close of if($candidate_education_qualification_check_select_query->rowCount() > 0) {
	return $constructed_array;
	
}

function update_candidate_professional_summary_based_on_cps_id($company_name, $designation, $location_city, $location_state, $location_country, $from_month, $from_year, $to_month, $to_year, $is_current_company,$ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_sub, $event_datetime, $current_epoch, $crpe_id) {
	
	global $dbcon;
	
	$candidate_professional_summary_update_sql = "UPDATE `candidate_rel_professional_experiences` SET `company_name`=:company_name,`designation`=:designation,`location_city`=:location_city,`location_state`=:location_state,`location_country`=:location_country,`from_month`=:from_month, `from_year`=:from_year, `to_month`=:to_month, `to_year`=:to_year, `is_current_company`=:is_current_company,`last_updated_by_user_id`=:last_updated_by_user_id,`last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch WHERE `crpe_id`=:crpe_id";	
	$candidate_professional_summary_update_query = $dbcon->prepare($candidate_professional_summary_update_sql);		
	$candidate_professional_summary_update_query->bindValue(":company_name",$company_name);
	$candidate_professional_summary_update_query->bindValue(":designation",$designation);
	$candidate_professional_summary_update_query->bindValue(":location_city",$location_city);
	$candidate_professional_summary_update_query->bindValue(":location_state",$location_state);
	$candidate_professional_summary_update_query->bindValue(":location_country",$location_country);
	$candidate_professional_summary_update_query->bindValue(":from_month",$from_month);
	$candidate_professional_summary_update_query->bindValue(":from_year",$from_year);
	$candidate_professional_summary_update_query->bindValue(":to_month",$to_month);
	$candidate_professional_summary_update_query->bindValue(":to_year",$to_year);
	$candidate_professional_summary_update_query->bindValue(":is_current_company",$is_current_company);
	//$candidate_professional_summary_update_query->bindValue(":last_updated_by_user_type",$ea_extracted_jwt_token_user_type);
	$candidate_professional_summary_update_query->bindValue(":last_updated_by_user_id",$ea_extracted_jwt_token_sub);
	$candidate_professional_summary_update_query->bindValue(":last_updated_date_time",$event_datetime);
	$candidate_professional_summary_update_query->bindValue(":last_updated_date_time_epoch",$current_epoch);
	$candidate_professional_summary_update_query->bindValue(":crpe_id",$crpe_id);
	
	if ($candidate_professional_summary_update_query->execute()) {
		return true;
		
	}//close of if ($user_account_password_request_status_update_query->execute()) {
	
	return false;
}


function insert_candidate_professional_summary($user_id_input, $company_name, $designation, $location_city, $location_state, $location_country, $from_month, $from_year, $to_month, $to_year, $is_current_company, $sequence_order, $ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_sub, $event_datetime, $current_epoch, $company_id_input) {
	
	global $dbcon;
	
	//Do Insert Query
	$candidate_professional_summary_insert_sql = 
	"INSERT INTO `candidate_rel_professional_experiences`(`sm_memb_id`, `company_id`, `company_name`, `designation`, `location_city`, `location_state`, `location_country`, `from_month`, `from_year`, `to_month`, `to_year`, `is_current_company`, `sequence_order`, `added_by_user_id`, `added_date_time`, `added_date_time_epoch`) VALUES (:sm_memb_id,:company_id,:company_name,:designation,:location_city,:location_state,:location_country,:from_month,:from_year,:to_month,:to_year,:is_current_company,:sequence_order,:added_by_user_id,:added_date_time,:added_date_time_epoch)";
	
	
	$candidate_professional_summary_insert_query = $dbcon->prepare($candidate_professional_summary_insert_sql);
	$candidate_professional_summary_insert_query->bindValue(":sm_memb_id",$user_id_input);
	$candidate_professional_summary_insert_query->bindValue(":company_id",$company_id_input);
	$candidate_professional_summary_insert_query->bindValue(":company_name",$company_name);
	$candidate_professional_summary_insert_query->bindValue(":designation",$designation);
	$candidate_professional_summary_insert_query->bindValue(":location_city",$location_city);
	$candidate_professional_summary_insert_query->bindValue(":location_state",$location_state);
	$candidate_professional_summary_insert_query->bindValue(":location_country",$location_country);
	$candidate_professional_summary_insert_query->bindValue(":from_month",$from_month);
	$candidate_professional_summary_insert_query->bindValue(":from_year",$from_year);
	$candidate_professional_summary_insert_query->bindValue(":to_month",$to_month);
	$candidate_professional_summary_insert_query->bindValue(":to_year",$to_year);
	$candidate_professional_summary_insert_query->bindValue(":is_current_company",$is_current_company);
	$candidate_professional_summary_insert_query->bindValue(":sequence_order",$sequence_order);
	$candidate_professional_summary_insert_query->bindValue(":added_by_user_id",$ea_extracted_jwt_token_sub);
	$candidate_professional_summary_insert_query->bindValue(":added_date_time",$event_datetime);
	$candidate_professional_summary_insert_query->bindValue(":added_date_time_epoch",$current_epoch);
	
	if ($candidate_professional_summary_insert_query->execute()) {
		
		$last_inserted_id = $dbcon->lastInsertId();			
		//$eventLog->log("record inserted successfully");
		
		$constructed_array["last_inserted_id"] = $last_inserted_id;
		
		return $constructed_array;
					
	} else {
		//$eventLog->log("Error occurred during process. Please try again");						
			return $constructed_array;						
	}
}

function candidate_professional_summary_record_count($user_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	
	$candidate_professional_summary_record_count_sql = "SELECT COUNT(*) AS count FROM `candidate_rel_professional_experiences` WHERE `sm_memb_id`=:sm_memb_id";
	$candidate_professional_summary_record_count_select_query = $dbcon->prepare($candidate_professional_summary_record_count_sql);
	$candidate_professional_summary_record_count_select_query->bindValue(":sm_memb_id",$user_id_input);			
	$candidate_professional_summary_record_count_select_query->execute(); 
	
	if($candidate_professional_summary_record_count_select_query->rowCount() > 0) {
		$candidate_professional_summary_record_count_select_query_result = $candidate_professional_summary_record_count_select_query->fetch();
		$current_count = $candidate_professional_summary_record_count_select_query_result["count"];
		
		$constructed_array["current_count"] = $current_count;
		
	    return $constructed_array;
	
	} else {
		$constructed_array["current_count"] = 0;
		
		return $constructed_array;
		
	}//close of if($candidate_education_qualification_record_count_select_query->rowCount() > 0) {
	
	
}

function candidate_professional_summary_records_list($user_id_input) {
	global $dbcon;
	
	$constructed_array = array();
	
	$candidate_professional_summary_records_list_sql = "SELECT * FROM `candidate_rel_professional_experiences` WHERE `sm_memb_id`=:sm_memb_id";
	$candidate_professional_summary_records_list_select_query = $dbcon->prepare($candidate_professional_summary_records_list_sql);
	$candidate_professional_summary_records_list_select_query->bindValue(":sm_memb_id",$user_id_input);			
	$candidate_professional_summary_records_list_select_query->execute(); 
	
	if($candidate_professional_summary_records_list_select_query->rowCount() > 0) {
		$candidate_professional_summary_records_list_select_query_result = $candidate_professional_summary_records_list_select_query->fetchAll();
		return $candidate_professional_summary_records_list_select_query_result;
	
	}//close of if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
	return $constructed_array;
}

function delete_candidate_professional_summary_record($user_id_input, $db_based_company_name_field_value, $db_based_from_month_field_value, $db_based_from_year_field_value){
	global $dbcon;
	
	$candidate_company_name_input = '%'.$db_based_company_name_field_value.'%';
	$candidate_from_month_input = '%'.$db_based_from_month_field_value.'%';
	$candidate_from_year_input = '%'.$db_based_from_year_feild_value.'%';
	$candidate_professional_summary_delete_sql = "DELETE FROM `candidate_rel_professional_experiences` WHERE `sm_memb_id`=:sm_memb_id AND `company_name` LIKE :company_name AND `from_month` LIKE :from_month AND `from_year` LIKE :from_year";
	$candidate_professional_summary_delete_query = $dbcon->prepare($candidate_professional_summary_delete_sql);
	$candidate_professional_summary_delete_query->bindValue(":sm_memb_id",$user_id_input);	
	$candidate_professional_summary_delete_query->bindValue(":from_month",$candidate_from_month_input);	
	$candidate_professional_summary_delete_query->bindValue(":from_year",$candidate_from_year_input);		
	 
	if ($candidate_professional_summary_delete_query->execute()) {
		return true;
		
	}//close of if ($candidate_professional_summary_delete_query->execute()) {
	
	return false;
	
}

function delete_candidate_professional_summary_record_based_on_cps_id_input($crpe_id_input) {
	global $dbcon;
	
	$candidate_professional_summary_record_delete_sql = "DELETE FROM `candidate_rel_professional_experiences` WHERE `crpe_id`=:crpe_id";
	$candidate_professional_summary_record_delete_query = $dbcon->prepare($candidate_professional_summary_record_delete_sql);
	$candidate_professional_summary_record_delete_query->bindValue(":crpe_id",$crpe_id_input);		
	if ($candidate_professional_summary_record_delete_query->execute()) {
		return true;
		
	}//close of if ($candidate_professional_summary_record_delete_query->execute()) {
	
	return false;
	
}



function candidate_professional_summary_rel_file_upload_references_insert($user_id_input,$activity_ref_input,$activity_ref_id_input,$additional_document_ref_input,$original_filename_input,$generated_filename_input,$ea_extracted_jwt_token_user_type,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch){
    global $dbcon;
	
	$constructed_array = array();
	
	$candidate_professional_summary_rel_file_upload_references_insert_sql = "INSERT INTO `candidate_rel_all_uploaded_visa_documents`(`sm_memb_id`, `activity_ref`, `activity_ref_id`, `additional_document_ref`, `original_filename`, `generated_filename`, `added_by_user_type`, `added_by_user_id`, `added_date_time`, `added_date_time_epoch`) VALUES (:sm_memb_id,:activity_ref,:activity_ref_id,:additional_document_ref,:original_filename,:generated_filename,:added_by_user_type,:added_by_user_id,:added_date_time,:added_date_time_epoch)";
	$candidate_professional_summary_rel_file_upload_references_insert_q = $dbcon->prepare($candidate_professional_summary_rel_file_upload_references_insert_sql);
	$candidate_professional_summary_rel_file_upload_references_insert_q->bindValue(":sm_memb_id",$user_id_input);
	$candidate_professional_summary_rel_file_upload_references_insert_q->bindValue(":activity_ref",$activity_ref_input);
	$candidate_professional_summary_rel_file_upload_references_insert_q->bindValue(":activity_ref_id",$activity_ref_id_input);
	$candidate_professional_summary_rel_file_upload_references_insert_q->bindValue(":additional_document_ref",$additional_document_ref_input);
	$candidate_professional_summary_rel_file_upload_references_insert_q->bindValue(":original_filename",$original_filename_input);
	$candidate_professional_summary_rel_file_upload_references_insert_q->bindValue(":generated_filename",$generated_filename_input);
	$candidate_professional_summary_rel_file_upload_references_insert_q->bindValue(":added_by_user_type",$ea_extracted_jwt_token_user_type);
	$candidate_professional_summary_rel_file_upload_references_insert_q->bindValue(":added_by_user_id",$ea_extracted_jwt_token_sub);
	$candidate_professional_summary_rel_file_upload_references_insert_q->bindValue(":added_date_time",$event_datetime);
	$candidate_professional_summary_rel_file_upload_references_insert_q->bindValue(":added_date_time_epoch",$current_epoch);
	
	if ($candidate_professional_summary_rel_file_upload_references_insert_q->execute()) {
		$last_inserted_id = $dbcon->lastInsertId();			
		//$eventLog->log("record inserted successfully");
		
		$constructed_array["last_inserted_id"] = $last_inserted_id;
		
		return $constructed_array;
					
	} else {
		//$eventLog->log("Error occurred during process. Please try again");						
		return $constructed_array;						
	}//close of else of if ($candidate_education_qualification_rel_personal_info_insert_q->execute()) {
		
}

function company_client_spoc_details_duplicate_check_based_on_id($company_client_spoc_name_id) {
	
	global $dbcon;
	
	$constructed_array = array();
	
	
	
	$candidate_skill_certificate_check_sql = "SELECT * FROM `candidate_rel_professional_experiences` WHERE `crpe_id`=:crpe_id";
	$candidate_skill_certificate_check_select_query = $dbcon->prepare($candidate_skill_certificate_check_sql);
	$candidate_skill_certificate_check_select_query->bindValue(":crpe_id",$company_client_spoc_name_id);
   
	$candidate_skill_certificate_check_select_query->execute(); 
	
	if($candidate_skill_certificate_check_select_query->rowCount() > 0) {
		$candidate_skill_certificate_check_select_query_result = $candidate_skill_certificate_check_select_query->fetch();
	    return $candidate_skill_certificate_check_select_query_result;
	
	}//close of if($candidate_education_qualification_check_select_query->rowCount() > 0) {
	return $constructed_array;
	
}

exit;
?>