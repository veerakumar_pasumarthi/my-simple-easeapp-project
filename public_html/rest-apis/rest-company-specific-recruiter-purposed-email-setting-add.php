<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					
					/* if (isset($ea_received_rest_ws_raw_array_input['recruiter_role_to_sales_user_assignment_status'])) {
						$content .= $ea_received_rest_ws_raw_array_input['recruiter_role_to_sales_user_assignment_status'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['recruiter_role_to_sales_user_assignment_status'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['sales_role_to_recruiter_user_assignment_status'])) {
						$content .= $ea_received_rest_ws_raw_array_input['sales_role_to_recruiter_user_assignment_status'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['sales_role_to_recruiter_user_assignment_status'])) */ 
						
					if (isset($ea_received_rest_ws_raw_array_input['company_recruiter_purposed_email'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_recruiter_purposed_email'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_recruiter_purposed_email'])) 	
					
					 
                    
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				
				/* $recruiter_role_to_sales_user_assignment_status_input = trim(isset($ea_received_rest_ws_raw_array_input['recruiter_role_to_sales_user_assignment_status']) ? filter_var($ea_received_rest_ws_raw_array_input['recruiter_role_to_sales_user_assignment_status'], FILTER_SANITIZE_STRING) : '');
				
				$sales_role_to_recruiter_user_assignment_status_input = trim(isset($ea_received_rest_ws_raw_array_input['sales_role_to_recruiter_user_assignment_status']) ? filter_var($ea_received_rest_ws_raw_array_input['sales_role_to_recruiter_user_assignment_status'], FILTER_SANITIZE_STRING) : ''); */
				
				
				

				
				
				
				
			
				 $company_recruiter_purposed_email_input = trim(isset($ea_received_rest_ws_raw_array_input['company_recruiter_purposed_email']) ? filter_var($ea_received_rest_ws_raw_array_input['company_recruiter_purposed_email'], FILTER_SANITIZE_EMAIL) : '');
				
				
				 if (!filter_var($company_recruiter_purposed_email_input, FILTER_VALIDATE_EMAIL) == true) {
					
					$eventLog->log($company_recruiter_purposed_email_input . " - Not a Valid Email Address");
					$company_recruiter_purposed_email_input = "";
					
				}  
				
				 		
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				if ($company_id_input == "") {
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "missing company id";
					
					$eventLog->log("missing-company-id: Please provide a valid Company id.");
					
				} /* else if ($recruiter_role_to_sales_user_assignment_status_input == "") {
					//Invalid Mobile Number scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-recruiter-role-to-sales-user-assignment-status";
					$response['status_description'] = "Missing recruiter role to sales user assignment status";
					
					$eventLog->log("missing-recruiter-role-to-sales-user-assignment-status: Missing recruiter role to sales user assignment status.");
					
				} else if ($sales_role_to_recruiter_user_assignment_status_input == "") {
					//Invalid Salutation scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-sales-role-to-recruiter-user-assignment-status";
					$response['status_description'] = "Missing sales role to recruiter user assignment status";
					
					$eventLog->log("missing-sales-role-to-recruiter-user-assignment-status: Missing sales role to recruiter user assignment status.");
				} */else if ($company_recruiter_purposed_email_input == "") {
					//Invalid Salutation scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-recruiter-purposed-email";
					$response['status_description'] = "Missing Company recruiter purposed email ";
					
					$eventLog->log("missing-company-recruiter-purposed-email: Please provide a valid company recruiter purposed email.");	
				
				} else if ($ip_address_input == " ") {
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Some Additional information is missing,Please check and try again.";
					
					$eventLog->log("missing-some-additional-information: Some Additional information is missing,Please check and try again.");
					
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					$company_specific_settings_duplicate_check_result = company_specific_settings_duplicate_check_based_on_company_id($company_id_input);
					if (count($company_specific_settings_duplicate_check_result) > 0) {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "company-specific-recruiter-purposed-email-settings-already-exists";
						$response['status_description'] = "Company specific recruiter purposed email settings already exists";
						
                        $eventLog->log("company-specific-recruiter-purposed-email-settings-already-exists:Company specific recruiter purposed email settings already exists");						
					
					} else {
						
						//Event Time, as per Indian Standard Time
						$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
						$event_expiry_datetime_epoch = $current_epoch+$password_resetting_link_expiry_period;	
						//$valid_from_date_input = '2018-11-10 16:18:20';
						
						
						$company_specific_settings_add_next_step = "";
						
						if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$company_specific_settings_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$company_specific_settings_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
						} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
						}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
							
						if ($company_specific_settings_add_next_step == "PROCEED-TO-NEXT-STEP") {
							try {
								
								/*COMMENTED on 05-04-2019 20:08
								//Check if the User, whose sm_memb_id is received from "sub" of JWT Auth Token exists
								if (count($ea_token_based_sub_user_id_details) > 0) {
									
									//$eventLog->log($ea_token_based_sub_user_id_details["sm_lastname"]);
														
									$last_inserted_id = company_basic_details_insert($company_name_input,$company_seo_name_input,$company_brand_name_input,$company_registration_type_id_input,$company_registration_type_name_input,$company_registration_type_seo_name_input,$company_mobile_number_input,$company_website_url_input, $company_email_input, $company_support_email_input); 
									
								} else {
									$last_inserted_id = company_basic_details_insert($company_name_input,$company_seo_name_input,$company_brand_name_input,$company_registration_type_id_input,$company_registration_type_name_input,$company_registration_type_seo_name_input,$company_mobile_number_input,$company_website_url_input,$company_email_input,$company_support_email_input);
								    
								}//close of else of if (count($ea_token_based_sub_user_id_details) > 0) {
								*/
								$eventLog->log("before insert query");
								
								$last_inserted_id = company_specific_settings_details_insert($company_id_input,$company_recruiter_purposed_email_input,$event_datetime,$current_epoch,$ea_extracted_jwt_token_sub);
								   $eventLog->log("after insert query"); 
								
								$eventLog->log($last_inserted_id);
								
									
									if ($last_inserted_id != "") {
										$company_status_change_result = company_status_change($company_id_input); 
										if($company_status_change_result == true){
										    $response['data'] = array();
											$response['status'] = "company-specific-recruiter-purposed-email-setting-insertion-successful";
											$response['status_description'] = "company specific recruiter purposed email setting insertion successful.";
										
											$eventLog->log("company-specific-recruiter-purposed-email-setting-insertion-successful: company specific recruiter purposed email setting insertion successful.");   
										
									} else {
										//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
										$response['data'] = array();
										$response['status'] = "company-specific-recruiter-purposed-email-setting-insertion-error";
										$response['status_description'] = "Error occurred when adding the company specific recruiter purposed email setting in the Database.";
										
										$eventLog->log("company-specific-recruiter-purposed-email-setting-insertion-error: Error occurred when adding the company specific recruiter purposed email setting in the Database.");
										
									}
									
								
								}	
							         
									
								
							} catch (Exception $e) {
								
								//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
								$response['data'] = array();
								$response['status'] = "company-specific-recruiter-purposed-email-setting-insertion-error";
								$response['status_description'] = "Error occurred when adding the company specific recruiter purposed email setting in the Database.";
								
								$eventLog->log("company-specific-recruiter-purposed-email-setting-insertion-error: Error occurred when adding the company specific recruiter purposed email setting in the Database.");
							}
						}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							
						
					}
				}//close of else of if ($email_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
    
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
			
	
	}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
function company_specific_settings_duplicate_check_based_on_company_id($company_id_input) {

	global $dbcon;
	$constructed_array = array();
	
	$company_specific_settings_duplicate_check_sql = "SELECT * FROM `company_specific_settings` WHERE `company_id`=:company_id";
	$company_specific_settings_details_select_query = $dbcon->prepare($company_specific_settings_duplicate_check_sql);
	$company_specific_settings_details_select_query->bindValue(":company_id",$company_id_input);
	$company_specific_settings_details_select_query->execute();

	if($company_specific_settings_details_select_query->rowCount() > 0) {
		$company_specific_settings_details_select_query_result = $company_specific_settings_details_select_query->fetch();
	     return $company_specific_settings_details_select_query_result;

	}//close of if($company_specific_settings_details_select_query->rowCount() > 0) {
	return $constructed_array;

}	
function company_specific_settings_details_insert($company_id_input,$company_recruiter_purposed_email_input,$event_datetime,$current_epoch,$ea_extracted_jwt_token_sub) {
	global $dbcon,$eventLog;
	$is_active_status = '1';
    $recruiter_role_to_sales_user_assignment_status_input = '1';
	$sales_role_to_recruiter_user_assignment_status_input ='1';
	$eventLog->log("before insert query");

	$company_specific_settings_details_insert_sql = "INSERT INTO `company_specific_settings`(`company_id`, `recruiter_role_to_sales_user_assignment_status`, `sales_role_to_recruiter_user_assignment_status`, `company_recruiter_purposed_email`, `added_datetime`, `added_datetime_epoch`, `added_by_sm_memb_id`, `is_active_status`) VALUES (:company_id,:recruiter_role_to_sales_user_assignment_status,:sales_role_to_recruiter_user_assignment_status,:company_recruiter_purposed_email,:added_datetime,:added_datetime_epoch,:added_by_sm_memb_id,:is_active_status)";
	$eventLog->log("after insert query");

	$company_specific_settings_details_insert_query = $dbcon->prepare($company_specific_settings_details_insert_sql);
	$company_specific_settings_details_insert_query->bindValue(":company_id",$company_id_input);
	$company_specific_settings_details_insert_query->bindValue(":recruiter_role_to_sales_user_assignment_status",$recruiter_role_to_sales_user_assignment_status_input);
	$company_specific_settings_details_insert_query->bindValue(":sales_role_to_recruiter_user_assignment_status",$sales_role_to_recruiter_user_assignment_status_input);
	$company_specific_settings_details_insert_query->bindValue(":company_recruiter_purposed_email",$company_recruiter_purposed_email_input);
	$company_specific_settings_details_insert_query->bindValue(":added_datetime",$event_datetime);
	$company_specific_settings_details_insert_query->bindValue(":added_datetime_epoch",$current_epoch);
	$company_specific_settings_details_insert_query->bindValue(":added_by_sm_memb_id",$ea_extracted_jwt_token_sub);
    $company_specific_settings_details_insert_query->bindValue(":is_active_status",$is_active_status);
$eventLog->log("before excute");

		if ($company_specific_settings_details_insert_query->execute()) {
$eventLog->log("after excute");

            $last_inserted_id = $dbcon->lastInsertId();
			$eventLog->log("last-inserted-id=>".$last_inserted_id);

			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}
function company_status_change($company_id_input) {
	global $dbcon, $eventLog;
	$is_active_status = '1';
	$eventLog->log("in function before query");
    
	$company_status_change_update_sql = "UPDATE `companies` SET `is_active_status`=:is_active_status WHERE `company_id`=:company_id";
	$company_status_change_update_query = $dbcon->prepare($company_status_change_update_sql);
	$eventLog->log("in function before bind");
	
	$company_status_change_update_query->bindValue(":company_id",$company_id_input);
	$company_status_change_update_query->bindValue(":is_active_status",$is_active_status);
	
	$eventLog->log("in function after bind");

	if ($company_status_change_update_query->execute()) {
		
		$eventLog->log("in function enter into if");

		return true;

	}
	return false;
}
exit;
?>