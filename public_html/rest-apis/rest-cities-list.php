<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "2")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
				$state_id_input = trim(isset($ea_received_rest_ws_raw_array_input['state_id']) ? filter_var($ea_received_rest_ws_raw_array_input['state_id'],
				FILTER_SANITIZE_STRING) : '');
				
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                 
				 
				  if ($state_id_input == "") {
					//Invalid petitioner id scenario
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-state-id";
					$response['status_description'] = "Missing state id, please check and try again.";
					
					$eventLog->log("Please provide a valid state id.");
					
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Invalid IP Address is Submitted, Please check and try again.";
					
					$eventLog->log("Please provide valid ip_address.");
					
				} else {
					//all inputs are valid
					$cities_types_list_info_result = cities_types_list_info($state_id_input);
					if (count($cities_types_list_info_result) > 0) {
						
						$response['data'] = $cities_types_list_info_result;
						$response['status'] = "cities-types-list-successfully-fetched";
						$response['status_description'] = "cities Types List Successfully Received";
					} 			
					else {
							
							$response['data'] = array();
							$response['status'] = "cities-types-list-not-found";
							$response['status_description'] = "cities Types List Not Successfully Received.";

                           }
				}
					
			}
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "1")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	function cities_types_list_info($state_id) {

    global $dbcon;
	$constructed_array = array();
	$is_active_status = '1';
		$cities_types_list_get_sql = "SELECT * FROM `cities` WHERE `is_active_status`= :is_active_status AND `state_id`=:state_id";
		$cities_types_list_get_select_query = $dbcon->prepare($cities_types_list_get_sql);
		$cities_types_list_get_select_query->bindValue(":is_active_status",$is_active_status);
		$cities_types_list_get_select_query->bindValue(":state_id",$state_id);
		$cities_types_list_get_select_query->execute();

	if($cities_types_list_get_select_query->rowCount() > 0) {
	    $cities_types_list_get_select_query_result = $cities_types_list_get_select_query->fetchAll();
		foreach ($cities_types_list_get_select_query_result as $cities_types_list_get_select_query_result_row) {
			/*$constructed_string = $cities_types_list_get_select_query_result_row["city_id"]. ":::::" .$cities_types_list_get_select_query_result_row["country_id"]. ":::::" .$cities_types_list_get_select_query_result_row["state_id"]. ":::::" .$cities_types_list_get_select_query_result_row["city_name"]. ":::::" .$cities_types_list_get_select_query_result_row["zipcode_or_pincode"]. ":::::" 
			.$cities_types_list_get_select_query_result_row["dst"]. ":::::" .$cities_types_list_get_select_query_result_row["longitude"]. ":::::" .$cities_types_list_get_select_query_result_row["latitude"]. ":::::" 
			.$cities_types_list_get_select_query_result_row["timezone_offset"];*/
			$temp_row_array = array();
			$temp_row_array["city_id"] = $cities_types_list_get_select_query_result_row["city_id"];
		    $temp_row_array["country_id"] = $cities_types_list_get_select_query_result_row["country_id"];
		    $temp_row_array["state_id"] = $cities_types_list_get_select_query_result_row["state_id"];
		    $temp_row_array["state_two_lettered_code"] = $cities_types_list_get_select_query_result_row["state_two_lettered_code"];
			$temp_row_array["city_name"] = $cities_types_list_get_select_query_result_row["city_name"];
			$temp_row_array["zipcode_or_pincode"] = $cities_types_list_get_select_query_result_row["zipcode_or_pincode"];
			$temp_row_array["dst"] = $cities_types_list_get_select_query_result_row["dst"];
			$temp_row_array["longitude"] = $cities_types_list_get_select_query_result_row["longitude"];
			$temp_row_array["latitude"] = $cities_types_list_get_select_query_result_row["latitude"];
			$temp_row_array["timezone_offset"] = $cities_types_list_get_select_query_result_row["timezone_offset"];
		    $temp_row_array["is_active_status"] = $cities_types_list_get_select_query_result_row["is_active_status"];
			//$temp_row_array["constructed_string"] = $constructed_string;
		    $constructed_array[] = $temp_row_array;
	    }

		return $constructed_array;
	}
	return $constructed_array;
}

exit;
?>