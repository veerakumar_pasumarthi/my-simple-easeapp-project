<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Edit Quick Admin Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_client_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['client_company_name'])) {
						$content .= $ea_received_rest_ws_raw_array_input['client_company_name'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['client_company_name']))
						
					if (isset($ea_received_rest_ws_raw_array_input['client_company_brand_name'])) {
						$content .= $ea_received_rest_ws_raw_array_input['client_company_brand_name'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['client_company_brand_name'])) 	
					
					if (isset($ea_received_rest_ws_raw_array_input['client_company_registration_type'])) {
						$content .= $ea_received_rest_ws_raw_array_input['client_company_registration_type'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['client_company_registration_type'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['client_company_mobile_number'])) {
						$content .= $ea_received_rest_ws_raw_array_input['client_company_mobile_number'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['client_company_mobile_number']))
					if (isset($ea_received_rest_ws_raw_array_input['client_company_mobile_number_code'])) {
						$content .= $ea_received_rest_ws_raw_array_input['client_company_mobile_number_code'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['client_company_mobile_number_code']))	
					
					if (isset($ea_received_rest_ws_raw_array_input['client_company_website_url'])) {
						$content .= $ea_received_rest_ws_raw_array_input['client_company_website_url'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['client_company_website_url'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['client_company_email'])) {
						$content .= $ea_received_rest_ws_raw_array_input['client_company_email'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['client_company_email'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['client_company_support_email'])) {
						$content .= $ea_received_rest_ws_raw_array_input['client_company_support_email'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['client_company_support_email'])) 	
						
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$company_client_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_client_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_client_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$client_company_name_input = trim(isset($ea_received_rest_ws_raw_array_input['client_company_name']) ? filter_var($ea_received_rest_ws_raw_array_input['client_company_name'], FILTER_SANITIZE_STRING) : '');
				$client_company_brand_name_input = trim(isset($ea_received_rest_ws_raw_array_input['client_company_brand_name']) ? filter_var($ea_received_rest_ws_raw_array_input['client_company_brand_name'], FILTER_SANITIZE_STRING) : '');
				$client_company_registration_type_input = trim(isset($ea_received_rest_ws_raw_array_input['client_company_registration_type']) ? filter_var($ea_received_rest_ws_raw_array_input['client_company_registration_type'], FILTER_SANITIZE_STRING) : '');
				
				$eventLog->log("client_company_registration_type_input => ".$client_company_registration_type_input);
				
				$client_company_registration_type_input_exploded = explode(":::::",$client_company_registration_type_input);
				
				$client_company_registration_type_input_exploded_count = count($client_company_registration_type_input_exploded);
				
				$client_company_registration_type_id_input = "";
					
				$client_company_registration_type_name_input = "";
				
				$client_company_registration_type_seo_name_input = "";
				
				if ($client_company_registration_type_input_exploded_count == "3") {
					
					$client_company_registration_type_id_input = trim(isset($client_company_registration_type_input_exploded[0]) ? filter_var($client_company_registration_type_input_exploded[0], FILTER_SANITIZE_NUMBER_INT) : '');
					
					$client_company_registration_type_name_input = trim(isset($client_company_registration_type_input_exploded[1]) ? filter_var($client_company_registration_type_input_exploded[1], FILTER_SANITIZE_STRING) : '');
					
					$client_company_registration_type_seo_name_input = trim(isset($client_company_registration_type_input_exploded[2]) ? filter_var($client_company_registration_type_input_exploded[2], FILTER_SANITIZE_STRING) : '');
					
				} else {
				    $client_company_registration_type_id_input = null ;
				    $client_company_registration_type_name_input = null ;
				    $client_company_registration_type_seo_name_input = null ;
				}
				//close of if ($company_registration_type_input_exploded_count != "3") {
				$client_company_mobile_number_input = trim(isset($ea_received_rest_ws_raw_array_input['client_company_mobile_number']) ? filter_var($ea_received_rest_ws_raw_array_input['client_company_mobile_number'], FILTER_SANITIZE_NUMBER_INT) : '');
				$client_company_mobile_number_code_input = trim(isset($ea_received_rest_ws_raw_array_input['client_company_mobile_number_code']) ? filter_var($ea_received_rest_ws_raw_array_input['client_company_mobile_number_code'], FILTER_SANITIZE_STRING) : '');
				$client_company_website_url_input = trim(isset($ea_received_rest_ws_raw_array_input['client_company_website_url']) ? filter_var($ea_received_rest_ws_raw_array_input['client_company_website_url'], FILTER_SANITIZE_URL) : '');
				$client_company_email_input = trim(isset($ea_received_rest_ws_raw_array_input['client_company_email']) ? filter_var($ea_received_rest_ws_raw_array_input['client_company_email'], FILTER_SANITIZE_EMAIL) : '');
				$client_company_support_email_input = trim(isset($ea_received_rest_ws_raw_array_input['client_company_support_email']) ? filter_var($ea_received_rest_ws_raw_array_input['client_company_support_email'], FILTER_SANITIZE_EMAIL) : '');
				
				
				
				
				if (!filter_var($client_company_email_input, FILTER_VALIDATE_EMAIL) == true) {
					
					$eventLog->log($client_company_email_input . " - Not a Valid Email Address");
					$client_company_email_input = "";
				}
					
				/*} else if (!filter_var($client_company_support_email_input, FILTER_VALIDATE_EMAIL) == true) {
					
					$eventLog->log($client_company_support_email_input . " - Not a Valid Email Address");
					$client_company_support_email_input = "";
				} 
				if (!filter_var($client_company_website_url_input, FILTER_VALIDATE_URL) == true) {
					
					$eventLog->log($client_company_website_url_input . " - Not a Valid url ");
					$client_company_website_url_input = null;
					
				}*/
				
				if (($client_company_mobile_number_input == '0') || (!ctype_digit($client_company_mobile_number_input))) {
						$eventLog->log($client_company_mobile_number_input . " - Not a Valid company Mobile Number");
						$client_company_mobile_number_input = "";
				} 
				
						
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				//$eventLog->log("company_id : " . $company_id_input);
				//Check if all inputs are received correctly from the REST Web Service
				if ($company_client_id_input == "") {
					//Invalid Mobile Number scenario
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-client-id";
					$response['status_description'] = "invalid company client id, please check and try again.";
					
					$eventLog->log("Please provide a  company client id.");
					
				} else if ($client_company_name_input == "") {
					//Invalid Salutation scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-client-company-name";
					$response['status_description'] = "Invalid client company name, please check and try again.";
					
					$eventLog->log("Please provide a valid client company name.");
				
				} else if ($client_company_email_input == "") {
					//Invalid First name scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-client-company-email";
					$response['status_description'] = "invalid client company email, please check and try again.";
					
					$eventLog->log("Please provide a valid client company email.");
				
				} else if ($client_company_brand_name_input == "") {
					//Invalid Last name scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-client-company-brand-name";
					$response['status_description'] = "invalid client company brand name, please check and try again.";
					
					$eventLog->log("Please provide a valid cliet company brand name.");	
					
				} else if (($ip_address_input == "")) {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Some Additional Information is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");	
				} /* else if ($client_company_registration_type_input_exploded_count != "3") {
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-client-company-registration-type-info";
					$response['status_description'] = "Invalid client Company Registration Type Info provided, Please check and try again.";
					
					$eventLog->log("invalid-client-company-registration-type-info: Invalid client Company Registration Type Info provided, Please check and try again.");
					
				} */ else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//Event Time, as per Indian Standard Time
					$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
		            $company_details_get_result = company_details_get($company_client_id_input);
					$company_id_input= $company_details_get_result["company_id"];
					$client_company_edit_next_step = "";
					
					if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$client_company_edit_next_step = "PROCEED-TO-NEXT-STEP";
							} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {	
							//} else if (isset($ea_extracted_jwt_token_user_company_id)) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$client_company_edit_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
					} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
					}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
							
						if ($client_company_edit_next_step == "PROCEED-TO-NEXT-STEP") {
						$client_company_basic_details_result = client_company_basic_details_check_based_on_company_client_id($company_client_id_input);
					
						if (count($client_company_basic_details_result) > 0) {
						
						
                       //if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
								$eventLog->log("before enter into query.");
								
								      
									  $client_company_basic_details_result_update = client_company_basic_details_result_update_based_on_company_client_id($company_client_id_input,$client_company_name_input,$client_company_brand_name_input,$client_company_registration_type_id_input,$client_company_registration_type_name_input,$client_company_registration_type_seo_name_input,$client_company_email_input,$client_company_support_email_input,$client_company_mobile_number_input,$client_company_mobile_number_code_input,$client_company_website_url_input,$event_datetime,$current_epoch);
									  
									  
									  
								      $eventLog->log("After update query.");
									  
												if ($client_company_basic_details_result_update== true) {
													$eventLog->log("enter into user ass if condition.");
													//Use Privilege details are updated successfully, for the user, in sm_site_member_classification_associations db table
													//Construct Content, that will be sent in Response body, of the REST Web Service
													$response['data'] = array();
													$response['status'] = "client-company-details-updated-successfully";
													$response['status_description'] = "Updated successfully.";
													
													$eventLog->log("The client company details are edited Successfully.");
													$eventLog->log("before update status.");
												} else {
													//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
													$response['data'] = array();
													$response['status'] = "client-company-data-updating-error";
													$response['status_description'] = "Update error.";
													
													$eventLog->log("There is an error, There is an error, when updating the client company in the Database.");
													
												}
										
										
								} catch (Exception $e) {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "client-company-updating-error";
									$response['status_description'] = "update error.";
									
									$eventLog->log("Error occurred when updating the client  company details.");	
									
								} 
					   
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "The User Account, that is attempted to be edited, is invalid, please check and try again.";
						
						$eventLog->log("Please provide a Valid User ID.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
					
						
				}//close of else of if ($email_id_input == "") {
				
			}
			
		}	
			
	} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function client_company_basic_details_result_update_based_on_company_client_id($company_client_id_input,$client_company_name_input,$client_company_brand_name_input,$client_company_registration_type_id_input,$client_company_registration_type_name_input,$client_company_registration_type_seo_name_input,$client_company_email_input,$client_company_support_email_input,$client_company_mobile_number_input,$client_company_mobile_number_code_input,$client_company_website_url_input,$event_datetime,$current_epoch) {
	global $dbcon, $eventLog;
	
	$eventLog->log("in function before query");
    
	$client_company_details_update_sql = "UPDATE `company_clients` SET `client_company_name`=:client_company_name,`client_company_brand_name`=:client_company_brand_name,`client_company_registration_type_id`=:client_company_registration_type_id,`client_company_registration_type_name`=:client_company_registration_type_name,`client_company_registration_type_seo_name`=:client_company_registration_type_seo_name,`client_company_email`=:client_company_email,`client_company_support_email`=:client_company_support_email,`client_company_mobile_number`=:client_company_mobile_number,`client_mobile_country_code`=:client_mobile_country_code,`client_company_website_url`=:client_company_website_url,`last_updated_datetime`=:last_updated_datetime,`last_updated_datetime_epoch`=:last_updated_datetime_epoch WHERE `company_client_id`=:company_client_id";
	$client_company_details_update_query = $dbcon->prepare($client_company_details_update_sql);
	$eventLog->log("in function before bind");
	
	$client_company_details_update_query->bindValue(":company_client_id",$company_client_id_input);
	$client_company_details_update_query->bindValue(":client_company_name",$client_company_name_input);
	$client_company_details_update_query->bindValue(":client_company_brand_name",$client_company_brand_name_input);
	$client_company_details_update_query->bindValue(":client_company_registration_type_id",$client_company_registration_type_id_input);
	$client_company_details_update_query->bindValue(":client_company_registration_type_name",$client_company_registration_type_name_input);
	$client_company_details_update_query->bindValue(":client_company_registration_type_seo_name",$client_company_registration_type_seo_name_input);
	$client_company_details_update_query->bindValue(":client_company_email",$client_company_email_input);
	$client_company_details_update_query->bindValue(":client_company_support_email",$client_company_support_email_input);
	$client_company_details_update_query->bindValue(":client_company_mobile_number",$client_company_mobile_number_input);
	$client_company_details_update_query->bindValue(":client_mobile_country_code",$client_company_mobile_number_code_input);
	$client_company_details_update_query->bindValue(":client_company_website_url",$client_company_website_url_input);
	$client_company_details_update_query->bindValue(":last_updated_datetime",$event_datetime);
	$client_company_details_update_query->bindValue(":last_updated_datetime_epoch",$current_epoch);
	
	$eventLog->log("in function after bind");

	if ($client_company_details_update_query->execute()) {
		
		$eventLog->log("in function enter into if");

		return true;

	}
	return false;
}	
function company_details_get($company_client_id_input) {
	global $dbcon;
	$constructed_array = array();
	$company_details_check_sql = "SELECT * FROM `company_clients` WHERE `company_client_id` = :company_client_id";
	$company_details_check_select_query = $dbcon->prepare($company_details_check_sql);
	$company_details_check_select_query->bindValue(":company_client_id",$company_client_id_input);
	$company_details_check_select_query->execute();

	if($company_details_check_select_query->rowCount() > 0) {
		$company_details_check_select_query_result = $company_details_check_select_query->fetch();
	     return $company_details_check_select_query_result;

	}//close of if($company_details_check_select_query->rowCount() > 0) {
	return $constructed_array;
	}
exit;
?>


