<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Edit Quick Admin Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['user_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['user_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['user_id'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				
				if ($user_id_input == "") {
					//Invalid Mobile Number scenario
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-user-id";
					$response['status_description'] = "invalid user id, please check and try again.";
					
					$eventLog->log("Please provide a valid user id.");
					
				
				
				} else if ($company_id_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "missing company id, please check and try again.";
					
					$eventLog->log("Please provide all information.");	
				
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Some Additional Information is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//Event Time, as per Indian Standard Time
					$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
		
					/*$user_additional_informaton_details_proceed_next_step = "";
					
					
					if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$user_additional_informaton_details_proceed_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$user_additional_informaton_details_proceed_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
					} else {
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
					}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
							
					if ($user_additional_informaton_details_proceed_next_step == "PROCEED-TO-NEXT-STEP") {
						*/
							try {
								    $user_additional_information_details_result = user_additional_details_check_based_on_user_id($user_id_input,$company_id_input);
								    if(count($user_additional_information_details_result) > 0){
										 
										$response['data'] = $user_additional_information_details_result;
										$response['status'] = "user-additional-details-successfully-fetched";
										$response['status_description'] = "user additional details fetched Successfully.";

										$eventLog->log("The user additional details fetched Successfully.");
													     
									} else {
									    
										$response['data'] = $user_additional_information_details_result;
										$response['status'] = "no-additional-details-found-for-this-candidate";
										$response['status_description'] = "No additional details found for this Candidate.";
										
										$eventLog->log("No additional details found for this Candidate.");
													
									}			
								} catch (Exception $e) {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "user-additional- details-fetching-error";
									$response['status_description'] = "There is an error, when fetching user additional details.";
									
									$eventLog->log("There is an error, when fetching user additional details.");	
									
								}
					   
						
					/*} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "The User Account, that is attempted to be edited, is invalid, please check and try again.";
						
						$eventLog->log("Please provide a Valid user ID.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
					*/
						
				}//close of else of if ($email_id_input == "") {
				
			}
			
		//}	
			
	} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	}
	
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	

	
function user_additional_details_check_based_on_user_id($user_id_input,$company_id_input) {

	global $dbcon, $eventLog;
	$constructed_array = array();
    $is_active_status ='1';
	$user_additional_details_get_sql = "SELECT * FROM site_members sm JOIN candidate_rel_additional_info crai ON crai.sm_memb_id = sm.sm_memb_id  JOIN candidate_rel_preferred_job_locations crpjl ON crpjl.sm_memb_id= sm.sm_memb_id WHERE crai.sm_memb_id =:user_id AND sm.company_id=:company_id";
	$user_aditional_details_select_query = $dbcon->prepare($user_additional_details_get_sql);
    $user_aditional_details_select_query->bindValue(":user_id",$user_id_input);
	$user_aditional_details_select_query->bindValue(":company_id",$company_id_input);


	$user_aditional_details_select_query->execute();
   //var_dump($user_aditional_details_select_query);
	if($user_aditional_details_select_query->rowCount() > 0) {
		$eventLog->log("in if condition");
	    $user_additional_details_select_query_result = $user_aditional_details_select_query->fetchAll();
		
		foreach($user_additional_details_select_query_result as $user_additional_details_select_query_result_row) {
          
		    $constructed_array["crai_id"] = $user_additional_details_select_query_result_row["crai_id"];
		    $constructed_array["company_id"] = $user_additional_details_select_query_result_row["company_id"];
			$constructed_array["sm_job_experience_level"] = $user_additional_details_select_query_result_row["sm_job_experience_level"];
		    $constructed_array["experience_years"] = $user_additional_details_select_query_result_row["experience_years"];
			$constructed_array["experience_months"] = $user_additional_details_select_query_result_row["experience_months"];
			$constructed_array["current_salary"] = $user_additional_details_select_query_result_row["current_salary"];
			$constructed_array["preferred_salary"] = $user_additional_details_select_query_result_row["preferred_salary"];
			$constructed_array["on_notice_period"] = $user_additional_details_select_query_result_row["on_notice_period"];
			$constructed_array["notice_period"] = $user_additional_details_select_query_result_row["notice_period"];
			$constructed_array["notice_period_metric"] = $user_additional_details_select_query_result_row["notice_period_metric"];
			$constructed_array["crpjl_id"] = $user_additional_details_select_query_result_row["crpjl_id"];
			$constructed_array["city_name"] = $user_additional_details_select_query_result_row["city_name"];
			$constructed_array["state_name"] = $user_additional_details_select_query_result_row["state_name"];
			$constructed_array["country_name"] = $user_additional_details_select_query_result_row["country_name"];
			
		    $constructed_array["is_active_status"] = $user_additional_details_select_query_result_row["is_active_status"];
		   // var_dump($constructed_array);
	    }

		return $constructed_array;
	}
	return $constructed_array;

}
exit;
?>


