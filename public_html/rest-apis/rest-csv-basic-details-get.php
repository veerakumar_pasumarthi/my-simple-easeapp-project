<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Edit Quick Admin Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "2")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_client_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_client_id'])) 
						
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$company_client_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_client_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_client_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				
				
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				
				if ($company_client_id_input == "") {
					//Invalid Mobile Number scenario
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-client-id";
					$response['status_description'] = "invalid company client id, please check and try again.";
					
					$eventLog->log("Please provide a valid company client id.");
					
				
				
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Some Additional Information is missing, please check and try again.";
					
					$eventLog->log("Please provide all information.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					//Event Time, as per Indian Standard Time
					$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
		
					
						
							try {
								    $client_single_view_basic_details_result = client_company_basic_details_check_based_on_id($company_client_id_input);
								    if(count($client_single_view_basic_details_result) > 0){
										  
										$response['data'] = $client_single_view_basic_details_result;
										$response['status'] = "csv-basic-details-successfully-fetched";
										$response['status_description'] = "CSV basic details fetched Successfully.";

										$eventLog->log("The CSV basic details fetchedSuccessfully.");
													     
									} else {
									
										$response['data'] = array();
										$response['status'] = "csv-basic-details-fetching-error";
										$response['status_description'] = "There is an error, when fetching csv basic details.";
										
										$eventLog->log("There is an error, when fetching csv basic details.");
													
									}			
								} catch (Exception $e) {
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "csv-basic-details-fetching-error";
									$response['status_description'] = "There is an error, when fetching csv basic details.";
									
									$eventLog->log("There is an error, when fetching csv basic details.");	
									
								}
					   
						
					
						
				}//close of else of if ($email_id_input == "") {
				
			}
			
		}	
			
	} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "9")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	

	
/*function client_company_basic_details_check_based_on_company_client_id($company_client_id_input) {
	global $dbcon;
	$constructed_array = array();
	$rest_client_company_check_sql = "SELECT * FROM `company_clients` WHERE `company_client_id` = :company_client_id";
	$rest_client_company_check_select_query = $dbcon->prepare($rest_client_company_check_sql);
	$rest_client_company_check_select_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_company_check_select_query->execute();

	if($rest_client_company_check_select_query->rowCount() > 0) {
		$rest_client_company_check_select_query_result = $rest_client_company_check_select_query->fetch();
		foreach ($rest_client_company_check_select_query_result as $client_company_check_select_query_result_row) {
			
			$temp_row_array = array();
			$temp_row_array["client_company_name"] = $client_company_check_select_query_result_row["client_company_name"];
		    $temp_row_array["client_company_email"] = $client_company_check_select_query_result_row["client_company_email"];
		    $temp_row_array["client_company_brand_name"] = $client_company_check_select_query_result_row["client_company_brand_name"];
		    $temp_row_array["client_company_support_email"] = $client_company_check_select_query_result_row["client_company_support_email"];
			$temp_row_array["client_company_website_url"] = $client_company_check_select_query_result_row["client_company_website_url"];
			$temp_row_array["client_company_mobile_number"] = $client_company_check_select_query_result_row["client_company_mobile_number"];
			
			 }

		return $constructed_array;
	}
	return $constructed_array;
}*/

function client_company_basic_details_check_based_on_id($company_client_id_input) {
	global $dbcon;
	$constructed_array = $temp_array = array();
	$rest_client_company_check_sql = "SELECT * FROM `company_clients` WHERE `company_client_id` = :company_client_id";
	$rest_client_company_check_select_query = $dbcon->prepare($rest_client_company_check_sql);
	$rest_client_company_check_select_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_company_check_select_query->execute();

	if($rest_client_company_check_select_query->rowCount() > 0) {
		$job_rel_candidature_screening_levels_list_query_result = $rest_client_company_check_select_query->fetchAll();
		
		foreach($job_rel_candidature_screening_levels_list_query_result as $job_rel_candidature_screening_levels_list_query_result_row) {
		//$rest_client_company_check_select_query_result = $rest_client_company_check_select_query->fetch();
		 $constructed_array['client_company_name'] = $job_rel_candidature_screening_levels_list_query_result_row['client_company_name'];
	      $constructed_array['client_company_brand_name'] = $job_rel_candidature_screening_levels_list_query_result_row['client_company_brand_name'];
	       $constructed_array['client_company_email'] = $job_rel_candidature_screening_levels_list_query_result_row['client_company_email'];
	        $constructed_array['client_company_support_email'] = $job_rel_candidature_screening_levels_list_query_result_row['client_company_support_email'];
	         $constructed_array['client_company_mobile_number'] = $job_rel_candidature_screening_levels_list_query_result_row['client_company_mobile_number'];
	          $constructed_array['client_company_website_url'] = $job_rel_candidature_screening_levels_list_query_result_row['client_company_website_url'];
	           $constructed_array['is_active_status'] = $job_rel_candidature_screening_levels_list_query_result_row['is_active_status'];
	     $company_client_basic_details_result = company_office_addresses_duplicate_check_based_on_id($company_client_id_input);
     if(count($company_client_basic_details_result) > 0){

             $constructed_array['address_line_1'] = $company_client_basic_details_result['address_line_1'];
	        $constructed_array['address_line_2'] = $company_client_basic_details_result['address_line_2'];
	         $constructed_array['city'] = $company_client_basic_details_result['city'];
	          $constructed_array['state'] = $company_client_basic_details_result['state'];
	          $constructed_array['country'] = $company_client_basic_details_result['country'];
	          $constructed_array['pincode'] = $company_client_basic_details_result['pincode'];
	       } else {
	       	 $constructed_array['address_line_1'] = null;
	        $constructed_array['address_line_2'] = null;
	         $constructed_array['city'] = null;
	          $constructed_array['state'] = null;
	          $constructed_array['country'] = null;
	          $constructed_array['pincode'] = null;
	       }
	    $rest_client_closed_check_sql = "SELECT * FROM `jobs` WHERE `company_client_id` = :company_client_id AND `job_recruitment_status` =:job_recruitment_status";
	$rest_client_closed_check_select_query = $dbcon->prepare($rest_client_closed_check_sql);
	$rest_client_closed_check_select_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_closed_check_select_query->bindValue(":job_recruitment_status",0);
	$rest_client_closed_check_select_query->execute();
	$constructed_array['Jobs_closed'] = $rest_client_closed_check_select_query->rowCount();

	 $rest_client_opened_check_sql = "SELECT * FROM `jobs` WHERE `company_client_id` = :company_client_id AND `job_recruitment_status` =:job_recruitment_status";
	$rest_client_opened_check_select_query = $dbcon->prepare($rest_client_opened_check_sql);
	$rest_client_opened_check_select_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_opened_check_select_query->bindValue(":job_recruitment_status",1);
	$rest_client_opened_check_select_query->execute();
	$constructed_array['Jobs_opened'] = $rest_client_opened_check_select_query->rowCount();

	$rest_client_hold_check_sql = "SELECT * FROM `jobs` WHERE `company_client_id` = :company_client_id AND `job_recruitment_status` =:job_recruitment_status";
	$rest_client_hold_check_select_query = $dbcon->prepare($rest_client_hold_check_sql);
	$rest_client_hold_check_select_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_hold_check_select_query->bindValue(":job_recruitment_status",2);
	$rest_client_hold_check_select_query->execute();
	$constructed_array['Jobs_hold'] = $rest_client_hold_check_select_query->rowCount();

	$rest_client_filled_check_sql = "SELECT * FROM `jobs` WHERE `company_client_id` = :company_client_id AND `job_recruitment_status` =:job_recruitment_status";
	$rest_client_filled_check_select_query = $dbcon->prepare($rest_client_filled_check_sql);
	$rest_client_filled_check_select_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_filled_check_select_query->bindValue(":job_recruitment_status",3);
	$rest_client_filled_check_select_query->execute();
	$constructed_array['Jobs_filled'] = $rest_client_filled_check_select_query->rowCount();

	$rest_client_cancelled_check_sql = "SELECT * FROM `jobs` WHERE `company_client_id` = :company_client_id AND `job_recruitment_status` =:job_recruitment_status";
	$rest_client_cancelled_check_sql_query = $dbcon->prepare($rest_client_cancelled_check_sql);
	$rest_client_cancelled_check_sql_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_cancelled_check_sql_query->bindValue(":job_recruitment_status",4);
	$rest_client_cancelled_check_sql_query->execute();
	$constructed_array['Jobs_cancelled'] = $rest_client_cancelled_check_sql_query->rowCount();

	$rest_client_expired_check_sql = "SELECT * FROM `jobs` WHERE `company_client_id` = :company_client_id AND `job_recruitment_status` =:job_recruitment_status";
	$rest_client_expired_check_sql_query = $dbcon->prepare($rest_client_expired_check_sql);
	$rest_client_expired_check_sql_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_expired_check_sql_query->bindValue(":job_recruitment_status",5);
	$rest_client_expired_check_sql_query->execute();
	$constructed_array['Jobs_expired'] = $rest_client_expired_check_sql_query->rowCount();

	$rest_client_ignored_check_sql = "SELECT * FROM `jobs` WHERE `company_client_id` = :company_client_id AND `job_recruitment_status` =:job_recruitment_status";
	$rest_client_ignored_check_sql_query = $dbcon->prepare($rest_client_ignored_check_sql);
	$rest_client_ignored_check_sql_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_ignored_check_sql_query->bindValue(":job_recruitment_status",6);
	$rest_client_ignored_check_sql_query->execute();
	$constructed_array['Jobs_ignored'] = $rest_client_ignored_check_sql_query->rowCount();
	$temp_array[] = $constructed_array;
}
	     return $temp_array;

	}//close of if($rest_client_company_check_select_query->rowCount() > 0) {
	return $temp_array;

}

function company_office_addresses_duplicate_check_based_on_id($company_client_id_input) {

	global $dbcon;
	$constructed_array = array();
	
	$company_office_addresses_duplicate_check_sql = "SELECT * FROM `company_client_office_addresses` WHERE `company_client_id`=:company_client_id";
	$company_office_addresses_details_select_query = $dbcon->prepare($company_office_addresses_duplicate_check_sql);
    $company_office_addresses_details_select_query->bindValue(":company_client_id",$company_client_id_input);


	$company_office_addresses_details_select_query->execute();

	if($company_office_addresses_details_select_query->rowCount() > 0) {
		$company_office_addresses_details_select_query_result = $company_office_addresses_details_select_query->fetch();
	     

	     return $company_office_addresses_details_select_query_result;

	}//close of if($company_office_addresses_details_select_query_result->rowCount() > 0) {
	return $constructed_array;

}

	


exit;
?>


