<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "4")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['profile_source_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['profile_source_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['profile_source_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['resume_text'])) {
						$content .= $ea_received_rest_ws_raw_array_input['resume_text'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['resume_text'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
			
			    $profile_source_id_input = trim(isset($ea_received_rest_ws_raw_array_input['profile_source_id']) ? filter_var($ea_received_rest_ws_raw_array_input['profile_source_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$resume_text_input = trim(isset($ea_received_rest_ws_raw_array_input['resume_text']) ? filter_var($ea_received_rest_ws_raw_array_input['resume_text'], FILTER_SANITIZE_STRING) : '');
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				
				if ($company_id_input == "") {
			
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-company-id";
					$response['status_description'] = "Missing company id.";
					
					$eventLog->log("missing-company-id: Please provide a valid Company Id.");
				
				} else if ($profile_source_id_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-profile-source-id";
					$response['status_description'] = "Missing profile source id.";
					
					$eventLog->log("missing-profile-source-id: Please provide a valid Profile Source Id.");
					
				
				} else if ($resume_text_input == "") {
					//Invalid resume text scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-resume-text";
					$response['status_description'] = "Missing resume text.";
					
					$eventLog->log("missing-company-email: Please provide a valid Company Email.");
						
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					
						//Event Time, as per Indian Standard Time
						$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
						
						$candidate_add_next_step = "";
						
						if ($ea_extracted_jwt_token_user_type == "admin") {
							
							if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
								$candidate_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
								
								$candidate_add_next_step = "PROCEED-TO-NEXT-STEP";
								
							} else {
								//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions-different-company-user-id";
								$response['status_description'] = "Insufficient Set of Permissions.";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
								$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
								
							}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
							
							
							
						} else {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
			
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
						
						}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {

						$profile_source_details_result = profile_source_details_based_on_profile_source_id($profile_source_id_input);
	                                        
						$profile_source_name_input = $profile_source_details_result["profile_source_name"];	
						$profile_source_seo_name_input = $profile_source_details_result["profile_source_seo_name"];	
						if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							try {
								
								$last_inserted_id = resume_text_insert($company_id_input,$profile_source_id_input,$profile_source_name_input,$profile_source_seo_name_input,$resume_text_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch);
								
								$eventLog->log($last_inserted_id);
								 
									
									if ($last_inserted_id != "") {
										
										    $response['data'] = array();
											$response['status'] = "resume-text-insertion-successful.";
											$response['status_description'] = "Submitted successfully.";
										
											$eventLog->log("resume-text-insertion-successful:Resume Text is added Successfully.");

											
										
									} else {
										//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
										$response['data'] = array();
										$response['status'] = "resume-text-insertion-error.";
										$response['status_description'] = "Submit error.";
										
										$eventLog->log("resume-text-insertion-error: There is an error, when adding the Resume Text into the Database.");
										
									}
									
								
									
							         
									
								
							} catch (Exception $e) {
								
								//There is an error when inserting User privilege info, for the user, in sm_site_member_classification_associations db table
								$response['data'] = array();
								$response['status'] = "resume-text-insertion-error.";
								$response['status_description'] = "No records found.";
								
								$eventLog->log("resume-text-insertion-error: There is an error, when adding the Resume Text in the Database.");
							}
						}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
							
						
					
				}//close of else of if ($email_id_input == "") {
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
    
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
			
	
	}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
	$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function resume_text_insert($company_id_input,$profile_source_id_input,$profile_source_name_input,$profile_source_seo_name_input,$resume_text_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch) {
    global $dbcon,$eventLog;
    $resume_text_insert_sql ="INSERT INTO `candidate_rel_submitted_resume_texts` (`company_id`, `profile_source_id`,`profile_source_name`,`profile_source_seo_name`, `resume_text`, `added_by_user_id`, `added_date_time`, `added_date_time_epoch`) VALUES (:company_id,:profile_source_id,:profile_source_name,:profile_source_seo_name,:resume_text,:added_by_user_id,:added_date_time,:added_date_time_epoch)";
	$eventLog->log("after query");

	$resume_text_insert_query = $dbcon->prepare($resume_text_insert_sql);
	$resume_text_insert_query->bindValue(":company_id",$company_id_input);
	$resume_text_insert_query->bindValue(":profile_source_id",$profile_source_id_input);
	$resume_text_insert_query->bindValue(":profile_source_name",$profile_source_name_input);
	$resume_text_insert_query->bindValue(":profile_source_seo_name",$profile_source_seo_name_input);
	$resume_text_insert_query->bindValue(":resume_text",$resume_text_input);
	$resume_text_insert_query->bindValue(":added_by_user_id",$ea_extracted_jwt_token_sub);
	$resume_text_insert_query->bindValue(":added_date_time",$event_datetime);
	$resume_text_insert_query->bindValue(":added_date_time_epoch",$current_epoch);


		if ($resume_text_insert_query->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			$eventLog->log("record inserted successfully");

			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}	

exit;
?>