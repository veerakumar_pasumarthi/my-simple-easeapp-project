<?php 
defined('START') or die; 

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "6")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
					
					if (isset($ea_received_rest_ws_raw_array_input['user_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['user_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['user_id'])) 
					
					/*  if (isset($ea_received_rest_ws_raw_array_input['resume_upload_mode'])) {
						$content .= $ea_received_rest_ws_raw_array_input['resume_upload_mode'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['resume_upload_mode'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['resume_source'])) {
						$content .= $ea_received_rest_ws_raw_array_input['resume_source'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['resume_source']))  */ 
						
					if (isset($ea_received_rest_ws_raw_array_input['file_rel_original_filename'])) {
						$content .= $ea_received_rest_ws_raw_array_input['file_rel_original_filename'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['file_rel_original_filename'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['job_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['file_content'])) {
						$content .= $ea_received_rest_ws_raw_array_input['file_content'] . "\r\n";
					} //close of if (isset($ea_received_rest_ws_raw_array_input['file_content']))
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					} //close of if (isset($ea_received_rest_ws_raw_array_input['company_id']))
					
					if (isset($ea_received_rest_ws_raw_array_input['ip_address'])) {
						$content .= $ea_received_rest_ws_raw_array_input['ip_address'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['ip_address']))
					
					$eventLog->log("Received Inputs => ".$content);
					
				}//close of if ($ea_received_rest_ws_raw_array_input != "") 
					
				//Filter Inputs	
				$user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				 /* $resume_upload_mode_input = trim(isset($ea_received_rest_ws_raw_array_input['resume_upload_mode']) ? filter_var($ea_received_rest_ws_raw_array_input['resume_upload_mode'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$resume_source_input = trim(isset($ea_received_rest_ws_raw_array_input['resume_source']) ? filter_var($ea_received_rest_ws_raw_array_input['resume_source'], FILTER_SANITIZE_NUMBER_INT) : ''); */ 
				
				$job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$file_rel_original_filename_input = trim(isset($ea_received_rest_ws_raw_array_input['file_rel_original_filename']) ? filter_var($ea_received_rest_ws_raw_array_input['file_rel_original_filename'], FILTER_SANITIZE_STRING) : '');
				
				$file_content_input = trim(isset($ea_received_rest_ws_raw_array_input['file_content']) ? filter_var($ea_received_rest_ws_raw_array_input['file_content'], FILTER_SANITIZE_STRING) : '');
				
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {				
				$eventLog->log("company id =>".$company_id_input);
				//Check if all inputs are received correctly from the REST Web Service
				if ($user_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-user-id";
					$response['status_description'] = "Missing User ID, please check and try again.";
					
					$eventLog->log("Please provide a valid User ID.");
					
				}  /* else if ($resume_upload_mode_input == "") {
					//Invalid File Upload Reference ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-resume-upload-mode-input";
					$response['status_description'] = "Missing Resume Upload Mode";
					
					$eventLog->log("missing-resume-upload-mode-input: Missing Resume Upload Mode, please check and try again.");
				
				} else if ($resume_source_input == "") {
					//Invalid File Upload Reference ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-resume-source-input";
					$response['status_description'] = "Missing Resume Source";
					
					$eventLog->log("missing-resume-source-input: Missing Resume Source, please check and try again.");
				
				} */ else if ($job_id_input == "") {
					//Invalid File Upload Reference ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-job-id";
					$response['status_description'] = "Missing job ID";
					
					$eventLog->log("missing-file-upload-ref-id: Missing File Upload Reference ID, please check and try again.");
				
				} else if ($file_rel_original_filename_input == "") {
					//Invalid File Upload Reference ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-file-ref-original-filename";
					$response['status_description'] = "Missing File related Original File Name";
					
					$eventLog->log("missing-file-ref-original-filename: Missing File related Original File Name, please check and try again.");
					
				} else if ($file_content_input == "") {
					//Empty File Content scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "empty-file-content-received";
					$response['status_description'] = "Empty File Content Received";
					
					$eventLog->log("empty-file-content-received: Empty File Content Received, Please check and try again.");
					
				} else if ($company_id_input == "") {
					//Empty File Content scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "company-id-missing";
					$response['status_description'] = "Empty Company Id Received";
					
					$eventLog->log("company-id-missing: Empty File Content Received, Please check and try again.");
					
				} else if ($ip_address_input == "") {
					////Invalid additional information
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-some-additional-information";
					$response['status_description'] = "Some Additional Information is missing";
					
					$eventLog->log("missing-some-additional-information: Please provide all information.");	
				
				} else {	
					//All inputs are Valid
					
					$eventLog->log("All inputs are valid.");
					//$eventLog->log("Received Inputs - " . $content);
					
					$ea_action_specific_auth_check_result = "";
					
					//User Details, based on User ID input, through api request
					$user_basic_details_result = user_basic_details_check_based_on_user_id($user_id_input);
					
					if (count($user_basic_details_result) > 0) {
						
						//$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
						
						$ea_action_specific_auth_check_result = ea_check_action_specific_user_privileges_on_different_user_levels($ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_user_company_id,  $ea_extracted_jwt_token_sub, $user_id_input, $ea_extracted_jwt_token_user_privileges_list, $user_basic_details_result["company_id"], $page_filename);					
						
						$eventLog->log("ea_extracted_jwt_token_user_type =>" . $ea_extracted_jwt_token_user_type);
						$eventLog->log("ea_extracted_jwt_token_user_company_id =>" . $ea_extracted_jwt_token_user_company_id);
						$eventLog->log("ea_extracted_jwt_token_sub =>" . $ea_extracted_jwt_token_sub);
						
						$eventLog->log("user_id_input =>" . $user_id_input);
						
						$eventLog->log("ea_extracted_jwt_token_user_privileges_list =>" . $ea_extracted_jwt_token_user_privileges_list);
						$eventLog->log("user_basic_details_result[\"company_id\"] =>" . $user_basic_details_result["company_id"]);
						
						
						$eventLog->log("ea_action_specific_auth_check_result =>" . $ea_action_specific_auth_check_result);				
						
						//echo "ea_action_specific_auth_check_result: " . $ea_action_specific_auth_check_result;					
							
						
						if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
								
								$resume_details_get_result = resume_details_get($user_id_input);
								
								if(count($resume_details_get_result)>0){
									$eventLog->log(" if Profile source details");
									$profile_source_id_input = $resume_details_get_result["profile_source_id"];
									$eventLog->log($profile_source_id_input);
									$profile_source_name_input = $resume_details_get_result["profile_source_name"];
									$eventLog->log($profile_source_name_input);
									$profile_source_seo_name_input = $resume_details_get_result["profile_source_seo_name"];
									$eventLog->log($profile_source_seo_name_input);
																																		} else{
									$eventLog->log("else Profile source details");
									
									$profile_source_id_input = null;
									$profile_source_name_input = null;
									$profile_source_seo_name_input = null;
								}
	
								
								$eventLog->log("file_content_input: " . $file_content_input);	
								$eventLog->log("All inputs are Valid");	
								
								//Event Time, as per Indian Standard Time
								$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, "Europe/London");
					
								
								/*$candidate_rel_document_file_info_result = candidate_rel_document_file_info_get($user_file_upload_ref_id_input);
											
								if (count($candidate_rel_document_file_info_result) > 0) {
									
									$crauvd_id = $candidate_rel_document_file_info_result["crauvd_id"];
									$crauvd_id_rel_activity_ref = $candidate_rel_document_file_info_result["activity_ref"];
									$crauvd_id_rel_activity_ref_id = $candidate_rel_document_file_info_result["activity_ref_id"];
									$crauvd_id_rel_additional_document_ref = $candidate_rel_document_file_info_result["additional_document_ref"];
									$crauvd_id_rel_original_filename = $candidate_rel_document_file_info_result["original_filename"];
									$crauvd_id_rel_generated_filename = $candidate_rel_document_file_info_result["generated_filename"];
									
								*/	
									//if ($crauvd_id_rel_generated_filename == "") {
										
										//data:image/png;base64,iVBORw0KGgoAAAANSUhEUgA
									
										//Extract the received file content, and mime type parts
										$file_content_input_semi_colon_base64_comma_exploded = explode(";base64,",$file_content_input);
										
										if (count($file_content_input_semi_colon_base64_comma_exploded) == "2") {
											
											$uploaded_file_extension_rel_mime_type_and_data = $file_content_input_semi_colon_base64_comma_exploded[0];
											$uploaded_file_base64_encoded_data = $file_content_input_semi_colon_base64_comma_exploded[1];
											
											//Extract mime type from the mime type parts
											$uploaded_file_extension_rel_mime_type_and_data_exploded = explode(":",$uploaded_file_extension_rel_mime_type_and_data);
											
											if (count($uploaded_file_extension_rel_mime_type_and_data_exploded) == "2") {
												$uploaded_file_extension_rel_mime_type_and_data_first_part = $uploaded_file_extension_rel_mime_type_and_data_exploded[0];
												$uploaded_file_mime_type = $uploaded_file_extension_rel_mime_type_and_data_exploded[1];
												
												$eventLog->log("uploaded_file_mime_type: " . $uploaded_file_mime_type);	
												
												$random_text_generation_inputs = uniqid(rand(), TRUE);
												$generated_file_name_without_file_extension = hash('sha256', $random_text_generation_inputs);
												$candidate_document_upload_folder = "./uploaded-documents/";
												
												//https://gist.github.com/raphael-riel/1253986
												$prime_mime_types = array(
														'pdf'     => 'application/pdf',
														'doc'     => 'application/msword',
														'docx'	  => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
													);
											
											    $secondary_mime_types = array(
														
														'doc'     => 'application/octet-stream',
														'docx'	  => 'application/octet-stream'
													);
												
												$mime_types_imploded = implode(", ", $prime_mime_types);
												     //function for compare the mimetypes of what we allow the mime_types and upload from the user mime_type
												$extracted_file_extension_result = extract_file_extension_based_on_uploaded_file_mime_type_input_sample($prime_mime_types, $secondary_mime_types ,$uploaded_file_mime_type);
												
												$eventLog->log("extracted_file_extension_result: " . $extracted_file_extension_result);
												
												if ($extracted_file_extension_result != "") {
													
													//Generated File Name
													$generated_file_name = $generated_file_name_without_file_extension . "." . $extracted_file_extension_result; 
													
													
													/*$crauvd_id_rel_activity_ref_id = $candidate_rel_document_file_info_result["activity_ref_id"];
													$crauvd_id_rel_additional_document_ref = $candidate_rel_document_file_info_result["additional_document_ref"];
													
													$crauvd_id_folder_name = create_seo_name(strtolower_utf8_extended($crauvd_id)) . "_" . create_seo_name(strtolower_utf8_extended($crauvd_id_rel_activity_ref_id)) . "_" . create_seo_name(strtolower_utf8_extended($crauvd_id_rel_additional_document_ref));
													*/
													//Create Full Upload Folder related Absolute Path
													//$crauvd_id_full_folder_absolute_path = $site_home_path . "uploaded-documents/" . $user_id_input . "/" . $crauvd_id_folder_name;
													$crauvd_id_full_folder_absolute_path = $site_home_path . "uploaded-documents/";
													$eventLog->log("Full Upload Folder (with absolute path): " . $crauvd_id_full_folder_absolute_path);
													
													if(!is_dir($crauvd_id_full_folder_absolute_path)) {
													
														mkdir($crauvd_id_full_folder_absolute_path, 0755, true);
														chmod($crauvd_id_full_folder_absolute_path, 0755);
														clearstatcache();	
														
														$eventLog->log("(Absolute Path - Not a Directory Structure) Condition");
														
												    } else if(!is_writable($crauvd_id_full_folder_absolute_path)) {
													
														chmod($crauvd_id_full_folder_absolute_path, 0755);
														clearstatcache();
														
														$eventLog->log("(Absolute Path - Directory Structure Not Writable) Condition");
														
												    }//close of else if of if(!is_dir($site_home_path . "uploaded-documents/" . $user_id_input . "/" . $crauvd_id_folder_name)) {
													
													
													//Create Full Upload Folder related Relative Path
													$crauvd_id_full_folder_relative_path = $candidate_document_upload_main_folder . "/"; // $candidate_document_upload_main_folder from /app/core/main-config.php
													
													$eventLog->log("Full Upload Folder (with relative path): " . $crauvd_id_full_folder_relative_path);
													
													//Stored File Location
													$targetFile =  $crauvd_id_full_folder_relative_path. $generated_file_name;
													
													
													/*
													$targetFile =  $candidate_document_upload_main_folder. $generated_file_name;
													*/
													$eventLog->log("targetFile (with path): " . $targetFile);
													
													
													//Base64 Decode the received file input
													$uploaded_file_base64_decoded_data = base64_decode($uploaded_file_base64_encoded_data);
													
													//Store the Content in the FILEINFO_MIME
													file_put_contents($targetFile, $uploaded_file_base64_decoded_data);
													
													$eventLog->log("The Uploaded file is saved now");
													
													//Create Full Upload Folder related Absolute Path
													$targetFile_absolute_path = $candidate_document_upload_main_folder_rel_absolute_path . "/" . $generated_file_name; // $candidate_document_upload_main_folder from /app/core/hostname-check.php
													
													
													$eventLog->log("Full Upload Folder (with relative path): " . $crauvd_id_full_folder_relative_path);
													
													
													//Check Received File Mimetype
													$finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
													//$file_mime_type = finfo_file($finfo, $targetFile) . "\n";
													$file_mime_type = finfo_file($finfo, $targetFile);
													finfo_close($finfo);
													
													$eventLog->log("enumerated file_mime_type: " . $file_mime_type);
													$sample=extract_file_extension_based_on_uploaded_file_mime_type_input_sample($prime_mime_types, $secondary_mime_types, $file_mime_type);
												
												   $eventLog->log("enumerated file_mime_type_sample: " . $sample);
													//Check, if user submitted file mime type = enumerated file mime type
													if ($sample != "") {
														//Do Update Query
														//$user_file_upload_ref_id_input
														//$file_rel_original_filename_input
														
														$file_size_bytes_input = filesize($targetFile);
														$file_size_kilo_bytes_input = $file_size_bytes_input/1024;
														$file_size_mega_bytes_input = $file_size_bytes_input/(1024*1024);
														
														$eventLog->log("bytes: " . $file_size_bytes_input . " kilo bytes: " . $file_size_kilo_bytes_input . " mega bytes: " . $file_size_mega_bytes_input);
														
														//Check the File Size of the Uploaded File
														if ($file_size_mega_bytes_input > 25) {
															
															//Construct Content, that will be sent in Response body, of the REST Web Service
															$response['data'] = array();
															$response['status'] = "larger-file-uploaded";
															$response['status_description'] = "The File Size of the Uploaded File is > 25MB. Please uploaded the File, that is Smaller in Size.";
															
															$eventLog->log("The File Size of the Uploaded File is > 25MB");
															
														} else {
															$eventLog->log("Done");
															
															//File Size is <= 25MB
															
															if ($job_id_input == "") {
																$job_id_input = null;
															}//close of if ($job_id_input == "") {
															$eventLog->log("company id =>".$company_id_input);	
															$eventLog->log("user id =>".$user_id_input);
															
															$eventLog->log("profile source id =>".$profile_source_id_input);
															$eventLog->log("profile source name =>".$profile_source_name_input);
															$eventLog->log("profile source seo name =>".$profile_source_seo_name_input);
															$eventLog->log("job id =>".$job_id_input);
															$eventLog->log("file related original file name =>".$file_rel_original_filename_input);
															$eventLog->log("generated file name =>".$generated_file_name);
															$eventLog->log("file size =>".$file_size_bytes_input);
															$eventLog->log("file size kilo bytes =>".$file_size_kilo_bytes_input);
															$eventLog->log("file size mega bytes =>".$file_size_mega_bytes_input);
															$eventLog->log("event datetime =>".$event_datetime);	
															$eventLog->log("cureent epoch =>".$current_epoch);
															
																
																
																
															//Insert Query
															$job_rel_resume_info_insert_result = job_rel_resume_info_insert($company_id_input,$user_id_input,$profile_source_id_input,$profile_source_name_input,$profile_source_seo_name_input,$job_id_input,$file_rel_original_filename_input, $generated_file_name,$file_size_bytes_input, $file_size_kilo_bytes_input, $file_size_mega_bytes_input,$ea_extracted_jwt_token_sub,$event_datetime, $current_epoch);	
															$eventLog->log("file_info_update var_dump: " . $job_rel_resume_info_insert_result);
															
															$last_inserted_id = $job_rel_resume_info_insert_result;
															$eventLog->log("last inserted id =>".$last_inserted_id);
															if($last_inserted_id!= null)
															{
																$job_applicant_crur_id_update_result = job_applicant_crur_id_update($last_inserted_id,$job_id_input,$user_id_input);
															}
															
															
															if (($job_rel_resume_info_insert_result)&&($job_applicant_crur_id_update_result == true)) {
																//Construct Content, that will be sent in Response body, of the REST Web Service
																$response['data'] = array();
																$response['status'] = "job-specific-resume-upload-successful";
																$response['status_description'] = "The job specific resume uploaded successfully.";
																
																$eventLog->log("The job specific resume upload successfully");	
																
															} else {
																//Construct Content, that will be sent in Response body, of the REST Web Service
																$response['data'] = array();
																$response['status'] = "job-specific-resume-upload-error";
																$response['status_description'] = "There occurred an error when uploading the job specific resume,please check and try again.";
																
																$eventLog->log("There occurred an error when uploading the job specific resume,please check and try again.");	
																
															}//close of else of if ($candidate_rel_document_file_info_update_result) {
															
														}//close of else of if ($file_size_mega_bytes_input > 25) {
														
														
													} else {
														//File Mime Type is mis-matching
														
														//Construct Content, that will be sent in Response body, of the REST Web Service
														$response['data'] = array();
														$response['status'] = "job-specific-resume-mime-type-mismatching";
														$response['status_description'] = "resume Mime Type is mismatching, w.r.t. particular job";
														
														$eventLog->log("job-specific-resume-mime-type-mismatching: resume Mime Type is mis-matching, w.r.t. particular job, Please check and try again.");
														
													}//close of if ($uploaded_file_mime_type == $file_mime_type) {

													
													
													
												}  else {
													//Construct Content, that will be sent in Response body, of the REST Web Service
													$response['data'] = array();
													$response['status'] = "job-specific-resume-mime-type-not-accepted";
													$response['status_description'] = "The Uploaded resume's Mime Type is not in the accepted list of file types";
													
													$eventLog->log("job-specific-resume-mime-type-not-accepted: The Uploaded resume's Mime Type is not in the accepted list of file types, please check and try again.");	
												}//close of else of if ($extracted_file_extension_result != "") { 
													
												
												
											} else {
												//Construct Content, that will be sent in Response body, of the REST Web Service
												$response['data'] = array();
												$response['status'] = "invalid-file-extension-input";
												$response['status_description'] = "Invalid File Extension Input";
												
												$eventLog->log("invalid-file-extension-input: Invalid info w.r.t. file extension part. Invalid File Extension Input, Please Check and try again.");	
											}//close of else of if (count($uploaded_file_extension_rel_mime_type_and_data_exploded) == "2") {
											
											
											
										} else {
											//Construct Content, that will be sent in Response body, of the REST Web Service
											$response['data'] = array();
											$response['status'] = "invalid-base64-encoded-file-input";
											$response['status_description'] = "Invalid Base64 Encoded File Input";
											
											$eventLog->log("invalid-base64-encoded-file-input: Invalid base64 encoded file content");	
										}//close of else of if (count($file_content_input_semi_colon_base64_comma_exploded) == "2") {
										
										
								/*} else {
										//Construct Content, that will be sent in Response body, of the REST Web Service
										$response['data'] = array();
										$response['status'] = "file-already-uploaded";
										$response['status_description'] = "The File is already Uploaded";
										
										$eventLog->log("file-already-uploaded: The File is already Uploaded, w.r.t. particular User's document reference.");	
										
									}//close of else of if ($crauvd_id_rel_generated_filename == "") {*/
									
										
									
								/*} else {
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "invalid-file-upload-ref-id";
									$response['status_description'] = "Invalid File Upload Ref ID Submitted";
									
									$eventLog->log("invalid-file-upload-ref-id: Invalid File Upload Ref ID Submitted, please check and try again.");	
									
								}//close of else of if (count($candidate_rel_document_file_info_result) > 0) {*/
								
								
								
							} catch (Exception $e) {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "job-specific-resume-upload-request-initiating-error";
								$response['status_description'] = "Error occurred when initiating the job specific resume Upload Request";
								
								$eventLog->log("job-specific-resume-upload-request-initiating-error: Error occurred when initiating the job specific resume Upload Request.");	
								
							}
					    
					    } else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-subject-platform-scope-user";
							$response['status_description'] = "Insufficient Set of Permissions, to complete this action, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						}    else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-different-company-user-id";
							$response['status_description'] = "Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.");	
							
						}   else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-INSUFFICIENT-PERMISSIONS") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-self-action";
							$response['status_description'] = "Insufficient Set of Permissions, as self action is Dis-allowed, please check and try again.";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						}//close of if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
						
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "The User Account, that is attempted to be edited, is invalid, please check and try again.";
						
						$eventLog->log("Please provide a Valid User ID.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
						
					
				}//close of else of if ($user_id_input == "") {*/
				
				
			}//close of //close of if ($ea_is_user_page_access_authorized) {
				
			
	    } else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted, please check and try again.";
			
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	//}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input, Please check and provide all information.";
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "5")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){
	
function resume_details_get($user_id_input) {
	global $dbcon;
	$constructed_array = array();
	$resume_details_get_sql = "SELECT * FROM `candidate_rel_uploaded_resumes` WHERE `sm_memb_id`=:sm_memb_id";
	$resume_details_get_query = $dbcon->prepare($resume_details_get_sql);
	$resume_details_get_query->bindValue(":sm_memb_id",$user_id_input);
	$resume_details_get_query->execute();

	if($resume_details_get_query->rowCount() > 0) {
		$resume_details_get_query_result = $resume_details_get_query->fetch();
	     return $resume_details_get_query_result;

	}//close of if($company_details_check_select_query->rowCount() > 0) {
	return $constructed_array;
}	

function job_rel_resume_info_insert($company_id_input,$user_id_input,$profile_source_id_input,$profile_source_name_input,$profile_source_seo_name_input,$job_id_input,$file_rel_original_filename_input, $generated_file_name,$file_size_bytes_input, $file_size_kilo_bytes_input, $file_size_mega_bytes_input,$ea_extracted_jwt_token_sub,$event_datetime, $current_epoch) {	
    global $dbcon,$eventLog;
	
	
	
	$job_rel_resume_info_insert_sql = "INSERT INTO `candidate_rel_uploaded_resumes`(`company_id`,`sm_memb_id`,`resume_upload_mode`, `resume_source`, `profile_source_id`, `profile_source_name`, `profile_source_seo_name`,`job_id`, `original_filename`, `generated_filename`, `file_size_bytes`, `file_size_kilo_bytes`, `file_size_mega_bytes`, `resume_processing_status`, `added_by_user_id`, `added_date_time`, `added_date_time_epoch`, `is_active_status`) VALUES (:company_id,:sm_memb_id,:resume_upload_mode,:resume_source,:profile_source_id,:profile_source_name,:profile_source_seo_name,:job_id,:original_filename,:generated_filename,:file_size_bytes,:file_size_kilo_bytes,:file_size_mega_bytes,:resume_processing_status,:added_by_user_id,:added_date_time,:added_date_time_epoch,:is_active_status)";
	
	
	$job_rel_resume_info_insert_query = $dbcon->prepare($job_rel_resume_info_insert_sql);
	$job_rel_resume_info_insert_query->bindValue(":company_id",$company_id_input);
	
	$job_rel_resume_info_insert_query->bindValue(":sm_memb_id",$user_id_input);
	$job_rel_resume_info_insert_query->bindValue(":resume_upload_mode",3);
	$job_rel_resume_info_insert_query->bindValue(":resume_source",3);
	$job_rel_resume_info_insert_query->bindValue(":profile_source_id",$profile_source_id_input);
	$job_rel_resume_info_insert_query->bindValue(":profile_source_name",$profile_source_name_input);
	$job_rel_resume_info_insert_query->bindValue(":profile_source_seo_name",$profile_source_seo_name_input);
	$job_rel_resume_info_insert_query->bindValue(":job_id",$job_id_input);
	$job_rel_resume_info_insert_query->bindValue(":original_filename",$file_rel_original_filename_input);
	$job_rel_resume_info_insert_query->bindValue(":generated_filename",$generated_file_name);
	$job_rel_resume_info_insert_query->bindValue(":file_size_bytes",$file_size_bytes_input);
	$job_rel_resume_info_insert_query->bindValue(":file_size_kilo_bytes",$file_size_kilo_bytes_input);
	$job_rel_resume_info_insert_query->bindValue(":file_size_mega_bytes",$file_size_mega_bytes_input);
	$job_rel_resume_info_insert_query->bindValue(":resume_processing_status",0);
	$job_rel_resume_info_insert_query->bindValue(":added_by_user_id",$ea_extracted_jwt_token_sub);
	$job_rel_resume_info_insert_query->bindValue(":added_date_time",$event_datetime);
	$job_rel_resume_info_insert_query->bindValue(":added_date_time_epoch",$current_epoch);
	$job_rel_resume_info_insert_query->bindValue(":is_active_status",1);
	$eventLog->log("before excute.");
	if ($job_rel_resume_info_insert_query->execute()) {
			$eventLog->log("after excute.");
        $last_inserted_id = $dbcon->lastInsertId();
		$eventLog->log("last inserted id =>".$last_inserted_id);
			return $last_inserted_id;
						
	} else {						
			return "";						
	}
	
}
function job_applicant_crur_id_update($last_inserted_id,$job_id_input,$user_id_input) {
    global $dbcon,$eventLog;
    $job_applicant_crur_id_update_sql = "UPDATE `job_applicant_requests_info` SET `crur_id`=:crur_id WHERE `job_id`=:job_id AND `job_applied_by_user_id`=:job_applied_by_user_id";
	
	$job_applicant_crur_id_update_q = $dbcon->prepare($job_applicant_crur_id_update_sql);
	$job_applicant_crur_id_update_q->bindValue(":crur_id",$last_inserted_id);
	$job_applicant_crur_id_update_q->bindValue(":job_id",$job_id_input);
	$job_applicant_crur_id_update_q->bindValue(":job_applied_by_user_id",$user_id_input);
	
		
	if ($job_applicant_crur_id_update_q->execute()) {

			return true;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return false;
	    }

}
function extract_file_extension_based_on_uploaded_file_mime_type_input_sample($prime_mime_types, $secondary_mime_types, $uploaded_file_mime_type_input) {
											
	$uploaded_file_rel_extension = "";
	
	foreach ($prime_mime_types as $mime_types_input_key => $mime_types_input_value) {
		
		if (trim($uploaded_file_mime_type_input) == $mime_types_input_value) {
			
			$uploaded_file_rel_extension = $mime_types_input_key;
			
			return $uploaded_file_rel_extension;
			
		}//close of if (in_array(trim($uploaded_file_mime_type_input), $mime_types_input_value, true)) {
			
	}  if ($uploaded_file_rel_extension == "") {
		
		foreach ($secondary_mime_types as $mime_types_input_key => $mime_types_input_value) {
		
		if (trim($uploaded_file_mime_type_input) == $mime_types_input_value) {
			
			$uploaded_file_rel_extension = $mime_types_input_key;
			
			return $uploaded_file_rel_extension;
			
		}//close of if (in_array(trim($uploaded_file_mime_type_input), $mime_types_input_value, true)) {
			
	}
			
}//close of foreach ($mime_types_input as $mime_types_input_key => $mime_types_input_value) {
	return $uploaded_file_rel_extension;
	
}

exit;
?>