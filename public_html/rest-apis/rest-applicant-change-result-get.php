
<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to Add Quick Candidate Info, to the System.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "4")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				
				if (is_array($ea_received_rest_ws_raw_array_input)) {
					$content = "";
						
					if (isset($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'])) 
					
					if (isset($ea_received_rest_ws_raw_array_input['company_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['company_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['company_id']))
					
					if (isset($ea_received_rest_ws_raw_array_input['job_id'])) {
						$content .= $ea_received_rest_ws_raw_array_input['job_id'] . "\r\n";
					}//close of if (isset($ea_received_rest_ws_raw_array_input['job_id'])) 
					 
				}	
	
					
					$job_applicant_request_info_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_applicant_request_info_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					$job_id_input = trim(isset($ea_received_rest_ws_raw_array_input['job_id']) ? filter_var($ea_received_rest_ws_raw_array_input['job_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
					
					if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
						$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
					} else {
						$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
						$ip_address_input = '';
					}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						

					if ($job_applicant_request_info_id_input == "") {
						
						$response['data'] = array();
						$response['status'] = "missing-job-applicant-request-info-id";
						$response['status_description'] = "Missing Job applicant request info Id";
						
						$eventLog->log("Missing Job applicant request info Id");
						
					
					} else if ($company_id_input == "") {
						
						$response['data'] = array();
						$response['status'] = "missing-company-id";
						$response['status_description'] = "Missing Company Id";
						
						$eventLog->log("Missing company Id.");
						
					} else if ($job_id_input == "") {
						
						$response['data'] = array();
						$response['status'] = "missing-job-id";
						$response['status_description'] = "Missing Job Id";
						
						$eventLog->log("Missing Job Id.");
						
						
					
					} else {	
						
							$job_rel_screening_level_add_next_step = "";
							if ($ea_extracted_jwt_token_user_type == "admin") {
								
								if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
									$job_rel_screening_level_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("before company id condition");
								} else if ((isset($ea_extracted_jwt_token_user_company_id)) && ($company_id_input == $ea_extracted_jwt_token_user_company_id)) {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
									
									$job_rel_screening_level_add_next_step = "PROCEED-TO-NEXT-STEP";
									 $eventLog->log("after add condition");
								} else {
									//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = array();
									$response['status'] = "insufficient-permissions-different-company-user-id";
									$response['status_description'] = "Insufficient Set of Permissions";
									
									//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
									header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
									$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
									
								}//close of else of if ((in_array("Super Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded)) || (in_array("Site Administrator", $ea_extracted_jwt_token_user_privileges_list_exploded))) {
								
								
								
							} else {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "insufficient-permissions";
								$response['status_description'] = "Insufficient Set of Permissions";
								
								//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
								header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
				
								$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
							
							}//close of else of if ($ea_extracted_jwt_token_user_type == "admin") {
								
							
							
							if ($job_rel_screening_level_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
								/* $job_pool_specific_existence_check_result = job_pool_specific_existence_check($ea_extracted_jwt_token_sub,$job_id_input); */
								
								//if(count($job_pool_specific_existence_check_result) > 0) {
								
									try {
											$event_datetime = df_convert_unix_timestamp_to_datetime_custom_timezone($current_epoch, $date_default_timezone_set);
											$applicant_scheduled_screening_level_get_result = applicant_scheduled_screening_level_get($job_id_input,$company_id_input,$job_applicant_request_info_id_input);
								
											if(count($applicant_scheduled_screening_level_get_result) > 0) {
												
												$response['data'] = $applicant_scheduled_screening_level_get_result;
												$response['status'] = "applicant-current-screening-level-details-fetched-successfully";
												$response['status_description'] = "applicant current screening level details successfully fetched";
												
												$eventLog->log("applicant current screening level details successfully fetched");
													
											} else {
												
												$response['data'] = array();
												$response['status'] = "no-screening-levels-currently-scheduled";
												$response['status_description'] = "No screening levels scheduled currently";
												
												$eventLog->log("No screening levels scheduled currently");
												
											}	
										
									} catch (Exception $e) {
										
										$response['data'] = array();
										$response['status'] = "applicant-current-screening-level-details-fetching-error";
										$response['status_description'] = "Error occurred when fetching applicant current screening level details.";
											
										$eventLog->log("Error occurred when fetching applicant current screening level details.");
											
									}
									
							/* 	} else {
									
									$response['data'] = array();
									$response['status'] = "insufficient-permissions";
									$response['status_description'] = "Insufficient Set of Permissions";
									
									//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
									header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
					
									$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, this process will not be allowed any further, please check and try again.");	
									
								} */
							}//close of if ($candidate_add_next_step == "PROCEED-TO-NEXT-STEP") {
								
							
						}
			//}//close of else of if ($email_id_input == "") {
					
		}//close of //close of if ($ea_is_user_page_access_authorized) {
					
				
	} else {
				
				//Construct Content, that will be sent in Response body, of the REST Web Service
				$response['data'] = array();
				$response['status'] = "invalid-auth-token-submitted";
				$response['status_description'] = "Invalid Authentication";
				
				//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
				header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
				
				$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
				
			}//close of else of if ($ea_auth_token_validation_status) {
		
		}//close of if ($ea_maintenance_mode == false) {
		
} else {

		//Construct Content, that will be sent in Response body, of the REST Web Service
		$response['data'] = array();
		$response['status'] = "invalid-input";
		$response['status_description'] = "Invalid Input, Please check and provide all information.";
		
		//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
		
		$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
				
		
	}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "10")) {




	//Check if Maintenance Mode is Turned On
	if ($ea_maintenance_mode) {	
		
		//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
		//header('Maintenance-Progress: true', false);
		//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
		$response['data'] = array();
		$response['status'] = "application-maintenance-in-progress";
		$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
		header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
		
		$eventLog->log("application-maintenance-in-progress: Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time));
		
	} else {	

		//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
		header('Content-Type: application/json');
		echo json_encode($response,JSON_PRETTY_PRINT);
		
	}//close of else of if ($ea_maintenance_mode){
		

function applicant_scheduled_screening_level_get($job_id_input,$company_id_input,$job_applicant_request_info_id_input) {
	global $dbcon,$expiring_link_lifetime,$expiring_link_secret_key,$expiring_link_hash_algorithm,$current_epoch,$ea_extracted_jwt_token_sub;
	
	$constructed_array = array();
	$temp = array();
	$event_status = "1";
	
	$job_rel_candidature_screening_levels_list_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `job_id` =:job_id AND `company_id` =:company_id AND `job_applicant_request_info_id` = :job_applicant_request_info_id AND `event_status` IN (4,5,6,7) ORDER BY added_datetime DESC LIMIT 1";
	$job_rel_candidature_screening_levels_list_query = $dbcon->prepare($job_rel_candidature_screening_levels_list_sql);
	$job_rel_candidature_screening_levels_list_query->bindValue(":job_id",$job_id_input);		
	$job_rel_candidature_screening_levels_list_query->bindValue(":company_id",$company_id_input);
	$job_rel_candidature_screening_levels_list_query->bindValue(":job_applicant_request_info_id",$job_applicant_request_info_id_input);
	//$job_rel_candidature_screening_levels_list_query->bindValue(":event_status",$event_status);
	$job_rel_candidature_screening_levels_list_query->execute(); 
	
	if($job_rel_candidature_screening_levels_list_query->rowCount() > 0) {
		
		$job_rel_candidature_screening_levels_list_query_result = $job_rel_candidature_screening_levels_list_query->fetchAll();
		
		 	
		
		foreach($job_rel_candidature_screening_levels_list_query_result as $job_rel_candidature_screening_levels_list_query_result_row) {
			
				
			$temp["jsl_applicant_evaluation_info_id"] = $job_rel_candidature_screening_levels_list_query_result_row["jsl_applicant_evaluation_info_id"];
			$temp["scheduled_event_country_two_lett_code"] = $job_rel_candidature_screening_levels_list_query_result_row["scheduled_event_country_two_lett_code"];
			$temp["scheduled_event_iana_timezone"] = $job_rel_candidature_screening_levels_list_query_result_row["scheduled_event_iana_timezone"];
			$temp["scheduled_event_def_datetime"] = $job_rel_candidature_screening_levels_list_query_result_row["scheduled_event_def_datetime"];
			$temp["online_interview_link"] = $job_rel_candidature_screening_levels_list_query_result_row["online_interview_link"];
			$temp["telephonic_interview_number"] = $job_rel_candidature_screening_levels_list_query_result_row["telephonic_interview_number"];
			$temp["in_person_interview_location_address"] = $job_rel_candidature_screening_levels_list_query_result_row["in_person_interview_location_address"];
			
			$temp["company_client_id"] = $job_rel_candidature_screening_levels_list_query_result_row["company_client_id"];
			
			$company_client_basic_details_result = client_company_details_get($temp["company_client_id"]);
			$temp["company_client_name"] = $company_client_basic_details_result["client_company_name"];
			
			
			$temp["job_id"] = $job_rel_candidature_screening_levels_list_query_result_row["job_id"];
			
	
			$job_basic_details_result = job_details_get($temp["job_id"]);
			$temp["job_title"] = $job_basic_details_result["job_title"];
			
		
			$temp["jsl_classification_detail_id"] = $job_rel_candidature_screening_levels_list_query_result_row["jsl_classification_detail_id"];
			$temp["jsl_classification_detail_name"] = $job_rel_candidature_screening_levels_list_query_result_row["jsl_classification_detail_name"];
			$temp["jsl_classification_detail_seo_name"] = $job_rel_candidature_screening_levels_list_query_result_row["jsl_classification_detail_seo_name"];
			$temp["jsl_sub_classification_detail_id"] = $job_rel_candidature_screening_levels_list_query_result_row["jsl_sub_classification_detail_id"];
			$temp["jsl_sub_classification_detail_name"] = $job_rel_candidature_screening_levels_list_query_result_row["jsl_sub_classification_detail_name"];
			$temp["jsl_classification_detail_name"] = $job_rel_candidature_screening_levels_list_query_result_row["jsl_classification_detail_name"];
			$temp["jsl_sub_classification_detail_seo_name"] = $job_rel_candidature_screening_levels_list_query_result_row["jsl_sub_classification_detail_seo_name"];
			
			$temp["jsl_info_id"] = $job_rel_candidature_screening_levels_list_query_result_row["jsl_info_id"];
			$screening_level_details = job_screening_level_details_based_on_jsl_info_id($temp["jsl_info_id"]);
			$temp["jsl_title"] = $screening_level_details["jsl_title"];
			$temp["job_applicant_request_info_id"] = $job_rel_candidature_screening_levels_list_query_result_row["job_applicant_request_info_id"];
			$temp["job_applicant_sm_memb_id"] = $job_rel_candidature_screening_levels_list_query_result_row["job_applicant_sm_memb_id"];
		
			$uploaded_resume_info_result = uploaded_resume_info_result($temp["job_applicant_sm_memb_id"]);
			
			if(count($uploaded_resume_info_result) > 0) {
				$crur_id = $uploaded_resume_info_result["crur_id"];
				$site_hostname_value = "https://uploaded-files-dev.resumecrab.com/";
				$link_expiry_time = $current_epoch+$expiring_link_lifetime;
				
				$uploaded_file_link_with_signature_result = create_uploaded_file_link_with_signature($ea_extracted_jwt_token_sub, $temp["job_applicant_sm_memb_id"], $crur_id, "https", $site_hostname_value, $link_expiry_time, $expiring_link_hash_algorithm, $expiring_link_secret_key);
				if (isset($uploaded_file_link_with_signature_result["uploaded_file_url"])) {
				$temp["resume_url"] = $uploaded_file_link_with_signature_result["uploaded_file_url"];
					
				} else {
					$temp["resume_url"] = null;
				}
				
			}
			
			

		
			
			/* $temp["scheduled_event_datetime"] = $job_rel_candidature_screening_levels_list_query_result_row["scheduled_event_datetime"];
			$temp["scheduled_event_datetime_epoch"] = $job_rel_candidature_screening_levels_list_query_result_row["scheduled_event_datetime_epoch"];
			$temp["online_interview_link"] = $job_rel_candidature_screening_levels_list_query_result_row["online_interview_link"];
	
			$temp["telephonic_interview_number"] = $job_rel_candidature_screening_levels_list_query_result_row["telephonic_interview_number"];
			
			$temp["in_person_interview_location_address"] = $job_rel_candidature_screening_levels_list_query_result_row["in_person_interview_location_address"]; */
			$temp["added_datetime"] = $job_rel_candidature_screening_levels_list_query_result_row["added_datetime"];
			$temp["added_datetime_epoch"] = $job_rel_candidature_screening_levels_list_query_result_row["added_datetime_epoch"];
			$temp["added_by_sm_memb_id"] = $job_rel_candidature_screening_levels_list_query_result_row["added_by_sm_memb_id"];
			
			/* $temp["sm_salutation"] = $job_rel_candidature_screening_levels_list_query_result_row["sm_salutation"];
			$temp["sm_firstname"] = $job_rel_candidature_screening_levels_list_query_result_row["sm_firstname"];
			$temp["sm_middlename"] = $job_rel_candidature_screening_levels_list_query_result_row["sm_middlename"];
			$temp["sm_lastname"] = $job_rel_candidature_screening_levels_list_query_result_row["sm_lastname"];
			$temp["sm_email"] = $job_rel_candidature_screening_levels_list_query_result_row["sm_email"];
			$temp["sm_name"] = $temp["sm_firstname"] ." ".$temp["sm_middlename"] ." ".$temp["sm_lastname"];
			$temp["company_id"] = $job_rel_candidature_screening_levels_list_query_result_row["company_id"]; */
			
			//$constructed_array[] = $temp;
		}//close of foreach($candidate_education_qualification_records_list_select_query_result as $candidate_education_qualification_records_list_select_query_result_row) {
			
		
		return $temp;
	
		//return $job_rel_candidature_screening_levels_list_query_result;
	
	}//close of if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
	return $constructed_array;
}

function job_details_get_based_on_job_id($job_id_input) {
	global $dbcon;
	$constructed_array = array();
	$job_details_get_sql = "SELECT * FROM `jobs` j JOIN `company_clients` cc ON j.company_client_id = cc.company_client_id WHERE j.job_id =:job_id";
	$job_details_ge_select_query = $dbcon->prepare($job_details_get_sql);
	$job_details_ge_select_query->bindValue(":job_id",$job_id_input);	
	$job_details_ge_select_query->execute(); 
	
	if($job_details_ge_select_query->rowCount() > 0) {
		$job_details_ge_select_query_result = $job_details_ge_select_query->fetchAll();
		
		return $job_details_ge_select_query_result;
	}
	return $constructed_array;
}

function job_screening_level_details_based_on_jsl_info_id($jsl_info_id_input) {
	
	global $dbcon;
	$constructed_array = array();
	
	$job_screening_levels_list_info_sql = "SELECT * FROM `jsl_info` WHERE `jsl_info_id` =:jsl_info_id";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":jsl_info_id",$jsl_info_id_input);		
	$job_rel_screening_levels_list_info_query->execute(); 
	
	if($job_rel_screening_levels_list_info_query->rowCount() > 0) {
		$job_rel_screening_levels_list_info_query_result = $job_rel_screening_levels_list_info_query->fetch();
	     return $job_rel_screening_levels_list_info_query_result;
	
	}
	return $constructed_array;

}

function job_details_get($job_id_input) {
	
	global $dbcon;
	$constructed_array = array();
	
	$job_screening_levels_list_info_sql = "SELECT * FROM `jobs` WHERE `job_id` =:job_id";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":job_id",$job_id_input);		
	$job_rel_screening_levels_list_info_query->execute(); 
	
	if($job_rel_screening_levels_list_info_query->rowCount() > 0) {
		$job_rel_screening_levels_list_info_query_result = $job_rel_screening_levels_list_info_query->fetch();
	     return $job_rel_screening_levels_list_info_query_result;
	
	}
	return $constructed_array;

}
function client_company_details_get($client_company_id_input) {
	
	global $dbcon;
	$constructed_array = array();
	
	$job_screening_levels_list_info_sql = "SELECT * FROM `company_clients` WHERE `company_client_id` =:company_client_id";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":company_client_id",$client_company_id_input);		
	$job_rel_screening_levels_list_info_query->execute(); 
	
	if($job_rel_screening_levels_list_info_query->rowCount() > 0) {
		$job_rel_screening_levels_list_info_query_result = $job_rel_screening_levels_list_info_query->fetch();
	     return $job_rel_screening_levels_list_info_query_result;
	
	}
	return $constructed_array;

}

function uploaded_resume_info_result($applicant_id){
	
	global $dbcon;
	$constructed_array = array();
	
	$job_screening_levels_list_info_sql = "SELECT * FROM `candidate_rel_uploaded_resumes` WHERE sm_memb_id = :sm_memb_id ORDER BY `added_date_time` DESC LIMIT 1";
	$job_rel_screening_levels_list_info_query = $dbcon->prepare($job_screening_levels_list_info_sql);
	$job_rel_screening_levels_list_info_query->bindValue(":sm_memb_id",$applicant_id);		
	$job_rel_screening_levels_list_info_query->execute(); 
	
	if($job_rel_screening_levels_list_info_query->rowCount() > 0) {
		$job_rel_screening_levels_list_info_query_result = $job_rel_screening_levels_list_info_query->fetch();
	     return $job_rel_screening_levels_list_info_query_result;
	
	}
	return $constructed_array;
	
	
}

exit;
?>