<?php 
defined('START') or die; 

/**
 *
 * This REST API Endpoint is used to get List of Candidates, in the response.
 *
 */

$eventLogFileName = $route_filename . "-log";
$eventLog = new Logger($eventLogFileName, true);
$eventLog->logNewSeperator();
$eventLog->log("Content-type => " . $ea_received_rest_ws_content_type);
$eventLog->log("Server protocol => " . $_SERVER['SERVER_PROTOCOL']);
//$eventLog->log("Request Headers => " . $ea_received_request_headers_json_encoded);
//$eventLog->log("JWT Token => " . $ea_auth_token);

if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "4")) {
	
	//Process, only if the Maintenance Mode is turned off
	if ($ea_maintenance_mode == false) {
		
		//Do Verify, if the JWT Auth Token Verification Status is Valid
		if ($ea_auth_token_validation_status) {
			$eventLog->log("JWT Auth Token is Verified and Valid, for this User");
			
			//If User is Authorized, to access this Page / REST API Service
			if ($ea_is_user_page_access_authorized) {
				//Filter Inputs	
				
				$user_id_input = trim(isset($ea_received_rest_ws_raw_array_input['user_id']) ? filter_var($ea_received_rest_ws_raw_array_input['user_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$company_id_input = trim(isset($ea_received_rest_ws_raw_array_input['company_id']) ? filter_var($ea_received_rest_ws_raw_array_input['company_id'], FILTER_SANITIZE_NUMBER_INT) : '');
				
				$data_scope_input = trim(isset($ea_received_rest_ws_raw_array_input['data_scope']) ? filter_var($ea_received_rest_ws_raw_array_input['data_scope'], FILTER_SANITIZE_STRING) : '');
				
				//Check if the IP Address Input is a Valid IPv4 Address
				if (filter_var($ea_received_rest_ws_raw_array_input['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
					//$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - A valid IPv4 address");
					$ip_address_input = trim($ea_received_rest_ws_raw_array_input['ip_address']);
				} else {
					$eventLog->log($ea_received_rest_ws_raw_array_input['ip_address'] . " - not a valid IPv4 address");
					$ip_address_input = '';
				}//close of else of if (filter_var($_POST['ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {


				if ($user_id_input == "") {
					//Invalid User ID scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-user-id";
					$response['status_description'] = "Missing user id.";
					
					$eventLog->log("missing-user-id: Please provide a valid User ID.");
					
				} else if (($data_scope_input != "all") && ($data_scope_input != "recruiters") && ($data_scope_input != "sales") ) {
					//Invalid Data Scope scenario
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "invalid-data-scope";
					$response['status_description'] = "Invalid data scope.";
					
					$eventLog->log("invalid-data-scope: Invalid Data Scope submitted, please check and try again.");
					
				} else if ($ip_address_input == "") {
					//One or More Inputs are Missing!!!
					
					//Construct Content, that will be sent in Response body, of the REST Web Service
					$response['data'] = array();
					$response['status'] = "missing-ip-address";
					$response['status_description'] = "Missing IP-Address";
					
					$eventLog->log("missing-ip-address: Please provide valid ip_address.");
					
				} else {
					//all inputs are valid
					$eventLog->log("All inputs are valid.");
					
					$ea_action_specific_auth_check_result = "";
					
					//User Details, based on User ID input, through api request
					$user_basic_details_result = user_basic_details_check_based_on_user_id($user_id_input);
					
					if (count($user_basic_details_result) > 0) {
						
						/* //$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
						$ea_action_specific_auth_check_result = ea_check_action_specific_user_privileges_different_user_levels_reset_password_feature($ea_extracted_jwt_token_user_type, $ea_extracted_jwt_token_user_company_id,  $ea_extracted_jwt_token_sub, $user_id_input, $ea_extracted_jwt_token_user_privileges_list, $user_basic_details_result["company_id"], $page_filename);					
						 */
						//echo "ea_action_specific_auth_check_result: " . $ea_action_specific_auth_check_result;					
						
							
						
						$ea_action_specific_auth_check_result = "PROCEED-TO-NEXT-STEP";
						
						if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
							try {
								
								$eventLog->log("try condition");	
							
							    $all_classified_users_array = array();
								
								if ($data_scope_input == "all") {
									
									$eventLog->log("data scope input: all");
								
									//$all_classified_users_array["primary-recruiters"] = all_primary_recruiters_list($company_id_input);
									$data_scope = "recruiters";
									$all_classified_users_array["recruiters"] = all_classified_users_list($company_id_input,$data_scope);
									
									//$all_classified_users_array["all-recruiters"] = recruiters_list($company_id_input);
									
									//$all_classified_users_array["primary-sales"] = all_primary_sales_list($company_id_input);
									$data_scope = "sales";
									$all_classified_users_array["sales"] = all_classified_users_list($company_id_input,$data_scope);
									
									//$all_classified_users_array["all-sales"] = sales_list($company_id_input);
									
									
			
									$response['data'] = $all_classified_users_array;
									$response['status'] = "all-classified-users-list-fetched-successfully";
									$response['status_description'] = "List successfully receved.";
									
									$eventLog->log("All classified users list successfully fetched.");	
									
								/* } else if ($data_scope_input == "primary-recruiters") {
									
									$eventLog->log("data scope input: primary-recruiters");
									
									$all_classified_users_array["primary-recruiters"] = all_primary_recruiters_list($company_id_input);
									
									
									$response['data'] = $all_classified_users_array;
									$response['status'] = "candidate-visa-documentation-summary-result";
									$response['status_description'] = "Candidate specific Visa Documentation Summary";
									
									$eventLog->log("candidate-visa-documentation-summary-result: Candidate specific Visa Documentation Summary, w.r.t. particular Candidate.");	
									 */
								} else if ($data_scope_input == "recruiters") {
									
									$eventLog->log("data scope input: recruiters");
									$all_classified_users_array["recruiters"] = all_classified_users_list($company_id_input,$data_scope_input);
									
									$response['data'] = $all_classified_users_array;
									$response['status'] = "all-recruiters-list-fetched-successfully";
									$response['status_description'] = "All Recruiters list successfully fetched";
									
									$eventLog->log("All Recruiters list successfully fetched.");	
								/* } else if ($data_scope_input == "all-recruiters") {
									
									$eventLog->log("data scope input: all-recruiters");
									
									$all_classified_users_array["primary-recruiters"] = candidate_education_qualification_records_list($user_id_input);
									
									$all_classified_users_array["recruiters"] = candidate_education_qualification_records_list_with_uploaded_file_info($user_id_input);
									
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = $all_classified_users_array;
									$response['status'] = "candidate-visa-documentation-summary-result";
									$response['status_description'] = "Candidate specific Visa Documentation Summary";
									
									$eventLog->log("candidate-visa-documentation-summary-result: Candidate specific Visa Documentation Summary, w.r.t. particular Candidate.");	
									
								} else if ($data_scope_input == "primary-sales") {
									
									$eventLog->log("data scope input: primary-sales");
									
									$all_classified_users_array["primary-sales"] = candidate_professional_summary_records_list($user_id_input);
									
									//$all_collected_candidate_visa_documentation_info_array["professional_summary"] = candidate_professional_summary_records_list_with_uploaded_file_info($user_id_input);
									
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = $all_classified_users_array;
									$response['status'] = "candidate-visa-documentation-summary-result";
									$response['status_description'] = "Candidate specific Visa Documentation Summary";
									
									$eventLog->log("candidate-visa-documentation-summary-result: Candidate specific Visa Documentation Summary, w.r.t. particular Candidate.");	
									 */
								} else if ($data_scope_input == "sales") {
									
									$eventLog->log("data scope input: sales");
									
									$all_classified_users_array["sales"] = all_classified_users_list($company_id_input,$data_scope_input);
									
									$response['data'] = $all_classified_users_array;
									$response['status'] = "all-sales-list-fetched-successfully";
									$response['status_description'] = "All Sales list successfully fetched";
									
									$eventLog->log("All Sales list successfully fetched.");
								/* } else if ($data_scope_input == "all-sales") {
									
									$eventLog->log("data scope input: all-sales");
									
									$all_classified_users_array["primary-sales"] = candidate_rel_client_vendor_data_summary($user_id_input);
									$all_classified_users_array["sales"] = candidate_rel_client_vendor_data_summary($user_id_input);
									//Construct Content, that will be sent in Response body, of the REST Web Service
									$response['data'] = $all_classified_users_array;
									$response['status'] = "candidate-visa-documentation-summary-result";
									$response['status_description'] = "Candidate specific Visa Documentation Summary";
									
									$eventLog->log("candidate-visa-documentation-summary-result: Candidate specific Visa Documentation Summary, w.r.t. particular Candidate.");	
									 */
								} else {
									
									$response['data'] = array();
									$response['status'] = "no-data-scope-found";
									$response['status_description'] = "Invalid data scope";
									
									$eventLog->log("Invalid data scope.");
									
								}//close of else of if ($data_scope_input == "all") {
									
								
									
							} catch (Exception $e) {
								
								//Construct Content, that will be sent in Response body, of the REST Web Service
								$response['data'] = array();
								$response['status'] = "classified-list-fetching-error";
								$response['status_description'] = "No records found.";
								
								$eventLog->log("Classified Users list fetching error.");
								
							}
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-subject-platform-scope-user";
							$response['status_description'] = "Insufficient Set of Permissions, to complete this action";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-subject-platform-scope-user: Insufficient Set of Permissions, as the User belongs to Admin Group in Platform Scope, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-different-company-user-id";
							$response['status_description'] = "Insufficient Set of Permissions, as the User belongs to a different company";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-different-company-user-id: Insufficient Set of Permissions, as the User belongs to a different company, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-INSUFFICIENT-PERMISSIONS") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions";
							$response['status_description'] = "Insufficient Set of Permissions, as this process will not be allowed any further";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions: Insufficient Set of Permissions, as this process will not be allowed any further, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-self-action";
							$response['status_description'] = "Insufficient Set of Permissions, as self action is Dis-allowed";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-disallowed-self-action: Insufficient Set of Permissions, as self action is Dis-allowed, please check and try again.");	
							
						} else if ($ea_action_specific_auth_check_result == "DONOT-PROCEED-TO-NEXT-STEP-AS-ACTION-SUBJECT-IS-A-DIFFERENT-USER") {
							
							//Construct Content, that will be sent in Response body, of the REST Web Service
							$response['data'] = array();
							$response['status'] = "insufficient-permissions-disallowed-action-on-different-user";
							$response['status_description'] = "Insufficient Set of Permissions, as action subject is a different user";
							
							//Define Response Header, with 403 Forbidden HTTP Response Code, back to the Client Application.
							header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 403 Forbidden');
							
							$eventLog->log("insufficient-permissions-disallowed-action-on-different-user: Insufficient Set of Permissions, as action subject is a different user, please check and try again.");	
							
						}//close of if ($ea_action_specific_auth_check_result == "PROCEED-TO-NEXT-STEP") {
						
						
					} else {
						
						//Construct Content, that will be sent in Response body, of the REST Web Service
						$response['data'] = array();
						$response['status'] = "invalid-user-account";
						$response['status_description'] = "Invalid User Account, that is attempted to be edited";
						
						$eventLog->log("invalid-user-account:Invalid User Account, that is attempted to be edited.");	
					
					}//close of else of if (count($user_basic_details_result) > 0) {
					

				}//close of else of if ($user_type_input != 'admin') {
					
			}//close of //close of if ($ea_is_user_page_access_authorized) {
			
			
		} else {
			
			//Construct Content, that will be sent in Response body, of the REST Web Service
			$response['data'] = array();
			$response['status'] = "invalid-auth-token-submitted";
			$response['status_description'] = "Invalid Authentication Token Submitted";
			$eventLog->log("invalid-auth-token-submitted: Invalid Authentication Token Submitted, please check and try again.");
			//Define Response Header, with 401 Unauthorized HTTP Response Code, back to the Client Application. This is specific to Invalid JWT Token Submission by Client Applications.
			header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 401 Unauthorized');
			
		}//close of else of if ($ea_auth_token_validation_status) {
	
	}//close of if ($ea_maintenance_mode == false) {
	
} else {

	//Construct Content, that will be sent in Response body, of the REST Web Service
	$response['data'] = array();
	$response['status'] = "invalid-input";
	$response['status_description'] = "Invalid Input";
	$eventLog->log("invalid-input: Invalid Input, Please check and provide all information.");
	
	//Define Response Header, with 400 Bad Request HTTP Response Code, back to the Client Application
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 400 Bad Request');
}//close of else of if ((isset($ea_received_rest_ws_raw_array_input)) && (is_array($ea_received_rest_ws_raw_array_input)) && (count($ea_received_rest_ws_raw_array_input) == "3")) {




//Check if Maintenance Mode is Turned On
if ($ea_maintenance_mode) {	
	
	//Define Response Header, that sends Maintenance Status and corresponding Wait time information, back to the Client Application
	//header('Maintenance-Progress: true', false);
	//header('Maintenance-Time: '.html_escaped_output($ea_maintanance_mode_time), false);	
	$response['data'] = array();
	$response['status'] = "application-maintenance-in-progress";
	$response['status_description'] = "Application is under active maintenance. Maintenance Activity will be completed in " . html_escaped_output($ea_maintanance_mode_time);
	header(html_escaped_output($_SERVER['SERVER_PROTOCOL']) . ' 503 Service Unavailable');
	
} else {	

	//Define Response Header, that conveys the info that, the response will be issued in JSON Format and with Content-Type: application/json, back to the Client Application
	header('Content-Type: application/json');
	echo json_encode($response,JSON_PRETTY_PRINT);
	
}//close of else of if ($ea_maintenance_mode){

//THIS FUNCTION ALREADY EXISTS IN EDUCATION QUALIFICATIONS PAGE. ONE COPY HAS TO BE DELETED, WHEN MOVING THEM IN TO COMMON FILES.
function all_classified_users_list($company_id_input,$data_scope_input) {
	global $dbcon;
	$sm_user_status = "1";
	
	$constructed_array = array();
	$valid_to_date = "present";
	$sm_user_type = "admin";
	
	if($data_scope_input == "recruiters") {
	   $lead_classification_id = 7;
	   $classification_id = 6;
	} 
	if($data_scope_input == "sales") {
	   $lead_classification_id = 5;
	   $classification_id = 4;
	}
	$all_classified_users_list_sql = "SELECT * FROM site_members sm LEFT JOIN sm_site_member_classification_associations ssmca ON ssmca.sm_memb_id = sm.sm_memb_id WHERE sm.sm_user_status = :sm_user_status AND sm.sm_user_type LIKE :sm_user_type AND ssmca.valid_to_date LIKE :valid_to_date AND sm.company_id = :company_id AND ssmca.sm_site_member_classification_detail_id IN (:lead_classification_detail_id,:classification_detail_id) ORDER BY sm.sm_memb_id ASC";

	$all_classified_users_list_select_query = $dbcon->prepare($all_classified_users_list_sql);
	$all_classified_users_list_select_query->bindValue(":company_id",$company_id_input);
	$all_classified_users_list_select_query->bindValue(":lead_classification_detail_id",$lead_classification_id);
	$all_classified_users_list_select_query->bindValue(":classification_detail_id",$classification_id);
	$all_classified_users_list_select_query->bindValue(":sm_user_status",$sm_user_status);
	$all_classified_users_list_select_query->bindValue(":valid_to_date",$valid_to_date);
	$all_classified_users_list_select_query->bindValue(":sm_user_type",$sm_user_type);	
	$all_classified_users_list_select_query->execute(); 
	
	if($all_classified_users_list_select_query->rowCount() > 0) {
		$all_classified_users_list_select_query_result = $all_classified_users_list_select_query->fetchAll();
		
		foreach($all_classified_users_list_select_query_result as $all_classified_users_list_select_query_result_row) {
			
			$temp = array();		
			$temp["sm_memb_id"] = $all_classified_users_list_select_query_result_row["sm_memb_id"];
			$temp["sm_salutation"] = $all_classified_users_list_select_query_result_row["sm_salutation"];
			$temp["sm_firstname"] = $all_classified_users_list_select_query_result_row["sm_firstname"];
			$temp["sm_middlename"] = $all_classified_users_list_select_query_result_row["sm_middlename"];
			$temp["sm_lastname"] = $all_classified_users_list_select_query_result_row["sm_lastname"];
			$temp["sm_fullname"] = $temp["sm_firstname"]. " ". $temp["sm_middlename"] . " ". $temp["sm_lastname"];
			$temp["sm_site_member_classification_detail_id"] = $all_classified_users_list_select_query_result_row["sm_site_member_classification_detail_id"];
			$temp["sm_email"] = $all_classified_users_list_select_query_result_row["sm_email"];
			$temp["company_id"] = $all_classified_users_list_select_query_result_row["company_id"];
			
			$constructed_array[] = $temp;
		}//close of foreach($candidate_education_qualification_records_list_select_query_result as $candidate_education_qualification_records_list_select_query_result_row) {
			
		
		return $constructed_array;
	
	}//close of if($candidate_education_qualification_records_list_select_query->rowCount() > 0) {
	return $constructed_array;
}

exit;
?>