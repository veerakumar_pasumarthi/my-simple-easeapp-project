-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2020 at 02:38 AM
-- Server version: 10.2.27-MariaDB-1:10.2.27+maria~bionic-log
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jobs_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` smallint(4) NOT NULL,
  `country_name` varchar(75) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `country_two_lettered_code` char(2) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_active_status` enum('0','1') COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1' COMMENT '0: Disabled, 1: Enabled'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `country_name`, `country_two_lettered_code`, `is_active_status`) VALUES
(1, 'Afghanistan', 'AF', '1'),
(2, 'Aland Islands', 'AX', '1'),
(3, 'Albania', 'AL', '1'),
(4, 'Algeria', 'DZ', '1'),
(5, 'American Samoa', 'AS', '1'),
(6, 'Andorra', 'AD', '1'),
(7, 'Angola', 'AO', '1'),
(8, 'Anguilla', 'AI', '1'),
(9, 'Antarctica', 'AQ', '1'),
(10, 'Antigua and Barbuda', 'AG', '1'),
(11, 'Argentina', 'AR', '1'),
(12, 'Armenia', 'AM', '1'),
(13, 'Aruba', 'AW', '1'),
(14, 'Australia', 'AU', '1'),
(15, 'Austria', 'AT', '1'),
(16, 'Azerbaijan', 'AZ', '1'),
(17, 'Bahamas', 'BS', '1'),
(18, 'Bahrain', 'BH', '1'),
(19, 'Bangladesh', 'BD', '1'),
(20, 'Barbados', 'BB', '1'),
(21, 'Belarus', 'BY', '1'),
(22, 'Belgium', 'BE', '1'),
(23, 'Belize', 'BZ', '1'),
(24, 'Benin', 'BJ', '1'),
(25, 'Bermuda', 'BM', '1'),
(26, 'Bhutan', 'BT', '1'),
(27, 'Bolivia', 'BO', '1'),
(28, 'Bonaire, Sint Eustatius and Saba', 'BQ', '1'),
(29, 'Bosnia and Herzegovina', 'BA', '1'),
(30, 'Botswana', 'BW', '1'),
(31, 'Bouvet Island', 'BV', '1'),
(32, 'Brazil', 'BR', '1'),
(33, 'British Indian Ocean Territory', 'IO', '1'),
(34, 'Brunei Darussalam', 'BN', '1'),
(35, 'Bulgaria', 'BG', '1'),
(36, 'Burkina Faso', 'BF', '1'),
(37, 'Burundi', 'BI', '1'),
(38, 'Cambodia', 'KH', '1'),
(39, 'Cameroon', 'CM', '1'),
(40, 'Canada', 'CA', '1'),
(41, 'Cape Verde', 'CV', '1'),
(42, 'Cayman Islands', 'KY', '1'),
(43, 'Central African Republic', 'CF', '1'),
(44, 'Chad', 'TD', '1'),
(45, 'Chile', 'CL', '1'),
(46, 'China', 'CN', '1'),
(47, 'Christmas Island', 'CX', '1'),
(48, 'Cocos (Keeling) Islands', 'CC', '1'),
(49, 'Colombia', 'CO', '1'),
(50, 'Comoros', 'KM', '1'),
(51, 'Congo', 'CG', '1'),
(52, 'Congo, The Democratic Republic of ', 'CD', '1'),
(53, 'Cook Islands', 'CK', '1'),
(54, 'Costa Rica', 'CR', '1'),
(55, 'Cote d\'Ivoire', 'CI', '1'),
(56, 'Croatia', 'HR', '1'),
(57, 'Cuba', 'CU', '1'),
(59, 'Cyprus', 'CY', '1'),
(60, 'Czechia', 'CZ', '1'),
(61, 'Denmark', 'DK', '1'),
(62, 'Djibouti', 'DJ', '1'),
(63, 'Dominica', 'DM', '1'),
(64, 'Dominican Republic', 'DO', '1'),
(65, 'Ecuador', 'EC', '1'),
(66, 'Egypt', 'EG', '1'),
(67, 'El Salvador', 'SV', '1'),
(68, 'Equatorial Guinea', 'GQ', '1'),
(69, 'Eritrea', 'ER', '1'),
(70, 'Estonia', 'EE', '1'),
(71, 'Ethiopia', 'ET', '1'),
(72, 'Falkland Islands (Malvinas)', 'FK', '1'),
(73, 'Faroe Islands', 'FO', '1'),
(74, 'Fiji', 'FJ', '1'),
(75, 'Finland', 'FI', '1'),
(76, 'France', 'FR', '1'),
(77, 'French Guiana', 'GF', '1'),
(78, 'French Polynesia', 'PF', '1'),
(79, 'French Southern Territories', 'TF', '1'),
(80, 'Gabon', 'GA', '1'),
(81, 'Gambia', 'GM', '1'),
(82, 'Georgia', 'GE', '1'),
(83, 'Germany', 'DE', '1'),
(84, 'Ghana', 'GH', '1'),
(85, 'Gibraltar', 'GI', '1'),
(86, 'Greece', 'GR', '1'),
(87, 'Greenland', 'GL', '1'),
(88, 'Grenada', 'GD', '1'),
(89, 'Guadeloupe', 'GP', '1'),
(90, 'Guam', 'GU', '1'),
(91, 'Guatemala', 'GT', '1'),
(92, 'Guernsey', 'GG', '1'),
(93, 'Guinea', 'GN', '1'),
(94, 'Guinea-Bissau', 'GW', '1'),
(95, 'Guyana', 'GY', '1'),
(96, 'Haiti', 'HT', '1'),
(97, 'Heard and Mc Donald Islands', 'HM', '1'),
(98, 'Holy See (Vatican City State)', 'VA', '1'),
(99, 'Honduras', 'HN', '1'),
(100, 'Hong Kong', 'HK', '1'),
(101, 'Hungary', 'HU', '1'),
(102, 'Iceland', 'IS', '1'),
(103, 'India', 'IN', '1'),
(104, 'Indonesia', 'ID', '1'),
(105, 'Iran, Islamic Republic of', 'IR', '1'),
(106, 'Iraq', 'IQ', '1'),
(107, 'Ireland', 'IE', '1'),
(108, 'Isle of Man', 'IM', '1'),
(109, 'Israel', 'IL', '1'),
(110, 'Italy', 'IT', '1'),
(111, 'Jamaica', 'JM', '1'),
(112, 'Japan', 'JP', '1'),
(113, 'Jersey', 'JE', '1'),
(114, 'Jordan', 'JO', '1'),
(115, 'Kazakstan', 'KZ', '1'),
(116, 'Kenya', 'KE', '1'),
(117, 'Kiribati', 'KI', '1'),
(118, 'Korea, Democratic People\'s Republic of', 'KP', '1'),
(119, 'Korea, Republic of', 'KR', '1'),
(120, 'Kosovo (temporary code)', 'XK', '1'),
(121, 'Kuwait', 'KW', '1'),
(122, 'Kyrgyzstan', 'KG', '1'),
(123, 'Lao, People\'s Democratic Republic', 'LA', '1'),
(124, 'Latvia', 'LV', '1'),
(125, 'Lebanon', 'LB', '1'),
(126, 'Lesotho', 'LS', '1'),
(127, 'Liberia', 'LR', '1'),
(128, 'Libyan Arab Jamahiriya', 'LY', '1'),
(129, 'Liechtenstein', 'LI', '1'),
(130, 'Lithuania', 'LT', '1'),
(131, 'Luxembourg', 'LU', '1'),
(132, 'Macao', 'MO', '1'),
(133, 'Macedonia, The Former Yugoslav Republic Of', 'MK', '1'),
(134, 'Madagascar', 'MG', '1'),
(135, 'Malawi', 'MW', '1'),
(136, 'Malaysia', 'MY', '1'),
(137, 'Maldives', 'MV', '1'),
(138, 'Mali', 'ML', '1'),
(139, 'Malta', 'MT', '1'),
(140, 'Marshall Islands', 'MH', '1'),
(141, 'Martinique', 'MQ', '1'),
(142, 'Mauritania', 'MR', '1'),
(143, 'Mauritius', 'MU', '1'),
(144, 'Mayotte', 'YT', '1'),
(145, 'Mexico', 'MX', '1'),
(146, 'Micronesia, Federated States of', 'FM', '1'),
(147, 'Moldova, Republic of', 'MD', '1'),
(148, 'Monaco', 'MC', '1'),
(149, 'Mongolia', 'MN', '1'),
(150, 'Montenegro', 'ME', '1'),
(151, 'Montserrat', 'MS', '1'),
(152, 'Morocco', 'MA', '1'),
(153, 'Mozambique', 'MZ', '1'),
(154, 'Myanmar', 'MM', '1'),
(155, 'Namibia', 'NA', '1'),
(156, 'Nauru', 'NR', '1'),
(157, 'Nepal', 'NP', '1'),
(158, 'Netherlands', 'NL', '1'),
(159, 'Netherlands Antilles', 'AN', '1'),
(160, 'New Caledonia', 'NC', '1'),
(161, 'New Zealand', 'NZ', '1'),
(162, 'Nicaragua', 'NI', '1'),
(163, 'Niger', 'NE', '1'),
(164, 'Nigeria', 'NG', '1'),
(165, 'Niue', 'NU', '1'),
(166, 'Norfolk Island', 'NF', '1'),
(167, 'Northern Mariana Islands', 'MP', '1'),
(168, 'Norway', 'NO', '1'),
(169, 'Oman', 'OM', '1'),
(170, 'Pakistan', 'PK', '1'),
(171, 'Palau', 'PW', '1'),
(172, 'Palestinian Territory, Occupied', 'PS', '1'),
(173, 'Panama', 'PA', '1'),
(174, 'Papua New Guinea', 'PG', '1'),
(175, 'Paraguay', 'PY', '1'),
(176, 'Peru', 'PE', '1'),
(177, 'Philippines', 'PH', '1'),
(178, 'Pitcairn', 'PN', '1'),
(179, 'Poland', 'PL', '1'),
(180, 'Portugal', 'PT', '1'),
(181, 'Puerto Rico', 'PR', '1'),
(182, 'Qatar', 'QA', '1'),
(183, 'Republic of Serbia', 'RS', '1'),
(184, 'Reunion', 'RE', '1'),
(185, 'Romania', 'RO', '1'),
(186, 'Russia Federation', 'RU', '1'),
(187, 'Rwanda', 'RW', '1'),
(189, 'Saint Helena', 'SH', '1'),
(190, 'Saint Kitts & Nevis', 'KN', '1'),
(191, 'Saint Lucia', 'LC', '1'),
(192, 'Saint Martin', 'MF', '1'),
(193, 'Saint Pierre and Miquelon', 'PM', '1'),
(194, 'Saint Vincent and the Grenadines', 'VC', '1'),
(195, 'Samoa', 'WS', '1'),
(196, 'San Marino', 'SM', '1'),
(197, 'Sao Tome and Principe', 'ST', '1'),
(198, 'Saudi Arabia', 'SA', '1'),
(199, 'Senegal', 'SN', '1'),
(200, 'Serbia and Montenegro', 'CS', '1'),
(201, 'Seychelles', 'SC', '1'),
(202, 'Sierra Leone', 'SL', '1'),
(203, 'Singapore', 'SG', '1'),
(204, 'Sint Maarten', 'SX', '1'),
(205, 'Slovakia', 'SK', '1'),
(206, 'Slovenia', 'SI', '1'),
(207, 'Solomon Islands', 'SB', '1'),
(208, 'Somalia', 'SO', '1'),
(209, 'South Africa', 'ZA', '1'),
(210, 'South Georgia & The South Sandwich Islands', 'GS', '1'),
(211, 'South Sudan', 'SS', '1'),
(212, 'Spain', 'ES', '1'),
(213, 'Sri Lanka', 'LK', '1'),
(214, 'Sudan', 'SD', '1'),
(215, 'Suriname', 'SR', '1'),
(216, 'Svalbard and Jan Mayen', 'SJ', '1'),
(217, 'Swaziland', 'SZ', '1'),
(218, 'Sweden', 'SE', '1'),
(219, 'Switzerland', 'CH', '1'),
(220, 'Syrian Arab Republic', 'SY', '1'),
(221, 'Taiwan, Province of China', 'TW', '1'),
(222, 'Tajikistan', 'TJ', '1'),
(223, 'Tanzania, United Republic of', 'TZ', '1'),
(224, 'Thailand', 'TH', '1'),
(225, 'Timor-Leste', 'TL', '1'),
(226, 'Togo', 'TG', '1'),
(227, 'Tokelau', 'TK', '1'),
(228, 'Tonga', 'TO', '1'),
(229, 'Trinidad and Tobago', 'TT', '1'),
(230, 'Tunisia', 'TN', '1'),
(231, 'Turkey', 'TR', '1'),
(232, 'Turkish Rep N Cyprus (temporary code)', 'XT', '1'),
(233, 'Turkmenistan', 'TM', '1'),
(234, 'Turks and Caicos Islands', 'TC', '1'),
(235, 'Tuvalu', 'TV', '1'),
(236, 'Uganda', 'UG', '1'),
(237, 'Ukraine', 'UA', '1'),
(238, 'United Arab Emirates', 'AE', '1'),
(239, 'United Kingdom', 'GB', '1'),
(240, 'United States', 'US', '1'),
(241, 'United States Minor Outlying Islands', 'UM', '1'),
(242, 'Uruguay', 'UY', '1'),
(243, 'Uzbekistan', 'UZ', '1'),
(244, 'Vanuatu', 'VU', '1'),
(245, 'Venezuela', 'VE', '1'),
(246, 'Vietnam', 'VN', '1'),
(247, 'Virgin Islands, British', 'VG', '1'),
(248, 'Virgin Islands, U.S.', 'VI', '1'),
(249, 'Wallis and Futuna', 'WF', '1'),
(250, 'Western Sahara', 'EH', '1'),
(251, 'Yemen', 'YE', '1'),
(252, 'Zambia', 'ZM', '1'),
(253, 'Zimbabwe', 'ZW', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`),
  ADD KEY `country_name` (`country_name`),
  ADD KEY `country_two_lettered_code` (`country_two_lettered_code`),
  ADD KEY `is_active_status` (`is_active_status`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
