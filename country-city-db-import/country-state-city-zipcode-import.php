<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('max_execution_time', 14000);
error_reporting(E_ALL);
$debug_mode = "ON";

//Database Connection Details 
$dbhost_site = "localhost";
$dbusername_site = "root";
$dbpassword_site = "";
$dbname_site = "zipcode";
    

try {

	$dbcon = new PDO("mysql:host=$dbhost_site;dbname=$dbname_site;charset=utf8mb4", $dbusername_site, $dbpassword_site);
	// throw exceptions in case of errors (default: stay silent)
	$dbcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$dbcon->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	// fetch associative arrays (default: mixed arrays)
	$dbcon->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	
	echo "db connected<br>";

}
catch (PDOException $e) {

  if($debug_mode == "ON")
	{
		print "Error!: " . $e->getMessage() . "<br>";
	  //var_dump($e);
	  die();
	}
	elseif($debug_mode == "OFF")
	{
		echo "db related error!!!";
	  die();
	}
  //print "Error!: " . $e->getMessage() . "<br>";
  //var_dump($e);
  //die();
}
$data = file_get_contents('us-zip-code-latitude-and-longitude.json');
$data_json_decoded = json_decode($data, true);
echo "<pre>";
//var_dump($data_json_decoded);

foreach($data_json_decoded as $data_json_decoded_content_row) {
	
	/* array(5) {
	  ["datasetid"]=>
	  string(34) "us-zip-code-latitude-and-longitude"
	  ["recordid"]=>
	  string(40) "6a0a9c66f8e0292a54c9f023c93732f1b41d8943"
	  ["fields"]=>
	  array(8) {
		["city"]=>
		string(4) "Cove"
		["zip"]=>
		string(5) "71937"
		["dst"]=>
		int(1)
		["geopoint"]=>
		array(2) {
		  [0]=>
		  float(34.398483)
		  [1]=>
		  float(-94.39398)
		}
		["longitude"]=>
		float(-94.39398)
		["state"]=>
		string(2) "AR"
		["latitude"]=>
		float(34.398483)
		["timezone"]=>
		int(-6)
	  }
	  ["geometry"]=>
	  array(2) {
		["type"]=>
		string(5) "Point"
		["coordinates"]=>
		array(2) {
		  [0]=>
		  float(-94.39398)
		  [1]=>
		  float(34.398483)
		}
	  }
	  ["record_timestamp"]=>
	  string(29) "2018-02-09T22:03:38.603+05:30"
	}
 */	
	//var_dump($data_json_decoded_content_row);
	echo "datasetid: " . $data_json_decoded_content_row["datasetid"] . "<br>";
	echo "recordid: " . $data_json_decoded_content_row["recordid"] . "<br>";
	echo "city:" . $data_json_decoded_content_row["fields"]["city"] . "<br>";
	echo "zip: " . $data_json_decoded_content_row["fields"]["zip"] . "<br>";
	echo "dst: " . $data_json_decoded_content_row["fields"]["dst"] . "<br>";
	echo "longitude: " . $data_json_decoded_content_row["fields"]["longitude"] . "<br>";
	echo "state: " . $data_json_decoded_content_row["fields"]["state"] . "<br>";
	echo "latitude: " . $data_json_decoded_content_row["fields"]["latitude"] . "<br>";
	echo "timezone: " . $data_json_decoded_content_row["fields"]["timezone"] . "<br>";
	echo "record_timestamp: " . $data_json_decoded_content_row["record_timestamp"] . "<br>";
	
	$state_details_result = get_state_details($data_json_decoded_content_row["fields"]["state"]);
	
	if (count($state_details_result) > 0) {
		
		$state_id = $state_details_result["state_id"];
		
		$city_details_result = get_city_details($state_id, $data_json_decoded_content_row["fields"]["state"], $data_json_decoded_content_row["fields"]["city"], $data_json_decoded_content_row["fields"]["zip"]);
		
		if (count($city_details_result) > 0) {
			
			echo "Duplicate Exists for city: " . $data_json_decoded_content_row["fields"]["city"] . " | Zipcode or Pincode: " . $data_json_decoded_content_row["fields"]["zip"] . "\n";
			
		} else {
			
			//Do Insert Query
			insert_city_details('240', $state_id, $data_json_decoded_content_row["fields"]["state"], $data_json_decoded_content_row["fields"]["city"], $data_json_decoded_content_row["fields"]["zip"], $data_json_decoded_content_row["fields"]["dst"], $data_json_decoded_content_row["fields"]["longitude"], $data_json_decoded_content_row["fields"]["latitude"], $data_json_decoded_content_row["fields"]["timezone"]);
			
		}
		
		
		
	}
	//exit;
}

//Do Get state_id from states db table
function get_state_details($state_two_lettered_code_input) {
global $dbcon;

	$constructed_array = array();
	
	$state_details_get_sql = "SELECT * FROM `states` WHERE `state_two_lettered_code` LIKE :state_two_lettered_code"; 	 
	$state_details_get_select_query = $dbcon->prepare($state_details_get_sql);
	$state_details_get_select_query->bindValue(":state_two_lettered_code",$state_two_lettered_code_input);
	$state_details_get_select_query->execute();

	if($state_details_get_select_query->rowCount() > 0) {
	   $state_details_get_select_query_result = $state_details_get_select_query->fetch();
	   //print_r($state_details_get_select_query_result);
	   return $state_details_get_select_query_result;
	}
	return $constructed_array;
}

//Do Get city details from cities db table
function get_city_details($state_id_input, $state_two_lettered_code_input, $city_name_input, $zipcode_input) {
global $dbcon;

	$constructed_array = array();
	
	$city_details_get_sql = "SELECT * FROM `cities` WHERE `state_id`=:state_id AND `state_two_lettered_code` LIKE :state_two_lettered_code AND `city_name` LIKE :city_name AND `zipcode_or_pincode` LIKE :zipcode_or_pincode"; 	 
	$city_details_get_select_query = $dbcon->prepare($city_details_get_sql);
	$city_details_get_select_query->bindValue(":state_id",$state_id_input);
	$city_details_get_select_query->bindValue(":state_two_lettered_code",$state_two_lettered_code_input);
	$city_details_get_select_query->bindValue(":city_name",$city_name_input);
	$city_details_get_select_query->bindValue(":zipcode_or_pincode",$zipcode_input);
	$city_details_get_select_query->execute();

	if($city_details_get_select_query->rowCount() > 0) {
	   $city_details_get_select_query_result = $city_details_get_select_query->fetch();
	   //print_r($city_details_get_select_query_result);
	   return $city_details_get_select_query_result;
	}
	return $constructed_array;
}

function insert_city_details($country_id_input, $state_id_input, $state_two_lettered_code_input, $city_name_input, $zipcode_or_pincode_input, $dst_input, $longitude_input, $latitude_input, $timezone_offset_input) {
	global $dbcon;
	
	$city_details_insert_sql = "INSERT INTO `cities`(`country_id`, `state_id`, `state_two_lettered_code`, `city_name`, `zipcode_or_pincode`, `dst`, `longitude`, `latitude`, `timezone_offset`) VALUES (:country_id,:state_id,:state_two_lettered_code,:city_name,:zipcode_or_pincode,:dst,:longitude,:latitude,:timezone_offset)";
	$city_details_insert_query = $dbcon->prepare($city_details_insert_sql);
	$city_details_insert_query->bindValue(":country_id",$country_id_input);
	$city_details_insert_query->bindValue(":state_id",$state_id_input);
	$city_details_insert_query->bindValue(":state_two_lettered_code",$state_two_lettered_code_input);
	$city_details_insert_query->bindValue(":city_name",$city_name_input);
	$city_details_insert_query->bindValue(":zipcode_or_pincode",$zipcode_or_pincode_input);
    $city_details_insert_query->bindValue(":dst",$dst_input);
	$city_details_insert_query->bindValue(":longitude",$longitude_input);
	$city_details_insert_query->bindValue(":latitude",$latitude_input);
	$city_details_insert_query->bindValue(":timezone_offset",$timezone_offset_input);
	
		if ($city_details_insert_query->execute()) {
			
            $last_inserted_id = $dbcon->lastInsertId();			
			//$eventLog->log("record inserted successfully");
			
			return $last_inserted_id;
						
		} else {
		    //$eventLog->log("Error occurred during process. Please try again");						
				return "";						
	    }
}

?>