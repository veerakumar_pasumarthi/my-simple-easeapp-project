-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2020 at 04:34 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zipcode`
--

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `state_id` smallint(4) NOT NULL,
  `country_id` smallint(4) NOT NULL,
  `state_name` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `state_two_lettered_code` char(2) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_active_status` enum('0','1') COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1' COMMENT '0: Disabled, 1: Enabled 	'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state_id`, `country_id`, `state_name`, `state_two_lettered_code`, `is_active_status`) VALUES
(1, 240, 'Alabama', 'AL', '1'),
(2, 240, 'Alaska', 'AK', '1'),
(3, 240, 'Arizona', 'AZ', '1'),
(4, 240, 'Arkansas', 'AR', '1'),
(5, 240, 'California', 'CA', '1'),
(6, 240, 'Colorado', 'CO', '1'),
(7, 240, 'Connecticut', 'CT', '1'),
(8, 240, 'Delaware', 'DE', '1'),
(9, 240, 'District of Columbia', 'DC', '1'),
(10, 240, 'Florida', 'FL', '1'),
(11, 240, 'Georgia', 'GA', '1'),
(12, 240, 'Hawaii', 'HI', '1'),
(13, 240, 'Idaho', 'ID', '1'),
(14, 240, 'Illinois', 'IL', '1'),
(15, 240, 'Indiana', 'IN', '1'),
(16, 240, 'Iowa', 'IA', '1'),
(17, 240, 'Kansas', 'KS', '1'),
(18, 240, 'Kentucky', 'KY', '1'),
(19, 240, 'Louisiana', 'LA', '1'),
(20, 240, 'Maine', 'ME', '1'),
(21, 240, 'Montana', 'MT', '1'),
(22, 240, 'Nebraska', 'NE', '1'),
(23, 240, 'Nevada', 'NV', '1'),
(24, 240, 'New Hampshire', 'NH', '1'),
(25, 240, 'New Jersey', 'NJ', '1'),
(26, 240, 'New Mexico', 'NM', '1'),
(27, 240, 'New York', 'NY', '1'),
(28, 240, 'North Carolina', 'NC', '1'),
(29, 240, 'North Dakota', 'ND', '1'),
(30, 240, 'Ohio', 'OH', '1'),
(31, 240, 'Oklahoma', 'OK', '1'),
(32, 240, 'Oregon', 'OR', '1'),
(33, 240, 'Maryland', 'MD', '1'),
(34, 240, 'Massachusetts', 'MA', '1'),
(35, 240, 'Michigan', 'MI', '1'),
(36, 240, 'Minnesota', 'MN', '1'),
(37, 240, 'Mississippi', 'MS', '1'),
(38, 240, 'Missouri', 'MO', '1'),
(39, 240, 'Pennsylvania', 'PA', '1'),
(40, 240, 'Rhode Island', 'RI', '1'),
(41, 240, 'South Carolina', 'SC', '1'),
(42, 240, 'South Dakota', 'SD', '1'),
(43, 240, 'Tennessee', 'TN', '1'),
(44, 240, 'Texas', 'TX', '1'),
(45, 240, 'Utah', 'UT', '1'),
(46, 240, 'Vermont', 'VT', '1'),
(47, 240, 'Virginia', 'VA', '1'),
(48, 240, 'Washington', 'WA', '1'),
(49, 240, 'West Virginia', 'WV', '1'),
(50, 240, 'Wisconsin', 'WI', '1'),
(51, 240, 'Wyoming', 'WY', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`),
  ADD KEY `country_name` (`state_name`),
  ADD KEY `country_two_lettered_code` (`state_two_lettered_code`),
  ADD KEY `is_active_status` (`is_active_status`),
  ADD KEY `country_id` (`country_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `state_id` smallint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
