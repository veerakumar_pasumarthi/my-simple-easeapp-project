<?php
/**
 * Robots.Txt Parser.
 * --
 * 
 * Original PHP code by Chirp Internet: www.chirp.com.au
 * Adapted to include 404 and Allow directive checking by Eric at LinkUp.com
 * Simpler handling of 404, separation of loading robots.txt file & other 
 * minor tweaks by Orinoco http://jugglingedge.com
 * some additional improvements that includes Local Robots.txt Cache (with 1 day cache timeout), optional usage of Curl to retrieve Robots.txt file etc... by Raghu Veer D.
 * Please acknowledge use of this code by including this header.
 * --
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 */
  
  // Check robots.txt
  function getRobotsTxt($url, $crawler_user_agent, $ssl_requirement = false)
  {
	global $siteroot_basedir_cli; //$siteroot_basedir_cli from /app/core/command-line-include.php  
	//echo "siteroot_basedir_cli: " . $siteroot_basedir_cli . "\n";
    $parsed_url = parse_url($url);
	//Check if Port Number Exists,
	if ($parsed_url['port'] != "") {
		//if Port Number exists, the port number will be added in the file name
		$robots_txt_local_filename = $siteroot_basedir_cli . "robots-txt-cache/" . $parsed_url['scheme'] . "_" . str_ireplace(".", "_", $parsed_url['host']) . "_" . $parsed_url['port'] . "_robotstxt";
	} else {
		//if Port Number does not exist, the port number will not be added in the file name 
		$robots_txt_local_filename = $siteroot_basedir_cli . "robots-txt-cache/" . $parsed_url['scheme'] . "_" . str_ireplace(".", "_", $parsed_url['host']) . "_" . "robotstxt";
	}
	$robots_txt_local_filename = trim($robots_txt_local_filename);
	
	//Check if the constructed file path and file name exists, likewise, check if the last modified time of the file is more than 1 day
	if ((file_exists($robots_txt_local_filename)) && ($current_epoch - filemtime($robots_txt_local_filename) < 86400)) {
		//echo "The file $robots_txt_local_filename exists \n";
		
		if (is_readable($robots_txt_local_filename)) {
			//echo 'The file is readable \n';
			//Read the file content in to an array
			$robotstxt = @file($robots_txt_local_filename);
			
		} else {
			//echo 'The file is not readable \n';
			$robotstxt = false;
		}//close of else of if (is_readable($robots_txt_local_filename)) {
		
	} else {
		//echo "The file $robots_txt_local_filename does not exist \n";
		
		//Construct the Robots.txt file url and get the content of the Robots.txt file from the remote server, cache it and serve it
		$robots_txt_file_url = $parsed_url['scheme'] . '://' . $parsed_url['host'].'/robots.txt';
		if(function_exists('curl_init')) {
		  //$handle = curl_init("http://{$parsed['host']}/robots.txt");
		  $handle = curl_init($robots_txt_file_url);
		  curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
		  curl_setopt($handle, CURLOPT_USERAGENT,$crawler_user_agent);
		  //curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
			if ($ssl_requirement === true) {
				curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 1);
				curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 1);
				
			} else {
				curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 0);
				
			}
		  curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 0); 
		  curl_setopt($handle, CURLOPT_TIMEOUT, 60);
		  $robots_txt_file_content = curl_exec($handle);
		  $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
		  if($httpCode == "200") {
			// Write the contents of the remote robots.txt file in to the local file (cache)
			file_put_contents($robots_txt_local_filename, $robots_txt_file_content);
			if (is_readable($robots_txt_local_filename)) {
				//echo 'The file is readable \n';
				//Read the file content in to an array
				$robotstxt = @file($robots_txt_local_filename);
				
			} else {
				//echo 'The file is not readable \n';
				$robotstxt = false;
			}//close of else of if (is_readable($robots_txt_local_filename)) {  
			//$robotstxt = explode("\n", $robots_txt_file_content);
		  } else {
			$robotstxt = false;
		  }
		  curl_close($handle);
		} else {
			//get content of remote robots.txt file
			$robots_txt_file_content = file_get_contents($robots_txt_file_url);
			
			// Write the contents of the remote robots.txt file in to the local file (cache)
			file_put_contents($robots_txt_local_filename, $robots_txt_file_content);
            if (is_readable($robots_txt_local_filename)) {
				//echo 'The file is readable \n';
				//Read the file content in to an array
				$robotstxt = @file($robots_txt_local_filename);
				
			} else {
				//echo 'The file is not readable \n';
				$robotstxt = false;
			}//close of else of if (is_readable($robots_txt_local_filename)) {			
		    //$robotstxt = @file($robots_txt_file_url);
		}//close of else of if(function_exists('curl_init')) {
	}//close of else of if (file_exists($robots_txt_local_filename)) {
	//Do call clearstatecache function to clear any cached result of file_exists function
	clearstatcache();
	//return explode("\n",file_get_contents($RobotsURL));
	return $robotstxt;
  }
  
  /*// Check robots.txt
  function getRobotsTxt($url, $crawler_user_agent, $ssl_requirement = false)
  {
    $parsed_url = parse_url($url);
	$robots_txt_file_url = $parsed_url['scheme'] . '://' . $parsed_url['host'].'/robots.txt';
	if(function_exists('curl_init')) {
      //$handle = curl_init("http://{$parsed['host']}/robots.txt");
	  $handle = curl_init($robots_txt_file_url);
      curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
	  curl_setopt($handle, CURLOPT_USERAGENT,$crawler_user_agent);
	  //curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
		if ($ssl_requirement === true) {
			curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 1);
			curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 1);
			
		} else {
			curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 0);
			
		}
	  curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 0); 
	  curl_setopt($handle, CURLOPT_TIMEOUT, 60);
      $robots_txt_file_content = curl_exec($handle);
      $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
      if($httpCode == "200") {
        $robotstxt = explode("\n", $robots_txt_file_content);
      } else {
        $robotstxt = false;
      }
      curl_close($handle);
    } else {
      //$robotstxt = @file("http://{$parsed['host']}/robots.txt");
	  $robotstxt = @file($robots_txt_file_url);
    }//close of else of if(function_exists('curl_init')) {
    //return explode("\n",file_get_contents($RobotsURL));
	return $robotstxt;
  }*/

  function AllowRobotIn($URL,$RobotsTxt,$UserAgent)
  {
    if(empty($RobotsTxt)) // Site has no robots.txt file, ok to continue
      return true;

// Escape user agent name for use in regexp just in case
    $UserAgent=preg_quote($UserAgent,'/');

// Get list of rules that apply to us
    $Rules=[];
    $Applies=false;
    foreach($RobotsTxt as $Line)
      {
// skip blanks & comments
        if(trim($Line)=='' || $Line[0]=='#')
          continue;

        if(preg_match('/^\s*User-agent:\s*(.*)/i',$Line,$Match))
          {
// Found start of a User-agent block, check if
// it applies to all bots, or our specific bot
            $Applies=preg_match("/(\*|$UserAgent)/i",$Match[1]);
            continue;
          }

        if($Applies)
          {
// Add rules to our $Rules array
            list($Type,$Rule)=explode(':',$Line,2);
            $Type=trim(strtolower($Type)); // Allow or Disallow
            $Rules[]=['Type'=>$Type,'Match'=>preg_quote(trim($Rule),'/')];
          }
      }


// Check URL against our list of rules

    $ParsedURL=parse_url($URL);

    $Allowed=true;
    $MaxLength=0;
    foreach($Rules as $Rule)
      {
        if(preg_match('/^'.$Rule['Match'].'/',$ParsedURL['path']))
          {
// Specified rule applies to the URL we are checking
// Longer rules > Shorter rules
// Allow > Disallow if rules same length

            $ThisLength=strlen($Rule['Match']);
            if($MaxLength<$ThisLength)
              {
                $Allowed=($Rule['Type']=='allow');
                $MaxLength=$ThisLength;
              }
            elseif($MaxLength==$ThisLength && $Rule['Type']=='allow')
              {
                $Allowed=true;
              }
          }
      }

    return $Allowed;
  }
?>