<?php
defined('START') or die;

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * This page contains user authentication and info functions.
 */
 
function ea_check_user_login_with_email_address($email_address_input, $password_input) {
	
	global $dbcon;
	
	$constructed_array = array();
		
	//$user_details_get_sql = "SELECT * FROM `site_members` WHERE `sm_email`=:sm_email";	
	$user_details_get_sql = "SELECT * FROM `site_members` WHERE `sm_email` =:sm_email AND `sm_user_status` != :sm_user_status";	
	$user_details_get_select_query = $dbcon->prepare($user_details_get_sql);		
	$user_details_get_select_query->bindValue(":sm_email",$email_address_input);
	$user_details_get_select_query->bindValue(":sm_user_status","4");
	$user_details_get_select_query->execute();
	if($user_details_get_select_query->rowCount() > 0) {
		$user_details_get_select_query_result = $user_details_get_select_query->fetch();
		
		$sm_memb_id = $user_details_get_select_query_result["sm_memb_id"];
		$sm_email = $user_details_get_select_query_result["sm_email"];
		$sm_password = $user_details_get_select_query_result["sm_password"];
		$sm_mobile = $user_details_get_select_query_result["sm_mobile"];
		$sm_salutation = $user_details_get_select_query_result["sm_salutation"];
		$sm_firstname = $user_details_get_select_query_result["sm_firstname"];
		$sm_middlename = $user_details_get_select_query_result["sm_middlename"];
		$sm_lastname = $user_details_get_select_query_result["sm_lastname"];
		$company_id = $user_details_get_select_query_result["company_id"];
		$sm_user_type = $user_details_get_select_query_result["sm_user_type"];
		$sm_user_status = $user_details_get_select_query_result["sm_user_status"];
		
		$constructed_array["user_status"] = null;
		$constructed_array["login_request_auth_status"] = null;
	
		if ($sm_user_status == "1") {
			$constructed_array["user_status"] = "1";
			//verification of password in login process based on BCRYPT Hashing Algorithm using Password hashing API, that is supported from PHP v5.5 and beyond
			if (password_verify($password_input, $sm_password)) {
				$constructed_array["login_request_auth_status"] = "1"; // Login Successful
				
				//User Details, that will be passed, as part of JWT Token Content
				$constructed_array["user_id"] = $sm_memb_id;
				$constructed_array["user_salutation"] = $sm_salutation;
				$constructed_array["user_firstname"] = $sm_firstname;
				$constructed_array["user_middlename"] = $sm_middlename;
				$constructed_array["user_lastname"] = $sm_lastname;
				$constructed_array["user_company_id"] = $company_id;
				$constructed_array["user_type"] = $sm_user_type;
				
				$user_classification_details_result = ea_get_user_classification_details($sm_memb_id);
				
				$user_classification_name_association_ups_array = array();
				foreach($user_classification_details_result as $user_classification_details_result_row) {
					$sm_site_member_classification_detail_id = $user_classification_details_result_row["sm_site_member_classification_detail_id"];
					$sm_user_role = $user_classification_details_result_row["sm_user_role"];
					$user_privilege_summary = $user_classification_details_result_row["user_privilege_summary"];
					
					$user_classification_name_association_ups_array[] = $user_privilege_summary;
					//$user_classification_name_association_ups_array[] = $sm_site_member_classification_detail_id;
					
				}//close of foreach($user_classification_details_result as $user_classification_details_result_row) {
				
				$constructed_array["user_privileges_list"] = implode(",", $user_classification_name_association_ups_array);
				//$constructed_array["user_privileges_list"] = $user_classification_name_association_ups_array;
				
			} else {
				$constructed_array["login_request_auth_status"] = "0"; // Login Failure
			}
		} else if ($sm_user_status == "2") {
			$constructed_array["user_status"] = "2";
		} else if ($sm_user_status == "3") {
			$constructed_array["user_status"] = "3";
		} else {
			$constructed_array["user_status"] = "0";
		}//close of else of if ($sm_user_status == "1") {
		return $constructed_array;
	}//close of if($user_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
}

function ea_check_user_login_with_mobile_number($mobile_number_input, $password_input) {
	
	global $dbcon;
	
	$constructed_array = array();
		
	$user_details_get_sql = "SELECT * FROM `site_members` WHERE `sm_mobile`=:sm_mobile";	
	$user_details_get_select_query = $dbcon->prepare($user_details_get_sql);		
	$user_details_get_select_query->bindValue(":sm_mobile",$mobile_number_input);
	$user_details_get_select_query->execute();
	if($user_details_get_select_query->rowCount() > 0) {
		$user_details_get_select_query_result = $user_details_get_select_query->fetch();
		
		$sm_memb_id = $user_details_get_select_query_result["sm_memb_id"];
		$sm_email = $user_details_get_select_query_result["sm_email"];
		$sm_password = $user_details_get_select_query_result["sm_password"];
		$sm_mobile = $user_details_get_select_query_result["sm_mobile"];
		$sm_salutation = $user_details_get_select_query_result["sm_salutation"];
		$sm_firstname = $user_details_get_select_query_result["sm_firstname"];
		$sm_middlename = $user_details_get_select_query_result["sm_middlename"];
		$sm_lastname = $user_details_get_select_query_result["sm_lastname"];
		$company_id = $user_details_get_select_query_result["company_id"];
		$sm_user_type = $user_details_get_select_query_result["sm_user_type"];
		$sm_user_status = $user_details_get_select_query_result["sm_user_status"];
		
		$constructed_array["user_status"] = null;
		$constructed_array["login_request_auth_status"] = null;
	
		if ($sm_user_status == "1") {
			$constructed_array["user_status"] = "1";
			//verification of password in login process based on BCRYPT Hashing Algorithm using Password hashing API, that is supported from PHP v5.5 and beyond
			if (password_verify($password_input, $sm_password)) {
				$constructed_array["login_request_auth_status"] = "1"; // Login Successful
				
				//User Details, that will be passed, as part of JWT Token Content
				$constructed_array["user_id"] = $sm_memb_id;
				$constructed_array["user_salutation"] = $sm_salutation;
				$constructed_array["user_firstname"] = $sm_firstname;
				$constructed_array["user_middlename"] = $sm_middlename;
				$constructed_array["user_lastname"] = $sm_lastname;
				$constructed_array["user_company_id"] = $company_id;
				$constructed_array["user_type"] = $sm_user_type;
				
				$user_classification_details_result = ea_get_user_classification_details($sm_memb_id);
				
				$user_classification_name_association_ups_array = array();
				foreach($user_classification_details_result as $user_classification_details_result_row) {
					$sm_site_member_classification_detail_id = $user_classification_details_result_row["sm_site_member_classification_detail_id"];
					$sm_user_role = $user_classification_details_result_row["sm_user_role"];
					$user_privilege_summary = $user_classification_details_result_row["user_privilege_summary"];
					
					$user_classification_name_association_ups_array[] = $user_privilege_summary;
					//$user_classification_name_association_ups_array[] = $sm_site_member_classification_detail_id;
					
				}//close of foreach($user_classification_details_result as $user_classification_details_result_row) {
				
				$constructed_array["user_privileges_list"] = implode(",", $user_classification_name_association_ups_array);
				//$constructed_array["user_privileges_list"] = $user_classification_name_association_ups_array;
				
				
			} else {
				$constructed_array["login_request_auth_status"] = "0"; // Login Failure
			}
		} else if ($sm_user_status == "2") {
			$constructed_array["user_status"] = "2";
		} else if ($sm_user_status == "3") {
			$constructed_array["user_status"] = "3";
		} else {
			$constructed_array["user_status"] = "0";
		}//close of else of if ($sm_user_status == "1") {
		return $constructed_array;
	}//close of if($user_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
}

function ea_check_user_current_password_based_on_user_id($sm_memb_id_input, $current_password_input) {
	
	global $dbcon;
	
	$constructed_array = array();
		
	$user_details_get_sql = "SELECT * FROM `site_members` WHERE `sm_memb_id`=:sm_memb_id";	
	$user_details_get_select_query = $dbcon->prepare($user_details_get_sql);		
	$user_details_get_select_query->bindValue(":sm_memb_id",$sm_memb_id_input);
	$user_details_get_select_query->execute();
	if($user_details_get_select_query->rowCount() > 0) {
		$user_details_get_select_query_result = $user_details_get_select_query->fetch();
		
		$sm_password = $user_details_get_select_query_result["sm_password"];
		$sm_user_status = $user_details_get_select_query_result["sm_user_status"];
		
		$constructed_array["user_status"] = null;
		$constructed_array["current_password_verification_status"] = null;
	
		if ($sm_user_status == "1") {
			$constructed_array["user_status"] = "1";
			//verification of password in login process based on BCRYPT Hashing Algorithm using Password hashing API, that is supported from PHP v5.5 and beyond
			if (password_verify($current_password_input, $sm_password)) {
				// Current Password is Successfully Verified
				$constructed_array["current_password_verification_status"] = "1"; 
				
			} else {
				$constructed_array["current_password_verification_status"] = "0"; // In-correct Current Password
			}//close of else of if (password_verify($current_password_input, $sm_password)) {
				
		} else if ($sm_user_status == "2") {
			$constructed_array["user_status"] = "2";
		} else if ($sm_user_status == "3") {
			$constructed_array["user_status"] = "3";
		} else {
			$constructed_array["user_status"] = "0";
		}//close of else of if ($sm_user_status == "1") {
		return $constructed_array;
	}//close of if($user_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
}

function ea_get_user_classification_details($sm_memb_id_input) {
	
	global $dbcon;
	
	$constructed_array = array();
		
	$valid_to_date_input = '%'.'present'.'%';
	$is_active_status_input = '1';

	//Check in site_members db table
	$user_classification_details_get_sql = "SELECT * FROM `sm_site_member_classification_associations` ssmca LEFT JOIN sm_site_member_classification_details ssmcd ON ssmca.sm_site_member_classification_detail_id = ssmcd.sm_site_member_classification_detail_id WHERE ssmca.is_active_status =:ssmca_is_active_status AND ssmcd.is_active_status =:ssmcd_is_active_status AND ssmca.valid_to_date LIKE :ssmca_valid_to_date AND ssmca.sm_memb_id =:ssmca_sm_memb_id ORDER BY ssmca.sm_site_member_classification_association_id ASC";
	
	$user_classification_details_get_select_query = $dbcon->prepare($user_classification_details_get_sql);
	$user_classification_details_get_select_query->bindValue(":ssmca_is_active_status",$is_active_status_input);
	$user_classification_details_get_select_query->bindValue(":ssmcd_is_active_status",$is_active_status_input);
	$user_classification_details_get_select_query->bindValue(":ssmca_valid_to_date",$valid_to_date_input);
	$user_classification_details_get_select_query->bindValue(":ssmca_sm_memb_id",$sm_memb_id_input);
	$user_classification_details_get_select_query->execute();
	
	if($user_classification_details_get_select_query->rowCount() > 0) {
	   $user_classification_details_get_select_query_result = $user_classification_details_get_select_query->fetchAll();
	   //print_r($user_classification_details_get_select_query_result);
	   return $user_classification_details_get_select_query_result;
	}
	return $constructed_array;
	
}

function ea_get_user_groups_list($sm_user_type_input, $user_group_status_input) {
	
	global $dbcon;
	
	$constructed_array = array();
	
	$sm_user_role_input = '%'.'super-admin'.'%';

	
	if ($user_group_status_input == "0") {
		//Check in sm_site_member_classification_details db table
		$user_classification_details_get_sql = "SELECT * FROM `sm_site_member_classification_details` WHERE `sm_user_type`=:sm_user_type AND `sm_user_role` NOT LIKE :sm_user_role AND `is_active_status`=:is_active_status ORDER BY `sm_user_level` ASC";
		
		$user_classification_details_get_select_query = $dbcon->prepare($user_classification_details_get_sql);
		$user_classification_details_get_select_query->bindValue(":sm_user_type",$sm_user_type_input);
		$user_classification_details_get_select_query->bindValue(":sm_user_role",$sm_user_role_input);
		$user_classification_details_get_select_query->bindValue(":is_active_status","0");
		$user_classification_details_get_select_query->execute();
		
	} else if ($user_group_status_input == "1") {
		//Check in sm_site_member_classification_details db table
		$user_classification_details_get_sql = "SELECT * FROM `sm_site_member_classification_details` WHERE `sm_user_type`=:sm_user_type AND `sm_user_role` NOT LIKE :sm_user_role AND `is_active_status`=:is_active_status ORDER BY `sm_user_level` ASC";
		
		$user_classification_details_get_select_query = $dbcon->prepare($user_classification_details_get_sql);
		$user_classification_details_get_select_query->bindValue(":sm_user_type",$sm_user_type_input);
		$user_classification_details_get_select_query->bindValue(":sm_user_role",$sm_user_role_input);
		$user_classification_details_get_select_query->bindValue(":is_active_status","1");
		$user_classification_details_get_select_query->execute();
		
	} else {
		//Check in sm_site_member_classification_details db table
		$user_classification_details_get_sql = "SELECT * FROM `sm_site_member_classification_details` WHERE `sm_user_type`=:sm_user_type AND `sm_user_role` NOT LIKE :sm_user_role ORDER BY `sm_user_level` ASC";
		
		$user_classification_details_get_select_query = $dbcon->prepare($user_classification_details_get_sql);
		$user_classification_details_get_select_query->bindValue(":sm_user_type",$sm_user_type_input);
		$user_classification_details_get_select_query->bindValue(":sm_user_role",$sm_user_role_input);
		$user_classification_details_get_select_query->execute();
	}//close of else of if ($user_group_status_input == "0") {
	
	if($user_classification_details_get_select_query->rowCount() > 0) {
	    $user_classification_details_get_select_query_result = $user_classification_details_get_select_query->fetchAll();
	    //print_r($user_classification_details_get_select_query_result);
	    
		foreach ($user_classification_details_get_select_query_result as $user_classification_details_get_select_query_result_row) {
			
			$temp_row_array = array();
		    $temp_row_array["user_classification_detail_id"] = $user_classification_details_get_select_query_result_row["sm_site_member_classification_detail_id"];
		    $temp_row_array["user_type"] = $user_classification_details_get_select_query_result_row["sm_user_type"];
		    $temp_row_array["user_level"] = $user_classification_details_get_select_query_result_row["sm_user_level"];
		    $temp_row_array["user_role"] = $user_classification_details_get_select_query_result_row["sm_user_role"];
		    $temp_row_array["user_department"] = $user_classification_details_get_select_query_result_row["department"];
		    $temp_row_array["user_privilege_summary"] = $user_classification_details_get_select_query_result_row["user_privilege_summary"];
		    $constructed_array[] = $temp_row_array;
	    }//close of foreach ($user_classification_details_get_select_query_result as $user_classification_details_get_select_query_result_row) {
			
		return $constructed_array;
	}
	return $constructed_array;
	
}

function ea_get_quick_user_info($sm_memb_id_input) {
	
	global $dbcon;
	
	$constructed_array = array();
		
	$quick_user_info_get_sql = "SELECT * FROM `site_members` WHERE `sm_memb_id`=:sm_memb_id";	
	$quick_user_info_get_select_query = $dbcon->prepare($quick_user_info_get_sql);		
	$quick_user_info_get_select_query->bindValue(":sm_memb_id",$sm_memb_id_input);
	$quick_user_info_get_select_query->execute();
	if($quick_user_info_get_select_query->rowCount() > 0) {
		$quick_user_info_get_select_query_result = $quick_user_info_get_select_query->fetch();
		
		$constructed_array["user_id"] = $quick_user_info_get_select_query_result["sm_memb_id"];
		$constructed_array["user_email"] = $quick_user_info_get_select_query_result["sm_email"];
		$constructed_array["user_mobile"] = $quick_user_info_get_select_query_result["sm_mobile"];
		$constructed_array["user_phone"] = $quick_user_info_get_select_query_result["sm_phone"];
		$constructed_array["user_salutation"] = $quick_user_info_get_select_query_result["sm_salutation"];
		$constructed_array["user_firstname"] = $quick_user_info_get_select_query_result["sm_firstname"];
		$constructed_array["user_middlename"] = $quick_user_info_get_select_query_result["sm_middlename"];
		$constructed_array["user_lastname"] = $quick_user_info_get_select_query_result["sm_lastname"];
		$constructed_array["user_company_id"] = $quick_user_info_get_select_query_result["company_id"];
		$constructed_array["user_type"] = $quick_user_info_get_select_query_result["sm_user_type"];
		$constructed_array["user_status"] = $quick_user_info_get_select_query_result["sm_user_status"];
		
		$user_classification_details_result = ea_get_user_classification_details($sm_memb_id_input);
		
		$user_classification_name_association_ups_array = array();
		foreach($user_classification_details_result as $user_classification_details_result_row) {
			$sm_site_member_classification_detail_id = $user_classification_details_result_row["sm_site_member_classification_detail_id"];
			$sm_user_role = $user_classification_details_result_row["sm_user_role"];
			$user_privilege_summary = $user_classification_details_result_row["user_privilege_summary"];
			
			$user_classification_name_association_ups_array[] = $user_privilege_summary;
			//$user_classification_name_association_ups_array[] = $sm_site_member_classification_detail_id;
			
		}//close of foreach($user_classification_details_result as $user_classification_details_result_row) {
		
		$constructed_array["user_privileges_list"] = implode(",", $user_classification_name_association_ups_array);
		//$constructed_array["user_privileges_list"] = $user_classification_name_association_ups_array;
		
		return $constructed_array;
	}//close of if($quick_user_info_get_select_query->rowCount() > 0) {
	return $constructed_array;
	
}

function ea_get_quick_user_info_based_on_email_or_mobile($email_input, $mobile_input, $user_unique_identifier_setting_input, $ip_address_input) {
	
	global $dbcon;
	
	$constructed_array = array();
	
	$quick_user_info_get_select_query = array();
	
	//Identify the Unique Identifier Setting of User Account
	if ($user_unique_identifier_setting_input == "email-address") {
		//Do Select Query based on Email Address as User Identifier
		$quick_user_info_get_sql = "SELECT * FROM `site_members` WHERE `sm_email`=:sm_email";	
		$quick_user_info_get_select_query = $dbcon->prepare($quick_user_info_get_sql);		
		$quick_user_info_get_select_query->bindValue(":sm_email",$email_input);
		$quick_user_info_get_select_query->execute();
			
	} else if ($user_unique_identifier_setting_input == "mobile-number") {
		//Do Select Query based on Mobile Number as User Identifier
		$quick_user_info_get_sql = "SELECT * FROM `site_members` WHERE `sm_mobile`:sm_mobile";	
		$quick_user_info_get_select_query = $dbcon->prepare($quick_user_info_get_sql);		
		$quick_user_info_get_select_query->bindValue(":sm_mobile",$mobile_input);
		$quick_user_info_get_select_query->execute();
		
	}//close of else if of if ($user_unique_identifier_setting_input == "email-address") {
	
	if($quick_user_info_get_select_query->rowCount() > 0) {
		$quick_user_info_get_select_query_result = $quick_user_info_get_select_query->fetch();
		
		$sm_memb_id_input = $quick_user_info_get_select_query_result["sm_memb_id"];
		
		$constructed_array["user_id"] = $quick_user_info_get_select_query_result["sm_memb_id"];
		$constructed_array["user_email"] = $quick_user_info_get_select_query_result["sm_email"];
		$constructed_array["user_mobile"] = $quick_user_info_get_select_query_result["sm_mobile"];
		$constructed_array["user_phone"] = $quick_user_info_get_select_query_result["sm_phone"];
		$constructed_array["user_salutation"] = $quick_user_info_get_select_query_result["sm_salutation"];
		$constructed_array["user_firstname"] = $quick_user_info_get_select_query_result["sm_firstname"];
		$constructed_array["user_middlename"] = $quick_user_info_get_select_query_result["sm_middlename"];
		$constructed_array["user_lastname"] = $quick_user_info_get_select_query_result["sm_lastname"];
		$constructed_array["user_company_id"] = $quick_user_info_get_select_query_result["company_id"];
		$constructed_array["user_type"] = $quick_user_info_get_select_query_result["sm_user_type"];
		$constructed_array["user_status"] = $quick_user_info_get_select_query_result["sm_user_status"];
		if(!is_null($constructed_array["user_company_id"])) {
		$company_details_result = get_company_details_based_on_company_id($constructed_array["user_company_id"]);
		$constructed_array["company_support_email"] = $company_details_result["company_support_email"];
		} else {
			$constructed_array["company_support_email"] = null;
		}
		$user_classification_details_result = ea_get_user_classification_details($sm_memb_id_input);
		
		$user_classification_name_association_ups_array = array();
		foreach($user_classification_details_result as $user_classification_details_result_row) {
			$sm_site_member_classification_detail_id = $user_classification_details_result_row["sm_site_member_classification_detail_id"];
			$sm_user_role = $user_classification_details_result_row["sm_user_role"];
			$user_privilege_summary = $user_classification_details_result_row["user_privilege_summary"];
			
			$user_classification_name_association_ups_array[] = $user_privilege_summary;
			//$user_classification_name_association_ups_array[] = $sm_site_member_classification_detail_id;
			
		}//close of foreach($user_classification_details_result as $user_classification_details_result_row) {
		
		$constructed_array["user_privileges_list"] = implode(",", $user_classification_name_association_ups_array);
		//$constructed_array["user_privileges_list"] = $user_classification_name_association_ups_array;
		
		return $constructed_array;
	}//close of if($quick_user_info_get_select_query->rowCount() > 0) {
	return $constructed_array;
	
}

function ea_check_user_privileges_list_jwt_token_db_source($jwt_token_user_privileges_list_array_input, $sm_memb_id_input) {
	
	global $dbcon;
	
	//Do Count the number of items in User Privileges List, that is received from the JWT Token
	$jwt_token_user_privileges_list_array_input_count = count($jwt_token_user_privileges_list_array_input);
				
	//Do Get the User Classification Details, based on User ID			
	$user_classification_details_result = ea_get_user_classification_details($sm_memb_id_input);
	
	
	$user_classification_name_association_ups_array = array();
	
	foreach($user_classification_details_result as $user_classification_details_result_row) {
		$sm_site_member_classification_detail_id = $user_classification_details_result_row["sm_site_member_classification_detail_id"];
		$sm_user_role = $user_classification_details_result_row["sm_user_role"];
		$user_privilege_summary = $user_classification_details_result_row["user_privilege_summary"];
		
		//Do Collect User Privileges List, from the Database, based on User ID
		$user_classification_name_association_ups_array[] = $user_privilege_summary;
		//$user_classification_name_association_ups_array[] = $sm_site_member_classification_detail_id;
		
	}//close of foreach($user_classification_details_result as $user_classification_details_result_row) {
	
	//Do Count the number of items in User Privileges List, that is received from the Database Source
	$user_classification_name_association_ups_array_count = count($user_classification_name_association_ups_array);
	
	
	//Do Compare the User Privileges List, that is received from JWT Token and also from the Database Source
	if (($jwt_token_user_privileges_list_array_input_count == $user_classification_name_association_ups_array_count) && ($jwt_token_user_privileges_list_array_input == $user_classification_name_association_ups_array)) {
		return true;
	} else {
		return false;
	}//close of else of if (($jwt_token_user_privileges_list_array_input_count == $user_classification_name_association_ups_array_count) && ($jwt_token_user_privileges_list_array_input == $user_classification_name_association_ups_array)) {
	
	
}

function ea_get_page_api_user_classification_association_details($page_filename_input) {
	
	global $dbcon;
	
	$constructed_array = array();
		
	$valid_to_date_input = '%'.'present'.'%';
	$is_active_status_input = '1';
	$page_filename_search_input = '%'.$page_filename_input.'%';

	//Check in site_members db table
	$page_api_user_classification_association_details_get_sql = "SELECT * FROM `page_api_sm_site_user_classification_associations` passuca LEFT JOIN sm_site_member_classification_details ssmcd ON passuca.sm_site_member_classification_detail_id = ssmcd.sm_site_member_classification_detail_id WHERE passuca.is_active_status =:passuca_is_active_status AND ssmcd.is_active_status =:ssmcd_is_active_status AND passuca.valid_to_date LIKE :passuca_valid_to_date AND passuca.page_filename  LIKE :passuca_page_filename ORDER BY passuca.page_api_sm_site_user_classification_association_id ASC";
	
	$page_api_user_classification_association_details_get_select_query = $dbcon->prepare($page_api_user_classification_association_details_get_sql);
	$page_api_user_classification_association_details_get_select_query->bindValue(":passuca_is_active_status",$is_active_status_input);
	$page_api_user_classification_association_details_get_select_query->bindValue(":ssmcd_is_active_status",$is_active_status_input);
	$page_api_user_classification_association_details_get_select_query->bindValue(":passuca_valid_to_date",$valid_to_date_input);
	$page_api_user_classification_association_details_get_select_query->bindValue(":passuca_page_filename",$page_filename_search_input);
	$page_api_user_classification_association_details_get_select_query->execute();
	
	if($page_api_user_classification_association_details_get_select_query->rowCount() > 0) {
	   $page_api_user_classification_association_details_get_select_query_result = $page_api_user_classification_association_details_get_select_query->fetchAll();
	   //print_r($page_api_user_classification_association_details_get_select_query_result);
	   return $page_api_user_classification_association_details_get_select_query_result;
	}
	return $constructed_array;
	
}

function ea_get_page_api_user_classification_association_details_based_on_upl_input($page_filename_input, $user_privileges_list_input) {
	
	global $dbcon;
	
	$constructed_array = array();
		
	$valid_to_date_input = '%'.'present'.'%';
	$is_active_status_input = '1';
	$page_filename_search_input = '%'.$page_filename_input.'%';
	
	//echo "user_privileges_list_input: " . $user_privileges_list_input;
	$user_privileges_list_input_exploded = explode(",", $user_privileges_list_input);
	$user_privileges_list_input_exploded_imploded = implode(",", $user_privileges_list_input_exploded);
	
	

	//Check in site_members db table
	$page_api_user_classification_association_details_get_sql = "SELECT * FROM `page_api_sm_site_user_classification_associations` passuca LEFT JOIN sm_site_member_classification_details ssmcd ON passuca.sm_site_member_classification_detail_id = ssmcd.sm_site_member_classification_detail_id WHERE passuca.is_active_status =:passuca_is_active_status AND ssmcd.is_active_status =:ssmcd_is_active_status AND passuca.valid_to_date LIKE :passuca_valid_to_date AND passuca.page_filename  LIKE :passuca_page_filename AND ssmcd.user_privilege_summary IN (:jwt_user_privilege_list) ORDER BY passuca.page_api_sm_site_user_classification_association_id ASC";
	
	$page_api_user_classification_association_details_get_select_query = $dbcon->prepare($page_api_user_classification_association_details_get_sql);
	$page_api_user_classification_association_details_get_select_query->bindValue(":passuca_is_active_status",$is_active_status_input);
	$page_api_user_classification_association_details_get_select_query->bindValue(":ssmcd_is_active_status",$is_active_status_input);
	$page_api_user_classification_association_details_get_select_query->bindValue(":passuca_valid_to_date",$valid_to_date_input);
	$page_api_user_classification_association_details_get_select_query->bindValue(":passuca_page_filename",$page_filename_search_input);
	$page_api_user_classification_association_details_get_select_query->bindValue(":jwt_user_privilege_list",$user_privileges_list_input_exploded_imploded);
	$page_api_user_classification_association_details_get_select_query->execute();
	
	if($page_api_user_classification_association_details_get_select_query->rowCount() > 0) {
	   $page_api_user_classification_association_details_get_select_query_result = $page_api_user_classification_association_details_get_select_query->fetchAll();
	   //print_r($page_api_user_classification_association_details_get_select_query_result);
	   return $page_api_user_classification_association_details_get_select_query_result;
	}
	return $constructed_array;
	
}


function ea_check_user_privileges_for_specific_page_api_db_source($jwt_token_user_privileges_list_array_input, $page_filename_input) {
	
	global $dbcon;
	
	//Do Get the User Classification Details, based on User ID			
	$page_api_user_classification_association_details_result = ea_get_page_api_user_classification_association_details($page_filename_input);
	
	
	$page_api_user_classification_association_array = array();
	
	foreach($page_api_user_classification_association_details_result as $page_api_user_classification_association_details_result_row) {
		$page_api_sm_site_user_classification_association_id = $page_api_user_classification_association_details_result_row["page_api_sm_site_user_classification_association_id"];
		$sm_site_member_classification_detail_id = $page_api_user_classification_association_details_result_row["sm_site_member_classification_detail_id"];
		$user_privilege_summary = $page_api_user_classification_association_details_result_row["user_privilege_summary"];
		
		//Do Collect User Privileges List, from the Database, based on Page Filename
		$page_api_user_classification_association_array[] = $user_privilege_summary;
		
	}//close of foreach($page_api_user_classification_association_details_result as $page_api_user_classification_association_details_result_row) {
	
	//Do Check, if at least one entry of the JWT Auth Token based User Privileges, matches with User Privileges from the Database Source
	if (count(array_intersect($jwt_token_user_privileges_list_array_input, $page_api_user_classification_association_array)) > 0) {
		return true;
	} else {
		return false;
	}//close of else of if (count(array_intersect($jwt_token_user_privileges_list_array_input, $page_api_user_classification_association_array)) > 0) {
	
	
}

/* commented on 18-02-2019 08:37, INCOMPLETE function function ea_get_user_privileges_for_specific_page_api_db_source($page_filename_input, $sm_memb_id_input) {
	
	global $dbcon;
	
	$constructed_array = array();
	
	$user_classification_details_result = ea_get_user_classification_details($sm_memb_id_input);
		
	$user_classification_name_association_ups_array = array();
	foreach($user_classification_details_result as $user_classification_details_result_row) {
		$sm_site_member_classification_detail_id = $user_classification_details_result_row["sm_site_member_classification_detail_id"];
		$sm_user_role = $user_classification_details_result_row["sm_user_role"];
		$user_privilege_summary = $user_classification_details_result_row["user_privilege_summary"];
		
		$user_classification_name_association_ups_array[] = $user_privilege_summary;
		//$user_classification_name_association_ups_array[] = $sm_site_member_classification_detail_id;
		
	}//close of foreach($user_classification_details_result as $user_classification_details_result_row) {
	
	$user_privileges_list = implode(",", $user_classification_name_association_ups_array);
	
	$page_filename_search_input = '%'.$page_filename_input.'%';
	
	//echo "user_privileges_list_input: " . $user_privileges_list_input;
	$user_privileges_list_input_exploded = explode(",", $user_privileges_list_input);
	$user_privileges_list_input_exploded_imploded = implode(",", $user_privileges_list_input_exploded);
	
	

	//Check in site_members db table
	$page_api_user_classification_association_details_get_sql = "SELECT * FROM `page_api_sm_site_user_classification_associations` passuca LEFT JOIN sm_site_member_classification_details ssmcd ON passuca.sm_site_member_classification_detail_id = ssmcd.sm_site_member_classification_detail_id WHERE passuca.is_active_status =:passuca_is_active_status AND ssmcd.is_active_status =:ssmcd_is_active_status AND passuca.valid_to_date LIKE :passuca_valid_to_date AND passuca.page_filename  LIKE :passuca_page_filename AND ssmcd.user_privilege_summary IN (:jwt_user_privilege_list) ORDER BY passuca.page_api_sm_site_user_classification_association_id ASC";
	
	$page_api_user_classification_association_details_get_select_query = $dbcon->prepare($page_api_user_classification_association_details_get_sql);
	$page_api_user_classification_association_details_get_select_query->bindValue(":passuca_is_active_status",$is_active_status_input);
	$page_api_user_classification_association_details_get_select_query->bindValue(":ssmcd_is_active_status",$is_active_status_input);
	$page_api_user_classification_association_details_get_select_query->bindValue(":passuca_valid_to_date",$valid_to_date_input);
	$page_api_user_classification_association_details_get_select_query->bindValue(":passuca_page_filename",$page_filename_search_input);
	$page_api_user_classification_association_details_get_select_query->bindValue(":jwt_user_privilege_list",$user_privileges_list_input_exploded_imploded);
	$page_api_user_classification_association_details_get_select_query->execute();
	
	if($page_api_user_classification_association_details_get_select_query->rowCount() > 0) {
	   $page_api_user_classification_association_details_get_select_query_result = $page_api_user_classification_association_details_get_select_query->fetchAll();
	   //print_r($page_api_user_classification_association_details_get_select_query_result);
	   return $page_api_user_classification_association_details_get_select_query_result;
	}
	return $constructed_array;
	
	
}*/

/*function ea_check_action_specific_user_privileges_different_user_levels_reset_password_feature($jwt_token_user_privileges_list_array_input, $action_subject_user_privileges_list_array_input, $jwt_token_user_type_input, $action_subject_user_type_input, $jwt_token_user_company_id_input, $action_subject_user_company_id_input, $action_subject_user_self_activity_req_status_input, $page_filename_input) {
function ea_check_action_specific_user_privileges_different_user_levels_reset_password_feature($jwt_token_user_type_input, $action_subject_user_type_input, $action_subject_user_id_input, $jwt_token_user_company_id_input, $action_subject_user_company_id_input, $action_subject_user_self_activity_req_status_input, $page_filename_input) {*/
function ea_check_action_specific_user_privileges_on_different_user_levels($jwt_token_user_type_input, $jwt_token_user_company_id_input, $jwt_token_user_id_input, $action_subject_user_id_input, $jwt_user_privilege_list_input, $action_subject_user_company_id_input, $page_filename_input) {
	
	global $dbcon;
	
	$next_step = "";
	
	//Do Get the User Classification Details, based on User ID			
	//$page_api_user_classification_association_details_result = ea_get_page_api_user_classification_association_details($page_filename_input);
	$page_api_user_classification_association_details_result = ea_get_page_api_user_classification_association_details_based_on_upl_input($page_filename_input, $jwt_user_privilege_list_input);
	
	
	$platform_specific_subject_classifications_array = array();
	$company_specific_subject_classifications_array = array();
	$self_action_allowance_status_array = array();
	
	foreach($page_api_user_classification_association_details_result as $page_api_user_classification_association_details_result_row) {
		$page_api_sm_site_user_classification_association_id = $page_api_user_classification_association_details_result_row["page_api_sm_site_user_classification_association_id"];
		$sm_site_member_classification_detail_id = $page_api_user_classification_association_details_result_row["sm_site_member_classification_detail_id"];
		$platform_specific_subject_classifications = $page_api_user_classification_association_details_result_row["platform_specific_subject_classifications"];
		$company_specific_subject_classifications = $page_api_user_classification_association_details_result_row["company_specific_subject_classifications"];
		$self_action_allowance_status = $page_api_user_classification_association_details_result_row["self_action_allowance_status"];
		
		$self_action_allowance_status_array[] = $self_action_allowance_status;
				
		//Explode Platform specific subject user classifications
		$platform_specific_subject_classifications_exploded = explode(",", $platform_specific_subject_classifications);
		
		//$platform_specific_subject_classifications_array[] = $platform_specific_subject_classifications_exploded;
		
		//Explode Company specific subject user classifications
		$company_specific_subject_classifications_exploded = explode(",", $company_specific_subject_classifications);
		
		//$company_specific_subject_classifications_array[] = $company_specific_subject_classifications_exploded;
		//array_push($company_specific_subject_classifications_array,$company_specific_subject_classifications_exploded);
		
		/* 
		//Do Collect User Privileges List, from the Database, based on Page Filename
		$platform_specific_subject_classifications_array[] = $platform_specific_subject_classifications;
		 */
	}//close of foreach($page_api_user_classification_association_details_result as $page_api_user_classification_association_details_result_row) {
	
	//$platform_specific_subject_classifications_array = array_unique($platform_specific_subject_classifications_array);
	//echo "<pre>";
	//print_r($platform_specific_subject_classifications_array);
	//$company_specific_subject_classifications_array = array_unique($company_specific_subject_classifications_array);
	//$self_action_allowance_status_array = array_unique($self_action_allowance_status_array);
	
	$action_subject_user_classification_details_result = ea_get_user_classification_details($action_subject_user_id_input);
	
	$user_classification_name_association_cdi_array = array();
	foreach($action_subject_user_classification_details_result as $action_subject_user_classification_details_result_row) {
		$sm_site_member_classification_detail_id = $action_subject_user_classification_details_result_row["sm_site_member_classification_detail_id"];
		$sm_user_role = $action_subject_user_classification_details_result_row["sm_user_role"];
		$user_privilege_summary = $action_subject_user_classification_details_result_row["user_privilege_summary"];
		
		$user_classification_name_association_cdi_array[] = $sm_site_member_classification_detail_id;
		
	}//close of foreach($action_subject_user_classification_details_result as $action_subject_user_classification_details_result_row) {
	
	//print_r($user_classification_name_association_cdi_array);
	
	//print_r($company_specific_subject_classifications_exploded);
	//print_r($user_classification_name_association_cdi_array);
	//echo "action_subject_user_company_id_input: " . $action_subject_user_company_id_input . "<br>";
	//var_dump($action_subject_user_company_id_input);
	//echo "jwt_token_user_company_id_input: " . $jwt_token_user_company_id_input . "<br>";
	//echo "<pre> self action allowance status: ";
	//print_r($self_action_allowance_status_array);
	
	if ($jwt_token_user_type_input == "admin") {
		
		//Do Check, if at least one entry of the JWT Auth Token based User Privileges, matches with User Privileges from the Database Source
		if (((count(array_intersect($platform_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) > 0) || (count(array_intersect($company_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) > 0)) && ($jwt_token_user_company_id_input == "")) {		
			
			if ((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) {
				
				//echo "matched self check condition (platform)";
				//User of the Admin User Group, who is attempting to do this Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
				$next_step = "PROCEED-TO-NEXT-STEP";
				
			} else if ((in_array("0", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) {
				
				//echo "does not match self check condition";
				$next_step = "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED";
				
			} else if ((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input != $action_subject_user_id_input)) {
				
				//echo "matched different user condition (platform)";
				//User of the Admin User Group, who is attempting to do this Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
				$next_step = "PROCEED-TO-NEXT-STEP";
				
			} else if ((in_array("0", $self_action_allowance_status_array)) && ($jwt_token_user_id_input != $action_subject_user_id_input)) {
				
				//echo "matched different user condition (platform)";
				//User of the Admin User Group, who is attempting to do this Activity, is either Super Administrator or Site Administrator, in the scope of the Platform
				$next_step = "PROCEED-TO-NEXT-STEP";
				
			}//close of else if of if (((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) || ((!in_array("1", $self_action_allowance_status_array)))) {
				
			
		} else if (((count(array_intersect($platform_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) == 0) && (count(array_intersect($company_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) == 0)) && (is_null($action_subject_user_company_id_input))) {
			
			//User of the Admin User Group, who is attempting to do this Activity, belongs to a Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...) and the Company ID of the Action Subject is EMPTY. This means action subject is among the Admin Users in Platform scope.
				
			$next_step = "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER";
			
		} else if (((count(array_intersect($platform_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) == 0) && (count(array_intersect($company_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) > 0)) && ($jwt_token_user_company_id_input != "") && ($action_subject_user_company_id_input == $jwt_token_user_company_id_input)) {
			
			if ((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) {
				
				//echo "matched self check condition (company)";
				//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
				
				$next_step = "PROCEED-TO-NEXT-STEP";
				
			} else if ((in_array("0", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) {
				
				//echo "does not match self check condition";
				$next_step = "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED";
				
			} else if ((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input != $action_subject_user_id_input)) {
				
				//echo "matched different user condition (company)";
				//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
				
				$next_step = "PROCEED-TO-NEXT-STEP";
				
			} else if ((in_array("0", $self_action_allowance_status_array)) && ($jwt_token_user_id_input != $action_subject_user_id_input)) {
				
				//echo "matched different user condition (company)";
				//User of the Admin User Group, who is attempting to do this Candidate Add Activity, belongs to the Same Company. The scope includes Company Administrator, Members of Company specific Admin User Teams (legal / immigration, internal admin etc...).
				
				$next_step = "PROCEED-TO-NEXT-STEP";
				
			}//close of else if of if (((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) || ((!in_array("1", $self_action_allowance_status_array)))) {
			
			
		} else {
			
			//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
			
			$next_step = "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID";
				
			
		}//close of else of if ((in_array("Super Administrator", $jwt_token_user_privileges_list_array_input)) || (in_array("Site Administrator", $jwt_token_user_privileges_list_array_input))) {
		
		
		
	} else if ($jwt_token_user_type_input == "member") {
		
		//Do Check, if at least one entry of the JWT Auth Token based User Privileges, matches with User Privileges from the Database Source
		if (((count(array_intersect($platform_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) == 0) && ((count(array_intersect($company_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) > 0) || (count(array_intersect($company_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) == 0))) && (is_null($action_subject_user_company_id_input))) {
			
			//User of the Admin User Group, who is attempting to do this Activity, belongs to a Company. The scope includes Company Administrator, Members of Company specific Admin User Teams and the Company ID of the Action Subject is EMPTY. This means action subject is among the Admin Users in Platform scope.
				
			$next_step = "DONOT-PROCEED-TO-NEXT-STEP-AS-SUBJECT-IS-PLATFORM-SCOPE-USER";
			
		} else if (((count(array_intersect($platform_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) == 0) && (count(array_intersect($company_specific_subject_classifications_exploded, $user_classification_name_association_cdi_array)) > 0)) && ($jwt_token_user_company_id_input != "") && ($action_subject_user_company_id_input == $jwt_token_user_company_id_input)) {
			
			if ((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) {
				
				//echo "matched self check condition (company)";
				//User of the Member User Type, who is attempting to do this Activity, belongs to the Same Company and is performing on Own Account. This applies to Candidates.
				
				$next_step = "PROCEED-TO-NEXT-STEP";
				
			} else if ((in_array("0", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) {
				
				//echo "does not match self check condition";
				$next_step = "DONOT-PROCEED-TO-NEXT-STEP-AS-SELF-ACTION-IS-NOT-ALLOWED";
				
			} else if ((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input != $action_subject_user_id_input)) {
				
				//echo "matched different user condition (company)";
				//User of the Member User Type, who is attempting to do this Activity, belongs to the Same Company and is performing on a Different User's Account. This applies to Candidates.
				
				$next_step = "DONOT-PROCEED-TO-NEXT-STEP-AS-ACTION-SUBJECT-IS-A-DIFFERENT-USER";
				
			} else if ((in_array("0", $self_action_allowance_status_array)) && ($jwt_token_user_id_input != $action_subject_user_id_input)) {
				
				//echo "matched different user condition (company)";
				//User of the Member User Type, who is attempting to do this Activity, belongs to the Same Company and is performing on a Different User's Account. This applies to Candidates.
				
				$next_step = "DONOT-PROCEED-TO-NEXT-STEP-AS-ACTION-SUBJECT-IS-A-DIFFERENT-USER";
				
			}//close of else if of if (((in_array("1", $self_action_allowance_status_array)) && ($jwt_token_user_id_input == $action_subject_user_id_input)) || ((!in_array("1", $self_action_allowance_status_array)))) {
			
			
		} else {
			
			//User of the Admin User Group, who is attempting to do this Candidate Add Activity, is neither Super Administrator or Site Administrator of the Platform or a representative of the Company
			
			$next_step = "DONOT-PROCEED-TO-NEXT-STEP-DIFFERENT-COMPANY-USER-ID";
				
			
		}//close of else of if ((in_array("Super Administrator", $jwt_token_user_privileges_list_array_input)) || (in_array("Site Administrator", $jwt_token_user_privileges_list_array_input))) {
		
		
		
	} else {
		
		$next_step = "DONOT-PROCEED-TO-NEXT-STEP-INSUFFICIENT-PERMISSIONS";
		
	}//close of else of if ($jwt_token_user_type_input == "admin") {
	
	return $next_step;
	
}

function ea_generate_user_password_setup_modification_request_ref_code($action_taker_sm_memb_id_input, $action_subject_sm_memb_id_input, $request_destination_input, $user_email_input, $event_datetime_input, $current_epoch_input, $expiry_datetime_epoch_input,$request_purpose) {
	
	global $dbcon;
	
	$constructed_array = array();
	
	$randomly_generated_bytes = random_bytes(20);
	//var_dump(bin2hex($randomly_generated_bytes));
	$randomly_generated_bytes_hex = bin2hex($randomly_generated_bytes);
	
	$password_request_code = hash("sha256", $randomly_generated_bytes_hex . $action_taker_sm_memb_id_input . $action_subject_sm_memb_id_input . $user_email_input . $current_epoch_input);
	
	//SELECT * FROM `password_setup_modification_requests` WHERE `unique_password_setup_request_ref_code` =:unique_password_setup_request_ref_code

	//Check in password_setup_modification_requests db table
	$password_setup_modification_request_details_get_sql = "SELECT * FROM `password_setup_modification_requests` WHERE `unique_password_setup_request_ref_code` =:unique_password_setup_request_ref_code";
	
	$password_setup_modification_request_details_get_select_query = $dbcon->prepare($password_setup_modification_request_details_get_sql);
	$password_setup_modification_request_details_get_select_query->bindValue(":unique_password_setup_request_ref_code",$password_request_code);
	$password_setup_modification_request_details_get_select_query->execute();
	
	if($password_setup_modification_request_details_get_select_query->rowCount() == 0) {
	
		//Do Insert Query
		$password_setup_request_insert_sql = "INSERT INTO `password_setup_modification_requests`(`sm_memb_id`, `request_purpose`, `request_destination`, `recipient_email_address`, `unique_password_setup_request_ref_code`, `added_datetime`, `added_datetime_epoch`, `added_by_sm_memb_id`, `request_expiry_datetime_epoch`, `email_sending_status`, `sent_datetime`, `sent_datetime_epoch`, `email_service_provider`, `email_service_provider_mail_ref_id`, `email_service_provider_mail_ref_message`, `is_active_status`) VALUES (:sm_memb_id,:request_purpose,:request_destination,:recipient_email_address,:unique_password_setup_request_ref_code,:added_datetime,:added_datetime_epoch,:added_by_sm_memb_id,:request_expiry_datetime_epoch,:email_sending_status,:sent_datetime,:sent_datetime_epoch,:email_service_provider,:email_service_provider_mail_ref_id,:email_service_provider_mail_ref_message,:is_active_status)";
		$password_setup_request_insert_query = $dbcon->prepare($password_setup_request_insert_sql);
		$password_setup_request_insert_query->bindValue(":sm_memb_id",$action_subject_sm_memb_id_input);
		$password_setup_request_insert_query->bindValue(":request_destination",$request_destination_input);
		$password_setup_request_insert_query->bindValue(":recipient_email_address",$user_email_input);
		$password_setup_request_insert_query->bindValue(":unique_password_setup_request_ref_code",$password_request_code);
		$password_setup_request_insert_query->bindValue(":added_datetime",$event_datetime_input);
		$password_setup_request_insert_query->bindValue(":added_datetime_epoch",$current_epoch_input);
		$password_setup_request_insert_query->bindValue(":added_by_sm_memb_id",$action_taker_sm_memb_id_input);
		$password_setup_request_insert_query->bindValue(":request_expiry_datetime_epoch",$expiry_datetime_epoch_input);
		$password_setup_request_insert_query->bindValue(":email_sending_status","0");
		$password_setup_request_insert_query->bindValue(":request_purpose",$request_purpose);
		$password_setup_request_insert_query->bindValue(":sent_datetime",null);
		$password_setup_request_insert_query->bindValue(":sent_datetime_epoch",null);
		$password_setup_request_insert_query->bindValue(":email_service_provider",null);
		$password_setup_request_insert_query->bindValue(":email_service_provider_mail_ref_id",null);
		$password_setup_request_insert_query->bindValue(":email_service_provider_mail_ref_message",null);
		
		$password_setup_request_insert_query->bindValue(":is_active_status","1");
		
		if ($password_setup_request_insert_query->execute()) {
			
			$last_inserted_id = $dbcon->lastInsertId();			
			//$eventLog->log("record inserted successfully");
			
			$constructed_array["last_inserted_id"] = $last_inserted_id;
			$constructed_array["password_request_code"] = $password_request_code;
			
			return $constructed_array;
						
		} else {
			//$eventLog->log("Error occurred during process. Please try again");						
				return $constructed_array;						
		}
	}//close of else of if($password_setup_modification_request_details_get_select_query->rowCount() > 0) {
		
	
	
}

function ea_get_user_account_password_request_details_based_on_password_request_code($password_request_code_input) {
	
	global $dbcon;
	
	$constructed_array = array();
	
	$user_account_password_request_details_get_sql = "SELECT * FROM `password_setup_modification_requests` WHERE `unique_password_setup_request_ref_code` =:unique_password_setup_request_ref_code";	
	$user_account_password_request_details_get_select_query = $dbcon->prepare($user_account_password_request_details_get_sql);		
	$user_account_password_request_details_get_select_query->bindValue(":unique_password_setup_request_ref_code",$password_request_code_input);
	$user_account_password_request_details_get_select_query->execute();
	if($user_account_password_request_details_get_select_query->rowCount() > 0) {
		$user_account_password_request_details_get_select_query_result = $user_account_password_request_details_get_select_query->fetch();
		
		return $user_account_password_request_details_get_select_query_result;
		
	}//close of if($user_account_password_request_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
}

function ea_update_user_account_password_request_status_based_on_is_active_status($event_datetime_input, $current_epoch_input, $action_taker_sm_memb_id_input, $is_active_status_input, $password_setup_modification_request_id_input) {
	
	global $dbcon;
	
	$user_account_password_request_status_update_sql = "UPDATE `password_setup_modification_requests` SET `last_updated_datetime`=:last_updated_datetime,`last_updated_datetime_epoch`=:last_updated_datetime_epoch,`last_updated_by_sm_memb_id`=:last_updated_by_sm_memb_id,`is_active_status`=:is_active_status WHERE `password_setup_modification_request_id` =:password_setup_modification_request_id";	
	$user_account_password_request_status_update_query = $dbcon->prepare($user_account_password_request_status_update_sql);		
	$user_account_password_request_status_update_query->bindValue(":last_updated_datetime",$event_datetime_input);
	$user_account_password_request_status_update_query->bindValue(":last_updated_datetime_epoch",$current_epoch_input);
	$user_account_password_request_status_update_query->bindValue(":last_updated_by_sm_memb_id",$action_taker_sm_memb_id_input);
	$user_account_password_request_status_update_query->bindValue(":is_active_status",$is_active_status_input);
	$user_account_password_request_status_update_query->bindValue(":password_setup_modification_request_id",$password_setup_modification_request_id_input);
	
	if ($user_account_password_request_status_update_query->execute()) {
		return true;
		
	}//close of if ($user_account_password_request_status_update_query->execute()) {
	
	return false;
}

function ea_update_user_account_password_based_on_sm_memb_id($password_hash_input, $event_datetime_input, $current_epoch_input, $action_subject_sm_memb_id_input) {
	
	global $dbcon;
	
	$user_account_password_request_status_update_sql = "UPDATE `site_members` SET `sm_password`=:sm_password,`sm_user_status`=:sm_user_status,`last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch WHERE `sm_memb_id`=:sm_memb_id";	
	$user_account_password_request_status_update_query = $dbcon->prepare($user_account_password_request_status_update_sql);		
	$user_account_password_request_status_update_query->bindValue(":sm_password",$password_hash_input);
	$user_account_password_request_status_update_query->bindValue(":sm_user_status","1");
	$user_account_password_request_status_update_query->bindValue(":last_updated_date_time",$event_datetime_input);
	$user_account_password_request_status_update_query->bindValue(":last_updated_date_time_epoch",$current_epoch_input);
	$user_account_password_request_status_update_query->bindValue(":sm_memb_id",$action_subject_sm_memb_id_input);
	
	if ($user_account_password_request_status_update_query->execute()) {
		return true;
		
	}//close of if ($user_account_password_request_status_update_query->execute()) {
	
	return false;
}

/*function ea_get_user_account_password_request_details_based_on_password_request_code($password_request_code_input, $event_datetime_input, $current_epoch_input) {
	
	global $dbcon;
	
	$constructed_array = array();
	
	$user_account_password_request_details_get_sql = "SELECT * FROM `password_setup_modification_requests` WHERE `unique_password_setup_request_ref_code` =:unique_password_setup_request_ref_code";	
	$user_account_password_request_details_get_select_query = $dbcon->prepare($user_account_password_request_details_get_sql);		
	$user_account_password_request_details_get_select_query->bindValue(":unique_password_setup_request_ref_code",$password_request_code_input);
	$user_account_password_request_details_get_select_query->execute();
	if($user_account_password_request_details_get_select_query->rowCount() > 0) {
		$user_account_password_request_details_get_select_query_result = $user_account_password_request_details_get_select_query->fetch();
		
		$sm_password = $user_account_password_request_details_get_select_query_result["sm_password"];
		$sm_user_status = $user_account_password_request_details_get_select_query_result["sm_user_status"];
		
		$constructed_array["user_status"] = null;
		$constructed_array["current_password_verification_status"] = null;
	
		if ($sm_user_status == "1") {
			$constructed_array["user_status"] = "1";
			//verification of password in login process based on BCRYPT Hashing Algorithm using Password hashing API, that is supported from PHP v5.5 and beyond
			if (password_verify($current_password_input, $sm_password)) {
				// Current Password is Successfully Verified
				$constructed_array["current_password_verification_status"] = "1"; 
				
			} else {
				$constructed_array["current_password_verification_status"] = "0"; // In-correct Current Password
			}//close of else of if (password_verify($current_password_input, $sm_password)) {
				
		} else if ($sm_user_status == "2") {
			$constructed_array["user_status"] = "2";
		} else if ($sm_user_status == "3") {
			$constructed_array["user_status"] = "3";
		} else {
			$constructed_array["user_status"] = "0";
		}//close of else of if ($sm_user_status == "1") {
		return $constructed_array;
	}//close of if($user_account_password_request_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
}*/
?>