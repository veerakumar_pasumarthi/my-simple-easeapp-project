<?php
//http://phpsnips.com/539/Extract-URL's-from-HTML#.WBCl0SQ731Y
//modified by raghu
// $html        = the html on the page
// $current_url = the full url that the html came from (only needed for $repath)
// $repath      = converts ../ and / and // urls to full valid urls
function pageLinks($html, $current_url = "", $repath = false){
    preg_match_all("/\<a.+?href=(\"|')(?!javascript:|#)(.+?)(\"|')/i", $html, $matches);
	//print_r($matches);
    $links = array();
    if(isset($matches[2])){
        $links = $matches[2];
	}
    if($repath && count($links) > 0 && strlen($current_url) > 0){
        $pathi      = pathinfo($current_url);
		//echo "pathi: " . print_r($pathi) . "<br>";
        $dir        = $pathi["dirname"];
		//echo "dir: " . $dir . "<br>";
        $base       = parse_url($current_url);
		//echo "base: " . print_r($base) . "<br>";
        $split_path = explode("/", $dir);
		//echo "split_path: " . print_r($split_path) . "<br>";
        $url        = "";
        foreach($links as $k => $link){
			//echo $link;
            if(preg_match("/^\.\./", $link)){
                $total = substr_count($link, "../");
                for($i = 0; $i < $total; $i++){
                    array_pop($split_path);
                }
				
                /*commented by raghu (to prepend the current url correctly for urls with symlinks
				$url = implode("/", $split_path) . "/" . str_replace("../", "", $link);*/
				$url = $current_url . "/" . str_replace("../", "", $link); //edited by raghu
            }elseif(preg_match("/^\/\//", $link)){
                $url = $base["scheme"] . ":" . $link;
			}elseif(preg_match("/^\/|^.\//", $link)){
                $url = $base["scheme"] . "://" . $base["host"] . $link;
            }elseif(preg_match("/^[a-zA-Z0-9]/", $link)){
                if(preg_match("/^http/", $link)){
                    $url = $link;
                }else if(preg_match("/^mailto:/", $link)){
                    //this condition is added by raghu
					$url = "";
                }else{
                    /*commented by raghu (to correct the protocol and add the missing hostname
					$url       = $dir . "/" . $link;*/
					$url       = $current_url . "/" . $link; //edited by raghu
					//$url       = $current_url . $link; //edited by raghu
                }
            }
            $links[$k] = $url;
        }
    }
    return $links;
}

//when the search engine truncates the display url then we use the bellow one for the actual url
  function get_actual_url_after_redirection($url){
	  global $hash_algorithm;
		$location = "";
		$header_values_array = array();
		$url_array = array();
		$sanitized_url_input = isset($url) ? filter_var($url, FILTER_SANITIZE_URL) : '';
		// Validate url
		if (!filter_var($sanitized_url_input, FILTER_VALIDATE_URL) === false) {
			//echo("$sanitized_url_input is a valid URL");
			$url_array["initial_url"] = $sanitized_url_input;
		} else {
			//echo("$sanitized_url_input is not a valid URL");
			$url_array["initial_url"] = "";
		}
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_USERAGENT,"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13)
				  Gecko/20080311 Firefox/2.0.0.13");
		//curl_setopt($ch, CURLOPT_USERAGENT,$crawler_user_agent);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		$headers_input = curl_exec($ch);
		// get the content type (from headers)
		//http://stackoverflow.com/a/2610734
        $content_type_extracted = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
		$content_type_exploded = explode(";", $content_type_extracted);
		$url_array["content_type"] = $content_type_exploded[0];
		//echo "content_type: " . $url_array["content_type"] . "\n";
		$data=explode("\n",$headers_input);
		//print_r($data);
        //$header_values_array['status']=$data[0];  
        $status_message_input = isset($data[0]) ? filter_var($data[0], FILTER_SANITIZE_STRING) : '';
		$header_values_array['status']=$status_message_input;	
        //echo "header_values_array status: " . $header_values_array['status'] . "<br>";
		if ($header_values_array['status'] != "") {
			$status_summ_exploded = explode(" ", $header_values_array['status']);
		    $status = $status_summ_exploded[1];
		} else {
			$status = "";
		}
		$url_array["status"] = $status;
		//echo "status: " . $status . "<br>";
        array_shift($data);
		foreach($data as $part){
        $middle=explode(": ",$part,2);
        /* http://stackoverflow.com/questions/17456325/php-notice-undefined-offset-1-with-array-when-reading-data
        http://stackoverflow.com/a/17456614
        */
        $header_values_array[trim($middle[0])] = isset($middle[1]) ? trim($middle[1]) : null;
        }
		if(isset($header_values_array['Location']) && $header_values_array['Location'] != ""){
			//extract value of Location from headers
			//$url_array["redirected_url"] = $header_values_array['Location'];
			$sanitized_redirect_url_input = isset($header_values_array['Location']) ? filter_var($header_values_array['Location'], FILTER_SANITIZE_URL) : '';
			// Validate url
			if (!filter_var($sanitized_redirect_url_input, FILTER_VALIDATE_URL) === false) {
				//echo("$sanitized_redirect_url_input is a valid URL");
				$url_array["redirected_url"] = $sanitized_redirect_url_input;
			} else {
				//echo("$sanitized_redirect_url_input is not a valid URL");
				$url_array["redirected_url"] = "";
			}
		} else {
			//return the inputed url itself when there is no redirect and http status code is 200
			if ($status != "") {
				//status code exists
				$url_array["redirected_url"] = $sanitized_url_input;
			} else {
				//status code do not exist, due to some error 
				$url_array["redirected_url"] = "";
			}
			
		}
		
		//Create Whirlpool Hash of the input url
		if (!filter_var($sanitized_url_input, FILTER_VALIDATE_URL) === false) {
			//echo("$sanitized_url_input is a valid URL");
			$url_array["initial_url_hash"] = hash($hash_algorithm, $url_array["initial_url"]); //$hash_algorithm from //app/core/main-config.php
		} else {
			//echo("$sanitized_url_input is not a valid URL");
			$url_array["initial_url_hash"] = "";
		}
		
		return $url_array;
  }
  
  function getHtmlPageContent($url, $crawler_user_agent, $ssl_requirement = false)
			  {
				$page_content_array = array();
				
				$sanitized_url_input = isset($url) ? filter_var($url, FILTER_SANITIZE_URL) : '';
				// Validate url
				if (!filter_var($sanitized_url_input, FILTER_VALIDATE_URL) === false) {
					//echo("$sanitized_url_input is a valid URL");
					$url_array["initial_url"] = $sanitized_url_input;
				} else {
					//echo("$sanitized_url_input is not a valid URL");
					$url_array["initial_url"] = "";
				}
				if(function_exists('curl_init')) {
				  $ch = curl_init($sanitized_url_input);
				  curl_setopt($ch,  CURLOPT_RETURNTRANSFER, TRUE);
				  curl_setopt($ch, CURLOPT_USERAGENT,"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13)
				  Gecko/20080311 Firefox/2.0.0.13");
				  //curl_setopt($ch, CURLOPT_USERAGENT,$crawler_user_agent);
				  //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				    if ($ssl_requirement === true) {
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
						
					} else {
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
						
					}
				  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
				  curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				  
				  $url_response = curl_exec($ch);
				  $url_content = $url_response;  
				  $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				  $redirectURL = curl_getinfo($ch,CURLINFO_EFFECTIVE_URL );
				  if (($httpCode == "301") || ($httpCode == "302")) {
					  $handle = curl_init($sanitized_url_input);
					  curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
					  curl_setopt($handle,  CURLOPT_AUTOREFERER, TRUE);
					  curl_setopt($handle, CURLOPT_FOLLOWLOCATION, true);
					  curl_setopt($handle, CURLOPT_MAXREDIRS, "6");
					  curl_setopt($handle, CURLOPT_USERAGENT,"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13)
					  Gecko/20080311 Firefox/2.0.0.13");
					  //curl_setopt($handle, CURLOPT_USERAGENT,$crawler_user_agent);
					  //curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
						if ($ssl_requirement === true) {
							curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 1);
							curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 1);
							
						} else {
							curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 0);
							curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 0);
							
						}
					  curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 0); 
					  curl_setopt($handle, CURLOPT_TIMEOUT, 60);
					  
					  $url_response = curl_exec($handle);
					  $url_content = $url_response;  
					  $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
					  $redirectURL = curl_getinfo($handle,CURLINFO_EFFECTIVE_URL );
					  
					  $page_content_array["url_content"] = $url_content;
					  /*$page_content_array["http_code"] = $httpCode;
					  $page_content_array["inputed_url"] = $sanitized_url_input;
					  $page_content_array["redirect_url"] = $redirectURL;*/
					  //echo "http_code: " . $httpCode . "<br>";
					 // echo "redirectURL: " . $redirectURL . "<br>";
					  curl_close($handle);
					  
				  } else {
					  $page_content_array["url_content"] = $url_content;
					  /*$page_content_array["http_code"] = $httpCode;
					  $page_content_array["inputed_url"] = $sanitized_url_input;
					  $page_content_array["redirect_url"] = $redirectURL;*/
					  //echo "http_code: " . $httpCode . "<br>";
					  //echo "redirectURL: " . $redirectURL . "<br>";
					  curl_close($ch);
					  
				  }
				  
				} else {
					//echo "curl has issue<br>";
				  $url_content = file_get_contents($url);
				  $page_content_array["url_content"] = $url_content;
				  /*$page_content_array["http_code"] = "";
				  $page_content_array["inputed_url"] = $sanitized_url_input;
				  $page_content_array["redirect_url"] = "";*/
				}//close of else of if(function_exists('curl_init')) {
				//return $url_content;
				return $page_content_array;
			  }
  //extract email from html
function get_email_ids_from_html($html_string){
//http://stackoverflow.com/questions/3901070/in-php-how-do-i-extract-multiple-e-mail-addresses-from-a-block-of-text-and-put
	//http://stackoverflow.com/a/24969553
	//This is modified to accomodate different kinds of domain tlds those that are recently available in the market
	//$text=file_get_contents($url);
	$email_ids = array();
	$text=$html_string;
	$res = preg_match_all(
	"/[a-z0-9]+[_a-z0-9\.-]*[a-z0-9]+@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,20})/i",
	$text,
	$matches
	);
	
	if ($res) {
		print_r($matches);
		foreach(array_unique($matches[0]) as $email) {
			
			// Remove all illegal characters from email
			$email = filter_var($email, FILTER_SANITIZE_EMAIL);

			// Validate e-mail
			if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
				//echo("$email is a valid email address<br>");
				//echo $email . "<br>";
				$email_ids[] = $email;
			} else {
				echo("$email is not a valid email address");
			}
		//echo $email . "<br />";
		}//close of foreach(array_unique($matches[0]) as $email) {
			
	}//close of if ($res) {
	else {
	//echo "No emails found.";
	}//close of else of if ($res) {
	return $email_ids;
}

//extract email from html
function get_phone_numbers_from_html($html_string){
//http://stackoverflow.com/questions/3901070/in-php-how-do-i-extract-multiple-e-mail-addresses-from-a-block-of-text-and-put
	//http://stackoverflow.com/a/24969553
	//This is modified to accomodate different kinds of domain tlds those that are recently available in the market
	//$text=file_get_contents($url);
	$phone_numbers = array();
	$text=$html_string;
	$res = preg_match_all(
	"@[^a-zA-Z]{7,}@is ",
	$text,
	$matches
	);
	
	if ($res) {
		print_r($matches);
		foreach(array_unique($matches[0]) as $phone_number) {
			
			// Remove all illegal characters from email
			$phone_number = filter_var($phone_number, FILTER_SANITIZE_NUMBER_INT);

			if(strlen($phone_number)>=10){
				$phone_numbers[] = $phone_number;
			} else {
				echo("$phone_number is not a valid phone number");
			}
		//echo $email . "<br />";
		}//close of foreach(array_unique($matches[0]) as $email) {
			
	}//close of if ($res) {
	else {
	//echo "No emails found.";
	}//close of else of if ($res) {
	return $phone_numbers;
}


//Make a select query to fqdn_extensions db table to verify if this input matches any of the known domain name extensions
  function check_domain_name_extension($fqdn_hostname){
	global $dbcon;
	//verify how many dots are there and then compare this with the known domain extensions in the database
	$fqdn_hostname_exploded = explode(".", $fqdn_hostname);
	//count number of parts
	$fqdn_hostname_exploded_count = count($fqdn_hostname_exploded);
	echo "fqdn_hostname_exploded_count: " . $fqdn_hostname_exploded_count . "\n";
	print_r($fqdn_hostname_exploded);
	echo 
	$hostname_details = array();
	if($fqdn_hostname_exploded_count == "1") {
		//example: domainname INVALID CASE
		echo "fqdn_hostname: " . $fqdn_hostname . "\n";
		$hostname_details["fqdn"] = "";
		$hostname_details["is_subdomain"] = "no";
		$hostname_details["domain_name_extension"] = "";
		$hostname_details["is_domain_name_extension_valid"] = "invalid";
		
	} else if($fqdn_hostname_exploded_count >= "2") {
		//example: domainname.com
		$hostname_details["fqdn"] = $fqdn_hostname;
		$hostname_details["is_subdomain"] = "no";
		$hostname_details["domain_name_extension"] = $fqdn_hostname_exploded[1];
		$hostname_details["is_domain_name_extension_valid"] = "unknown";
		
	} else if($fqdn_hostname_exploded_count == "3") {
		
		if (($fqdn_hostname_exploded[0] == "www") || ($fqdn_hostname_exploded[0] == "blog") || ($fqdn_hostname_exploded[0] == "mail") || ($fqdn_hostname_exploded[0] == "webmail") || ($fqdn_hostname_exploded[0] == "server") || ($fqdn_hostname_exploded[0] == "admin") || ($fqdn_hostname_exploded[0] == "remote") || ($fqdn_hostname_exploded[0] == "ns1") || ($fqdn_hostname_exploded[0] == "ns2") || ($fqdn_hostname_exploded[0] == "m") || ($fqdn_hostname_exploded[0] == "smtp") || ($fqdn_hostname_exploded[0] == "secure") || ($fqdn_hostname_exploded[0] == "shop") || ($fqdn_hostname_exploded[0] == "ftp") || ($fqdn_hostname_exploded[0] == "ftps") || ($fqdn_hostname_exploded[0] == "dev") || ($fqdn_hostname_exploded[0] == "web") || ($fqdn_hostname_exploded[0] == "email") || ($fqdn_hostname_exploded[0] == "cdn") || ($fqdn_hostname_exploded[0] == "api") || ($fqdn_hostname_exploded[0] == "app") || ($fqdn_hostname_exploded[0] == "vps") || ($fqdn_hostname_exploded[0] == "news") || ($fqdn_hostname_exploded[0] == "cdn") || ($fqdn_hostname_exploded[0] == "host") || ($fqdn_hostname_exploded[0] == "support") || ($fqdn_hostname_exploded[0] == "mx") || ($fqdn_hostname_exploded[0] == "store") || ($fqdn_hostname_exploded[0] == "bbs")) {
			//example: www.domainname.org or blog.domainname.org
			$hostname_details["fqdn"] = $fqdn_hostname_exploded[1] . "." . $fqdn_hostname_exploded[2];
			$hostname_details["is_subdomain"] = "yes";
			$hostname_details["domain_name_extension"] = $fqdn_hostname_exploded[2];
			$hostname_details["is_domain_name_extension_valid"] = "unknown";
			
			
		} else {
			//example: domainname.ae.org
			$hostname_details["fqdn"] = $fqdn_hostname;
			$hostname_details["is_subdomain"] = "no";
			$hostname_details["domain_name_extension"] = $fqdn_hostname_exploded[1] . "." . $fqdn_hostname_exploded[2];
			$hostname_details["is_domain_name_extension_valid"] = "unknown";
		}
	} else if($fqdn_hostname_exploded_count == "4") {
		if (($fqdn_hostname_exploded[0] != "www") && ($fqdn_hostname_exploded[0] != "blog") && ($fqdn_hostname_exploded[0] != "mail") && ($fqdn_hostname_exploded[0] != "webmail") && ($fqdn_hostname_exploded[0] != "server") && ($fqdn_hostname_exploded[0] != "admin") && ($fqdn_hostname_exploded[0] != "remote") && ($fqdn_hostname_exploded[0] != "ns1") && ($fqdn_hostname_exploded[0] != "ns2") && ($fqdn_hostname_exploded[0] != "m") && ($fqdn_hostname_exploded[0] != "smtp") && ($fqdn_hostname_exploded[0] != "secure") && ($fqdn_hostname_exploded[0] != "shop") && ($fqdn_hostname_exploded[0] != "ftp") && ($fqdn_hostname_exploded[0] != "ftps") && ($fqdn_hostname_exploded[0] != "dev") && ($fqdn_hostname_exploded[0] != "web") && ($fqdn_hostname_exploded[0] != "email") && ($fqdn_hostname_exploded[0] != "cdn") && ($fqdn_hostname_exploded[0] != "api") && ($fqdn_hostname_exploded[0] != "app") && ($fqdn_hostname_exploded[0] != "vps") && ($fqdn_hostname_exploded[0] != "news") && ($fqdn_hostname_exploded[0] != "cdn") && ($fqdn_hostname_exploded[0] != "host") && ($fqdn_hostname_exploded[0] != "support") && ($fqdn_hostname_exploded[0] != "mx") && ($fqdn_hostname_exploded[0] != "store") && ($fqdn_hostname_exploded[0] != "bbs")) {
			//example: domainname.ae.org.in
			$hostname_details["fqdn"] = $fqdn_hostname;
			$hostname_details["is_subdomain"] = "no";
			$hostname_details["domain_name_extension"] = $fqdn_hostname_exploded[1] . "." . $fqdn_hostname_exploded[2] . "." . $fqdn_hostname_exploded[3];
			$hostname_details["is_domain_name_extension_valid"] = "invalid";
		} else {
			 if (($fqdn_hostname_exploded[0] == "www") && (($fqdn_hostname_exploded[1] == "blog") || ($fqdn_hostname_exploded[1] == "mail") || ($fqdn_hostname_exploded[1] == "webmail") || ($fqdn_hostname_exploded[1] == "server") || ($fqdn_hostname_exploded[1] == "admin") || ($fqdn_hostname_exploded[1] == "remote") || ($fqdn_hostname_exploded[1] == "ns1") || ($fqdn_hostname_exploded[1] == "ns2") || ($fqdn_hostname_exploded[1] == "m") || ($fqdn_hostname_exploded[1] == "smtp") || ($fqdn_hostname_exploded[1] == "secure") || ($fqdn_hostname_exploded[1] == "shop") || ($fqdn_hostname_exploded[1] == "ftp") || ($fqdn_hostname_exploded[1] == "ftps") || ($fqdn_hostname_exploded[1] == "dev") || ($fqdn_hostname_exploded[1] == "web") || ($fqdn_hostname_exploded[1] == "email") || ($fqdn_hostname_exploded[1] == "cdn") || ($fqdn_hostname_exploded[1] == "api") || ($fqdn_hostname_exploded[1] == "app") || ($fqdn_hostname_exploded[1] == "vps") || ($fqdn_hostname_exploded[1] == "news") || ($fqdn_hostname_exploded[1] == "cdn") || ($fqdn_hostname_exploded[1] == "host") || ($fqdn_hostname_exploded[1] == "support") || ($fqdn_hostname_exploded[1] == "mx") || ($fqdn_hostname_exploded[1] == "store") || ($fqdn_hostname_exploded[1] == "bbs"))) {
				//example: www.blog.domainname.org (top level tld based sub domain)
				$hostname_details["fqdn"] = $fqdn_hostname_exploded[2] . "." . $fqdn_hostname_exploded[3];
				$hostname_details["is_subdomain"] = "yes";
				$hostname_details["domain_name_extension"] = $fqdn_hostname_exploded[3];
				$hostname_details["is_domain_name_extension_valid"] = "unknown";
			} else if ($fqdn_hostname_exploded[0] == "www") {
				//example: www.domainname.ae.org , blog.domainname.ae.org, ns1.domainname.ae.org (country level tld based sub domain)
				$hostname_details["fqdn"] = $fqdn_hostname_exploded[1] . "." . $fqdn_hostname_exploded[2] . "." . $fqdn_hostname_exploded[3];
				$hostname_details["is_subdomain"] = "yes";
				$hostname_details["domain_name_extension"] = $fqdn_hostname_exploded[2] . "." . $fqdn_hostname_exploded[3];
				$hostname_details["is_domain_name_extension_valid"] = "unknown";
			}
			
		}
	} else if($fqdn_hostname_exploded_count > "4") {
		//take last three items only. but these links are not supported as of now
		    
			$hostname_details["fqdn"] = "";
			$hostname_details["is_subdomain"] = "no";
			$hostname_details["domain_name_extension"] = "";
			$hostname_details["is_domain_name_extension_valid"] = "not-supported";
	}
	
	$domain_extension_sql = "SELECT `fqdn_extension_id`, `fqdn_extension_type`, `fqdn_extension_description`, `is_active_status` FROM `fqdn_extensions` WHERE `fqdn_extension_name`=:fqdn_extension_name";
	
	$domain_extension_query = $dbcon->prepare($domain_extension_sql);
	$domain_extension_query->bindValue(":fqdn_extension_name",$hostname_details["domain_name_extension"]);
	$domain_extension_query->execute();
	if($domain_extension_query->rowCount() > 0) {
	   //$domain_extension_query_result = $domain_extension_query->fetch();
	   $hostname_details["is_domain_name_extension_valid"] = "valid";
	   //return $domain_extension_query_result;
	}
	return $hostname_details;
  }
  
  //Make a select query to fqdn_extensions db table to verify if this input matches any of the known domain name extensions
  function check_domain_name_extension_minimal($domain_name_extension){
	global $dbcon;
	$domain_extension_sql = "SELECT `fqdn_extension_id`, `fqdn_extension_type`, `fqdn_extension_description`, `is_active_status` FROM `fqdn_extensions` WHERE `fqdn_extension_name`=:fqdn_extension_name";
	
	$domain_extension_query = $dbcon->prepare($domain_extension_sql);
	$domain_extension_query->bindValue(":fqdn_extension_name",$domain_name_extension);
	$domain_extension_query->execute();
	if($domain_extension_query->rowCount() > 0) {
	   return true;
	} else {
	   return false;	
	}//close of else of if($domain_extension_query->rowCount() > 0) {
  }
  
  //Make a select query to fqdn_domain_names db table to get the fqdn_domain_name
  function get_fqdn_domain_name_from_id($fqdn_id){
	global $dbcon;
	$fqdn_domain_name_sql = "SELECT `fqdn_domain_name` FROM `fqdn_domain_names` WHERE `fqdn_domain_name_id`=:fqdn_domain_name_id";	
	$fqdn_domain_name_query = $dbcon->prepare($fqdn_domain_name_sql);
	$fqdn_domain_name_query->bindValue(":fqdn_domain_name_id",$fqdn_id);
	$fqdn_domain_name_query->execute();
	if($fqdn_domain_name_query->rowCount() > 0) {
	   $fqdn_domain_name_query_result = $fqdn_domain_name_query->fetch();
	   return $fqdn_domain_name_query_result["fqdn_domain_name"];
	} else {
	   return "";	
	}//close of else of if($fqdn_domain_name_query->rowCount() > 0) {
  }
  
  //Make a select query to fqdn_domain_names db table to get the fqdn_domain_name
  function get_fqdn_sub_domain_name_from_id($fqdn_sub_domain_name_id){
	global $dbcon;
	$fqdn_sub_domain_name_sql = "SELECT `sub_domain_url_scheme`, `sub_domain_name` FROM `fqdn_domain_name_sub_dom_mapping` WHERE `sub_domain_name_id`=:sub_domain_name_id";	
	$fqdn_sub_domain_name_query = $dbcon->prepare($fqdn_sub_domain_name_sql);
	$fqdn_sub_domain_name_query->bindValue(":sub_domain_name_id",$fqdn_sub_domain_name_id);
	$fqdn_sub_domain_name_query->execute();
	if($fqdn_sub_domain_name_query->rowCount() > 0) {
	   $fqdn_sub_domain_name_query_result = $fqdn_sub_domain_name_query->fetch();
	   return $fqdn_sub_domain_name_query_result["sub_domain_name"];
	} else {
	   return "";	
	}//close of else of if($fqdn_sub_domain_name_query->rowCount() > 0) {
  }
  
  //Make a select query to fqdn_domain_names db table to get the priority_status of fqdn_domain_name
  function get_priority_status_fqdn_domain_name_from_id($fqdn_id){
	global $dbcon;
	$fqdn_domain_name_sql = "SELECT `priority_status` FROM `fqdn_domain_names` WHERE `fqdn_domain_name_id`=:fqdn_domain_name_id";	
	$fqdn_domain_name_query = $dbcon->prepare($fqdn_domain_name_sql);
	$fqdn_domain_name_query->bindValue(":fqdn_domain_name_id",$fqdn_id);
	$fqdn_domain_name_query->execute();
	if($fqdn_domain_name_query->rowCount() > 0) {
	   $fqdn_domain_name_query_result = $fqdn_domain_name_query->fetch();
	   return $fqdn_domain_name_query_result["priority_status"];
	} else {
	   return "";	
	}//close of else of if($fqdn_domain_name_query->rowCount() > 0) {
  }
  
  //Make a select query to fqdn_domain_names db table to verify if this keyword is inputed for the first time or this is a recurring attempt
  function check_domain_name_input($domain_name){
	global $dbcon;
	$fqdn_domain_check_sql = "SELECT `fqdn_domain_name_id`, `domain_name_actual_input_count`, `fqdn_domain_name_registration_status`, `is_free_email_provider`, `is_disposable_email_address_provider`, `fqdn_domain_name_entry_type`, `priority_status`, `is_crawling_done` FROM `fqdn_domain_names` WHERE `fqdn_domain_name`=:fqdn_domain_name";
	
	$fqdn_domain_check_query = $dbcon->prepare($fqdn_domain_check_sql);
	$fqdn_domain_check_query->bindValue(":fqdn_domain_name",$domain_name);
	$fqdn_domain_check_query->execute();
	if($fqdn_domain_check_query->rowCount() > 0) {
	   $fqdn_domain_check_query_result = $fqdn_domain_check_query->fetch();
	   return $fqdn_domain_check_query_result;
	}
	return "";
  }
  
  function check_sub_domain_name_input($sub_domain_scheme, $sub_domain_name){
	global $dbcon;
	$sub_domain_check_sql = "SELECT `sub_domain_name_id`, `sub_domain_url_scheme`, `sub_domain_name`, `fqdn_domain_name_id`, `http_status_code`, `redirected_to`, `is_crawled_for_links`, `is_active_status` FROM `fqdn_domain_name_sub_dom_mapping` WHERE `sub_domain_url_scheme`=:sub_domain_url_scheme AND `sub_domain_name`=:sub_domain_name";
	
	$sub_domain_check_query = $dbcon->prepare($sub_domain_check_sql);
	$sub_domain_check_query->bindValue(":sub_domain_url_scheme",$sub_domain_scheme);
	$sub_domain_check_query->bindValue(":sub_domain_name",$sub_domain_name);
	$sub_domain_check_query->execute();
	if($sub_domain_check_query->rowCount() > 0) {
	   $sub_domain_check_query_result = $sub_domain_check_query->fetch();
	   return $sub_domain_check_query_result;
	}
	return "";
  }
  
  //Make a select query to internal_urls db table to verify if this url input is inputed for the first time or this is a recurring attempt
  function check_internal_url_input($internal_url_input){
	global $dbcon;
	$internal_url_check_sql = "SELECT `internal_url_id`, `http_status_code`, `redirected_to`, `sub_domain_name_id`, `fqdn_domain_name_id`, `is_crawled_for_int_urls`, `is_crawled_for_email_ids`, `total_number_of_links`, `last_crawled_date_time`, `last_crawled_date_time_epoch`, `is_active_status` FROM `internal_urls` WHERE `internal_url`=:internal_url";
	
	$internal_url_check_query = $dbcon->prepare($internal_url_check_sql);
	$internal_url_check_query->bindValue(":internal_url",$internal_url_input);
	$internal_url_check_query->execute();
	if($internal_url_check_query->rowCount() > 0) {
	   $internal_url_check_query_result = $internal_url_check_query->fetch();
	   return $internal_url_check_query_result;
	}
	return "";
  }
  
  //Make a select query to internal_urls db table to verify if the hash of this url input is inputed for the first time or this is a recurring attempt
  function check_internal_url_hash_input($internal_url_input_hash){
	global $dbcon;
	$internal_url_check_sql = "SELECT `internal_url_id`, `internal_url`, `http_status_code`, `redirected_to`, `sub_domain_name_id`, `fqdn_domain_name_id`, `is_crawled_for_int_urls`, `is_crawled_for_email_ids`, `total_number_of_links`, `last_crawled_date_time`, `last_crawled_date_time_epoch`, `is_active_status` FROM `internal_urls` WHERE `int_url_hash`=:int_url_hash";
	
	$internal_url_check_query = $dbcon->prepare($internal_url_check_sql);
	$internal_url_check_query->bindValue(":int_url_hash",$internal_url_input_hash);
	$internal_url_check_query->execute();
	if($internal_url_check_query->rowCount() > 0) {
	   $internal_url_check_query_result = $internal_url_check_query->fetch();
	   return $internal_url_check_query_result;
	}
	return "";
  }
  
  //Make a select query to internal_url_reference_mapping db table to verify if this referring url input and referred url input is inputed for the first time or this is a recurring attempt
  function check_internal_url_ref_mapping($referring_internal_url_input, $referred_internal_url_input){
	global $dbcon;
	$internal_url_ref_mapping_check_sql = "SELECT `fqdn_internal_url_mapping_id`, `referring_internal_url_id`, `referring_int_url_sub_domain_name_id`, `referring_int_url_fqdn_domain_name_id`, `referred_internal_url_id`, `link_type`, `is_active_status` FROM `internal_url_reference_mapping` WHERE `referring_internal_url_id`=:referring_internal_url_id AND `referred_internal_url_id`=:referred_internal_url_id";
	
	$internal_url_ref_mapping_check_query = $dbcon->prepare($internal_url_ref_mapping_check_sql);
	$internal_url_ref_mapping_check_query->bindValue(":referring_internal_url_id",$referring_internal_url_input);
	$internal_url_ref_mapping_check_query->bindValue(":referred_internal_url_id",$referred_internal_url_input);
	$internal_url_ref_mapping_check_query->execute();
	if($internal_url_ref_mapping_check_query->rowCount() > 0) {
	   $internal_url_ref_mapping_check_query_result = $internal_url_ref_mapping_check_query->fetch();
	   return $internal_url_ref_mapping_check_query_result;
	}
	return "";
  }
  
  
  
  function get_domain_name_dns_a_ns_mx_cname_records($domain_name){
                    $domain_final_array = dns_get_record($domain_name, DNS_A + DNS_NS + DNS_MX + DNS_CNAME);
                    return $domain_final_array;
  }
  
  
  //Make a select query to collected_email_ids db table to verify if this collected email id input is inputed for the first time or this is a recurring attempt
  function check_collected_email_id_input($collected_email_id_input){
	global $dbcon;
	$collected_email_id_check_sql = "SELECT `collected_email_ids_id`, `collected_email_id`, `first_name`, `middle_name`, `last_name`, `email_account_type`, `entry_type`, `email_format_status`, `dns_mx_record_status`, `have_mail_server_on_domain`, `smtp_message_check_status`, `smtp_message_status_result`, `direct_email_message_check_status`, `direct_email_message_check_result`, `is_catch_all_email_address`, `is_verified`, `confidence_score`, `is_active_status` FROM `collected_email_ids` WHERE `collected_email_id`=:collected_email_id";
	
	$collected_email_id_check_query = $dbcon->prepare($collected_email_id_check_sql);
	$collected_email_id_check_query->bindValue(":collected_email_id",$collected_email_id_input);
	$collected_email_id_check_query->execute();
	if($collected_email_id_check_query->rowCount() > 0) {
	   $collected_email_id_check_query_result = $collected_email_id_check_query->fetch();
	   return $collected_email_id_check_query_result;
	}
	return "";
  }
  
  //Make a select query to collected_email_int_url_domain_name_mapping db table to verify if this collected email id input and internal url input is inputed for the first time or this is a recurring attempt
  function check_collected_email_int_url_ref_mapping($collected_email_ids_id_input, $internal_url_id_input){
	global $dbcon;
	$coll_email_id_int_url_ref_map_check_sql = "SELECT `collected_email_id_int_url_dom_name_map_id`, `collected_email_ids_id`, `collected_email_id`, `internal_url_id`, `internal_url`, `sub_domain_name_id`, `fqdn_domain_name_id`, `is_active_status` FROM `collected_email_int_url_domain_name_mapping` WHERE `collected_email_ids_id` = :collected_email_ids_id AND `internal_url_id` = :internal_url_id";
	
	$coll_email_id_int_url_ref_map_check_query = $dbcon->prepare($coll_email_id_int_url_ref_map_check_sql);
	$coll_email_id_int_url_ref_map_check_query->bindValue(":collected_email_ids_id",$collected_email_ids_id_input);
	$coll_email_id_int_url_ref_map_check_query->bindValue(":internal_url_id",$internal_url_id_input);
	$coll_email_id_int_url_ref_map_check_query->execute();
	if($coll_email_id_int_url_ref_map_check_query->rowCount() > 0) {
	   $coll_email_id_int_url_ref_map_check_query_result = $coll_email_id_int_url_ref_map_check_query->fetch();
	   return $coll_email_id_int_url_ref_map_check_query_result;
	}
	return "";
  }
  
  
  
  //Verify Email 
  function get_email_id_details($email_id, $email_id_local_part, $email_id_hostname, $email_id_fqdn, $email_id_hostname_extension){
	global $dbcon;
	$email_id_details_array = array();
	$email_id = filter_var($email_id, FILTER_SANITIZE_EMAIL);
	if(filter_var($email_id, FILTER_VALIDATE_EMAIL)) {
		//Email format is valid
		$email_id_details_array["email_id"] = $email_id;
		$email_id_details_array["email_id_local_part"] = $email_id_local_part;
		$email_id_details_array["email_id_hostname"] = $email_id_hostname;
		
		//create FQDN from the Email ID Hostname
		
		$email_id_details_array["email_id_domain_name"] = $email_id_fqdn;
		
		//verify if this fqdn domain name is valid as per DNS
		if (checkdnsrr($email_id_details_array['email_id_domain_name'], "MX")) {
			$email_id_details_array["domain_name_mx_record_status"] = "valid-dns";
		} else {
			$email_id_details_array["domain_name_mx_record_status"] = "invalid-dns";
		}//close of else of if (checkdnsrr($email_id_details_array['email_id_domain_name'], "MX")) {
		
		$fqdn_domain_check_sql = "SELECT `fqdn_domain_name_id`, `fqdn_domain_name`, `domain_name_actual_input_count`, `fqdn_domain_name_registration_status`, `is_free_email_provider`, `is_disposable_email_address_provider`, `is_crawling_done` FROM `fqdn_domain_names` WHERE `fqdn_domain_name`=:fqdn_domain_name";
		
		$fqdn_domain_check_query = $dbcon->prepare($fqdn_domain_check_sql);
		$fqdn_domain_check_query->bindValue(":fqdn_domain_name",$email_id_details_array['email_id_domain_name']);
		$fqdn_domain_check_query->execute();
		if($fqdn_domain_check_query->rowCount() > 0) {
		   $fqdn_domain_check_query_result = $fqdn_domain_check_query->fetch();
		}
		
		if ($fqdn_domain_check_query_result) {
			if (($fqdn_domain_check_query_result["is_free_email_provider"] == "0") && ($fqdn_domain_check_query_result["is_disposable_email_address_provider"] == "1")) {
			//this domain name is a disposable email provider
				$email_id_details_array["email_service_provider_type"] = "D";
			} else if (($fqdn_domain_check_query_result["is_free_email_provider"] == "1") && ($fqdn_domain_check_query_result["is_disposable_email_address_provider"] == "0")) {
				//this domain name is a free email provider
				$email_id_details_array["email_service_provider_type"] = "F";
			} else if (($fqdn_domain_check_query_result["is_free_email_provider"] == "1") && ($fqdn_domain_check_query_result["is_disposable_email_address_provider"] == "1")) {
				//this domain name is a free email as well as a disposable email provider
				$email_id_details_array["email_service_provider_type"] = "FD";
			} else {
				// this is a regular and professional email provider
				$email_id_details_array["email_service_provider_type"] = "O";
			}//close of else of if (($email_id_domain_name_details["is_free_email_provider"] == "0") && ($email_id_domain_name_details["is_disposable_email_address_provider"] == "1")) {
			$email_id_details_array["email_service_provider_name"] = $fqdn_domain_check_query_result["fqdn_domain_name"];
			$email_id_details_array["email_service_provider_fqdn_domain_name_id"] = $fqdn_domain_check_query_result["fqdn_domain_name_id"];
			
		} else {
				$email_id_details_array["email_service_provider_type"] = "U";
				$email_id_details_array["email_service_provider_name"] = null;
				$email_id_details_array["email_service_provider_fqdn_domain_name_id"] = null;
				
		}//close of else of if ($fqdn_domain_check_query_result) {
		
		//check the email account related hostname extension_loaded
		
		$email_id_hostname_extension_sql = "SELECT `fqdn_extension_id`, `fqdn_extension_type`, `fqdn_extension_description`, `is_active_status` FROM `fqdn_extensions` WHERE `fqdn_extension_name`=:fqdn_extension_name";
		
		$email_id_hostname_extension_query = $dbcon->prepare($email_id_hostname_extension_sql);
		$email_id_hostname_extension_query->bindValue(":fqdn_extension_name",$email_id_hostname_extension);
		$email_id_hostname_extension_query->execute();
		if($email_id_hostname_extension_query->rowCount() > 0) {
		   $email_id_details_array["email_hostname_extension_check"] = "hostname-extension-valid";
		} else {
		   $email_id_details_array["email_hostname_extension_check"] = "hostname-extension-invalid";
		}//close of else of if($email_id_hostname_extension_query->rowCount() > 0) {
			
				
		
		
		//check the email_account_type
		$email_account_type_sql = "SELECT `generic_email_address_role_id`, `is_active_status` FROM `generic_email_address_role` WHERE `role_name` = :role_name";
		
		$email_account_type_query = $dbcon->prepare($email_account_type_sql);
		$email_account_type_query->bindValue(":role_name",$email_id_details_array['email_id_local_part']);
		$email_account_type_query->execute();
		if($email_account_type_query->rowCount() > 0) {
		   $email_account_type_query_result = $email_account_type_query->fetch();
		}
		if ($email_account_type_query_result) {
			$email_id_details_array["email_account_type"] = "generic"; // personal | generic
		} else {
			$email_id_details_array["email_account_type"] = "personal"; // personal | generic
		}//close of else of if ($email_account_type_query_result) {
		
		//extract First Name & Last name from the Email ID Local Part
		/*
		firstname.lastname@companyname.com (default preference)
		firstname@companyname.com (default preference)
		lastname@companyname.com
		firstinitial+lastname@companyname.com
		firstname+lastname@companyname.com
		firstinitial+lastinitial@companyname.com
		firstinitial+lastname@companyname.com
		firstinitial.lastname@companyname.com	
		firstinitial@companyname.com
		*/
		if(!stristr($email_id_details_array['email_id_local_part'], '_') === FALSE) {
			//underscore is found
			$email_id_localpart_exploded = explode("_",$email_id_details_array['email_id_local_part']);
			
		} else {
			$email_id_localpart_exploded = explode(".",$email_id_details_array['email_id_local_part']);
		}
		//print_r($email_id_localpart_exploded);
		//calculate the count of exploded values
		$email_id_localpart_exploded_count = count($email_id_localpart_exploded);
		if ($email_id_localpart_exploded_count == "3") {
			//echo "count: 3 \n";
			if ($email_id_details_array["email_account_type"] == "personal") {
				$email_id_details_array["email_id_first_name"] = $email_id_localpart_exploded[0];
				$email_id_details_array["email_id_middle_name"] = $email_id_localpart_exploded[1];
				$email_id_details_array["email_id_last_name"] = $email_id_localpart_exploded[2];
			} else{
				$email_id_details_array["email_id_first_name"] = null;
				$email_id_details_array["email_id_middle_name"] = null;
				$email_id_details_array["email_id_last_name"] = null;
			} //close of else of if ($email_id_details_array["email_account_type"] == "personal") {
		} else if ($email_id_localpart_exploded_count == "2") {
			//echo "count: 2 \n";
			if ($email_id_details_array["email_account_type"] == "personal") {
				$email_id_details_array["email_id_first_name"] = $email_id_localpart_exploded[0];
				$email_id_details_array["email_id_middle_name"] = null;
				$email_id_details_array["email_id_last_name"] = $email_id_localpart_exploded[1];
			} else{
				$email_id_details_array["email_id_first_name"] = null;
				$email_id_details_array["email_id_middle_name"] = null;
				$email_id_details_array["email_id_last_name"] = null;
			} //close of else of if ($email_id_details_array["email_account_type"] == "personal") {
		} else if ($email_id_localpart_exploded_count == "1") {
			//echo "count: 1 \n";
			if ($email_id_details_array["email_account_type"] == "personal") {
				$email_id_details_array["email_id_first_name"] = $email_id_localpart_exploded[0];
				$email_id_details_array["email_id_middle_name"] = null;
				$email_id_details_array["email_id_last_name"] = null;
			} else{
				$email_id_details_array["email_id_first_name"] = null;
				$email_id_details_array["email_id_middle_name"] = null;
				$email_id_details_array["email_id_last_name"] = null;
			} //close of else of if ($email_id_details_array["email_account_type"] == "personal") {
		} else {
			//echo "count: ELSE \n";
			$email_id_details_array["email_id_first_name"] = null;
			$email_id_details_array["email_id_middle_name"] = null;
			$email_id_details_array["email_id_last_name"] = null;
		}//close of else of if ($email_id_localpart_exploded_count = "3") {
		
		
		
		//SMTP checks
		
		//Email Format Status
		$email_id_details_array["email_id_format_status"] = "valid-format";
		
		//Email ID Status
		$email_id_details_array["is_email_id_valid"] = "valid";
	} else {
		//Email format is invalid
			$email_id_details_array["email_id"] = $email_id;
			$email_id_details_array["email_id_local_part"] = null;
			$email_id_details_array["email_id_domain_name"] = null;
		
			$email_id_details_array["domain_name_mx_record_status"] = "invalid-dns";
		
			$email_id_details_array["email_service_provider_type"] = null;
			$email_id_details_array["email_service_provider_name"] = null;
			
			$email_id_details_array["email_hostname_extension_check"] = "hostname-extension-unknown";
		
			$email_id_details_array["email_account_type"] = null;
		
			$email_id_details_array["email_id_first_name"] = null;
			$email_id_details_array["email_id_middle_name"] = null;
			$email_id_details_array["email_id_last_name"] = null;
			
			$email_id_details_array["email_id_format_status"] = "invalid-format";
		
		    $email_id_details_array["is_email_id_valid"] = "invalid";
		
	}//close of else of if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
	
	
	
	
	return $email_id_details_array;
  }
  
?>