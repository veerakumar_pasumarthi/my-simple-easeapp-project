<?php

function create_expiring_link_with_signature($link_without_signature_input, $base64_encoded_secret_key_input, $hmac_hash_algorithm_input) {
	
	//Collect the Base64 Decoded version of the Expiring Link Secret Key
	$base64_decoded_expiring_link_secret = base64_decode($base64_encoded_secret_key_input);
	
	//Create Signature
	$created_hash = hash_hmac($hmac_hash_algorithm_input, $link_without_signature_input, $base64_decoded_expiring_link_secret, true);
	
	//Base64 URL Encode the Created Hash
	$base64_urlencoded_created_hash = base64url_encode($created_hash);

	//Do Remove padding (=), from the Base64 URL Encoded Hash
	$base64_urlencoded_created_hash_after_removing_padding = str_replace("=", "", $base64_urlencoded_created_hash);
	
	return $base64_urlencoded_created_hash_after_removing_padding;

}

function verify_expiring_link_with_signature($link_without_signature_input, $base64_encoded_secret_key_input, $hmac_hash_algorithm_input, $received_signature_input) {
	
	//Collect the Base64 Decoded version of the Expiring Link Secret Key
	$base64_decoded_expiring_link_secret = base64_decode($base64_encoded_secret_key_input);
	
	//Create Signature
	$created_hash = hash_hmac($hmac_hash_algorithm_input, $link_without_signature_input, $base64_decoded_expiring_link_secret, true);
	
	//Base64 URL Encode the Created Hash
	$base64_urlencoded_created_hash = base64url_encode($created_hash);

	//Do Remove padding (=), from the Base64 URL Encoded Hash
	$base64_urlencoded_created_hash_after_removing_padding = str_replace("=", "", $base64_urlencoded_created_hash);
	
	if ((function_exists('hash_equals')) && (hash_equals($received_signature_input, $base64_urlencoded_created_hash_after_removing_padding))) {
		//Valid Signature Scenario
		return true;
	} else {
		//Signature Not Valid Scenario
		return false;
	}
	
}
 

?>