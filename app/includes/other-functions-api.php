<?php
if(defined('STDIN') ){
  //echo("Running from CLI");
} else{
  //echo("Not Running from CLI");
  defined('START') or die;
}

/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 *
 * This page contains application related rest api specific functions.
 */

function user_register_basic_details_insert_duplicate_check($email_id_input) {
	global $dbcon;
	$constructed_array = array();

	$rest_register_check_sql = "SELECT `sm_memb_id`,`sm_email` FROM `site_members` WHERE `sm_email` = :sm_email AND `sm_user_status` != :sm_user_status";
	$rest_register_check_q = $dbcon->prepare($rest_register_check_sql);
	$rest_register_check_q->bindValue(":sm_email",$email_id_input);
    $rest_register_check_q->bindValue(":sm_user_status","4");
	$rest_register_check_q->execute();

	if($rest_register_check_q->rowCount() > 0) {
		$rest_register_check_q_result = $rest_register_check_q->fetch();
	     return $rest_register_check_q_result;

	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;

}


function user_basic_details_check_based_on_user_id($sm_memb_id_input) {
	global $dbcon;
	$constructed_array = array();
	$rest_user_check_sql = "SELECT * FROM `site_members` WHERE `sm_memb_id` = :sm_memb_id";
	$rest_user_check_select_query = $dbcon->prepare($rest_user_check_sql);
	$rest_user_check_select_query->bindValue(":sm_memb_id",$sm_memb_id_input);
	$rest_user_check_select_query->execute();

	if($rest_user_check_select_query->rowCount() > 0) {
		$rest_user_check_select_query_result = $rest_user_check_select_query->fetch();
	     return $rest_user_check_select_query_result;

	}//close of if($rest_user_check_select_query->rowCount() > 0) {
	return $constructed_array;

}
function candidate_rel_additional_info_details_get($last_inserted_id) {
	global $dbcon;
	$constructed_array = array();
	$candidate_rel_additional_details_check_sql = "SELECT * FROM `candidate_rel_additional_info` WHERE `sm_memb_id` = :sm_memb_id";
	$candidate_details_check_select_query = $dbcon->prepare($candidate_rel_additional_details_check_sql);
	$candidate_details_check_select_query->bindValue(":sm_memb_id",$last_inserted_id);
	$candidate_details_check_select_query->execute();

	if($candidate_details_check_select_query->rowCount() > 0) {
		$candidate_details_check_select_query_result = $candidate_details_check_select_query->fetch();
	     return $candidate_details_check_select_query_result;

	}//close of if($rest_user_check_select_query->rowCount() > 0) {
	return $constructed_array;

}

function company_basic_details_check_based_on_company_id($company_id_input) {
	global $dbcon;
	$constructed_array = array();
	$rest_company_check_sql = "SELECT * FROM `companies` WHERE `company_id` = :company_id";
	$rest_company_check_select_query = $dbcon->prepare($rest_company_check_sql);
	$rest_company_check_select_query->bindValue(":company_id",$company_id_input);
	$rest_company_check_select_query->execute();

	if($rest_company_check_select_query->rowCount() > 0) {
		$rest_company_check_select_query_result = $rest_company_check_select_query->fetch();
	     return $rest_company_check_select_query_result;

	}//close of if($rest_company_check_select_query->rowCount() > 0) {
	return $constructed_array;

}
function client_company_basic_details_check_based_on_company_client_id($company_client_id_input) {
	global $dbcon;
	$constructed_array = array();
	$rest_client_company_check_sql = "SELECT * FROM `company_clients` WHERE `company_client_id` = :company_client_id";
	$rest_client_company_check_select_query = $dbcon->prepare($rest_client_company_check_sql);
	$rest_client_company_check_select_query->bindValue(":company_client_id",$company_client_id_input);
	$rest_client_company_check_select_query->execute();

	if($rest_client_company_check_select_query->rowCount() > 0) {
		$rest_client_company_check_select_query_result = $rest_client_company_check_select_query->fetch();
	     return $rest_client_company_check_select_query_result;

	}//close of if($rest_client_company_check_select_query->rowCount() > 0) {
	return $constructed_array;

}

function user_register_basic_details_insert($salutation_input,$firstname_input,$middlename_input,$lastname_input,$mobile_number_input,$email_id_input,$alternate_email_id_input,$company_id_input,$user_type_input,$admin_level_input, $user_role_input, $added_by_admin_user_id_input, $added_by_admin_user_firstname_input, $added_by_admin_user_middlename_input, $added_by_admin_user_lastname_input, $added_date_time_input, $added_date_time_epoch_input, $sm_user_email_act_code_input, $sm_user_status_input) {
	global $dbcon;
	$register_details_sql = "INSERT INTO `site_members` (sm_salutation,sm_firstname,sm_middlename,sm_lastname,sm_mobile,sm_email,sm_alternate_email,company_id,sm_user_type,sm_admin_level,sm_user_role,added_by_admin_user_id,added_by_admin_user_firstname,added_by_admin_user_middlename,added_by_admin_user_lastname,added_date_time,added_date_time_epoch,sm_user_email_act_code,sm_user_status) VALUES(:sm_salutation, :sm_firstname, :sm_middlename, :sm_lastname, :sm_mobile, :sm_email, :sm_alternate_email, :company_id, :sm_user_type, :sm_admin_level, :sm_user_role, :added_by_admin_user_id, :added_by_admin_user_firstname, :added_by_admin_user_middlename, :added_by_admin_user_lastname, :added_date_time, :added_date_time_epoch, :sm_user_email_act_code, :sm_user_status)";
	$register_details_q = $dbcon->prepare($register_details_sql);
	$register_details_q->bindValue(":sm_salutation",$salutation_input);
	$register_details_q->bindValue(":sm_firstname",$firstname_input);
	$register_details_q->bindValue(":sm_middlename",$middlename_input);
	$register_details_q->bindValue(":sm_lastname",$lastname_input);
	$register_details_q->bindValue(":sm_mobile",$mobile_number_input);
    $register_details_q->bindValue(":sm_email",$email_id_input);
	$register_details_q->bindValue(":sm_alternate_email",$alternate_email_id_input);
	$register_details_q->bindValue(":company_id",$company_id_input);
	$register_details_q->bindValue(":sm_user_type",$user_type_input);
	$register_details_q->bindValue(":sm_admin_level",$admin_level_input);
	$register_details_q->bindValue(":sm_user_role",$user_role_input);
	$register_details_q->bindValue(":added_by_admin_user_id",$added_by_admin_user_id_input);
	$register_details_q->bindValue(":added_by_admin_user_firstname",$added_by_admin_user_firstname_input);
	$register_details_q->bindValue(":added_by_admin_user_middlename",$added_by_admin_user_middlename_input);
	$register_details_q->bindValue(":added_by_admin_user_lastname",$added_by_admin_user_lastname_input);
	$register_details_q->bindValue(":added_date_time",$added_date_time_input);
	$register_details_q->bindValue(":added_date_time_epoch",$added_date_time_epoch_input);
	$register_details_q->bindValue(":sm_user_email_act_code",$sm_user_email_act_code_input);
	$register_details_q->bindValue(":sm_user_status",$sm_user_status_input);

		if ($register_details_q->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");

			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }
}

function user_association_record_check($last_inserted_id,$admin_classification_input){
	global $dbcon;
	$constructed_array = array();
	$valid_to_date_input = '%'.'present'.'%';
	$is_active_status_input = '1';

	$user_association_record_check_sql = "SELECT * FROM sm_site_member_classification_associations WHERE sm_memb_id = :sm_memb_id AND sm_site_member_classification_detail_id = :sm_site_member_classification_detail_id AND valid_to_date LIKE :valid_to_date AND is_active_status = :is_active_status";
	$user_association_record_check_q = $dbcon->prepare($user_association_record_check_sql);
	$user_association_record_check_q->bindValue(":sm_memb_id",$last_inserted_id);
    $user_association_record_check_q->bindValue(":sm_site_member_classification_detail_id",$admin_classification_input);
	$user_association_record_check_q->bindValue(":valid_to_date",$valid_to_date_input);
	$user_association_record_check_q->bindValue(":is_active_status",$is_active_status_input);
	$user_association_record_check_q->execute();

	if($user_association_record_check_q->rowCount() > 0) {
		$user_association_record_check_q_result = $rest_register_check_q->fetch();
	     return $user_association_record_check_q_result;

	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;

}
function user_association_record_insert($last_inserted_id,$sm_classification_detail_id_input,$valid_from_date_input){
	global $dbcon;
	$constructed_array = array();
	$valid_to_date_input = 'present';
	$is_active_status_input = '1';

	$user_association_record_insert_sql = "INSERT INTO `sm_site_member_classification_associations`(`sm_memb_id`, `sm_site_member_classification_detail_id`, `valid_from_date`, `valid_to_date`, `is_active_status`) VALUES (:sm_memb_id, :sm_site_member_classification_detail_id, :valid_from_date, :valid_to_date, :is_active_status)";
	$user_association_record_insert_q = $dbcon->prepare($user_association_record_insert_sql);
	$user_association_record_insert_q->bindValue(":sm_memb_id",$last_inserted_id);
	$user_association_record_insert_q->bindValue(":sm_site_member_classification_detail_id",$sm_classification_detail_id_input);
	$user_association_record_insert_q->bindValue(":valid_from_date",$valid_from_date_input);
	$user_association_record_insert_q->bindValue(":valid_to_date",$valid_to_date_input);
    $user_association_record_insert_q->bindValue(":is_active_status",$is_active_status_input);
		if ($user_association_record_insert_q->execute()) {

            $last_ass_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("ass record inserted successfully");
			return $last_ass_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
			return "";
	    }
}
function candidate_rel_additional_details_information_insert($last_inserted_id,$company_id_input,$profile_source_id_input,$profile_source_name_input,$profile_source_seo_name_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch){
	global $dbcon;
	$constructed_array = array();
	
	$candidate_rel_additional_details_information_insert_sql = "INSERT INTO `candidate_rel_additional_info`(`sm_memb_id`,`company_id`, `profile_source_id`, `profile_source_name`, `profile_source_seo_name`, `added_by_user_id`, `added_date_time`, `added_date_time_epoch`)VALUES (:sm_memb_id,:company_id,:profile_source_id,:profile_source_name,:profile_source_seo_name,:added_by_user_id,:added_date_time,:added_date_time_epoch)";
	$candidate_rel_additional_details_information_insert_q = $dbcon->prepare($candidate_rel_additional_details_information_insert_sql);
	$candidate_rel_additional_details_information_insert_q->bindValue(":sm_memb_id",$last_inserted_id);
	$candidate_rel_additional_details_information_insert_q->bindValue(":company_id",$company_id_input);
	$candidate_rel_additional_details_information_insert_q->bindValue(":profile_source_id",$profile_source_id_input);
	$candidate_rel_additional_details_information_insert_q->bindValue(":profile_source_name",$profile_source_name_input);
	$candidate_rel_additional_details_information_insert_q->bindValue(":profile_source_seo_name",$profile_source_seo_name_input);
    $candidate_rel_additional_details_information_insert_q->bindValue(":added_by_user_id",$ea_extracted_jwt_token_sub);
	$candidate_rel_additional_details_information_insert_q->bindValue(":added_date_time",$event_datetime);
	$candidate_rel_additional_details_information_insert_q->bindValue(":added_date_time_epoch",$current_epoch);
		if ($candidate_rel_additional_details_information_insert_q->execute()) {

            $last_ass_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("ass record inserted successfully");
			return $last_ass_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
			return "";
	    }
}


function user_association_record_update($sm_site_member_classification_association_id,$sm_classification_detail_id_input){
	global $dbcon;
	$constructed_array = array();
	$is_active_status_input = '1';
	$valid_to_date_input = 'present';

	$user_association_record_insert_sql = "UPDATE `sm_site_member_classification_associations` SET `sm_site_member_classification_detail_id`=:sm_site_member_classification_detail_id, `valid_to_date`=:valid_to_date, `is_active_status`=:is_active_status WHERE `sm_site_member_classification_association_id`=:sm_site_member_classification_association_id";
	$user_association_record_insert_q = $dbcon->prepare($user_association_record_insert_sql);
	$user_association_record_insert_q->bindValue(":sm_site_member_classification_association_id",$sm_site_member_classification_association_id);
	$user_association_record_insert_q->bindValue(":sm_site_member_classification_detail_id",$sm_classification_detail_id_input);
	$user_association_record_insert_q->bindValue(":valid_to_date",$valid_to_date_input);
    $user_association_record_insert_q->bindValue(":is_active_status",$is_active_status_input);
		if ($user_association_record_insert_q->execute()) {


			$eventLog->log("ass record updated successfully");
			return true;

		} else {
		    $eventLog->log("Error occurred during process. Please try again");
				return false;
	    }
}

function admin_user_register_basic_details_insert($salutation_input,$firstname_input,$middlename_input,$lastname_input,$mobile_number_input,$email_id_input,$alternate_email_id_input,$company_id_input,$user_type_input,$admin_level_input, $user_role_input, $added_by_admin_user_id_input, $added_by_admin_user_firstname_input, $added_by_admin_user_middlename_input, $added_by_admin_user_lastname_input, $added_date_time_input, $added_date_time_epoch_input, $sm_user_email_act_code_input, $sm_user_status_input) {
	global $dbcon;
	$register_details_sql = "INSERT INTO `site_members` (sm_salutation,sm_firstname,sm_middlename,sm_lastname,sm_mobile,sm_email,sm_alternate_email,company_id,sm_user_type,sm_admin_level,sm_user_role,added_by_admin_user_id,added_by_admin_user_firstname,added_by_admin_user_middlename,added_by_admin_user_lastname,added_date_time,added_date_time_epoch,sm_user_email_act_code,sm_user_status) VALUES(:sm_salutation, :sm_firstname, :sm_middlename, :sm_lastname, :sm_mobile, :sm_email, :sm_alternate_email, :company_id, :sm_user_type, :sm_admin_level, :sm_user_role, :added_by_admin_user_id, :added_by_admin_user_firstname, :added_by_admin_user_middlename, :added_by_admin_user_lastname, :added_date_time, :added_date_time_epoch, :sm_user_email_act_code, :sm_user_status)";
	$register_details_q = $dbcon->prepare($register_details_sql);
	$register_details_q->bindValue(":sm_salutation",$salutation_input);
	$register_details_q->bindValue(":sm_firstname",$firstname_input);
	$register_details_q->bindValue(":sm_middlename",$middlename_input);
	$register_details_q->bindValue(":sm_lastname",$lastname_input);
	$register_details_q->bindValue(":sm_mobile",$mobile_number_input);
    $register_details_q->bindValue(":sm_email",$email_id_input);
	$register_details_q->bindValue(":sm_alternate_email",$alternate_email_id_input);
	$register_details_q->bindValue(":company_id",$company_id_input);
	$register_details_q->bindValue(":sm_user_type",$user_type_input);
	$register_details_q->bindValue(":sm_admin_level",$admin_level_input);
	$register_details_q->bindValue(":sm_user_role",$user_role_input);
	$register_details_q->bindValue(":added_by_admin_user_id",$added_by_admin_user_id_input);
	$register_details_q->bindValue(":added_by_admin_user_firstname",$added_by_admin_user_firstname_input);
	$register_details_q->bindValue(":added_by_admin_user_middlename",$added_by_admin_user_middlename_input);
	$register_details_q->bindValue(":added_by_admin_user_lastname",$added_by_admin_user_lastname_input);
	$register_details_q->bindValue(":added_date_time",$added_date_time_input);
	$register_details_q->bindValue(":added_date_time_epoch",$added_date_time_epoch_input);
	$register_details_q->bindValue(":sm_user_email_act_code",$sm_user_email_act_code_input);
	$register_details_q->bindValue(":sm_user_status",$sm_user_status_input);

		if ($register_details_q->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");
			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }
}
function admin_user_register_basic_details_insert_value($email_id_input,$sm_mobile_phone_country_code_input,$mobile_number_input,$alternate_email_id_input,$salutation_input,$firstname_input,$middlename_input,$lastname_input,$dob_input,$address_input,$city_input,$state_input,$country_input,$designation_input,$company_id_input,$user_type_input,$admin_user_level, $user_role_input, $added_by_admin_user_id_input, $added_by_admin_user_firstname_input, $added_by_admin_user_middlename_input, $added_by_admin_user_lastname_input, $added_date_time_input, $added_date_time_epoch_input, $sm_user_email_act_code_input, $sm_user_status_input){
	global $dbcon;
	
	$register_details_sql = "INSERT INTO `site_members` (sm_email,sm_mobile_phone_country_code,sm_mobile,sm_alternate_email,sm_salutation,sm_firstname,sm_middlename,sm_lastname,sm_dob,sm_address,sm_city,sm_state,sm_country,sm_designation,company_id,sm_user_type,sm_admin_level,sm_user_role,added_by_admin_user_id,added_by_admin_user_firstname,added_by_admin_user_middlename,added_by_admin_user_lastname,added_date_time,added_date_time_epoch,sm_user_email_act_code,sm_user_status) VALUES(:sm_email,:sm_mobile_phone_country_code,:sm_mobile,:sm_alternate_email,:sm_salutation, :sm_firstname, :sm_middlename, :sm_lastname,:sm_dob,:sm_address,:sm_city,:sm_state,:sm_country,:sm_designation,:company_id, :sm_user_type, :sm_admin_level, :sm_user_role, :added_by_admin_user_id, :added_by_admin_user_firstname, :added_by_admin_user_middlename, :added_by_admin_user_lastname, :added_date_time, :added_date_time_epoch, :sm_user_email_act_code, :sm_user_status)";
	$register_details_q = $dbcon->prepare($register_details_sql);
	$register_details_q->bindValue(":sm_email",$email_id_input);
	$register_details_q->bindValue(":sm_mobile_phone_country_code",$sm_mobile_phone_country_code_input);
	$register_details_q->bindValue(":sm_mobile",$mobile_number_input);
	$register_details_q->bindValue(":sm_alternate_email",$alternate_email_id_input);
	$register_details_q->bindValue(":sm_salutation",$salutation_input);
	$register_details_q->bindValue(":sm_firstname",$firstname_input);
	$register_details_q->bindValue(":sm_middlename",$middlename_input);
	$register_details_q->bindValue(":sm_lastname",$lastname_input);
	$register_details_q->bindValue(":sm_dob",$dob_input);
	$register_details_q->bindValue(":sm_address",$address_input);
    $register_details_q->bindValue(":sm_city",$city_input);
	$register_details_q->bindValue(":sm_state",$state_input);
	$register_details_q->bindValue(":sm_country",$country_input);
	$register_details_q->bindValue(":sm_designation",$designation_input);
	$register_details_q->bindValue(":company_id",$company_id_input);
	$register_details_q->bindValue(":sm_user_type",$user_type_input);
	$register_details_q->bindValue(":sm_admin_level",$admin_user_level);
	$register_details_q->bindValue(":sm_user_role",$user_role_input);
	$register_details_q->bindValue(":added_by_admin_user_id",$added_by_admin_user_id_input);
	$register_details_q->bindValue(":added_by_admin_user_firstname",$added_by_admin_user_firstname_input);
	$register_details_q->bindValue(":added_by_admin_user_middlename",$added_by_admin_user_middlename_input);
	$register_details_q->bindValue(":added_by_admin_user_lastname",$added_by_admin_user_lastname_input);
	$register_details_q->bindValue(":added_date_time",$added_date_time_input);
	$register_details_q->bindValue(":added_date_time_epoch",$added_date_time_epoch_input);
	$register_details_q->bindValue(":sm_user_email_act_code",$sm_user_email_act_code_input);
	$register_details_q->bindValue(":sm_user_status",$sm_user_status_input);

		if ($register_details_q->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");
			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }
}

/*function get_candidate_list($company_id_input, $user_type_input) {
	global $dbcon;
	$constructed_array = array();

	$user_type_input = '%'.$user_type_input.'%';

	if ($company_id_input == "") {
		$get_candidate_list_sql = "SELECT * FROM `site_members` WHERE `sm_user_type` LIKE :sm_user_type ORDER BY `sm_memb_id` ASC";
		$get_candidate_list_select_query = $dbcon->prepare($get_candidate_list_sql);
	} else {
		$get_candidate_list_sql = "SELECT * FROM `site_members` WHERE `company_id` =:company_id AND `sm_user_type` LIKE :sm_user_type ORDER BY `sm_memb_id` ASC";
		$get_candidate_list_select_query = $dbcon->prepare($get_candidate_list_sql);
		$get_candidate_list_select_query->bindValue(":company_id",$company_id_input);
	}//close of else of if ($company_id_input == "") {

	$get_candidate_list_select_query->bindValue(":sm_user_type",$user_type_input);
	$get_candidate_list_select_query->execute();

	if($get_candidate_list_select_query->rowCount() > 0) {
		$get_candidate_list_result = $get_candidate_list_select_query->fetchAll();

		$candidates_list_array = array();
		foreach ($get_candidate_list_result as $get_candidate_list_result_row) {
			$candidate_row_array = array();

			$candidate_row_array["sm_memb_id"] = $get_candidate_list_result_row["sm_memb_id"];
			$candidate_row_array["sm_email"] = $get_candidate_list_result_row["sm_email"];
			$candidate_row_array["sm_mobile"] = $get_candidate_list_result_row["sm_mobile"];
			$candidate_row_array["sm_alternate_email"] = $get_candidate_list_result_row["sm_alternate_email"];
			$candidate_row_array["sm_salutation"] = $get_candidate_list_result_row["sm_salutation"];
			$candidate_row_array["sm_firstname"] = $get_candidate_list_result_row["sm_firstname"];
			$candidate_row_array["sm_middlename"] = $get_candidate_list_result_row["sm_middlename"];
			$candidate_row_array["sm_lastname"] = $get_candidate_list_result_row["sm_lastname"];
			$candidate_row_array["sm_user_status"] = $get_candidate_list_result_row["sm_user_status"];

			array_push($candidates_list_array,$candidate_row_array);

		}//close of foreach ($get_candidate_list_result as $get_candidate_list_result_row) {
		return $candidates_list_array;

	}//close of if($get_candidate_list_select_query->rowCount() > 0) {
	return $constructed_array;
}*/

/*function get_candidate_list($company_id_input) {
	global $dbcon;
	$constructed_array = array();


	if ($company_id_input == "") {
		$get_candidate_list_sql = "SELECT * FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE ssmca.sm_site_member_classification_detail_id = '1' ORDER BY sm.sm_memb_id ASC";
		$get_candidate_list_select_query = $dbcon->prepare($get_candidate_list_sql);
	} else {
		$get_candidate_list_sql = "SELECT * FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE ssmca.sm_site_member_classification_detail_id = '1' AND sm.company_id =:company_id ORDER BY sm.sm_memb_id ASC";
		$get_candidate_list_select_query = $dbcon->prepare($get_candidate_list_sql);
		$get_candidate_list_select_query->bindValue(":company_id",$company_id_input);
	}//close of else of if ($company_id_input == "") {


	$get_candidate_list_select_query->execute();

	if($get_candidate_list_select_query->rowCount() > 0) {
		$get_candidate_list_result = $get_candidate_list_select_query->fetchAll();

		$candidates_list_array = array();
		foreach ($get_candidate_list_result as $get_candidate_list_result_row) {
			$candidate_row_array = array();

			$candidate_row_array["sm_memb_id"] = $get_candidate_list_result_row["sm_memb_id"];
			$candidate_row_array["sm_email"] = $get_candidate_list_result_row["sm_email"];
			$candidate_row_array["sm_mobile"] = $get_candidate_list_result_row["sm_mobile"];
			$candidate_row_array["sm_alternate_email"] = $get_candidate_list_result_row["sm_alternate_email"];
			$candidate_row_array["sm_salutation"] = $get_candidate_list_result_row["sm_salutation"];
			$candidate_row_array["sm_firstname"] = $get_candidate_list_result_row["sm_firstname"];
			$candidate_row_array["sm_middlename"] = $get_candidate_list_result_row["sm_middlename"];
			$candidate_row_array["sm_lastname"] = $get_candidate_list_result_row["sm_lastname"];
			$candidate_row_array["sm_user_status"] = $get_candidate_list_result_row["sm_user_status"];

			/*$candidate_confirmed_awaiting_confirmation_documents_count_result = candidate_confirmed_awaiting_confirmation_documents_count_based_on_sm_memb_id($candidate_row_array["sm_memb_id"]);

			if ($candidate_confirmed_awaiting_confirmation_documents_count_result["awaiting_confirmation_documents_count"] > 0) {
				$candidate_row_array["awaiting_confirmation_documents_count"] = $candidate_confirmed_awaiting_confirmation_documents_count_result["awaiting_confirmation_documents_count"];
			} else {
				$candidate_row_array["awaiting_confirmation_documents_count"] = "0";
			}//close of else of if ($candidate_confirmed_awaiting_confirmation_documents_count_result["awaiting_confirmation_documents_count"] > 0) {

			if ($candidate_confirmed_awaiting_confirmation_documents_count_result["confirmed_documents_count"] > 0) {
				$candidate_row_array["confirmed_documents_count"] = $candidate_confirmed_awaiting_confirmation_documents_count_result["confirmed_documents_count"];
			} else {
				$candidate_row_array["confirmed_documents_count"] = "0";
			}//close of else of if ($candidate_confirmed_awaiting_confirmation_documents_count_result["confirmed_documents_count"] > 0) {


			$constructed_array[] = $candidate_row_array;



		}//close of foreach ($get_candidate_list_result as $get_candidate_list_result_row) {
		return $constructed_array;

	}//close of if($get_candidate_list_select_query->rowCount() > 0) {
	return $constructed_array;
}*/

function candidate_confirmed_awaiting_confirmation_documents_count_based_on_sm_memb_id($user_id_input) {
	global $dbcon, $ea_extracted_jwt_token_sub, $current_epoch, $expiring_link_lifetime, $expiring_link_secret_key, $expiring_link_hash_algorithm, $site_hostname_value;
	$constructed_array = array();
	$candidate_file_upload_current_status_check_list_array = array();
	$candidate_file_upload_current_status_list_array = array();
	$uploaded_file_url = "";
	$is_first_visit_from_db = "";
	$personal_info_result = candidate_visa_rel_personal_info_duplicate_check($user_id_input);
	if(count($personal_info_result) > 0) {
	   $is_first_visit_from_db = $personal_info_result["is_first_visit"];

	}

	$candidate_file_upload_current_status_check_sql = "SELECT * FROM `candidate_rel_all_uploaded_visa_documents` WHERE `sm_memb_id`=:sm_memb_id AND `is_active_status`=:is_active_status";
	$candidate_file_upload_current_status_check_q = $dbcon->prepare($candidate_file_upload_current_status_check_sql);
	$candidate_file_upload_current_status_check_q->bindValue(":sm_memb_id",$user_id_input);
	$candidate_file_upload_current_status_check_q->bindValue(":is_active_status","2");
	$candidate_file_upload_current_status_check_q->execute();



	if($candidate_file_upload_current_status_check_q->rowCount() > 0) {

		$candidate_file_upload_current_status_check_result = $candidate_file_upload_current_status_check_q->fetchAll();


		foreach ($candidate_file_upload_current_status_check_result as $get_candidate_file_upload_list_result_row) {
			$candidate_file_upload_current_status_row_array = array();

			$candidate_file_upload_current_status_row_array["crauvd_id"] = $get_candidate_file_upload_list_result_row["crauvd_id"];
			$candidate_file_upload_current_status_row_array["additional_document_ref"] = $get_candidate_file_upload_list_result_row["additional_document_ref"];

			//This File is Confirmed, file_upload_status = 2
			$candidate_file_upload_current_status_row_array["file_upload_status"] = "2";

			//Link Expiry Time
			$link_expiry_time = $current_epoch+$expiring_link_lifetime;//from /app/core/main-config.php

			//Exclude I-94 Form and USA Social Security Number, in the case of Candidate USA First Visit
			if((($is_first_visit_from_db == '1') && ($candidate_file_upload_current_status_row_array["additional_document_ref"] != "I-94 Form") && ($candidate_file_upload_current_status_row_array["additional_document_ref"] != "USA Social Security Number")) || ($is_first_visit_from_db == '0')) {
			    array_push($candidate_file_upload_current_status_check_list_array,$candidate_file_upload_current_status_row_array);
			}//close of if((($is_first_visit_from_db == '1') && ($candidate_file_upload_current_status_row_array["additional_document_ref"] != "I-94 Form") && ($candidate_file_upload_current_status_row_array["additional_document_ref"] != "USA Social Security Number")) || ($is_first_visit_from_db == '0')) {


		}//close of foreach ($get_admin_users_list_result as $get_admin_users_list_result_row) {
	    //return $candidate_file_upload_current_status_check_list_array;


	}

	$candidate_file_upload_current_status_sql = "SELECT * FROM `candidate_rel_all_uploaded_visa_documents` WHERE `sm_memb_id`=:sm_memb_id AND `generated_filename` IS NOT NULL AND `is_active_status`=:is_active_status";
	$candidate_file_upload_current_status_q = $dbcon->prepare($candidate_file_upload_current_status_sql);
	$candidate_file_upload_current_status_q->bindValue(":sm_memb_id",$user_id_input);
	$candidate_file_upload_current_status_q->bindValue(":is_active_status","1");
	$candidate_file_upload_current_status_q->execute();

	if($candidate_file_upload_current_status_q->rowCount() > 0) {

		$candidate_file_upload_current_status_result = $candidate_file_upload_current_status_q->fetchAll();



		foreach ($candidate_file_upload_current_status_result as $get_candidate_file_upload_list_result_row) {
			$candidate_file_upload_status_row_array = array();

			$candidate_file_upload_status_row_array["crauvd_id"] = $get_candidate_file_upload_list_result_row["crauvd_id"];
			$candidate_file_upload_status_row_array["additional_document_ref"] = $get_candidate_file_upload_list_result_row["additional_document_ref"];

			//Link Expiry Time
			$link_expiry_time = $current_epoch+$expiring_link_lifetime;//from /app/core/main-config.php

			//Exclude I-94 Form and USA Social Security Number, in the case of Candidate USA First Visit
			if((($is_first_visit_from_db == '1') && ($candidate_file_upload_status_row_array["additional_document_ref"] != "I-94 Form") && ($candidate_file_upload_status_row_array["additional_document_ref"] != "USA Social Security Number")) || ($is_first_visit_from_db == '0')) {
			    array_push($candidate_file_upload_current_status_list_array,$candidate_file_upload_status_row_array);
			}//close of if((($is_first_visit_from_db == '1') && ($candidate_file_upload_status_row_array["additional_document_ref"] != "I-94 Form") && ($candidate_file_upload_status_row_array["additional_document_ref"] != "USA Social Security Number")) || ($is_first_visit_from_db == '0')) {


		}//close of foreach ($get_admin_users_list_result as $get_admin_users_list_result_row) {
	    //return $candidate_file_upload_current_status_check_list_array;


	}

	$confirmed_documents_count = count($candidate_file_upload_current_status_check_list_array);
	$awaiting_confirmation_documents_count = count($candidate_file_upload_current_status_list_array);

	$constructed_array["awaiting_confirmation_documents_count"] = $awaiting_confirmation_documents_count;
	$constructed_array["confirmed_documents_count"] = $confirmed_documents_count;

	//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;

}

/*function 
($company_id_input, $user_type_input) {
	global $dbcon;
	$constructed_array = array();

	$user_type_input = '%'.$user_type_input.'%';

	if ($company_id_input == "") {
		$get_admin_users_list_sql = "SELECT * FROM `site_members` WHERE `sm_user_type` LIKE :sm_user_type ORDER BY `sm_memb_id` ASC";
		$get_admin_users_list_select_query = $dbcon->prepare($get_admin_users_list_sql);
	} else {
		$get_admin_users_list_sql = "SELECT * FROM `site_members` WHERE `company_id` =:company_id AND `sm_user_type` LIKE :sm_user_type ORDER BY `sm_memb_id` ASC";
		$get_admin_users_list_select_query = $dbcon->prepare($get_admin_users_list_sql);
		$get_admin_users_list_select_query->bindValue(":company_id",$company_id_input);
	}//close of else of if ($company_id_input == "") {

	$get_admin_users_list_select_query->bindValue(":sm_user_type",$user_type_input);
	$get_admin_users_list_select_query->execute();

	if($get_admin_users_list_select_query->rowCount() > 0) {
		$get_admin_users_list_result = $get_admin_users_list_select_query->fetchAll();

		$admin_users_list_array = array();
		foreach ($get_admin_users_list_result as $get_admin_users_list_result_row) {
			$admin_user_row_array = array();

			$admin_user_row_array["sm_memb_id"] = $get_admin_users_list_result_row["sm_memb_id"];
			$admin_user_row_array["sm_email"] = $get_admin_users_list_result_row["sm_email"];
			$admin_user_row_array["sm_mobile"] = $get_admin_users_list_result_row["sm_mobile"];
			$admin_user_row_array["sm_alternate_email"] = $get_admin_users_list_result_row["sm_alternate_email"];
			$admin_user_row_array["sm_salutation"] = $get_admin_users_list_result_row["sm_salutation"];
			$admin_user_row_array["sm_firstname"] = $get_admin_users_list_result_row["sm_firstname"];
			$admin_user_row_array["sm_middlename"] = $get_admin_users_list_result_row["sm_middlename"];
			$admin_user_row_array["sm_lastname"] = $get_admin_users_list_result_row["sm_lastname"];
			$admin_user_row_array["sm_user_status"] = $get_admin_users_list_result_row["sm_user_status"];

			array_push($admin_users_list_array,$admin_user_row_array);

		}//close of foreach ($get_admin_users_list_result as $get_admin_users_list_result_row) {
	    return $admin_users_list_array;

	}//close of if($get_admin_users_list_select_query->rowCount() > 0) {
	return $constructed_array;
}
*/

function get_admin_users_list($company_id_input, $user_type_input) {
	global $dbcon;
	$constructed_array = array();

	$user_type_input = '%'.$user_type_input.'%';
	$valid_to_date = '%present%';

	if ($company_id_input == "") {
		//$get_admin_users_list_sql = "SELECT * FROM `site_members` WHERE `sm_user_type` LIKE :sm_user_type ORDER BY `sm_memb_id` ASC";
		$get_admin_users_list_sql = "SELECT * FROM site_members sm LEFT JOIN sm_site_member_classification_associations ssmca ON ssmca.sm_memb_id = sm.sm_memb_id WHERE sm.sm_user_type LIKE :sm_user_type AND ssmca.valid_to_date LIKE :valid_to_date ORDER BY sm.sm_memb_id ASC";
		$get_admin_users_list_select_query = $dbcon->prepare($get_admin_users_list_sql);
	} else {
		//$get_admin_users_list_sql = "SELECT * FROM `site_members` WHERE `company_id` =:company_id AND `sm_user_type` LIKE :sm_user_type ORDER BY `sm_memb_id` ASC";
		$get_admin_users_list_sql = "SELECT * FROM site_members sm LEFT JOIN sm_site_member_classification_associations ssmca ON ssmca.sm_memb_id = sm.sm_memb_id WHERE sm.company_id =:company_id AND sm.sm_user_type LIKE :sm_user_type AND ssmca.valid_to_date LIKE :valid_to_date ORDER BY sm.sm_memb_id ASC";
		$get_admin_users_list_select_query = $dbcon->prepare($get_admin_users_list_sql);
		$get_admin_users_list_select_query->bindValue(":company_id",$company_id_input);
	}//close of else of if ($company_id_input == "") {

	$get_admin_users_list_select_query->bindValue(":sm_user_type",$user_type_input);
	$get_admin_users_list_select_query->bindValue(":valid_to_date",$valid_to_date);
	$get_admin_users_list_select_query->execute();

	if($get_admin_users_list_select_query->rowCount() > 0) {
		$get_admin_users_list_result = $get_admin_users_list_select_query->fetchAll();

		$admin_users_list_array = array();
		foreach ($get_admin_users_list_result as $get_admin_users_list_result_row) {
			$admin_user_row_array = array();

			$admin_user_row_array["sm_memb_id"] = $get_admin_users_list_result_row["sm_memb_id"];
			$admin_user_row_array["sm_email"] = $get_admin_users_list_result_row["sm_email"];
			$admin_user_row_array["sm_mobile"] = $get_admin_users_list_result_row["sm_mobile"];
			$admin_user_row_array["sm_alternate_email"] = $get_admin_users_list_result_row["sm_alternate_email"];
			$admin_user_row_array["sm_salutation"] = $get_admin_users_list_result_row["sm_salutation"];
			$admin_user_row_array["sm_firstname"] = $get_admin_users_list_result_row["sm_firstname"];
			$admin_user_row_array["sm_middlename"] = $get_admin_users_list_result_row["sm_middlename"];
			$admin_user_row_array["sm_lastname"] = $get_admin_users_list_result_row["sm_lastname"];
			$admin_user_row_array["sm_user_status"] = $get_admin_users_list_result_row["sm_user_status"];

			$admin_user_row_array["sm_site_member_classification_detail_id"] = $get_admin_users_list_result_row["sm_site_member_classification_detail_id"];



			array_push($admin_users_list_array,$admin_user_row_array);

		}//close of foreach ($get_admin_users_list_result as $get_admin_users_list_result_row) {
	    return $admin_users_list_array;

	}//close of if($get_admin_users_list_select_query->rowCount() > 0) {
	return $constructed_array;
}

function get_companies_list($company_status_input) {

    global $dbcon;

	$constructed_array = array();
		$companies_list_get_sql = "SELECT * FROM `companies` WHERE `is_active_status`=:is_active_status";

		$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
		$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
		$companies_list_get_select_query->execute();

	if($companies_list_get_select_query->rowCount() > 0) {
	    $companies_list_get_select_query_result = $companies_list_get_select_query->fetchAll();
	    //print_r($user_classification_details_get_select_query_result);

		foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

			$temp_row_array = array();
		    $temp_row_array["company_id"] = $companies_list_get_select_query_result_row["company_id"];
		    $temp_row_array["company_name"] = $companies_list_get_select_query_result_row["company_name"];
			$temp_row_array["company_seo_name"] = $companies_list_get_select_query_result_row["company_seo_name"];
		    $temp_row_array["company_brand_name"] = $companies_list_get_select_query_result_row["company_brand_name"];
		    $temp_row_array["company_status"] = $companies_list_get_select_query_result_row["is_active_status"];

		    $constructed_array[] = $temp_row_array;
	    }//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

		return $constructed_array;
	}
	return $constructed_array;
}

/* function get_companies_list_with_pagination_inputs($company_status_input, $records_after_input, $number_of_records_input) {

    global $dbcon, $default_number_of_records_pagination; //app/core/main-config.php

	$constructed_array = array();

	if (($records_after_input == "") && ($number_of_records_input == "")) {
		//Give All Data

		if (is_null($company_status_input)) {
			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$companies_list_get_sql = "SELECT * FROM `companies`";

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			$companies_list_get_select_query->execute();

		} else {
			//Give List of Companies, based on value of is_active_status
			$companies_list_get_sql = "SELECT * FROM `companies` WHERE `is_active_status`=:is_active_status";

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->execute();

		}//close of else of if (is_null($company_status_input)) {

	} else if (($records_after_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than records_after_input, till the number of records, as defined in the $default_number_of_records_pagination

		if (is_null($company_status_input)) {
			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$companies_list_get_sql = "SELECT * FROM `companies` WHERE `company_id` >:company_id ORDER BY `company_id` ASC LIMIT :number_of_records";

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			$companies_list_get_select_query->bindValue(":company_id",$records_after_input);
			$companies_list_get_select_query->bindValue(":number_of_records",$default_number_of_records_pagination);
			$companies_list_get_select_query->execute();

		} else {
			//Give List of Companies, based on value of is_active_status
			$companies_list_get_sql = "SELECT * FROM `companies` WHERE `is_active_status`=:is_active_status AND `company_id` >:company_id ORDER BY `company_id` ASC LIMIT :number_of_records";

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->bindValue(":company_id",$records_after_input);
			$companies_list_get_select_query->bindValue(":number_of_records",$default_number_of_records_pagination);
			$companies_list_get_select_query->execute();

		}//close of else of if (is_null($company_status_input)) {




	} else if (($records_after_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records


		if (is_null($company_status_input)) {
			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$companies_list_get_sql = "SELECT * FROM `companies` ORDER BY `company_id` ASC LIMIT :number_of_records";

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			$companies_list_get_select_query->bindValue(":number_of_records",$number_of_records_input);
			$companies_list_get_select_query->execute();

		} else {
			//Give List of Companies, based on value of is_active_status
			$companies_list_get_sql = "SELECT * FROM `companies` WHERE `is_active_status`=:is_active_status ORDER BY `company_id` ASC LIMIT :number_of_records";

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->bindValue(":number_of_records",$number_of_records_input);
			$companies_list_get_select_query->execute();

		}//close of else of if (is_null($company_status_input)) {



	} else {
		//Give Data, from given Record, till the requested number of records
		if (is_null($company_status_input)) {
			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$companies_list_get_sql = "SELECT * FROM `companies` WHERE `company_id` > :company_id ORDER BY `company_id` ASC LIMIT :number_of_records";

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			$companies_list_get_select_query->bindValue(":company_id",$records_after_input);
			$companies_list_get_select_query->bindValue(":number_of_records",$number_of_records_input);
			$companies_list_get_select_query->execute();

		} else {
			//Give List of Companies, based on value of is_active_status
			$companies_list_get_sql = "SELECT * FROM `companies` WHERE `is_active_status`=:is_active_status AND `company_id` > :company_id ORDER BY `company_id` ASC LIMIT :number_of_records";

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->bindValue(":company_id",$records_after_input);
			$companies_list_get_select_query->bindValue(":number_of_records",$number_of_records_input);
			$companies_list_get_select_query->execute();

		}//close of else of if (is_null($company_status_input)) {

	}//close of else of if (($records_after_input == "") && ($number_of_records_input == "")) {


	if($companies_list_get_select_query->rowCount() > 0) {
	    $companies_list_get_select_query_result = $companies_list_get_select_query->fetchAll();
	    //print_r($user_classification_details_get_select_query_result);

		foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

			$temp_row_array = array();
		    $temp_row_array["company_id"] = $companies_list_get_select_query_result_row["company_id"];
		    $temp_row_array["company_name"] = $companies_list_get_select_query_result_row["company_name"];
			$temp_row_array["company_seo_name"] = $companies_list_get_select_query_result_row["company_seo_name"];
		    $temp_row_array["company_brand_name"] = $companies_list_get_select_query_result_row["company_brand_name"];
		    $temp_row_array["company_status"] = $companies_list_get_select_query_result_row["is_active_status"];

		    $constructed_array[] = $temp_row_array;
	    }//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

		return $constructed_array;
	}
	return $constructed_array;
} */

/*IN PROGRESS BY RAGHU 06-05-2019 function get_companies_list_with_pagination_inputs($company_status_input, $records_after_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input) {

    global $dbcon, $default_number_of_records_pagination; //app/core/main-config.php

	$constructed_array = array();

	if (!is_null($search_criteria_input)) {
		//When Search Criteria input is not empty
		$search_criteria_variable = "%" . $search_criteria_input . "%";
		//$search_criteria_in_query = "(`company_name` LIKE '%info%') OR (`company_seo_name` LIKE '%info%') OR (`company_brand_name` LIKE '%info%') OR (`is_active_status` LIKE '%info%')";

		$search_criteria_in_query_with_where_keyword = " WHERE (`company_id` LIKE :company_id_search_keyword) OR (`company_name` LIKE :company_name_search_keyword) OR (`company_seo_name` LIKE :company_seo_name_search_keyword) OR (`company_brand_name` LIKE :company_brand_name_search_keyword) OR (`is_active_status` LIKE :is_active_status_search_keyword) ";

		$search_criteria_in_query_without_where_keyword = " AND (`company_id` LIKE :company_id_search_keyword) OR (`company_name` LIKE :company_name_search_keyword) OR (`company_seo_name` LIKE :company_seo_name_search_keyword) OR (`company_brand_name` LIKE :company_brand_name_search_keyword) OR (`is_active_status` LIKE :is_active_status_search_keyword) ";

	} else {
		//When Search Criteria input is empty
		$search_criteria_in_query_with_where_keyword = " WHERE ";

		$search_criteria_in_query_without_where_keyword = "";

	}//close of else of if (!is_null($search_criteria_input)) {


	if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {

		if (!is_null($sort_field_input)) {
			//When the Sort Field input is not empty


			if ($sort_field_input == "company_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_id` " . $sort_order_input;

			} else if ($sort_field_input == "company_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_name` " . $sort_order_input;

			} else if ($sort_field_input == "company_seo_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_seo_name` " . $sort_order_input;

			} else if ($sort_field_input == "company_brand_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_brand_name` " . $sort_order_input;

			} else if ($sort_field_input == "company_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `is_active_status` " . $sort_order_input;

			} else {
				//When Sort Field input, is not among the expected values, use primary key, with provided Sorting Order input
				$sort_details_in_query = " ORDER BY `company_id` " . $sort_order_input;



			}


		} else {
			//When the Sort Field input is empty, primary key has to be taken, with provided Sorting Order input
			$sort_details_in_query = " ORDER BY `company_id` " . $sort_order_input;

		}//close of if (!is_null($sort_field_input)) {


	} else {
		//When the Sort Field input is empty, primary key has to be taken, with ASC Order, by default
		$sort_details_in_query = " ORDER BY `company_id` ASC ";

	}//close of else of if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {




	if (($records_after_input == "") && ($number_of_records_input == "")) {
		//Give All Data

		if (is_null($company_status_input)) {

			if (!is_null($search_criteria_input)) {
				//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
				$companies_list_get_sql = "SELECT * FROM `companies`" . $search_criteria_in_query_with_where_keyword . $sort_details_in_query;

				$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
				$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);
				$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
				$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
				$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
				$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
				$companies_list_get_select_query->execute();
			} else {
				//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
				$companies_list_get_sql = "SELECT * FROM `companies`" . $sort_details_in_query;

				$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
				$companies_list_get_select_query->execute();
			}//close of else of if (!is_null($search_criteria_input)) {


		} else {
			if (!is_null($search_criteria_input)) {
				//Give List of Companies, based on value of is_active_status
				$companies_list_get_sql = "SELECT * FROM `companies` WHERE `is_active_status`=:is_active_status" . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;

				$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
				$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);
				$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
				$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
				$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
				$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
				$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
				$companies_list_get_select_query->execute();
			} else {
				//Give List of Companies, based on value of is_active_status
				$companies_list_get_sql = "SELECT * FROM `companies` WHERE `is_active_status`=:is_active_status" . $sort_details_in_query;

				$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
				$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
				$companies_list_get_select_query->execute();
			}//close of else of if (!is_null($search_criteria_input)) {



		}//close of else of if (is_null($company_status_input)) {

	} else if (($records_after_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than records_after_input, till the number of records, as defined in the $default_number_of_records_pagination

		if (is_null($company_status_input)) {
			//if (!is_null($search_criteria_input)) {

			//} else {

			//}//close of else of if (!is_null($search_criteria_input)) {

			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$companies_list_get_sql = "SELECT * FROM `companies` WHERE `company_id` >:company_id ORDER BY `company_id` ASC LIMIT :number_of_records";

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			$companies_list_get_select_query->bindValue(":company_id",$records_after_input);
			$companies_list_get_select_query->bindValue(":number_of_records",$default_number_of_records_pagination);
			$companies_list_get_select_query->execute();

		} else {
			//Give List of Companies, based on value of is_active_status
			$companies_list_get_sql = "SELECT * FROM `companies` WHERE `is_active_status`=:is_active_status AND `company_id` >:company_id ORDER BY `company_id` ASC LIMIT :number_of_records";

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->bindValue(":company_id",$records_after_input);
			$companies_list_get_select_query->bindValue(":number_of_records",$default_number_of_records_pagination);
			$companies_list_get_select_query->execute();

		}//close of else of if (is_null($company_status_input)) {




	} else if (($records_after_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records


		if (is_null($company_status_input)) {
			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$companies_list_get_sql = "SELECT * FROM `companies` ORDER BY `company_id` ASC LIMIT :number_of_records";

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			$companies_list_get_select_query->bindValue(":number_of_records",$number_of_records_input);
			$companies_list_get_select_query->execute();

		} else {
			//Give List of Companies, based on value of is_active_status
			$companies_list_get_sql = "SELECT * FROM `companies` WHERE `is_active_status`=:is_active_status ORDER BY `company_id` ASC LIMIT :number_of_records";

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->bindValue(":number_of_records",$number_of_records_input);
			$companies_list_get_select_query->execute();

		}//close of else of if (is_null($company_status_input)) {



	} else {
		//Give Data, from given Record, till the requested number of records
		if (is_null($company_status_input)) {
			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$companies_list_get_sql = "SELECT * FROM `companies` WHERE `company_id` > :company_id ORDER BY `company_id` ASC LIMIT :number_of_records";

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			$companies_list_get_select_query->bindValue(":company_id",$records_after_input);
			$companies_list_get_select_query->bindValue(":number_of_records",$number_of_records_input);
			$companies_list_get_select_query->execute();

		} else {
			//Give List of Companies, based on value of is_active_status
			$companies_list_get_sql = "SELECT * FROM `companies` WHERE `is_active_status`=:is_active_status AND `company_id` > :company_id ORDER BY `company_id` ASC LIMIT :number_of_records";

			$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
			$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->bindValue(":company_id",$records_after_input);
			$companies_list_get_select_query->bindValue(":number_of_records",$number_of_records_input);
			$companies_list_get_select_query->execute();

		}//close of else of if (is_null($company_status_input)) {

	}//close of else of if (($records_after_input == "") && ($number_of_records_input == "")) {


	if($companies_list_get_select_query->rowCount() > 0) {
	    $companies_list_get_select_query_result = $companies_list_get_select_query->fetchAll();
	    //print_r($user_classification_details_get_select_query_result);

		foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

			$temp_row_array = array();
		    $temp_row_array["company_id"] = $companies_list_get_select_query_result_row["company_id"];
		    $temp_row_array["company_name"] = $companies_list_get_select_query_result_row["company_name"];
			$temp_row_array["company_seo_name"] = $companies_list_get_select_query_result_row["company_seo_name"];
		    $temp_row_array["company_brand_name"] = $companies_list_get_select_query_result_row["company_brand_name"];
		    $temp_row_array["company_status"] = $companies_list_get_select_query_result_row["is_active_status"];

		    $constructed_array[] = $temp_row_array;
	    }//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

		return $constructed_array;
	}
	return $constructed_array;
}
*/



function get_company_clients_list_with_pagination_inputs($company_id_input,$client_company_status_input, $page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input) {

    global $dbcon, $default_number_of_records_pagination; //app/core/main-config.php
	global $eventLog;
	$constructed_array = array();
	$eventLog->log("before is_null concept");

	if ($search_criteria_input == "") {
		$search_criteria_input = null;
	}//close of if ($search_criteria_input == "") {


	if (!is_null($search_criteria_input)) {
		//When Search Criteria input is not empty
		$search_criteria_variable = "%" . $search_criteria_input . "%";
		//$search_criteria_in_query = "(`company_name` LIKE '%info%') OR (`company_seo_name` LIKE '%info%') OR (`company_brand_name` LIKE '%info%') OR (`is_active_status` LIKE '%info%')";

		$search_criteria_in_query_with_where_keyword = " WHERE ((`company_client_id` LIKE :company_client_id_search_keyword) OR (`company_id` LIKE :company_id_search_keyword) OR (`client_company_name` LIKE :client_company_name_search_keyword) OR (`client_company_seo_name` LIKE :client_company_seo_name_search_keyword) OR (`client_company_brand_name` LIKE :client_company_brand_name_search_keyword) OR (`client_company_email` LIKE :client_company_email_search_keyword) OR (`client_company_support_email` LIKE :client_company_support_email_search_keyword) OR (`is_active_status` LIKE :is_active_status_search_keyword)) ";
		$eventLog->log("after search criteria concept");
		$search_criteria_in_query_without_where_keyword = " AND ((`company_client_id` LIKE :company_client_id_search_keyword) OR (`company_id` LIKE :company_id_search_keyword) OR (`client_company_name` LIKE :client_company_name_search_keyword) OR (`client_company_seo_name` LIKE :client_company_seo_name_search_keyword) OR (`client_company_brand_name` LIKE :client_company_brand_name_search_keyword) OR (`client_company_email` LIKE :client_company_email_search_keyword) OR (`client_company_support_email` LIKE :client_company_support_email_search_keyword) OR (`is_active_status` LIKE :is_active_status_search_keyword)) ";
	    $eventLog->log("after search criteria without where concept");
	} else {
		//When Search Criteria input is empty
		$search_criteria_in_query_with_where_keyword = " WHERE ";

		$search_criteria_in_query_without_where_keyword = "";

	}//close of else of if (!is_null($search_criteria_input)) {
	$eventLog->log("before sort order concept");

	if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {

		if (!is_null($sort_field_input)) {
			//When the Sort Field input is not empty
			$eventLog->log("null condition  sort concept");

			if ($sort_field_input == "company_client_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_client_id` " . $sort_order_input;

			} else if ($sort_field_input == "company_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_id` " . $sort_order_input;

			} else if ($sort_field_input == "client_company_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `client_company_name` " . $sort_order_input;

			} else if ($sort_field_input == "client_company_seo_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `client_company_seo_name` " . $sort_order_input;

			} else if ($sort_field_input == "client_company_brand_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `client_company_brand_name` " . $sort_order_input;

            } else if ($sort_field_input == "client_company_email") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `client_company_email` " . $sort_order_input;

			}else if ($sort_field_input == "client_company_support_email") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `client_company_support_email` " . $sort_order_input;

			}else if ($sort_field_input == "is_active_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `is_active_status` " . $sort_order_input;

			} else {
				//When Sort Field input, is not among the expected values, use primary key, with provided Sorting Order input
				$sort_details_in_query = " ORDER BY `company_client_id` " . $sort_order_input;


			}
			$eventLog->log("after checking inputs concept");

		} else {
			//When the Sort Field input is empty, primary key has to be taken, with provided Sorting Order input
			$sort_details_in_query = " ORDER BY `company_client_id` " . $sort_order_input;

		}//close of if (!is_null($sort_field_input)) {


	} else {
		//When the Sort Field input is empty, primary key has to be taken, with ASC Order, by default
		$sort_details_in_query = " ORDER BY `company_client_id` ASC ";

	}//close of else of if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {


	$limit_offset_value = 0;

	if (($page_number_input == "") && ($number_of_records_input == "")) {
		//Give All Data
		//No LIMIT Clause in the Query
		$limit_offset_in_query = "";

	} else if (($page_number_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than page_number_input, till the number of records, as defined in the $default_number_of_records_pagination

		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $default_number_of_records_pagination;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$default_number_of_records_pagination;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $default_number_of_records_pagination;
		}//close of else if of if ($page_number_input == "1") {


		$eventLog->log("before page number concept");
	} else if (($page_number_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records
		$limit_offset_in_query = " LIMIT " . $number_of_records_input;

	} else {
		//Give Data, from given Record, till the requested number of records
		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $number_of_records_input;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$number_of_records_input;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $number_of_records_input;
		}//close of else if of if ($page_number_input == "1") {

	}//close of else of if (($page_number_input == "") && ($number_of_records_input == "")) {
	$eventLog->log("before client_company_status concept");



	if (is_null($client_company_status_input)) {
		$eventLog->log("after check client_company_status concept");
		if (!is_null($search_criteria_input)) {
			//Give List of Company clients, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$eventLog->log("before named parameters search_criteria concept");

			//Define Named parameters and corresponding Values
			$company_clients_named_parameters_values_array_input = array(":company_id" => $company_id_input,":company_client_id_search_keyword" => $search_criteria_variable, ":company_id_search_keyword" => $search_criteria_variable, ":client_company_name_search_keyword" => $search_criteria_variable, ":client_company_seo_name_search_keyword" => $search_criteria_variable, ":client_company_brand_name_search_keyword" => $search_criteria_variable,":client_company_email_search_keyword" => $search_criteria_variable,":client_company_support_email_search_keyword" => $search_criteria_variable,":is_active_status_search_keyword" => $search_criteria_variable);
			$eventLog->log("before getting the content details");
			//Get Company clients List Count
			$company_clients_list_count_get_sql = "SELECT COUNT(*) AS count FROM `company_clients` WHERE `company_id`=:company_id " . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;
			$eventLog->log("company_clients_list_count_get_sql-> " .$company_clients_list_count_get_sql);
			$company_clients_list_count_get_select_query = $dbcon->prepare($company_clients_list_count_get_sql);
			$company_clients_list_count_get_select_query->execute($company_clients_named_parameters_values_array_input);
			$eventLog->log("after getting the company_clients list concept");
			//Get Company clients List
			$company_clients_list_get_sql = "SELECT * FROM `company_clients` WHERE `company_id`=:company_id " . $search_criteria_in_query_without_where_keyword . $sort_details_in_query . $limit_offset_in_query;

			$company_clients_list_get_select_query = $dbcon->prepare($company_clients_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->execute();*/
			$company_clients_list_get_select_query->execute($company_clients_named_parameters_values_array_input);

		} else {
			$eventLog->log("else condition of client_company_status concept");
			//Define Named parameters and corresponding Values
			$company_clients_named_parameters_values_array_input = array(":company_id" => $company_id_input);

			//Give List of Companies Count, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$company_clients_list_count_get_sql = "SELECT COUNT(*) AS count FROM `company_clients` WHERE `company_id`=:company_id " . $sort_details_in_query;
			$company_clients_list_count_get_select_query = $dbcon->prepare($company_clients_list_count_get_sql);
			$company_clients_list_count_get_select_query->execute($company_clients_named_parameters_values_array_input);


			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$company_clients_list_get_sql = "SELECT * FROM `company_clients` WHERE `company_id`=:company_id " . $sort_details_in_query . $limit_offset_in_query;
			$company_clients_list_get_select_query = $dbcon->prepare($company_clients_list_get_sql);
			$company_clients_list_get_select_query->execute($company_clients_named_parameters_values_array_input);

		}//close of else of if (!is_null($search_criteria_input)) {


	} else {
		if (!is_null($search_criteria_input)) {
			$eventLog->log("check the search_criteria_input null concept");

			//Define Named parameters and corresponding Values
			$company_clients_named_parameters_values_array_input = array(":company_id" => $company_id_input, ":is_active_status" => $client_company_status_input, ":company_client_id_search_keyword" => $search_criteria_variable, ":company_id_search_keyword" => $search_criteria_variable, ":client_company_name_search_keyword" => $search_criteria_variable, ":client_company_seo_name_search_keyword" => $search_criteria_variable, ":client_company_brand_name_search_keyword" => $search_criteria_variable,":client_company_email_search_keyword" => $search_criteria_variable, ":client_company_support_email_search_keyword" => $search_criteria_variable, ":is_active_status_search_keyword" => $search_criteria_variable);

			//$company_clients_named_parameters_values_array_input = array();

			//Get Company Clients List Count
			$company_clients_list_count_get_sql = "SELECT COUNT(*) AS count FROM `company_clients` WHERE `company_id`=:company_id AND `is_active_status`=:is_active_status " . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;
			$eventLog->log("before query");
			$eventLog->log("company_clients_list_count_get_sql-> " . $company_clients_list_count_get_sql);
			$company_clients_list_count_get_select_query = $dbcon->prepare($company_clients_list_count_get_sql);
			$company_clients_list_count_get_select_query->execute($company_clients_named_parameters_values_array_input);
			$eventLog->log("after excute the value");


			//Give List of Company Clients, based on value of is_active_status
			$company_clients_list_get_sql = "SELECT * FROM `company_clients` WHERE `company_id`=:company_id AND `is_active_status`=:is_active_status" . $search_criteria_in_query_without_where_keyword . $sort_details_in_query . $limit_offset_in_query;
		    $eventLog->log("company_clients_list_count_get_sql-> " . $company_clients_list_get_sql);
			$company_clients_list_get_select_query = $dbcon->prepare($company_clients_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->execute();*/
			$company_clients_list_get_select_query->execute($company_clients_named_parameters_values_array_input);

		} else {
			$eventLog->log("else condition for search_criteria_input null concept");
			//Define Named parameters and corresponding Values
			$company_clients_named_parameters_values_array_input = array(":is_active_status" => $client_company_status_input, ":company_id" => $company_id_input);

			//Give List of Companies Count, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$company_clients_list_count_get_sql = "SELECT COUNT(*) AS count FROM `company_clients` WHERE `company_id`=:company_id AND `is_active_status`=:is_active_status " . $sort_details_in_query;
			$company_clients_list_count_get_select_query = $dbcon->prepare($company_clients_list_count_get_sql);
			$company_clients_list_count_get_select_query->execute($company_clients_named_parameters_values_array_input);


			//Give List of Company clients, based on value of is_active_status
			$company_clients_list_get_sql = "SELECT * FROM `company_clients` WHERE `company_id`=:company_id AND `is_active_status`=:is_active_status " . $sort_details_in_query . $limit_offset_in_query;

			$company_clients_list_get_select_query = $dbcon->prepare($company_clients_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->execute();*/
			$company_clients_list_get_select_query->execute($company_clients_named_parameters_values_array_input);

		}//close of else of if (!is_null($search_criteria_input)) {



	}//close of else of if (is_null($company_status_input)) {

	//Process / Get Company clients Count
	$eventLog->log("get the row count value");
	if($company_clients_list_count_get_select_query->rowCount() > 0) {
	    $company_clients_list_count_get_select_query_result = $company_clients_list_count_get_select_query->fetch();
	    //print_r($company_clients_list_count_get_select_query_result);

		$total_company_clients_count = $company_clients_list_count_get_select_query_result["count"];
		$constructed_array["total_records_count"] = $total_company_clients_count;

	}//close of if($company_clients_list_count_get_select_query->rowCount() > 0) {


	//Process / Fetch Companies List
	if($company_clients_list_get_select_query->rowCount() > 0) {
	    $company_clients_list_get_select_query_result = $company_clients_list_get_select_query->fetchAll();
	    //print_r($companies_list_get_select_query_result);
	    $eventLog->log("retrive the details ");
		foreach ($company_clients_list_get_select_query_result as $company_clients_list_get_select_query_result_row) {

			$temp_row_array = array();
		    $temp_row_array["company_client_id"] = $company_clients_list_get_select_query_result_row["company_client_id"];
		    $temp_row_array["company_id"] = $company_clients_list_get_select_query_result_row["company_id"];
			$temp_row_array["client_company_name"] = $company_clients_list_get_select_query_result_row["client_company_name"];
		    $temp_row_array["client_company_seo_name"] = $company_clients_list_get_select_query_result_row["client_company_seo_name"];
		    $temp_row_array["client_company_brand_name"] = $company_clients_list_get_select_query_result_row["client_company_brand_name"];
			$temp_row_array["client_company_email"] = $company_clients_list_get_select_query_result_row["client_company_email"];
			$temp_row_array["client_company_support_email"] = $company_clients_list_get_select_query_result_row["client_company_support_email"];
			$temp_row_array["is_active_status"] = $company_clients_list_get_select_query_result_row["is_active_status"];


		    $constructed_array["list"][] = $temp_row_array;
	    }//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

	}//close of if($companies_list_get_select_query->rowCount() > 0) {
		$eventLog->log("all input values are recives");
	return $constructed_array;
}

function get_candidates_list_with_pagination_inputs($company_id_input,$sm_user_status_input, $page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input) {

    global $dbcon, $default_number_of_records_pagination; //app/core/main-config.php
	global $eventLog,$ea_extracted_jwt_token_sub, $current_epoch, $expiring_link_lifetime, $expiring_link_secret_key, $expiring_link_hash_algorithm, $site_hostname_value;
	$constructed_array = array();
	$eventLog->log("before is_null concept");
	/* if ($company_id_input == "") {
		$get_candidate_list_sql = "SELECT * FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE ssmca.sm_site_member_classification_detail_id = '1' ORDER BY sm.sm_memb_id ASC";
		//$get_candidate_list_select_query = $dbcon->prepare($get_candidate_list_sql);
	} else {
		$get_candidate_list_sql = "SELECT * FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE ssmca.sm_site_member_classification_detail_id = '1' AND sm.company_id =:company_id ORDER BY sm.sm_memb_id ASC";
		//$get_candidate_list_select_query = $dbcon->prepare($get_candidate_list_sql);
		//$get_candidate_list_select_query->bindValue(":company_id",$company_id_input);
	}//close of else of if ($company_id_input == "") {
 */
	if ($search_criteria_input == "") {
		$search_criteria_input = null;
	}//close of if ($search_criteria_input == "") {


	if (!is_null($search_criteria_input)) {
		//When Search Criteria input is not empty
		$search_criteria_variable = "%" . $search_criteria_input . "%";
		//$search_criteria_in_query = "(`company_name` LIKE '%info%') OR (`company_seo_name` LIKE '%info%') OR (`company_brand_name` LIKE '%info%') OR (`is_active_status` LIKE '%info%')";

		$search_criteria_in_query_with_where_keyword = " WHERE ((sm.sm_memb_id LIKE :sm_memb_id_search_keyword) OR (sm.sm_email LIKE :sm_email_search_keyword) OR (sm.sm_mobile LIKE :sm_mobile_search_keyword) OR (sm.sm_alternate_email LIKE :sm_alternate_email_search_keyword) OR (sm.sm_salutation LIKE :sm_salutation_search_keyword) OR (sm.sm_firstname LIKE :sm_firstname_search_keyword) OR (sm.sm_middlename LIKE :sm_middlename_search_keyword) OR (sm.sm_lastname LIKE :sm_lastname_search_keyword) OR (sm.sm_user_status LIKE :sm_user_status_search_keyword)) ";
		$eventLog->log("after search criteria concept");
		$search_criteria_in_query_without_where_keyword = " AND ((sm.sm_memb_id LIKE :sm_memb_id_search_keyword) OR (sm.sm_email LIKE :sm_email_search_keyword) OR (sm.sm_mobile LIKE :sm_mobile_search_keyword) OR (sm.sm_alternate_email LIKE :sm_alternate_email_search_keyword) OR (sm.sm_salutation LIKE :sm_salutation_search_keyword) OR (sm.sm_firstname LIKE :sm_firstname_search_keyword) OR (sm.sm_middlename LIKE :sm_middlename_search_keyword) OR (sm.sm_lastname LIKE :sm_lastname_search_keyword) OR (sm.sm_user_status LIKE :sm_user_status_search_keyword)) ";
	    $eventLog->log("after search criteria without where concept");
	} else {
		//When Search Criteria input is empty
		$search_criteria_in_query_with_where_keyword = " WHERE ";

		$search_criteria_in_query_without_where_keyword = "";

	}//close of else of if (!is_null($search_criteria_input)) {
	$eventLog->log("before sort order concept");

	if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {

		if (!is_null($sort_field_input)) {
			//When the Sort Field input is not empty
			$eventLog->log("null condition  sort concept");

			if ($sort_field_input == "sm_memb_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_memb_id " . $sort_order_input;

			} else if ($sort_field_input == "sm_email") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_email " . $sort_order_input;

			} else if ($sort_field_input == "sm_mobile") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_mobile " . $sort_order_input;

			} else if ($sort_field_input == "sm_alternate_email") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_alternate_email " . $sort_order_input;

			} else if ($sort_field_input == "sm_salutation") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_salutation " . $sort_order_input;

            } else if ($sort_field_input == "sm_firstname") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_firstname " . $sort_order_input;

			}else if ($sort_field_input == "sm_middlename") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_middlename " . $sort_order_input;

			}else if ($sort_field_input == "sm_lastname") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_lastname " . $sort_order_input;

			} else if ($sort_field_input == "sm_user_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_user_status " . $sort_order_input;

			} else {
				//When Sort Field input, is not among the expected values, use primary key, with provided Sorting Order input
				$sort_details_in_query = " ORDER BY sm.sm_memb_id " . $sort_order_input;


			}
			$eventLog->log("after checking inputs concept");

		} else {
			//When the Sort Field input is empty, primary key has to be taken, with provided Sorting Order input
			$sort_details_in_query = " ORDER BY sm.sm_memb_id " . $sort_order_input;

		}//close of if (!is_null($sort_field_input)) {


	} else {
		//When the Sort Field input is empty, primary key has to be taken, with ASC Order, by default
		$sort_details_in_query = " ORDER BY sm.sm_memb_id ASC ";

	}//close of else of if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {


	$limit_offset_value = 0;

	if (($page_number_input == "") && ($number_of_records_input == "")) {
		//Give All Data
		//No LIMIT Clause in the Query
		$limit_offset_in_query = "";

	} else if (($page_number_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than page_number_input, till the number of records, as defined in the $default_number_of_records_pagination

		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $default_number_of_records_pagination;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$default_number_of_records_pagination;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $default_number_of_records_pagination;
		}//close of else if of if ($page_number_input == "1") {


		$eventLog->log("before page number concept");
	} else if (($page_number_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records
		$limit_offset_in_query = " LIMIT " . $number_of_records_input;

	} else {
		//Give Data, from given Record, till the requested number of records
		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $number_of_records_input;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$number_of_records_input;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $number_of_records_input;
		}//close of else if of if ($page_number_input == "1") {

	}//close of else of if (($page_number_input == "") && ($number_of_records_input == "")) {
	$eventLog->log("before sm_user_status concept");



	if (is_null($sm_user_status_input)) {
		$eventLog->log("after check candidate_status concept");
		if (!is_null($search_criteria_input)) {
			//Give List of Company clients, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$eventLog->log("before named parameters search_criteria concept");

			//Define Named parameters and corresponding Values
			$candidates_named_parameters_values_array_input = array(":company_id" => $company_id_input,":sm_memb_id_search_keyword" => $search_criteria_variable, ":sm_email_search_keyword" => $search_criteria_variable, ":sm_mobile_search_keyword" => $search_criteria_variable, ":sm_alternate_email_search_keyword" => $search_criteria_variable, ":sm_salutation_search_keyword" => $search_criteria_variable,":sm_firstname_search_keyword" => $search_criteria_variable,":sm_middlename_search_keyword" => $search_criteria_variable,":sm_lastname_search_keyword" => $search_criteria_variable,":sm_user_status_search_keyword" =>$search_criteria_variable,":ssmca_sm_site_member_classification_detail_id" => "1");
			
			$eventLog->log("before getting the content details");
			
			//Get Candidates List Count
			$candidates_list_count_get_sql = "SELECT COUNT(*) AS count FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE sm.company_id=:company_id AND ssmca.sm_site_member_classification_detail_id = :ssmca_sm_site_member_classification_detail_id " . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;
			$eventLog->log("candidates_list_count_get_sql-> " .$candidates_list_count_get_sql);
			$candidates_list_count_get_select_query = $dbcon->prepare($candidates_list_count_get_sql);
			$candidates_list_count_get_select_query->execute($candidates_named_parameters_values_array_input);
			
			$eventLog->log("after getting the candidates list concept");
			
			//Get Candidates List
			$candidates_list_get_sql = "SELECT * FROM  `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE sm.company_id=:company_id AND ssmca.sm_site_member_classification_detail_id = :ssmca_sm_site_member_classification_detail_id " . $search_criteria_in_query_without_where_keyword . $sort_details_in_query . $limit_offset_in_query;

			$candidates_list_get_select_query = $dbcon->prepare($candidates_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->execute();*/
			$candidates_list_get_select_query->execute($candidates_named_parameters_values_array_input);

		} else {
			$eventLog->log("else condition of user_status concept");
			//Define Named parameters and corresponding Values
			$candidates_named_parameters_values_array_input = array(":company_id" => $company_id_input, ":ssmca_sm_site_member_classification_detail_id" => "1");

			//Give List of Candidates Count, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$candidates_list_count_get_sql = "SELECT COUNT(*) AS count FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id  WHERE sm.company_id=:company_id AND ssmca.sm_site_member_classification_detail_id = :ssmca_sm_site_member_classification_detail_id " . $sort_details_in_query;
			$candidates_list_count_get_select_query = $dbcon->prepare($candidates_list_count_get_sql);
			$candidates_list_count_get_select_query->execute($candidates_named_parameters_values_array_input);


			//Give List of Candidates, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$candidates_list_get_sql = "SELECT * FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id  WHERE sm.company_id=:company_id AND ssmca.sm_site_member_classification_detail_id = :ssmca_sm_site_member_classification_detail_id " . $sort_details_in_query . $limit_offset_in_query;
			$candidates_list_get_select_query = $dbcon->prepare($candidates_list_get_sql);
			$candidates_list_get_select_query->execute($candidates_named_parameters_values_array_input);

		}//close of else of if (!is_null($search_criteria_input)) {


	} else {
		if (!is_null($search_criteria_input)) {
			$eventLog->log("check the search_criteria_input null concept");

			//Define Named parameters and corresponding Values
			$candidates_named_parameters_values_array_input = array(":company_id" => $company_id_input, ":sm_user_status" => $sm_user_status_input, ":sm_memb_id_search_keyword" => $search_criteria_variable, ":sm_email_search_keyword" => $search_criteria_variable, ":sm_mobile_search_keyword" => $search_criteria_variable, ":sm_alternate_email_search_keyword" => $search_criteria_variable, ":sm_salutation_search_keyword" => $search_criteria_variable,":sm_firstname_search_keyword" => $search_criteria_variable, ":sm_middlename_search_keyword" => $search_criteria_variable, ":sm_lastname_search_keyword" => $search_criteria_variable,":sm_user_status_search_keyword" => $search_criteria_variable, ":ssmca_sm_site_member_classification_detail_id" => "1");

			//$company_clients_named_parameters_values_array_input = array();

			//Get Company Clients List Count
			$candidates_list_count_get_sql = "SELECT COUNT(*) AS count FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE `company_id`=:company_id AND `sm_user_status`=:sm_user_status AND ssmca.sm_site_member_classification_detail_id = :ssmca_sm_site_member_classification_detail_id " . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;
			$eventLog->log("before query");
			$eventLog->log("candidates_list_count_get_sql-> " . $candidates_list_count_get_sql);
			$candidates_list_count_get_select_query = $dbcon->prepare($candidates_list_count_get_sql);
			$candidates_list_count_get_select_query->execute($candidates_named_parameters_values_array_input);
			$eventLog->log("after excute the value");


			//Give List of Company Clients, based on value of is_active_status
			$candidates_list_get_sql = "SELECT * FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE `company_id`=:company_id AND `sm_user_status`=:sm_user_status AND ssmca.sm_site_member_classification_detail_id = :ssmca_sm_site_member_classification_detail_id " . $search_criteria_in_query_without_where_keyword . $sort_details_in_query . $limit_offset_in_query;
		    $eventLog->log("candidates_list_count_get_sql-> " . $candidates_list_get_sql);
			$candidates_list_get_select_query = $dbcon->prepare($candidates_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);\
			$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->execute();*/
			$candidates_list_get_select_query->execute($candidates_named_parameters_values_array_input);

		} else {
			$eventLog->log("else condition for search_criteria_input null concept");
			//Define Named parameters and corresponding Values
			$candidates_named_parameters_values_array_input = array(":sm_user_status" => $sm_user_status_input, ":company_id" => $company_id_input, ":ssmca_sm_site_member_classification_detail_id" => "1");

			//Give List of candidates Count, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$candidates_list_count_get_sql = "SELECT COUNT(*) AS count FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE `company_id`=:company_id AND `sm_user_status`=:sm_user_status  AND ssmca.sm_site_member_classification_detail_id = :ssmca_sm_site_member_classification_detail_id " . $sort_details_in_query;
			$candidates_list_count_get_select_query = $dbcon->prepare($candidates_list_count_get_sql);
			$candidates_list_count_get_select_query->execute($candidates_named_parameters_values_array_input);


			//Give List of Candidates, based on value of is_active_status
			$candidates_list_get_sql = "SELECT * FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE `company_id`=:company_id AND `sm_user_status`=:sm_user_status  AND ssmca.sm_site_member_classification_detail_id = :ssmca_sm_site_member_classification_detail_id " . $sort_details_in_query . $limit_offset_in_query;

			$candidates_list_get_select_query = $dbcon->prepare($candidates_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->execute();*/
			$candidates_list_get_select_query->execute($candidates_named_parameters_values_array_input);

		}//close of else of if (!is_null($search_criteria_input)) {



	}//close of else of if (is_null($company_status_input)) {

	//Process  Get Candidates Count
	$eventLog->log("get the row count value");
	if($candidates_list_count_get_select_query->rowCount() > 0) {
	    $candidates_list_count_get_select_query_result = $candidates_list_count_get_select_query->fetch();
	    //print_r($candidates_list_count_get_select_query_result);

		$total_candidates_count = $candidates_list_count_get_select_query_result["count"];
		$constructed_array["total_records_count"] = $total_candidates_count;

	}//close of if($candidates_list_count_get_select_query->rowCount() > 0) {


	//Process / Fetch Companies List
	if($candidates_list_get_select_query->rowCount() > 0) {
	    $candidates_list_get_select_query_result = $candidates_list_get_select_query->fetchAll();
	    //print_r($companies_list_get_select_query_result);
	    $eventLog->log("retrive the details ");
		foreach ($candidates_list_get_select_query_result as $candidates_list_get_select_query_result_row) {

			$temp_row_array = array();
		    $temp_row_array["sm_memb_id"] = $candidates_list_get_select_query_result_row["sm_memb_id"];
		    $temp_row_array["sm_email"] = $candidates_list_get_select_query_result_row["sm_email"];
			$temp_row_array["sm_mobile"] = $candidates_list_get_select_query_result_row["sm_mobile"];
		    $temp_row_array["sm_alternate_email"] = $candidates_list_get_select_query_result_row["sm_alternate_email"];
		    $temp_row_array["sm_salutation"] = $candidates_list_get_select_query_result_row["sm_salutation"];
			$temp_row_array["sm_firstname"] = $candidates_list_get_select_query_result_row["sm_firstname"];
			$temp_row_array["sm_middlename"] = $candidates_list_get_select_query_result_row["sm_middlename"];
			$temp_row_array["sm_lastname"] = $candidates_list_get_select_query_result_row["sm_lastname"];
			$temp_row_array["company_id"] = $candidates_list_get_select_query_result_row["company_id"];
			
			if($temp_row_array["sm_salutation"] == "") {
				$salutation_string = "";
				
			} else {
				$salutation_string = $temp_row_array["sm_salutation"] . ".";
				
			}
			
			if (($temp_row_array["sm_firstname"] != "") && ($temp_row_array["sm_middlename"] != "") && ($temp_row_array["sm_lastname"] != "")) {
				$candidate_fullname = $salutation_string . $temp_row_array["sm_firstname"] . " " . $temp_row_array["sm_middlename"] . " " . $temp_row_array["sm_lastname"];

			} else if (($temp_row_array["sm_firstname"] == "") && ($temp_row_array["sm_middlename"] != "") && ($temp_row_array["sm_lastname"] == "")) {
				$candidate_fullname = $salutation_string . $temp_row_array["sm_firstname"] . " " . $temp_row_array["sm_middlename"];
		
			} else if (($temp_row_array["sm_firstname"] != "") && ($temp_row_array["sm_middlename"] == "") && ($temp_row_array["sm_lastname"] == "")) {
				$candidate_fullname = $salutation_string . $temp_row_array["sm_firstname"];
		
			} else if (($temp_row_array["sm_firstname"] != "") && ($temp_row_array["sm_middlename"] == "") && ($temp_row_array["sm_lastname"] != "")) {
				$candidate_fullname = $salutation_string . $temp_row_array["sm_firstname"] . " " . $temp_row_array["sm_lastname"];
			
			} else if (($temp_row_array["sm_firstname"] == "") && ($temp_row_array["sm_middlename"] != "") && ($temp_row_array["sm_lastname"] != "")) {
				$candidate_fullname = $salutation_string . $temp_row_array["sm_middlename"] . " " . $temp_row_array["sm_lastname"];
			
			} else if (($temp_row_array["sm_firstname"] == "") && ($temp_row_array["sm_middlename"] == "") && ($temp_row_array["sm_lastname"] != "")) {
				$candidate_fullname = $salutation_string . $temp_row_array["sm_lastname"];
			} else {
				$candidate_fullname = "";
			}
			
			$temp_row_array["sm_fullname"] = $candidate_fullname;
			$temp_row_array["sm_user_status"] = $candidates_list_get_select_query_result_row["sm_user_status"];
            
			$candidate_resume_details_result = candidate_rel_uploaded_resumes_based_on_sm_memb_id_input($temp_row_array["sm_memb_id"]);	    
			if(count($candidate_resume_details_result) > 0) {
			  $temp_row_array["crur_id"] = $candidate_resume_details_result["crur_id"];
			    
				$link_expiry_time = $current_epoch+$expiring_link_lifetime;
				$uploaded_file_link_with_signature_result = create_uploaded_file_link_with_signature($ea_extracted_jwt_token_sub, $temp_row_array["sm_memb_id"], $temp_row_array["crur_id"], "https", $site_hostname_value, $link_expiry_time, $expiring_link_hash_algorithm, $expiring_link_secret_key);
				if (isset($uploaded_file_link_with_signature_result["uploaded_file_url"])) {
				$temp_row_array["uploaded_file_url"] = $uploaded_file_link_with_signature_result["uploaded_file_url"];
					
				} else {
					$temp_row_array["uploaded_file_url"] = null;
				}//close of if (isset($uploaded_file_link_with_signature_result["uploaded_file_url"])) {
					
				$download_file_link_with_signature_result = create_download_file_link_with_signature($ea_extracted_jwt_token_sub, $temp_row_array["sm_memb_id"], $temp_row_array["crur_id"], "https", $site_hostname_value, $link_expiry_time, $expiring_link_hash_algorithm, $expiring_link_secret_key);
				if (isset($download_file_link_with_signature_result["download_file_url"])) {
				$temp_row_array["download_file_url"] = $download_file_link_with_signature_result["download_file_url"];
					
				} else {
					$temp_row_array["download_file_url"] = null;
				}//close of if (isset($download_file_link_with_signature_result["uploaded_file_url"])) {
			} else {
				$temp_row_array["crur_id"] = null;
				$temp_row_array["uploaded_file_url"] = null;
				$temp_row_array["download_file_url"] = null;
			}
			
			$constructed_array["list"][] = $temp_row_array;
	    }//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

	}//close of if($companies_list_get_select_query->rowCount() > 0) {
		$eventLog->log("all input values are received");
	return $constructed_array;
}

function candidate_rel_uploaded_resumes_based_on_sm_memb_id_input($sm_memb_id) {
    global $dbcon;
	$constructed_array = array();
	
	$candidate_rel_uploaded_resumes_based_on_sm_memb_id_sql = "SELECT * FROM `candidate_rel_uploaded_resumes` WHERE `sm_memb_id`=:sm_memb_id ORDER BY `added_date_time_epoch` DESC LIMIT 1";
	$candidate_rel_uploaded_resumes_based_on_sm_memb_id_query = $dbcon->prepare($candidate_rel_uploaded_resumes_based_on_sm_memb_id_sql);
	$candidate_rel_uploaded_resumes_based_on_sm_memb_id_query->bindValue(":sm_memb_id",$sm_memb_id);	
	$candidate_rel_uploaded_resumes_based_on_sm_memb_id_query->execute(); 
	
	if($candidate_rel_uploaded_resumes_based_on_sm_memb_id_query->rowCount() > 0) {
		$candidate_rel_uploaded_resumes_based_on_sm_memb_id_query_result = $candidate_rel_uploaded_resumes_based_on_sm_memb_id_query->fetch();
	     return $candidate_rel_uploaded_resumes_based_on_sm_memb_id_query_result;
	
	}//close of if($candidate_rel_uploaded_resumes_based_on_sm_memb_id_query->rowCount() > 0) {
	return $constructed_array;
}

function create_uploaded_file_link_with_signature($viewer_user_id_input, $candidate_user_id_input, $uploaded_file_ref_id_input, $uploaded_file_url_protocol_input, $uploaded_file_url_hostname_input, $expiring_link_lifetime_input, $expiring_link_hash_algorithm_input, $expiring_link_secret_key_input) {
	global $dbcon, $eventLog;
	
	$constructed_array = array();
	
	//Use Active JWT Token Details of the Document Viewing User (Admin user / Company User / Candidate thyself)
	$viewing_user_rel_active_jwt_token_details_result = ea_get_user_rel_active_jwt_token_details($viewer_user_id_input);
	
	if (count($viewing_user_rel_active_jwt_token_details_result) > 0) {
		
		foreach($viewing_user_rel_active_jwt_token_details_result as $viewing_user_rel_active_jwt_token_details_result_key => $viewing_user_rel_active_jwt_token_details_result_value) {
				
			//$user_auth_token_id = $viewing_user_rel_active_jwt_token_details_result_row["user_auth_token_id"];
			$user_auth_token_id = $viewing_user_rel_active_jwt_token_details_result_key;
			
			
			$candidate_rel_uploaded_visa_document_details_result = candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id($candidate_user_id_input, $uploaded_file_ref_id_input);
			
			if (count($candidate_rel_uploaded_visa_document_details_result) > 0) {
				
				$generated_filename = $candidate_rel_uploaded_visa_document_details_result["generated_filename"];
				
				
				
				/*//Gathering inputs for Link Signature
				$link_signature_inputs = $viewer_user_id_input . ":::::" . $candidate_user_id_input . ":::::" . $uploaded_file_ref_id_input . ":::::" . $generated_filename . ":::::" . $user_auth_token_id;
				//expiring_links_lifetime
				$link_signature = hash('sha256', $link_signature_inputs);
				*/
				//return $constructed_array["link_signature"] = $link_signature;
				
				//Create Uploaded File URL
				//https://dev-visadoc.securitywonks.net/viewer/1/candidate/11/uploaded-file-ref-id/151/display/link-signature/tff8t76rft8tyfy88y8f8y
				////https://dev-visadoc.securitywonks.net/viewer/11/candidate/11/uploaded-file-ref-id/46/display/expires/1111111/link-signature/bd27845343fdfer3redsfr3regfh6t3rfdggj78453f
				
				//$uploaded_file_url = $uploaded_file_url_protocol_input . "://" . $uploaded_file_url_hostname_input . "/viewer/" . $viewer_user_id_input . "/candidate/" . $candidate_user_id_input . "/uploaded-file-ref-id/" . $uploaded_file_ref_id_input . "/display/expires/" . $expiring_link_lifetime_input . "/link-signature/" . $link_signature;
				
				$uploaded_file_url_without_signature = $uploaded_file_url_protocol_input . "://" . $uploaded_file_url_hostname_input . "/viewer/" . $viewer_user_id_input . "/candidate/" . $candidate_user_id_input . "/uploaded-file-ref-id/" . $uploaded_file_ref_id_input . "/display/expires/" . $expiring_link_lifetime_input;
				
				$eventLog->log("uploaded_file_url (without signature): " . $uploaded_file_url_without_signature);
				
				/*//Creating Link Signature
				$link_signature = hash('sha256', $user_auth_token_id . $uploaded_file_url_without_signature);
				*/
				
				//Base64 Decode the Base64 Encoded Expiring Link related Secret Key
				$expiring_link_rel_secret_base64_encoded = $expiring_link_secret_key_input;
				$expiring_link_rel_secret_base64_decoded = base64_decode($expiring_link_rel_secret_base64_encoded);
				
				//Create Link Signature using sha256
				$created_link_signature = hash_hmac($expiring_link_hash_algorithm_input, $user_auth_token_id . $uploaded_file_url_without_signature, $expiring_link_rel_secret_base64_decoded, true);
				
				//Base64 URL Encode the Created Token Signature
				$created_link_signature_base64_urlencoded = base64url_encode($created_link_signature); //from /app/includes/other-functions-api.php
				
				//remove padding (=), from Base64 URL Encoded Token Signature
				$created_link_signature_base64_urlencoded_after_removing_padding = str_replace("=", "", $created_link_signature_base64_urlencoded);
				
				$uploaded_file_url_with_signature = $uploaded_file_url_without_signature . "/link-signature/" . $created_link_signature_base64_urlencoded_after_removing_padding;
				
				$constructed_array["uploaded_file_url"] = $uploaded_file_url_with_signature;
				
				$eventLog->log("uploaded_file_url (with signature): " . $uploaded_file_url_with_signature);
				
				return $constructed_array;
				
			} else {
				$eventLog->log("Uploaded file ref id is not Valid!!!!");	
			}//close of else of if (count($candidate_rel_uploaded_visa_document_details_result) > 0) {
				
			
			
		}//close of foreach($viewing_user_rel_active_jwt_token_details_result as $viewing_user_rel_active_jwt_token_details_result_key => $viewing_user_rel_active_jwt_token_details_result_value) {
		
	} else {
		$eventLog->log("Active Token Details of Viewing User is not received");
	}//close of else of if (count($viewing_user_rel_active_jwt_token_details_result) > 0) {
	
	return $constructed_array;
}

function create_download_file_link_with_signature($viewer_user_id_input, $candidate_user_id_input, $uploaded_file_ref_id_input, $uploaded_file_url_protocol_input, $uploaded_file_url_hostname_input, $expiring_link_lifetime_input, $expiring_link_hash_algorithm_input, $expiring_link_secret_key_input) {
	global $dbcon, $eventLog;
	
	$constructed_array = array();
	
	//Use Active JWT Token Details of the Document Viewing User (Admin user / Company User / Candidate thyself)
	$viewing_user_rel_active_jwt_token_details_result = ea_get_user_rel_active_jwt_token_details($viewer_user_id_input);
	
	if (count($viewing_user_rel_active_jwt_token_details_result) > 0) {
		
		foreach($viewing_user_rel_active_jwt_token_details_result as $viewing_user_rel_active_jwt_token_details_result_key => $viewing_user_rel_active_jwt_token_details_result_value) {
				
			//$user_auth_token_id = $viewing_user_rel_active_jwt_token_details_result_row["user_auth_token_id"];
			$user_auth_token_id = $viewing_user_rel_active_jwt_token_details_result_key;
			
			
			$candidate_rel_uploaded_visa_document_details_result = candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id($candidate_user_id_input, $uploaded_file_ref_id_input);
			
			if (count($candidate_rel_uploaded_visa_document_details_result) > 0) {
				
				$generated_filename = $candidate_rel_uploaded_visa_document_details_result["generated_filename"];
				
				
				
				/*//Gathering inputs for Link Signature
				$link_signature_inputs = $viewer_user_id_input . ":::::" . $candidate_user_id_input . ":::::" . $uploaded_file_ref_id_input . ":::::" . $generated_filename . ":::::" . $user_auth_token_id;
				//expiring_links_lifetime
				$link_signature = hash('sha256', $link_signature_inputs);
				*/
				//return $constructed_array["link_signature"] = $link_signature;
				
				//Create Uploaded File URL
				//https://dev-visadoc.securitywonks.net/viewer/1/candidate/11/uploaded-file-ref-id/151/display/link-signature/tff8t76rft8tyfy88y8f8y
				////https://dev-visadoc.securitywonks.net/viewer/11/candidate/11/uploaded-file-ref-id/46/display/expires/1111111/link-signature/bd27845343fdfer3redsfr3regfh6t3rfdggj78453f
				
				//$uploaded_file_url = $uploaded_file_url_protocol_input . "://" . $uploaded_file_url_hostname_input . "/viewer/" . $viewer_user_id_input . "/candidate/" . $candidate_user_id_input . "/uploaded-file-ref-id/" . $uploaded_file_ref_id_input . "/display/expires/" . $expiring_link_lifetime_input . "/link-signature/" . $link_signature;
				
				$download_file_url_without_signature = $uploaded_file_url_protocol_input . "://" . $uploaded_file_url_hostname_input . "/viewer/" . $viewer_user_id_input . "/candidate/" . $candidate_user_id_input . "/uploaded-file-ref-id/" . $uploaded_file_ref_id_input . "/download/expires/" . $expiring_link_lifetime_input;
				
				$eventLog->log("uploaded_file_url (without signature): " . $download_file_url_without_signature);
				
				/*//Creating Link Signature
				$link_signature = hash('sha256', $user_auth_token_id . $download_file_url_without_signature);
				*/
				
				//Base64 Decode the Base64 Encoded Expiring Link related Secret Key
				$expiring_link_rel_secret_base64_encoded = $expiring_link_secret_key_input;
				$expiring_link_rel_secret_base64_decoded = base64_decode($expiring_link_rel_secret_base64_encoded);
				
				//Create Link Signature using sha256
				$created_link_signature = hash_hmac($expiring_link_hash_algorithm_input, $user_auth_token_id . $download_file_url_without_signature, $expiring_link_rel_secret_base64_decoded, true);
				
				//Base64 URL Encode the Created Token Signature
				$created_link_signature_base64_urlencoded = base64url_encode($created_link_signature); //from /app/includes/other-functions-api.php
				
				//remove padding (=), from Base64 URL Encoded Token Signature
				$created_link_signature_base64_urlencoded_after_removing_padding = str_replace("=", "", $created_link_signature_base64_urlencoded);
				
				$download_file_url_with_signature = $download_file_url_without_signature . "/link-signature/" . $created_link_signature_base64_urlencoded_after_removing_padding;
				
				$constructed_array["download_file_url"] = $download_file_url_with_signature;
				
				$eventLog->log("download_file_url (with signature): " . $download_file_url_with_signature);
				
				return $constructed_array;
				
			} else {
				$eventLog->log("Uploaded file ref id is not Valid!!!!");	
			}//close of else of if (count($candidate_rel_uploaded_visa_document_details_result) > 0) {
				
			
			
		}//close of foreach($viewing_user_rel_active_jwt_token_details_result as $viewing_user_rel_active_jwt_token_details_result_key => $viewing_user_rel_active_jwt_token_details_result_value) {
		
	} else {
		$eventLog->log("Active Token Details of Viewing User is not received");
	}//close of else of if (count($viewing_user_rel_active_jwt_token_details_result) > 0) {
	
	return $constructed_array;
}


function candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id($user_id_input, $crauvd_id_input) {
    global $dbcon;
	$constructed_array = array();
	
	$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_sql = "SELECT * FROM `candidate_rel_uploaded_resumes` WHERE `crur_id`=:crur_id AND `sm_memb_id`=:sm_memb_id";
	$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query = $dbcon->prepare($candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_sql);
	$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query->bindValue(":crur_id",$crauvd_id_input);	
	$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query->bindValue(":sm_memb_id",$user_id_input);	
	$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query->execute(); 
	
	if($candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query->rowCount() > 0) {
		$candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query_result = $candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query->fetch();
	     return $candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query_result;
	
	}//close of if($candidate_rel_uploaded_visa_document_details_based_on_sm_memb_id_crauvd_id_query->rowCount() > 0) {
	return $constructed_array;
}




function get_admin_users_list_with_pagination_inputs($company_id_input,$sm_user_status_input, $page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input) {

    global $dbcon, $default_number_of_records_pagination,$ea_extracted_jwt_token_sub; //app/core/main-config.php
	global $eventLog;
	$constructed_array = array();
	$eventLog->log("before is_null concept");
	$user_id_input = $ea_extracted_jwt_token_sub;
	/* if ($company_id_input == "") {
		$get_candidate_list_sql = "SELECT * FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE ssmca.sm_site_member_classification_detail_id = '1' ORDER BY sm.sm_memb_id ASC";
		//$get_candidate_list_select_query = $dbcon->prepare($get_candidate_list_sql);
	} else {
		$get_candidate_list_sql = "SELECT * FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE ssmca.sm_site_member_classification_detail_id = '1' AND sm.company_id =:company_id ORDER BY sm.sm_memb_id ASC";
		//$get_candidate_list_select_query = $dbcon->prepare($get_candidate_list_sql);
		//$get_candidate_list_select_query->bindValue(":company_id",$company_id_input);
	}//close of else of if ($company_id_input == "") {
 */
	if ($search_criteria_input == "") {
		$search_criteria_input = null;
	}//close of if ($search_criteria_input == "") {


	if (!is_null($search_criteria_input)) {
		//When Search Criteria input is not empty
		$search_criteria_variable = "%" . $search_criteria_input . "%";
		//$search_criteria_in_query = "(`company_name` LIKE '%info%') OR (`company_seo_name` LIKE '%info%') OR (`company_brand_name` LIKE '%info%') OR (`is_active_status` LIKE '%info%')";

		$search_criteria_in_query_with_where_keyword = " WHERE ((sm.sm_memb_id LIKE :sm_memb_id_search_keyword) OR (sm.sm_email LIKE :sm_email_search_keyword) OR (sm.sm_mobile LIKE :sm_mobile_search_keyword) OR (sm.sm_alternate_email LIKE :sm_alternate_email_search_keyword) OR (sm.sm_salutation LIKE :sm_salutation_search_keyword) OR (sm.sm_firstname LIKE :sm_firstname_search_keyword) OR (sm.sm_middlename LIKE :sm_middlename_search_keyword) OR (sm.sm_lastname LIKE :sm_lastname_search_keyword) OR (sm.sm_user_status LIKE :sm_user_status_search_keyword)) ";
		$eventLog->log("after search criteria concept");
		$search_criteria_in_query_without_where_keyword = " AND ((sm.sm_memb_id LIKE :sm_memb_id_search_keyword) OR (sm.sm_email LIKE :sm_email_search_keyword) OR (sm.sm_mobile LIKE :sm_mobile_search_keyword) OR (sm.sm_alternate_email LIKE :sm_alternate_email_search_keyword) OR (sm.sm_salutation LIKE :sm_salutation_search_keyword) OR (sm.sm_firstname LIKE :sm_firstname_search_keyword) OR (sm.sm_middlename LIKE :sm_middlename_search_keyword) OR (sm.sm_lastname LIKE :sm_lastname_search_keyword) OR (sm.sm_user_status LIKE :sm_user_status_search_keyword)) ";
	    $eventLog->log("after search criteria without where concept");
	} else {
		//When Search Criteria input is empty
		$search_criteria_in_query_with_where_keyword = " WHERE ";

		$search_criteria_in_query_without_where_keyword = "";

	}//close of else of if (!is_null($search_criteria_input)) {
	$eventLog->log("before sort order concept");

	if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {

		if (!is_null($sort_field_input)) {
			//When the Sort Field input is not empty
			$eventLog->log("null condition  sort concept");

			if ($sort_field_input == "sm_memb_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_memb_id " . $sort_order_input;

			} else if ($sort_field_input == "sm_email") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_email " . $sort_order_input;

			} else if ($sort_field_input == "sm_mobile") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_mobile " . $sort_order_input;

			} else if ($sort_field_input == "sm_alternate_email") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_alternate_email " . $sort_order_input;

			} else if ($sort_field_input == "sm_salutation") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_salutation " . $sort_order_input;

            } else if ($sort_field_input == "sm_firstname") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_firstname " . $sort_order_input;

			}else if ($sort_field_input == "sm_middlename") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_middlename " . $sort_order_input;

			}else if ($sort_field_input == "sm_lastname") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_lastname " . $sort_order_input;

			} else if ($sort_field_input == "sm_user_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY sm.sm_user_status " . $sort_order_input;

			} else {
				//When Sort Field input, is not among the expected values, use primary key, with provided Sorting Order input
				$sort_details_in_query = " ORDER BY sm.sm_memb_id " . $sort_order_input;


			}
			$eventLog->log("after checking inputs concept");

		} else {
			//When the Sort Field input is empty, primary key has to be taken, with provided Sorting Order input
			$sort_details_in_query = " ORDER BY sm.sm_memb_id " . $sort_order_input;

		}//close of if (!is_null($sort_field_input)) {


	} else {
		//When the Sort Field input is empty, primary key has to be taken, with ASC Order, by default
		$sort_details_in_query = " ORDER BY sm.sm_memb_id ASC ";

	}//close of else of if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {


	$limit_offset_value = 0;

	if (($page_number_input == "") && ($number_of_records_input == "")) {
		//Give All Data
		//No LIMIT Clause in the Query
		$limit_offset_in_query = "";

	} else if (($page_number_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than page_number_input, till the number of records, as defined in the $default_number_of_records_pagination

		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $default_number_of_records_pagination;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$default_number_of_records_pagination;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $default_number_of_records_pagination;
		}//close of else if of if ($page_number_input == "1") {


		$eventLog->log("before page number concept");
	} else if (($page_number_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records
		$limit_offset_in_query = " LIMIT " . $number_of_records_input;

	} else {
		//Give Data, from given Record, till the requested number of records
		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $number_of_records_input;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$number_of_records_input;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $number_of_records_input;
		}//close of else if of if ($page_number_input == "1") {

	}//close of else of if (($page_number_input == "") && ($number_of_records_input == "")) {
	$eventLog->log("before sm_user_status concept");



	if (is_null($sm_user_status_input)) {
		$eventLog->log("after check candidate_status concept");
		if (!is_null($search_criteria_input)) {
			//Give List of Company clients, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$eventLog->log("before named parameters search_criteria concept");

			//Define Named parameters and corresponding Values
			$candidates_named_parameters_values_array_input = array(":company_id" => $company_id_input,":sm_memb_id_search_keyword" => $search_criteria_variable, ":sm_email_search_keyword" => $search_criteria_variable, ":sm_mobile_search_keyword" => $search_criteria_variable, ":sm_alternate_email_search_keyword" => $search_criteria_variable, ":sm_salutation_search_keyword" => $search_criteria_variable,":sm_firstname_search_keyword" => $search_criteria_variable,":sm_middlename_search_keyword" => $search_criteria_variable,":sm_lastname_search_keyword" => $search_criteria_variable,":sm_user_status_search_keyword" =>$search_criteria_variable,":ssmca_sm_site_member_classification_detail_id" => "1");
			
			$eventLog->log("before getting the content details");
			
			//Get Candidates List Count
			$candidates_list_count_get_sql = "SELECT COUNT(*) AS count FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE sm.company_id=:company_id AND ssmca.sm_site_member_classification_detail_id != :ssmca_sm_site_member_classification_detail_id " . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;
			$eventLog->log("candidates_list_count_get_sql-> " .$candidates_list_count_get_sql);
			$candidates_list_count_get_select_query = $dbcon->prepare($candidates_list_count_get_sql);
			$candidates_list_count_get_select_query->execute($candidates_named_parameters_values_array_input);
			
			$eventLog->log("after getting the candidates list concept");
			
			//Get Candidates List
			$candidates_list_get_sql = "SELECT * FROM  `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE sm.company_id=:company_id AND ssmca.sm_site_member_classification_detail_id != :ssmca_sm_site_member_classification_detail_id " . $search_criteria_in_query_without_where_keyword . $sort_details_in_query . $limit_offset_in_query;

			$candidates_list_get_select_query = $dbcon->prepare($candidates_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->execute();*/
			$candidates_list_get_select_query->execute($candidates_named_parameters_values_array_input);

		} else {
			$eventLog->log("else condition of user_status concept");
			//Define Named parameters and corresponding Values
			$candidates_named_parameters_values_array_input = array(":company_id" => $company_id_input, ":ssmca_sm_site_member_classification_detail_id" => "1");

			//Give List of Candidates Count, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$candidates_list_count_get_sql = "SELECT COUNT(*) AS count FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id  WHERE sm.company_id=:company_id AND ssmca.sm_site_member_classification_detail_id != :ssmca_sm_site_member_classification_detail_id " . $sort_details_in_query;
			$candidates_list_count_get_select_query = $dbcon->prepare($candidates_list_count_get_sql);
			$candidates_list_count_get_select_query->execute($candidates_named_parameters_values_array_input);


			//Give List of Candidates, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$candidates_list_get_sql = "SELECT * FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id  WHERE sm.company_id=:company_id AND ssmca.sm_site_member_classification_detail_id != :ssmca_sm_site_member_classification_detail_id " . $sort_details_in_query . $limit_offset_in_query;
			$candidates_list_get_select_query = $dbcon->prepare($candidates_list_get_sql);
			$candidates_list_get_select_query->execute($candidates_named_parameters_values_array_input);

		}//close of else of if (!is_null($search_criteria_input)) {


	} else {
		if (!is_null($search_criteria_input)) {
			$eventLog->log("check the search_criteria_input null concept");

			//Define Named parameters and corresponding Values
			$candidates_named_parameters_values_array_input = array(":company_id" => $company_id_input, ":sm_user_status" => $sm_user_status_input, ":sm_memb_id_search_keyword" => $search_criteria_variable, ":sm_email_search_keyword" => $search_criteria_variable, ":sm_mobile_search_keyword" => $search_criteria_variable, ":sm_alternate_email_search_keyword" => $search_criteria_variable, ":sm_salutation_search_keyword" => $search_criteria_variable,":sm_firstname_search_keyword" => $search_criteria_variable, ":sm_middlename_search_keyword" => $search_criteria_variable, ":sm_lastname_search_keyword" => $search_criteria_variable,":sm_user_status_search_keyword" => $search_criteria_variable, ":ssmca_sm_site_member_classification_detail_id" => "1");

			//$company_clients_named_parameters_values_array_input = array();

			//Get Company Clients List Count
			$candidates_list_count_get_sql = "SELECT COUNT(*) AS count FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE `company_id`=:company_id AND `sm_user_status`=:sm_user_status AND ssmca.sm_site_member_classification_detail_id != :ssmca_sm_site_member_classification_detail_id " . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;
			$eventLog->log("before query");
			$eventLog->log("candidates_list_count_get_sql-> " . $candidates_list_count_get_sql);
			$candidates_list_count_get_select_query = $dbcon->prepare($candidates_list_count_get_sql);
			$candidates_list_count_get_select_query->execute($candidates_named_parameters_values_array_input);
			$eventLog->log("after excute the value");


			//Give List of Company Clients, based on value of is_active_status
			$candidates_list_get_sql = "SELECT * FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE `company_id`=:company_id AND `sm_user_status`=:sm_user_status AND ssmca.sm_site_member_classification_detail_id != :ssmca_sm_site_member_classification_detail_id " . $search_criteria_in_query_without_where_keyword . $sort_details_in_query . $limit_offset_in_query;
		    $eventLog->log("candidates_list_count_get_sql-> " . $candidates_list_get_sql);
			$candidates_list_get_select_query = $dbcon->prepare($candidates_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);\
			$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->execute();*/
			$candidates_list_get_select_query->execute($candidates_named_parameters_values_array_input);

		} else {
			$eventLog->log("else condition for search_criteria_input null concept");
			//Define Named parameters and corresponding Values
			$candidates_named_parameters_values_array_input = array(":sm_user_status" => $sm_user_status_input, ":company_id" => $company_id_input, ":ssmca_sm_site_member_classification_detail_id" => "1");

			//Give List of candidates Count, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$candidates_list_count_get_sql = "SELECT COUNT(*) AS count FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE `company_id`=:company_id AND `sm_user_status`=:sm_user_status  AND ssmca.sm_site_member_classification_detail_id != :ssmca_sm_site_member_classification_detail_id " . $sort_details_in_query;
			$candidates_list_count_get_select_query = $dbcon->prepare($candidates_list_count_get_sql);
			$candidates_list_count_get_select_query->execute($candidates_named_parameters_values_array_input);


			//Give List of Candidates, based on value of is_active_status
			$candidates_list_get_sql = "SELECT * FROM `site_members` sm LEFT JOIN `sm_site_member_classification_associations` ssmca ON sm.sm_memb_id = ssmca.sm_memb_id WHERE `company_id`=:company_id AND `sm_user_status`=:sm_user_status  AND ssmca.sm_site_member_classification_detail_id != :ssmca_sm_site_member_classification_detail_id " . $sort_details_in_query . $limit_offset_in_query;

			$candidates_list_get_select_query = $dbcon->prepare($candidates_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->execute();*/
			$candidates_list_get_select_query->execute($candidates_named_parameters_values_array_input);

		}//close of else of if (!is_null($search_criteria_input)) {



	}//close of else of if (is_null($company_status_input)) {

	//Process  Get Candidates Count
	$eventLog->log("get the row count value");
	if($candidates_list_count_get_select_query->rowCount() > 0) {
	    $candidates_list_count_get_select_query_result = $candidates_list_count_get_select_query->fetch();
	    //print_r($candidates_list_count_get_select_query_result);

		$total_candidates_count = $candidates_list_count_get_select_query_result["count"];
		$constructed_array["total_records_count"] = $total_candidates_count;

	}//close of if($candidates_list_count_get_select_query->rowCount() > 0) {


	//Process / Fetch Companies List
	if($candidates_list_get_select_query->rowCount() > 0) {
	    $candidates_list_get_select_query_result = $candidates_list_get_select_query->fetchAll();
	    //print_r($companies_list_get_select_query_result);
	    $eventLog->log("retrive the details ");
		foreach ($candidates_list_get_select_query_result as $candidates_list_get_select_query_result_row) {

			$temp_row_array = array();
		    $temp_row_array["sm_memb_id"] = $candidates_list_get_select_query_result_row["sm_memb_id"];
		    $temp_row_array["sm_email"] = $candidates_list_get_select_query_result_row["sm_email"];
			$temp_row_array["sm_mobile"] = $candidates_list_get_select_query_result_row["sm_mobile"];
		    $temp_row_array["sm_alternate_email"] = $candidates_list_get_select_query_result_row["sm_alternate_email"];
		    $temp_row_array["sm_salutation"] = $candidates_list_get_select_query_result_row["sm_salutation"];
			$temp_row_array["sm_firstname"] = $candidates_list_get_select_query_result_row["sm_firstname"];
			$temp_row_array["sm_middlename"] = $candidates_list_get_select_query_result_row["sm_middlename"];
			$temp_row_array["sm_lastname"] = $candidates_list_get_select_query_result_row["sm_lastname"];
			$sm_role_result = sm_user_classification_details_get($temp_row_array["sm_memb_id"]);
			$logged_user_role_result = sm_user_classification_details_get($user_id_input);
			$logged_user_classification_id = $logged_user_role_result["sm_site_member_classification_detail_id"];
			$temp_row_array["sm_user_role"] = $sm_role_result["sm_user_role"];
			$temp_row_array["sm_site_member_classification_detail_id"] = $sm_role_result["sm_site_member_classification_detail_id"];
			
			$temp_row_array["sm_user_status"] = $candidates_list_get_select_query_result_row["sm_user_status"];
			$admin_user_actions_allowed_list_result =  admin_user_actions_allowed_list($logged_user_classification_id);
			if (in_array($temp_row_array["sm_site_member_classification_detail_id"], $admin_user_actions_allowed_list_result)) {
				$temp_row_array["actions_allowed_status"] = "1";
			} else {
				$temp_row_array["actions_allowed_status"] = "0";
			}
						


		     $constructed_array["list"][] = $temp_row_array;
	    }//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

	}//close of if($companies_list_get_select_query->rowCount() > 0) {
		$eventLog->log("all input values are received");
	return $constructed_array;
}
	
function admin_user_actions_allowed_list($user_classification_id) {
	
	global $dbcon,$eventLog;
	$is_active_status = "1";
	
	$constructed_array = array();
	
	$user_groups_list_sql = "SELECT `company_specific_subject_classifications`  FROM `page_api_sm_site_user_classification_associations` WHERE `page_filename` LIKE 'rest-admin-users-list.php' AND `sm_site_member_classification_detail_id` = :sm_site_member_classification_detail_id AND is_active_status = :is_active_status";
	
	$user_groups_list_query = $dbcon->prepare($user_groups_list_sql);
	$user_groups_list_query->bindValue(":sm_site_member_classification_detail_id",$user_classification_id);
	$user_groups_list_query->bindValue(":is_active_status",$is_active_status);
	$user_groups_list_query->execute();
	if($user_groups_list_query->rowCount() > 0) {
	    $user_groups_list_query_result = $user_groups_list_query->fetch();
		$constructed_array = explode(",",$user_groups_list_query_result["company_specific_subject_classifications"]);
		if($user_classification_id == "13") {
			array_push($constructed_array,"12");
		}
		
		
		return $constructed_array;
		//return $user_groups_list_query_result;
	}
	return $constructed_array;
	
	
	
	
}


function get_companies_list_specific_company_user_based_on_company_id_input($company_id_input) {

    global $dbcon;
	$constructed_array = array();
		$companies_list_get_sql = "SELECT * FROM `companies` WHERE `company_id`=:company_id";

		$companies_list_get_select_query = $dbcon->prepare($companies_list_get_sql);
		$companies_list_get_select_query->bindValue(":company_id",$company_id_input);
		$companies_list_get_select_query->execute();

	if($companies_list_get_select_query->rowCount() > 0) {
	    $companies_list_get_select_query_result = $companies_list_get_select_query->fetch();
	    //print_r($user_classification_details_get_select_query_result);

		$temp_row_array = array();
		$temp_row_array["company_id"] = $companies_list_get_select_query_result["company_id"];
		$temp_row_array["company_name"] = $companies_list_get_select_query_result["company_name"];
		$temp_row_array["company_seo_name"] = $companies_list_get_select_query_result["company_seo_name"];
		$temp_row_array["company_brand_name"] = $companies_list_get_select_query_result["company_brand_name"];
		$temp_row_array["company_status"] = $companies_list_get_select_query_result["is_active_status"];
		$temp_row_array["company_support_email"] = $companies_list_get_select_query_result["company_support_email"];
		$constructed_array[] = $temp_row_array;

		return $constructed_array;
	}
	return $constructed_array;
}

function get_company_details_based_on_company_id($company_id_input) {

    global $dbcon;
	$constructed_array = array();
		$companies_details_get_sql = "SELECT * FROM `companies` WHERE `company_id`=:company_id";

		$companies_details_get_select_query = $dbcon->prepare($companies_details_get_sql);
		$companies_details_get_select_query->bindValue(":company_id",$company_id_input);
		$companies_details_get_select_query->execute();

	if($companies_details_get_select_query->rowCount() > 0) {
	    $companies_details_get_select_query_result = $companies_details_get_select_query->fetch();
	    //print_r($companies_details_get_select_query_result);

		return $companies_details_get_select_query_result;
	}
	return $constructed_array;
}

function update_user_rel_sm_user_status_based_on_sm_memb_id($sm_memb_id_input, $new_status_input, $event_datetime_input, $event_datetime_epoch_input) {
	global $dbcon;

	$user_rel_sm_user_status_update_sql = "UPDATE `site_members` SET `last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch,`sm_user_status`=:sm_user_status,`closed_date_time`=:closed_date_time,`closed_date_time_epoch`=:closed_date_time_epoch WHERE `sm_memb_id`=:sm_memb_id";
	$user_rel_sm_user_status_update_query = $dbcon->prepare($user_rel_sm_user_status_update_sql);
	$user_rel_sm_user_status_update_query->bindValue(":last_updated_date_time",$event_datetime_input);
	$user_rel_sm_user_status_update_query->bindValue(":last_updated_date_time_epoch",$event_datetime_epoch_input);
	$user_rel_sm_user_status_update_query->bindValue(":sm_user_status",$new_status_input);
	$user_rel_sm_user_status_update_query->bindValue(":sm_memb_id",$sm_memb_id_input);
	if($new_status_input == 4) {
	$user_rel_sm_user_status_update_query->bindValue(":closed_date_time",$event_datetime_input);
	$user_rel_sm_user_status_update_query->bindValue(":closed_date_time_epoch",$event_datetime_epoch_input);
	} else {
	$user_rel_sm_user_status_update_query->bindValue(":closed_date_time",null);
	$user_rel_sm_user_status_update_query->bindValue(":closed_date_time_epoch",null);
	}


		if ($user_rel_sm_user_status_update_query->execute()) {

            return true;

		}
	return false;
}

function ea_user_details_delete($user_id_input) {
	global $dbcon;

	$ea_user_details_delete_sql = "DELETE sm,smca FROM `site_members` sm INNER JOIN `sm_site_member_classification_associations` smca ON sm.sm_memb_id = smca.sm_memb_id AND sm.sm_memb_id =:sm_memb_id";
    $ea_user_details_delete_query = $dbcon->prepare($ea_user_details_delete_sql);
	$ea_user_details_delete_query->bindValue(":sm_memb_id",$user_id_input);
	    if($ea_user_details_delete_query->execute()) {

			return true;
		}
	return false;
}

function candidate_client_vendor_data_duplicate_check($user_id_input,$year) {
	global $dbcon;
	$constructed_array = array();
	$candidate_client_vendor_data_duplicate_check_sql = "SELECT `sm_memb_id`,`year` FROM `candidate_rel_client_vendor_data` WHERE `sm_memb_id` =:sm_memb_id AND `year` =:year";
	$candidate_client_vendor_data_duplicate_check_q = $dbcon->prepare($candidate_client_vendor_data_duplicate_check_sql);
	$candidate_client_vendor_data_duplicate_check_q->bindValue(":sm_memb_id",$user_id_input);
    $candidate_client_vendor_data_duplicate_check_q->bindValue(":year",$year);
	$candidate_client_vendor_data_duplicate_check_q->execute();

	if($candidate_client_vendor_data_duplicate_check_q->rowCount() > 0) {
		$candidate_client_vendor_data_duplicate_check_result = $candidate_client_vendor_data_duplicate_check_q->fetch();
	     return $candidate_client_vendor_data_duplicate_check_result;

	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;

}

function candidate_client_vendor_data_update($user_id_input, $year, $end_client_fullname_input, $implementation_partner_details_input, $vendor1_fullname_input, $vendor2_fullname_input, $vendor3_fullname_input, $vendor4_fullname_input, $vendor5_fullname_input, $vendor6_fullname_input, $vendor7_fullname_input, $vendor8_fullname_input, $vendor9_fullname_input, $event_datetime, $current_epoch){
    global $dbcon;

	$candidate_client_vendor_data_update_sql = "UPDATE `candidate_rel_client_vendor_data` SET `end_client_full_name`=:end_client_full_name, `implementation_partner_details`=:implementation_partner_details, `vendor_1_full_name`=:vendor_1_full_name, `vendor_2_full_name`=:vendor_2_full_name, `vendor_3_full_name`=:vendor_3_full_name, `vendor_4_full_name`=:vendor_4_full_name, `vendor_5_full_name`=:vendor_5_full_name, `vendor_6_full_name`=:vendor_6_full_name, `vendor_7_full_name`=:vendor_7_full_name, `vendor_8_full_name`=:vendor_8_full_name, `vendor_9_full_name`=:vendor_9_full_name, `last_updated_date_time`=:last_updated_date_time, `last_updated_date_time_epoch`=:last_updated_date_time_epoch WHERE `sm_memb_id` = :sm_memb_id AND `year` = :year";
	$candidate_client_vendor_data_update_q = $dbcon->prepare($candidate_client_vendor_data_update_sql);
	$candidate_client_vendor_data_update_q->bindValue(":sm_memb_id",$user_id_input);
	$candidate_client_vendor_data_update_q->bindValue(":year",$year);
	$candidate_client_vendor_data_update_q->bindValue(":end_client_full_name",$end_client_fullname_input);
	$candidate_client_vendor_data_update_q->bindValue(":implementation_partner_details",$implementation_partner_details_input);
	$candidate_client_vendor_data_update_q->bindValue(":vendor_1_full_name",$vendor1_fullname_input);
	$candidate_client_vendor_data_update_q->bindValue(":vendor_2_full_name",$vendor2_fullname_input);
	$candidate_client_vendor_data_update_q->bindValue(":vendor_3_full_name",$vendor3_fullname_input);
	$candidate_client_vendor_data_update_q->bindValue(":vendor_4_full_name",$vendor4_fullname_input);
	$candidate_client_vendor_data_update_q->bindValue(":vendor_5_full_name",$vendor5_fullname_input);
	$candidate_client_vendor_data_update_q->bindValue(":vendor_6_full_name",$vendor6_fullname_input);
	$candidate_client_vendor_data_update_q->bindValue(":vendor_7_full_name",$vendor7_fullname_input);
	$candidate_client_vendor_data_update_q->bindValue(":vendor_8_full_name",$vendor8_fullname_input);
    $candidate_client_vendor_data_update_q->bindValue(":vendor_9_full_name",$vendor9_fullname_input);
	$candidate_client_vendor_data_update_q->bindValue(":last_updated_date_time",$event_datetime);
	$candidate_client_vendor_data_update_q->bindValue(":last_updated_date_time_epoch",$current_epoch);
		if ($candidate_client_vendor_data_update_q->execute()) {
			return true;
		}
	return false;

}

function candidate_client_vendor_data_insert($user_id_input, $year, $end_client_fullname_input, $implementation_partner_details_input, $vendor1_fullname_input, $vendor2_fullname_input, $vendor3_fullname_input, $vendor4_fullname_input, $vendor5_fullname_input, $vendor6_fullname_input, $vendor7_fullname_input, $vendor8_fullname_input, $vendor9_fullname_input, $added_by_user_type_input, $added_by_user_id_input, $event_datetime, $current_epoch){
    global $dbcon;

	$candidate_client_vendor_data_insert_sql = "INSERT INTO `candidate_rel_client_vendor_data`(`sm_memb_id`, `year`, `end_client_full_name`, `implementation_partner_details`, `vendor_1_full_name`, `vendor_2_full_name`, `vendor_3_full_name`, `vendor_4_full_name`, `vendor_5_full_name`, `vendor_6_full_name`, `vendor_7_full_name`, `vendor_8_full_name`, `vendor_9_full_name`, `added_by_user_type`, `added_by_user_id`, `added_date_time`, `added_date_time_epoch`) VALUES (:sm_memb_id, :year, :end_client_full_name, :implementation_partner_details, :vendor_1_full_name, :vendor_2_full_name, :vendor_3_full_name, :vendor_4_full_name, :vendor_5_full_name, :vendor_6_full_name, :vendor_7_full_name, :vendor_8_full_name, :vendor_9_full_name, :added_by_user_type, :added_by_user_id, :added_date_time, :added_date_time_epoch)";
	$candidate_client_vendor_data_insert_q = $dbcon->prepare($candidate_client_vendor_data_insert_sql);
	$candidate_client_vendor_data_insert_q->bindValue(":sm_memb_id",$user_id_input);
	$candidate_client_vendor_data_insert_q->bindValue(":year",$year);
	$candidate_client_vendor_data_insert_q->bindValue(":end_client_full_name",$end_client_fullname_input);
	$candidate_client_vendor_data_insert_q->bindValue(":implementation_partner_details",$implementation_partner_details_input);
	$candidate_client_vendor_data_insert_q->bindValue(":vendor_1_full_name",$vendor1_fullname_input);
	$candidate_client_vendor_data_insert_q->bindValue(":vendor_2_full_name",$vendor2_fullname_input);
	$candidate_client_vendor_data_insert_q->bindValue(":vendor_3_full_name",$vendor3_fullname_input);
	$candidate_client_vendor_data_insert_q->bindValue(":vendor_4_full_name",$vendor4_fullname_input);
	$candidate_client_vendor_data_insert_q->bindValue(":vendor_5_full_name",$vendor5_fullname_input);
	$candidate_client_vendor_data_insert_q->bindValue(":vendor_6_full_name",$vendor6_fullname_input);
	$candidate_client_vendor_data_insert_q->bindValue(":vendor_7_full_name",$vendor7_fullname_input);
	$candidate_client_vendor_data_insert_q->bindValue(":vendor_8_full_name",$vendor8_fullname_input);
    $candidate_client_vendor_data_insert_q->bindValue(":vendor_9_full_name",$vendor9_fullname_input);
	$candidate_client_vendor_data_insert_q->bindValue(":added_by_user_type",$added_by_user_type_input);
	$candidate_client_vendor_data_insert_q->bindValue(":added_by_user_id",$added_by_user_id_input);
	$candidate_client_vendor_data_insert_q->bindValue(":added_date_time",$event_datetime);
	$candidate_client_vendor_data_insert_q->bindValue(":added_date_time_epoch",$current_epoch);
		if ($candidate_client_vendor_data_insert_q->execute()) {
			return true;
		}
	return false;
}

function candidate_current_job_info_duplicate_check($user_id_input)	{
	global $dbcon;
	$constructed_array = array();
	$candidate_current_job_info_duplicate_check_sql = "SELECT * FROM `candidate_current_job_specific_info` WHERE `sm_memb_id` =:sm_memb_id ";
	$candidate_current_job_info_duplicate_check_q = $dbcon->prepare($candidate_current_job_info_duplicate_check_sql);
	$candidate_current_job_info_duplicate_check_q->bindValue(":sm_memb_id",$user_id_input);
	$candidate_current_job_info_duplicate_check_q->execute();

	if($candidate_current_job_info_duplicate_check_q->rowCount() > 0) {
		$candidate_current_job_info_duplicate_check_result = $candidate_current_job_info_duplicate_check_q->fetch();
	     return $candidate_current_job_info_duplicate_check_result;

	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;

}

function candidate_current_job_info_update($user_id_input,$current_work_location_full_address_input, $job_duties_in_detail_input, $event_datetime, $current_epoch){
    global $dbcon;

	$candidate_current_job_info_update_sql = "UPDATE `candidate_current_job_specific_info` SET `current_work_location`=:current_work_location, `job_duties_full_detail`=:job_duties_full_detail, `last_updated_date_time`=:last_updated_date_time, `last_updated_date_time_epoch`=:last_updated_date_time_epoch WHERE `sm_memb_id` = :sm_memb_id";
	$candidate_current_job_info_update_q = $dbcon->prepare($candidate_current_job_info_update_sql);
	$candidate_current_job_info_update_q->bindValue(":sm_memb_id",$user_id_input);
	$candidate_current_job_info_update_q->bindValue(":current_work_location",$current_work_location_full_address_input);
	$candidate_current_job_info_update_q->bindValue(":job_duties_full_detail",$job_duties_in_detail_input);
	$candidate_current_job_info_update_q->bindValue(":last_updated_date_time",$event_datetime);
	$candidate_current_job_info_update_q->bindValue(":last_updated_date_time_epoch",$current_epoch);

		if ($candidate_current_job_info_update_q->execute()) {
			return true;
		}
	return false;
}

function candidate_current_job_info_insert($user_id_input, $current_work_location_full_address_input, $job_duties_in_detail_input, $added_by_user_type_input, $added_by_user_id_input, $event_datetime, $current_epoch) {
    global $dbcon;

	$constructed_array = array();

	$candidate_current_job_info_insert_sql = "INSERT INTO `candidate_current_job_specific_info`(`sm_memb_id`, `current_work_location`, `job_duties_full_detail`, `added_by_user_type`, `added_by_user_id`, `added_date_time`, `added_date_time_epoch`) VALUES (:sm_memb_id, :current_work_location, :job_duties_full_detail, :added_by_user_type, :added_by_user_id, :added_date_time, :added_date_time_epoch)";
	$candidate_current_job_info_insert_q = $dbcon->prepare($candidate_current_job_info_insert_sql);
	$candidate_current_job_info_insert_q->bindValue(":sm_memb_id",$user_id_input);
	$candidate_current_job_info_insert_q->bindValue(":current_work_location",$current_work_location_full_address_input);
	$candidate_current_job_info_insert_q->bindValue(":job_duties_full_detail",$job_duties_in_detail_input);
	$candidate_current_job_info_insert_q->bindValue(":added_by_user_type",$added_by_user_type_input);
	$candidate_current_job_info_insert_q->bindValue(":added_by_user_id",$added_by_user_id_input);
	$candidate_current_job_info_insert_q->bindValue(":added_date_time",$event_datetime);
	$candidate_current_job_info_insert_q->bindValue(":added_date_time_epoch",$current_epoch);
	/* 	if ($candidate_current_job_info_insert_q->execute()) {
			return true;
		}
	return false; */
	if ($candidate_current_job_info_insert_q->execute()) {
		$last_inserted_id = $dbcon->lastInsertId();
		//$eventLog->log("record inserted successfully");

		$constructed_array["last_inserted_id"] = $last_inserted_id;

		return $constructed_array;

	} else {
		//$eventLog->log("Error occurred during process. Please try again");
		return $constructed_array;
	}//close of else of if ($candidate_current_job_info_insert_q->execute()) {

}

function candidate_visa_rel_personal_info_duplicate_check($user_id_input) {
    global $dbcon;
	$constructed_array = array();
	$candidate_visa_rel_personal_info_duplicate_check_sql = "SELECT * FROM `candidate_visa_rel_personal_information` WHERE `sm_memb_id` =:sm_memb_id ";
	$candidate_visa_rel_personal_info_duplicate_check_q = $dbcon->prepare($candidate_visa_rel_personal_info_duplicate_check_sql);
	$candidate_visa_rel_personal_info_duplicate_check_q->bindValue(":sm_memb_id",$user_id_input);
	$candidate_visa_rel_personal_info_duplicate_check_q->execute();

	if($candidate_visa_rel_personal_info_duplicate_check_q->rowCount() > 0) {
		$candidate_visa_rel_personal_info_duplicate_check_result = $candidate_visa_rel_personal_info_duplicate_check_q->fetch();
	     return $candidate_visa_rel_personal_info_duplicate_check_result;

	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
}

function candidate_visa_rel_personal_info_update($user_id_input, $salutation_input, $first_name_input, $middle_name_input, $last_name_input, $date_of_birth_input, $birth_country_input, $birth_state_input, $birth_city_input, $is_married_input, $spouse_child_h4_visa_requirement_input,$passport_number_input, $passport_date_of_issue_input, $passport_date_of_expiry_input, $passport_country_of_issue_input,$primary_email_id_input, $alternate_email_id_input, $home_country_telephone_no_input, $usa_telephone_no_input, $home_country_mobile_no_input, $usa_mobile_no_input, $home_country_postal_address_input, $usa_postal_address_input, $usa_social_security_no_input, $recent_us_entry_visa_type_input, $recent_us_entry_date_input, $recent_us_entry_place_input, $recent_us_entry_i94_number_input, $i94_number_expiry_date_input, $last_updated_by_user_type_input, $last_updated_by_user_id_input, $event_datetime, $current_epoch, $is_first_visit_input) {
    global $dbcon;

	/* if ($is_married_input == "0") {
		$spouse_child_h4_visa_requirement_input = "0";
	}//close of if ($is_married_input == "0") {
		 */
	$candidate_visa_rel_personal_info_update_sql = "UPDATE `candidate_visa_rel_personal_information` SET `salutation`=:salutation,`first_name`=:first_name,`middle_name`=:middle_name,`last_name`=:last_name,`date_of_birth`=:date_of_birth,`birth_country`=:birth_country,`birth_state`=:birth_state,`birth_city`=:birth_city,`is_married`=:is_married,`spouse_child_h4_visa_requirement`=:spouse_child_h4_visa_requirement,`passport_number`=:passport_number,`passport_date_of_issue`=:passport_date_of_issue,`passport_date_of_expiry`=:passport_date_of_expiry,`passport_country_of_issue`=:passport_country_of_issue,`primary_email_id`=:primary_email_id,`alternate_email_id`=:alternate_email_id,`home_country_telephone_no`=:home_country_telephone_no,`usa_telephone_no`=:usa_telephone_no,`home_country_mobile_no`=:home_country_mobile_no,`usa_mobile_no`=:usa_mobile_no,`home_country_postal_address`=:home_country_postal_address,`usa_postal_address`=:usa_postal_address,`usa_social_security_no`=:usa_social_security_no,`recent_us_entry_visa_type`=:recent_us_entry_visa_type,`recent_us_entry_date`=:recent_us_entry_date,`recent_us_entry_place`=:recent_us_entry_place,`recent_us_entry_i94_number`=:recent_us_entry_i94_number,`i94_number_expiry_date`=:i94_number_expiry_date,`last_updated_by_user_type`=:last_updated_by_user_type,`last_updated_by_user_id`=:last_updated_by_user_id,`last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch, `is_first_visit`=:is_first_visit_input WHERE `sm_memb_id` = :sm_memb_id";
	$candidate_visa_rel_personal_info_update_q = $dbcon->prepare($candidate_visa_rel_personal_info_update_sql);
	$candidate_visa_rel_personal_info_update_q->bindValue(":sm_memb_id",$user_id_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":salutation",$salutation_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":first_name",$first_name_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":middle_name",$middle_name_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":last_name",$last_name_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":date_of_birth",$date_of_birth_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":birth_country",$birth_country_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":birth_state",$birth_state_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":birth_city",$birth_city_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":is_married",$is_married_input);
	if($spouse_child_h4_visa_requirement_input == "") {
		$candidate_visa_rel_personal_info_update_q->bindValue(":spouse_child_h4_visa_requirement",null);
	} else {
		$candidate_visa_rel_personal_info_update_q->bindValue(":spouse_child_h4_visa_requirement",$spouse_child_h4_visa_requirement_input);
	}
	$candidate_visa_rel_personal_info_update_q->bindValue(":passport_number",$passport_number_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":passport_date_of_issue",$passport_date_of_issue_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":passport_date_of_expiry",$passport_date_of_expiry_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":passport_country_of_issue",$passport_country_of_issue_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":primary_email_id",$primary_email_id_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":alternate_email_id",$alternate_email_id_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":home_country_telephone_no",$home_country_telephone_no_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":usa_telephone_no",$usa_telephone_no_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":home_country_mobile_no",$home_country_mobile_no_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":usa_mobile_no",$usa_mobile_no_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":home_country_postal_address",$home_country_postal_address_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":usa_postal_address",$usa_postal_address_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":usa_social_security_no",$usa_social_security_no_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":recent_us_entry_visa_type",$recent_us_entry_visa_type_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":recent_us_entry_date",$recent_us_entry_date_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":recent_us_entry_place",$recent_us_entry_place_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":recent_us_entry_i94_number",$recent_us_entry_i94_number_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":i94_number_expiry_date",$i94_number_expiry_date_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":last_updated_by_user_type",$last_updated_by_user_type_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":last_updated_by_user_id",$last_updated_by_user_id_input);
	$candidate_visa_rel_personal_info_update_q->bindValue(":last_updated_date_time",$event_datetime);
	$candidate_visa_rel_personal_info_update_q->bindValue(":last_updated_date_time_epoch",$current_epoch);
	$candidate_visa_rel_personal_info_update_q->bindValue(":is_first_visit_input",$is_first_visit_input);

		if ($candidate_visa_rel_personal_info_update_q->execute()) {
			return true;
		}
	return false;
}


function candidate_visa_rel_personal_info_insert($salutation_input,$user_id_input,$first_name_input,$middle_name_input,$last_name_input,$date_of_birth_input,$birth_country_input,$birth_state_input,$birth_city_input,$is_married_input,$spouse_child_h4_visa_requirement_input,$passport_number_input,$passport_date_of_issue_input,$passport_date_of_expiry_input,$passport_country_of_issue_input,$primary_email_id_input,$alternate_email_id_input,$home_country_telephone_no_input,$usa_telephone_no_input,$home_country_mobile_no_input,$usa_mobile_no_input,$home_country_postal_address_input,$usa_postal_address_input,$usa_social_security_no_input,$recent_us_entry_visa_type_input,$recent_us_entry_date_input,$recent_us_entry_place_input,$recent_us_entry_i94_number_input, $i94_number_expiry_date_input,$ea_extracted_jwt_token_user_type,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch, $is_first_visit_input){
    global $dbcon;

	$constructed_array = array();

	$candidate_visa_rel_personal_info_insert_sql = "INSERT INTO `candidate_visa_rel_personal_information`(`salutation`, `sm_memb_id`, `first_name`, `middle_name`, `last_name`, `date_of_birth`, `birth_country`, `birth_state`, `birth_city`, `is_married`, `spouse_child_h4_visa_requirement`, `passport_number`, `passport_date_of_issue`, `passport_date_of_expiry`,`passport_country_of_issue`,`primary_email_id`, `alternate_email_id`, `home_country_telephone_no`, `usa_telephone_no`, `home_country_mobile_no`, `usa_mobile_no`, `home_country_postal_address`, `usa_postal_address`, `usa_social_security_no`, `recent_us_entry_visa_type`, `recent_us_entry_date`, `recent_us_entry_place`, `recent_us_entry_i94_number`, `i94_number_expiry_date`, `added_by_user_type`, `added_by_user_id`, `added_date_time`, `added_date_time_epoch`, `is_first_visit`) VALUES (:salutation,:sm_memb_id,:first_name,:middle_name,:last_name,:date_of_birth,:birth_country,:birth_state,:birth_city,:is_married,:spouse_child_h4_visa_requirement,:passport_number,:passport_date_of_issue,:passport_date_of_expiry,:passport_country_of_issue,:primary_email_id,:alternate_email_id,:home_country_telephone_no,:usa_telephone_no,:home_country_mobile_no,:usa_mobile_no,:home_country_postal_address,:usa_postal_address,:usa_social_security_no,:recent_us_entry_visa_type,:recent_us_entry_date,:recent_us_entry_place,:recent_us_entry_i94_number,:i94_number_expiry_date,:added_by_user_type,:added_by_user_id,:added_date_time,:added_date_time_epoch,:is_first_visit_input)";
	$candidate_visa_rel_personal_info_insert_q = $dbcon->prepare($candidate_visa_rel_personal_info_insert_sql);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":salutation",$salutation_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":sm_memb_id",$user_id_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":first_name",$first_name_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":middle_name",$middle_name_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":last_name",$last_name_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":date_of_birth",$date_of_birth_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":birth_country",$birth_country_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":birth_state",$birth_state_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":birth_city",$birth_city_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":is_married",$is_married_input);
	if($spouse_child_h4_visa_requirement_input == "") {
		$candidate_visa_rel_personal_info_insert_q->bindValue(":spouse_child_h4_visa_requirement",null);
	} else {
		$candidate_visa_rel_personal_info_insert_q->bindValue(":spouse_child_h4_visa_requirement",$spouse_child_h4_visa_requirement_input);
	}
	$candidate_visa_rel_personal_info_insert_q->bindValue(":passport_number",$passport_number_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":passport_date_of_issue",$passport_date_of_issue_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":passport_date_of_expiry",$passport_date_of_expiry_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":passport_country_of_issue",$passport_country_of_issue_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":primary_email_id",$primary_email_id_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":alternate_email_id",$alternate_email_id_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":home_country_telephone_no",$home_country_telephone_no_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":usa_telephone_no",$usa_telephone_no_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":home_country_mobile_no",$home_country_mobile_no_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":usa_mobile_no",$usa_mobile_no_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":home_country_postal_address",$home_country_postal_address_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":usa_postal_address",$usa_postal_address_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":usa_social_security_no",$usa_social_security_no_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":recent_us_entry_visa_type",$recent_us_entry_visa_type_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":recent_us_entry_date",$recent_us_entry_date_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":recent_us_entry_place",$recent_us_entry_place_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":recent_us_entry_i94_number",$recent_us_entry_i94_number_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":i94_number_expiry_date",$i94_number_expiry_date_input);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":added_by_user_type",$ea_extracted_jwt_token_user_type);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":added_by_user_id",$ea_extracted_jwt_token_sub);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":added_date_time",$event_datetime);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":added_date_time_epoch",$current_epoch);
	$candidate_visa_rel_personal_info_insert_q->bindValue(":is_first_visit_input",$is_first_visit_input);

	if ($candidate_visa_rel_personal_info_insert_q->execute()) {
		$last_inserted_id = $dbcon->lastInsertId();
		//$eventLog->log("record inserted successfully");

		$constructed_array["last_inserted_id"] = $last_inserted_id;

		return $constructed_array;

	} else {
		//$eventLog->log("Error occurred during process. Please try again");
		return $constructed_array;
	}//close of else of if ($candidate_visa_rel_personal_info_insert_q->execute()) {

}

function delete_candidate_unused_file_record_based_on_activity_ref_id_input($activity_ref_id,$additional_document_ref) {
	global $dbcon, $site_home_path;
	$deleted_id_array = array();

	$additional_document_ref_search_input = "%" . $additional_document_ref . "%";

	$candidate_unused_file_record_select_sql = "SELECT * FROM `candidate_rel_all_uploaded_visa_documents` WHERE `activity_ref_id`=:activity_ref_id AND `additional_document_ref` LIKE :additional_document_ref";
	$candidate_unused_file_record_select_query = $dbcon->prepare($candidate_unused_file_record_select_sql);
	$candidate_unused_file_record_select_query->bindValue(":activity_ref_id",$activity_ref_id);
	$candidate_unused_file_record_select_query->bindValue(":additional_document_ref",$additional_document_ref_search_input);
	$candidate_unused_file_record_select_query->execute();


	if($candidate_unused_file_record_select_query->rowCount() > 0) {
		//echo "after select query row count \n";

		$candidate_unused_file_record_select_query_result = $candidate_unused_file_record_select_query->fetchAll();
			foreach ($candidate_unused_file_record_select_query_result as $candidate_unused_file_record_select_query_result_row) {
				$crauvd_id_input = $candidate_unused_file_record_select_query_result_row["crauvd_id"];
				$sm_memb_id_input = $candidate_unused_file_record_select_query_result_row["sm_memb_id"];
				$activity_ref_id_input = $candidate_unused_file_record_select_query_result_row["activity_ref_id"];
				$additional_document_ref_input = $candidate_unused_file_record_select_query_result_row["additional_document_ref"];
				$generated_filename_input = $candidate_unused_file_record_select_query_result_row["generated_filename"];



				$crauvd_id_folder_name = create_seo_name(strtolower_utf8_extended($crauvd_id_input)) . "_" . create_seo_name(strtolower_utf8_extended($activity_ref_id_input)) . "_" . create_seo_name(strtolower_utf8_extended($additional_document_ref_input));

				//Create Full Upload Folder related Absolute Path
				$crauvd_id_full_folder_absolute_path = $site_home_path . "uploaded-documents/" . $sm_memb_id_input . "/" . $crauvd_id_folder_name;

				if (file_exists($crauvd_id_full_folder_absolute_path . "/" . $generated_filename_input)) {
					unlink($crauvd_id_full_folder_absolute_path . "/" . $generated_filename_input);
					/* rmdir($crauvd_id_full_folder_absolute_path);

					$candidate_unused_file_record_delete_sql = "DELETE FROM `candidate_rel_all_uploaded_visa_documents` WHERE `crauvd_id`=:crauvd_id";
				    $candidate_unused_file_record_delete_query = $dbcon->prepare($candidate_unused_file_record_delete_sql);
				    $candidate_unused_file_record_delete_query->bindValue(":crauvd_id",$crauvd_id_input);

				    if ($candidate_unused_file_record_delete_query->execute()) {
		                  $deleted_id_array[] = $crauvd_id_input;
						   return $deleted_id_array;
				    } */
				}/* else {
				    rmdir($crauvd_id_full_folder_absolute_path);

					$candidate_unused_file_record_delete_sql = "DELETE FROM `candidate_rel_all_uploaded_visa_documents` WHERE `crauvd_id`=:crauvd_id";
				    $candidate_unused_file_record_delete_query = $dbcon->prepare($candidate_unused_file_record_delete_sql);
				    $candidate_unused_file_record_delete_query->bindValue(":crauvd_id",$crauvd_id_input);

				    if ($candidate_unused_file_record_delete_query->execute()) {
		                  $deleted_id_array[] = $crauvd_id_input;
						   return $deleted_id_array;
				    }

				} */

				if (is_dir($crauvd_id_full_folder_absolute_path)) {
					rmdir($crauvd_id_full_folder_absolute_path);
				}//close of rmdir($crauvd_id_full_folder_absolute_path);

				clearstatcache();
				$candidate_unused_file_record_delete_sql = "DELETE FROM `candidate_rel_all_uploaded_visa_documents` WHERE `crauvd_id`=:crauvd_id";
				$candidate_unused_file_record_delete_query = $dbcon->prepare($candidate_unused_file_record_delete_sql);
				$candidate_unused_file_record_delete_query->bindValue(":crauvd_id",$crauvd_id_input);

				if ($candidate_unused_file_record_delete_query->execute()) {
					  $deleted_id_array[] = $crauvd_id_input;
					   //return $deleted_id_array;
				}
	        }
			return $deleted_id_array;
	} else {
		return $deleted_id_array;
	}

}

function candidate_prior_usa_visits_f1_specific_sevis_id_duplicate_check($user_id_input, $candidate_sevis_id_input) {
	global $dbcon;
	$constructed_array = array();

	$candidate_sevis_id_search_input = "%" . $candidate_sevis_id_input . "%";

	$candidate_prior_usa_visits_f1_specific_sevis_id_duplicate_check_sql = "SELECT * FROM `candidate_all_prior_usa_visits_f1` WHERE `sm_memb_id` =:sm_memb_id AND `sevis_id` LIKE :sevis_id";
	$candidate_prior_usa_visits_f1_specific_sevis_id_duplicate_check_select_query = $dbcon->prepare($candidate_prior_usa_visits_f1_specific_sevis_id_duplicate_check_sql);
	$candidate_prior_usa_visits_f1_specific_sevis_id_duplicate_check_select_query->bindValue(":sm_memb_id",$user_id_input);
	$candidate_prior_usa_visits_f1_specific_sevis_id_duplicate_check_select_query->bindValue(":sevis_id",$candidate_sevis_id_search_input);
	$candidate_prior_usa_visits_f1_specific_sevis_id_duplicate_check_select_query->execute();

	if($candidate_prior_usa_visits_f1_specific_sevis_id_duplicate_check_select_query->rowCount() > 0) {
		$candidate_prior_usa_visits_f1_specific_sevis_id_duplicate_check_select_query_result = $candidate_prior_usa_visits_f1_specific_sevis_id_duplicate_check_select_query->fetch();
	     return $candidate_prior_usa_visits_f1_specific_sevis_id_duplicate_check_select_query_result;

	}//close of if($candidate_prior_usa_visits_f1_specific_sevis_id_duplicate_check_select_query->rowCount() > 0) {
	return $constructed_array;

}

function candidate_prior_usa_visits_f1_specific_sevis_id_insert($user_id_input, $visa_type_input, $candidate_sevis_id_input, $added_by_user_type_input, $added_by_user_id_input, $event_datetime_input, $event_datetime_epoch_input) {
    global $dbcon;

	$constructed_array = array();

	$candidate_prior_usa_visits_f1_specific_sevis_id_insert_sql = "INSERT INTO `candidate_all_prior_usa_visits_f1`(`sm_memb_id`, `visa_type`, `sevis_id`, `added_by_user_type`, `added_by_user_id`, `added_date_time`, `added_date_time_epoch`) VALUES (:sm_memb_id,:visa_type,:sevis_id,:added_by_user_type,:added_by_user_id,:added_date_time,:added_date_time_epoch)";
	$candidate_prior_usa_visits_f1_specific_sevis_id_insert_query = $dbcon->prepare($candidate_prior_usa_visits_f1_specific_sevis_id_insert_sql);
	$candidate_prior_usa_visits_f1_specific_sevis_id_insert_query->bindValue(":sm_memb_id",$user_id_input);
	$candidate_prior_usa_visits_f1_specific_sevis_id_insert_query->bindValue(":visa_type",$visa_type_input);
	$candidate_prior_usa_visits_f1_specific_sevis_id_insert_query->bindValue(":sevis_id",$candidate_sevis_id_input);
	$candidate_prior_usa_visits_f1_specific_sevis_id_insert_query->bindValue(":added_by_user_type",$added_by_user_type_input);
	$candidate_prior_usa_visits_f1_specific_sevis_id_insert_query->bindValue(":added_by_user_id",$added_by_user_id_input);
	$candidate_prior_usa_visits_f1_specific_sevis_id_insert_query->bindValue(":added_date_time",$event_datetime_input);
	$candidate_prior_usa_visits_f1_specific_sevis_id_insert_query->bindValue(":added_date_time_epoch",$event_datetime_epoch_input);

	if ($candidate_prior_usa_visits_f1_specific_sevis_id_insert_query->execute()) {

		$last_inserted_id = $dbcon->lastInsertId();

		//$eventLog->log("record inserted successfully");

		$constructed_array["last_inserted_id"] = $last_inserted_id;

		return $constructed_array;

	} else {

		//$eventLog->log("Error occurred during process. Please try again");
		return $constructed_array;

	}//close of else of if ($candidate_prior_usa_visits_f1_specific_sevis_id_insert_query->execute()) {

}

function candidate_prior_usa_visits_eads_availability_mandate_duplicate_check($user_id_input) {
	global $dbcon;
	$constructed_array = array();

	$candidate_prior_usa_visits_eads_availability_mandate_duplicate_check_sql = "SELECT * FROM `candidate_all_prior_usa_visits_eads` WHERE `sm_memb_id` =:sm_memb_id";
	$candidate_prior_usa_visits_eads_availability_mandate_duplicate_check_select_query = $dbcon->prepare($candidate_prior_usa_visits_eads_availability_mandate_duplicate_check_sql);
	$candidate_prior_usa_visits_eads_availability_mandate_duplicate_check_select_query->bindValue(":sm_memb_id",$user_id_input);
	$candidate_prior_usa_visits_eads_availability_mandate_duplicate_check_select_query->execute();

	if($candidate_prior_usa_visits_eads_availability_mandate_duplicate_check_select_query->rowCount() > 0) {
		$candidate_prior_usa_visits_eads_availability_mandate_duplicate_check_select_query_result = $candidate_prior_usa_visits_eads_availability_mandate_duplicate_check_select_query->fetch();
	     return $candidate_prior_usa_visits_eads_availability_mandate_duplicate_check_select_query_result;

	}//close of if($candidate_prior_usa_visits_eads_availability_mandate_duplicate_check_select_query->rowCount() > 0) {
	return $constructed_array;

}

function candidate_prior_usa_visits_eads_availability_mandate_insert($user_id_input, $added_by_user_type_input, $added_by_user_id_input, $event_datetime_input, $event_datetime_epoch_input) {
    global $dbcon;

	$constructed_array = array();

	$candidate_prior_usa_visits_eads_availability_mandate_insert_sql = "INSERT INTO `candidate_all_prior_usa_visits_eads`(`sm_memb_id`, `added_by_user_type`, `added_by_user_id`, `added_date_time`, `added_date_time_epoch`) VALUES (:sm_memb_id,:added_by_user_type,:added_by_user_id,:added_date_time,:added_date_time_epoch)";
	$candidate_prior_usa_visits_eads_availability_mandate_insert_query = $dbcon->prepare($candidate_prior_usa_visits_eads_availability_mandate_insert_sql);
	$candidate_prior_usa_visits_eads_availability_mandate_insert_query->bindValue(":sm_memb_id",$user_id_input);
	$candidate_prior_usa_visits_eads_availability_mandate_insert_query->bindValue(":added_by_user_type",$added_by_user_type_input);
	$candidate_prior_usa_visits_eads_availability_mandate_insert_query->bindValue(":added_by_user_id",$added_by_user_id_input);
	$candidate_prior_usa_visits_eads_availability_mandate_insert_query->bindValue(":added_date_time",$event_datetime_input);
	$candidate_prior_usa_visits_eads_availability_mandate_insert_query->bindValue(":added_date_time_epoch",$event_datetime_epoch_input);

	if ($candidate_prior_usa_visits_eads_availability_mandate_insert_query->execute()) {

		$last_inserted_id = $dbcon->lastInsertId();

		//$eventLog->log("record inserted successfully");

		$constructed_array["last_inserted_id"] = $last_inserted_id;

		return $constructed_array;

	} else {

		//$eventLog->log("Error occurred during process. Please try again");
		return $constructed_array;

	}//close of else of if ($candidate_prior_usa_visits_eads_availability_mandate_insert_query->execute()) {

}
//*************** Recruitment Application related functions *******************

function company_details_duplicate_check_based_on_company_name($company_name_input) {

	global $dbcon;
	$constructed_array = array();
	$company_name = '%'.$company_name_input.'%';
	$company_details_duplicate_check_sql = "SELECT * FROM `companies` WHERE `company_name` LIKE :company_name";
	$company_details_select_query = $dbcon->prepare($company_details_duplicate_check_sql);
	$company_details_select_query->bindValue(":company_name",$company_name);
	$company_details_select_query->execute();

	if($company_details_select_query->rowCount() > 0) {
		$company_details_select_query_result = $company_details_select_query->fetch();
	     return $company_details_select_query_result;

	}//close of if($company_details_select_query_result->rowCount() > 0) {
	return $constructed_array;

}

function company_basic_details_insert($company_name_input,$company_seo_name_input,$company_brand_name_input,$company_registration_type_id_input,$company_registration_type_name_input,$company_registration_type_seo_name_input,$company_mobile_number_input,$company_website_url_input,$company_email_input, $company_support_email_input,$company_mobile_number_code_input,$event_datetime,$current_epoch)



 {
	global $dbcon,$ea_extracted_jwt_token_sub;
	$is_active_status = '0';

	$company_details_insert_sql = "INSERT INTO `companies` (company_name,company_seo_name,company_brand_name,company_registration_type_id,company_registration_type_name,company_registration_type_seo_name,company_mobile_number,company_website_url,company_email,company_support_email,company_mobile_number_country_code,is_active_status,added_datetime, added_datetime_epoch,added_by_admin_user_id) VALUES(:company_name,:company_seo_name,:company_brand_name,:company_registration_type_id,:company_registration_type_name,:company_registration_type_seo_name, :company_mobile_number, :company_website_url,:company_email, :company_support_email,:company_mobile_number_country_code, :is_active_status,:added_datetime, :added_datetime_epoch,:added_by_admin_user_id)";
	$company_details_insert_query = $dbcon->prepare($company_details_insert_sql);
	$company_details_insert_query->bindValue(":company_name",$company_name_input);
	$company_details_insert_query->bindValue(":company_seo_name",$company_seo_name_input);
	$company_details_insert_query->bindValue(":company_brand_name",$company_brand_name_input);
	$company_details_insert_query->bindValue(":company_registration_type_id",$company_registration_type_id_input);
	$company_details_insert_query->bindValue(":company_registration_type_name",$company_registration_type_name_input);
	$company_details_insert_query->bindValue(":company_registration_type_seo_name",$company_registration_type_seo_name_input);
	$company_details_insert_query->bindValue(":company_mobile_number",$company_mobile_number_input);
	$company_details_insert_query->bindValue(":company_website_url",$company_website_url_input);
	
    $company_details_insert_query->bindValue(":company_email",$company_email_input);
	$company_details_insert_query->bindValue(":company_support_email",$company_support_email_input);
	$company_details_insert_query->bindValue(":company_mobile_number_country_code",$company_mobile_number_code_input);
	$company_details_insert_query->bindValue(":added_datetime",$event_datetime);
    $company_details_insert_query->bindValue(":added_datetime_epoch",$current_epoch);
	$company_details_insert_query->bindValue(":added_by_admin_user_id",$ea_extracted_jwt_token_sub);
    $company_details_insert_query->bindValue(":is_active_status",$is_active_status);

		if ($company_details_insert_query->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");

			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}
function job_applicant_invite_duplicate_check_based_on_job_id($job_id_input,$company_id_input,$company_client_id_input,$invitation_received_by_user_id_input) {

	global $dbcon;
	$constructed_array = array();
	$job_applicant_invite_details_duplicate_check_sql = "SELECT * FROM `job_applicant_invites` WHERE `job_id`= :job_id AND `company_id`=:company_id AND`company_client_id`=:company_client_id AND `invitation_received_by_user_id`=:invitation_received_by_user_id";
	$job_applicant_invite_details_select_query = $dbcon->prepare($job_applicant_invite_details_duplicate_check_sql);
	$job_applicant_invite_details_select_query->bindValue(":job_id",$job_id_input);
	$job_applicant_invite_details_select_query->bindValue(":company_id",$company_id_input);
	$job_applicant_invite_details_select_query->bindValue(":company_client_id",$company_client_id_input);
	$job_applicant_invite_details_select_query->bindValue(":invitation_received_by_user_id",$invitation_received_by_user_id_input);
	$job_applicant_invite_details_select_query->execute();

	if($job_applicant_invite_details_select_query->rowCount() > 0) {
		$job_applicant_invite_details_select_query_result = $job_applicant_invite_details_select_query->fetchAll();
	     return $job_applicant_invite_details_select_query_result;

	}//close of if($job_applicant_invite_details_select_query_result ->rowCount() > 0) {
	return $constructed_array;

}


















function job_details_duplicate_check_based_on_company_id($company_id_input,$company_client_id_input,$job_title_input,$job_posted_date_time_from_input,$job_posted_date_time_to_input){

	global $dbcon;
	$constructed_array = array();
	$is_active_status = '1';
	$job_title = '%'.$job_title_input.'%';
	$job_details_duplicate_check_sql = "SELECT * FROM `jobs` WHERE `company_id`= :company_id AND `company_client_id` =:company_client_id AND (`job_posted_date_time` BETWEEN :job_posted_date_time_from AND :job_posted_date_time_to) AND `job_title` LIKE :job_title";

	$job_details_select_query = $dbcon->prepare($job_details_duplicate_check_sql);

	$job_details_select_query->bindValue(":company_id",$company_id_input);
	$job_details_select_query->bindValue(":company_client_id",$company_client_id_input);
	$job_details_select_query->bindValue(":job_title",$job_title_input);
	$job_details_select_query->bindValue(":job_posted_date_time_from",$job_posted_date_time_from_input);
	$job_details_select_query->bindValue(":job_posted_date_time_to",$job_posted_date_time_to_input);
	$job_details_select_query->execute();
	if($job_details_select_query->rowCount() > 0) {
		$job_details_select_query_result = $job_details_select_query->fetch();
	     return $job_details_select_query_result;

	}//close of if($company_details_select_query_result->rowCount() > 0) {
	return $constructed_array;

}

/*function  job_basic_details_insert($company_id_input,$company_client_id_input,$sm_site_member_classification_detail_id,$ea_extracted_jwt_token_sub,$job_title_input,$job_seo_title_input,$job_summary_input,$job_summary_safe_html_input,$job_full_description_input,$job_full_description_safe_html_input,$job_type_id_input,$job_type_name_input,$job_type_seo_name_input,$job_experience_level_id_input,$job_experience_level_name_input,$job_experience_level_seo_name_input,$currency_id_input,$currency_name_input,$currency_seo_name_input,$currency_three_lettered_code_input,$min_compensation_value_input,$max_compensation_value_input,$salary_display_options_input,$job_compensation_period_id_input,$job_compensation_period_name_input,$job_compensation_period_seo_name_input,$job_industry_id_input,$job_industry_name_input,$job_industry_seo_name_input,$job_work_location_requirement_id_input,$job_work_location_requirement_name_input,$job_work_location_requirement_seo_name_input,$city_input,$state_input,$country_input,$available_positions_input,$resume_submission_end_date_input,$resume_submission_end_date_epoch_input,$job_posted_date_time_to_input,$current_epoch,$address_line_1_input,$address_line_2_input,$notes_input,$remarks_input,$no_of_years_experience_input){
	
	global $dbcon,$eventLog;
	$is_active_status = '1';
	$resume_acceptance_status = '1';
	$job_recruitment_status = '1';
	
 

	$job_details_insert_sql = "INSERT INTO `jobs`(`company_id`, `company_client_id`, `job_posted_by_member_classification_detail_id`, `sm_memb_id`, `job_title`, `job_seo_title`, `job_summary`, `job_summary_safe_html`, `job_full_description`, `job_full_description_safe_html`, `job_type_id`, `job_type_name`, `job_type_seo_name`, `job_experience_level_id`, `job_experience_level_name`, `job_experience_level_seo_name`, `currency_id`, `currency_name`, `currency_seo_name`, `currency_three_lettered_code`, `min_compensation_value`, `max_compensation_value`, `salary_display_options`, `job_compensation_period_id`, `job_compensation_period_name`, `job_compensation_period_seo_name`, `job_industry_id`, `job_industry_name`, `job_industry_seo_name`, `job_work_location_requirement_id`, `job_work_location_requirement_name`, `job_work_location_requirement_seo_name`, `city`, `state`, `country`, `available_positions`, `resume_submission_end_date`, `resume_submission_end_date_epoch`, `job_posted_date_time`, `job_posted_date_time_epoch`,`resume_acceptance_status`,`job_recruitment_status`, `is_active_status`,`no_of_years_experience`,`address_line_1`,`address_line_2`,`notes`,`remarks`,`no_of_openings`) VALUES(:company_id,:company_client_id,:job_posted_by_member_classification_detail_id,:sm_memb_id,:job_title,:job_seo_title,:job_summary,:job_summary_safe_html,:job_full_description,:job_full_description_safe_html,:job_type_id,:job_type_name,:job_type_seo_name,:job_experience_level_id,:job_experience_level_name,:job_experience_level_seo_name,:currency_id,:currency_name,:currency_seo_name,:currency_three_lettered_code,:min_compensation_value,:max_compensation_value,:salary_display_options,:job_compensation_period_id,:job_compensation_period_name,:job_compensation_period_seo_name,:job_industry_id,:job_industry_name,:job_industry_seo_name,:job_work_location_requirement_id,:job_work_location_requirement_name,:job_work_location_requirement_seo_name,:city,:state,:country,:available_positions,:resume_submission_end_date,:resume_submission_end_date_epoch,:job_posted_date_time,:job_posted_date_time_epoch,:resume_acceptance_status,:job_recruitment_status,:is_active_status,:no_of_years_experience,:address_line_1,:address_line_2,:notes,:remarks,:no_of_openings)";
	$eventLog->log("after insert function.");
	$job_details_insert_query = $dbcon->prepare($job_details_insert_sql);
	$job_details_insert_query->bindValue(":company_id",$company_id_input);
	$job_details_insert_query->bindValue(":company_client_id",$company_client_id_input);
	$job_details_insert_query->bindValue(":job_posted_by_member_classification_detail_id",$job_posted_by_member_classification_detail_id_input);
	$job_details_insert_query->bindValue(":sm_memb_id",$sm_memb_id_input);
	$job_details_insert_query->bindValue(":job_title",$job_title_input);
	$job_details_insert_query->bindValue(":job_seo_title",$job_seo_title_input);
	$job_details_insert_query->bindValue(":job_summary",$job_summary_input);
	$job_details_insert_query->bindValue(":job_summary_safe_html",$job_summary_safe_html_input);
	$job_details_insert_query->bindValue(":job_full_description",$job_full_description_input);
	$job_details_insert_query->bindValue(":job_full_description_safe_html",$job_full_description_safe_html_input);
    $job_details_insert_query->bindValue(":job_type_id",$job_type_id_input);
	$job_details_insert_query->bindValue(":job_type_name",$job_type_name_input);
	$job_details_insert_query->bindValue(":job_type_seo_name",$job_type_seo_name_input);
	$job_details_insert_query->bindValue(":job_experience_level_id",$job_experience_level_id_input);
	$job_details_insert_query->bindValue(":job_experience_level_name",$job_experience_level_name_input);
	$job_details_insert_query->bindValue(":job_experience_level_seo_name",$job_experience_level_seo_name_input);
	$job_details_insert_query->bindValue(":currency_id",$currency_id_input);
	$job_details_insert_query->bindValue(":currency_name",$currency_name_input);
	$job_details_insert_query->bindValue(":currency_seo_name",$currency_seo_name_input);
	$job_details_insert_query->bindValue(":currency_three_lettered_code",$currency_three_lettered_code_input);
	$job_details_insert_query->bindValue(":min_compensation_value",$min_compensation_value_input);
	$job_details_insert_query->bindValue(":max_compensation_value",$max_compensation_value_input);
	$job_details_insert_query->bindValue(":salary_display_options",$salary_display_options_input);
	$job_details_insert_query->bindValue(":job_compensation_period_id",$job_compensation_period_id_input);
	$job_details_insert_query->bindValue(":job_compensation_period_name",$job_compensation_period_name_input);
	$job_details_insert_query->bindValue(":job_compensation_period_seo_name",$job_compensation_period_seo_name_input);
	$job_details_insert_query->bindValue(":job_industry_id",$job_industry_id_input);
	$job_details_insert_query->bindValue(":job_industry_name",$job_industry_name_input);
	$job_details_insert_query->bindValue(":job_industry_seo_name",$job_industry_seo_name_input);
	$job_details_insert_query->bindValue(":job_work_location_requirement_id",$job_work_location_requirement_id_input);
	$job_details_insert_query->bindValue(":job_work_location_requirement_name",$job_work_location_requirement_name_input);
	$job_details_insert_query->bindValue(":job_work_location_requirement_seo_name",$job_work_location_requirement_seo_name_input);
	$job_details_insert_query->bindValue(":no_of_years_experience",$no_of_years_experience_input);
	$job_details_insert_query->bindValue(":address_line_1",$address_line_1_input);
	$job_details_insert_query->bindValue(":address_line_2",$address_line_2_input);
	$job_details_insert_query->bindValue(":city",$city_input);
	$job_details_insert_query->bindValue(":state",$state_input);
	$job_details_insert_query->bindValue(":country",$country_input);
	$job_details_insert_query->bindValue(":available_positions",$available_positions_input);
	$job_details_insert_query->bindValue(":no_of_openings",$available_positions_input);
	$job_details_insert_query->bindValue(":notes",$notes_input);
	$job_details_insert_query->bindValue(":remarks",$remarks_input);
	$job_details_insert_query->bindValue(":resume_submission_end_date",$resume_submission_end_date_input);
	$job_details_insert_query->bindValue(":resume_submission_end_date_epoch",$resume_submission_end_date_epoch_input);
	$job_details_insert_query->bindValue(":job_posted_date_time",$job_posted_date_time_input);
	$job_details_insert_query->bindValue(":job_posted_date_time_epoch",$job_posted_date_time_epoch_input);
	$job_details_insert_query->bindValue(":resume_acceptance_status",$resume_acceptance_status);
	$job_details_insert_query->bindValue(":job_recruitment_status",$job_recruitment_status);
	$job_details_insert_query->bindValue(":is_active_status",$is_active_status);
       $eventLog->log("after bind value.");
	if ($job_details_insert_query->execute()) {
          $eventLog->log("after execute.");
            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");
           
			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }

}*/

function company_office_address_duplicate_check_based_on_company_id($company_id,$address_line_1_input,$pincode_input) {
	global $dbcon;
	$address_line_1 = '%'.$address_line_1_input.'%';
	$constructed_array = array();
	$company_office_address_duplicate_check_sql = "SELECT * FROM `company_office_addresses` WHERE `company_id` = :company_id AND `address_line_1` LIKE :address_line_1 AND `pincode` = :pincode";
	$company_office_address_select_query = $dbcon->prepare($company_office_address_duplicate_check_sql);
	$company_office_address_select_query->bindValue(":company_id",$company_id);
    $company_office_address_select_query->bindValue(":address_line_1",$address_line_1);
    $company_office_address_select_query->bindValue(":pincode",$pincode_input);
	$company_office_address_select_query->execute();

	if($company_office_address_select_query->rowCount() > 0) {
		$company_office_address_select_query_result = $company_office_address_select_query->fetch();
	     return $company_office_address_select_query_result;

	}//close of if($company_office_address_select_query->rowCount() > 0) {
	return $constructed_array;

}

function company_office_address_record_insert($company_id_input, $premise_name_input, $address_line_1_input, $address_line_2_input, $city_input, $state_input, $country_input, $pincode_input, $event_datetime, $current_epoch){
	global $dbcon;
	$is_active_status = '1';
	$company_office_address_insert_sql = "INSERT INTO `company_office_addresses` (company_id,premise_name,address_line_1,address_line_2,city,state,country,pincode,added_datetime,added_datetime_epoch,is_active_status) VALUES(:company_id, :premise_name, :address_line_1, :address_line_2, :city, :state, :country, :pincode, :added_date_time, :added_date_time_epoch, :is_active_status)";
	$company_office_address_insert_query = $dbcon->prepare($company_office_address_insert_sql);
	$company_office_address_insert_query->bindValue(":company_id",$company_id_input);
	$company_office_address_insert_query->bindValue(":premise_name",$premise_name_input);
	$company_office_address_insert_query->bindValue(":address_line_1",$address_line_1_input);
	$company_office_address_insert_query->bindValue(":address_line_2",$address_line_2_input);
	$company_office_address_insert_query->bindValue(":city",$city_input);
	$company_office_address_insert_query->bindValue(":state",$state_input);
	$company_office_address_insert_query->bindValue(":country",$country_input);
	$company_office_address_insert_query->bindValue(":pincode",$pincode_input);
	$company_office_address_insert_query->bindValue(":added_date_time",$event_datetime);
    $company_office_address_insert_query->bindValue(":added_date_time_epoch",$current_epoch);
	$company_office_address_insert_query->bindValue(":is_active_status",$is_active_status);

		if ($company_office_address_insert_query->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");

			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }
}

function job_applicant_invite_insert($company_id_input,$company_client_id_input,$job_id_input,$invitation_sent_by_user_id,$invitation_received_by_user_id_input,$job_invite_ref_code, $event_datetime, $current_epoch,$event_expiry_date_time,$event_expiry_datetime_epoch){
	global $dbcon;
	$is_active_status = '1';

	$job_applicant_invite_insert_sql = "INSERT INTO `job_applicant_invites` (company_id,company_client_id,job_id,invitation_sent_by_user_id,invitation_received_by_user_id,invite_ref_code,event_date_time,event_date_time_epoch,invite_expiry_date_time,invite_expiry_date_time_epoch,is_active_status) VALUES(:company_id, :company_client_id, :job_id, :invitation_sent_by_user_id, :invitation_received_by_user_id,:invite_ref_code,:event_date_time,:event_date_time_epoch,:invite_expiry_date_time,:invite_expiry_date_time_epoch, :is_active_status)";
	$job_applicant_invite_insert_query = $dbcon->prepare($job_applicant_invite_insert_sql);
	$job_applicant_invite_insert_query->bindValue(":company_id",$company_id_input);
	$job_applicant_invite_insert_query->bindValue(":company_client_id",$company_client_id_input);
	$job_applicant_invite_insert_query->bindValue(":job_id",$job_id_input);
	$job_applicant_invite_insert_query->bindValue(":invitation_sent_by_user_id",$invitation_sent_by_user_id);
	$job_applicant_invite_insert_query->bindValue(":invitation_received_by_user_id",$invitation_received_by_user_id_input);
	$job_applicant_invite_insert_query->bindValue(":invite_ref_code",$job_invite_ref_code);
	$job_applicant_invite_insert_query->bindValue(":event_date_time",$event_datetime);
    $job_applicant_invite_insert_query->bindValue(":event_date_time_epoch",$current_epoch);
	$job_applicant_invite_insert_query->bindValue(":invite_expiry_date_time",$event_expiry_date_time);
	$job_applicant_invite_insert_query->bindValue(":invite_expiry_date_time_epoch",$event_expiry_datetime_epoch);
	$job_applicant_invite_insert_query->bindValue(":is_active_status",$is_active_status);

		if ($job_applicant_invite_insert_query->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");

			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }
}

function company_registration_types_list_info() {

    global $dbcon;
	$constructed_array = array();
	$is_active_status = '1';
		$company_registration_types_list_get_sql = "SELECT * FROM `company_registration_types` WHERE `is_active_status`= :is_active_status";
		$company_registration_types_list_get_select_query = $dbcon->prepare($company_registration_types_list_get_sql);
		$company_registration_types_list_get_select_query->bindValue(":is_active_status",$is_active_status);
		$company_registration_types_list_get_select_query->execute();

	if($company_registration_types_list_get_select_query->rowCount() > 0) {
	    $company_registration_types_list_get_select_query_result = $company_registration_types_list_get_select_query->fetchAll();
		foreach ($company_registration_types_list_get_select_query_result as $company_registration_types_list_get_select_query_result_row) {

			$constructed_string = $company_registration_types_list_get_select_query_result_row["company_registration_type_id"] . ":::::" . $company_registration_types_list_get_select_query_result_row["company_registration_type_name"] . ":::::" . $company_registration_types_list_get_select_query_result_row["company_registraton_type_seo_name"];

			$temp_row_array = array();
		    $temp_row_array["company_registration_type_id"] = $company_registration_types_list_get_select_query_result_row["company_registration_type_id"];
		    $temp_row_array["company_registration_type_name"] = $company_registration_types_list_get_select_query_result_row["company_registration_type_name"];
			$temp_row_array["company_registraton_type_seo_name"] = $company_registration_types_list_get_select_query_result_row["company_registraton_type_seo_name"];
		    $temp_row_array["is_active_status"] = $company_registration_types_list_get_select_query_result_row["is_active_status"];
			$temp_row_array["constructed_string"] = $constructed_string;

		    $constructed_array[] = $temp_row_array;
	    }

		return $constructed_array;
	}
	return $constructed_array;
}

function client_companies_list_specific_company_user_based_on_company_id_input($company_id_input,$client_company_status_input) {

    global $dbcon;
	$constructed_array = array();

	if (is_null($client_company_status_input)) {
		//Get List of Company Clients, irrespective of value of is_active_status
		$client_companies_list_get_sql = "SELECT * FROM `company_clients` WHERE `company_id`=:company_id";
		$client_companies_list_get_select_query = $dbcon->prepare($client_companies_list_get_sql);
		$client_companies_list_get_select_query->bindValue(":company_id",$company_id_input);
		$client_companies_list_get_select_query->execute();
	} else {
		//Get List of Company Clients, with either is_active_status = 0 or is_active_status = 1
		$client_companies_list_get_sql = "SELECT * FROM `company_clients` WHERE `company_id`=:company_id AND `is_active_status`=:is_active_status";
		$client_companies_list_get_select_query = $dbcon->prepare($client_companies_list_get_sql);
		$client_companies_list_get_select_query->bindValue(":company_id",$company_id_input);
		$client_companies_list_get_select_query->bindValue(":is_active_status",$client_company_status_input);
		$client_companies_list_get_select_query->execute();
	}
		/*$client_companies_list_get_sql = "SELECT * FROM `company_clients` WHERE `company_id`=:company_id AND `is_active_status`=:is_active_status";

		$client_companies_list_get_select_query = $dbcon->prepare($client_companies_list_get_sql);
		$client_companies_list_get_select_query->bindValue(":company_id",$company_id_input);
		$client_companies_list_get_select_query->bindValue(":is_active_status",$client_company_status_input);
		$client_companies_list_get_select_query->execute();*/

	if($client_companies_list_get_select_query->rowCount() > 0) {
	    $client_companies_list_get_select_query_result = $client_companies_list_get_select_query->fetchAll();
	    //print_r($user_classification_details_get_select_query_result);
	    foreach ($client_companies_list_get_select_query_result as $client_companies_list_get_select_query_result_row) {
			$temp_row_array = array();
			$temp_row_array["company_client_id"] = $client_companies_list_get_select_query_result_row["company_client_id"];
			$temp_row_array["company_id"] = $client_companies_list_get_select_query_result_row["company_id"];
			$temp_row_array["client_company_name"] = $client_companies_list_get_select_query_result_row["client_company_name"];
			$temp_row_array["client_company_seo_name"] = $client_companies_list_get_select_query_result_row["client_company_seo_name"];
			$temp_row_array["client_company_brand_name"] = $client_companies_list_get_select_query_result_row["client_company_brand_name"];
			$temp_row_array["client_company_email"] = $client_companies_list_get_select_query_result_row["client_company_email"];
			$temp_row_array["client_company_support_email"] = $client_companies_list_get_select_query_result_row["client_company_support_email"];
			$temp_row_array["is_active_status"] = $client_companies_list_get_select_query_result_row["is_active_status"];
			$constructed_array[] = $temp_row_array;
	    }
		return $constructed_array;
	}
	return $constructed_array;
}

/*function get_company_clients_list_with_pagination_inputs($company_id_input,$client_company_status_input, $records_after_input, $number_of_records_input) {

    global $dbcon, $default_number_of_records_pagination; //app/core/main-config.php

	$constructed_array = array();

	if (($records_after_input == "") && ($number_of_records_input == "")) {
		//Give All Data

		if (is_null($client_company_status_input)) {
			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$company_clients_list_get_sql = "SELECT * FROM `company_clients` WHERE `company_id`=:company_id";

			$company_clients_list_get_select_query = $dbcon->prepare($company_clients_list_get_sql);
			$company_clients_list_get_select_query->bindValue(":company_id",$company_id_input);
			$company_clients_list_get_select_query->execute();

		} else {
			//Give List of Companies, based on value of is_active_status
			$company_clients_list_get_sql = "SELECT * FROM `company_clients` WHERE `company_id`=:company_id AND `is_active_status`=:is_active_status";

			$company_clients_list_get_select_query = $dbcon->prepare($company_clients_list_get_sql);
			$company_clients_list_get_select_query->bindValue(":company_id",$company_id_input);
			$company_clients_list_get_select_query->bindValue(":is_active_status",$client_company_status_input);
			$company_clients_list_get_select_query->execute();

		}//close of else of if (is_null($company_status_input)) {

	} else if (($records_after_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than records_after_input, till the number of records, as defined in the $default_number_of_records_pagination

		if (is_null($client_company_status_input)) {
			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$company_clients_list_get_sql = "SELECT * FROM `company_clients` WHERE `company_id`=:company_id AND `company_client_id` >:company_client_id ORDER BY `company_client_id` ASC LIMIT :number_of_records";

			$company_clients_list_get_select_query = $dbcon->prepare($company_clients_list_get_sql);
			$company_clients_list_get_select_query->bindValue(":company_id",$company_id_input);
			$company_clients_list_get_select_query->bindValue(":company_client_id",$records_after_input);
			$company_clients_list_get_select_query->bindValue(":number_of_records",$default_number_of_records_pagination);
			$company_clients_list_get_select_query->execute();

		} else {
			//Give List of Companies, based on value of is_active_status
			$company_clients_list_get_sql = "SELECT * FROM `company_clients` WHERE `company_id`=:company_id AND `is_active_status`=:is_active_status AND `company_client_id` >:company_client_id ORDER BY `company_client_id` ASC LIMIT :number_of_records";

			$company_clients_list_get_select_query = $dbcon->prepare($company_clients_list_get_sql);
			$company_clients_list_get_select_query->bindValue(":company_id",$company_id_input);
			$company_clients_list_get_select_query->bindValue(":is_active_status",$client_company_status_input);
			$company_clients_list_get_select_query->bindValue(":company_client_id",$records_after_input);
			$company_clients_list_get_select_query->bindValue(":number_of_records",$default_number_of_records_pagination);
			$company_clients_list_get_select_query->execute();

		}//close of else of if (is_null($company_status_input)) {




	} else if (($records_after_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records


		if (is_null($client_company_status_input)) {
			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$company_clients_list_get_sql = "SELECT * FROM `company_clients` WHERE `company_id`=:company_id ORDER BY `company_client_id` ASC LIMIT :number_of_records";

			$company_clients_list_get_select_query = $dbcon->prepare($company_clients_list_get_sql);
			$company_clients_list_get_select_query->bindValue(":company_id",$company_id_input);
			$company_clients_list_get_select_query->bindValue(":number_of_records",$number_of_records_input);
			$company_clients_list_get_select_query->execute();

		} else {
			//Give List of Companies, based on value of is_active_status
			$company_clients_list_get_sql = "SELECT * FROM `company_clients` WHERE `company_id`=:company_id AND `is_active_status`=:is_active_status ORDER BY `company_client_id` ASC LIMIT :number_of_records";

			$company_clients_list_get_select_query = $dbcon->prepare($company_clients_list_get_sql);
			$company_clients_list_get_select_query->bindValue(":company_id",$company_id_input);
			$company_clients_list_get_select_query->bindValue(":is_active_status",$client_company_status_input);
			$company_clients_list_get_select_query->bindValue(":number_of_records",$number_of_records_input);
			$company_clients_list_get_select_query->execute();

		}//close of else of if (is_null($company_status_input)) {



	} else {
		//Give Data, from given Record, till the requested number of records
		if (is_null($client_company_status_input)) {
			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$company_clients_list_get_sql = "SELECT * FROM `company_clients` WHERE `company_id`=:company_id AND `company_client_id` > :company_client_id ORDER BY `company_client_id` ASC LIMIT :number_of_records";

			$company_clients_list_get_select_query = $dbcon->prepare($company_clients_list_get_sql);
			$company_clients_list_get_select_query->bindValue(":company_id",$company_id_input);
			$company_clients_list_get_select_query->bindValue(":company_client_id",$records_after_input);
			$company_clients_list_get_select_query->bindValue(":number_of_records",$number_of_records_input);
			$company_clients_list_get_select_query->execute();

		} else {
			//Give List of Companies, based on value of is_active_status
			$company_clients_list_get_sql = "SELECT * FROM `company_clients` WHERE `company_id`=:company_id AND `is_active_status`=:is_active_status AND `company_client_id` > :company_client_id ORDER BY `company_client_id` ASC LIMIT :number_of_records";

			$company_clients_list_get_select_query = $dbcon->prepare($company_clients_list_get_sql);
			$company_clients_list_get_select_query->bindValue(":company_id",$company_id_input);
			$company_clients_list_get_select_query->bindValue(":is_active_status",$client_company_status_input);
			$company_clients_list_get_select_query->bindValue(":company_client_id",$records_after_input);
			$company_clients_list_get_select_query->bindValue(":number_of_records",$number_of_records_input);
			$company_clients_list_get_select_query->execute();

		}//close of else of if (is_null($company_status_input)) {

	}//close of else of if (($records_after_input == "") && ($number_of_records_input == "")) {



	if($company_clients_list_get_select_query->rowCount() > 0) {
	    $company_clients_list_get_select_query_result = $company_clients_list_get_select_query->fetchAll();
	    //print_r($user_classification_details_get_select_query_result);

		foreach ($company_clients_list_get_select_query_result as $company_clients_list_get_select_query_result_row) {

			$temp_row_array = array();
		    $temp_row_array["company_client_id"] = $company_clients_list_get_select_query_result_row["company_client_id"];
		    $temp_row_array["company_id"] = $company_clients_list_get_select_query_result_row["company_id"];
			$temp_row_array["client_company_name"] = $company_clients_list_get_select_query_result_row["client_company_name"];
			$temp_row_array["client_company_seo_name"] = $company_clients_list_get_select_query_result_row["client_company_seo_name"];
		    $temp_row_array["client_company_brand_name"] = $company_clients_list_get_select_query_result_row["client_company_brand_name"];
		    $temp_row_array["client_company_email"] = $company_clients_list_get_select_query_result_row["client_company_email"];
			$temp_row_array["client_company_support_email"] = $company_clients_list_get_select_query_result_row["client_company_support_email"];
			$temp_row_array["is_active_status"] = $company_clients_list_get_select_query_result_row["is_active_status"];

		    $constructed_array[] = $temp_row_array;
	    }//close of foreach ($company_clients_list_get_select_query_result as $company_clients_list_get_select_query_result_row) {

		return $constructed_array;
	}
	return $constructed_array;
}*/

/*function get_jobs_list_with_pagination_inputs($company_id_input,$job_status_input, $company_client_id_input,$page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input) {

    global $dbcon, $default_number_of_records_pagination; //app/core/main-config.php
	global $eventLog;
	$constructed_array = array();
	$eventLog->log("before is_null concept");

	if ($search_criteria_input == "") {
		$search_criteria_input = null;
	}//close of if ($search_criteria_input == "") {

	if (!is_null($search_criteria_input)) {
		//When Search Criteria input is not empty
		$search_criteria_variable = "%" . $search_criteria_input . "%";
		//$search_criteria_in_query = "(`company_name` LIKE '%info%') OR (`company_seo_name` LIKE '%info%') OR (`company_brand_name` LIKE '%info%') OR (`is_active_status` LIKE '%info%')";
		$search_criteria_in_query_with_where_keyword = " WHERE ((`job_id` LIKE :job_id_search_keyword) OR (`company_id` LIKE :company_id_search_keyword) OR  (`company_client_id` LIKE :company_client_id_search_keyword) OR (`job_title` LIKE :job_title_search_keyword) OR (`available_positions` LIKE :available_positions_search_keyword) OR (`city` LIKE :city_search_keyword) OR (`state` LIKE :state_search_keyword) OR (`country` LIKE :country_search_keyword) OR (`job_posted_date_time` LIKE :job_posted_date_time_search_keyword) OR (`job_industry_name` LIKE :job_industry_name_search_keyword) OR (`resume_submission_end_date` LIKE :resume_submission_end_date_search_keyword) OR (`job_recruitment_status` LIKE :job_recruitment_status_search_keyword) OR (`is_active_status` LIKE :is_active_status_search_keyword)) ";
		$eventLog->log("after search criteria concept");
		$search_criteria_in_query_without_where_keyword = " AND ((`job_id` LIKE :job_id_search_keyword) OR (`company_id` LIKE :company_id_search_keyword) OR  (`company_client_id` LIKE :company_client_id_search_keyword) OR (`job_title` LIKE :job_title_search_keyword) OR (`available_positions` LIKE :available_positions_search_keyword) OR (`city` LIKE :city_search_keyword) OR (`state` LIKE :state_search_keyword) OR (`country` LIKE :country_search_keyword) OR (`job_posted_date_time` LIKE :job_posted_date_time_search_keyword) OR (`job_industry_name` LIKE :job_industry_name_search_keyword) OR (`resume_submission_end_date` LIKE :resume_submission_end_date_search_keyword) OR (`job_recruitment_status` LIKE :job_recruitment_status_search_keyword) OR (`is_active_status` LIKE :is_active_status_search_keyword)) ";
	    $eventLog->log("after search criteria without where concept");
	} else {
		//When Search Criteria input is empty
		$search_criteria_in_query_with_where_keyword = " WHERE ";

		$search_criteria_in_query_without_where_keyword = "";

	}//close of else of if (!is_null($search_criteria_input)) {
	$eventLog->log("before sort order concept");

	if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {

		if (!is_null($sort_field_input)) {
			//When the Sort Field input is not empty

			if ($sort_field_input == "job_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `job_id` " . $sort_order_input;

			} else if ($sort_field_input == "company_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_id` " . $sort_order_input;

			} else if ($sort_field_input == "company_client_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_client_id` " . $sort_order_input;

			} else if ($sort_field_input == "job_title") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `job_title` " . $sort_order_input;

			} else if ($sort_field_input == "job_industry_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `job_industry_name` " . $sort_order_input;

			} else if ($sort_field_input == "city") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `city` " . $sort_order_input;

			} else if ($sort_field_input == "state") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `state` " . $sort_order_input;

			} else if ($sort_field_input == "country") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `country` " . $sort_order_input;

			} else if ($sort_field_input == "available_positions") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `available_positions` " . $sort_order_input;

			} else if ($sort_field_input == "resume_submission_end_date") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `resume_submission_end_date` " . $sort_order_input;

			} else if ($sort_field_input == "job_recruitment_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `job_recruitment_status` " . $sort_order_input;

			} else if ($sort_field_input == "job_posted_date_time") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `job_posted_date_time` " . $sort_order_input;

			} else if ($sort_field_input == "job_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `is_active_status` " . $sort_order_input;

			} else {
				//When Sort Field input, is not among the expected values, use primary key, with provided Sorting Order input
				$sort_details_in_query = " ORDER BY `job_id` " . $sort_order_input;
			}

		} else {
			//When the Sort Field input is empty, primary key has to be taken, with provided Sorting Order input
			$sort_details_in_query = " ORDER BY `job_id` " . $sort_order_input;

		}//close of if (!is_null($sort_field_input)) {


	} else {
		//When the Sort Field input is empty, primary key has to be taken, with ASC Order, by default
		$sort_details_in_query = " ORDER BY `job_id` ASC ";

	}//close of else of if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {


	$limit_offset_value = 0;

	if (($page_number_input == "") && ($number_of_records_input == "")) {
		//Give All Data
		//No LIMIT Clause in the Query
		$limit_offset_in_query = "";

	} else if (($page_number_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than page_number_input, till the number of records, as defined in the $default_number_of_records_pagination

		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $default_number_of_records_pagination;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$default_number_of_records_pagination;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $default_number_of_records_pagination;
		}//close of else if of if ($page_number_input == "1") {



	} else if (($page_number_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records
		$limit_offset_in_query = " LIMIT " . $number_of_records_input;

	} else {
		//Give Data, from given Record, till the requested number of records
		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $number_of_records_input;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$number_of_records_input;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $number_of_records_input;
		}//close of else if of if ($page_number_input == "1") {

	}//close of else of if (($page_number_input == "") && ($number_of_records_input == "")) {

	if (is_null($job_status_input)) {

		if (!is_null($search_criteria_input)) {
			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)

			//Define Named parameters and corresponding Values
			$job_named_parameters_values_array_input = array(":job_id_search_keyword" => $search_criteria_variable,":company_id_search_keyword" => $search_criteria_variable, ":company_client_id_search_keyword" => $search_criteria_variable, ":job_title_search_keyword" => $search_criteria_variable, ":job_industry_name_search_keyword" => $search_criteria_variable, ":city_search_keyword" => $search_criteria_variable, ":state_search_keyword" => $search_criteria_variable, ":country_search_keyword" => $search_criteria_variable, ":available_positions_search_keyword" => $search_criteria_variable,":resume_submission_end_date_search_keyword" => $search_criteria_variable,":job_id_search_keyword" => $search_criteria_variable,":job_recruitment_status_search_keyword" => $search_criteria_variable,":job_posted_date_time_search_keyword" => $search_criteria_variable, ":is_active_status_search_keyword" => $search_criteria_variable);

			//Get Jobs List Count
			$jobs_list_count_get_sql = "SELECT COUNT(*) AS count FROM `jobs`" . $search_criteria_in_query_with_where_keyword . $sort_details_in_query;
			$jobs_list_count_get_select_query = $dbcon->prepare($jobs_list_count_get_sql);
			$jobs_list_count_get_select_query->execute($job_named_parameters_values_array_input);

			//Get Jobs List
			$jobs_list_get_sql = "SELECT * FROM `jobs`" . $search_criteria_in_query_with_where_keyword . $sort_details_in_query . $limit_offset_in_query;

			$jobs_list_get_select_query = $dbcon->prepare($jobs_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->execute();*/
			/*$jobs_list_get_select_query->execute($job_named_parameters_values_array_input);

		} else {
			//Give List of Jobs Count, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$jobs_list_count_get_sql = "SELECT COUNT(*) AS count FROM `jobs`" . $sort_details_in_query;
			$jobs_list_count_get_select_query = $dbcon->prepare($jobs_list_count_get_sql);
			$jobs_list_count_get_select_query->execute();


			//Give List of Jobs, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$jobs_list_get_sql = "SELECT * FROM `jobs`" . $sort_details_in_query . $limit_offset_in_query;
			$jobs_list_get_select_query = $dbcon->prepare($jobs_list_get_sql);
			$jobs_list_get_select_query->execute();

		}//close of else of if (!is_null($search_criteria_input)) {

	} else {

		if (!is_null($search_criteria_input)) {

			//Define Named parameters and corresponding Values
			$job_named_parameters_values_array_input = array(":is_active_status" => $job_status_input,":company_id_search_keyword" => $search_criteria_variable, ":company_client_id_search_keyword" => $search_criteria_variable, ":job_title_search_keyword" => $search_criteria_variable, ":job_industry_name_search_keyword" => $search_criteria_variable, ":city_search_keyword" => $search_criteria_variable, ":state_search_keyword" => $search_criteria_variable, ":country_search_keyword" => $search_criteria_variable, ":available_positions_search_keyword" => $search_criteria_variable,":resume_submission_end_date_search_keyword" => $search_criteria_variable,":job_id_search_keyword" => $search_criteria_variable,":job_recruitment_status_search_keyword" => $search_criteria_variable,":job_posted_date_time_search_keyword" => $search_criteria_variable, ":is_active_status_search_keyword" => $search_criteria_variable);

			//Get Companies List Count
			$jobs_list_count_get_sql = "SELECT COUNT(*) AS count FROM `jobs` WHERE `is_active_status`=:is_active_status" . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;
			$jobs_list_count_get_select_query = $dbcon->prepare($jobs_list_count_get_sql);
			$jobs_list_count_get_select_query->execute($job_named_parameters_values_array_input);


			//Give List of Companies, based on value of is_active_status
			$jobs_list_get_sql = "SELECT * FROM `jobs` WHERE `is_active_status`=:is_active_status" . $search_criteria_in_query_without_where_keyword . $sort_details_in_query . $limit_offset_in_query;

			$jobs_list_get_select_query = $dbcon->prepare($jobs_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->execute();*/
			/*$jobs_list_get_select_query->execute($job_named_parameters_values_array_input);

		} else {

			//Define Named parameters and corresponding Values
			$job_named_parameters_values_array_input = array(":is_active_status" => $job_status_input);

			//Give List of Companies Count, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$jobs_list_count_get_sql = "SELECT COUNT(*) AS count FROM `jobs` WHERE `is_active_status`=:is_active_status" . $sort_details_in_query;
			$jobs_list_count_get_select_query = $dbcon->prepare($jobs_list_count_get_sql);
			$jobs_list_count_get_select_query->execute($job_named_parameters_values_array_input);


			//Give List of Companies, based on value of is_active_status
			$jobs_list_get_sql = "SELECT * FROM `jobs` WHERE `is_active_status`=:is_active_status" . $sort_details_in_query . $limit_offset_in_query;

			$jobs_list_get_select_query = $dbcon->prepare($jobs_list_get_sql);
			/*$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->execute();*/
			/*$jobs_list_get_select_query->execute($job_named_parameters_values_array_input);

		}//close of else of if (!is_null($search_criteria_input)) {



	}//close of else of if (is_null($job_status_input)) {

	//Process / Get Companies Count
	if($jobs_list_count_get_select_query->rowCount() > 0) {
	    $jobs_list_count_get_select_query_result = $jobs_list_count_get_select_query->fetch();
	    //print_r($jobs_list_count_get_select_query_result);

		$total_jobs_count = $jobs_list_count_get_select_query_result["count"];
		$constructed_array["total_records_count"] = $total_jobs_count;

	}//close of if($jobs_list_count_get_select_query->rowCount() > 0) {

	//Process / Fetch Companies List
	if($jobs_list_get_select_query->rowCount() > 0) {
	    $jobs_list_get_select_query_result = $jobs_list_get_select_query->fetchAll();
	    //print_r($companies_list_get_select_query_result);

		foreach ($jobs_list_get_select_query_result as $jobs_list_get_select_query_result_row) {

			$temp_row_array = array();
		    $temp_row_array["job_id"] = $jobs_list_get_select_query_result_row["job_id"];
		    $temp_row_array["company_id"] = $jobs_list_get_select_query_result_row["company_id"];
			$temp_row_array["company_client_id"] = $jobs_list_get_select_query_result_row["company_client_id"];
		    $temp_row_array["job_title"] = $jobs_list_get_select_query_result_row["job_title"];
		    $temp_row_array["job_industry_name"] = $jobs_list_get_select_query_result_row["job_industry_name"];
			$temp_row_array["city"] = $jobs_list_get_select_query_result_row["city"];
			$temp_row_array["state"] = $jobs_list_get_select_query_result_row["state"];
			$temp_row_array["country"] = $jobs_list_get_select_query_result_row["country"];
			$temp_row_array["available_positions"] = $jobs_list_get_select_query_result_row["available_positions"];
			$temp_row_array["resume_submission_end_date"] = $jobs_list_get_select_query_result_row["resume_submission_end_date"];
			$temp_row_array["job_posted_date_time"] = $jobs_list_get_select_query_result_row["job_posted_date_time"];
			$temp_row_array["job_recruitment_status"] = $jobs_list_get_select_query_result_row["job_recruitment_status"];
			$temp_row_array["job_status"] = $jobs_list_get_select_query_result_row["is_active_status"];

		    $constructed_array["list"][] = $temp_row_array;
	    }//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

	}//close of if($companies_list_get_select_query->rowCount() > 0) {

	return $constructed_array;
}*/

/* function get_jobs_list_with_pagination_inputs($company_id_input,$job_status_input, $company_client_id_input,$page_number_input, $number_of_records_input, $sort_field_input, $sort_order_input, $search_criteria_input,$job_recruitment_status_input,$resume_acceptance_status_input) {

    global $dbcon, $default_number_of_records_pagination; //app/core/main-config.php
	global $eventLog;
	$constructed_array = array();
	$eventLog->log("before is_null concept");

	if ($search_criteria_input == "") {
		$search_criteria_input = null;
	}//close of if ($search_criteria_input == "") {

	if (!is_null($search_criteria_input)) {
		//When Search Criteria input is not empty
		$search_criteria_variable = "%" . $search_criteria_input . "%";
		//$search_criteria_in_query = "(`company_name` LIKE '%info%') OR (`company_seo_name` LIKE '%info%') OR (`company_brand_name` LIKE '%info%') OR (`is_active_status` LIKE '%info%')";
		$search_criteria_in_query_with_where_keyword = " WHERE ((`job_id` LIKE :job_id_search_keyword) OR (`company_id` LIKE :company_id_search_keyword) OR  (`company_client_id` LIKE :company_client_id_search_keyword) OR (`job_title` LIKE :job_title_search_keyword) OR (`available_positions` LIKE :available_positions_search_keyword) OR (`city` LIKE :city_search_keyword) OR (`state` LIKE :state_search_keyword) OR (`country` LIKE :country_search_keyword) OR (`job_posted_date_time` LIKE :job_posted_date_time_search_keyword) OR (`job_industry_name` LIKE :job_industry_name_search_keyword) OR (`resume_submission_end_date` LIKE :resume_submission_end_date_search_keyword) OR (`job_recruitment_status` LIKE :job_recruitment_status_search_keyword) OR (`is_active_status` LIKE :is_active_status_search_keyword)) ";
		$eventLog->log("after search criteria concept");
		$search_criteria_in_query_without_where_keyword = " AND ((`job_id` LIKE :job_id_search_keyword) OR (`company_id` LIKE :company_id_search_keyword) OR  (`company_client_id` LIKE :company_client_id_search_keyword) OR (`job_title` LIKE :job_title_search_keyword) OR (`available_positions` LIKE :available_positions_search_keyword) OR (`city` LIKE :city_search_keyword) OR (`state` LIKE :state_search_keyword) OR (`country` LIKE :country_search_keyword) OR (`job_posted_date_time` LIKE :job_posted_date_time_search_keyword) OR (`job_industry_name` LIKE :job_industry_name_search_keyword) OR (`resume_submission_end_date` LIKE :resume_submission_end_date_search_keyword) OR (`job_recruitment_status` LIKE :job_recruitment_status_search_keyword) OR (`is_active_status` LIKE :is_active_status_search_keyword)) ";
	    $eventLog->log("after search criteria without where concept");
	} else {
		//When Search Criteria input is empty
		$search_criteria_in_query_with_where_keyword = " WHERE ";

		$search_criteria_in_query_without_where_keyword = "";

	}//close of else of if (!is_null($search_criteria_input)) {
	$eventLog->log("before sort order concept");
	
	if ($company_client_id_input == "") {
		
		$company_client_id_in_query = "";
	
	} else {
		//When Search Criteria input is empty
		$company_client_id_in_query = "AND `company_client_id`=:company_client_id";
	}
	$recruitment_status_add_query = "AND `job_recruitment_status`=:job_recruitment_status AND `resume_acceptance_status`=:resume_acceptance_status ";
	

	if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {

		if (!is_null($sort_field_input)) {
			//When the Sort Field input is not empty

			if ($sort_field_input == "job_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `job_id` " . $sort_order_input;

			} else if ($sort_field_input == "company_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_id` " . $sort_order_input;

			} else if ($sort_field_input == "company_client_id") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `company_client_id` " . $sort_order_input;

			} else if ($sort_field_input == "job_title") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `job_title` " . $sort_order_input;

			} else if ($sort_field_input == "job_industry_name") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `job_industry_name` " . $sort_order_input;

			} else if ($sort_field_input == "city") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `city` " . $sort_order_input;

			} else if ($sort_field_input == "state") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `state` " . $sort_order_input;

			} else if ($sort_field_input == "country") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `country` " . $sort_order_input;

			} else if ($sort_field_input == "available_positions") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `available_positions` " . $sort_order_input;

			} else if ($sort_field_input == "resume_submission_end_date") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `resume_submission_end_date` " . $sort_order_input;

			} else if ($sort_field_input == "job_recruitment_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `job_recruitment_status` " . $sort_order_input;
				
			} else if ($sort_field_input == "resume_acceptance_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `resume_acceptance_status` " . $sort_order_input;

			} else if ($sort_field_input == "job_posted_date_time") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `job_posted_date_time` " . $sort_order_input;

			} else if ($sort_field_input == "job_status") {
				//Do Input the Column Name w.r.t. specific DB Table, that corresponds to the given field name input
				$sort_details_in_query = " ORDER BY `is_active_status` " . $sort_order_input;

			} else {
				//When Sort Field input, is not among the expected values, use primary key, with provided Sorting Order input
				$sort_details_in_query = " ORDER BY `job_id` " . $sort_order_input;
			}

		} else {
			//When the Sort Field input is empty, primary key has to be taken, with provided Sorting Order input
			$sort_details_in_query = " ORDER BY `job_id` " . $sort_order_input;

		}//close of if (!is_null($sort_field_input)) {


	} else {
		//When the Sort Field input is empty, primary key has to be taken, with ASC Order, by default
		$sort_details_in_query = " ORDER BY `job_id` ASC ";

	}//close of else of if (($sort_order_input == "ASC") || ($sort_order_input == "DESC")) {


	$limit_offset_value = 0;

	if (($page_number_input == "") && ($number_of_records_input == "")) {
		//Give All Data
		//No LIMIT Clause in the Query
		$limit_offset_in_query = "";

	} else if (($page_number_input != "") && ($number_of_records_input == "")) {
		//Give Data, whose primary key id value is greater than page_number_input, till the number of records, as defined in the $default_number_of_records_pagination

		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $default_number_of_records_pagination;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$default_number_of_records_pagination;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $default_number_of_records_pagination;
		}//close of else if of if ($page_number_input == "1") {



	} else if (($page_number_input == "") && ($number_of_records_input != "")) {
		//Give Data, from first record, till the requested number of records
		$limit_offset_in_query = " LIMIT " . $number_of_records_input;

	} else {
		//Give Data, from given Record, till the requested number of records
		if ($page_number_input == "1") {
			$limit_offset_in_query = " LIMIT 0, " . $number_of_records_input;
		} else if ($page_number_input >= "2") {
			$limit_offset_value = ($page_number_input-1)*$number_of_records_input;
			$limit_offset_in_query = " LIMIT " . $limit_offset_value . ", " . $number_of_records_input;
		}//close of else if of if ($page_number_input == "1") {

	}//close of else of if (($page_number_input == "") && ($number_of_records_input == "")) {
	
	if ($job_status_input == "") {

		if (!is_null($search_criteria_input)) {
			
			//Give List of Companies, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)

			//Define Named parameters and corresponding Values
			
			if($company_client_id_input=="") {
				$job_named_parameters_values_array_input = array(":company_id" => $company_id_input,":job_recruitment_status" => $job_recruitment_status_input,":resume_acceptance_status" => $resume_acceptance_status_input,":job_id_search_keyword" => $search_criteria_variable,":company_id_search_keyword" => $search_criteria_variable, ":company_client_id_search_keyword" => $search_criteria_variable, ":job_title_search_keyword" => $search_criteria_variable, ":job_industry_name_search_keyword" => $search_criteria_variable, ":city_search_keyword" => $search_criteria_variable, ":state_search_keyword" => $search_criteria_variable, ":country_search_keyword" => $search_criteria_variable, ":available_positions_search_keyword" => $search_criteria_variable,":resume_submission_end_date_search_keyword" => $search_criteria_variable,":job_id_search_keyword" => $search_criteria_variable,":job_recruitment_status_search_keyword" => $search_criteria_variable,":job_posted_date_time_search_keyword" => $search_criteria_variable, ":is_active_status_search_keyword" => $search_criteria_variable);    
			} else {
				$job_named_parameters_values_array_input = array(":company_id" => $company_id_input,":job_recruitment_status" => $job_recruitment_status_input,":resume_acceptance_status" => $resume_acceptance_status_input,":company_client_id" => $company_client_id_input,":job_id_search_keyword" => $search_criteria_variable,":company_id_search_keyword" => $search_criteria_variable, ":company_client_id_search_keyword" => $search_criteria_variable, ":job_title_search_keyword" => $search_criteria_variable, ":job_industry_name_search_keyword" => $search_criteria_variable, ":city_search_keyword" => $search_criteria_variable, ":state_search_keyword" => $search_criteria_variable, ":country_search_keyword" => $search_criteria_variable, ":available_positions_search_keyword" => $search_criteria_variable,":resume_submission_end_date_search_keyword" => $search_criteria_variable,":job_id_search_keyword" => $search_criteria_variable,":job_recruitment_status_search_keyword" => $search_criteria_variable,":job_posted_date_time_search_keyword" => $search_criteria_variable, ":is_active_status_search_keyword" => $search_criteria_variable);
			}
			
			//Get Jobs List Count
			$jobs_list_count_get_sql = "SELECT COUNT(*) AS count FROM `jobs` WHERE `company_id`=:company_id " . $recruitment_status_add_query . $company_client_id_in_query . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;
			$eventLog->log($jobs_list_count_get_sql);
			$jobs_list_count_get_select_query = $dbcon->prepare($jobs_list_count_get_sql);
			$jobs_list_count_get_select_query->execute($job_named_parameters_values_array_input);

			//Get Jobs List
			$jobs_list_get_sql = "SELECT * FROM `jobs` WHERE `company_id`=:company_id " . $recruitment_status_add_query . $company_client_id_in_query . $search_criteria_in_query_without_where_keyword . $sort_details_in_query . $limit_offset_in_query;

			$jobs_list_get_select_query = $dbcon->prepare($jobs_list_get_sql);
			/*******$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->execute();********/
			/*$jobs_list_get_select_query->execute($job_named_parameters_values_array_input);

		} else {
			
			
			//Give List of Jobs Count, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			if ($company_client_id_input == "") {
			  $job_named_parameters_values_array_input = array(":company_id" => $company_id_input,":job_recruitment_status" => $job_recruitment_status_input,":resume_acceptance_status" => $resume_acceptance_status_input);
			  
			} else {
				$job_named_parameters_values_array_input = array(":company_id" => $company_id_input,":job_recruitment_status" => $job_recruitment_status_input,":resume_acceptance_status" => $resume_acceptance_status_input,":company_client_id" => $company_client_id_input);
			}
			$jobs_list_count_get_sql = "SELECT COUNT(*) AS count FROM `jobs` WHERE `company_id`=:company_id ". $recruitment_status_add_query . $company_client_id_in_query . $sort_details_in_query;
			$eventLog->log($jobs_list_count_get_sql);
			$jobs_list_count_get_select_query = $dbcon->prepare($jobs_list_count_get_sql);
			$jobs_list_count_get_select_query->execute($job_named_parameters_values_array_input);


			//Give List of Jobs, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$jobs_list_get_sql = "SELECT * FROM `jobs` WHERE `company_id`=:company_id " . $recruitment_status_add_query . $company_client_id_in_query . $sort_details_in_query . $limit_offset_in_query;
			$jobs_list_get_select_query = $dbcon->prepare($jobs_list_get_sql);
			$jobs_list_get_select_query->execute($job_named_parameters_values_array_input);

		}//close of else of if (!is_null($search_criteria_input)) {

	} else {

		if (!is_null($search_criteria_input)) {

			//Define Named parameters and corresponding Values
			if ($company_client_id_input == "") {
				$job_named_parameters_values_array_input = array(":is_active_status" => $job_status_input,":company_id" => $company_id_input,":job_recruitment_status" => $job_recruitment_status_input,":resume_acceptance_status" => $resume_acceptance_status_input,":company_id_search_keyword" => $search_criteria_variable, ":company_client_id_search_keyword" => $search_criteria_variable, ":job_title_search_keyword" => $search_criteria_variable, ":job_industry_name_search_keyword" => $search_criteria_variable, ":city_search_keyword" => $search_criteria_variable, ":state_search_keyword" => $search_criteria_variable, ":country_search_keyword" => $search_criteria_variable, ":available_positions_search_keyword" => $search_criteria_variable,":resume_submission_end_date_search_keyword" => $search_criteria_variable,":job_id_search_keyword" => $search_criteria_variable,":job_recruitment_status_search_keyword" => $search_criteria_variable,":job_posted_date_time_search_keyword" => $search_criteria_variable, ":is_active_status_search_keyword" => $search_criteria_variable);
            } else {
				$job_named_parameters_values_array_input = array(":is_active_status" => $job_status_input,":company_id" => $company_id_input,":job_recruitment_status" => $job_recruitment_status_input,":resume_acceptance_status" => $resume_acceptance_status_input,":company_client_id" => $company_client_id_input,":company_id_search_keyword" => $search_criteria_variable, ":company_client_id_search_keyword" => $search_criteria_variable, ":job_title_search_keyword" => $search_criteria_variable, ":job_industry_name_search_keyword" => $search_criteria_variable, ":city_search_keyword" => $search_criteria_variable, ":state_search_keyword" => $search_criteria_variable, ":country_search_keyword" => $search_criteria_variable, ":available_positions_search_keyword" => $search_criteria_variable,":resume_submission_end_date_search_keyword" => $search_criteria_variable,":job_id_search_keyword" => $search_criteria_variable,":job_recruitment_status_search_keyword" => $search_criteria_variable,":job_posted_date_time_search_keyword" => $search_criteria_variable, ":is_active_status_search_keyword" => $search_criteria_variable);
			}
			//Get Companies List Count
			$jobs_list_count_get_sql = "SELECT COUNT(*) AS count FROM `jobs` WHERE `is_active_status`=:is_active_status AND `company_id`=:company_id " . $recruitment_status_add_query . $company_client_id_in_query . $search_criteria_in_query_without_where_keyword . $sort_details_in_query;
			$jobs_list_count_get_select_query = $dbcon->prepare($jobs_list_count_get_sql);
			$jobs_list_count_get_select_query->execute($job_named_parameters_values_array_input);


			//Give List of Companies, based on value of is_active_status
			$jobs_list_get_sql = "SELECT * FROM `jobs` WHERE `is_active_status`=:is_active_status AND `company_id`=:company_id " . $recruitment_status_add_query . $company_client_id_in_query . $search_criteria_in_query_without_where_keyword . $sort_details_in_query . $limit_offset_in_query;

			$jobs_list_get_select_query = $dbcon->prepare($jobs_list_get_sql);
			/******$companies_list_get_select_query->bindValue(":company_id_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_seo_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":company_brand_name_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status_search_keyword",$search_criteria_variable);
			$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->execute();*********/
			/*$jobs_list_get_select_query->execute($job_named_parameters_values_array_input);

		} else {
			//Define Named parameters and corresponding Values
			if($company_client_id_input == "") {
				$job_named_parameters_values_array_input = array(":is_active_status" => $job_status_input,":company_id" => $company_id_input,":job_recruitment_status" => $job_recruitment_status_input,":resume_acceptance_status" => $resume_acceptance_status_input);
            } else {
				$job_named_parameters_values_array_input = array(":is_active_status" => $job_status_input,":company_id" => $company_id_input,":job_recruitment_status" => $job_recruitment_status_input,":resume_acceptance_status" => $resume_acceptance_status_input,":company_client_id" => $company_client_id_input);
			}
			//Give List of Companies Count, irrespective of value of is_active_status (i.e. include both Active and In-active Companies)
			$jobs_list_count_get_sql = "SELECT COUNT(*) AS count FROM `jobs` WHERE `is_active_status`=:is_active_status AND `company_id`=:company_id " . $recruitment_status_add_query . $company_client_id_in_query . $sort_details_in_query;
			$eventLog->log($jobs_list_count_get_sql);
			$jobs_list_count_get_select_query = $dbcon->prepare($jobs_list_count_get_sql);
			$jobs_list_count_get_select_query->execute($job_named_parameters_values_array_input);
		

			//Give List of Companies, based on value of is_active_status
			$jobs_list_get_sql = "SELECT * FROM `jobs` WHERE `is_active_status`=:is_active_status AND `company_id`=:company_id " . $recruitment_status_add_query . $company_client_id_in_query . $sort_details_in_query . $limit_offset_in_query;
            $eventLog->log($jobs_list_get_sql);
			$jobs_list_get_select_query = $dbcon->prepare($jobs_list_get_sql);
			/********$companies_list_get_select_query->bindValue(":is_active_status",$company_status_input);
			$companies_list_get_select_query->execute();*********/
			/*$jobs_list_get_select_query->execute($job_named_parameters_values_array_input);
			

		}//close of else of if (!is_null($search_criteria_input)) {



	}//close of else of if (is_null($job_status_input)) {

	//Process / Get Companies Count
	if($jobs_list_count_get_select_query->rowCount() > 0) {
	    $jobs_list_count_get_select_query_result = $jobs_list_count_get_select_query->fetch();
	    //print_r($jobs_list_count_get_select_query_result);

		$total_jobs_count = $jobs_list_count_get_select_query_result["count"];
		$constructed_array["total_records_count"] = $total_jobs_count;

	}//close of if($jobs_list_count_get_select_query->rowCount() > 0) {

	//Process / Fetch Companies List
	if($jobs_list_get_select_query->rowCount() > 0) {
	    $jobs_list_get_select_query_result = $jobs_list_get_select_query->fetchAll();
	    //print_r($companies_list_get_select_query_result);

		foreach ($jobs_list_get_select_query_result as $jobs_list_get_select_query_result_row) {

			$temp_row_array = array();
		    $temp_row_array["job_id"] = $jobs_list_get_select_query_result_row["job_id"];
		    $temp_row_array["company_id"] = $jobs_list_get_select_query_result_row["company_id"];
			$temp_row_array["company_client_id"] = $jobs_list_get_select_query_result_row["company_client_id"];
		    $temp_row_array["job_title"] = $jobs_list_get_select_query_result_row["job_title"];
		    $temp_row_array["job_industry_name"] = $jobs_list_get_select_query_result_row["job_industry_name"];
			$temp_row_array["city"] = $jobs_list_get_select_query_result_row["city"];
			$temp_row_array["state"] = $jobs_list_get_select_query_result_row["state"];
			$temp_row_array["country"] = $jobs_list_get_select_query_result_row["country"];
			$temp_row_array["available_positions"] = $jobs_list_get_select_query_result_row["available_positions"];
			$temp_row_array["resume_submission_end_date"] = $jobs_list_get_select_query_result_row["resume_submission_end_date"];
			$temp_row_array["job_posted_date_time"] = $jobs_list_get_select_query_result_row["job_posted_date_time"];
			$temp_row_array["job_recruitment_status"] = $jobs_list_get_select_query_result_row["job_recruitment_status"];
			$temp_row_array["resume_acceptance_status"] = $jobs_list_get_select_query_result_row["resume_acceptance_status"];
			$temp_row_array["job_status"] = $jobs_list_get_select_query_result_row["is_active_status"];

		    $constructed_array["list"][] = $temp_row_array;
	    }//close of foreach ($companies_list_get_select_query_result as $companies_list_get_select_query_result_row) {

	}//close of if($companies_list_get_select_query->rowCount() > 0) {

	return $constructed_array;
}
 ********/
function client_company_office_addresses_list_specific_company_user_based_on_company_id_input($company_id_input,$company_client_id_input,$client_company_status_input) {

    global $dbcon, $eventLog;
	$constructed_array = array();
	$eventLog->log("before query");
	if($client_company_status_input != ""){
		$client_company_office_addresses_list_get_sql = "SELECT * FROM `company_client_office_addresses` WHERE `company_id`=:company_id AND `company_client_id`=:company_client_id AND `is_active_status`=:is_active_status";
    } else {
		$client_company_office_addresses_list_get_sql = "SELECT * FROM `company_client_office_addresses` WHERE `company_id`=:company_id AND `company_client_id`=:company_client_id";
	}
		$client_company_office_addresses_list_get_select_query = $dbcon->prepare($client_company_office_addresses_list_get_sql);
		$client_company_office_addresses_list_get_select_query->bindValue(":company_id",$company_id_input);
		$client_company_office_addresses_list_get_select_query->bindValue(":company_client_id",$company_client_id_input);
		if($client_company_status_input != "") {
			$client_company_office_addresses_list_get_select_query->bindValue(":is_active_status",$client_company_status_input);
		}
		$client_company_office_addresses_list_get_select_query->execute();
			$eventLog->log("after query");

	if($client_company_office_addresses_list_get_select_query->rowCount() > 0) {
			$eventLog->log("after if condition");
	    $client_company_office_addresses_list_get_select_query_result = $client_company_office_addresses_list_get_select_query->fetchAll();
	    //print_r($user_classification_details_get_select_query_result);
	    foreach ($client_company_office_addresses_list_get_select_query_result as $client_company_office_addresses_list_get_select_query_result_row) {
			$temp_row_array = array();
			$temp_row_array["company_id"] = $client_company_office_addresses_list_get_select_query_result_row["company_id"];
			$temp_row_array["company_client_id"] = $client_company_office_addresses_list_get_select_query_result_row["company_client_id"];
			$temp_row_array["company_client_office_address_id"] = $client_company_office_addresses_list_get_select_query_result_row["company_client_office_address_id"];
			$temp_row_array["premise_name"] = $client_company_office_addresses_list_get_select_query_result_row["premise_name"];
			$temp_row_array["address_line_1"] = $client_company_office_addresses_list_get_select_query_result_row["address_line_1"];
			$temp_row_array["address_line_2"] = $client_company_office_addresses_list_get_select_query_result_row["address_line_2"];
			$temp_row_array["city"] = $client_company_office_addresses_list_get_select_query_result_row["city"];
			$temp_row_array["state"] = $client_company_office_addresses_list_get_select_query_result_row["state"];
			$temp_row_array["country"] = $client_company_office_addresses_list_get_select_query_result_row["country"];
			$temp_row_array["pincode"] = $client_company_office_addresses_list_get_select_query_result_row["pincode"];
			$temp_row_array["is_active_status"] = $client_company_office_addresses_list_get_select_query_result_row["is_active_status"];
			$constructed_array[] = $temp_row_array;
	    }
		return $constructed_array;
		//return $client_company_office_addresses_list_get_select_query_result;
	}
	return $constructed_array;
}
function company_office_addresses_list_specific_company_user_based_on_company_id_input($company_id_input,$company_status_input) {

    global $dbcon;
	$constructed_array = array();
		$company_office_addresses_list_get_sql = "SELECT * FROM `company_office_addresses` WHERE `company_id`=:company_id AND `is_active_status`=:is_active_status";
		$company_office_addresses_list_get_select_query = $dbcon->prepare($company_office_addresses_list_get_sql);
		$company_office_addresses_list_get_select_query->bindValue(":company_id",$company_id_input);
		$company_office_addresses_list_get_select_query->bindValue(":is_active_status",$company_status_input);
		$company_office_addresses_list_get_select_query->execute();

	if($company_office_addresses_list_get_select_query->rowCount() > 0) {
	    $company_office_addresses_list_get_select_query_result = $company_office_addresses_list_get_select_query->fetchAll();
	    //print_r($user_classification_details_get_select_query_result);
	    foreach ($company_office_addresses_list_get_select_query_result as $company_office_addresses_list_get_select_query_result_row) {
			$temp_row_array = array();
			$temp_row_array["company_id"] = $company_office_addresses_list_get_select_query_result_row["company_id"];
			$temp_row_array["premise_name"] = $company_office_addresses_list_get_select_query_result_row["premise_name"];
			$temp_row_array["address_line_1"] = $company_office_addresses_list_get_select_query_result_row["address_line_1"];
			$temp_row_array["address_line_2"] = $company_office_addresses_list_get_select_query_result_row["address_line_2"];
			$temp_row_array["city"] = $company_office_addresses_list_get_select_query_result_row["city"];
			$temp_row_array["state"] = $company_office_addresses_list_get_select_query_result_row["state"];
			$temp_row_array["country"] = $company_office_addresses_list_get_select_query_result_row["country"];
			$temp_row_array["pincode"] = $company_office_addresses_list_get_select_query_result_row["pincode"];
			$temp_row_array["is_active_status"] = $company_office_addresses_list_get_select_query_result_row["is_active_status"];
			$constructed_array[] = $temp_row_array;
	    }
		return $constructed_array;
		//return $company_office_addresses_list_get_select_query_result;
	}
	return $constructed_array;
}

function job_types_list_info() {

    global $dbcon;
	$constructed_array = array();
	$is_active_status = '1';
		$job_types_list_get_sql = "SELECT * FROM `job_types` WHERE `is_active_status`= :is_active_status";
		$job_types_list_get_select_query = $dbcon->prepare($job_types_list_get_sql);
		$job_types_list_get_select_query->bindValue(":is_active_status",$is_active_status);
		$job_types_list_get_select_query->execute();

	if($job_types_list_get_select_query->rowCount() > 0) {
	    $job_types_list_get_select_query_result = $job_types_list_get_select_query->fetchAll();
		foreach ($job_types_list_get_select_query_result as $job_types_list_get_select_query_result_row) {
			$constructed_string =$job_types_list_get_select_query_result_row["job_type_id"]. ":::::" . $job_types_list_get_select_query_result_row["job_type_name"] .":::::" . $job_types_list_get_select_query_result_row["job_type_seo_name"];

			$temp_row_array = array();
		    $temp_row_array["job_type_id"] = $job_types_list_get_select_query_result_row["job_type_id"];
		    $temp_row_array["job_type_name"] = $job_types_list_get_select_query_result_row["job_type_name"];
			$temp_row_array["job_type_seo_name"] = $job_types_list_get_select_query_result_row["job_type_seo_name"];
		    $temp_row_array["is_active_status"] = $job_types_list_get_select_query_result_row["is_active_status"];
		    $temp_row_array["constructed_string"] = $constructed_string;
		    $constructed_array[] = $temp_row_array;
	    }

		return $constructed_array;
	}
	return $constructed_array;
}

function currency_types_list_info() {

    global $dbcon;
	$constructed_array = array();
	$is_active_status = '1';
		$currency_types_list_get_sql = "SELECT * FROM `currencies` WHERE `is_active_status`= :is_active_status";
		$currency_types_list_get_select_query = $dbcon->prepare($currency_types_list_get_sql);
		$currency_types_list_get_select_query->bindValue(":is_active_status",$is_active_status);
		$currency_types_list_get_select_query->execute();

	if($currency_types_list_get_select_query->rowCount() > 0) {
	    $currency_types_list_get_select_query_result = $currency_types_list_get_select_query->fetchAll();
		foreach ($currency_types_list_get_select_query_result as $currency_types_list_get_select_query_result_row) {
			$constructed_string = $currency_types_list_get_select_query_result_row["currency_id"]. ":::::" .$currency_types_list_get_select_query_result_row["currency_name"]. ":::::" .$currency_types_list_get_select_query_result_row["currency_seo_name"]. ":::::" .$currency_types_list_get_select_query_result_row["currency_three_lettered_code"];
			$temp_row_array = array();
		    $temp_row_array["currency_id"] = $currency_types_list_get_select_query_result_row["currency_id"];
		    $temp_row_array["currency_name"] = $currency_types_list_get_select_query_result_row["currency_name"];
			$temp_row_array["currency_seo_name"] = $currency_types_list_get_select_query_result_row["currency_seo_name"];
			$temp_row_array["currency_three_lettered_code"] = $currency_types_list_get_select_query_result_row["currency_three_lettered_code"];
		    $temp_row_array["is_active_status"] = $currency_types_list_get_select_query_result_row["is_active_status"];
			$temp_row_array["constructed_string"] = $constructed_string;
		    $constructed_array[] = $temp_row_array;
	    }

		return $constructed_array;
	}
	return $constructed_array;
}
function countries_list_info($country_status_input) {

    global $dbcon;
	$constructed_array = array();
	
	   if(($country_status_input == '1')|| ($country_status_input == '0')){
		$countries_list_get_sql = "SELECT * FROM `countries` WHERE `is_active_status`= :is_active_status";
		$countries_list_get_select_query = $dbcon->prepare($countries_list_get_sql);
		$countries_list_get_select_query->bindValue(":is_active_status",$country_status_input);
		$countries_list_get_select_query->execute();
	   } else {
		$countries_list_get_sql = "	SELECT * FROM `countries` ";
        $countries_list_get_select_query = $dbcon->prepare($countries_list_get_sql);	
		$countries_list_get_select_query->execute();  
	   }  

	if($countries_list_get_select_query->rowCount() > 0) {
	    $countries_list_get_select_query_result = $countries_list_get_select_query->fetchAll();
		foreach ($countries_list_get_select_query_result as $countries_list_get_select_query_result_row) {
			
			$temp_row_array = array();
		    $temp_row_array["country_id"] = $countries_list_get_select_query_result_row["country_id"];
		    $temp_row_array["country_name"] = $countries_list_get_select_query_result_row["country_name"];
			$temp_row_array["country_two_lettered_code"] = $countries_list_get_select_query_result_row["country_two_lettered_code"];
			
		    $temp_row_array["is_active_status"] = $countries_list_get_select_query_result_row["is_active_status"];
			$constructed_array[] = $temp_row_array;
	    }

		return $constructed_array;
	}
	return $constructed_array;
}

function phonecode_list_info($phonecode_status_input) {

    global $dbcon;
	$constructed_array = array();
	
	   if(($phonecode_status_input == '1')|| ($phonecode_status_input == '0')){
		$phonecode_list_get_sql = "SELECT * FROM `country_phonecodes` cp JOIN `countries` c ON cp.country_id=c.country_id WHERE cp.is_active_status =:is_active_status ORDER BY `code` ASC";
		$phonecode_list_get_select_query = $dbcon->prepare($phonecode_list_get_sql);
		$phonecode_list_get_select_query->bindValue(":is_active_status",$phonecode_status_input);
		$phonecode_list_get_select_query->execute();
	   } else {
		$phonecode_list_get_sql = "SELECT * FROM `country_phonecodes`cp JOIN `countries` c ON cp.country_id=c.country_id ORDER BY `code` ASC ";
        $phonecode_list_get_select_query = $dbcon->prepare($phonecode_list_get_sql);	
		$phonecode_list_get_select_query->execute();  
	   }  

	if($phonecode_list_get_select_query->rowCount() > 0) {
	    $phonecode_list_get_select_query_result = $phonecode_list_get_select_query->fetchAll();
		foreach ($phonecode_list_get_select_query_result as $phonecode_list_get_select_query_result_row) {
			
			$temp_row_array = array();
		    $temp_row_array["code_id"] = $phonecode_list_get_select_query_result_row["code_id"];
			$temp_row_array["code"] = $phonecode_list_get_select_query_result_row["code"];
		    $temp_row_array["country_name"] = $phonecode_list_get_select_query_result_row["country_name"];
		    $temp_row_array["is_active_status"] = $phonecode_list_get_select_query_result_row["is_active_status"];
			$constructed_array[] = $temp_row_array;
	    }

		return $constructed_array;
	}
	return $constructed_array;
}

function job_experience_levels_list_info() {
    global $dbcon;
	$constructed_array = array();
	$is_active_status = '1';
		$job_experience_levels_list_get_sql = "SELECT * FROM `job_experience_levels` WHERE `is_active_status`= :is_active_status";
		$job_experience_levels_list_get_select_query = $dbcon->prepare($job_experience_levels_list_get_sql);
		$job_experience_levels_list_get_select_query->bindValue(":is_active_status",$is_active_status);
		$job_experience_levels_list_get_select_query->execute();

	if($job_experience_levels_list_get_select_query->rowCount() > 0) {
	    $job_experience_levels_list_get_select_query_result = $job_experience_levels_list_get_select_query->fetchAll();
		foreach ($job_experience_levels_list_get_select_query_result as $job_experience_levels_list_get_select_query_result_row) {
			$constructed_string = $job_experience_levels_list_get_select_query_result_row["job_experience_level_id"]. ":::::" .$job_experience_levels_list_get_select_query_result_row["job_experience_level_name"]. ":::::" .$job_experience_levels_list_get_select_query_result_row["job_experience_level_seo_name"];
			$temp_row_array = array();
		    $temp_row_array["job_experience_level_id"] =$job_experience_levels_list_get_select_query_result_row["job_experience_level_id"];
		    $temp_row_array["job_experience_level_name"] = $job_experience_levels_list_get_select_query_result_row["job_experience_level_name"];
			$temp_row_array["job_experience_level_seo_name"] = $job_experience_levels_list_get_select_query_result_row["job_experience_level_seo_name"];
		    $temp_row_array["is_active_status"] = $job_experience_levels_list_get_select_query_result_row["is_active_status"];
		    $temp_row_array["constructed_string"] = $constructed_string;
		    $constructed_array[] = $temp_row_array;
	    }

		return $constructed_array;
	}
	return $constructed_array;
}

function job_compensation_period_list_info() {
    global $dbcon;
	$constructed_array = array();
	$is_active_status = '1';
		$job_compensation_period_list_get_sql = "SELECT * FROM `job_compensation_period` WHERE `is_active_status`= :is_active_status";
		$job_compensation_period_list_get_select_query = $dbcon->prepare($job_compensation_period_list_get_sql);
		$job_compensation_period_list_get_select_query->bindValue(":is_active_status",$is_active_status);
		$job_compensation_period_list_get_select_query->execute();

	if($job_compensation_period_list_get_select_query->rowCount() > 0) {
	    $job_compensation_period_list_get_select_query_result = $job_compensation_period_list_get_select_query->fetchAll();
		foreach ($job_compensation_period_list_get_select_query_result as $job_compensation_period_list_get_select_query_result_row) {
			$constructed_string = $job_compensation_period_list_get_select_query_result_row["job_compensation_period_id"]. ":::::" . $job_compensation_period_list_get_select_query_result_row["job_compensation_period_name"]. ":::::" . $job_compensation_period_list_get_select_query_result_row["job_compensation_period_seo_name"];

			$temp_row_array = array();
		    $temp_row_array["job_compensation_period_id"] = $job_compensation_period_list_get_select_query_result_row["job_compensation_period_id"];
		    $temp_row_array["job_compensation_period_name"] = $job_compensation_period_list_get_select_query_result_row["job_compensation_period_name"];
			$temp_row_array["job_compensation_period_seo_name"] = $job_compensation_period_list_get_select_query_result_row["job_compensation_period_seo_name"];
		    $temp_row_array["is_active_status"] = $job_compensation_period_list_get_select_query_result_row["is_active_status"];
		    $temp_row_array["constructed_string"] = $constructed_string;
		    $constructed_array[] = $temp_row_array;
	    }

		return $constructed_array;
	}
	return $constructed_array;
}

function job_work_location_requirements_list_info() {
    global $dbcon;
	$constructed_array = array();
	$is_active_status = '1';
		$job_work_location_requirements_list_get_sql = "SELECT * FROM `job_work_location_requirements` WHERE `is_active_status`= :is_active_status";
		$job_work_location_requirements_list_get_select_query = $dbcon->prepare($job_work_location_requirements_list_get_sql);
		$job_work_location_requirements_list_get_select_query->bindValue(":is_active_status",$is_active_status);
		$job_work_location_requirements_list_get_select_query->execute();

	if($job_work_location_requirements_list_get_select_query->rowCount() > 0) {
	    $job_work_location_requirements_list_get_select_query_result = $job_work_location_requirements_list_get_select_query->fetchAll();
		foreach ($job_work_location_requirements_list_get_select_query_result as $job_work_location_requirements_list_get_select_query_result_row) {

			$constructed_string=$job_work_location_requirements_list_get_select_query_result_row["job_work_location_requirement_id"] . ":::::" . $job_work_location_requirements_list_get_select_query_result_row["job_work_location_requirement_name"] . ":::::" . $job_work_location_requirements_list_get_select_query_result_row["job_work_location_requirement_seo_name"];

			$temp_row_array = array();

		    $temp_row_array["job_work_location_requirement_id"] = $job_work_location_requirements_list_get_select_query_result_row["job_work_location_requirement_id"];
		    $temp_row_array["job_work_location_requirement_name"] = $job_work_location_requirements_list_get_select_query_result_row["job_work_location_requirement_name"];
			$temp_row_array["job_work_location_requirement_seo_name"] = $job_work_location_requirements_list_get_select_query_result_row["job_work_location_requirement_seo_name"];
		    $temp_row_array["is_active_status"] = $job_work_location_requirements_list_get_select_query_result_row["is_active_status"];
			$temp_row_array["constructed_string"] = $constructed_string;

		    $constructed_array[] = $temp_row_array;
	    }

		return $constructed_array;
	}
	return $constructed_array;
}

function job_industries_list_info() {
    global $dbcon;
	$constructed_array = array();
	$is_active_status = '1';
		$job_industries_list_get_sql = "SELECT * FROM `job_industries` WHERE `is_active_status`= :is_active_status";
		$job_industries_list_get_select_query = $dbcon->prepare($job_industries_list_get_sql);
		$job_industries_list_get_select_query->bindValue(":is_active_status",$is_active_status);
		$job_industries_list_get_select_query->execute();

	if($job_industries_list_get_select_query->rowCount() > 0) {
	    $job_industries_list_get_select_query_result = $job_industries_list_get_select_query->fetchAll();
		foreach ($job_industries_list_get_select_query_result as $job_industries_list_get_select_query_result_row) {

			$constructed_string = $job_industries_list_get_select_query_result_row["job_industry_id"] . ":::::" . $job_industries_list_get_select_query_result_row["job_industry_name"] . ":::::" . $job_industries_list_get_select_query_result_row["job_industry_seo_name"];

			$temp_row_array = array();
		    $temp_row_array["job_industry_id"] = $job_industries_list_get_select_query_result_row["job_industry_id"];
		    $temp_row_array["job_industry_name"] = $job_industries_list_get_select_query_result_row["job_industry_name"];
			$temp_row_array["job_industry_seo_name"] = $job_industries_list_get_select_query_result_row["job_industry_seo_name"];
		    $temp_row_array["is_active_status"] = $job_industries_list_get_select_query_result_row["is_active_status"];
			$temp_row_array["constructed_string"] = $constructed_string;

		    $constructed_array[] = $temp_row_array;
	    }

		return $constructed_array;
	}
	return $constructed_array;
}
function client_company_duplicate_check_based_on_company_id_and_client_company_name($client_company_name_input,$company_id_input) {

	global $dbcon;
	$constructed_array = array();
	$client_company_name = '%'.$client_company_name_input.'%';
	$client_company_details_duplicate_check_sql = "SELECT * FROM `company_clients` WHERE `company_id`=:company_id and `client_company_name` LIKE :client_company_name";
	$client_company_details_select_query = $dbcon->prepare($client_company_details_duplicate_check_sql);
	$client_company_details_select_query->bindValue(":client_company_name",$client_company_name_input);
	$client_company_details_select_query->bindValue(":company_id",$company_id_input);


	$client_company_details_select_query->execute();

	if($client_company_details_select_query->rowCount() > 0) {
		$client_company_details_select_query_result = $client_company_details_select_query->fetch();
	     return $client_company_details_select_query_result;

	}//close of if($client_company_details_select_query_result->rowCount() > 0) {
	return $constructed_array;

}
function client_company_basic_details_insert($company_id_input,$client_company_name_input, $client_company_seo_name_input, $client_company_brand_name_input, $client_company_registration_type_id_input,$client_company_registration_type_name_input,$client_company_registration_type_seo_name_input,$client_company_mobile_number_input,$client_company_mobile_number_code_input,$client_company_website_url_input, $client_company_email_input, $client_company_support_email_input,$event_datetime,$current_epoch) {
	global $dbcon;
	$is_active_status = '1';
	$client_company_details_insert_sql = "INSERT INTO `company_clients` (company_id,client_company_name,client_company_seo_name,client_company_brand_name,client_company_registration_type_id,client_company_registration_type_name,client_company_registration_type_seo_name,client_company_mobile_number,client_mobile_country_code,client_company_website_url,client_company_email,client_company_support_email,added_datetime,added_datetime_epoch,is_active_status) VALUES(:company_id, :client_company_name, :client_company_seo_name,:client_company_brand_name,:client_company_registration_type_id,:client_company_registration_type_name,:client_company_registration_type_seo_name, :client_company_mobile_number,:client_mobile_country_code, :client_company_website_url, :client_company_email, :client_company_support_email, :added_datetime, :added_datetime_epoch, :is_active_status)";
	$client_company_details_insert_query = $dbcon->prepare($client_company_details_insert_sql);
	$client_company_details_insert_query->bindValue(":company_id",$company_id_input);
	$client_company_details_insert_query->bindValue(":client_company_name",$client_company_name_input);
	$client_company_details_insert_query->bindValue(":client_company_seo_name",$client_company_seo_name_input);
	$client_company_details_insert_query->bindValue(":client_company_brand_name",$client_company_brand_name_input);
	$client_company_details_insert_query->bindValue(":client_company_registration_type_id",$client_company_registration_type_id_input);
	$client_company_details_insert_query->bindValue(":client_company_registration_type_name",$client_company_registration_type_name_input);
	$client_company_details_insert_query->bindValue(":client_company_registration_type_seo_name",$client_company_registration_type_seo_name_input);
	$client_company_details_insert_query->bindValue(":client_company_mobile_number",$client_company_mobile_number_input);
	$client_company_details_insert_query->bindValue(":client_mobile_country_code",$client_company_mobile_number_code_input);
	$client_company_details_insert_query->bindValue(":client_company_website_url",$client_company_website_url_input);
	$client_company_details_insert_query->bindValue(":client_company_email",$client_company_email_input);
	$client_company_details_insert_query->bindValue(":client_company_support_email",$client_company_support_email_input);
	$client_company_details_insert_query->bindValue(":added_datetime",$event_datetime);
	$client_company_details_insert_query->bindValue(":added_datetime_epoch",$current_epoch);
    $client_company_details_insert_query->bindValue(":is_active_status",$is_active_status);

		if ($client_company_details_insert_query->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");

			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
		}
}
 function company_client_office_address_duplicate_check_based_on_company_id($company_id_input,$company_client_id_input,$address_line_1_input,$pincode_input) {
	global $dbcon;
    $address_line_1 = '%'.$address_line_1_input.'%';
	$constructed_array = array();
	$company_client_office_address_duplicate_check_sql = "SELECT * FROM `company_client_office_addresses` WHERE `company_id` = :company_id AND `company_client_id`
	= :company_client_id AND `address_line_1` LIKE :address_line_1 AND `pincode` = :pincode";
	$company_client_office_address_select_query = $dbcon->prepare($company_client_office_address_duplicate_check_sql);
	$company_client_office_address_select_query->bindValue(":company_id",$company_id_input);
	$company_client_office_address_select_query->bindValue(":company_client_id",$company_client_id_input);
    $company_client_office_address_select_query->bindValue(":address_line_1",$address_line_1_input);
    $company_client_office_address_select_query->bindValue(":pincode",$pincode_input);
	$company_client_office_address_select_query->execute();

	if($company_client_office_address_select_query->rowCount() > 0) {
		$company_client_office_address_select_query_result = $company_client_office_address_select_query->fetch();
	     return $company_client_office_address_select_query_result;

	}//close of if($company_client_office_address_select_query->rowCount() > 0) {
	return $constructed_array;

}
/*
function company_client_office_address_details_insert($company_id_input,$company_client_id_input, $premise_name_input, $address_line_1_input, $address_line_2_input, $city_input,$state_input,$country_input,$pincode_input,$event_datetime,$current_epoch) {
	
	 
	$is_active_status = '1';
	$company_client_office_address_details_insert_sql = "INSERT INTO `company_client_office_addresses` (company_id,company_client_id,premise_name,address_line_1,address_line_2,city,state,country,pincode,added_datetime,added_datetime_epoch,is_active_status) VALUES(:company_id, :company_client_id, :premise_name, :address_line_1, :address_line_2, :city, :state, :country, :pincode, :added_datetime, :added_datetime_epoch, :is_active_status)";
	$company_client_office_address_details_insert_query = $dbcon->prepare($company_client_office_address_details_insert_sql);
	$company_client_office_address_details_insert_query->bindValue(":company_id",$company_id_input);
	$company_client_office_address_details_insert_query->bindValue(":company_client_id",$company_client_id_input);
	$company_client_office_address_details_insert_query->bindValue(":premise_name",$premise_name_input);
	$company_client_office_address_details_insert_query->bindValue(":address_line_1",$address_line_1_input);
	$company_client_office_address_details_insert_query->bindValue(":address_line_2",$address_line_2_input);
	$company_client_office_address_details_insert_query->bindValue(":city",$city_input);
	$company_client_office_address_details_insert_query->bindValue(":state",$state_input);
	$company_client_office_address_details_insert_query->bindValue(":country",$country_input);
	$company_client_office_address_details_insert_query->bindValue(":pincode",$pincode_input);
	$company_client_office_address_details_insert_query->bindValue(":added_datetime",$event_datetime);
	$company_client_office_address_details_insert_query->bindValue(":added_datetime_epoch",$current_epoch);
    $company_client_office_address_details_insert_query->bindValue(":is_active_status",$is_active_status);

		if ($company_client_office_address_details_insert_query->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");

			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }
}*/

//function jobs_list_info($company_id_input) {
function jobs_list_info($company_id_input, $company_client_id_input) {

    global $dbcon;
	$constructed_array = array();
	$is_active_status = '1';

	if (is_null($company_client_id_input)) {
		//All Jobs, those that are posted w.r.t. the Company
		$jobs_list_get_sql = "SELECT * FROM `jobs` WHERE `company_id`= :company_id";
		$jobs_list_get_select_query = $dbcon->prepare($jobs_list_get_sql);
		$jobs_list_get_select_query->bindValue(":company_id",$company_id_input);
		$jobs_list_get_select_query->execute();
	} else {
		//Jobs, those that are posted w.r.t. the Company and particular Company Client
		$jobs_list_get_sql = "SELECT * FROM `jobs` WHERE `company_id`= :company_id AND `company_client_id`= :company_client_id";
		$jobs_list_get_select_query = $dbcon->prepare($jobs_list_get_sql);
		$jobs_list_get_select_query->bindValue(":company_id",$company_id_input);
		$jobs_list_get_select_query->bindValue(":company_client_id",$company_client_id_input);
		$jobs_list_get_select_query->execute();
	}
		/*$jobs_list_get_sql = "SELECT * FROM `jobs` WHERE `job_posting_user_id`= :job_posting_user_id AND `is_active_status`= :is_active_status";
		$jobs_list_get_select_query = $dbcon->prepare($jobs_list_get_sql);
		$jobs_list_get_select_query->bindValue(":job_posting_user_id",$company_id_input);
		$jobs_list_get_select_query->bindValue(":is_active_status",$is_active_status);
		$jobs_list_get_select_query->execute();*/

	if($jobs_list_get_select_query->rowCount() > 0) {
	    $jobs_list_get_select_query_result = $jobs_list_get_select_query->fetchAll();
		foreach ($jobs_list_get_select_query_result as $jobs_list_get_select_query_result_row) {

			$temp_row_array = array();
			$temp_row_array["job_id"] = $jobs_list_get_select_query_result_row["job_id"];
		    $temp_row_array["job_title"] = $jobs_list_get_select_query_result_row["job_title"];
		    $temp_row_array["job_seo_title"] = $jobs_list_get_select_query_result_row["job_seo_title"];
			$temp_row_array["job_summary"] = $jobs_list_get_select_query_result_row["job_summary"];
		    $temp_row_array["job_full_description"] = $jobs_list_get_select_query_result_row["job_full_description"];
			$temp_row_array["job_type_id"] = $jobs_list_get_select_query_result_row["job_type_id"];
			$temp_row_array["job_type_name"] = $jobs_list_get_select_query_result_row["job_type_name"];
			$temp_row_array["job_type_seo_name"] = $jobs_list_get_select_query_result_row["job_type_seo_name"];
			$temp_row_array["job_experience_level_id"] = $jobs_list_get_select_query_result_row["job_experience_level_id"];
			$temp_row_array["job_experience_level_name"] = $jobs_list_get_select_query_result_row["job_experience_level_name"];
			$temp_row_array["job_experience_level_seo_name"] = $jobs_list_get_select_query_result_row["job_experience_level_seo_name"];
			$temp_row_array["currency_id"] = $jobs_list_get_select_query_result_row["currency_id"];
			$temp_row_array["currency_name"] = $jobs_list_get_select_query_result_row["currency_name"];
			$temp_row_array["currency_seo_name"] = $jobs_list_get_select_query_result_row["currency_seo_name"];
			$temp_row_array["currency_three_lettered_code"] = $jobs_list_get_select_query_result_row["currency_three_lettered_code"];
			$temp_row_array["min_compensation_value"] = $jobs_list_get_select_query_result_row["min_compensation_value"];
			$temp_row_array["max_compensation_value"] = $jobs_list_get_select_query_result_row["max_compensation_value"];
			$temp_row_array["job_compensation_period_id"] = $jobs_list_get_select_query_result_row["job_compensation_period_id"];
			$temp_row_array["job_compensation_period_name"] = $jobs_list_get_select_query_result_row["job_compensation_period_name"];
			$temp_row_array["job_compensation_period_seo_name"] = $jobs_list_get_select_query_result_row["job_compensation_period_seo_name"];
			$temp_row_array["job_industry_id"] = $jobs_list_get_select_query_result_row["job_industry_id"];
			$temp_row_array["job_industry_name"] = $jobs_list_get_select_query_result_row["job_industry_name"];
			$temp_row_array["job_industry_seo_name"] = $jobs_list_get_select_query_result_row["job_industry_seo_name"];
			$temp_row_array["job_work_location_requirement_id"] = $jobs_list_get_select_query_result_row["job_work_location_requirement_id"];
			$temp_row_array["job_work_location_requirement_name"] = $jobs_list_get_select_query_result_row["job_work_location_requirement_name"];
			$temp_row_array["job_work_location_requirement_seo_name"] = $jobs_list_get_select_query_result_row["job_work_location_requirement_seo_name"];
			$temp_row_array["city"] = $jobs_list_get_select_query_result_row["city"];
			$temp_row_array["state"] = $jobs_list_get_select_query_result_row["state"];
			$temp_row_array["country"] = $jobs_list_get_select_query_result_row["country"];
			$temp_row_array["available_positions"] = $jobs_list_get_select_query_result_row["available_positions"];
			$temp_row_array["resume_submission_end_date"] = $jobs_list_get_select_query_result_row["resume_submission_end_date"];

		     $constructed_array[] = $temp_row_array;
	    }

		return $constructed_array;
	}
	return $constructed_array;
}

function update_company_rel_is_active_status_based_on_company_id($company_id_input, $new_status_input, $admin_user_id_input, $event_datetime_input, $event_datetime_epoch_input) {
	global $dbcon;

	$user_rel_sm_user_status_update_sql = "UPDATE `companies` SET `last_updated_datetime`=:last_updated_datetime,`last_updated_datetime_epoch`=:last_updated_datetime_epoch,`last_updated_by_admin_user_id`=:last_updated_by_admin_user_id,`is_active_status`=:is_active_status WHERE `company_id`=:company_id";
	$user_rel_sm_user_status_update_query = $dbcon->prepare($user_rel_sm_user_status_update_sql);
	$user_rel_sm_user_status_update_query->bindValue(":last_updated_datetime",$event_datetime_input);
	$user_rel_sm_user_status_update_query->bindValue(":last_updated_datetime_epoch",$event_datetime_epoch_input);
	$user_rel_sm_user_status_update_query->bindValue(":last_updated_by_admin_user_id",$admin_user_id_input);
	$user_rel_sm_user_status_update_query->bindValue(":is_active_status",$new_status_input);
	$user_rel_sm_user_status_update_query->bindValue(":company_id",$company_id_input);

	if ($user_rel_sm_user_status_update_query->execute()) {

		return true;

	}
	return false;
}

/*function company_basic_details_result_update_based_on_company_id($company_id_input,$company_name_input,$company_brand_name_input,$company_registration_type_id_input,$company_registration_type_name_input,$company_registration_type_seo_name_input,$company_email_input,$company_support_email_input,$company_mobile_number_input,$company_website_url_input,$company_recruiter_purposed_email_input,$event_datetime,$current_epoch) {
	global $dbcon, $eventLog;
	
	$eventLog->log("in function before query");
    
	$company_details_update_sql = "UPDATE `companies` SET `company_id`=:company_id,`company_name`=:company_name,`company_brand_name`=:company_brand_name,`company_registration_type_id`=:company_registration_type_id,`company_registration_type_name`=:company_registration_type_name,`company_registration_type_seo_name`=:company_registration_type_seo_name,`company_email`=:company_email,`company_support_email`=:company_support_email,`company_mobile_number`=:company_mobile_number,`company_website_url`=:company_website_url,`company_recruiter_purposed_email`=:company_recruiter_purposed_email,`last_updated_datetime`=:last_updated_datetime,`last_updated_datetime_epoch`=:last_updated_datetime_epoch WHERE `company_id`=:company_id";
	$company_details_update_query = $dbcon->prepare($company_details_update_sql);
	$eventLog->log("in function before bind");
	
	$company_details_update_query->bindValue(":company_id",$company_id_input);
	$company_details_update_query->bindValue(":company_name",$company_name_input);
	$company_details_update_query->bindValue(":company_brand_name",$company_brand_name_input);
	$company_details_update_query->bindValue(":company_registration_type_id",$company_registration_type_id_input);
	$company_details_update_query->bindValue(":company_registration_type_name",$company_registration_type_name_input);
	$company_details_update_query->bindValue(":company_registration_type_seo_name",$company_registration_type_seo_name_input);
	$company_details_update_query->bindValue(":company_email",$company_email_input);
	$company_details_update_query->bindValue(":company_support_email",$company_support_email_input);
	$company_details_update_query->bindValue(":company_mobile_number",$company_mobile_number_input);
	$company_details_update_query->bindValue(":company_website_url",$company_website_url_input);
	$company_details_update_query->bindValue(":company_recruiter_purposed_email",$company_recruiter_purposed_email_input);
	$company_details_update_query->bindValue(":last_updated_datetime",$event_datetime);
	$company_details_update_query->bindValue(":last_updated_datetime_epoch",$current_epoch);
	
	$eventLog->log("in function after bind");

	if ($company_details_update_query->execute()) {
		
		$eventLog->log("in function enter into if");

		return true;

	}
	return false;
}*/
function user_account_duplicate_check_based_on_email_id_input($email_id_input,$company_id_input) {
	global $dbcon;
	$constructed_array = array();

	$user_account_duplicate_check_sql = "SELECT `sm_memb_id`,`sm_email`,`company_id` FROM `site_members` WHERE `sm_email` LIKE :sm_email AND `company_id`=:company_id AND `sm_user_status` != :sm_user_status";
	$user_account_duplicate_check_q = $dbcon->prepare($user_account_duplicate_check_sql);
	$user_account_duplicate_check_q->bindValue(":sm_email",$email_id_input);
	$user_account_duplicate_check_q->bindValue(":company_id",$company_id_input);
    $user_account_duplicate_check_q->bindValue(":sm_user_status","4");
	$user_account_duplicate_check_q->execute();

	if($user_account_duplicate_check_q->rowCount() > 0) {
		$user_account_duplicate_check_q_result = $user_account_duplicate_check_q->fetch();
	     return $user_account_duplicate_check_q_result;

	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;

}

function candidate_rel_internal_notes_insert($company_id_input,$candidate_id,$notes,$reviewer_sm_memb_id_input,$reviewer_sm_firstname_input,$reviewer_sm_lastname_input,$event_date_time_input,$event_date_time_epoch_input) {
	global $dbcon;
	$is_active_status = '1';
	$candidate_rel_internal_notes_insert_sql = "INSERT INTO `candidate_rel_internal_notes` (company_id,candidate_id,notes,sm_memb_id,sm_firstname,sm_lastname,event_date_time,event_date_time_epoch,is_active_status) VALUES(:company_id, :candidate_id, :notes, :sm_memb_id,:sm_firstname,:sm_lastname,:event_date_time,:event_date_time_epoch, :is_active_status)";
	$candidate_rel_internal_notes_insert_query = $dbcon->prepare($candidate_rel_internal_notes_insert_sql);
	$candidate_rel_internal_notes_insert_query->bindValue(":company_id",$company_id_input);
	$candidate_rel_internal_notes_insert_query->bindValue(":candidate_id",$candidate_id);
	
	$candidate_rel_internal_notes_insert_query->bindValue(":notes",$notes);
	$candidate_rel_internal_notes_insert_query->bindValue(":sm_memb_id",$reviewer_sm_memb_id_input);
	$candidate_rel_internal_notes_insert_query->bindValue(":sm_firstname",$reviewer_sm_firstname_input);
	$candidate_rel_internal_notes_insert_query->bindValue(":sm_lastname",$reviewer_sm_lastname_input);
	$candidate_rel_internal_notes_insert_query->bindValue(":event_date_time",$event_date_time_input);
	$candidate_rel_internal_notes_insert_query->bindValue(":event_date_time_epoch",$event_date_time_epoch_input);
    $candidate_rel_internal_notes_insert_query->bindValue(":is_active_status",$is_active_status);

		if ($candidate_rel_internal_notes_insert_query->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully.");

			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
		}
}
function user_account_basic_details_insert($email_id_input,$mobile_phone_country_code_input,$mobile_number_input,$alternate_email_id_input,$salutation_input,$firstname_input,$middlename_input,$lastname_input,$work_phone_country_code_input,$work_phone_number_input,$home_phone_country_code_input,$home_phone_number_input,$fax_number_country_code_input,$fax_number_input,$address_input,$city_input,$state_input,$country_input,$zipcode_input,$company_id_input,$user_type_input,$admin_level_input, $user_role_input, $added_by_admin_user_id_input, $added_by_admin_user_firstname_input, $added_by_admin_user_middlename_input, $added_by_admin_user_lastname_input, $added_date_time_input, $added_date_time_epoch_input, $sm_user_email_act_code_input, $sm_user_status) {
	global $dbcon;$eventLog;
	
	$user_account_basic_details_sql = "INSERT INTO `site_members`(`sm_email`,`sm_mobile_phone_country_code`, `sm_mobile`, `sm_alternate_email`, `sm_salutation`, `sm_firstname`, `sm_middlename`, `sm_lastname`, `sm_work_phone_country_code`, `sm_work_phone`, `sm_home_phone_country_code`, `sm_home_phone`, `sm_fax_number_country_code`, `sm_fax_number`, `sm_address`, `sm_city`, `sm_state`, `sm_country`, `sm_zipcode`, `company_id`, `sm_user_type`, `sm_admin_level`, `sm_user_role`, `added_by_admin_user_id`, `added_by_admin_user_firstname`, `added_by_admin_user_middlename`, `added_by_admin_user_lastname`, `added_date_time`, `added_date_time_epoch`,`sm_user_email_act_code`, `sm_user_status`)VALUES(:sm_email,:sm_mobile_phone_country_code,:sm_mobile,:sm_alternate_email,:sm_salutation,:sm_firstname,:sm_middlename,:sm_lastname,:sm_work_phone_country_code,:sm_work_phone,:sm_home_phone_country_code,:sm_home_phone, :sm_fax_number_country_code,:sm_fax_number,:sm_address,:sm_city,:sm_state,:sm_country,:sm_zipcode,:company_id, :sm_user_type, :sm_admin_level, :sm_user_role, :added_by_admin_user_id, :added_by_admin_user_firstname, :added_by_admin_user_middlename, :added_by_admin_user_lastname, :added_date_time, :added_date_time_epoch, :sm_user_email_act_code, :sm_user_status)";
	
	$user_account_basic_details_q = $dbcon->prepare($user_account_basic_details_sql);
	$user_account_basic_details_q->bindValue(":sm_email",$email_id_input);
	$user_account_basic_details_q->bindValue(":sm_mobile_phone_country_code",$mobile_phone_country_code_input);
	$user_account_basic_details_q->bindValue(":sm_mobile",$mobile_number_input);
	$user_account_basic_details_q->bindValue(":sm_alternate_email",$alternate_email_id_input);
	$user_account_basic_details_q->bindValue(":sm_salutation",$salutation_input);
	$user_account_basic_details_q->bindValue(":sm_firstname",$firstname_input);
	$user_account_basic_details_q->bindValue(":sm_middlename",$middlename_input);
	$user_account_basic_details_q->bindValue(":sm_lastname",$lastname_input);
	$user_account_basic_details_q->bindValue(":sm_work_phone_country_code",$work_phone_country_code_input);
	$user_account_basic_details_q->bindValue(":sm_work_phone",$work_phone_number_input);
	$user_account_basic_details_q->bindValue(":sm_home_phone_country_code",$home_phone_country_code_input);
	$user_account_basic_details_q->bindValue(":sm_home_phone",$home_phone_number_input);
	$user_account_basic_details_q->bindValue(":sm_fax_number_country_code",$fax_number_country_code_input);
	$user_account_basic_details_q->bindValue(":sm_fax_number",$fax_number_input);
	$user_account_basic_details_q->bindValue(":sm_address",$address_input);
	$user_account_basic_details_q->bindValue(":sm_city",$city_input);
	$user_account_basic_details_q->bindValue(":sm_state",$state_input);
	$user_account_basic_details_q->bindValue(":sm_country",$country_input);
	$user_account_basic_details_q->bindValue(":sm_zipcode",$zipcode_input);
    $user_account_basic_details_q->bindValue(":company_id",$company_id_input);
	$user_account_basic_details_q->bindValue(":sm_user_type",$user_type_input);
	$user_account_basic_details_q->bindValue(":sm_admin_level",$admin_level_input);
	$user_account_basic_details_q->bindValue(":sm_user_role",$user_role_input);
	$user_account_basic_details_q->bindValue(":added_by_admin_user_id",$added_by_admin_user_id_input);
	$user_account_basic_details_q->bindValue(":added_by_admin_user_firstname",$added_by_admin_user_firstname_input);
	$user_account_basic_details_q->bindValue(":added_by_admin_user_middlename",$added_by_admin_user_middlename_input);
	$user_account_basic_details_q->bindValue(":added_by_admin_user_lastname",$added_by_admin_user_lastname_input);
	$user_account_basic_details_q->bindValue(":added_date_time",$added_date_time_input);
	$user_account_basic_details_q->bindValue(":added_date_time_epoch",$added_date_time_epoch_input);
	$user_account_basic_details_q->bindValue(":sm_user_email_act_code",$sm_user_email_act_code_input);
	$user_account_basic_details_q->bindValue(":sm_user_status",$sm_user_status);
	;
	
       
		if ($user_account_basic_details_q->execute()) {

            $last_inserted_id = $dbcon->lastInsertId();
			//$eventLog->log("record inserted successfully");

			return $last_inserted_id;

		} else {
		    //$eventLog->log("Error occurred during process. Please try again");
				return "";
	    }
}

function duplicate_check_collected_email($final_email_id,$company_id){
	global $dbcon;
	$constructed_array = array();

	$user_account_duplicate_check_sql = "SELECT `sm_email`,`company_id` FROM `site_members` WHERE `sm_email` LIKE :sm_email AND `company_id`=:company_id AND `sm_user_status` = :sm_user_status";
	$user_account_duplicate_check_q = $dbcon->prepare($user_account_duplicate_check_sql);
	$user_account_duplicate_check_q->bindValue(":sm_email",$final_email_id);
	$user_account_duplicate_check_q->bindValue(":company_id",$company_id);
    $user_account_duplicate_check_q->bindValue(":sm_user_status","1");
	$user_account_duplicate_check_q->execute();

	if($user_account_duplicate_check_q->rowCount() > 0) {
		$user_account_duplicate_check_q_result = $user_account_duplicate_check_q->fetch();
	     return $user_account_duplicate_check_q_result;

	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
}


function extract_file_extension_based_on_uploaded_file_mime_type_input($mime_types_input, $uploaded_file_mime_type_input) {
											
	$uploaded_file_rel_extension = "";
	
	foreach ($mime_types_input as $mime_types_input_key => $mime_types_input_value) {
	
		if (trim($uploaded_file_mime_type_input) == $mime_types_input_value) {
			
			$uploaded_file_rel_extension = $mime_types_input_key;
			
			return $uploaded_file_rel_extension;
			
		}//close of if (in_array(trim($uploaded_file_mime_type_input), $mime_types_input_value, true)) {
			
	}//close of foreach ($mime_types_input as $mime_types_input_key => $mime_types_input_value) {
	return $uploaded_file_rel_extension;
	
}


function candidate_rel_document_file_info_get($crauvd_id_input) {
    global $dbcon;
	$constructed_array = array();
	$candidate_rel_document_file_info_get_sql = "SELECT * FROM `candidate_rel_all_uploaded_visa_documents` WHERE `crauvd_id`=:crauvd_id";
	$candidate_rel_document_file_info_get_select_query = $dbcon->prepare($candidate_rel_document_file_info_get_sql);
	$candidate_rel_document_file_info_get_select_query->bindValue(":crauvd_id",$crauvd_id_input);	
	$candidate_rel_document_file_info_get_select_query->execute(); 
	
	if($candidate_rel_document_file_info_get_select_query->rowCount() > 0) {
		$candidate_rel_document_file_info_get_select_query_result = $candidate_rel_document_file_info_get_select_query->fetch();
	     return $candidate_rel_document_file_info_get_select_query_result;
	
	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
}

function candidate_rel_document_file_info_update($user_id_input, $file_size_bytes_input, $file_size_kilo_bytes_input, $file_size_mega_bytes_input, $original_filename_input, $generated_filename_input, $last_updated_by_user_type_input, $last_updated_by_user_id_input, $last_updated_date_time_input, $last_updated_date_time_epoch_input, $crauvd_id_input) {	
    global $dbcon;
	
	
	
	$candidate_rel_document_file_info_update_sql = "UPDATE `candidate_rel_all_uploaded_visa_documents` SET `original_filename`=:original_filename,`generated_filename`=:generated_filename,`file_size_bytes`=:file_size_bytes,`file_size_kilo_bytes`=:file_size_kilo_bytes,`file_size_mega_bytes`=:file_size_mega_bytes,`last_updated_by_user_type`=:last_updated_by_user_type,`last_updated_by_user_id`=:last_updated_by_user_id,`last_updated_date_time`=:last_updated_date_time,`last_updated_date_time_epoch`=:last_updated_date_time_epoch WHERE `crauvd_id`=:crauvd_id";
	$candidate_rel_document_file_info_update_update_query = $dbcon->prepare($candidate_rel_document_file_info_update_sql);
	$candidate_rel_document_file_info_update_update_query->bindValue(":original_filename",$original_filename_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":generated_filename",$generated_filename_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":file_size_bytes",$file_size_bytes_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":file_size_kilo_bytes",$file_size_kilo_bytes_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":file_size_mega_bytes",$file_size_mega_bytes_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":last_updated_by_user_type",$last_updated_by_user_type_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":last_updated_by_user_id",$last_updated_by_user_id_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":last_updated_date_time",$last_updated_date_time_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":last_updated_date_time_epoch",$last_updated_date_time_epoch_input);
	$candidate_rel_document_file_info_update_update_query->bindValue(":crauvd_id",$crauvd_id_input);
	
	if ($candidate_rel_document_file_info_update_update_query->execute()) {
		return true;
	}
	return false;
}

function candidate_rel_document_file_info_insert($company_id_input, $resume_upload_mode_input, $resume_source_input, $job_id_input, $file_size_bytes_input, $file_size_kilo_bytes_input, $file_size_mega_bytes_input, $original_filename_input, $generated_filename_input, $added_by_user_id_input, $added_date_time_input, $added_date_time_epoch_input) {	
    global $dbcon;
	
	$candidate_rel_document_file_info_insert_sql = "INSERT INTO `candidate_rel_uploaded_resumes`(`company_id`,`resume_upload_mode`, `resume_source`, `job_id`, `original_filename`, `generated_filename`, `file_size_bytes`, `file_size_kilo_bytes`, `file_size_mega_bytes`, `resume_processing_status`, `added_by_user_id`, `added_date_time`, `added_date_time_epoch`, `is_active_status`) VALUES (:company_id,:resume_upload_mode,:resume_source,:job_id,:original_filename,:generated_filename,:file_size_bytes,:file_size_kilo_bytes,:file_size_mega_bytes,:resume_processing_status,:added_by_user_id,:added_date_time,:added_date_time_epoch,:is_active_status)";
	
	$candidate_rel_document_file_info_insert_query = $dbcon->prepare($candidate_rel_document_file_info_insert_sql);
	$candidate_rel_document_file_info_insert_query->bindValue(":company_id",$company_id_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":resume_upload_mode",$resume_upload_mode_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":resume_source",$resume_source_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":job_id",$job_id_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":original_filename",$original_filename_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":generated_filename",$generated_filename_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":file_size_bytes",$file_size_bytes_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":file_size_kilo_bytes",$file_size_kilo_bytes_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":file_size_mega_bytes",$file_size_mega_bytes_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":resume_processing_status",0);
	$candidate_rel_document_file_info_insert_query->bindValue(":added_by_user_id",$added_by_user_id_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":added_date_time",$added_date_time_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":added_date_time_epoch",$added_date_time_epoch_input);
	$candidate_rel_document_file_info_insert_query->bindValue(":is_active_status",1);
	if ($candidate_rel_document_file_info_insert_query->execute()) {
			
        $last_inserted_id = $dbcon->lastInsertId();			
			return $last_inserted_id;
						
	} else {						
			return "";						
	}
	
}

function get_company_details_based_on_company_recruiter_purposed_email($company_recruiter_purposed_email_input) {

    global $dbcon;
	$constructed_array = array();
		$companies_details_get_sql = "SELECT * FROM `companies` WHERE `company_recruiter_purposed_email`=:company_recruiter_purposed_email";

		$companies_details_get_select_query = $dbcon->prepare($companies_details_get_sql);
		$companies_details_get_select_query->bindValue(":company_recruiter_purposed_email",$company_recruiter_purposed_email_input);
		$companies_details_get_select_query->execute();

	if($companies_details_get_select_query->rowCount() > 0) {
	    $companies_details_get_select_query_result = $companies_details_get_select_query->fetch();
	    //print_r($companies_details_get_select_query_result);

		return $companies_details_get_select_query_result;
	}
	return $constructed_array;
}
function user_upload_resume_details_based_on_user_id($user_id_input) {

	global $dbcon, $eventLog;
	$constructed_array = array();
    
	$user_upload_resume_details_get_sql = "SELECT * FROM `candidate_rel_uploaded_resumes` WHERE `sm_memb_id` =:sm_memb_id";
	$user_upload_resume_details_select_query = $dbcon->prepare($user_upload_resume_details_get_sql);
    $user_upload_resume_details_select_query->bindValue(":sm_memb_id",$user_id_input);
	
      $user_upload_resume_details_select_query->execute();
	  
	if($user_upload_resume_details_select_query->rowCount() > 0) {
		$user_upload_resume_details_select_query_result = $user_upload_resume_details_select_query->fetch();
		return $user_upload_resume_details_select_query_result;
	
	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
	
}
function user_upload_resume_details_information_based_on_user_id_and_company_id($user_id_input,$company_id_input) {

    global $dbcon,$ea_extracted_jwt_token_sub,$current_epoch,$expiring_link_lifetime, $expiring_link_secret_key, $expiring_link_hash_algorithm, $site_hostname_value,$eventLog;
	$site_hostname_value = "uploaded-files-dev.resumecrab.com";
	$constructed_array = array();
		$user_upload_resume_details_information_get_sql = "SELECT * FROM `resume_extracted_info` WHERE `sm_memb_id`=:sm_memb_id AND `company_id`=:company_id LIMIT 1 ";
		$user_upload_resume_details_information_select_query = $dbcon->prepare($user_upload_resume_details_information_get_sql);
		$user_upload_resume_details_information_select_query->bindValue(":sm_memb_id",$user_id_input);
		$user_upload_resume_details_information_select_query->bindValue(":company_id",$company_id_input);
		$user_upload_resume_details_information_select_query->execute();

	if($user_upload_resume_details_information_select_query->rowCount() > 0) {
	    $user_upload_resume_details_information_select_query_result = $user_upload_resume_details_information_select_query->fetchAll();
	    //print_r($user_classification_details_get_select_query_result);
	    foreach ($user_upload_resume_details_information_select_query_result as $user_upload_resume_details_information_select_query_result_row) {
			//$temp_row_array = array();
			$constructed_array["sm_memb_id"] = $user_upload_resume_details_information_select_query_result_row["sm_memb_id"];
			$constructed_array["resume_text"] = $user_upload_resume_details_information_select_query_result_row["resume_text"];
            $constructed_array["crur_id"] = $user_upload_resume_details_information_select_query_result_row["crur_id"]; 			
			$user_upload_resume_details_result = user_upload_resume_details_based_on_user_id($user_id_input);
			if(count($user_upload_resume_details_result) > 0) {
				$eventLog->log("enter into count condition");
				$constructed_array["added_by_user_id"] = $user_upload_resume_details_result["added_by_user_id"];
				$user_details_result = user_basic_details_check_based_on_user_id($constructed_array["added_by_user_id"]);
				$user_firstname = $user_details_result["sm_firstname"];
				$user_middlename = $user_details_result["sm_middlename"];
				$user_lastname = $user_details_result["sm_lastname"];
				$constructed_array["added_by_user_name"] = $user_firstname . " " . $user_middlename ." ". $user_lastname;
				$constructed_array["added_date_time"] = $user_upload_resume_details_result["added_date_time"];
				$constructed_array["added_date_time_epoch"] = $user_upload_resume_details_result["added_date_time_epoch"]; 
			}
			$link_expiry_time = $current_epoch+$expiring_link_lifetime;
			$uploaded_file_link_with_signature_result = create_uploaded_file_link_with_signature($ea_extracted_jwt_token_sub, $constructed_array["sm_memb_id"], $constructed_array["crur_id"], "https", $site_hostname_value, $link_expiry_time, $expiring_link_hash_algorithm, $expiring_link_secret_key);
									if (isset($uploaded_file_link_with_signature_result["uploaded_file_url"])) {
									$constructed_array["uploaded_file_url"] = $uploaded_file_link_with_signature_result["uploaded_file_url"];
										
									} else {
										$constructed_array["uploaded_file_url"] = null;
									}
			
			
			
			//$constructed_array[] = $temp_row_array;
	    }
		return $constructed_array;
		//return $company_office_addresses_list_get_select_query_result;
	}
	return $constructed_array;
}

function profile_source_details_based_on_profile_source_id($profile_source_id_input) {

	global $dbcon, $eventLog;
	$constructed_array = array();
    
	$profile_source_details_get_sql = "SELECT * FROM `profile_sources` WHERE `profile_source_id` =:profile_source_id";
	$profile_source_details_select_query = $dbcon->prepare($profile_source_details_get_sql);
    $profile_source_details_select_query->bindValue(":profile_source_id",$profile_source_id_input);
	
      $profile_source_details_select_query->execute();
	  
	if($profile_source_details_select_query->rowCount() > 0) {
		$profile_source_details_select_query_result = $profile_source_details_select_query->fetch();
		return $profile_source_details_select_query_result;
	
	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
	
}
function jsl_classification_details_based_on_jsl_classification_detail_id($jsl_classification_detail_id) {

	global $dbcon, $eventLog;
	$constructed_array = array();
    
	$jsl_classification_details_get_sql = "SELECT * FROM `jsl_classification_details` WHERE `jsl_classification_detail_id` =:jsl_classification_detail_id";
	$jsl_classification_details_select_query = $dbcon->prepare($jsl_classification_details_get_sql);
    $jsl_classification_details_select_query->bindValue(":jsl_classification_detail_id",$jsl_classification_detail_id);
	
      $jsl_classification_details_select_query->execute();
	  
	if($jsl_classification_details_select_query->rowCount() > 0) {
		$jsl_classification_details_select_query_result = $jsl_classification_details_select_query->fetch();
		return $jsl_classification_details_select_query_result;
	
	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
	
}

function jsl_sub_classification_details_based_on_jsl_sub_classification_detail_id($jsl_sub_classification_detail_id_input) {

	global $dbcon, $eventLog;
	$constructed_array = array();
    
	$jsl_sub_classification_details_get_sql = "SELECT * FROM `jsl_sub_classification_details` WHERE `jsl_sub_classification_detail_id` =:jsl_sub_classification_detail_id";
	$jsl_sub_classification_details_select_query = $dbcon->prepare($jsl_sub_classification_details_get_sql);
    $jsl_sub_classification_details_select_query->bindValue(":jsl_sub_classification_detail_id",$jsl_sub_classification_detail_id_input);
	
      $jsl_sub_classification_details_select_query->execute();
	  
	if($jsl_sub_classification_details_select_query->rowCount() > 0) {
		$jsl_sub_classification_details_select_query_result = $jsl_sub_classification_details_select_query->fetch();
		return $jsl_sub_classification_details_select_query_result;
	
	}//close of if($user_active_token_details_get_select_query->rowCount() > 0) {
	return $constructed_array;
	
}

function jsl_evaluation_info_get($jsl_applicant_evaluation_info_id_input) {
	
	global $dbcon;
	$constructed_array = array();
	
	$jsl_evaluation_info_get_sql = "SELECT * FROM `jsl_applicant_evaluation_info` WHERE `jsl_applicant_evaluation_info_id` =:jsl_applicant_evaluation_info_id";
	$jsl_evaluation_info_get_query = $dbcon->prepare($jsl_evaluation_info_get_sql);
	$jsl_evaluation_info_get_query->bindValue(":jsl_applicant_evaluation_info_id",$jsl_applicant_evaluation_info_id_input);		
	$jsl_evaluation_info_get_query->execute(); 
	
	if($jsl_evaluation_info_get_query->rowCount() > 0) {
		$jsl_evaluation_info_get_query_result = $jsl_evaluation_info_get_query->fetch();
	     return $jsl_evaluation_info_get_query_result;
	
	}
	return $constructed_array;

}

function jsl_applicant_evaluation_info_result_notes_add($company_id_input,$company_client_id_input,$job_id_input,$job_applicant_sm_memb_id_input,$jsl_applicant_evaluation_info_id_input,$notes_input,$notes_safe_html_input,$ea_extracted_jwt_token_sub,$event_datetime,$current_epoch) {
	
	global $dbcon,$eventLog;

	$jsl_applicant_evaluation_info_result_notes_add_sql = "INSERT INTO `jsl_applicant_evaluation_info_result_notes`(`company_id`, `company_client_id`, `job_id`, `candidate_sm_memb_id`, `jsl_applicant_evaluation_info_id`, `notes`, `notes_safe_html`, `added_datetime`, `added_datetime_epoch`, `added_by_sm_memb_id`) VALUES (:company_id,:company_client_id,:job_id,:candidate_sm_memb_id,:jsl_applicant_evaluation_info_id,:notes,:notes_safe_html,:added_datetime,:added_datetime_epoch,:added_by_sm_memb_id)";
	
	$jsl_applicant_evaluation_info_result_notes_add_query = $dbcon->prepare($jsl_applicant_evaluation_info_result_notes_add_sql);
	
	$jsl_applicant_evaluation_info_result_notes_add_query->bindValue(":company_id",$company_id_input);
	
	$jsl_applicant_evaluation_info_result_notes_add_query->bindValue(":company_client_id",$company_client_id_input);

	$jsl_applicant_evaluation_info_result_notes_add_query->bindValue(":job_id",$job_id_input);
	
	$jsl_applicant_evaluation_info_result_notes_add_query->bindValue(":candidate_sm_memb_id",$job_applicant_sm_memb_id_input);
	
	$jsl_applicant_evaluation_info_result_notes_add_query->bindValue(":jsl_applicant_evaluation_info_id",$jsl_applicant_evaluation_info_id_input);
	
	$jsl_applicant_evaluation_info_result_notes_add_query->bindValue(":notes",$notes_input);
	
	$jsl_applicant_evaluation_info_result_notes_add_query->bindValue(":notes_safe_html",$notes_safe_html_input);
	
	$jsl_applicant_evaluation_info_result_notes_add_query->bindValue(":added_datetime",$event_datetime);

	$jsl_applicant_evaluation_info_result_notes_add_query->bindValue(":added_datetime_epoch",$current_epoch);

	$jsl_applicant_evaluation_info_result_notes_add_query->bindValue(":added_by_sm_memb_id",$ea_extracted_jwt_token_sub);

	
	if ($jsl_applicant_evaluation_info_result_notes_add_query->execute()) {
           
		  $last_inserted_id = $dbcon->lastInsertId();
           return $last_inserted_id;

	} else {
		  
			return "";
    }
	
}








?>
