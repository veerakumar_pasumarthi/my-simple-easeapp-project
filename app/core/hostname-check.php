<?php
  defined('START') or die;
 /**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 */ 
//This is to verify the host name where the scipt is executed 

    /* protocol & hostname start */

 if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
   $site_protocol_name = "https://";
 } else {
   $site_protocol_name = "http://";
 }
 

if(isset($_SERVER['HTTP_HOST']) && strstr($_SERVER['HTTP_HOST'], $live_url)) {
    $site_hostname_value = $live_url; //you're on online prod
  
    $html_purified_css_path_initial = $site_hostname_value . $project_main_folder . "/" . "templates/" . $chosen_template . "/css/";
    $html_purified_css = $site_protocol_name . $html_purified_css_path_initial;
  
    $html_purified_js_path_initial = $site_hostname_value . $project_main_folder . "/" . "templates/" . $chosen_template . "/js/";
    $html_purified_js = $site_protocol_name . $html_purified_js_path_initial;
         
    $dbconnection_active = "live";

    $site_basedir_path = $siteroot_basedir_command_line;
   
    $site_home_path = $site_home_path_full;
  
  
    //Frontend Application Project URL Hostname Settings
    $frontend_site_url_project_main = $frontend_site_url_project_main_prod;
    $frontend_site_url_project_main_with_path = $frontend_site_url_project_main_with_path_prod;
  
    //SMTP Settings (Production)
    //Server settings
    $ea_smtp_debug_setting = $ea_smtp_debug_setting_prod;// Enable verbose debug output
    $ea_smtp_host = $ea_smtp_host_prod;  // Specify main and backup SMTP servers
    $ea_smtp_auth_setting = $ea_smtp_auth_setting_prod;// Enable SMTP authentication
    $ea_smtp_username = $ea_smtp_username_prod;// SMTP username
    $ea_smtp_password = $ea_smtp_password_prod;// SMTP password
    $ea_is_smtp_secure_enabled = $ea_is_smtp_secure_enabled_prod;// 0: not enabled, 1: enabled
    $ea_smtp_secure_setting = $ea_smtp_secure_setting_prod;// Enable TLS encryption, `ssl` also accepted, values: tls | ssl
    $ea_smtp_secure_ssl_port_setting = $ea_smtp_secure_ssl_port_setting_prod; 
    $ea_smtp_secure_tls_port_setting = $ea_smtp_secure_tls_port_setting_prod; 
    $ea_smtp_insecure_port_setting = $ea_smtp_insecure_port_setting_prod;
  
  
    //activation email sender info (Prod)
    $activation_email_sender_name = $activation_email_sender_name_prod;
    $activation_email_sender_email = $activation_email_sender_email_prod;
    $activation_email_subject = $activation_email_subject_prod;
    $resend_activation_email_subject = $resend_activation_email_subject_prod;

    //password resetting email sender info (Prod)
    $password_resetting_email_sender_name = $password_resetting_email_sender_name_prod;
    $password_resetting_email_sender_email = $password_resetting_email_sender_email_prod;
    $password_resetting_email_subject = $password_resetting_email_subject_prod;
    $password_resetting_link_expiry_period = $password_resetting_link_expiry_period_prod;//in seconds, hours = 24
	
	//Elasticsearch Settings (PROD)
    /* $elasticsearch_host_array = $elasticsearch_host_array_prod;
	$elasticsearch_index_name = $elasticsearch_index_name_prod;
	$elasticsearch_index_candidate_rel_type = $elasticsearch_index_candidate_rel_type_prod;
	$elasticsearch_index_job_rel_type = $elasticsearch_index_job_rel_type_prod;
 */
    $elasticsearch_host_array = $elasticsearch_host_array_prod;
    $elasticsearch_candidates_index = $elasticsearch_candidates_index_prod;
    $elasticsearch_jobs_index = $elasticsearch_jobs_index_prod;

          
} elseif(isset($_SERVER['HTTP_HOST']) && strstr($_SERVER['HTTP_HOST'], $dev_url)) {
  $site_hostname_value = $dev_url; //you're on online dev
  
  $html_purified_css_path_initial = $site_hostname_value . $project_main_folder . "/" . "templates/" . $chosen_template . "/css/";
  $html_purified_css = $site_protocol_name . $html_purified_css_path_initial;
  
  $html_purified_js_path_initial = $site_hostname_value . $project_main_folder . "/" . "templates/" . $chosen_template . "/js/";
  $html_purified_js = $site_protocol_name . $html_purified_js_path_initial;
         
  $dbconnection_active = "dev";

  $site_basedir_path = $siteroot_basedir_command_line_dev;
  
  $site_home_path = $site_home_path_full_dev;
  
  //Frontend Application Project URL Hostname Settings
  $frontend_site_url_project_main = $frontend_site_url_project_main_dev;
  $frontend_site_url_project_main_with_path = $frontend_site_url_project_main_with_path_dev;
  
  //SMTP Settings (Dev)
  //Server settings
  $ea_smtp_debug_setting = $ea_smtp_debug_setting_dev;// Enable verbose debug output
  $ea_smtp_host = $ea_smtp_host_dev;  // Specify main and backup SMTP servers
  $ea_smtp_auth_setting = $ea_smtp_auth_setting_dev;// Enable SMTP authentication
  $ea_smtp_username = $ea_smtp_username_dev;// SMTP username
  $ea_smtp_password = $ea_smtp_password_dev;// SMTP password
  $ea_is_smtp_secure_enabled = $ea_is_smtp_secure_enabled_dev;// 0: not enabled, 1: enabled
  $ea_smtp_secure_setting = $ea_smtp_secure_setting_dev;// Enable TLS encryption, `ssl` also accepted, values: tls | ssl
  $ea_smtp_secure_ssl_port_setting = $ea_smtp_secure_ssl_port_setting_dev; 
  $ea_smtp_secure_tls_port_setting = $ea_smtp_secure_tls_port_setting_dev; 
  $ea_smtp_insecure_port_setting = $ea_smtp_insecure_port_setting_dev;
  
  //activation email sender info (Dev)
  $activation_email_sender_name = $activation_email_sender_name_dev;
  $activation_email_sender_email = $activation_email_sender_email_dev;
  $activation_email_subject = $activation_email_subject_dev;
  $resend_activation_email_subject = $resend_activation_email_subject_dev;
  
  //password resetting email sender info (Dev)
  $password_resetting_email_sender_name = $password_resetting_email_sender_name_dev;
  $password_resetting_email_sender_email = $password_resetting_email_sender_email_dev;
  $password_resetting_email_subject = $password_resetting_email_subject_dev;
  $password_resetting_link_expiry_period = $password_resetting_link_expiry_period_dev;//in seconds, hours = 24

  //Elasticsearch Settings (DEV)
  //old account details
  /* $elasticsearch_host_array = $elasticsearch_host_array_dev;
  $elasticsearch_index_name = $elasticsearch_index_name_dev;
  $elasticsearch_index_candidate_rel_type = $elasticsearch_index_candidate_rel_type_dev;
  $elasticsearch_index_job_rel_type = $elasticsearch_index_job_rel_type_dev; */
  
  $elasticsearch_host_array = $elasticsearch_host_array_dev;
  $elasticsearch_candidates_index = $elasticsearch_candidates_index_dev;
  $elasticsearch_jobs_index = $elasticsearch_jobs_index_dev;

  
  } else {
    header('Location: ' . html_escaped_output($site_protocol_name . $live_url) . "/");
    exit;
  }
  //echo $dbconnection_active;
  
  $site_url_hostname = $site_protocol_name . $site_hostname_value;
  
  $site_url_project_main = $site_protocol_name . $site_hostname_value . $project_main_folder . "/";
  $site_url_project_main_prefix = $site_protocol_name . $site_hostname_value . $project_main_folder;

  $site_url_login = $site_protocol_name . $site_hostname_value . $project_main_folder . "/login";
  
  $site_url_logout = $site_protocol_name . $site_hostname_value . $project_main_folder . "/logout";
  
  $site_url_forgot_password = $site_protocol_name . $site_hostname_value . $project_main_folder . "/forgot-password";
  
  $site_url_register = $site_protocol_name . $site_hostname_value . $project_main_folder . "/register";
  
  $site_url_main_admin_dashboard = $site_protocol_name . $site_hostname_value . $project_main_folder . "/admin/dashboard";
  
  $site_url_main_member_dashboard = $site_protocol_name . $site_hostname_value . $project_main_folder . "/member/dashboard";
  
      /* protocol & hostname end */

  //Uploaded File Document Storage Absolute Path
  $candidate_document_upload_main_folder_rel_absolute_path = $site_home_path . "uploaded-documents/";
  
?>
