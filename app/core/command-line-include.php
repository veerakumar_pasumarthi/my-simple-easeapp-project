<?php
/**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP 
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 */
//if the directory is one directory above the current file's parent directory http://stackoverflow.com/a/2100763/811207
//include("/home/swnet/public_html/core/server-var-info.php");
include(dirname(dirname(__FILE__)) . "/core/server-var-info.php");
include(dirname(dirname(__FILE__)) . "/core/main-config.php");
echo "after includes\n";


//Include Composer
//include(dirname(dirname(dirname(__FILE__))) . "/public_html/vendor/autoload.php");
require dirname(dirname(dirname(__FILE__))) . "/vendor/autoload.php";

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

//$uuid4 = Uuid::uuid4();
//echo "UUID4: " . $uuid4->toString() . "\n"; // i.e. 25769c6c-d34d-4bfe-ba98-e0ee856f3e7a

//http://stackoverflow.com/questions/18738225/how-to-get-the-actual-current-working-dir-for-a-php-cli-script?rq=1
//http://stackoverflow.com/a/18738438
$file_directory_path = __DIR__; //this is your directory path
echo "after file directory\n";
//echo $file_directory_path . "\n\n";
//http://in3.php.net/manual/en/function.stripos.php
if ((stripos($file_directory_path, $cli_dev_account_ref)) !== false) {
	
  echo "inside if condition\n";
  $dbconnection_active = "dev";
  $var = "dev";
  $siteroot_basedir_cli = $siteroot_basedir_command_line_dev;
  
  $site_basedir_path = $siteroot_basedir_command_line_dev;
  
  $site_home_path = $site_home_path_full_dev;
  
  //Frontend Application Project URL Hostname Settings
  $frontend_site_url_project_main = $frontend_site_url_project_main_dev;
  $frontend_site_url_project_main_with_path = $frontend_site_url_project_main_with_path_dev;
  
  //SMTP Settings (Dev)
  //Server settings
  $ea_smtp_debug_setting = $ea_smtp_debug_setting_dev;// Enable verbose debug output
  $ea_smtp_host = $ea_smtp_host_dev;  // Specify main and backup SMTP servers
  $ea_smtp_auth_setting = $ea_smtp_auth_setting_dev;// Enable SMTP authentication
  $ea_smtp_username = $ea_smtp_username_dev;// SMTP username
  $ea_smtp_password = $ea_smtp_password_dev;// SMTP password
  $ea_is_smtp_secure_enabled = $ea_is_smtp_secure_enabled_dev;// 0: not enabled, 1: enabled
  $ea_smtp_secure_setting = $ea_smtp_secure_setting_dev;// Enable TLS encryption, `ssl` also accepted, values: tls | ssl
  $ea_smtp_secure_ssl_port_setting = $ea_smtp_secure_ssl_port_setting_dev; 
  $ea_smtp_secure_tls_port_setting = $ea_smtp_secure_tls_port_setting_dev; 
  $ea_smtp_insecure_port_setting = $ea_smtp_insecure_port_setting_dev;
  
  //activation email sender info (Dev)
  $activation_email_sender_name = $activation_email_sender_name_dev;
  $activation_email_sender_email = $activation_email_sender_email_dev;
  $activation_email_subject = $activation_email_subject_dev;
  $resend_activation_email_subject = $resend_activation_email_subject_dev;
    
  //password resetting email sender info (Dev)
  $password_resetting_email_sender_name = $password_resetting_email_sender_name_dev;
  $password_resetting_email_sender_email = $password_resetting_email_sender_email_dev;
  $password_resetting_email_subject = $password_resetting_email_subject_dev;
  $password_resetting_link_expiry_period = $password_resetting_link_expiry_period_dev;//in seconds, hours = 24
  
  //Elasticsearch Settings (DEV)
  //old account related config
  /* $elasticsearch_host_array = $elasticsearch_host_array_dev;
  $elasticsearch_index_name = $elasticsearch_index_name_dev;
  $elasticsearch_index_candidate_rel_type = $elasticsearch_index_candidate_rel_type_dev;
  $elasticsearch_index_job_rel_type = $elasticsearch_index_job_rel_type_dev; */
  
  $elasticsearch_host_array = $elasticsearch_host_array_dev;
  $elasticsearch_candidates_index = $elasticsearch_candidates_index_dev;
  $elasticsearch_jobs_index = $elasticsearch_jobs_index_dev;
  
  
} elseif ((stripos($file_directory_path, $cli_live_account_ref)) !== false) {
  echo "inside elseif condition\n";
  $dbconnection_active = "live";
  $var = "live";
  $siteroot_basedir_cli = $siteroot_basedir_command_line;
  
  $site_basedir_path = $siteroot_basedir_command_line;
  
  $site_home_path = $site_home_path_full;
  
  //Frontend Application Project URL Hostname Settings
  $frontend_site_url_project_main = $frontend_site_url_project_main_prod;
  $frontend_site_url_project_main_with_path = $frontend_site_url_project_main_with_path_prod;
  
  //SMTP Settings (Production)
  //Server settings
  $ea_smtp_debug_setting = $ea_smtp_debug_setting_prod;// Enable verbose debug output
  $ea_smtp_host = $ea_smtp_host_prod;  // Specify main and backup SMTP servers
  $ea_smtp_auth_setting = $ea_smtp_auth_setting_prod;// Enable SMTP authentication
  $ea_smtp_username = $ea_smtp_username_prod;// SMTP username
  $ea_smtp_password = $ea_smtp_password_prod;// SMTP password
  $ea_is_smtp_secure_enabled = $ea_is_smtp_secure_enabled_prod;// 0: not enabled, 1: enabled
  $ea_smtp_secure_setting = $ea_smtp_secure_setting_prod;// Enable TLS encryption, `ssl` also accepted, values: tls | ssl
  $ea_smtp_secure_ssl_port_setting = $ea_smtp_secure_ssl_port_setting_prod; 
  $ea_smtp_secure_tls_port_setting = $ea_smtp_secure_tls_port_setting_prod; 
  $ea_smtp_insecure_port_setting = $ea_smtp_insecure_port_setting_prod;
  
  //activation email sender info (Prod)
  $activation_email_sender_name = $activation_email_sender_name_prod;
  $activation_email_sender_email = $activation_email_sender_email_prod;
  $activation_email_subject = $activation_email_subject_prod;
  $resend_activation_email_subject = $resend_activation_email_subject_prod;

  //password resetting email sender info (Prod)
  $password_resetting_email_sender_name = $password_resetting_email_sender_name_prod;
  $password_resetting_email_sender_email = $password_resetting_email_sender_email_prod;
  $password_resetting_email_subject = $password_resetting_email_subject_prod;
  $password_resetting_link_expiry_period = $password_resetting_link_expiry_period_prod;//in seconds, hours = 24

  //Elasticsearch Settings (PROD)
  /* $elasticsearch_host_array = $elasticsearch_host_array_prod;
  $elasticsearch_index_name = $elasticsearch_index_name_prod;
  $elasticsearch_index_candidate_rel_type = $elasticsearch_index_candidate_rel_type_prod;
  $elasticsearch_index_job_rel_type = $elasticsearch_index_job_rel_type_prod;
   */
  $elasticsearch_host_array = $elasticsearch_host_array_prod;
  $elasticsearch_candidates_index = $elasticsearch_candidates_index_prod;
  $elasticsearch_jobs_index = $elasticsearch_jobs_index_prod;
  
  
}

  //Uploaded File Document Storage Absolute Path
  $candidate_document_upload_main_folder_rel_absolute_path = $site_home_path . "uploaded-documents/";
  


include(dirname(dirname(__FILE__)) . "/class/PDOEx.php");
echo "after/class/PDOEx.php \n";
include(dirname(dirname(__FILE__)) . "/core/db-connect-main.php");
echo "after core/db-connect-main.php \n";
include(dirname(dirname(__FILE__)) . "/class/EALogger.php");
echo "after /class/EALogger.php \n ";
include(dirname(dirname(__FILE__)) . "/class/EASQLDBManager.php");
echo "after /class/EASQLDBManager.php \n";
//HTMLawed Library to purify and filter HTML (http://www.bioinformatics.org/phplabware/internal_utilities/htmLawed/)
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/htmLawed-v1241.php"); 
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/validate-sanitize-functions.php");
echo "after /app/includes/validate-sanitize-functions.php \n";
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/date-functions.php");
echo "after /app/includes/date-functions.php \n";
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/mysql-mariadb-database-functions.php");
echo "after /app/includes/mysql-mariadb-database-functions.php \n";
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/other-functions.php");
echo "after/app/includes/other-functions.php \n ";
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/expiring-links.php");
echo "after/app/includes/expiring-links.php \n ";
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/uuid.php");
echo "after /app/includes/uuid.php \n";
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/extract-links-emails-from-html.php");
echo "after /app/includes/extract-links-emails-from-html.php \n";

//PHPMailer Library: This is to send Email through SMTP / Sendmail in PHP Scripts
/* include(dirname(dirname(dirname(__FILE__))) . "/app/includes/phpmailer-v605/src/Exception.php");
echo "after /app/includes/phpmailer-v605/src/Exception.php \n ";
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/phpmailer-v605/src/PHPMailer.php");
echo "after /app/includes/phpmailer-v605/src/PHPMailer.php \n";
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/phpmailer-v605/src/SMTP.php");
echo "after /app/includes/phpmailer-v605/src/SMTP.php \n";
 */
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
echo "PHPMailer\n";
use PHPMailer\PHPMailer\Exception;
echo "Exception \n";
use PHPMailer\PHPMailer\SMTP;
echo "SMTP\n";

//Define Sendmail Transport
$phpmailer_sendmail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
$phpmailer_sendmail->IsSendmail(); // telling the class to use SendMail transport
$phpmailer_sendmail->CharSet="utf-8";
echo "after sendmail transport \n";


//Define SMTP Transport
$phpmailer_smtp = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
/*//Server settings
$phpmailer_smtp->SMTPDebug = 2;                                 // Enable verbose debug output
$phpmailer_smtp->isSMTP();                                      // Set mailer to use SMTP
//$phpmailer_smtp->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
$phpmailer_smtp->Host = 'vdmail.securitywonks.net';  // Specify main and backup SMTP servers
$phpmailer_smtp->SMTPAuth = true;                               // Enable SMTP authentication
$phpmailer_smtp->Username = 'notifications@vdmail.securitywonks.net';                 // SMTP username
$phpmailer_smtp->Password = 'notifications';                           // SMTP password
//$phpmailer_smtp->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
$phpmailer_smtp->Port = 26;*/


$phpmailer_smtp->SMTPDebug = $ea_smtp_debug_setting;                                 // Enable verbose debug output
$phpmailer_smtp->isSMTP();                                      // Set mailer to use SMTP
//$phpmailer_smtp->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
$phpmailer_smtp->Host = $ea_smtp_host;  // Specify main and backup SMTP servers
$phpmailer_smtp->SMTPAuth = $ea_smtp_auth_setting;                               // Enable SMTP authentication
$phpmailer_smtp->Username = $ea_smtp_username;                 // SMTP username
$phpmailer_smtp->Password = $ea_smtp_password;                           // SMTP password
	

if ($ea_is_smtp_secure_enabled == "1") {
	//Secure SMTP Mode
	
	if ($ea_smtp_secure_setting == "tls") {
		//TLS Mode
		$phpmailer_smtp->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$phpmailer_smtp->Port = $ea_smtp_secure_tls_port_setting;       // TCP port to connect to
		
	} else if ($ea_smtp_secure_setting == "ssl") {
		//SSL Mode
		$phpmailer_smtp->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		$phpmailer_smtp->Port = $ea_smtp_secure_ssl_port_setting;       // TCP port to connect to
		
	}//close of else if of if ($ea_smtp_secure_setting == "tls") {
	
	
} else {
	//In Secure SMTP Mode
	$phpmailer_smtp->Port = 26;                                    // TCP port to connect to
	
}//close of else if of if ($ea_is_smtp_secure_enabled == "0") {      

echo "after SMTP \n";
//This hosts any miscellaneous set of functions that are useful and that do not fit elsewhere
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/misc-functions.php");

echo "after misc function includes\n";


if (version_compare(PHP_VERSION, '7.2.0') >= 0) {
	echo "after version compare condition\n";
	echo 'I am at least PHP version 7.2.0, my version: ' . PHP_VERSION . "\n<br><br>";
	//Include Constant Time Encoding Library v2.0 from Paragonie
	include(dirname(dirname(dirname(__FILE__))) . "/app/includes/constant-time-encoding-v20/constant-time-encoding-v20-includes-commandline.php");
	echo "after Halite Library v402 \n";
	//Include Halite Library from Paragonie
	//include(dirname(dirname(dirname(__FILE__))) . "/app/includes/halite-v402/halite-v402-includes-commandline.php");
	include(dirname(dirname(dirname(__FILE__))) . "/app/includes/halite-v441/halite-v441-includes-commandline.php");
	echo "after Halite Library v441 \n";
	
	/*//Check if Libsodium is setup correctly, result will be bool(true), if all is good, https://stackoverflow.com/a/37687595
	//var_dump(ParagonIE\Halite\Halite::isLibsodiumSetupCorrectly());
	//echo ParagonIE\Halite\Halite::isLibsodiumSetupCorrectly(true);	
	//echo "<br><br>";
	//Check the installed versions of Libsodium
	echo "<br>var_dump result<br>";
	var_dump([
		SODIUM_LIBRARY_MAJOR_VERSION,
		SODIUM_LIBRARY_MINOR_VERSION,
		SODIUM_LIBRARY_VERSION
	]);
	echo "<br>print_r result<br>";
	print_r([
		SODIUM_LIBRARY_MAJOR_VERSION,
		SODIUM_LIBRARY_MINOR_VERSION,
		SODIUM_LIBRARY_VERSION
	]);
	echo "<br><hr><br><pre>";
	*/

	
	//Check, if Libsodium is setup correctly
	//if (ParagonIE\Halite\Halite::isLibsodiumSetupCorrectly() === true) {
		//echo "After Libsodium setup check";
		//echo "true<br>";
		/*Retrieve the previous saved Symmetric Encryption key from the file
		$pg_symmetric_encryption_key = \ParagonIE\Halite\KeyFactory::loadEncryptionKey($site_home_path . $pg_generated_enc_keys_folder_name. $pg_symmetric_encryption_key_filename);*/

		//Retrieve the previous saved Asymmetric Anonymous Encryption key from the file
		//$pg_asymmetric_anonymous_encryption_keypair = \ParagonIE\Halite\KeyFactory::loadEncryptionKeyPair($site_home_path . $pg_generated_enc_keys_folder_name. $pg_asymmetric_anonymous_encryption_keypair_filename);

		//$pg_asymmetric_anonymous_encryption_secret_key = $pg_asymmetric_anonymous_encryption_keypair->getSecretKey();
		//$pg_asymmetric_anonymous_encryption_public_key = $pg_asymmetric_anonymous_encryption_keypair->getPublicKey();
		
		//Retrieve the previous saved Asymmetric Anonymous Encryption key for Logs from the file
		//$pg_asymmetric_anonymous_encryption_logs_keypair = \ParagonIE\Halite\KeyFactory::loadEncryptionKeyPair($site_home_path . $pg_generated_enc_keys_folder_name. $pg_asymmetric_anonymous_encryption_logs_keypair_filename);

		//$pg_asymmetric_anonymous_encryption_logs_secret_key = $pg_asymmetric_anonymous_encryption_logs_keypair->getSecretKey();
		//$pg_asymmetric_anonymous_encryption_logs_public_key = $pg_asymmetric_anonymous_encryption_logs_keypair->getPublicKey();

		//Retrieve the previous saved Asymmetric Authentication key from the file
		//$pg_asymmetric_authentication_keypair = \ParagonIE\Halite\KeyFactory::loadSignatureKeyPair($site_home_path . $pg_generated_enc_keys_folder_name. $pg_asymmetric_authentication_keypair_filename);

		//$pg_asymmetric_authentication_secret_key = $pg_asymmetric_authentication_keypair->getSecretKey();
		//$pg_asymmetric_authentication_public_key = $pg_asymmetric_authentication_keypair->getPublicKey();
		
		//Retrieve the previous saved Asymmetric Authentication key for Logs from the file
		//$pg_asymmetric_authentication_logs_keypair = \ParagonIE\Halite\KeyFactory::loadSignatureKeyPair($site_home_path . $pg_generated_enc_keys_folder_name. $pg_asymmetric_authentication_logs_keypair_filename);

		//$pg_asymmetric_authentication_logs_secret_key = $pg_asymmetric_authentication_logs_keypair->getSecretKey();
		//$pg_asymmetric_authentication_logs_public_key = $pg_asymmetric_authentication_logs_keypair->getPublicKey();
	//}//close of if (ParagonIE\Halite\Halite::isLibsodiumSetupCorrectly() === true) {
		
	
	
	
}//close of if (version_compare(PHP_VERSION, '7.2.0') >= 0) {






include(dirname(dirname(dirname(__FILE__))) . "/app/includes/other-functions-api.php");
//echo "after other functions include\n";

//Elasticsearch

use Elasticsearch\ClientBuilder;

//Elasticsearch Client Connection
$elasticsearch_client = ClientBuilder::create()
						->setHosts($elasticsearch_host_array)
						->setRetries(0)
						->build();

//Elasticsearch: Define Index name
$elasticsearchCandidateIndexParams['index']  = $elasticsearch_candidates_index;   

//Elasticsearch: Index Exist Check
$elasticsearch_candidate_db_existence_status_result = $elasticsearch_client->indices()->exists($elasticsearchCandidateIndexParams);

//Check if index exists Result is true, and Create Index, if it isn't
if ($elasticsearch_candidate_db_existence_status_result === false) {
	//Create index (as noun / Database)
	
	$candidateMapping = array(
		'properties' => array(
			'sm_memb_id' => array(
				'type' => 'integer'
			),
			'badges' => array(
				'type' => 'integer'
			),
			'sm_email' => array(
				'type' => 'keyword'
			),
			'sm_alternate_email' => array(
				'type' => 'keyword'
			),
			'sm_mobile' => array(
				'type' => 'keyword'
			),
			'sm_salutation' => array(
				'type' => 'keyword'
			),
			'sm_middlename' => array(
				'type' => 'keyword'
			),
			'sm_lastname' => array(
				'type' => 'keyword'
			),
			'sm_fullname' => array(
				'type' => 'keyword'
			),
			'sm_city' => array(
				'type' => 'keyword'
			),
			'sm_state' => array(
				'type' => 'keyword'
			),
			'sm_country' => array(
				'type' => 'keyword'
			),
			'company_id' => array(
				'type' => 'keyword'
			),
			'crur_id' => array(
				'type' => 'integer'
			),
			'original_filename' => array(
				'type' => 'keyword'
			),
			'resume_extracted_info_id' => array(
				'type' => 'integer'
			),
			'resume_text' => array(
				'type' => 'text'
			)
		)
	);
	
	$elasticsearchCandidateIndexParams['body']['mappings'] = $candidateMapping;
	//http://www.searchly.com/docs/php
	//Create Index (Database)
	$elasticsearch_client->indices()->create($elasticsearchCandidateIndexParams);

}//close of if ($elasticsearch_candidate_db_existence_status_result === false) {

//Elasticsearch: Define Index name
$elasticsearchJobIndexParams['index']  = $elasticsearch_jobs_index;   

//Elasticsearch: Index Exist Check
$elasticsearch_jobs_db_existence_status_result = $elasticsearch_client->indices()->exists($elasticsearchJobIndexParams);

//Check if index exists Result is true, and Create Index, if it isn't
if ($elasticsearch_jobs_db_existence_status_result === false) {
	//Create index (as noun / Database)
	
	$jobMapping = array(
	
		'properties' => array(

			'job_id' => array(
				'type' => 'integer'
			),
			'badges' => array(
				'type' => 'integer'
			),
			'company_id' => array(
				'type' => 'integer'
			),
			'company_client_id' => array(
				'type' => 'integer'
			),
			'job_posted_by_member_classification_detail_id' => array(
				'type' => 'integer'
			),
			'job_posted_by_sm_memb_id' => array(
				'type' => 'integer'
			),
			'job_title' => array(
				'type' => 'text'
			),

			'job_summary' => array(
				'type' => 'text'
			),

			'job_full_description' => array(
				'type' => 'text'
			),
		
			'job_type_id' => array(
				'type' => 'integer'
			),
			'address_line_1' => array(
				'type' => 'text'
			),
			'address_line_2' => array(
				'type' => 'text'
			),
			'city' => array(
				'type' => 'text'
			),
			'state' => array(
				'type' => 'text'
			),
			'country' => array(
				'type' => 'text'
			),
			'zipcode' => array(
				'type' => 'keyword'
			),
			'job_posted_date_time' => array(
				'type' => 'text'
			),
			'job_recruitment_status' => array(
				'type' => 'integer'
			),
			'no_of_openings' => array(
				'type' => 'integer'
			),
			'available_positions' => array(
				'type' => 'integer'
			)
			
		)
	);
	
	$elasticsearchJobIndexParams['body']['mappings'] = $jobMapping;
	//http://www.searchly.com/docs/php
	//Create Index (Database)
	$elasticsearch_client->indices()->create($elasticsearchJobIndexParams);

}//close of if ($elasticsearch_jobs_db_existence_status_result === false) {
	


//jeremykendall-php-domain-parser start https://github.com/jeremykendall/php-domain-parser
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/jeremykendall-php-domain-parser/src/Pdp/PublicSuffixListManager.php");
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/jeremykendall-php-domain-parser/src/Pdp/PublicSuffixList.php");
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/jeremykendall-php-domain-parser/src/Pdp/Parser.php");
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/jeremykendall-php-domain-parser/src/pdp-parse-url.php");
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/jeremykendall-php-domain-parser/src/Pdp/Uri/Url/Host.php");
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/jeremykendall-php-domain-parser/src/Pdp/Uri/Url.php");

use Pdp\PublicSuffixListManager;
use Pdp\Parser;
// Obtain an instance of the parser
$pslManager = new PublicSuffixListManager();
$parser = new Parser($pslManager->getList());
//jeremykendall-php-domain-parser end https://github.com/jeremykendall/php-domain-parser
  //echo $var . "\n";
$starttime = time();
ini_set("max_execution_time","18000");
ignore_user_abort(2); 
//echo "\n dbconnection_active  = ".$dbconnection_active;
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

//FPDF to Create PDF Files
include(dirname(dirname(dirname(__FILE__))) . "/app/includes/fpdf-181/fpdf.php");




$siteLogPath = dirname(dirname(dirname(__FILE__))) . "/easeapp-logs/";

//Include a Custome Halite Operation
include(dirname(dirname(dirname(__FILE__))) . "/app/class/EAHalite.php");
$objEAHalite = new EAHalite();

$db = new DB();


?>
