<?php
  defined('START') or die;
 /**
 * Easeapp PHP Framework - A Simple MVC based Procedural Framework in PHP
 *
 * @package  Easeapp
 * @author   Raghu Veer Dendukuri <raghuveer.d@easeapp.org>
 * @website  http://www.easeapp.org
 * @license  The Easeapp PHP framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
 * @copyright Copyright (c) 2014-2018 Raghu Veer Dendukuri, excluding any third party code / libraries, those that are copyrighted to / owned by it's Authors and / or              * Contributors and is licensed as per their Open Source License choices.
 */
 /*
  * This is to host the core framework related routing engine rules
  *
  * I. some different combinations and related meanings (this is to differentiate whether the page is a frontend page or a admin panel page or related to Ajax / API Service):
  *
  * 1) "is_ajax" => "0", "is_frontend_page" => "0"
  * This means, http request is normal (not ajax) and admin panel template has to be loaded
  *
  * 2) "is_ajax" => "0", "is_frontend_page" => "1"
  * This means, http request is normal (not ajax) and frontend template has to be loaded
  *
  * 3) "is_ajax" => "0", "is_frontend_page" => "2"
  * This means, http request is normal (not ajax) and based on logged in user type, either frontend template or admin panel template has to be loaded
  *
  * 4) "is_ajax" => "0", "is_frontend_page" => "3"
  * This means, http request is normal (not ajax) and this represents, inappropriate template settings
  *
  * 5) "is_ajax" => "1", "is_frontend_page" => "3"
  * This means, http request is an ajax request and this represents, pure ajax call/web service call
  * a) "is_web_service_endpoint" => "0"
  * This means, http request is an ajax request and this represents, pure ajax call
  * b) "is_web_service_endpoint" => "1"
  * This means, http request is an ajax request and this represents, web service endpoint
  * c) "is_web_service_endpoint" => "2"
  * This means, http request is an ajax request and this represents, either ajax call or web service endpoint
  * d) "is_web_service_endpoint" => "3"
  * This means, http request is basically not an ajax request
  *
  *
  *
  * II. This is to check the Request Method of the request
  *
  * 1) "request_method" => "ANY"
  * This means, there is no restriction about the METHOD that is used for this http / https request (GET / POST / PUT / DELETE all works), if the VALUE is ANY.
  *
  * 2) "request_method" => "GET"
  * This means, only requests that is initiated using GET METHOD are allowed, if the VALUE is GET.
  *
  * 3) "request_method" => "POST"
  * This means, only requests that is initiated using POST METHOD are allowed, if the VALUE is POST.
  *
  * 4) "request_method" => "PUT"
  * This means, only requests that is initiated using PUT METHOD are allowed, if the VALUE is PUT.
  *
  * 5) "request_method" => "DELETE"
  * This means, only requests that is initiated using DELETE METHOD are allowed, if the VALUE is DELETE.
  *
  * 6) "request_method" => "HEAD" (NOT COMPLETED)
  * This means, only requests that is initiated using HEAD METHOD are allowed, if the VALUE is HEAD. The Script will be terminated after outputting headers if output buffering is not enabled.
  *
  */
//Different Routing Engine Rules
$user_defined_routes = array();

//API to provide Candidates List, in Admin Panel
$user_defined_routes["rest-candidates-list"] = array("route_value" => "/rest/candidates/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidates-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

//API to provide Admin Users (different user groups) List, in Admin Panel
$user_defined_routes["rest-admin-users-list"] = array("route_value" => "/rest/admin-users/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-admin-users-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

//API to Collect Quick Candidate Info, as part of Candidate Initial On-boarding process, in Admin Panel
$user_defined_routes["rest-quick-onboarding-flow-candidate-add"] = array("route_value" => "/rest/quick-onboarding-flow/candidate/add",
							 "route_var_count" => "5",
							 "page_filename" => "rest-quick-onboarding-flow-candidate-add.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
							
//API to Edit Quick Candidate Info, as part of Candidate Initial On-boarding process, in Admin Panel
$user_defined_routes["rest-quick-onboarding-flow-candidate-edit"] = array("route_value" => "/rest/quick-onboarding-flow/candidate/edit",
							 "route_var_count" => "5",
							 "page_filename" => "rest-quick-onboarding-flow-candidate-edit.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);


//API to Collect Quick Admin User Info, as part of Admin user Initial On-boarding process, in Admin Panel
$user_defined_routes["rest-quick-onboarding-flow-admin-user-add"] = array("route_value" => "/rest/quick-onboarding-flow/admin-user/add",
							 "route_var_count" => "5",
							 "page_filename" => "rest-quick-onboarding-flow-admin-user-add.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

//API to Edit Quick Admin User Info, as part of Admin user Initial On-boarding process, in Admin Panel
$user_defined_routes["rest-quick-onboarding-flow-admin-user-edit"] = array("route_value" => "/rest/quick-onboarding-flow/admin-user/edit",
							 "route_var_count" => "5",
							 "page_filename" => "rest-quick-onboarding-flow-admin-user-edit.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
							
//API to get Admin User details, in Admin Panel
$user_defined_routes["rest-admin-user-details-get"] = array("route_value" => "/rest/admin-user-details/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-admin-user-details-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
							
//API to get Candidate details, in Admin Panel
$user_defined_routes["rest-candidate-details-get"] = array("route_value" => "/rest/candidate-details/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-details-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
							
							

//API to provide Companies (different company groups) List, in Admin Panel
$user_defined_routes["rest-companies-list"] = array("route_value" => "/rest/companies/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-companies-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);



//API to Collect File Uploads
$user_defined_routes["rest-user-file-upload"] = array("route_value" => "/rest/user/file-upload",
							 "route_var_count" => "4",
							 "page_filename" => "rest-user-file-upload.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);




$user_defined_routes["candidate-rel-pdf-document-display"] = array("route_value" => "/viewer/" . $routing_eng_var_2 . "/candidate/" . $routing_eng_var_4 . "/uploaded-file-ref-id/" . $routing_eng_var_6 . "/display/expires/" . $routing_eng_var_9 . "/link-signature/" . $routing_eng_var_11,
                                    "route_var_count" => "12",
                                    //"page_filename" => is_session_valid($_SESSION['loggedin'], "candidate-uploaded-pdf-document-display.php"),
									"page_filename" => "candidate-uploaded-pdf-document-display.php",
                                    "is_ajax" => "1",
									"is_web_service_endpoint" => "0",
                                    "is_frontend_page" => "3",
						            "request_method" => "ANY"
                                   );

//candidates-multiple-resumes-download-zip
$user_defined_routes["candidate-rel-multiple-resumes-download"] = array("route_value" => "/viewer/" . $routing_eng_var_2 . "/candidate-resumes/" . $routing_eng_var_4 . "/download/expires/" . $routing_eng_var_7 . "/link-signature/" . $routing_eng_var_9,
                                    "route_var_count" => "10",
                                    "page_filename" => "candidate-rel-multiple-resumes-download.php",
                                    "is_ajax" => "1",
									"is_web_service_endpoint" => "0",
                                    "is_frontend_page" => "3",
						            "request_method" => "ANY"
                                   );
								   
$user_defined_routes["candidate-rel-resume-download"] = array("route_value" => "/viewer/" . $routing_eng_var_2 . "/candidate/" . $routing_eng_var_4 . "/uploaded-file-ref-id/" . $routing_eng_var_6 . "/download/expires/" . $routing_eng_var_9 . "/link-signature/" . $routing_eng_var_11,

//array("route_value" => "/viewer/" . $routing_eng_var_2 . "/candidate/" . $routing_eng_var_4 ."/uploaded-file-ref-id/" . $routing_eng_var_6 . "/download/expires/" . $routing_eng_var_9 . "/link-signature/" . $routing_eng_var_11,
                                    "route_var_count" => "12",
                                    //"page_filename" => is_session_valid($_SESSION['loggedin'], "candidate-rel-all-documents-download.php"),
									"page_filename" => "candidate-rel-resume-download.php",
                                    "is_ajax" => "1",
									"is_web_service_endpoint" => "0",
                                    "is_frontend_page" => "3",
						            "request_method" => "ANY"
                                   );

//API to Collect IPv4 Address of the Client Browser
$user_defined_routes["rest-client-ipv4-address-collect"] = array("route_value" => "/rest/client-ipv4-address/collect",
							 "route_var_count" => "4",
							 "page_filename" => "rest-client-ipv4-address-collect.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "GET"
							);



// *********Staffing and Recruitment application related rules**********

//API to add company, in Admin Panel
$user_defined_routes["rest-company-add"] = array("route_value" => "/rest/company/add",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-add.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
                            );
//API to add candidate, in Admin panel
$user_defined_routes["rest-candidate-add"] = array("route_value" => "/rest/candidate/add",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-add.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

//API to add candidate, in Admin panel
$user_defined_routes["rest-candidate-edit"] = array("route_value" => "/rest/candidate/edit",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-edit.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
							
//API to edit company, in Admin panel
$user_defined_routes["rest-company-edit"] = array("route_value" => "/rest/company/edit",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-edit.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);	
//API to disable company, in Admin panel
$user_defined_routes["rest-company-disable"] = array("route_value" => "/rest/company/disable",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-disable.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

//API to disable company office adderss, in Admin panel
$user_defined_routes["rest-company-office-address-disable"] = array("route_value" => "/rest/company-office-address/disable",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-office-address-disable.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
							
//API to disable company client office adderss, in Admin panel
$user_defined_routes["rest-company-client-office-address-disable"] = array("route_value" => "/rest/company-client-office-address/disable",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-client-office-address-disable.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

//API to get company details, in Admin panel
$user_defined_routes["rest-company-details-get"] = array("route_value" => "/rest/company-details/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-details-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);								
//API to add client company, in Admin Panel
$user_defined_routes["rest-company-client-add"] = array("route_value" => "/rest/company-client/add",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-client-add.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to edit client company, in Admin Panel
$user_defined_routes["rest-company-clients-edit"] = array("route_value" => "/rest/company-clients/edit",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-clients-edit.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to disable client company, in Admin Panel
$user_defined_routes["rest-company-clients-disable"] = array("route_value" => "/rest/company-clients/disable",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-clients-disable.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to details of client company, in Admin Panel
$user_defined_routes["rest-company-client-details-get"] = array("route_value" => "/rest/company-client-details/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-client-details-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);								

//API to provide Companies (different company groups) List, in Admin Panel
$user_defined_routes["rest-company-registration-types-list"] = array("route_value" => "/rest/company/registration-types/list",
							 "route_var_count" => "5",
							 "page_filename" => "rest-company-registration-types-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);


//API to provide Companies (different company groups) List, in Admin Panel
$user_defined_routes["rest-company-clients-list"] = array("route_value" => "/rest/company-clients/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-clients-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

//API to provide Companies (different company groups) List, in Admin Panel
$user_defined_routes["rest-job-types-list"] = array("route_value" => "/rest/job-types/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-types-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to provide jobs List, in Admin Panel
$user_defined_routes["rest-jobs-list"] = array("route_value" => "/rest/jobs/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-jobs-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
							
//API to get job details, in Admin Panel
$user_defined_routes["rest-job-details-get"] = array("route_value" => "/rest/job-details/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-details-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to change job status, in Admin Panel
$user_defined_routes["rest-job-disable"] = array("route_value" => "/rest/job/disable",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-disable.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

//API to provide Companies (different company groups) List, in Admin Panel
$user_defined_routes["rest-job-experience-levels-list"] = array("route_value" => "/rest/job-experience-levels/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-experience-levels-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

//API to provide Companies (different company groups) List, in Admin Panel
$user_defined_routes["rest-job-compensation-period-list"] = array("route_value" => "/rest/job-compensation-period/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-compensation-period-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to provide country phonecodes List, in Admin Panel
$user_defined_routes["rest-countries-phonecodes-list"] = array("route_value" => "/rest/countries-phonecodes/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-countries-phonecodes-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

//API to provide Companies (different company groups) List, in Admin Panel
$user_defined_routes["rest-job-work-location-requirements-list"] = array("route_value" => "/rest/job-work-location-requirements/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-work-location-requirements-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

//API to provide Companies (different company groups) List, in Admin Panel
$user_defined_routes["rest-job-industries-list"] = array("route_value" => "/rest/job-industries/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-industries-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to provide Companies (different company groups) List, in Admin Panel
$user_defined_routes["rest-currencies-list"] = array("route_value" => "/rest/currencies/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-currencies-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

//API to add company client office address add, in Admin Panel
$user_defined_routes["rest-company-client-office-address-add"] = array("route_value" => "/rest/company-client-office-address/add",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-client-office-address-add.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to add company office address add, in Admin Panel
$user_defined_routes["rest-company-office-address-add"] = array("route_value" => "/rest/company-office-address/add",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-office-address-add.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to add company office address add, in Admin Panel
$user_defined_routes["rest-company-office-addresses-list"] = array("route_value" => "/rest/company-office-addresses/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-office-addresses-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

//API to provide Company Client Office Addresses List, in Admin Panel
$user_defined_routes["rest-company-client-office-addresses-list"] = array("route_value" => "/rest/company-client-office-addresses/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-client-office-addresses-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to provide Company Client Office Addresses List, in Admin Panel
$user_defined_routes["rest-job-applicant-invite"] = array("route_value" => "/rest/job-applicant/invite",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-applicant-invite.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

//API for Company change status
$routes["rest-company-change-status"] = array("route_value" => "/rest/company/change-status",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-change-status.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

/* //Web Hook: To Collect Received Email Content, from Sendgrid, w.r.t. Recruiter specific Received Resumes
 $routes["rest-company-change-status"] = array("route_value" => "/rest/company/change-status",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-change-status.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
 */
//API to provide Countries List, in Admin Panel
$user_defined_routes["rest-countries-list"] = array("route_value" => "/rest/countries/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-countries-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
							
//API to provide resumes list based on keyword search, in Admin Panel
$user_defined_routes["rest-resume-keyword-search"] = array("route_value" => "/rest/resumes/keyword-search",
							 "route_var_count" => "4",
							 "page_filename" => "rest-resume-keyword-search.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to provide add job, in Admin Panel
$user_defined_routes["rest-job-add"] = array("route_value" => "/rest/job/add",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-add.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to provide edit job, in Admin Panel
$user_defined_routes["rest-job-edit"] = array("route_value" => "/rest/job/edit",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-edit.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to get company client office address details, in Admin Panel
$user_defined_routes["rest-company-client-office-address-get"] = array("route_value" => "/rest/company-client/office-address/get",
							 "route_var_count" => "5",
							 "page_filename" => "rest-company-client-office-address-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to get company office address details, in Admin Panel
$user_defined_routes["rest-company-office-address-get"] = array("route_value" => "/rest/company/office-address/get",
							 "route_var_count" => "5",
							 "page_filename" => "rest-company-office-address-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to edit company client office address details, in Admin Panel
$user_defined_routes["rest-company-client-office-address-edit"] = array("route_value" => "/rest/company-client/office-address/edit",
							 "route_var_count" => "5",
							 "page_filename" => "rest-company-client-office-address-edit.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

//API to get multiple file download requests, in Admin Panel
$user_defined_routes["rest-multiple-file-download-request-get"] = array("route_value" => "/rest/multiple-file/download-request/get",
							 "route_var_count" => "5",
							 "page_filename" => "rest-multiple-file-download-request-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							 
							);	
//API to edit company office address , in Admin Panel
$user_defined_routes["rest-job-recruitment-change-status"] = array("route_value" => "/rest/job-recruitment-change-status",
							 "route_var_count" => "3",
							 "page_filename" => "rest-job-recruitment-change-status.php",
                             "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"

						    );	
//API to edit company office address , in Admin Panel
$user_defined_routes["rest-candidate-invite-jobs-change-status"] = array("route_value" => "/rest/candidate-invite-jobs-change-status",
							 "route_var_count" => "3",
							 "page_filename" => "rest-candidate-invite-jobs-change-status.php",
                             "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"

						    );		
//API to change the resume submission end date , in Admin Panel
$user_defined_routes["rest-job-change-resume-submission-end-date"] = array("route_value" => "/rest/job-change-resume-submission-end-date",
							 "route_var_count" => "3",
							 "page_filename" => "rest-job-change-resume-submission-end-date.php",
                             "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"

						    );									

//API to edit company office address , in Admin Panel
$user_defined_routes["rest-company-office-address-edit"] = array("route_value" => "/rest/company-office-address/edit",
							 "route_var_count" => "4",
							 "page_filename" => "rest-company-office-address-edit.php",
                             "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"

						    );
//API to Collect Education Qualifications, in one go, from Candidate, as part of the Candidate's Visa Documentation Collection
$user_defined_routes["rest-candidate-all-education-qualifications-collect"] = array("route_value" => "/rest/candidate/all-education-qualifications/collect",
							 "route_var_count" => "5",
							 "page_filename" => "rest-candidate-all-education-qualifications-collect.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST" 							 
							);


//API to Collect Professional Summary, in one go, from Candidate, as part of the Candidate's Visa Documentation Collection
$user_defined_routes["rest-candidate-all-professional-summary-collect"] = array("route_value" => "/rest/candidate/all-professional-summary/collect",
							 "route_var_count" => "5",
							 "page_filename" => "rest-candidate-all-professional-summary-collect.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST" 							 
							);

$user_defined_routes["rest-profile-sources-list"] = array("route_value" => "/rest/profile-sources/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-profile-sources-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);


//API to Collect Professional Summary, in one go, from Candidate, as part of the Candidate's Visa Documentation Collection
$user_defined_routes["rest-candidate-all-professional-summary-collect"] = array("route_value" => "/rest/candidate/all-professional-summary/collect",
							 "route_var_count" => "5",
							 "page_filename" => "rest-candidate-all-professional-summary-collect.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST" 							 
							);								

//API to provide add candidate aditional information, in Admin Panel
$user_defined_routes["rest-candidate-additional-information-details-add"] = array("route_value" => "/rest/candidate-additional-information-details/add",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-additional-information-details-add.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

$user_defined_routes["rest-resume-text-add"] = array("route_value" => "/rest/resume-text/add",
							 "route_var_count" => "4",
							 "page_filename" => "rest-resume-text-add.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
							
$user_defined_routes["rest-profile-source-add"] = array("route_value" => "/rest/profile-source/add",
							 "route_var_count" => "4",
							 "page_filename" => "rest-profile-source-add.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
$user_defined_routes["rest-profile-source-edit"] = array("route_value" => "/rest/profile-source/edit",
							 "route_var_count" => "4",
							 "page_filename" => "rest-profile-source-edit.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to edit Candidate additional details, in Admin Panel							
$user_defined_routes["rest-candidate-additional-information-edit"] = array("route_value" => "/rest/candidate-additional-information/edit",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-additional-information-edit.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);							
							
$user_defined_routes["rest-candidate-all-educational-qualifications-get"] = array("route_value" => "/rest/educational-qualifications/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-all-educational-qualifications-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
$user_defined_routes["rest-candidate-internal-notes-details-get"] = array("route_value" => "/rest/candidate-internal-notes-details/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-internal-notes-details-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);							
							
$user_defined_routes["rest-candidate-all-professional-summary-get"] = array("route_value" => "/rest/professional-summary/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-all-professional-summary-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to get Candidate additional details, in Admin Panel
$user_defined_routes["rest-candidate-additional-information-details-get"] = array("route_value" => "/rest/candidate-additional-information-details/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-additional-information-details-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to get Candidate additional details, in Admin Panel
$user_defined_routes["rest-candidate-uploaded-resume-details-get"] = array("route_value" => "/rest/candidate-uploaded-resume-details/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-uploaded-resume-details-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

$user_defined_routes["rest-candidate-invited-jobs-get"] = array("route_value" => "/rest/candidate-invited-jobs/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-invited-jobs-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
							
$user_defined_routes["rest-candidate-applied-jobs-get"] = array("route_value" => "/rest/candidate-applied-jobs/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-applied-jobs.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
$user_defined_routes["rest-candidate-all-skill-certification-summary-get"] = array("route_value" => "/rest/candidate-all-skill-certification-summary/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-all-skill-certification-summary-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);										

$user_defined_routes["rest-profile-sources-list"] = array("route_value" => "/rest/profile-sources/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-profile-sources-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST" 							 
							);								

//API to provide add candidate aditional information, in Admin Panel
$user_defined_routes["rest-candidate-additional-information-details-add"] = array("route_value" => "/rest/candidate-additional-information-details/add",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-additional-information-details-add.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

$user_defined_routes["rest-candidate-considered-proposal-list-get"] = array("route_value" => "/rest/candidate-considered-proposal-list/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-considered-proposal-list-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

$user_defined_routes["rest-resume-text-add"] = array("route_value" => "/rest/resume-text/add",
							 "route_var_count" => "4",
							 "page_filename" => "rest-resume-text-add.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
							
$user_defined_routes["rest-profile-source-add"] = array("route_value" => "/rest/profile-source/add",
							 "route_var_count" => "4",
							 "page_filename" => "rest-profile-source-add.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
$user_defined_routes["rest-profile-source-edit"] = array("route_value" => "/rest/profile-source/edit",
							 "route_var_count" => "4",
							 "page_filename" => "rest-profile-source-edit.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to edit Candidate additional details, in Admin Panel							
$user_defined_routes["rest-candidate-additional-information-edit"] = array("route_value" => "/rest/candidate-additional-information/edit",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-additional-information-edit.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);							
							
$user_defined_routes["rest-candidate-all-educational-qualifications-get"] = array("route_value" => "/rest/educational-qualifications/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-all-educational-qualifications-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
$user_defined_routes["rest-candidate-all-professional-summary-get"] = array("route_value" => "/rest/professional-summary/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-all-professional-summary-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
$user_defined_routes["rest-candidate-all-skills-summary-get"] = array("route_value" => "/rest/skills-summary/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-candidate-all-skills-summary-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

//API to Get Paste Resume for Candidate
$user_defined_routes["rest-paste-resume-details-get"] = array("route_value" => "/rest/candidate/paste-resume/get",
							 "route_var_count" => "5",
							 "page_filename" => "rest-paste-resume-details-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST" 							 
							);
//API to Get job related received applications list get
$user_defined_routes["rest-job-related-received-applications-list-get"] = array("route_value" => "/rest/job-related-received-applications-list/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-related-received-applications-list-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST" 							 
							);	


//API to Get job related received applications list get
$user_defined_routes["rest-job-rel-considered-proposals-list"] = array("route_value" => "/rest/job-related-considered-proposals/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-rel-considered-proposals-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST" 							 
							);						

//API to Get job related sent invitations list get
$user_defined_routes["rest-job-sent-invitations-list-get"] = array("route_value" => "/rest/job-sent-invitations-list/get",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-sent-invitations-list-get.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST" 							 
							);								
							
							
//API to Collect Skill Sets for Candidate
$user_defined_routes["rest-candidate-all-skills-collect"] = array("route_value" => "/rest/candidate/all-skills/collect",
							 "route_var_count" => "5",
							 "page_filename" => "rest-candidate-all-skills-collect.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST" 							 
							);		
//API to Collect Skill Certification for Candidate
$user_defined_routes["rest-candidate-all-skillcertifications-collect"] = array("route_value" => "/rest/candidate/all-skillcertifications/collect",
							 "route_var_count" => "5",
							 "page_filename" => "rest-candidate-all-skillcertifications-collect.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST" 							 
							);	
							
$user_defined_routes["rest-candidate-internal-notes-add"] = array("route_value" => "/rest/candidate/internal-notes/add",
							 "route_var_count" => "5",
							 "page_filename" => "rest-candidate-internal-notes-add.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);

$user_defined_routes["rest-job-candidate-consider"] = array("route_value" => "/rest/job-rel-candidate/consider",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-candidate-consider.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);	

$user_defined_routes["all-classified-users-list"] = array("route_value" => "/rest/all-classified-users/list",
							 "route_var_count" => "4",
							 "page_filename" => "all-classified-users-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
							
$user_defined_routes["rest-job-rel-classified-users-list"] = array("route_value" => "/rest/job-rel-classified-users/list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-rel-classified-users-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
							
$user_defined_routes["rest-job-rel-other-requirements-collect"] = array("route_value" => "/rest/job-rel-other-requirements/collect",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-rel-other-requirements-collect.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
							
$user_defined_routes["rest-job-invite-rel-mass-campaign-candidates-list"] = array("route_value" => "/rest/job-invite-rel/mass-campaign/candidates-list",
							 "route_var_count" => "5",
							 "page_filename" => "rest-job-invite-rel-mass-campaign-candidates-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);	
//API to add job applicant requests info, in Admin panel
$user_defined_routes["rest-job-applicant-requests-info-add"] = array("route_value" => "/rest/job-applicant-requests-info/add",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-applicant-requests-info-add.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);
//API to add job applicant requests info, in Admin panel
$user_defined_routes["rest-job-role-assignment"] = array("route_value" => "/rest/job-rel-role/assignment",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-role-assignment.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);	
//API to add job applicant requests info, in Admin panel
$user_defined_routes["dashboardpoc"] = array("route_value" => "/rest/dashboardpoc",
							 "route_var_count" => "3",
							 "page_filename" => "dashboardpoc.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);		
$user_defined_routes["rest-job-screening-level-classifications-list"] = array("route_value" => "/rest/job-rel-screening-level/classifications-list",
							 "route_var_count" => "4",
							 "page_filename" => "rest-job-screening-level-classifications-list.php",
							 "is_ajax" => "1",
							 "is_web_service_endpoint" => "1",
							 "is_frontend_page" => "3",
							 "request_method" => "POST"
							);							

?>
